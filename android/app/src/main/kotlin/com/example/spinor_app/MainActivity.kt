package com.spinormedia.kinshealthcare.flutter

import android.app.NotificationChannel
import android.app.NotificationManager
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.util.Log
import com.example.spinor_app.Haptic
import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant


class MainActivity: FlutterFragmentActivity() {
    private val channel = "haptic_controller"
    private lateinit var haptic: Haptic

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {

        GeneratedPluginRegistrant.registerWith(flutterEngine)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val soundUri: Uri = Uri.parse("android.resource://" + applicationContext.packageName + "/" + R.raw.alert)
            val audioAttributes: AudioAttributes = AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build()

            // Creating Channel
            val channel = NotificationChannel("kins_notification_channel",
                    "kins_notification", NotificationManager.IMPORTANCE_HIGH)
            channel.setSound(soundUri, audioAttributes)
            val notificationManager: NotificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }

        registerHaptic(flutterEngine)
    }

    fun registerHaptic(flutterEngine: FlutterEngine) {
        // 햅틱 기능 연결
        haptic = Haptic(this.applicationContext)

        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, channel).setMethodCallHandler { call, result ->
            when (call.method) {
                "canHaptic" -> {
                    result.success(haptic.canHaptic)
                }
                "hapticIntensity" -> {
                    val argMap = call.arguments as? Map<String, Any>
                    val value = argMap?.get("set") as? Double

                    if (value != null) {
                        haptic.setHapticIntensity(value)
                    }

                    result.success(haptic.getHapticIntensity())
                }
                "hapticTime" -> {
                    val argMap = call.arguments as? Map<String, Any>
                    val value = argMap?.get("set") as? Double

                    if (value != null) {
                        haptic.setHapticTime(value)
                    }

                    result.success(haptic.getHapticTime())
                }
                "haptic" -> {
                    haptic.haptic()
                    result.success("")
                }
                "hapticPattern" -> {
                    val argMap = call.arguments as? Map<String, Any>
                    val delayTime = argMap?.get("delayTime") as? DoubleArray
                    val duration = argMap?.get("duration") as? DoubleArray
                    val intensities = argMap?.get("intensities") as? DoubleArray

                    if (delayTime != null && duration != null && intensities != null) {
                        haptic.hapticPattern(delayTime = delayTime, intensities = intensities, duration = duration)
                        result.success("")
                    } else {
                        result.error("ParameterError", "Some parameter is null", null)
                    }
                }
                else -> {
                    result.notImplemented()
                }
            }
        }
    }
}
