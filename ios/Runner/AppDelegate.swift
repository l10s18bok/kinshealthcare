import UIKit
import Flutter


@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {

    @available(iOS 13.0, *)
    private static let haptic:Haptic = Haptic()
    
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        if #available(iOS 13.0, *) {
            registerHaptic()
        }
        
        GeneratedPluginRegistrant.register(with: self)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    @available(iOS 13.0, *)
    private func registerHaptic() {
        let channelName = "haptic_controller"
        
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
        let channel = FlutterMethodChannel(name: channelName,
                                                      binaryMessenger: controller.binaryMessenger)
        channel.setMethodCallHandler({ (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            
            switch call.method {
            case "canHaptic":
                result(Haptic.canHaptic)
                break
            case "haptic":
                AppDelegate.haptic.haptic()
                result(true)
                break
            case "hapticTime":
                if let args = call.arguments as? [String:NSNumber],
                   let value = args["set"]{
                    Haptic.hapticDuration = Double(truncating: value)
                    result(true)
                    break
                } else {
                    result(Haptic.hapticDuration)
                    break
                }
            case "hapticIntensity":
                if let args = call.arguments as? [String:NSNumber],
                   let value = args["set"]{
                    Haptic.hapticIntensity = Double(truncating: value)
                    result(true)
                    break
                } else {
                    result(Haptic.hapticIntensity)
                    break
                }
            case "hapticPattern":
                guard let args = call.arguments as? [String:FlutterStandardTypedData],
                      let delayTime = self.makeDoubleArray(args["delayTime"]),
                      let intensities = self.makeDoubleArray(args["intensities"]),
                      let duration = self.makeDoubleArray(args["duration"]) else {
                    result(FlutterError(code: call.method, message: "wrong arguments or nil", details: nil))
                    break
                }
                
                AppDelegate.haptic.hapticPattern(delayTime: delayTime, duration: duration, intensities: intensities)
                result(true)
                break
            default:
                result(false)
            }
        })

        
    }
    
}


@available(iOS 13.0, *)
extension AppDelegate {
    
    func makeDoubleArray(_ data:FlutterStandardTypedData?) -> [Double]? {
        guard data != nil && data!.type == FlutterStandardDataType.float64 else {
            return nil
        }
        
        return makeArray([UInt8](data!.data))
    }
    
    private func makeArray(_ bytes:[UInt8]) -> [Double]? {
        let size = MemoryLayout<Double>.size
        
        guard bytes.count % size == 0 else {
            return nil
        }
        
        var array:[Double] = []
        
        for index in stride(from: 0, to: bytes.count, by: size) {
            array.append(Double(Array(bytes[index..<index + size])) ?? 0)
        }
        
        return array
    }
}

extension FloatingPoint {
    init?(_ bytes: [UInt8]) {
        guard bytes.count == MemoryLayout<Self>.size else { return nil }
        self = bytes.withUnsafeBytes {
            return $0.load(fromByteOffset: 0, as: Self.self)
        }
    }
    
}

