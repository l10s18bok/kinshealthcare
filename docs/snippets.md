# 스니펫 수동입력 가이드 문서

스니펫이 제대로 동작하지 않는다면 해당 문서를 참고하여 작성해주시기 바랍니다.

1. [안드로이드 스튜디오](#안드로이드-스튜디오)
2. [VS Code](#vs-code)

---

## 1. 안드로이드 스튜디오
참고문서 : https://www.jetbrains.com/help/idea/creating-and-editing-live-templates.html
- **주의 : IntelliJ IDEA는 실제로 테스트해보지 않음**
1. 안드로이드 스튜디오의 환경설정에서 Editor - Live Templates로 이동
2. 아래의 내용을 복사한 다음에 원하는 위치에서 오른클릭한 다음 메뉴에서 Paste 선택하거나 Ctrl+V(맥은 ⌘+V)로 붙여넣기 (원하는 부분 변경해도 무방)
~~~
<template name="headerLog" value="$BLOCKSTART$&#10; 작성일 : $DATE$&#10; 작성자 : $USER$&#10; 화면명 : $SCREEN$&#10; 경로 : $PATH$&#10; 클래스 : $CLASS$&#10; 설명 : $DESC$&#10;$BLOCKEND$&#10;$END$" shortcut="ENTER" description="킨즈 플러터 다트 헤더 로그 스니펫" toReformat="true" toShortenFQNames="true">
  <variable name="BLOCKSTART" expression="blockCommentStart()&#9;" defaultValue="" alwaysStopAt="false" />
  <variable name="DATE" expression="date(&quot;YYYY-MM-DD&quot;)" defaultValue="(오늘 날짜)" alwaysStopAt="false" />
  <variable name="USER" expression="" defaultValue="(작성자 이름 입력)" alwaysStopAt="true" />
  <variable name="SCREEN" expression="" defaultValue="(화면 코드 입력)" alwaysStopAt="true" />
  <variable name="PATH" expression="" defaultValue="" alwaysStopAt="true" />
  <variable name="CLASS" expression="capitalize(camelCase(fileNameWithoutExtension()))" defaultValue="(클래스 이름)" alwaysStopAt="false" />
  <variable name="DESC" expression="" defaultValue="(클래스 설명 입력)" alwaysStopAt="true" />
  <variable name="BLOCKEND" expression="blockCommentEnd()" defaultValue="" alwaysStopAt="false" />
  <context>
    <option name="DART_TOPLEVEL" value="true" />
  </context>
</template>
~~~

---

## 2. VS Code
참고문서 : https://code.visualstudio.com/docs/editor/userdefinedsnippets
- **주의 : 개별 항목에 대한 설명은 이 문서에서 하지 않으므로 참고문서 링크를 보기 바람**
1. 파일 생성 메뉴
    - 윈도 : File - Preferences - User Snippets
    - 맥 : Code - Preferences - User Snippets    
2. Global 혹은 프로젝트 전용중 맘에 드는거 암거나 선택하고 이름 입력하여 생성
3. 내용은 다음의 것을 입력하기 (원하는 부분 변경해도 무방)    
~~~
{
	"Write Dart Header Log for Kins": {
		"prefix": ["headerLog", "hl"],
		"body": [
			"$BLOCK_COMMENT_START",
			" 작성일 : ${CURRENT_YEAR}-${CURRENT_MONTH}-${CURRENT_DATE}",
			" 작성자 : ${1:(작성자 이름 입력)}",
			" 화면명 : ${2:(화면 코드 입력)}",
			" 클래스 : ${TM_FILENAME_BASE/(.*)/${1:/pascalcase}/}",
			" 경로 : ${3}",
			" 설명 : ${4:(클래스 설명 입력)}",
			"$BLOCK_COMMENT_END",
			"${0}"
		],
		"description": "킨즈 플러터 다트 헤더 로그 스니펫"
	}
}
~~~
---
