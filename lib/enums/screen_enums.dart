//알림 설정 화면에서 사용하기 위한 Enum
enum NotificationType {
  KKAKKA,
  PAYMENT,
  ANNIVERSARY,
  KKAKKA_SUM,
  RE_INSSUANCE,
  MEDICAL,
  EVENT,
  DISTURB,
  VIBRATOR,
}

//게시판 형태의 화면에서 사용하기 위한 Enum
enum BoardType {
  NOTICE,
  INQUIRY,
  FAQ,
  INFO,
}
