//해당 클래스와 아래의 enum은 순서가 연관되어 있으므로 enum 수정시에 같이 수정바랍니다
//CategoryFilter

import 'package:flutter/foundation.dart';

/// 까까 상태
///
/// - 작성자 : ???
/// - 작성일 : ???
///
/// {@category Enum}
enum KkakkaStatus {
  ///활성
  ACTIVE,

  ///해제
  DROP,

  ///
  USE_ALL,

  ///
  NO_PAYMENT,

  ///전체 (서버에는 null로 전달하기)
  ALL,
}
enum KkakkaFilterStatus {
  ///요청
  REQUEST,

  ///승인
  ACCEPTE,

  ///취소
  CANCEL,

  ///거절
  DENY,

  ///활성
  ACTIVE,

  ///해제
  DROP,

  ///모두사용
  USE_ALL,
}

///까까 유형
///
///괄호 안의 글자는 서버에 보낼 때 사용하는 문자
enum KkakkaType {
  ///기념일 (A)
  ANNIVERSARY,

  ///기부 (D)
  DONATE,

  ///지역 (E)
  AREA,

  ///의료 (M)
  MEDICAL,

  ///용돈 (P)
  PIN,

  ///셀프 (S)
  SELF,

  ///복지 (W)
  WELFARE,

  ///교육 (E)
  EDUCATION,

  ///전체 (서버에는 null로 전달하기)
  ALL,
}

extension KkakkaTypeExtension on KkakkaType {
  String get name => describeEnum(this);

  String? get krName {
    switch (this) {
      case KkakkaType.ANNIVERSARY:
        return '기념일 (A)';

      case KkakkaType.DONATE:
        return '기부 (D)';

      case KkakkaType.AREA:
        return '지역 (E)';

      case KkakkaType.MEDICAL:
        return '의료 (M)';

      case KkakkaType.PIN:
        return '용돈 (P)';

      case KkakkaType.SELF:
        return '셀프 (S)';

      case KkakkaType.WELFARE:
        return '복지 (W)';

      case KkakkaType.EDUCATION:
        return '교육';

      case KkakkaType.ALL:
        return '전체 (서버에는 null로 전달하기)';
    }
  }

  KkakkaType? parseType(String rawType) {
    for (KkakkaType type in KkakkaType.values) {
      if (describeEnum(type) == rawType) {
        return type;
      }
    }

    return null;
  }
}

///까까 요청 상태
enum KkakkaReqStatus {
  ///요청함
  REQUEST,

  ///승낙됨
  APPROVE,

  ///취소함
  CANCEL,

  ///거부함
  REJECT,

  ///전체 (서버에는 null로 전달하기)
  ALL,
}

///까까 필터링 조건
enum KkakkaFilterType {
  ///요청한 (내가 요청함)
  REQUESTME,

  ///요청받은 (상대가 요청함)
  REQUESTOPPONENT,

  ///발행함 (상대가 나에게 보냄)
  SENDME,

  ///발행받은 (내가 상대에게 보냄)
  SENDOPPONENT,

  ///전체 (서버에는 null로 전달하기)
  ALL,
}

enum UserStatus {
  ACTIVE,
  DROP,
  WITHDRAW,
}

///까까 정렬방식
///서버에서 정렬하는 칼럼명
enum SortType {
  ///ID, 기본값
  ID,

  ///시작 일자
  START_DATE,

  ///종료 일자
  END_DATE,
}
