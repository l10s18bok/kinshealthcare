import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

part 'base_model.g.dart';

@JsonSerializable()
class BaseModel {
  @JsonKey(ignore: true)
  TextUtils textUtils = TextUtils();

  BaseModel();

  factory BaseModel.fromJson(Map<String, dynamic> json) => _$BaseModelFromJson(json);

  Map<String, dynamic> toJson() => _$BaseModelToJson(this);

  T stringToEnum<T>({
    required List<T> enums,
    required String stringVal,
  }) {
    final exactMatchTypes = [
      KkakkaStatus,
      KkakkaType,
    ];

    final firstAlphaMatchTypes = [
      UserStatus,
    ];

    final val = enums.firstWhere((element) {
      final split = element.toString().split('.');

      if (exactMatchTypes.contains(T)) {
        return parseEnumExactMatch(strVal: stringVal, enumVal: split[1]);
      } else if (firstAlphaMatchTypes.contains(T)) {
        return parseFirstAlphaMatch(strVal: stringVal, enumVal: split[1]);
      } else {
        throw Exception('no matching enum type found');
      }
    });

    return val;
  }

  static bool parseEnumExactMatch({
    required String strVal,
    required String enumVal,
  }) {
    return strVal == enumVal;
  }

  static bool parseFirstAlphaMatch({
    required String strVal,
    required String enumVal,
  }) {
    return strVal == enumVal[0];
  }
}
