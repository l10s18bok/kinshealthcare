import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

import 'local/like_reply_model.dart';
import 'local/post_model.dart';

part 'kkakka_model.g.dart';

/// 까까 기본모델. 모든 까까 관련 모델들의 base 가 되는
/// 클래스로 사용하려 했지만 모델 정규화 부재로 대기중
///
/// - 작성자 : JC
/// - 작성일 : 2021-05-01
///
/// {@category Model}
@JsonSerializable()
class KkakkaModel extends BaseRetrofitModel {
  final int? kkakkaBalance;
  final int? kkakkaId;
  final String? kkakkaImage;
  final int? kkakkaLimit;
  final String? kkakkaMessage;
  final int? kkakkaPrice;
  final DateTime? endDate;
  final String? profileImgOpponent;
  final DateTime? startDate;
  final String? userNameOpponent;
  final String? userPhoneOpponent;
  final String? userRelCdMe;
  final String? userRelCdOpponent;
  final DateTime? issueDate;
  final String? authYn;

  int? likeYn;
  int? likeCount;
  String? firstLikeUserName;
  final String? firstRepl;
  final String? firstReplUserName;
  final String? firstReplImg;
  bool? isContentOpen;

  @JsonKey(name: 'kkakkaStatus')
  final String? _kkakkaStatus;
  @JsonKey(name: 'userStatusOpponent')
  final String? _userStatusOpponent;
  @JsonKey(name: 'kkakkaType')
  final String? _kkakkaType;

  @JsonKey(ignore: true)
  final _kkakkaStatusMap = {
    '활성': KkakkaStatus.ACTIVE,
    '해제': KkakkaStatus.DROP,
    '모두사용': KkakkaStatus.USE_ALL,
    'ACTIVE': KkakkaStatus.ACTIVE,
    'DROP': KkakkaStatus.DROP,
    'USE_ALL': KkakkaStatus.USE_ALL,
  };
  @JsonKey(ignore: true)
  final _userStatusOpponentMap = {
    '정상': UserStatus.ACTIVE,
    '탈퇴': UserStatus.DROP,
    '휴면': UserStatus.WITHDRAW,
    'ACTIVE': UserStatus.ACTIVE,
    'DROP': UserStatus.DROP,
    'WITHDRAW': UserStatus.WITHDRAW,
  };
  @JsonKey(ignore: true)
  final _kkakkaTypeMap = {
    '기념일': KkakkaType.ANNIVERSARY,
    '기부': KkakkaType.DONATE,
    '지역': KkakkaType.AREA,
    '의료': KkakkaType.MEDICAL,
    '용돈': KkakkaType.PIN,
    'SELF': KkakkaType.SELF,
    '복지': KkakkaType.WELFARE,
    'ANNIVERSARY': KkakkaType.ANNIVERSARY,
    'DONATE': KkakkaType.DONATE,
    'AREA': KkakkaType.AREA,
    'MEDICAL': KkakkaType.MEDICAL,
    'PIN': KkakkaType.PIN,
    'WELFARE': KkakkaType.WELFARE,
  };

  KkakkaModel({
    required this.kkakkaBalance,
    this.kkakkaId,
    this.kkakkaImage,
    required this.kkakkaLimit,
    this.kkakkaMessage,
    required this.kkakkaPrice,
    required this.endDate,
    this.profileImgOpponent,
    this.startDate,
    this.userNameOpponent,
    this.userPhoneOpponent,
    this.userRelCdMe,
    this.userRelCdOpponent,
    this.issueDate,
    this.authYn,
    this.likeYn,
    this.likeCount,
    this.firstLikeUserName,
    this.firstRepl,
    this.firstReplUserName,
    this.firstReplImg,
    String? kkakkaStatus,
    String? userStatusOpponent,
    String? kkakkaType,
  })  : this._kkakkaStatus = kkakkaStatus,
        this._userStatusOpponent = userStatusOpponent,
        this._kkakkaType = kkakkaType;

  factory KkakkaModel.fromJson(Object? json) =>
      _$KkakkaModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$KkakkaModelToJson(this);

  get userStatusOpponent {
    return this._userStatusOpponentMap[this._userStatusOpponent];
  }

  get kkakkaType {
    // TODO 서버쪽 작업이 느려져서 디폴트값 넣었습니다.
    // 원래는 값 없으면 에러 던지는게 맞습니다.

    final type = this._kkakkaTypeMap[this._kkakkaType];

    return type ?? this._kkakkaTypeMap['기념일'];
  }

  get kkakkaStatus {
    return this._kkakkaStatusMap[this._kkakkaStatus];
  }

  get daysTillEndDate {
    if (this.endDate == null) return null;
    var inday = this.endDate!.difference(DateTime.now()).inDays;
    var inHorwer = this.endDate!.difference(DateTime.now()).inHours;
    var inMinutes = this.endDate!.difference(DateTime.now()).inMinutes;
    if (inday > 0) {
      return '$inday일';
    } else if (inHorwer > 0) {
      return '$inHorwer시간';
    } else if (inMinutes > 0) {
      return '$inMinutes분';
    } else {
      return '방금';
    }
  }

  get kkakkaTypeKr {
    return this._kkakkaType;
  }

  get kkakkaPriceDisplay {
    if (this.kkakkaPrice == null) {
      return '0';
    }

    return super.textUtils.numberToLocalCurrency(
          amount: this.kkakkaPrice!.toDouble(),
        );
  }

  get kkakkaBalanceDisplay {
    if (this.kkakkaBalance == null) {
      return '0';
    }

    return super.textUtils.numberToLocalCurrency(
          amount: this.kkakkaBalance!.toDouble(),
        );
  }

  get kkakkaLimitDisplay {
    if (this.kkakkaLimit == null) {
      return '0';
    }

    return super.textUtils.numberToLocalCurrency(
          amount: this.kkakkaLimit!.toDouble(),
        );
  }

  get kkakkaIcName {
    Map<KkakkaType, String> dict = {
      KkakkaType.ANNIVERSARY: 'ic_anniversary_kkakka.svg',
      KkakkaType.AREA: 'ic_area_kkakka.svg',
      KkakkaType.DONATE: 'ic_donate_kkakka.svg',
      KkakkaType.MEDICAL: 'ic_medical_kkakka.svg',
      KkakkaType.PIN: 'ic_pin_kkakka.svg',
      KkakkaType.SELF: 'ic_self_kkakka.svg',
      KkakkaType.WELFARE: 'ic_welfare_kkakka.svg',
    };

    return dict[this.kkakkaType];
  }

  get primaryColor {
    // TODO setup primary color options

    Map<KkakkaType, Color> dict = {
      KkakkaType.ANNIVERSARY: Color(0xFFFF9B95),
      KkakkaType.AREA: Color(0xFFFF9B95),
      KkakkaType.DONATE: Color(0xFF8B6ADC),
      KkakkaType.MEDICAL: Color(0xFF6FA7FA),
      KkakkaType.PIN: Color(0xFF6CC4BF),
      KkakkaType.SELF: Color(0xFF6CC4BF),
      KkakkaType.WELFARE: Color(0xFF8B6ADC),
    };

    return dict[this.kkakkaType];
  }

  LikeReplyModel get likeReplyModel {
    return LikeReplyModel(
      likeCount: this.likeCount,
      likeYn: this.likeYn,
      firstLikeName: this.firstLikeUserName,
      firstRepl: this.firstRepl,
      firstReplName: this.firstReplUserName,
      firstReplImg: this.firstReplImg,
      message: this.kkakkaMessage,
      isContentOpen: this.isContentOpen,
      localModel: PostLocalModel(
        boardNo: this.kkakkaId!,
        boardType: 'K',
      ),
    );
  }
}
