import 'package:kins_healthcare/models/models.dart';

class UserModel extends BaseModel {
  final String username;
  final String name;

  UserModel.fromJson({
    required Map<String, dynamic> json,
  })   : this.username = json['username'],
        this.name = json['name'];

  UserModel({
    required String username,
    required String name,
    int? id,
  })  : this.username = username,
        this.name = name;
}
