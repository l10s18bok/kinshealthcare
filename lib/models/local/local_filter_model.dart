import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/services/kkakka/model/list_filter_pagination_body.dart';
/*
 작성일 : 2021-03-02
 작성자 : Mark
 화면명 : 
 클래스 : LocalFilterModel
 경로 : 
 설명 : 까까 필터링 통신을 위한 클래스
*/

///까까 필터링 통신을 위한 클래스
///보낼때는 Get.back(result: model); 방식읊 사용합니다

part 'local_filter_model.g.dart';

@JsonSerializable()
class LocalFilterModel {
  int page = 0;
  int _size = 10;

  ///정렬 종류
  SortType? sortBy;

  ///정렬이 정순인가 역순인가
  bool? isAsc;

  ///까까의 필터링 조건
  KkakkaFilterType? filterType;

  ///까까 유형
  KkakkaType? type;

  ///까까 상태
  KkakkaStatus? status;

  ///시작 시간, 형식은 yyyy-mm-dd 사용
  String? fromDate;

  ///끝 시간, 형식은 yyyy-mm-dd 사용
  String? toDate;

  ///디버깅용 출력 함수
  @override
  String toString() {
    return '[LocalFilterModel] sortBy : $sortBy, isAsc : $isAsc, filterType : $filterType, type : $type, status : $status, fromDate : $fromDate, toDate : $toDate';
  }

  String get filterTypeString {
    switch (filterType) {
      case KkakkaFilterType.REQUESTME:
        return 'REQME';
      case KkakkaFilterType.REQUESTOPPONENT:
        return 'REQOPPO';
      case KkakkaFilterType.SENDME:
        return 'SENDME';
      case KkakkaFilterType.SENDOPPONENT:
        return 'SENDOPPO';
      case KkakkaFilterType.ALL:
        return '';
      default:
    }
    return '';
  }

  String get typeString {
    switch (type) {
      case KkakkaType.ANNIVERSARY:
        return 'A';
      case KkakkaType.DONATE:
        return 'D';
      case KkakkaType.AREA:
        return 'E';
      case KkakkaType.MEDICAL:
        return 'M';
      case KkakkaType.PIN:
        return 'P';
      case KkakkaType.SELF:
        return 'S';
      case KkakkaType.WELFARE:
        return 'W';
      case KkakkaType.ALL:
        return '';
      default:
    }
    return '';
  }

  String get statusString {
    switch (status) {
      case KkakkaStatus.ACTIVE:
        return 'ACTIVE';
      case KkakkaStatus.DROP:
        return 'DROP';
      case KkakkaStatus.USE_ALL:
        return 'USE_ALL';
      case KkakkaStatus.NO_PAYMENT:
        return 'NO_PAYMENT';
      case KkakkaStatus.ALL:
        return '';
      default:
    }
    return '';
  }

  String get sortString {
    switch (sortBy) {
      case SortType.ID:
        return 'id';
      case SortType.START_DATE:
        return 'startDate';
      case SortType.END_DATE:
        return 'endDate';
      default:
    }
    return 'id';
  }

  DateTime get defaultStartDate => DateTime.now().subtract(Duration(days: 7));
  DateTime get defaultEndDate => DateTime.now();

  get listFilterBody {
    final format = DateFormat('yyyy-MM-dd');

    return ListFilterPaginationBody(
      direction: isAsc ?? false,
      page: page,
      size: _size,
      sortBy: sortString,
      status: statusString,
      kkakkaFilterType: filterTypeString,
      type: typeString,
      fromDate: fromDate ?? format.format(defaultStartDate),
      toDate: toDate ?? format.format(defaultEndDate),
    );
  }
}
