class PostLocalModel {
  int boardNo;
  String boardType;

  PostLocalModel({
    required this.boardNo,
    required this.boardType,
  });
}
