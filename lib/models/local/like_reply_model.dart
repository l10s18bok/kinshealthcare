import 'package:kins_healthcare/models/local/post_model.dart';

class LikeReplyModel {
  int? likeCount;
  int? likeYn;
  String? firstLikeName;
  String? firstRepl;
  String? firstReplName;
  String? firstReplImg;
  String? message;
  bool? isContentOpen;
  bool? isTabMenu;
  PostLocalModel? localModel;

  LikeReplyModel(
      {this.likeCount,
      this.likeYn,
      this.firstLikeName,
      this.firstRepl,
      this.firstReplName,
      this.firstReplImg,
      this.message,
      this.isContentOpen,
      this.localModel,
      this.isTabMenu});
}
