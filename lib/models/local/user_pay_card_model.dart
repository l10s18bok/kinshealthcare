import 'dart:ui';

import 'package:kins_healthcare/services/user/payment/model/payment_card_info.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-03-18
 작성자 : Andy
 화면명 :
 클래스 : UserPayCardModel
 경로 :
 설명 : 까까 보내기에서 카드 선택, 컬러, 로고이미지를 위한 모델
*/

class UserPayCardModel {
  PaymentCardInfoResponse? cardInfo;
  String? imgUrl;
  bool isSelected = false;
  Color? color;

  UserPayCardModel({
    this.cardInfo,
    this.imgUrl,
    this.isSelected = false,
    this.color,
  });

  get cardColor {

    return bankNameColor[this.cardInfo!.paymentCorp];
  }

  get cardPath {

    return bankLogo[this.cardInfo!.paymentCorp];
  }
}
