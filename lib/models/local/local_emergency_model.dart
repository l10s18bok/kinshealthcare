import 'package:kins_healthcare/services/emergency/model/post_search_list_model.dart';

/*
 작성일 : 2021-03-16
 작성자 : Andy,
 화면명 : (화면명),
 경로 : 없음
 클래스 : LocalEmergencyModel,
 설명 : Screen 이동시에 병원 정보를 저장하기 위해 사용되는 Model
*/

class LocalEmergencyModel {
  int? hsptlId;
  List<PostSearchListResponse>? regList;
}

class EmHospitalDetailData {
  int? hsptlId;
  String? hsptlName;

  EmHospitalDetailData({
    this.hsptlId,
    this.hsptlName,
  });
}
