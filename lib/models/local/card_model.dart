import 'package:flutter/material.dart';

/*
 작성일 : 2021-03-30
 작성자 : Mark
 화면명 :
 클래스 : CardModel
 경로 :
 설명 : Credit Card Widget 에 Data 를 표시하기 위한 Model
*/

class CardModel {
  String title;
  String imagePath;
  Color color;
  Color textColor;
  String? cardNumber;
  bool isNon;
  String? bankCode;  // 2021.05.31 Andy 추가

  CardModel({
    required this.title,
    required this.imagePath,
    required this.color,
    this.textColor = Colors.white,
    this.cardNumber,
    this.isNon = false,
    this.bankCode,
  });
}
