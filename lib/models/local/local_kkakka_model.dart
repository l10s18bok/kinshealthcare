import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/box_grid_model.dart';
import 'package:kins_healthcare/services/kkakka/model/get_request_models.dart';
import 'package:kins_healthcare/services/relation/model/post_relation_search_models.dart';
import 'package:kins_healthcare/services/suggest/model/suggest_detail_model.dart';

/*
 작성일 : 2021-02-23
 작성자 : Andy,
 화면명 : (화면명),
 경로 : 없음
 클래스 : LocalKkakkaModel,
 설명 : Local 에서 Screen 이동시에 Kkakka 정보를 저장하기 위해 사용되는 Kkakka Model
*/

class LocalKkakkaModel {
  KkakkaType? type;

  String get kkakkaType {
    switch (type) {
      case KkakkaType.ANNIVERSARY:
        return 'ANNIVERSARY';
      case KkakkaType.DONATE:
        return 'DONATE';
      case KkakkaType.AREA:
        return 'AREA';
      case KkakkaType.MEDICAL:
        return 'MEDICAL';
      case KkakkaType.PIN:
        return 'PIN';
      case KkakkaType.SELF:
        return 'SELF';
      case KkakkaType.WELFARE:
        return 'WELFARE';
      case KkakkaType.ALL:
        return 'WELFARE';
      default:
    }
    return 'WELFARE';
  }

  KkakkaType getTitleToKkakkaType(String title) {
    switch (title) {
      case '기념일':
        return KkakkaType.ANNIVERSARY;
      case '기부':
        return KkakkaType.DONATE;
      case '지역':
        return KkakkaType.AREA;
      case '의료비':
        return KkakkaType.MEDICAL;
      case '용돈':
        return KkakkaType.PIN;
      case '교육비':
        return KkakkaType.SELF;
      case '복지':
        return KkakkaType.WELFARE;
    }
    return KkakkaType.WELFARE;
  }

  String get getEngTitleToKkakkaType {
    switch (requestModel!.kkakkaType) {
      case 'ANNIVERSARY':
        return '기념일';
      case 'DONATE':
        return '기부';
      case 'AREA':
        return '지역';
      case 'MEDICAL':
        return '의료비';
      case 'PIN':
        return '용돈';
      case 'SELF':
        return '교육비';
      case 'WELFARE':
        return '복지';
    }
    return '기념일';
  }

  BoxGridModel getCategory() {
    switch (type) {
      case KkakkaType.ANNIVERSARY:
        return BoxGridModel(
          title: '기념일',
          imagePath: 'assets/svgs/ic/ic_anniversary_publish_unselect.svg',
        );
      case KkakkaType.MEDICAL:
        return BoxGridModel(
          title: '의료비',
          imagePath: 'assets/svgs/ic/ic_medical_publish_unselect.svg',
        );
      case KkakkaType.PIN:
        return BoxGridModel(
          title: '용돈',
          imagePath: 'assets/svgs/ic/ic_money_publish_unselect.svg',
        );
      case KkakkaType.DONATE:
        return BoxGridModel(
          title: '기부',
          imagePath: 'assets/svgs/ic/ic_money_publish_unselect.svg',
        );
      default:
    }

    return BoxGridModel(
      title: '기념일',
      imagePath: 'assets/svgs/ic/ic_anniversary_publish_unselect.svg',
    );
  }

  modelSet() {
    userRelation = SearchRelResponse(
      profileImgOpponent: suggestDetailModel!.userImage ?? '',
      userNameOpponent: suggestDetailModel!.userNameOpponent!,
      userRelCdOpponentValue: suggestDetailModel!.userFamilyNameOpponent ?? '',
      userPhoneOpponent: suggestDetailModel!.userPhone ?? '',
      userNoRel: suggestDetailModel!.userNoTarget!,
      userRelCdMeValue: suggestDetailModel!.userRelCdMe ?? '',
      userStatusOpponent: suggestDetailModel!.kkakkaStatus ?? '',
    );
    requestModel = RequestModel(
      userRelNo: suggestDetailModel!.suggestNo!,
      kkakkaType: suggestDetailModel!.kkakkaType!,
      kkakkaReqStatus: suggestDetailModel!.kkakkaStatus,
      startDate: suggestDetailModel!.startDate,
      endDate: suggestDetailModel!.endDate,
      kkakkaPrice: suggestDetailModel!.kkakkaPrice,
    );
  }

  SearchRelResponse? userRelation;
  RequestModel? requestModel;
  SuggestDetailModel? suggestDetailModel;
}
