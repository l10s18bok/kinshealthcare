// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'local_filter_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LocalFilterModel _$LocalFilterModelFromJson(Map<String, dynamic> json) {
  return LocalFilterModel()
    ..page = json['page'] as int
    ..sortBy = _$enumDecodeNullable(_$SortTypeEnumMap, json['sortBy'])
    ..isAsc = json['isAsc'] as bool?
    ..filterType =
        _$enumDecodeNullable(_$KkakkaFilterTypeEnumMap, json['filterType'])
    ..type = _$enumDecodeNullable(_$KkakkaTypeEnumMap, json['type'])
    ..status = _$enumDecodeNullable(_$KkakkaStatusEnumMap, json['status'])
    ..fromDate = json['fromDate'] as String?
    ..toDate = json['toDate'] as String?;
}

Map<String, dynamic> _$LocalFilterModelToJson(LocalFilterModel instance) =>
    <String, dynamic>{
      'page': instance.page,
      'sortBy': _$SortTypeEnumMap[instance.sortBy],
      'isAsc': instance.isAsc,
      'filterType': _$KkakkaFilterTypeEnumMap[instance.filterType],
      'type': _$KkakkaTypeEnumMap[instance.type],
      'status': _$KkakkaStatusEnumMap[instance.status],
      'fromDate': instance.fromDate,
      'toDate': instance.toDate,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

K? _$enumDecodeNullable<K, V>(
  Map<K, V> enumValues,
  dynamic source, {
  K? unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<K, V>(enumValues, source, unknownValue: unknownValue);
}

const _$SortTypeEnumMap = {
  SortType.ID: 'ID',
  SortType.START_DATE: 'START_DATE',
  SortType.END_DATE: 'END_DATE',
};

const _$KkakkaFilterTypeEnumMap = {
  KkakkaFilterType.REQUESTME: 'REQUESTME',
  KkakkaFilterType.REQUESTOPPONENT: 'REQUESTOPPONENT',
  KkakkaFilterType.SENDME: 'SENDME',
  KkakkaFilterType.SENDOPPONENT: 'SENDOPPONENT',
  KkakkaFilterType.ALL: 'ALL',
};

const _$KkakkaTypeEnumMap = {
  KkakkaType.ANNIVERSARY: 'ANNIVERSARY',
  KkakkaType.DONATE: 'DONATE',
  KkakkaType.AREA: 'AREA',
  KkakkaType.MEDICAL: 'MEDICAL',
  KkakkaType.PIN: 'PIN',
  KkakkaType.SELF: 'SELF',
  KkakkaType.WELFARE: 'WELFARE',
  KkakkaType.EDUCATION: 'EDUCATION',
  KkakkaType.ALL: 'ALL',
};

const _$KkakkaStatusEnumMap = {
  KkakkaStatus.ACTIVE: 'ACTIVE',
  KkakkaStatus.DROP: 'DROP',
  KkakkaStatus.USE_ALL: 'USE_ALL',
  KkakkaStatus.NO_PAYMENT: 'NO_PAYMENT',
  KkakkaStatus.ALL: 'ALL',
};
