import 'package:kins_healthcare/models/base_model.dart';

class LocalModel extends BaseModel {
  String? userName;
  String? nickName;

  LocalModel({
    String? userName,
    String? nickName,
  })  : this.userName = userName,
        this.nickName = nickName;
}
