import 'package:kins_healthcare/services/comm/model/cert_ok_email_model.dart';
import 'package:kins_healthcare/services/register/model/reg_ok_email_model.dart';

class EmailVerifyFormModel {
  String? _email;
  String? _verificationCode;
  int? _seq;

  setEmail(String? val) {
    this._email = val;
  }

  setVerificationCode(String? val) {
    this._verificationCode = val;
  }

  setSeq(int? val) {
    this._seq = val;
  }

  String? get verificationCode => _verificationCode;

  String? get email => _email;

  toBody() {
    return CertOkEmailBody(
      email: this._email,
      certText: this._verificationCode,
    );
  }

  toRegBody() {
    return RegOkEmailBody(
      email: this._email,
      certText: this._verificationCode,
      seq: this._seq,
    );
  }
}
