/*
 * 작성일 : 2021-02-27
 * 작성자 : JC
 * 화면명 : HR_0001
 * 경로 :
 * 클래스 : LoginFormModel
 * 설명 : //
 */
class LoginFormModel {
  String? _email;
  String? _password;

  setEmail(String? email) {
    _email = email;
  }

  setPassword(String? password) {
    _password = password;
  }

  get email => _email;

  get password => _password;
}
