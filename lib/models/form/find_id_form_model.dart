import 'package:kins_healthcare/services/login/model/find_id_model.dart';

class FindIdFormModel {
  String? _lastname;
  String? _firstname;
  String? _phone;
  String? _nation;

  setFirstname(String? firstname) {
    this._firstname = firstname;
  }

  setLastname(String? lastname) {
    this._lastname = lastname;
  }

  setPhone(String? phone) {
    this._phone = phone;
  }

  // Form Field Setter 라 dynamic 으로 받아야함
  setNation(nation) {
    this._nation = nation;
  }

  String? get nation => _nation;

  String? get phone => _phone;

  String? get firstname => _firstname;

  String? get lastname => _lastname;

  String? get name {
    if (_lastname == null) return null;
    if (_firstname == null) return null;
    return _lastname! + _firstname!;
  }

  FindIdBody toBody() {
    return FindIdBody(
      firstname: this._firstname,
      lastname: this._lastname,
      nation: this._nation,
      phone: this._phone,
    );
  }
}
