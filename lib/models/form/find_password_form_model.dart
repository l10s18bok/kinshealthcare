import 'package:kins_healthcare/services/login/model/find_password_model.dart';

/*
 * 작성일 : 2021-02-27
 * 작성자 : JC
 * 화면명 : HL_IP_0002
 * 경로 :
 * 클래스 : FindPasswordFormModel
 * 설명 :
 */
class FindPasswordFormModel {
  String? _email;
  String? _nation;
  String? _phone;

  setEmail(String? val) {
    _email = val;
  }

  setNation(dynamic val) {
    _nation = val;
  }

  setPhone(String? val) {
    _phone = val;
  }

  toBody() {
    return FindPasswordBody(
      phone: this._phone,
      email: this._email,
      nation: this._nation,
    );
  }

  String? get phone => _phone;

  String? get nation => _nation;

  String? get email => _email;
}
