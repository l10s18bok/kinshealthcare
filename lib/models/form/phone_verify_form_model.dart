import 'package:kins_healthcare/services/comm/model/cert_ok_sms_model.dart';
import 'package:kins_healthcare/services/register/model/reg_ok_sms_model.dart';

class PhoneVerifyFormModel {
  String? _phone;
  String? _certText;
  int? _seq;

  setPhone(String? val) {
    this._phone = val;
  }

  setCertText(String? val) {
    this._certText = val;
  }

  setSeq(int? val) {
    this._seq = val;
  }

  String? get certText => _certText;

  String? get phone => _phone;

  toBody() {
    return CertOkSmsBody(
      phone: this._phone,
      certText: this._certText,
    );
  }

  toRegBody() {
    return RegOkSmsBody(
      phone: this._phone,
      certText: this._certText,
      seq: this._seq!,
    );
  }
}
