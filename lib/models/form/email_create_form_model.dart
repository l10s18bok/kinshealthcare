import 'package:kins_healthcare/services/user/model/email_create_models.dart';

class EmailCreateFormModel {
  String? _lastname;
  String? _firstname;
  String? _id;
  String? _password;
  String? _nation;
  String? _question;
  String? _answer;

  setLastname(String? val) {
    this._lastname = val;
  }

  setFirstname(String? val) {
    this._firstname = val;
  }

  setId(String? val) {
    this._id = val;
  }

  setPassword(String? val) {
    this._password = val;
  }

  setNation(dynamic val) {
    this._nation = val;
  }

  setQuestion(dynamic val) {
    this._question = val;
  }

  setAnswer(String? val) {
    this._answer = val;
  }

  toBody() {
    return EmailCreateBody(
      lastname: this._lastname,
      firstname: this._firstname,
      id: this._id,
      password: this._password,
      nation: this._nation,
      question: this._question,
      answer: this._answer,
    );
  }

  String? get answer => _answer;

  String? get question => _question;

  String? get nation => _nation;

  String? get password => _password;

  String? get id => _id;

  String? get firstname => _firstname;

  String? get lastname => _lastname;
}
