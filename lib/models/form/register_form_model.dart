import 'package:kins_healthcare/services/user/model/register_model.dart';

/*
 * 작성일 : 2021-02-26
 * 작성자 : JC
 * 화면명 : HR_0001
 * 경로 :
 * 클래스 : RegisterFormModel
 * 설명 : 회원가입 폼 모델
 */

/// 회원가입 폼 모델
///
/// - 화면명 : HR_0001 \
/// - 작성자 : JC \
/// - 작성일 : 2021-02-26
///
/// {@category FormModel}
class RegisterFormModel {
  /// 이메일
  String? _id;

  /// 비밀번호
  String? _password;

  /// 국가
  String? _nation;

  /// 성
  String? _lastname;

  /// 이름
  String? _firstname;

  /// 생년월일
  String? _birth;

  /// 성별
  String? _gender;

  /// 의료종사자 (Y/N)
  String? _mediYn;

  /// 전화번호
  String? _phone;

  /// 2021.04.09 Andy 추가
  String? _userFamilyName;

  /// 2021.04.09 Andy 추가
  String? _userName;

  setId(String? val) {
    this._id = val;
  }

  setPassword(String? val) {
    this._password = val?.trim();
  }

  setNation(dynamic val) {
    this._nation = val;
  }

  setLastname(String? val) {
    this._lastname = val?.trim();
  }

  setFirstname(String? val) {
    this._firstname = val?.trim();
  }

  setBirth(String? val) {
    this._birth = val?.trim();
  }

  setGender(String? val) {
    this._gender = val;
  }

  setMediYn(String? val) {
    this._mediYn = val;
  }

  setPhone(String? val) {
    this._phone = val?.trim();
  }

  setUserFamilyName(String? val) {
    this._userFamilyName = val?.trim();
  }

  setUserName(String? val) {
    this._userName = val?.trim();
  }

  String? get id => _id;

  String? get password => _password;

  String? get nation => _nation;

  String? get lastname => _lastname;

  String? get firstname => _firstname;

  String? get birth => _birth;

  String? get gender => _gender;

  String? get mediYn => _mediYn;

  String? get phone => _phone;

  String? get userFamilyName => _userFamilyName;

  String? get userName => _userName;

  toBody() {
    return RegisterBody(
      id: this._id,
      password: this._password,
      nation: this._nation,
      lastname: this._lastname,
      firstname: this._firstname,
      birth: this._birth,
      gender: this._gender,
      phone: this._phone,
      mediYn: this._mediYn,
    );
  }

  toChildBody() {
    //2021.04.09 Andy 추가
    return RegisterChildBody(
      id: this._id,
      password: this._password,
      nation: this._nation,
      firstname: this._firstname,
      lastname: this._lastname,
      birth: this._birth,
      gender: this._gender,
      phone: this._phone,
    );
  }

  @override
  String toString() {
    return {
      'id': this._id,
      'password': this._password,
      'nation': this._nation,
      'lastname': this._lastname,
      'firstname': this._firstname,
      'birth': this._birth,
      'gender': this._gender,
      'phone': this._phone,
      'mediYn': this._mediYn,
    }.toString();
  }
}
