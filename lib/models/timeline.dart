import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'timeline.g.dart';

@JsonSerializable()
class TimelineModel extends BaseRetrofitModel {
  final String? timelineId;
  final int? userNo;
  final String? profileImg;
  final String? userName;
  final String? userPhone;
  final String? userRelCd;
  final String? message;
  final String? timelineType;
  final String? processType;
  final String? processYn;
  final String? userRelName;
  final String? lastAmdTime;

  TimelineModel({
    this.timelineId,
    this.userNo,
    this.profileImg,
    this.userName,
    this.userPhone,
    this.userRelCd,
    this.message,
    this.timelineType,
    this.processType,
    this.processYn,
    this.userRelName,
    this.lastAmdTime,
  });

  factory TimelineModel.fromJson(Object? json) =>
      _$TimelineModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$TimelineModelToJson(this);
}
