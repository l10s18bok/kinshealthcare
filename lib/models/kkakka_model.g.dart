// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'kkakka_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

KkakkaModel _$KkakkaModelFromJson(Map<String, dynamic> json) {
  return KkakkaModel(
    kkakkaBalance: json['kkakkaBalance'] as int?,
    kkakkaId: json['kkakkaId'] as int?,
    kkakkaImage: json['kkakkaImage'] as String?,
    kkakkaLimit: json['kkakkaLimit'] as int?,
    kkakkaMessage: json['kkakkaMessage'] as String?,
    kkakkaPrice: json['kkakkaPrice'] as int?,
    endDate: json['endDate'] == null
        ? null
        : DateTime.parse(json['endDate'] as String),
    profileImgOpponent: json['profileImgOpponent'] as String?,
    startDate: json['startDate'] == null
        ? null
        : DateTime.parse(json['startDate'] as String),
    userNameOpponent: json['userNameOpponent'] as String?,
    userPhoneOpponent: json['userPhoneOpponent'] as String?,
    userRelCdMe: json['userRelCdMe'] as String?,
    userRelCdOpponent: json['userRelCdOpponent'] as String?,
    issueDate: json['issueDate'] == null
        ? null
        : DateTime.parse(json['issueDate'] as String),
    authYn: json['authYn'] as String?,
    likeYn: json['likeYn'] as int?,
    likeCount: json['likeCount'] as int?,
    firstLikeUserName: json['firstLikeUserName'] as String?,
    firstRepl: json['firstRepl'] as String?,
    firstReplUserName: json['firstReplUserName'] as String?,
    firstReplImg: json['firstReplImg'] as String?,
    kkakkaStatus: json['kkakkaStatus'] as String?,
    userStatusOpponent: json['userStatusOpponent'] as String?,
    kkakkaType: json['kkakkaType'] as String?,
  )..isContentOpen = json['isContentOpen'] as bool?;
}

Map<String, dynamic> _$KkakkaModelToJson(KkakkaModel instance) =>
    <String, dynamic>{
      'kkakkaBalance': instance.kkakkaBalance,
      'kkakkaId': instance.kkakkaId,
      'kkakkaImage': instance.kkakkaImage,
      'kkakkaLimit': instance.kkakkaLimit,
      'kkakkaMessage': instance.kkakkaMessage,
      'kkakkaPrice': instance.kkakkaPrice,
      'endDate': instance.endDate?.toIso8601String(),
      'profileImgOpponent': instance.profileImgOpponent,
      'startDate': instance.startDate?.toIso8601String(),
      'userNameOpponent': instance.userNameOpponent,
      'userPhoneOpponent': instance.userPhoneOpponent,
      'userRelCdMe': instance.userRelCdMe,
      'userRelCdOpponent': instance.userRelCdOpponent,
      'issueDate': instance.issueDate?.toIso8601String(),
      'authYn': instance.authYn,
      'likeYn': instance.likeYn,
      'likeCount': instance.likeCount,
      'firstLikeUserName': instance.firstLikeUserName,
      'firstRepl': instance.firstRepl,
      'firstReplUserName': instance.firstReplUserName,
      'firstReplImg': instance.firstReplImg,
      'isContentOpen': instance.isContentOpen,
      'userStatusOpponent': instance.userStatusOpponent,
      'kkakkaType': instance.kkakkaType,
      'kkakkaStatus': instance.kkakkaStatus,
    };
