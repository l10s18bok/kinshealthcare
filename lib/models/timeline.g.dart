// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timeline.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TimelineModel _$TimelineModelFromJson(Map<String, dynamic> json) {
  return TimelineModel(
    timelineId: json['timelineId'] as String?,
    userNo: json['userNo'] as int?,
    profileImg: json['profileImg'] as String?,
    userName: json['userName'] as String?,
    userPhone: json['userPhone'] as String?,
    userRelCd: json['userRelCd'] as String?,
    message: json['message'] as String?,
    timelineType: json['timelineType'] as String?,
    processType: json['processType'] as String?,
    processYn: json['processYn'] as String?,
    userRelName: json['userRelName'] as String?,
    lastAmdTime: json['lastAmdTime'] as String?,
  );
}

Map<String, dynamic> _$TimelineModelToJson(TimelineModel instance) =>
    <String, dynamic>{
      'timelineId': instance.timelineId,
      'userNo': instance.userNo,
      'profileImg': instance.profileImg,
      'userName': instance.userName,
      'userPhone': instance.userPhone,
      'userRelCd': instance.userRelCd,
      'message': instance.message,
      'timelineType': instance.timelineType,
      'processType': instance.processType,
      'processYn': instance.processYn,
      'userRelName': instance.userRelName,
      'lastAmdTime': instance.lastAmdTime,
    };
