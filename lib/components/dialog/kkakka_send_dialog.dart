import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/box_grid_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-04-13
 작성자 : Mark,
 화면명 : ,
 경로 : ,
 클래스 : KkakkaSendDialog,
 설명 : 까까 제안하기 실행시에 보여주는 Dialog
*/

// ignore: must_be_immutable
class KkakkaSendDialog extends StatelessWidget {
  int price, limit, whichKkaka; // 2021.04.26 Andy 추가  => whichKkaka = 1. 보낼까까 확인, 2, 기부까까 확인, 3. 타임라인까까 확인
  String? imagePath;
  String name;
  String nickName, phoneNumber;
  String? firstAmount, secoundAmount; // 2021.04.20 Andy 추가
  String title, btnTitle;

  BoxGridModel category;
  DateTime startDate, endDate;
  late BuildContext context;

  KkakkaSendDialog({
    required this.name,
    required this.nickName,
    required this.phoneNumber,
    required this.category,
    required this.startDate,
    required this.endDate,
    required this.price,
    required this.whichKkaka,
    this.limit = 0,
    this.title = '제안받은 까까 확인', // 2021.04.20 Andy 추가
    this.btnTitle = '소개 요청하기', // 2021.04.20 Andy 추가
    this.firstAmount, // 2021.04.20 Andy 추가
    this.secoundAmount, // 2021.04.20 Andy 추가
    this.imagePath,
  });

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return ClipPath(
      clipper: DolDurmaClipper(top: 63, holeRadius: 16),
      child: Container(
        width: 320,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: renderMain(),
            ),
            renderRowLine(),
            renderBtnRow(),
          ],
        ),
      ),
    );
  }

  Widget renderMain() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ...renderTitle(),
        ...renderSendPerson(),
        ...renderPriceRow(),
        renderMethodPayment(), // 2021.04.21 Andy 추가
      ],
    );
  }

  List<Widget> renderTitle() {
    return [
      SizedBox(height: 16),
      Align(
        alignment: Alignment.center,
        child: Text(
          title, // 2021.04.20 Andy 수정
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize15,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
      ),
      SizedBox(height: 14),
      Container(height: 2, color: Color(0xFF4042AB)),
    ];
  }

  List<Widget> renderSendPerson() {
    return [
      SizedBox(height: 14),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'To.',
            style: TextStyle(
              fontSize: ThemeFactory.of(context).theme.fontSize15,
              color: kinsGray6A,
              fontWeight: ThemeFactory.of(context).theme.heavyFontWeight,
            ),
          ),
          renderPersonTile(),
        ],
      ),
      renderRowLine(),
    ];
  }

  // 2021.04.20 Andy 추가
  renderMethodPayment() {
    if (whichKkaka == 2)
      return Column(
        children: [
          ...renderRow('결제수단1', firstAmount!),
          ...renderRow('결제수단2', secoundAmount!),
        ],
      );

    if (whichKkaka == 3)
      return Column(
        children: [
          ...renderCategoryRow(),
          ...renderRow('1회 사용한도', limit != 0 ? '${NumberFormat("#,###").format(limit)}원' : '없음'),
        ],
      );

    if (firstAmount == null) return Container();
    return Column(
      children: [
        ...renderCategoryRow(),
        ...renderRow('1회 사용한도', limit != 0 ? '${NumberFormat("#,###").format(limit)}원' : '없음'),
        ...renderRow('결제수단1', firstAmount!),
        ...renderRow('결제수단2', secoundAmount!),
      ],
    );
  }

  Widget renderAvatar() {
    return Stack(
      children: [
        CircleNetworkImage(imageSize: 46, path: imagePath),
        //Positioned(left: 0, top: 0, child: renderCallIcon(false)),
      ],
    );
  }

  Widget renderCallIcon(bool isOnContacts) {
    final path = isOnContacts ? 'assets/svgs/ic/ic_call_contactpage_on.svg' : 'assets/svgs/ic/ic_call_contactpage_off.svg';

    return SvgPicture.asset(path, width: 15, height: 15);
  }

  Widget renderPersonTile() {
    String subName = '';
    if (nickName.length > 0) subName = '($nickName)';
    return ListTile(
      contentPadding: const EdgeInsets.all(0.0),
      leading: renderAvatar(),
      title: RichText(
        text: TextSpan(
          text: name,
          children: [
            TextSpan(
              text: subName,
              style: TextStyle(
                color: Color(0xFF4042AB),
                fontSize: ThemeFactory.of(context).theme.fontSize12,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
          style: TextStyle(
            color: Color(0xFF363636),
            fontSize: ThemeFactory.of(context).theme.fontSize14,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      subtitle: Text(
        phoneNumber,
        style: TextStyle(
          color: Color(0xFF535353),
          fontSize: ThemeFactory.of(context).theme.fontSize13,
        ),
      ),
    );
  }

  List<Widget> renderPriceRow() {
    return [
      SizedBox(height: 15),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '금액',
            style: TextStyle(
              fontSize: ThemeFactory.of(context).theme.fontSize14,
              fontWeight: FontWeight.bold,
              color: Color(0xFF6A6A6A),
            ),
          ),
          renderPriceContent(),
        ],
      ),
      SizedBox(height: 15),
      renderRowLine(),
    ];
  }

  Widget renderPriceContent() {
    final start = DateFormat('yyyy.MM.dd').format(startDate);
    final end = DateFormat('yyyy.MM.dd').format(endDate);
    final timeString = '$start ~ $end';

    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Row(
          children: [
            Text(
              NumberFormat("#,###").format(price),
              style: TextStyle(
                fontSize: ThemeFactory.of(context).theme.fontSize18,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            SizedBox(width: 2),
            Text(
              '원',
              style: TextStyle(
                fontSize: ThemeFactory.of(context).theme.fontSize15,
                color: Colors.black,
              ),
            ),
          ],
        ),
        SizedBox(height: 6),
        Text(
          timeString,
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize11,
            color: Color(0xFF6A6A6A),
          ),
        ),
      ],
    );
  }

  List<Widget> renderCategoryRow() {
    return [
      SizedBox(height: 15),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            '카테고리',
            style: TextStyle(
              fontSize: ThemeFactory.of(context).theme.fontSize14,
              fontWeight: FontWeight.bold,
              color: Color(0xFF6A6A6A),
            ),
          ),
          Spacer(),
          SvgPicture.asset(category.imagePath ?? ''),
          Text(
            category.title,
            style: TextStyle(
              fontSize: ThemeFactory.of(context).theme.fontSize15,
              color: Color(0xFF4042AB),
            ),
          ),
        ],
      ),
      SizedBox(height: 15),
      renderRowLine(),
    ];
  }

  List<Widget> renderRow(String title, String content) {
    final theme = ThemeFactory.of(context).theme;

    return [
      SizedBox(height: 15),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: TextStyle(
              fontSize: theme.fontSize14,
              fontWeight: FontWeight.bold,
              color: Color(0xFF6A6A6A),
            ),
          ),
          Text(
            content,
            style: TextStyle(
              fontSize: theme.fontSize15,
              fontWeight: FontWeight.w400,
              color: Colors.black,
            ),
          ),
        ],
      ),
      SizedBox(height: 15),
    ];
  }

  Widget renderRowLine() {
    return Container(height: 1, color: Color(0xFFE7E7E7));
  }

  Widget renderBtnRow() {
    return Container(
      height: 50,
      alignment: Alignment.center,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: InkWell(
              onTap: () => Get.back(result: false),
              child: Text(
                '닫기',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: ThemeFactory.of(context).theme.fontSize14,
                  color: Color(0xFF6A6A6A),
                ),
              ),
            ),
          ),
          Expanded(child: renderSubmitBtn()),
        ],
      ),
    );
  }

  Widget renderSubmitBtn() {
    Color textColor = kinsBlue40;
    if (btnTitle == '소개 요청하기') textColor = kinsOrangeF2;

    return InkWell(
      onTap: () => Get.back(result: true),
      child: Text(
        btnTitle, // 2021.04.20 Andy 수정
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: ThemeFactory.of(context).theme.fontSize14,
          fontWeight: FontWeight.bold,
          color: textColor,
        ),
      ),
    );
  }
}

/// For Dialog Box
class DolDurmaClipper extends CustomClipper<Path> {
  DolDurmaClipper({required this.top, required this.holeRadius});

  final double top;
  final double holeRadius;

  @override
  Path getClip(Size size) {
    final path = Path()
      ..moveTo(0, 0)
      ..lineTo(size.width, 0)
      ..lineTo(size.width, top - holeRadius)
      ..arcToPoint(
        Offset(size.width, top),
        clockwise: false,
        radius: Radius.circular(1),
      )
      ..lineTo(size.width, size.height)
      ..lineTo(0, size.height)
      ..lineTo(0, top)
      ..arcToPoint(
        Offset(0.0, top - holeRadius),
        clockwise: false,
        radius: Radius.circular(1),
      );

    path.lineTo(0, size.height);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(DolDurmaClipper oldClipper) => true;
}
