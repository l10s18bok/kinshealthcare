import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class AuthBottomSheet extends StatefulWidget {
  final String title;
  final String? subTitle;
  final bool renderDecoration;

  AuthBottomSheet({
    required this.title,
    this.subTitle,
    this.renderDecoration = false,
  });

  @override
  _AuthBottomSheetState createState() => _AuthBottomSheetState();
}

class _AuthBottomSheetState extends State<AuthBottomSheet> {
  renderButton() {
    return PrimaryButton(
      label: '확인',
      onTap: () {
        Get.back();
      },
    );
  }

  renderSubtitle() {
    final theme = ThemeFactory.of(context).theme;

    final subTitleStyle = TextStyle(
      color: Color(0xFF161616),
      fontSize: theme.fontSize14,
      fontWeight: theme.primaryFontWeight,
    );

    if (widget.subTitle == null) {
      return Container();
    } else {
      return Column(
        children: [
          Container(height: 10),
          Text(
            widget.subTitle!,
            style: subTitleStyle,
          ),
        ],
      );
    }
  }

  renderText() {
    final theme = ThemeFactory.of(context).theme;

    final titleStyle = TextStyle(
      fontSize: theme.fontSize17,
      color: Color(0xFF161616),
      fontWeight: theme.heavyFontWeight,
    );

    return Column(
      children: [
        Text(
          widget.title,
          style: titleStyle,
          textAlign: TextAlign.center,
        ),
        renderSubtitle(),
      ],
    );
  }

  renderDecoration() {
    return SvgPicture.asset(
      'assets/svgs/img/bg_popup.svg',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(26.0),
          topLeft: Radius.circular(26.0),
        ),
      ),
      child: SafeArea(
        bottom: true,
        child: Padding(
          padding: EdgeInsets.only(
            left: 16.0,
            right: 16.0,
            bottom: 16.0,
          ),
          child: Stack(
            children: [
              renderDecoration(),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(height: 48),
                  renderText(),
                  Container(height: 48),
                  renderButton(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
