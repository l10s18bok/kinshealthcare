import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class NewPaymentMethodBottomSheet extends StatefulWidget {
  @override
  _NewPaymentMethodBottomSheetState createState() =>
      _NewPaymentMethodBottomSheetState();
}

class _NewPaymentMethodBottomSheetState
    extends State<NewPaymentMethodBottomSheet> {
  renderButton({
    required String iconName,
    required String label,
    GestureTapCallback? onTap,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return InkWell(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            6.0,
          ),
          border: Border.all(
            color: Color(0xFFD7D8EE),
            width: 2.0,
          ),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 11.0, horizontal: 20.0),
          child: Row(
            children: [
              Expanded(
                child: Row(
                  children: [
                    SvgPicture.asset(
                      'assets/svgs/ic/$iconName.svg',
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Text(
                  label,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: theme.fontSize16,
                    color: Color(0xFF545454),
                  ),
                ),
              ),
              Spacer(),
            ],
          ),
        ),
      ),
      onTap: onTap,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseBottomSheet(
      body: Column(
        children: [
          renderButton(
            iconName: 'ic_card_paymentpopup',
            label: '카드 등록',
            onTap: () {
              //Get.offNamed('/payment/methods/register/bank');
              Get.back(result: 'CARD');
            },
          ),
          Container(height: 11.0),
          renderButton(
            iconName: 'ic_bank_paymentpopup',
            label: '계좌 등록',
            onTap: () {
              Get.back(result : 'ACCOUNT');
              //Get.offNamed('/payment/methods/register/account_reg_pageList');
            },
          ),
          Container(height: 40.0),
          PrimaryButton(
            label: '확인',
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
