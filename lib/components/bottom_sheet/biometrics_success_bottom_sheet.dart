import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/// 생체/지문인증 성공시 나오는 BottomSheet
///
/// [피그마](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A6700)
///
/// - 작성자 : JC
/// - 작성일 : 2021-05-01
///
/// {@category ComponentBottomSheet}
class BiometricsSuccessBottomSheet extends StatefulWidget {
  @override
  _BiometricsSuccessBottomSheetState createState() =>
      _BiometricsSuccessBottomSheetState();
}

class _BiometricsSuccessBottomSheetState
    extends State<BiometricsSuccessBottomSheet> {
  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return BaseBottomSheet(
      body: Column(
        children: [
          Text(
            '지문 등록이\n완료되었습니다 :)',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: theme.fontSize17,
            ),
          ),
          Container(height: 50.0),
          PrimaryButton(
            label: '확인',
            onTap: () {
              Get.back();
            },
          ),
        ],
      ),
    );
  }
}
