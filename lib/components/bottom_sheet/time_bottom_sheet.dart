import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-02-01
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : TimeBottomSheet,
 설명 : 시간 시간 종료 시간을 선택하기 위한 Bottom sheet
*/

class TimeBottomSheet extends StatefulWidget {
  TimeBottomSheet({
    Key? key,
    required this.title,
    this.isAmPm = false,
  }) : super(key: key);
  final String title;
  final bool isAmPm;

  @override
  TimeBottomSheetState createState() => TimeBottomSheetState();
}

class TimeBottomSheetState extends State<TimeBottomSheet> {
  FixedExtentScrollController _amPmScrollController = FixedExtentScrollController();
  FixedExtentScrollController _hourScrollController = FixedExtentScrollController();
  FixedExtentScrollController _minuteScrollController = FixedExtentScrollController();
  int selectAmPm = 0, selectHour = 0, selectMinute = 0;
  final List<String> amPmList = ['AM', 'PM'];
  final List<String> hourList = [];
  final List<String> minuteList = [];

  @override
  void initState() {
    super.initState();

    if (widget.isAmPm)
      _setAmPmTime();
    else
      _setTime();
  }

  _setTime() {
    for (int i = 0; i < 60; ++i) {
      if (i < 10) {
        minuteList.add('0$i');
        if (i < 9)
          hourList.add('0${i + 1}');
        else
          hourList.add('${i + 1}');
      } else {
        minuteList.add('$i');
        if (i < 24) hourList.add('${i + 1}');
      }
    }
  }

  _setAmPmTime() {
    for (int i = 0; i < 60; ++i) {
      if (i < 10) {
        minuteList.add('0$i');
        if (i < 9)
          hourList.add('0${i + 1}');
        else
          hourList.add('${i + 1}');
      } else {
        minuteList.add('$i');
        if (i < 12) hourList.add('${i + 1}');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return renderBottomSheetLayout(
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 20,
            alignment: Alignment.center,
            child: Text(
              widget.title,
              style: TextStyle(
                color: kinsBlack35,
                fontWeight: FontWeight.w500,
                fontSize: ThemeFactory.of(context).theme.fontSize15,
              ),
            ),
          ),
          SizedBox(height: 10),
          Expanded(child: renderMainRow()),
          SizedBox(height: 10),
          SizedBox(
            width: double.infinity,
            height: 55,
            child: PrimaryButton(
              label: '적용',
              onTap: _btnOnTap,
            ),
          ),
        ],
      ),
    );
  }

  Widget renderBottomSheetLayout({required Widget child}) {
    return Container(
      height: 270,
      width: double.infinity,
      color: Colors.transparent,
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(26.0),
          topRight: Radius.circular(26.0),
        ),
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 30),
          child: child,
        ),
      ),
    );
  }

  Widget renderMainRow() {
    return Row(
      children: [
        widget.isAmPm
            ? Expanded(child: renderStack(amPmList, _amPmScrollController))
            : Container(),
        widget.isAmPm ? SizedBox(width: 4) : Container(),
        Expanded(child: renderStack(hourList, _hourScrollController)),
        SizedBox(width: 4),
        Expanded(child: renderStack(minuteList, _minuteScrollController)),
      ],
    );
  }

  Widget renderStack(
    List<String> items,
    FixedExtentScrollController controller,
  ) {
    final itemExtent = 38.0;

    return Stack(
      children: [
        Positioned.fill(
          child: renderListView(itemExtent, items, controller),
        ),
        Positioned.fill(
          child: Container(
            width: double.infinity,
            height: double.infinity,
            alignment: Alignment.center,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(width: double.infinity, height: 1, color: kinsBlue40),
                SizedBox(height: itemExtent),
                Container(width: double.infinity, height: 1, color: kinsBlue40),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget renderListView(
    double itemExtent,
    List<String> items,
    FixedExtentScrollController controller,
  ) {
    return NotificationListener<ScrollNotification>(
      child: ListWheelScrollView(
        controller: controller,
        itemExtent: itemExtent,
        useMagnifier: true,
        children: items.map((item) => renderListItem(item)).toList(),
        onSelectedItemChanged: (int index) => setNowIndex(items, index),
      ),
      onNotification: (scrollNotification) {
        if (scrollNotification is ScrollEndNotification) {
          int nowIndex = getNowIndex(items);

          WidgetsBinding.instance!.addPostFrameCallback((_) {
            controller.animateToItem(
              nowIndex,
              duration: Duration(milliseconds: 200),
              curve: Curves.linear,
            );
          });
        }

        return true;
      },
    );
  }

  Widget renderListItem(String title) {
    return Center(
      child: Text(
        title,
        style: TextStyle(
          fontSize: ThemeFactory.of(context).theme.fontSize15,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }

  _btnOnTap() {
    final amPm = widget.isAmPm ? amPmList[selectAmPm] : '';
    final hour = hourList[selectHour];
    final minute = minuteList[selectMinute];

    final title = '$amPm $hour:$minute';
    Get.back(result: title);
  }

  int getNowIndex(List<String> items) {
    if (items == amPmList) return selectAmPm;
    if (items == hourList) return selectHour;
    return selectMinute;
  }

  setNowIndex(List<String> items, int index) {
    if (items == amPmList) {
      selectAmPm = index;
    } else if (items == hourList) {
      selectHour = index;
    } else if (items == minuteList) {
      selectMinute = index;
    }
  }
}
