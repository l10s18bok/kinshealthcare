import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import '../../utils/resource.dart';

/*
 작성일 : 2021-02-01
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : BtnBottomSheet,
 설명 : Title Button Bottom Sheet
 Bottom Sheet Title 강조되는 굵은 글자를 넣기 위한 Bottom Sheet
*/

// ignore: must_be_immutable
class BtnBottomSheet extends StatelessWidget {
  BtnBottomSheet({
    required this.title,
    this.boldTitle,
    this.isOneBtn = false,
  });

  final String title;
  final String? boldTitle;
  bool isOneBtn;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 248,
      width: double.infinity,
      color: Colors.transparent,
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(26.0),
          topRight: Radius.circular(26.0),
        ),
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(child: Center(child: titleText(context))),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  renderCancelBtn(),
                  Expanded(
                    child: PrimaryButton(
                      label: '확인',
                      onTap: () {
                        Get.back(result: 'OK');
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget titleText(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        style: TextStyle(
          color: kinsBlack,
          fontSize: ThemeFactory.of(context).theme.fontSize17,
        ),
        children: <TextSpan>[
          renderBoldText(),
          TextSpan(text: title),
        ],
      ),
    );
  }

  TextSpan renderBoldText() {
    return TextSpan(text: boldTitle ?? '', style: TextStyle(fontWeight: FontWeight.w700));
  }

  Widget renderCancelBtn() {
    if (isOneBtn == true) return Container();

    return Expanded(
      child: Row(
        children: [
          Expanded(
            child: PrimaryButton(
              label: '취소',
              onTap: () {
                Get.back(result: 'CANCEL');
              },
            ),
          ),
          SizedBox(width: 12),
        ],
      ),
    );
  }
}
