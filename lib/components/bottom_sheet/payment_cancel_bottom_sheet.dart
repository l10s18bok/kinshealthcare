import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';

class PaymentCancelBottomSheet extends StatefulWidget {
  @override
  _PaymentCancelBottomSheetState createState() =>
      _PaymentCancelBottomSheetState();
}

class _PaymentCancelBottomSheetState extends State<PaymentCancelBottomSheet> {
  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return BaseBottomSheet(
      body: Column(
        children: [
          Text(
            '모든 과정이 취소됩니다.',
            style: TextStyle(),
          ),
          Container(
            height: 16.0,
          ),
          Text(
            '결제를 취소하시겠어요?',
            style: TextStyle(
              fontWeight: theme.heavyFontWeight,
              fontSize: theme.fontSize17,
            ),
          ),
          Container(
            height: 24.0,
          ),
          Row(
            children: [
              Expanded(
                child: PrimaryButton(
                  bgColor: Colors.grey[300],
                  txtColor: Colors.grey,
                  label: '취소',
                  onTap: () {
                    Get.back();
                  },
                ),
              ),
              Container(width: 16.0),
              Expanded(
                child: PrimaryButton(
                  label: '확인',
                  onTap: () async {
                    try {
                      await Get.find<PaymentController>().cancelPayment();
                      await Get.find<PaymentController>().checkPaymentStatus();

                      await Get.toNamed('/payment/fail');
                    } on DioError catch (e) {
                      Get.back();
                      SnackbarUtils().parseDioErrorMessage(
                        error: e,
                        title: '결제 취소 에러',
                      );
                    }
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
