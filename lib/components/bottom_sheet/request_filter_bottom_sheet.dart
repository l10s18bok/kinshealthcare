import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/button/check_button.dart';
import 'package:kins_healthcare/components/drop_down/default_drop_down_form_field.dart';
import 'package:kins_healthcare/components/text_field/text_field.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class RequestFilterBottomSheet extends StatefulWidget {
  @override
  _RequestFilterBottomSheetState createState() =>
      _RequestFilterBottomSheetState();
}

class _RequestFilterBottomSheetState extends State<RequestFilterBottomSheet> {
  renderTitle() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      children: [
        Spacer(
          flex: 1,
        ),
        Expanded(
          child: Text(
            '필터',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: theme.fontSize15,
              fontWeight: theme.mediumFontWeight,
            ),
          ),
        ),
        Expanded(
          child: Align(
            alignment: Alignment.centerRight,
            child: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Icon(
                Icons.close,
              ),
            ),
          ),
        ),
      ],
    );
  }

  renderSort() {
    return Row(
      children: [
        CheckButton(
          label: '남은기간 긴 순',
          onTap: () {},
        ),
        Container(width: 20.0),
        CheckButton(label: '남은기간 짧은 순', isActive: true, onTap: () {}),
      ],
    );
  }

  renderFilters() {
    return Row(
      children: [
        CheckButton(
          label: '전체',
          onTap: () {},
        ),
        Container(width: 20.0),
        CheckButton(
          label: '유효한 것만',
          isActive: true,
          onTap: () {},
        ),
      ],
    );
  }

  renderDivider() {
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 1.0,
            color: Color(0xFFE6E6E6),
          ),
        ),
      ],
    );
  }

  renderCategory() {
    return DefaultDropDownFormField<String>(
      label: '사용 카테고리',
      onChanged: (val) {},
      items: [
        DropdownMenuItem(
          child: Text('전체'),
        ),
      ],
      validator: (val) {
        return null;
      },
      onSaved: (val) {},
    );
  }

  renderAmount() {
    return UnderlineTextFormField(
      labelText: '사용 가능금액',
      hintText: '0',
      validator: (val) {
        return null;
      },
      onSaved: (val) {},
    );
  }

  renderDateRange() {
    return UnderlineTextFormField(
      labelText: '유효기간',
      hintText: '2020.07.12 ~ 2020.07.12',
      validator: (val) {
        return null;
      },
      onSaved: (val) {},
    );
  }

  renderConfirmButton() {
    return PrimaryButton(label: '확인', onTap: () {});
  }

  @override
  Widget build(BuildContext context) {
    return BaseBottomSheet(
      body: Column(
        children: [
          renderTitle(),
          Container(height: 34.0),
          renderSort(),
          renderDivider(),
          renderFilters(),
          Container(height: 14.0),
          renderCategory(),
          Container(height: 28.0),
          renderAmount(),
          Container(height: 28.0),
          renderDateRange(),
          Container(height: 28.0),
          renderConfirmButton(),
        ],
      ),
    );
  }
}
