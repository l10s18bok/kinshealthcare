import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/avatar/payment_bottom_sheet_avatar.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/card/quote_card.dart';
import 'package:kins_healthcare/controllers/force_controller.dart';
import 'package:kins_healthcare/services/force/model/event_detail_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

class PaymentOverlayBottomSheet extends StatefulWidget {
  final EventDetailResponse event;

  PaymentOverlayBottomSheet({
    required this.event,
  });

  @override
  _PaymentOverlayBottomSheetState createState() =>
      _PaymentOverlayBottomSheetState();
}

class _PaymentOverlayBottomSheetState extends State<PaymentOverlayBottomSheet> {
  renderName() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          widget.event.relation.userNameOpponent,
          style: TextStyle(
            fontSize: theme.fontSize15,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
      ],
    );
  }

  renderLabelText(String key) {
    final theme = ThemeFactory.of(context).theme;

    return Text(
      key,
      style: TextStyle(
        fontSize: theme.fontSize14,
        fontWeight: theme.heavyFontWeight,
        color: theme.primaryBlueColor,
      ),
    );
  }

  renderChip() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      decoration: BoxDecoration(
        color: Color(0xFFFDD84F),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 3.0, horizontal: 11.0),
        child: Text(
          '기획필요',
          style: TextStyle(
            color: Colors.white,
            fontSize: theme.fontSize14,
          ),
        ),
      ),
    );
  }

  renderTexts() {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            renderLabelText('구분'),
            renderChip(),
          ],
        ),
        Container(height: 8.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            renderLabelText('상호명'),
            Text(
              widget.event.info.storeNm!,
              style: TextStyle(
                fontSize: theme.fontSize15,
              ),
            ),
          ],
        ),
        Container(height: 8.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            renderLabelText('요청 금액'),
            Text(
              TextUtils().numberToLocalCurrency(
                    amount: widget.event.info.amount!.toDouble(),
                  ) +
                  '원',
              style: TextStyle(
                fontSize: theme.fontSize20,
                fontWeight: theme.heavyFontWeight,
              ),
            ),
          ],
        ),
      ],
    );
  }

  renderButtons() {
    return Row(
      children: [
        Expanded(
          child: PrimaryButton(
            label: '거절',
            onTap: () async {
              await Get.find<ForceController>().forceEventProcess(
                eventParam: widget.event.event.eventParam,
                isCheck: false,
              );
              await Get.find<ForceController>().forceEventUnder(reset: true);
              Get.back();
            },
            bgColor: Color(0xFFE0E2E6),
            txtColor: Color(0xFF868686),
          ),
        ),
        Container(width: 16.0),
        Expanded(
          child: PrimaryButton(
            label: '수락',
            onTap: () async {
              await Get.find<ForceController>().forceEventProcess(
                eventParam: widget.event.event.eventParam,
                isCheck: true,
              );
              await Get.find<ForceController>().forceEventUnder(reset: true);
              Get.back();
            },
          ),
        ),
      ],
    );
  }

  renderSkipButton() {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      children: [
        Row(
          children: [
            Padding(
              padding: EdgeInsets.only(left: 16.0),
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  Get.back();
                },
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Colors.white,
                        width: 1.0,
                      ),
                    ),
                  ),
                  child: Text(
                    '다음에 보기',
                    style: TextStyle(
                      fontSize: theme.fontSize15,
                      fontWeight: theme.heavyFontWeight,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        BaseBottomSheet(
          body: Column(
            children: [
              Container(height: 14.0),
              renderName(),
              Container(height: 8.0),
              QuoteCard(text: '기획필요'),
              Container(height: 24.0),
              renderTexts(),
              Container(height: 28.0),
              renderButtons(),
            ],
          ),
        ),
        Positioned.fill(
          top: -32.0,
          child: renderSkipButton(),
        ),
        Positioned.fill(
          top: -37.0,
          child: PaymentBottomSheetAvatar(
            imageUrl: widget.event.relation.profileImgOpponent,
          ),
        ),
      ],
    );
  }
}
