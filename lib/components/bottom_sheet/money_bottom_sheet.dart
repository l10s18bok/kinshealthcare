import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/keyboard/money_input_keyboard.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

class MoneyBottomSheetResult {
  final double amount;

  MoneyBottomSheetResult({
    required this.amount,
  });
}

/*
 * 작성일 : 2021-02-27
 * 작성자 : JC
 * 화면명 :
 * 경로 :
 * 클래스 : MoneyBottomSheet
 * 설명 : 돈을 입력할 수 있는 bottom sheet
 */
// TODO 디자인이 아직 Figma 에 나오지 않은 관계로
// 나중에 색상과 사이즈등 재대로 다시 수정 필요
class MoneyBottomSheet extends StatefulWidget {
  @override
  _MoneyBottomSheetState createState() => _MoneyBottomSheetState();
}

class _MoneyBottomSheetState extends State<MoneyBottomSheet> {
  String value = '';

  double valueToDouble() {
    if (value.isEmpty) {
      return 0;
    } else {
      return double.parse(value);
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    final TextUtils textUtils = TextUtils();

    return BaseBottomSheet(
      body: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(
                      8.0,
                    ),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            textUtils.numberToLocalCurrency(
                                  amount: valueToDouble(),
                                ) +
                                '원',
                            style: TextStyle(
                              fontSize: theme.fontSize16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(height: 12.0),
          MoneyInputKeyboard(
            onConfirm: () {
              Get.back(
                result: MoneyBottomSheetResult(
                  amount: valueToDouble(),
                ),
              );
            },
            onKeyboardInput: (String value) {
              setState(() {
                this.value = value;
              });
            },
          ),
        ],
      ),
    );
  }
}
