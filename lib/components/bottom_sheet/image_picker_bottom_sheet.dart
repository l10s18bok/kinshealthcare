import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kins_healthcare/utils/utils.dart';

class ImagePickerBottomSheetResult {
  final File file;

  ImagePickerBottomSheetResult({
    required this.file,
  });
}

class ImagePickerBottomSheet extends StatefulWidget {
  final bool processImage;

  ImagePickerBottomSheet({
    required this.processImage,
  });

  @override
  _ImagePickerBottomSheetState createState() => _ImagePickerBottomSheetState();
}

class _ImagePickerBottomSheetState extends State<ImagePickerBottomSheet> {
  pickImage({
    required ImageSource source,
  }) async {
    final ImagePickerUtils imgPicker = ImagePickerUtils.of(context);

    File? file = await imgPicker.pickImage(
      imageSource: source,
      processImage: widget.processImage,
    );

    Get.back(
      result: file == null
          ? null
          : ImagePickerBottomSheetResult(
              file: file,
            ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        bottom: true,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
              onTap: () {
                this.pickImage(source: ImageSource.camera);
              },
              leading: Icon(
                Icons.camera,
              ),
              title: Text('카메라 열기'),
            ),
            ListTile(
              onTap: () {
                this.pickImage(source: ImageSource.gallery);
              },
              leading: Icon(
                Icons.image,
              ),
              title: Text('앨범에서 고르기'),
            ),
          ],
        ),
      ),
    );
  }
}
