import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-02-01
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : WheelBottomSheet,
 설명 : 여러개의 String List 데이터를 선택을 위한 Bottom sheet
*/

class WheelBottomSheet extends StatefulWidget {
  WheelBottomSheet({
    Key? key,
    required this.items,
    required this.title,
  }) : super(key: key);

  final List<String> items;
  final String title;

  @override
  WheelBottomSheetSate createState() => WheelBottomSheetSate();
}

class WheelBottomSheetSate extends State<WheelBottomSheet> {
  FixedExtentScrollController _scrollController = FixedExtentScrollController();
  int nowIndex = 0;

  @override
  Widget build(BuildContext context) {
    return renderBottomSheetLayout(
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: 20,
            alignment: Alignment.center,
            child: Text(
              widget.title,
              style: TextStyle(
                color: kinsBlack35,
                fontWeight: FontWeight.w500,
                fontSize: ThemeFactory.of(context).theme.fontSize15,
              ),
            ),
          ),
          SizedBox(height: 10),
          Expanded(child: renderStack()),
          SizedBox(height: 10),
          SizedBox(
            width: double.infinity,
            height: 55,
            child: PrimaryButton(
              label: '적용',
              onTap: _btnOnTap,
            ),
          ),
        ],
      ),
    );
  }

  Widget renderBottomSheetLayout({required Widget child}) {
    return Container(
      height: 270,
      width: double.infinity,
      color: Colors.transparent,
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(26.0),
          topRight: Radius.circular(26.0),
        ),
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 30),
          child: child,
        ),
      ),
    );
  }

  Widget renderStack() {
    final itemExtent = 38.0;

    return Stack(
      children: [
        Positioned.fill(
          child: renderListView(itemExtent),
        ),
        Positioned.fill(
          child: Container(
            width: double.infinity,
            height: double.infinity,
            alignment: Alignment.center,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(width: double.infinity, height: 1, color: kinsBlue40),
                SizedBox(height: itemExtent),
                Container(width: double.infinity, height: 1, color: kinsBlue40),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget renderListView(double itemExtent) {
    return NotificationListener<ScrollNotification>(
      child: ListWheelScrollView(
        controller: _scrollController,
        itemExtent: itemExtent,
        useMagnifier: true,
        children: widget.items.map((item) => renderListItem(item)).toList(),
        onSelectedItemChanged: (int index) => nowIndex = index,
      ),
      onNotification: _onScrollNotification,
    );
  }

  Widget renderListItem(String title) {
    return Center(
      child: Text(
        title,
        style: TextStyle(
          fontSize: ThemeFactory.of(context).theme.fontSize15,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }

  _btnOnTap() {
    final title = widget.items[nowIndex];
    Get.back(result: title);
  }

  bool _onScrollNotification(ScrollNotification notification) {
    if (notification is ScrollEndNotification == false) return true;

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      _scrollController.animateToItem(
        nowIndex,
        duration: Duration(milliseconds: 200),
        curve: Curves.linear,
      );
    });

    return true;
  }
}
