import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

import '../../utils/resource.dart';

/**
 * @id      HU_1003_D
 * @comment Sub Button Bottom Sheet
 * @author  이명재
 * @version 1.0
 * @since   2021-02-02
 */

// ignore: must_be_immutable
class SubBtnBottomSheet extends StatelessWidget {
  SubBtnBottomSheet({
    required this.title,
    required this.boldTitle,
    required this.blueTitle,
    this.subBtnTitle,
    this.boldTitle02,
  });

  final String title, boldTitle, blueTitle;
  final String? subBtnTitle, boldTitle02;

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      alignment: Alignment.topCenter,
      children: [
        Container(
          height: 281,
          width: double.infinity,
          color: Colors.transparent,
          child: ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(26.0),
              topRight: Radius.circular(26.0),
            ),
            child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.white,
              padding:
                  EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Center(child: renderContentText(context)),
                  ),
                  bottomBtnRow(),
                ],
              ),
            ),
          ),
        ),
/**
 * 변경자: Daniel
 * 수정일: 04/08/2021
 * 내용: 디자인 변경으로 팝콘 사진 지움
    Positioned(
          top: -100,
          child: Image.asset(
            'assets/png/popcon.png',
            fit: BoxFit.cover,
          ),
        )
 */
      ],
    );
  }

  Widget renderContentText(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        boldTitleText(context),
        SizedBox(height: 16),
        Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: ThemeFactory.of(context).theme.fontSize15,
          ),
        ),
      ],
    );
  }

  Widget boldTitleText(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        style: TextStyle(
          color: kinsBlack,
          fontWeight: FontWeight.w700,
          fontSize: ThemeFactory.of(context).theme.fontSize17,
        ),
        children: <TextSpan>[
          TextSpan(text: boldTitle),
          renderBlueText(),
          TextSpan(text: boldTitle02 ?? ''),
        ],
      ),
    );
  }

  TextSpan renderBlueText() {
    return TextSpan(
      text: ' $blueTitle ',
      style: TextStyle(color: kinsBlue40),
    );
  }

  Widget bottomBtnRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        subBtnTitle == null ? Container() : renderSubCancelBtn(),
        Expanded(
          child: PrimaryButton(
            label: '확인',
            onTap: () {
              Get.back(result: 'OK');
            },
          ),
        ),
      ],
    );
  }

  Widget renderSubCancelBtn() {
    return Expanded(
      child: Row(
        children: [
          Expanded(
            child: PrimaryButton(
              label: subBtnTitle ?? '',
              onTap: () {
                Get.back(result: 'CANCEL');
              },
            ),
          ),
          SizedBox(width: 12),
        ],
      ),
    );
  }
}
