import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

import '../../utils/resource.dart';

/**
 * @id      HU_1002_D
 * @comment Sub Two Button Bottom Sheet
 * @author  Daniel
 * @version 2.0
 * @since   2021-04-07
 */

// ignore: must_be_immutable
class SubTwoBtnBottomSheet extends StatelessWidget {
  SubTwoBtnBottomSheet({
    required this.title,
    required this.boldTitle,
    required this.blueTitle,
    required this.bottomLeftBtnRowTitle,
    required this.bottomRightBtnRowTitle,
    this.extraTitle,
    this.extraTitle02,
    this.subBtnTitle,
    this.boldTitle02,
  });

  final String title,
      boldTitle,
      blueTitle,
      bottomLeftBtnRowTitle,
      bottomRightBtnRowTitle;
  final String? subBtnTitle, boldTitle02, extraTitle, extraTitle02;

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      alignment: Alignment.topCenter,
      children: [
        Container(
          height: 281,
          width: double.infinity,
          color: Colors.transparent,
          child: ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(26.0),
              topRight: Radius.circular(26.0),
            ),
            child: Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.white,
              padding:
                  EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Center(child: renderContentText(context)),
                  ),
                  Row(
                    children: [
                      Expanded(child: bottomLeftBtnRow()),
                      Container(width: 12.0),
                      Expanded(child: bottomRightBtnRow()),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget renderContentText(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        boldTitleText(context),
        SizedBox(height: 6),
        Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: ThemeFactory.of(context).theme.fontSize15,
          ),
        ),
        SizedBox(height: 16),
        Text(
          extraTitle ?? '',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: ThemeFactory.of(context).theme.fontSize15,
          ),
        ),
        SizedBox(height: 3),
        Text(
          extraTitle02 ?? '',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: ThemeFactory.of(context).theme.fontSize15,
          ),
        ),
      ],
    );
  }

  Widget boldTitleText(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        style: TextStyle(
          color: kinsBlack,
          fontWeight: FontWeight.w700,
          fontSize: ThemeFactory.of(context).theme.fontSize17,
        ),
        children: <TextSpan>[
          TextSpan(text: boldTitle),
          renderBlueText(),
          TextSpan(text: boldTitle02 ?? ''),
        ],
      ),
    );
  }

  TextSpan renderBlueText() {
    return TextSpan(
      text: ' $blueTitle ',
      style: TextStyle(color: kinsBlue40),
    );
  }

  Widget bottomLeftBtnRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        subBtnTitle == null ? Container() : renderSubCancelBtn(),
        Expanded(
          child: PrimaryButton(
            label: bottomLeftBtnRowTitle,
            bgColor: Color(0xFFE0E2E6),
            txtColor: Color(0xFF868686),
            onTap: () {
              Get.back(result: 'LEFT');
            },
          ),
        ),
      ],
    );
  }

  Widget bottomRightBtnRow() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        subBtnTitle == null ? Container() : renderSubCancelBtn(),
        Expanded(
          child: PrimaryButton(
            label: bottomRightBtnRowTitle,
            onTap: () {
              Get.back(result: 'RIGHT');
            },
          ),
        ),
      ],
    );
  }

  Widget renderSubCancelBtn() {
    return Expanded(
      child: Row(
        children: [
          Expanded(
            child: PrimaryButton(
              label: subBtnTitle ?? '',
              onTap: () {
                Get.back(result: 'CANCEL');
              },
            ),
          ),
          SizedBox(width: 12),
        ],
      ),
    );
  }
}
