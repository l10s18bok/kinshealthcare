import 'package:flutter/material.dart';

class BaseBottomSheet extends StatefulWidget {
  final Widget body;
  final double lrPadding; // 2021/03/25 Andy 추가
  final bool expand;

  BaseBottomSheet({
    required this.body,
    this.expand = false,
    this.lrPadding = 16.0,
  });

  @override
  _BaseBottomSheetState createState() => _BaseBottomSheetState();
}

class _BaseBottomSheetState extends State<BaseBottomSheet> {
  wrapExpand(Widget body) {
    if (widget.expand) {
      return Expanded(
        child: body,
      );
    } else {
      return body;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        wrapExpand(
          Row(
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(26.0),
                      topLeft: Radius.circular(26.0),
                    ),
                  ),
                  child: SafeArea(
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: widget.lrPadding,
                        right: widget.lrPadding,
                        top: 36.0,
                        bottom: 16.0,
                      ),
                      child: widget.body,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
