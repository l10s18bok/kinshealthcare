import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/models/local/local_filter_model.dart';
import 'package:kins_healthcare/screens/temp/calendar_screen.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-02-25
 작성자 : Victor
 화면명 : HH_0002_F
 클래스 : CategoryFilter
 경로 :
 설명 : 까까 필터링용 팝업창
*/

///까까 필터링용 팝업창
///
///local_filter_model.dart 파일과 연관되어 있으므로 해당 파일 내의 enum 수정시 맞춰줘야 함
class CategoryFilter extends StatefulWidget {
  @override
  _CategoryFilterState createState() => _CategoryFilterState();
}

class _CategoryFilterState extends State<CategoryFilter> {
  late LocalFilterModel filterModel;

  //정렬
  String orderValue = '발행순';
  List<String> orderList = [
    '발행순',
    '발행역순',
    '시작일자순',
    '시작일자역순',
    '종료일자순',
    '종료일자역순'
  ];
  //구분 (까까 필터링 조건)
  String divisionValue = '전체';
  List<String> divisionList = ['전체', '요청한', '요청받은', '발행한', '발행받은'];
  //용도 (까까 유형)
  String purposeValue = '전체';
  List<String> purposeList = ['전체', '기념일', '기부', '지역', '의료', '용돈', '셀프', '복지'];
  //상태 (까까 상태)
  // 2021/3/2 기준 활성, 해제만 사용
  String statusValue = '전체';
  List<String> statusList = ['전체', '활성', '해제'];
  //날짜 (전체 or 0000-00-00 ~ 0000-00-00)
  final format = DateFormat('yyyy-MM-dd');
  int divisionIndex = 0;
  int purposeIndex = 0;
  int statusIndex = 0;
  int orderIndex = 0;

  RxString dateSave = '전체'.obs;
  String get dateValue => dateSave.value;
  set dateValue(String value) => dateSave.value = value;

  @override
  void initState() {
    super.initState();

    filterModel = LocalFilterModel();

    filterModel.sortBy = SortType.ID;
    filterModel.isAsc = true;
    filterModel.filterType = KkakkaFilterType.ALL;
    filterModel.type = KkakkaType.ALL;
    filterModel.status = KkakkaStatus.ALL;

    //날짜 null도 가능함. null일경우 전체
    filterModel.fromDate = null;
    filterModel.toDate = null;
  }

  @override
  Widget build(BuildContext context) {
    return BaseBottomSheet(
      expand: true,
      body: ListView(
        children: [
          header(),
          space(),
          orderDropDown(),
          space(),
          divisionDropDown(),
          space(),
          purposeDropDown(),
          space(),
          statusDropDown(),
          space(),
          dateSelect(),
          space(size: 15),
          confirmButton(),
        ],
      ),
    );
  }

  //요청에 의해 기본 일자는 오늘 기준으로 7일 전까지 잡도록 했음
  DateTime get startDate => filterModel.fromDate != null
      ? DateTime.parse(filterModel.fromDate!)
      : DateTime.now().subtract(Duration(days: 7));
  DateTime get endDate => filterModel.toDate != null
      ? DateTime.parse(filterModel.toDate!)
      : DateTime.now();

  Widget orderDropDown() {
    return dropdown('정렬', orderList, orderList[orderIndex], (t) {
      setState(() {
        orderIndex = orderList.indexOf(t!);

        switch (orderIndex) {
          case 0:
            filterModel.sortBy = SortType.ID;
            filterModel.isAsc = true;
            break;
          case 1:
            filterModel.sortBy = SortType.ID;
            filterModel.isAsc = false;
            break;
          case 2:
            filterModel.sortBy = SortType.START_DATE;
            filterModel.isAsc = true;
            break;
          case 3:
            filterModel.sortBy = SortType.START_DATE;
            filterModel.isAsc = false;
            break;
          case 4:
            filterModel.sortBy = SortType.END_DATE;
            filterModel.isAsc = true;
            break;
          case 5:
            filterModel.sortBy = SortType.END_DATE;
            filterModel.isAsc = false;
            break;
        }
      });
    });
  }

  Widget divisionDropDown() {
    return dropdown('구분', divisionList, divisionList[divisionIndex], (t) {
      setState(() {
        divisionIndex = divisionList.indexOf(t!);

        switch (divisionIndex) {
          case 0:
            filterModel.filterType = KkakkaFilterType.ALL;
            break;
          case 1:
            filterModel.filterType = KkakkaFilterType.REQUESTME;
            break;
          case 2:
            filterModel.filterType = KkakkaFilterType.REQUESTOPPONENT;
            break;
          case 3:
            filterModel.filterType = KkakkaFilterType.SENDME;
            break;
          case 4:
            filterModel.filterType = KkakkaFilterType.SENDOPPONENT;
            break;
        }
      });
    });
  }

  Widget purposeDropDown() {
    return dropdown('용도', purposeList, purposeList[purposeIndex], (t) {
      setState(() {
        purposeIndex = purposeList.indexOf(t!);

        switch (purposeIndex) {
          case 0:
            filterModel.type = KkakkaType.ALL;
            break;
          case 1:
            filterModel.type = KkakkaType.ANNIVERSARY;
            break;
          case 2:
            filterModel.type = KkakkaType.DONATE;
            break;
          case 3:
            filterModel.type = KkakkaType.AREA;
            break;
          case 4:
            filterModel.type = KkakkaType.MEDICAL;
            break;
          case 5:
            filterModel.type = KkakkaType.PIN;
            break;
          case 6:
            filterModel.type = KkakkaType.SELF;
            break;
          case 7:
            filterModel.type = KkakkaType.WELFARE;
            break;
          default:
        }
      });
    });
  }

  Widget statusDropDown() {
    return dropdown('상태', statusList, statusList[statusIndex], (t) {
      setState(() {
        statusIndex = statusList.indexOf(t!);

        switch (statusIndex) {
          case 0:
            filterModel.status = KkakkaStatus.ALL;
            break;
          case 1:
            filterModel.status = KkakkaStatus.ACTIVE;
            break;
          case 2:
            filterModel.status = KkakkaStatus.DROP;
            break;
        }
      });
    });
  }

  Widget header() {
    var theme = ThemeFactory.of(context).theme;

    return Row(
      children: [
        Flexible(
          child: Container(
            width: double.infinity,
            alignment: Alignment.center,
            child: Text(
              '필터',
              style: TextStyle(
                fontSize: theme.fontSize20,
                color: Colors.black,
              ),
            ),
          ),
        ),
        InkWell(
          onTap: () {
            Get.back();
          },
          child: Text(
            '╳',
            style: TextStyle(
              fontSize: theme.fontSize16,
              color: Colors.black,
            ),
          ),
        ),
      ],
    );
  }

  Widget space({double size = 10}) {
    return Container(
      width: size,
      height: size,
      color: Colors.transparent,
    );
  }

  Widget dropdown(String title, List<String> itemList, String display,
      ValueChanged<String?> onChanged) {
    var theme = ThemeFactory.of(context).theme;

    return Column(
      children: [
        Container(
          width: double.infinity,
          alignment: Alignment.centerLeft,
          child: Text(
            title,
            style: TextStyle(
              fontSize: theme.fontSize16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          width: double.infinity,
          alignment: Alignment.center,
          child: DropdownButton<String>(
            value: display,
            onChanged: onChanged,
            items: itemList.map<DropdownMenuItem<String>>((value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
            isExpanded: true,
            itemHeight: 65,
            style: TextStyle(
              fontSize: theme.fontSize17,
              color: Colors.black,
            ),
            underline: Container(
              width: double.infinity,
              height: 1,
              color: theme.secondaryGreyTextColor,
            ),
          ),
        ),
      ],
    );
  }

  Widget dateSelect() {
    var theme = ThemeFactory.of(context).theme;

    return Column(
      children: [
        Container(
          width: double.infinity,
          alignment: Alignment.centerLeft,
          child: Text(
            '기준일',
            style: TextStyle(
              fontSize: theme.fontSize16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        space(size: 15),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Flexible(
              child: InkWell(
                onTap: () async {
                  var result = await Get.toNamed('/calendar',
                      preventDuplicates: false,
                      arguments: [startDate, endDate]);
                  if (result is CalendarScreenResult) {
                    filterModel.fromDate = format.format(result.range[0]);
                    filterModel.toDate = format.format(result.range[1]);
                    dateValue =
                        '${filterModel.fromDate} ~ ${filterModel.toDate}';
                  }
                },
                child: Container(
                  width: double.infinity,
                  alignment: Alignment.centerLeft,
                  child: Obx(
                    () => Text(
                      dateValue,
                      style: TextStyle(
                        fontSize: theme.fontSize17,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                filterModel.fromDate = null;
                filterModel.toDate = null;
                dateValue = '전체';
              },
              child: Container(
                alignment: Alignment.center,
                child: Text('전체'),
              ),
            )
          ],
        ),
        space(size: 15),
        Container(
          width: double.infinity,
          height: 1,
          color: theme.secondaryGreyTextColor,
        ),
      ],
    );
  }

  Widget confirmButton() {
    return PrimaryButton(
      label: '확인',
      onTap: () {
        Get.back(result: filterModel);
      },
    );
  }
}
