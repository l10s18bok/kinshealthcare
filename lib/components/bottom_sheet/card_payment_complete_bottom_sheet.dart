import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

import 'base_bottom_sheet.dart';

class CardPaymentCompleteBottomSheet extends StatefulWidget {
  @override
  _CardPaymentCompleteBottomSheetState createState() =>
      _CardPaymentCompleteBottomSheetState();
}

class _CardPaymentCompleteBottomSheetState
    extends State<CardPaymentCompleteBottomSheet> {
  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;
    return BaseBottomSheet(
      body: Column(
        children: [
          Text(
            '카드등록이 완료되었습니다:)',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: theme.fontSize17,
              color: Color(0xFF161616),
            ),
          ),
          Container(height: 50.0),
          PrimaryButton(
            label: '확인',
            onTap: () {
              Get.back();
            },
          ),
        ],
      ),
    );
  }
}
