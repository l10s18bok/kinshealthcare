import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class RelationRequestBottomSheet extends StatefulWidget {
  @override
  _RelationRequestBottomSheetState createState() =>
      _RelationRequestBottomSheetState();
}

class _RelationRequestBottomSheetState
    extends State<RelationRequestBottomSheet> {
  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return BaseBottomSheet(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 32.0),
            child: Text(
              '요청 완료되었습니다!',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: theme.heavyFontWeight,
              ),
            ),
          ),
          PrimaryButton(
            label: '완료',
            onTap: () async {
              Get.offNamedUntil('/my-kins', (route) => false);
            },
          ),
        ],
      ),
    );
  }
}
