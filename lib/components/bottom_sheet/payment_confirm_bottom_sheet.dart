import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/avatar/payment_bottom_sheet_avatar.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/card/quote_card.dart';
import 'package:kins_healthcare/controllers/force_controller.dart';
import 'package:kins_healthcare/controllers/kkakka_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/services/force/model/event_detail_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class PaymentConfirmBottomSheet extends StatefulWidget {
  final EventDetailResponse event;

  PaymentConfirmBottomSheet({
    required this.event,
  });

  @override
  _PaymentConfirmBottomSheetState createState() =>
      _PaymentConfirmBottomSheetState();
}

class _PaymentConfirmBottomSheetState extends State<PaymentConfirmBottomSheet> {
  String signature = '';

  renderTitle() {
    final theme = ThemeFactory.of(context).theme;

    final fontSize = theme.fontSize15;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          '${widget.event.relation.userNameOpponent}님께서 ',
          style: TextStyle(
            fontSize: fontSize,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
        Text(
          '님께서 ',
          style: TextStyle(
            fontSize: fontSize,
          ),
        ),
        Text(
          '까까',
          style: TextStyle(
            fontSize: fontSize,
            color: theme.primaryOrangeColor,
          ),
        ),
        Text(
          '를 보냈어요!',
          style: TextStyle(
            fontSize: fontSize,
          ),
        ),
      ],
    );
  }

  renderImage() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      height: 200.0,
      decoration: BoxDecoration(
        color: theme.primaryColor,
        borderRadius: BorderRadius.circular(6.0),
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(
            widget.event.info.kkakkaImage!,
          ),
        ),
      ),
    );
  }

  renderTextField() {
    return Row(
      children: [
        GetBuilder<UserController>(builder: (c) {
          return SizedBox(
            child: PaymentBottomSheetAvatar(
              radius: 16.5,
              padding: 7.0,
              imageUrl: c.userInfo!.image,
            ),
          );
        }),
        Container(width: 8.0),
        Expanded(
          child: TextFormField(
            onChanged: (String val) {
              this.signature = val;
            },
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
              hintText: '사인하기 (선택)',
            ),
          ),
        ),
      ],
    );
  }

  renderButton() {
    return PrimaryButton(
      label: '확인',
      onTap: () async {
        await Get.find<KkakkaController>().signature(
          kkakkaId: widget.event.info.kkakkaId!,
          signature: signature,
        );
        await Get.find<ForceController>().forceEventUnder(
          reset: true,
        );
        Get.back();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        BaseBottomSheet(
          body: Column(
            children: [
              Container(height: 18.0),
              renderTitle(),
              Container(height: 24.0),
              QuoteCard(text: '기획필요'),
              Container(height: 16.0),
              renderImage(),
              Container(height: 16.0),
              renderTextField(),
              Container(height: 16.0),
              renderButton(),
            ],
          ),
        ),
        Positioned.fill(
          top: -37.0,
          child: PaymentBottomSheetAvatar(
            imageUrl: widget.event.relation.profileImgOpponent,
          ),
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();

    Get.find<UserController>().getUserInfo();
  }
}
