import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class CancelPaymentBottomSheet extends StatefulWidget {
  @override
  _CancelPaymentBottomSheetState createState() => _CancelPaymentBottomSheetState();
}

class _CancelPaymentBottomSheetState extends State<CancelPaymentBottomSheet> {
  renderTexts() {
    final theme = ThemeFactory.of(context).theme;

    final normalSize = TextStyle(
      fontSize: theme.fontSize15,
      color: Color(0xFF161616),
      fontWeight: theme.primaryFontWeight,
    );

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '모든 과정이 취소됩니다.',
              style: normalSize,
            ),
          ],
        ),
        Container(height: 24.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '결제를 취소하시겠어요?',
              style: normalSize.copyWith(
                fontSize: 17.0,
                fontWeight: theme.heavyFontWeight,
              ),
            ),
          ],
        ),
      ],
    );
  }

  renderButtons() {
    return Row(
      children: [
        Expanded(
          child: PrimaryButton(
            txtColor: Color(0xFF868686),
            bgColor: Color(0xE0E2ED6),
            label: '취소',
            onTap: () {
              Get.back();
            },
          ),
        ),
        Container(width: 12.0),
        Expanded(
          child: PrimaryButton(
            label: '확인',
            onTap: () {
              Get.back(result: 'OK');
            },
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BaseBottomSheet(
      body: Column(
        children: [
          renderTexts(),
          Container(height: 40.0),
          renderButtons(),
        ],
      ),
    );
  }
}
