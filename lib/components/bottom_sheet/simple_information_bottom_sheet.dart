import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class SimpleInformationBottomSheet extends StatefulWidget {
  final String message;
  final bool messageBold;
  final String? accentMessage;

  const SimpleInformationBottomSheet({Key? key, required this.message, this.messageBold = false, this.accentMessage}) : super(key: key);
  @override
  _SimpleInformationBottomSheetState createState() => _SimpleInformationBottomSheetState();
}

class _SimpleInformationBottomSheetState extends State<SimpleInformationBottomSheet> {
  renderAccentMsg() {
    if (widget.accentMessage == null) return Container();
    final theme = ThemeFactory.of(context).theme;
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Text(
        widget.accentMessage!,
        style: TextStyle(
          fontSize: theme.fontSize14,
          color: Color(0xFF4042AB),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;
    return BaseBottomSheet(
      body: Column(
        children: [
          Text(
            widget.message,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: theme.fontSize17,
              fontWeight: widget.messageBold ? FontWeight.bold : null,
              color: Color(0xFF161616),
            ),
          ),
          renderAccentMsg(),
          Container(height: 50.0),
          PrimaryButton(
            label: '확인',
            onTap: () {
              Get.back(result: 'OK');
            },
          ),
        ],
      ),
    );
  }
}
