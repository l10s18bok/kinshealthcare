import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

enum AdvertFilterType {
  JOB_FIRST,
  JOB_SECOND,
  CAREER,
  GENDER,
}

class AdvertFilterBottomSheet extends StatefulWidget {
  @override
  _AdvertFilterBottomSheetState createState() => _AdvertFilterBottomSheetState();
}

class _AdvertFilterBottomSheetState extends State<AdvertFilterBottomSheet> {
  Map<AdvertFilterType, String> checkMap = {};

  Widget renderTitle() {
    final theme = ThemeFactory.of(context).theme;
    return Stack(
      children: [
        Positioned.fill(
          child: Align(
            alignment: Alignment.center,
            child: Text(
              '필터',
              style: TextStyle(
                fontSize: theme.fontSize15,
                fontWeight: theme.heavyFontWeight,
              ),
            ),
          ),
        ),
        Positioned(
          top: 0,
          right: 0,
          child: InkWell(
            child: Icon(Icons.close, size: 23),
            onTap: () => Get.back(),
          ),
        ),
      ],
    );
  }

  Widget renderRadioButton({
    required String label,
    required dynamic value,
    GestureTapCallback? onTap,
    bool isPrimary = false,
  }) {
    final theme = ThemeFactory.of(context).theme;

    TextStyle style = TextStyle(
      fontSize: theme.fontSize11,
      color: kinsGray6F,
      fontWeight: FontWeight.w400,
    );

    Color borderColor = kinsGrayE9;

    if (isPrimary) {
      style = style.copyWith(fontWeight: theme.heavyFontWeight);
      borderColor = theme.primaryColor;
    }

    return Material(
      borderRadius: BorderRadius.circular(4.0),
      color: Colors.white,
      child: InkWell(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: borderColor),
            borderRadius: BorderRadius.circular(4.0),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 7.0, horizontal: 20.0),
            child: Text(label, style: style),
          ),
        ),
      ),
    );
  }

  List<Widget> renderOptionList(
    AdvertFilterType type,
    List<String> values, {
    required ValueSetter<String> onTap,
  }) {
    return values.map(
      (value) {
        final isPrimary = checkMap[type] == value;

        return renderRadioButton(
          label: value,
          value: value,
          onTap: () => onTap(value),
          isPrimary: isPrimary,
        );
      },
    ).toList();
  }

  Widget renderOptions(AdvertFilterType type) {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          _getTitle(type),
          style: TextStyle(
            fontSize: theme.fontSize12,
            fontWeight: FontWeight.w700,
            color: kinsGray6F,
          ),
        ),
        Container(height: 10.0),
        Wrap(
          spacing: 6.0,
          runSpacing: 6.0,
          children: renderOptionList(
            type,
            _getList(type),
            onTap: (title) {
              checkMap[type] = title;
              setState(() {});
            },
          ),
        ),
      ],
    );
  }

  Widget renderSheetLayout(Widget body) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          children: [
            Expanded(
              child: Container(
                height: 510,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(26.0),
                    topLeft: Radius.circular(26.0),
                  ),
                ),
                child: SafeArea(
                  child: Container(
                    height: double.infinity,
                    width: double.infinity,
                    padding: EdgeInsets.only(
                      left: 16,
                      right: 16,
                      bottom: 16,
                      top: 16,
                    ),
                    child: body,
                  ),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  Widget renderScrollMain() {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            renderOptions(AdvertFilterType.JOB_FIRST),
            Container(height: 20.0),
            renderOptions(AdvertFilterType.JOB_SECOND),
            Container(height: 20.0),
            renderOptions(AdvertFilterType.CAREER),
            Container(height: 20.0),
            renderOptions(AdvertFilterType.GENDER),
            Container(height: 32.0),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return renderSheetLayout(
      Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            height: 30,
            child: renderTitle(),
          ),
          Container(height: 40.0),
          Expanded(child: renderScrollMain()),
          SizedBox(
            height: 55,
            child: PrimaryButton(
              padding: EdgeInsets.zero,
              label: '적용',
              onTap: () {
                Get.back(result: checkMap);
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    checkMap[AdvertFilterType.JOB_FIRST] = '전체';
    checkMap[AdvertFilterType.JOB_SECOND] = '간호사';
    checkMap[AdvertFilterType.CAREER] = '전체';
    checkMap[AdvertFilterType.GENDER] = '전체';
  }

  String _getTitle(AdvertFilterType type) {
    switch (type) {
      case AdvertFilterType.JOB_FIRST:
        return '직종(선택 1)';
      case AdvertFilterType.JOB_SECOND:
        return '직종(선택 2)';
      case AdvertFilterType.CAREER:
        return '경력';
      case AdvertFilterType.GENDER:
        return '성별';
      default:
        return '제목';
    }
  }

  List<String> _getList(AdvertFilterType type) {
    switch (type) {
      case AdvertFilterType.JOB_FIRST:
        return ['전체', '간호사', '의료기사', '의사직', '약무직', '의료기타', '사무, 원무, 코디'];
      case AdvertFilterType.JOB_SECOND:
        return ['간호사', '간호 조무사', '진료보조'];
      case AdvertFilterType.CAREER:
        return ['전체', '무관', '1년이상', '2년이상'];
      case AdvertFilterType.GENDER:
        return ['전체', '남', '여'];
      default:
        return [];
    }
  }
}
