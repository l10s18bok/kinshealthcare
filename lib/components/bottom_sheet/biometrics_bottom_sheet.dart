import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/biometrics_success_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:local_auth/local_auth.dart';

class BiometricsBottomSheet extends StatefulWidget {
  /// 인증 실패 에러 메세지
  final String? authMessage;

  BiometricsBottomSheet({this.authMessage});

  @override
  _BiometricsBottomSheetState createState() => _BiometricsBottomSheetState();
}

class _BiometricsBottomSheetState extends State<BiometricsBottomSheet> {
  onAuthenticate() async {
    // final statuses = await [
    //   Permission.contacts,
    // ].request();

    // if(statuses[Permission.] != PermissionStatus.granted){
    //   Get.snackbar('권한 필요', '');
    //   return;
    // }

    final localAuth = LocalAuthentication();

    try {
      bool didAuthenticate = await localAuth.authenticate(
        biometricOnly: true,
        localizedReason:
            widget.authMessage == null ? '인증을 해주세요!' : widget.authMessage!,
      );

      Get.back();

      if (didAuthenticate) {
        Get.bottomSheet(BiometricsSuccessBottomSheet());
      }
    } catch (e) {
      // TODO 인증 실패
      print('인증 실패');
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return BaseBottomSheet(
      body: Column(
        children: [
          Text(
            '본인 인증',
            style: TextStyle(
              fontSize: theme.fontSize17,
              fontWeight: theme.heavyFontWeight,
            ),
          ),
          Container(height: 22.0),
          SvgPicture.asset(
            'assets/svgs/ic/ic_fingerprint_popup.svg',
          ),
          Container(height: 16.0),
          Text(
            '지문으로 인증해주세요 :)',
            style: TextStyle(
              fontSize: theme.fontSize14,
            ),
          ),
          Container(height: 40.0),
          PrimaryButton(
            label: '확인',
            onTap: () {
              onAuthenticate();
            },
          ),
        ],
      ),
    );
  }
}
