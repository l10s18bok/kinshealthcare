import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import '../../utils/resource.dart';

/*
 작성일 : 2021-01-21
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : RelationsChoiceSheet,
 설명 : 가족관계도에서 2개의 정보가 있는 경우 선택하기 위한 Bottom Sheet
*/

// ignore: must_be_immutable
class RelationsChoiceSheet extends StatelessWidget {
  RelationsChoiceSheet({
    required this.title,
    required this.firstName,
    required this.secondName,
    this.addNameList,
  });

  final String title;
  final String firstName;
  final String secondName;
  List<String>? addNameList;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 260,
      width: double.infinity,
      color: Colors.transparent,
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(26.0),
          topRight: Radius.circular(26.0),
        ),
        child: Container(
          width: double.infinity,
          color: Colors.white,
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 48),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: TextStyle(
                  fontSize: 17,
                  color: kinsBlack16,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(height: 10),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      PrimaryButton(
                        label: firstName,
                        onTap: () {
                          Get.back(result: firstName);
                        },
                      ),
                      SizedBox(height: 16),
                      PrimaryButton(
                        label: secondName,
                        onTap: () {
                          Get.back(result: secondName);
                        },
                      ),
                      SizedBox(height: 16),
                      ...renderAddButtons(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> renderAddButtons() {
    if (addNameList == null) return [];

    return addNameList!.map((item) {
      return Column(
        children: [
          PrimaryButton(
            label: item,
            onTap: () {
              Get.back(result: item);
            },
          ),
          SizedBox(height: 16),
        ],
      );
    }).toList();
  }
}
