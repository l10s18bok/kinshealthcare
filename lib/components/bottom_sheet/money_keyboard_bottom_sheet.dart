import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_bottom_sheet.dart';
import 'package:kins_healthcare/components/keyboard/money_input_keyboard.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

/*
 * 작성일 : 2021-03-25
 * 작성자 : JC (리뉴얼 Andy)
 * 화면명 :
 * 경로 :
 * 클래스 : MoneyBottomSheet
 * 설명 : 돈을 입력할 수 있는 bottom sheet
 */

class MoneyKeyboardBottomSheet extends StatefulWidget {
  @override
  _MoneyKeyboardBottomSheetState createState() => _MoneyKeyboardBottomSheetState();
}

class _MoneyKeyboardBottomSheetState extends State<MoneyKeyboardBottomSheet> {
  String value = '';
  TextEditingController editingController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    editingController.dispose();
    super.dispose();
  }

  double valueToDouble() {
    if (value.isEmpty) {
      return 0;
    } else {
      return double.parse(value);
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextUtils textUtils = TextUtils();
    return Stack(
      clipBehavior: Clip.none,
      children: [
        BaseBottomSheet(
          lrPadding: 0,
          expand: false,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: () {
                  Get.back(
                    result: MoneyBottomSheetResult(
                      amount: 0,
                    ),
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Icon(
                    Icons.close,
                    size: 23,
                  ),
                ),
              ),
              SizedBox(height: 20),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      textUtils.numberToLocalCurrency(
                            amount: valueToDouble(),
                          ) +
                          '원',
                      style: TextStyle(
                        fontSize: 28,
                        color: Color(0xFF161616),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        editingController.text = '0';
                      },
                      child: Container(
                        width: 20.0,
                        height: 20.0,
                        margin: const EdgeInsets.only(top: 10, bottom: 10, left: 50, right: 0),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xFF878787),
                        ),
                        child: Icon(
                          Icons.close,
                          size: 16.0,
                          color: Colors.white,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 20.0),
              MoneyInputKeyboard(
                textController: editingController,
                onConfirm: () {
                  Get.back(
                    result: MoneyBottomSheetResult(
                      amount: valueToDouble(),
                    ),
                  );
                },
                onKeyboardInput: (String value) {
                  setState(() {
                    this.value = value;
                  });
                },
              ),
            ],
          ),
        ),
        Positioned(
          top: -45,
          left: 0,
          child: Image.asset(
            'assets/svgs/img/money_keyboard_bottom_sheet.png',
            fit: BoxFit.cover,
          ),
        ),
      ],
    );
  }
}
