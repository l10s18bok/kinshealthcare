import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class ColorSwitch {
  Color colorSwitchForKkakkaType(String type, BuildContext context) {
    switch (type) {
      case 'D':
        return ThemeFactory.of(context).theme.primaryPurpleColor;
      case 'A':
        return ThemeFactory.of(context).theme.primaryPinkColor;
      case 'E':
        return ThemeFactory.of(context).theme.primaryPinkColor;
      case 'M':
        return ThemeFactory.of(context).theme.primaryPinkColor;
      case 'P':
        return ThemeFactory.of(context).theme.primaryGreenColor;
      case 'S':
        return ThemeFactory.of(context).theme.primaryPinkColor;
      case 'W':
        return ThemeFactory.of(context).theme.primaryPinkColor;
      case '기부':
        return ThemeFactory.of(context).theme.primaryPurpleColor;
      case '기념일':
        return ThemeFactory.of(context).theme.primaryPinkColor;
      case '교육':
        return ThemeFactory.of(context).theme.primaryPinkColor;
      case '의료':
        return ThemeFactory.of(context).theme.primaryPinkColor;
      case '용돈':
        return ThemeFactory.of(context).theme.primaryGreenColor;
      case 'SELF':
        return ThemeFactory.of(context).theme.primaryPinkColor;
      case 'W':
        return ThemeFactory.of(context).theme.primaryPinkColor;
      default:
        return ThemeFactory.of(context).theme.primaryPinkColor;
    }
  }
}
