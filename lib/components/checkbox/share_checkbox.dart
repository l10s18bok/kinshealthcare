import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/components/avatar/phone_avatar.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-02-17
 * 작성자 : JC
 * 화면명 : HN_S_0001
 * 주요기능 : 공유 체크박스
 */
class ShareCheckbox extends StatefulWidget {
  final bool isActive;
  final GestureTapCallback? onTap;

  ShareCheckbox({
    required this.onTap,
    this.isActive = false,
  });

  @override
  _ShareCheckboxState createState() => _ShareCheckboxState();
}

class _ShareCheckboxState extends State<ShareCheckbox> {
  renderAvatar() {
    return PhoneAvatar(
      isOnContacts: true,
      bgImageUrl: null,
    );
  }

  renderLabels() {
    final theme = ThemeFactory.of(context).theme;

    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            '어마마마 (엄마)',
            style: TextStyle(
              fontSize: theme.fontSize14,
              fontWeight: theme.heavyFontWeight,
            ),
          ),
          Text(
            '010-1234-5678',
            style: TextStyle(
              fontSize: theme.fontSize12,
              color: Color(0xFF3E3E3E),
            ),
          ),
        ],
      ),
    );
  }

  renderCheckbox() {
    final theme = ThemeFactory.of(context).theme;

    Color bgColor = Color(0xFFCFCFE2);

    if (widget.isActive) {
      bgColor = theme.primaryColor;
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
            color: bgColor,
            borderRadius: BorderRadius.circular(4.0),
          ),
          height: 26.0,
          width: 26.0,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 7.0, vertical: 6.0),
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_check.svg',
              height: 7.0,
              width: 6.0,
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      behavior: HitTestBehavior.opaque,
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            renderAvatar(),
            Container(width: 16.0),
            renderLabels(),
            renderCheckbox(),
          ],
        ),
      ),
    );
  }
}
