import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class DefaultDropDownFormField<T> extends StatefulWidget {
  final FormFieldValidator validator;
  final FormFieldSetter onSaved;
  final ValueChanged<T?>? onChanged;
  final List<DropdownMenuItem<T>>? items;
  final String? label;
  final String? hint;
  final T? value;

  DefaultDropDownFormField({
    required this.items,
    required this.validator,
    required this.onSaved,
    required this.onChanged,
    this.label,
    this.hint,
    this.value,
  });

  @override
  _DefaultDropDownFormFieldState createState() =>
      _DefaultDropDownFormFieldState<T>();
}

class _DefaultDropDownFormFieldState<T>
    extends State<DefaultDropDownFormField> {
  renderLabel() {
    final theme = ThemeFactory.of(context).theme;

    if (widget.label != null) {
      return Column(
        children: [
          Row(
            children: [
              Text(
                widget.label!,
                style: TextStyle(
                  fontSize: theme.fontSize13,
                  fontWeight: theme.heavyFontWeight,
                ),
              ),
            ],
          ),
        ],
      );
    }

    return Container();
  }

  renderDropDown() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      children: [
        Expanded(
          child: DropdownButtonFormField<T>(
            style: TextStyle(
              fontSize: theme.fontSize15,
              color: Colors.black,
            ),
            onChanged: widget.onChanged,
            onSaved: widget.onSaved,
            validator: widget.validator,
            value: widget.value,
            items: widget.items as List<DropdownMenuItem<T>>,
            hint: widget.hint != null
                ? Text(
                    widget.hint!,
                    style: TextStyle(
                      fontSize: theme.fontSize15,
                    ),
                  )
                : Container(),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        renderLabel(),
        renderDropDown(),
      ],
    );
  }
}
