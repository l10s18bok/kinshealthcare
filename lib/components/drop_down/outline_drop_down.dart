import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class OutlineDropDown extends StatefulWidget {
  final FormFieldValidator validator;
  final FormFieldSetter onSaved;
  final ValueChanged? onChanged;
  final List<DropdownMenuItem> items;
  final String? label;
  final String? hint;

  OutlineDropDown({
    @required this.onChanged,
    required this.items,
    required this.validator,
    required this.onSaved,
    this.label,
    this.hint,
  });

  @override
  _OutlineDropDownState createState() => _OutlineDropDownState();
}

class _OutlineDropDownState extends State<OutlineDropDown> {
  renderLabel() {
    final theme = ThemeFactory.of(context).theme;

    if (widget.label != null) {
      return Column(
        children: [
          Row(
            children: [
              Text(
                widget.label!,
                style: TextStyle(
                  fontSize: theme.fontSize13,
                  fontWeight: theme.heavyFontWeight,
                ),
              ),
            ],
          ),
        ],
      );
    }

    return Container();
  }

  renderDropDown() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      children: [
        Expanded(
          child: DropdownButtonFormField(
            decoration: InputDecoration(
              isDense: true,
              contentPadding: EdgeInsets.symmetric(
                vertical: 9,
                horizontal: 14,
              ),
              border: OutlineInputBorder(),
            ),
            style: TextStyle(
              fontSize: theme.fontSize13,
              color: Colors.black,
            ),
            onChanged: widget.onChanged,
            onSaved: widget.onSaved,
            validator: widget.validator,
            items: widget.items,
            hint: widget.hint != null
                ? Text(
                    widget.hint!,
                    style: TextStyle(
                      fontSize: theme.fontSize15,
                    ),
                  )
                : Container(),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        renderLabel(),
        renderDropDown(),
      ],
    );
  }
}
