import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/drop_down/default_drop_down_form_field.dart';

class RegisterQuestionDropDownFormField extends StatefulWidget {
  final FormFieldSetter onSaved;

  RegisterQuestionDropDownFormField({
    required this.onSaved,
  });

  @override
  _RegisterQuestionDropDownFormFieldState createState() =>
      _RegisterQuestionDropDownFormFieldState();
}

class _RegisterQuestionDropDownFormFieldState
    extends State<RegisterQuestionDropDownFormField> {
  List<String> values = [
    '내가 나온 초등학교 이름은?',
    '기억에 남는 추억의 장소는?',
    '인상 깊게 읽은 책 이름은?',
    '자신의 보물 제 1호는?',
    '가장 기억에 남는 선생님 성함은?',
  ];

  String value = '1';

  @override
  Widget build(BuildContext context) {
    return DefaultDropDownFormField(
      label: '비밀번호 질문',
      onChanged: (val) {
        setState(() {
          value = val as String;
        });
      },
      items: this
          .values
          .map(
            (entry) => DropdownMenuItem(
              child: Text(
                entry,
              ),
              value: (this.values.indexOf(entry) + 1).toString(),
            ),
          )
          .toList(),
      value: this.value,
      validator: (val) {
        return null;
      },
      onSaved: widget.onSaved,
    );
  }
}
