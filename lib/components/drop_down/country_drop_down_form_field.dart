import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/drop_down/drop_down.dart';

class CountryDropDownFormField extends StatefulWidget {
  final String label;
  final FormFieldSetter onSaved;

  CountryDropDownFormField({
    required this.label,
    required this.onSaved,
  });

  @override
  _CountryDropDownFormFieldState createState() =>
      _CountryDropDownFormFieldState();
}

class _CountryDropDownFormFieldState extends State<CountryDropDownFormField> {
  String dropdownVal = '82';

  @override
  Widget build(BuildContext context) {
    return DefaultDropDownFormField(
      label: widget.label,
      onChanged: (val) {
        if (val != null) {
          setState(() {
            dropdownVal = val as String;
          });
        }
      },
      validator: (val) {
        return null;
      },
      onSaved: widget.onSaved,
      value: dropdownVal,
      items: [
        DropdownMenuItem(
          child: Text('한국 (+82)'),
          value: '82',
        ),
      ],
      // label: '핸드폰 번호',
    );
  }
}
