import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

/*
 * 작성일 : 2021-01-22
 * 작성자 : Andy
 * 사용화면 : HP_5003(병원 상세정보), HS_1001(까까 유형선택)
 */

enum Direction { forward, re }

class NormalSlideImage extends StatefulWidget {
  final List<String> imageList;

  const NormalSlideImage({
    required List<String> imageList,
  }) : this.imageList = imageList;

  @override
  _NormalSlideImageState createState() => _NormalSlideImageState();
}

class _NormalSlideImageState extends State<NormalSlideImage> {
  int _currentPage = 0;
  PageController _controller = PageController(initialPage: 0);
  late Timer timer;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: 5), (Timer timer) {
      return setState(() {
        if (_currentPage < widget.imageList.length - 1) {
          _currentPage++;
        } else {
          _currentPage = 0;
        }

        _controller.animateToPage(
          _currentPage,
          duration: Duration(milliseconds: 350),
          curve: Curves.easeIn,
        );
      });
    });
  }

  _onchanged(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  renderImgComponent(String imgUrl) {
    return CachedNetworkImage(
      imageUrl: imgUrl,
      fit: BoxFit.contain,
      alignment: Alignment.center,
      placeholder: (context, url) => Center(child: CircularProgressIndicator()),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }

  renderIndicator(BoxConstraints constraints) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List<Widget>.generate(widget.imageList.length, (int index) {
            return AnimatedContainer(
                duration: Duration(milliseconds: 300),
                height: 10,
                width: 10,
                margin: EdgeInsets.symmetric(
                    horizontal: 3, vertical: constraints.maxHeight / 20),
                decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: Color(0xFF4042AB),
                    ),
                    shape: BoxShape.circle,
                    color: (index == _currentPage) ? Color(0xFF4042AB) : null));
          }),
        ),
      ],
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      //shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      builder: (BuildContext context, BoxConstraints constraints) {
        return Stack(
          children: [
            PageView.builder(
              scrollDirection: Axis.horizontal,
              onPageChanged: _onchanged,
              controller: _controller,
              itemCount: widget.imageList.length,
              itemBuilder: (context, int index) {
                return renderImgComponent(widget.imageList[index]);
              },
            ),
            renderIndicator(constraints),
          ],
        );
      },
    );
  }
}
