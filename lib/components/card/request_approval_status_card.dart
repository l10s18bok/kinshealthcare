import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/services/payment/model/payment_payment_request_status.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

enum StatusTypes {
  pending,
  approved,
  rejected,
}

class RequestApprovalStatusCard extends StatefulWidget {
  /// PaymentRequestResponseMe || PaymentRequestResponseRequest
  final dynamic model;

  // final String label;
  // final String? subLabel;
  // final double? amount;
  final StatusTypes? type;

  // final String? imgUrl;

  /// 오른쪽 status 및 가격을 다른 위젯으로 대체가능
  final Widget? customRightSideWidget;

  RequestApprovalStatusCard({
    required this.model,
    // required this.label,
    // this.imgUrl,
    this.type,
    // this.amount,
    // this.subLabel,
    this.customRightSideWidget,
  });

  @override
  _RequestApprovalStatusCardState createState() =>
      _RequestApprovalStatusCardState();
}

class _RequestApprovalStatusCardState extends State<RequestApprovalStatusCard> {
  TextUtils textUtils = TextUtils();

  renderStatusChip({
    required StatusTypes status,
  }) {
    final theme = ThemeFactory.of(context).theme;

    String display = '승인대기';

    if (status == StatusTypes.approved) {
      display = '승인완료';
    } else if (status == StatusTypes.rejected) {
      display = '승인거절';
    }

    Color color = Color(0xFF999999);

    if (status == StatusTypes.approved) {
      color = theme.primaryColor;
    } else if (status == StatusTypes.rejected) {
      color = theme.primaryRedColor;
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          color: color,
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: 3.0,
              horizontal: 8.0,
            ),
            child: Text(
              display,
              style: TextStyle(
                color: Colors.white,
                fontWeight: theme.heavyFontWeight,
                fontSize: theme.fontSize10,
              ),
            ),
          ),
        ),
      ],
    );
  }

  renderKkakka() {
    final theme = ThemeFactory.of(context).theme;

    final model = widget.model as PaymentRequestStatusResponseMe;

    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(4.0)),
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border(
                left: BorderSide(
                  color: theme.primaryGreenColor,
                  width: 4.0,
                ),
              ),
            ),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(4.0),
                border: Border.all(
                  color: Colors.grey[200]!,
                  width: 1.0,
                ),
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 32.0),
                child: Center(
                  child: Column(
                    children: [
                      Text(
                        TextUtils().numberToLocalCurrency(
                              amount: model.approveAmount!.toDouble(),
                            ) +
                            '원',
                        style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: theme.heavyFontWeight,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 4.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircleAvatar(
                              radius: 8.0,
                              backgroundImage: NetworkImage(
                                model.kkakkaProfileImg!,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 4.0),
                              child: Text(
                                model.kkakkaUserName! +
                                    '(${model.kkakkaUserRelCd})',
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            right: 0,
            top: 0,
            child: SvgPicture.asset(
              model.imageName,
            ),
          ),
        ],
      ),
    );
  }

  renderCard() {
    final theme = ThemeFactory.of(context).theme;

    final model = widget.model as PaymentRequestStatusResponseMe;

    return Container(
      decoration: BoxDecoration(
        color: theme.primaryBlueColor,
        borderRadius: BorderRadius.circular(8),
      ),
      constraints: BoxConstraints(
        minHeight: 90.0,
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        child: Text(
          model.userPaymentWayCompany == null
              ? '카드사 알수없음'
              : model.userPaymentWayCompany!,
          style: TextStyle(
            color: Colors.white,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
      ),
    );
  }

  renderThumbnail() {
    if (widget.model is PaymentRequestStatusResponseRequest) {
      final model = widget.model as PaymentRequestStatusResponseRequest;
      return CircleAvatar(
        radius: 19,
        backgroundImage: NetworkImage(
          model.profileImgTo,
        ),
      );
    } else {
      final model = widget.model as PaymentRequestStatusResponseMe;

      if (model.setlePaymentType == '까까') {
        return Expanded(
          child: Padding(
            padding: EdgeInsets.only(),
            child: renderKkakka(),
          ),
        );
      } else {
        return Expanded(
          child: Padding(
            padding: EdgeInsets.only(),
            child: renderCard(),
          ),
        );
      }
    }
  }

  renderLabel() {
    final theme = ThemeFactory.of(context).theme;

    if (widget.model is PaymentRequestStatusResponseRequest) {
      final model = widget.model as PaymentRequestStatusResponseRequest;

      return IntrinsicWidth(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Text(
                        model.userNameTo,
                        style: TextStyle(
                          fontSize: theme.fontSize12,
                          fontWeight: theme.heavyFontWeight,
                          color: theme.primaryColor,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        model.userRelCdTo,
                        style: TextStyle(
                          fontSize: theme.fontSize11,
                          fontWeight: theme.primaryFontWeight,
                          color: Color(0xFF3E3E3E),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  renderLabelAndSubLabel() {
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          renderThumbnail(),
          Container(width: 10.0),
          renderLabel(),
        ],
      ),
    );
  }

  renderAmount() {
    final theme = ThemeFactory.of(context).theme;

    int amount = 0;

    if (widget.model is PaymentRequestStatusResponseRequest) {
      final model = widget.model as PaymentRequestStatusResponseRequest;

      amount = model.amount;
    } else {
      final model = widget.model as PaymentRequestStatusResponseMe;

      amount = model.approveAmount!;
    }

    return Column(
      children: [
        Container(height: 5.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              '${textUtils.numberToLocalCurrency(amount: amount.toDouble())}원',
              textAlign: TextAlign.end,
              style: TextStyle(
                color: Color(0xFF262626),
                fontSize: theme.fontSize15,
              ),
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: renderLabelAndSubLabel(),
        ),
        if (widget.customRightSideWidget != null) widget.customRightSideWidget!,
        if (widget.customRightSideWidget == null)
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                if (widget.type != null)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      renderStatusChip(
                        status: widget.type!,
                      ),
                    ],
                  ),
                renderAmount(),
              ],
            ),
          ),
      ],
    );
  }
}
