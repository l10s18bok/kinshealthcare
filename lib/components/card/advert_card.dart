import 'package:flutter/material.dart';

class AdvertCard extends StatefulWidget {
  @override
  _AdvertCardState createState() => _AdvertCardState();
}

class _AdvertCardState extends State<AdvertCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset(
        'assets/svgs/img/img_banner.jpg',
        fit: BoxFit.cover,
      ),
    );
  }
}

