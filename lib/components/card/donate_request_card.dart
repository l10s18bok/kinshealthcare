import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-02-08,
 작성자 : phil,
 화면명 :
 클래스 : DonateRequestCard,
 설명 : 기부 선택 카드,
*/

class DonateRequestCard extends StatefulWidget {
  final int id;
  final String title;
  final String? subTitle;
  final String iconPath;
  final GestureTapCallback? onTap;
  final bool isPrimaryColor;
  final double percent;
  final String treeLevel;

  DonateRequestCard({
    required int id,
    required String title,
    String? subTitle,
    required String iconPath,
    required double percent,
    required String treeLevel,
    bool isPrimaryColor = false,
    GestureTapCallback? onTap,
  })  : this.id = id,
        this.title = title,
        this.subTitle = subTitle,
        this.iconPath = iconPath,
        this.percent = percent,
        this.isPrimaryColor = isPrimaryColor,
        this.onTap = onTap,
        this.treeLevel = treeLevel;

  @override
  _DonateRequestCardState createState() => _DonateRequestCardState();
}

class _DonateRequestCardState extends State<DonateRequestCard> {
  renderIcon() {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      children: [
        SvgPicture.asset(
          widget.iconPath,
        ),
        Container(height: 4),
        Text(widget.treeLevel,
            style: TextStyle(
              color: Color(0xff3e3e3e),
              fontWeight: FontWeight.w700,
              fontFamily: "AppleSDGothicNeo",
              fontStyle: FontStyle.normal,
              fontSize: theme.fontSize10,
            ),
            textAlign: TextAlign.center)
      ],
    );
  }

  renderText() {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(widget.title,
            style: TextStyle(
              color: Color(0xff4042ab),
              fontWeight: FontWeight.w700,
              fontFamily: "AppleSDGothicNeo",
              fontStyle: FontStyle.normal,
              fontSize: theme.fontSize17,
            )),
        Container(height: 4),
        Text(widget.subTitle ?? '',
            style: TextStyle(
              color: Color(0xff3e3e3e),
              fontWeight: FontWeight.w400,
              fontFamily: "AppleSDGothicNeo",
              fontStyle: FontStyle.normal,
              fontSize: theme.fontSize11,
            )),
      ],
    );
  }

  renderProgress() {
    double percent;
    percent = widget.percent / 100;
    return Stack(
      children: [
        Container(
            height: 7,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(3.5)),
                color: widget.isPrimaryColor
                    ? const Color(0xffffffff)
                    : const Color(0xffe4e4ed))),
        Container(
            width: 360 * percent,
            height: 7,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(3.5)),
                color: const Color(0xff4042ab))),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final bgColor = widget.isPrimaryColor ? Color(0xffefeff8) : Colors.white;

    final boderColor = widget.isPrimaryColor ? Color(0xff1d1f75) : Color(0xffd7d8ee);

    final borderRadius = BorderRadius.circular(
      6.0,
    );

    return Ink(
      child: InkWell(
        borderRadius: borderRadius,
        onTap: widget.onTap,
        child: Container(
          decoration: BoxDecoration(
              borderRadius: borderRadius,
              color: bgColor,
              border: Border.all(color: boderColor)),
          child: Padding(
            padding: EdgeInsets.only(top: 16, left: 18, right: 18, bottom: 15),
            child: Column(
              children: [
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        renderText(),
                        renderIcon(),
                      ],
                    ),
                    Container(height: 10),
                    renderProgress(),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
