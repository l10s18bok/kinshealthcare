import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-03-04
 작성자 : Mark,
 화면명 : ,
 경로 : ,
 클래스 : KkakkaDetailContent,
 설명 : 까까 자세히 보기 Content Card
*/

// ignore: must_be_immutable
class KkakkaDetailContent extends StatelessWidget {
  KkakkaDetailContent({
    required this.content,
  });

  final String content;
  var theme;
  final margin = const EdgeInsets.only(left: 16, right: 16, top: 17);
  final inPadding = const EdgeInsets.symmetric(horizontal: 22, vertical: 27);

  @override
  Widget build(BuildContext context) {
    theme = ThemeFactory.of(context).theme;

    return Stack(
      children: [
        Container(
          margin: margin,
          padding: inPadding,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: kinsBlueEF,
            borderRadius: BorderRadius.circular(6.0),
          ),
          child: renderContent(),
        ),
        renderQuotes(),
      ],
    );
  }

  renderContent() {
    return Text(
      content,
      style: TextStyle(
        fontSize: theme.fontSize12,
        color: kinsBlack45,
      ),
      textAlign: TextAlign.center,
    );
  }

  renderQuotes() {
    return Positioned(
      top: 0,
      left: 20,
      child: Container(
        margin: EdgeInsets.all(8),
        height: 18,
        width: 18,
        child: SvgPicture.asset(
          'assets/svgs/ic/ic_quotes.svg',
          color: kinsBlue40,
          height: 18,
          width: 18,
        ),
      ),
    );
  }
}
