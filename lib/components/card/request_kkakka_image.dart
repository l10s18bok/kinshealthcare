import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/text/kkakka_type_translate.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/models/kkakka_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-04-07
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : RequestKkakkaImage,
 설명 : 까까 더보기 화면에 Card Image Widget
*/

// ignore: must_be_immutable
class RequestKkakkaImage extends StatelessWidget {
  dynamic kkakka;
  late BuildContext context;

  RequestKkakkaImage({required this.kkakka});

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return AspectRatio(
      aspectRatio: 328 / 180,
      child: Stack(
        children: [
          Positioned.fill(
            child: renderImage(kkakka.kkakkaImage),
          ),
          Positioned(
            right: 8,
            top: 12,
            child: renderType(),
          ),
          Positioned.fill(
            child: renderNotActivity(),
          ),
          Positioned.fill(
            child: Center(child: renderNotActivityImage()),
          ),
        ],
      ),
    );
  }

  Widget renderType() {
    final color = KkakkaTypeTranslate(model: kkakka).primaryColor;
    final title = kkakka.kkakkaType;

    return Container(
      alignment: Alignment.center,
      width: 61,
      height: 26,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.all(Radius.circular(4)),
      ),
      child: Text(
        title,
        style: TextStyle(
          color: kinsWhite,
          fontWeight: FontWeight.w500,
          fontSize: ThemeFactory.of(context).theme.fontSize14,
        ),
      ),
    );
  }

  Widget renderImage(String? path) {
    if (path == null) return nonImage();

    return CachedNetworkImage(
      fit: BoxFit.fitWidth,
      alignment: Alignment.center,
      imageUrl: path,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          color: kinsWhite,
          borderRadius: BorderRadius.circular(8.0),
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.cover,
            alignment: Alignment.center,
          ),
        ),
      ),
      errorWidget: (_, url, error) => nonImage(),
    );
  }

  Widget nonImage() {
    return NonKkakkaCard(title: 'No Image', height: double.infinity);
  }

  Widget renderNotActivity() {
    if (KkakkaTypeTranslate(model: kkakka).kkakkaStatus ==
            KkakkaStatus.ACTIVE ||
        KkakkaTypeTranslate(model: kkakka).kkakkaFilterStatus ==
            KkakkaFilterStatus.ACTIVE) return Container();

    return Opacity(
      opacity: 0.4796338762555803,
      child: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(4)),
          color: kinsBlack30,
        ),
      ),
    );
  }

  Widget renderNotActivityImage() {
    if (KkakkaTypeTranslate(model: kkakka).kkakkaStatus ==
            KkakkaStatus.ACTIVE ||
        KkakkaTypeTranslate(model: kkakka).kkakkaFilterStatus ==
            KkakkaFilterStatus.ACTIVE) return Container();

    final title = kkakka.kkakkaStatus;
    final path = KkakkaTypeTranslate(model: kkakka).getStatusImg;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(12),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            color: kinsBlue40,
          ),
          child: SvgPicture.asset(path, width: 30, height: 30),
        ),
        SizedBox(height: 6),
        Text(
          title,
          style: TextStyle(
            color: kinsWhite,
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize12,
          ),
        ),
      ],
    );
  }

  String? numberWithComma(int? param) {
    if (param == null) return null;
    return new NumberFormat('###,###,###,###')
        .format(param)
        .replaceAll(' ', '');
  }
}
