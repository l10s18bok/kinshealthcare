import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/components/button/friend_button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/widget_utils.dart';

import 'circle_network_image.dart';

class FriendCardWithButtons extends StatefulWidget {
  /// 전화번호부에 있는지 없는지 여부
  final bool isOnContacts;

  /// Option 버튼을 렌더링 할지여부 (이제 사용 안하는 것 같음)
  final bool renderOptions;

  /// 내가 상대를 볼때 이름
  final String oppName;

  /// 내가 상대를 볼때 관계
  final String oppRelationship;

  /// 상대가 나를 볼때 이름
  final String? meName;

  /// 상대가 나를 볼때 관계
  final String? meRelationship;

  /// 핸드폰 번호
  final String phoneNumber;

  /// 아래 렌더링 할 버튼들
  final List<FriendButton>? buttons;

  /// 오른쪽 옵션 위젯들
  final Widget? options;

  /// 아바타 이미지
  final String? avatarImageUrl;

  /// 배경 색깔
  final Color? avatarBgColor;

  /// 프로필 이미지 ID(2021/04/15  Andy 추가)
  final int? userNoRel;

  FriendCardWithButtons({
    required this.oppName,
    required this.phoneNumber,
    required this.oppRelationship,
    this.avatarImageUrl,
    this.options,
    this.buttons,
    this.meName,
    this.meRelationship,
    this.isOnContacts = false,
    this.renderOptions = false,
    this.avatarBgColor,
    this.userNoRel,
  });

  @override
  _FriendCardWithButtonsState createState() => _FriendCardWithButtonsState();
}

class _FriendCardWithButtonsState extends State<FriendCardWithButtons> {
  renderCallIcon() {
    if (widget.isOnContacts) {
      return SvgPicture.asset(
        'assets/svgs/ic/ic_call_contactpage_on.svg',
      );
    } else {
      return SvgPicture.asset(
        'assets/svgs/ic/ic_call_contactpage_off.svg',
      );
    }
  }

  renderAvatar() {
    // return PhoneAvatar(
    //   isOnContacts: widget.isOnContacts,
    //   bgImageUrl: widget.avatarImageUrl,
    //   callIconOff: widget.callIconOff,
    // );
    // 프로필 이미지 ID(2021/04/15  Andy 추가)
    return CircleNetworkImage(
      imageSize: 56,
      path: widget.avatarImageUrl,
      userNo: widget.userNoRel,
      heroTag: widget.avatarImageUrl! + widget.userNoRel.toString(),
    );
  }

  renderNameAndType({
    required String name,
    required String type,
  }) {
    final theme = ThemeFactory.of(context).theme;

    final typeStyle = TextStyle(
      fontSize: theme.fontSize14,
      color: widget.avatarBgColor == null ? theme.primaryColor : widget.avatarBgColor,
    );

    final nameStyle = typeStyle.copyWith(
      fontWeight: FontWeight.w700,
      color: Colors.black,
    );

    return Row(
      children: [
        Text(
          name,
          style: nameStyle,
        ),
        Container(width: 5.0),
        Text(
          type,
          style: typeStyle,
        ),
      ],
    );
  }

  renderOptionButton() {
    if (widget.renderOptions) {
      return Positioned(
        right: 0,
        top: 0,
        child: ConstrainedBox(
          constraints: BoxConstraints(
            maxWidth: 30.0,
            maxHeight: 30.0,
          ),
          child: PopupMenuButton(
            padding: EdgeInsets.zero,
            itemBuilder: (_) {
              return [
                PopupMenuItem(
                  child: Text('모르는척 해제'),
                ),
                PopupMenuItem(
                  child: Text('차단하기'),
                ),
              ];
            },
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  renderText() {
    final theme = ThemeFactory.of(context).theme;
    String reName = widget.oppRelationship;
    if (widget.oppRelationship.length > 7) {
      reName = widget.oppRelationship.substring(0, 7) + '..';
    }

    return Container(
      child: Stack(
        children: [
          renderOptionButton(),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                children: [
                  renderNameAndType(
                    type: widget.oppName,
                    name: reName,
                  ),
                  if (widget.meName != null && widget.meRelationship != null)
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      child: Icon(
                        Icons.arrow_right,
                        color: Color(0xFFBABAC2),
                      ),
                    ),
                  if (widget.meName != null && widget.meRelationship != null)
                    renderNameAndType(
                      name: widget.meName!,
                      type: widget.meRelationship!,
                    ),
                ],
              ),
              Row(
                children: [
                  Text(
                    widget.phoneNumber,
                    style: TextStyle(
                      fontSize: theme.fontSize12,
                      color: Color(0xFF3E3E3E),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  renderBody() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 14.0),
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            renderAvatar(),
            Container(width: 12),
            Expanded(
              child: renderText(),
            ),
            if (widget.options != null) widget.options!,
          ],
        ),
      ),
    );
  }

  renderButtons() {
    if (widget.buttons == null) {
      return Container();
    } else {
      WidgetUtils utils = WidgetUtils();

      return utils.renderListWidgetsInMultipleRows(
        widgets: widget.buttons!,
        rowMax: widget.buttons!.length,
        widgetSpacing: 14.0,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          renderBody(),
          renderButtons(),
        ],
      ),
    );
  }
}
