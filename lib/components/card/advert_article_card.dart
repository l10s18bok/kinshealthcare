import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class AdvertArticleCard extends StatefulWidget {
  @override
  _AdvertArticleCardState createState() => _AdvertArticleCardState();
}

class _AdvertArticleCardState extends State<AdvertArticleCard> {

  renderIndicator({
    bool isPrimary = false,
  }) {
    final theme = ThemeFactory.of(context).theme;

    Color bg = isPrimary ? theme.primaryColor : Color(0xFFE1E1E1);

    return Container(
      height: 2.0,
      width: 20.0,
      color: bg,
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                '기사 전체보기 >',
                style: TextStyle(
                  fontSize: theme.fontSize12,
                  color: Color(0xFF9C9C9C),
                  fontWeight: theme.heavyFontWeight,
                ),
              ),
            ],
          ),
          Container(height: 16.0),
          Row(
            children: [
              Expanded(
                child: /*Image.asset(
                  'assets/svgs/img/sample_article.png',
                  fit: BoxFit.cover,
                )*/
                ClipRRect (
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  child: Image.network(
                    "https://img1.daumcdn.net/thumb/R1280x0/?scode=mtistory2&fname=https%3A%2F%2Fblog.kakaocdn.net%2Fdn%2FlXAXt%2Fbtq22ENB7g7%2F3n766LMLXr4dwiL6z96cw1%2Fimg.jpg",
                    height: 169,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
          Container(height: 16.0),

          /** 아래는 carosel 적용시 삭제예정이나 디자인 참고용으로 잠시 놔둠 */
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              renderIndicator(),
              Container(width: 5.0),
              renderIndicator(),
              Container(width: 5.0),
              renderIndicator(
                isPrimary: true,
              ),
              Container(width: 5.0),
              renderIndicator(),
              Container(width: 5.0),
              renderIndicator(),
            ],
          ),
        ],
      ),
    );
  }
}

