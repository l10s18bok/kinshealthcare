import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/bottom_sheet/btn_bottom_sheet.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/payment/model/payment_list_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-03-19
 작성자 : Mark,
 화면명 : ,
 경로 : ,
 클래스 : PaymentListCard,
 설명 : 결제 목록 카드
*/

// ignore: must_be_immutable
class PaymentListCard extends StatelessWidget {
  var theme;
  PaymentListModel model;
  VoidCallback? endCallBack;
  PaymentListCard({
    required this.model,
    this.endCallBack,
  });

  @override
  Widget build(BuildContext context) {
    theme = ThemeFactory.of(context).theme;

    return GestureDetector(
      onTap: () {
        // TODO 결제 재 조회가능 여부 조건문 달기
        if (model.setleStatus == 'REQUEST') {
          Get.find<PaymentController>().setleId = model.setleId;
          Get.toNamed('/payment/wait');
        }
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          border: Border.all(
            width: 2.0,
            color: kinsBlueD7,
          ),
        ),
        child: Padding(
          padding: EdgeInsets.all(14.0),
          child: Column(
            children: [
              renderTopRow(model),
              SizedBox(height: 10),
              Container(
                color: Colors.grey[300],
                height: 1.0,
              ),
              SizedBox(height: 14),
              renderKeyValue(key: '가맹점', value: model.storeName ?? ''),
              renderKeyValue(
                  key: '주소', value: '${model.address1} ${model.address2}'),
              renderTimeKeyValue(),
              SizedBox(height: 20),
              renderBottomRow(model),
            ],
          ),
        ),
      ),
    );
  }

  renderTopRow(PaymentListModel model) {
    final path = model.userImage;
    return Row(
      children: [
        renderImage(path),
        Container(width: 8.0),
        Expanded(
          child: Text(
            model.userName ?? '',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: theme.fontSize12,
            ),
          ),
        ),
        Text(
          _getTitle(model),
          style: TextStyle(
            color: _getTitleColor(model),
            fontWeight: FontWeight.w700,
            fontSize: theme.fontSize18,
          ),
        ),
      ],
    );
  }

  Widget renderImage(String? path) {
    final imageHeight = 32.0;
    if (path == null || !path.startsWith('http'))
      return renderNonImage(imageHeight);
    return renderHttpImage(imageHeight, path);
  }

  Widget renderNonImage(double imageHeight) {
    return SizedBox(
      height: imageHeight,
      width: imageHeight,
      child: SvgPicture.asset(
        'assets/svgs/ic/ic_profile_signin.svg',
        fit: BoxFit.cover,
        width: imageHeight,
        height: imageHeight,
      ),
    );
  }

  Widget renderHttpImage(double imageHeight, String path) {
    return CachedNetworkImage(
      imageUrl: path,
      imageBuilder: (context, imageProvider) => Container(
        width: imageHeight,
        height: imageHeight,
        decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.circular(imageHeight / 2),
          image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
        ),
      ),
      //placeholder: (context, url) => CircularProgressIndicator(),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }

  renderKeyValue({
    required String key,
    required String value,
    bool isPrimary = false,
  }) {
    return Padding(
      padding: EdgeInsets.only(bottom: 4.0),
      child: Row(
        children: [
          Container(
            width: 80.0,
            child: Text(
              key,
              style: TextStyle(
                color: Colors.grey[700],
                fontWeight: theme.heavyFontWeight,
              ),
            ),
          ),
          Expanded(
            child: Text(
              value,
              style: TextStyle(
                color: isPrimary ? theme.primaryColor : Colors.grey,
                fontWeight:
                    isPrimary ? theme.heavyFontWeight : theme.primaryFontWeight,
              ),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }

  renderTimeKeyValue() {
    return Padding(
      padding: EdgeInsets.only(bottom: 4.0),
      child: Row(
        children: [
          Expanded(
              child: renderKeyValue(key: '결제일', value: model.lastDateString)),
          Expanded(child: renderKeyValue(key: '취소일', value: '')),
        ],
      ),
    );
  }

  renderBottomRow(PaymentListModel model) {
    final price = NumberFormat("#,###").format(model.totalAmount ?? 0);

    return Row(
      children: [
        _getButton(model),
        SizedBox(width: 10),
        Text(
          _showDate(model),
          style: TextStyle(
            color: Colors.grey,
            fontWeight: theme.primaryFontWeight,
          ),
        ),
        Expanded(
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(
              '$price 원',
              style: TextStyle(
                color: _getPriceColor(model),
                fontSize: theme.fontSize18,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        ),
      ],
    );
  }

  renderBtn(String title, {VoidCallback? onTap}) {
    if (onTap == null) return Container();

    return Container(
      alignment: Alignment.centerLeft,
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(4)),
        child: InkWell(
          child: Container(
            width: 100,
            height: 35,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(6)),
              border: Border.all(color: kinsGrayB6),
            ),
            child: Text(
              title,
              style: TextStyle(
                color: kinsBlack44,
                fontWeight: FontWeight.w500,
                fontSize: theme.fontSize11,
              ),
            ),
          ),
          onTap: onTap,
        ),
      ),
    );
  }

  String _showDate(PaymentListModel model) {
    if (model.lastDate == null) return "0일 전";

    final day = model.daysTillLastDate;
    if (day > 0) return "${day + 1} 일 전";

    final hours = model.hoursTillLastDate;
    if (hours > 1) return "$hours 시간 전";

    final minutes = model.minutesTillLastDate;
    return "$minutes 분 전";
  }

  String _getTitle(PaymentListModel model) {
    switch (model.setleStatus) {
      case "REQUEST":
        return '결제요청';
      case "FAIL":
        return '결제실패';
      case "SUCCESS":
        return '결제성공';
      case "CANCEL_REQUEST":
        return '취소요청';
      case "CANCEL_FAIL":
        return '취소실패';
      case "CANCEL_SUCCESS":
        return '취소성공';
      case "ROLLBACK_REQUEST":
        return '롤백완료';
      case "ROLLBACK_SUCCESS":
        return '롤백취소';
      default:
        return '결제완료';
    }
  }

  Color _getTitleColor(PaymentListModel model) {
    switch (model.setleStatus) {
      case "REQUEST":
      case "SUCCESS":
        return kinsBlue40;
      case "FAIL":
      case "CANCEL_REQUEST":
      case "ROLLBACK_SUCCESS":
        return kinsRedDC;
      case "CANCEL_SUCCESS":
      case "CANCEL_FAIL":
      case "ROLLBACK_REQUEST":
        return kinsGrayB6;
      default:
        return kinsGrayB6;
    }
  }

  Color _getPriceColor(PaymentListModel model) {
    switch (model.setleStatus) {
      case "REQUEST":
      case "SUCCESS":
        return kinsOrangeF2;
      case "CANCEL_SUCCESS":
      case "FAIL":
      case "CANCEL_REQUEST":
      case "ROLLBACK_SUCCESS":
      case "CANCEL_FAIL":
      case "ROLLBACK_REQUEST":
        return kinsGrayB6;
      default:
        return kinsGrayB6;
    }
  }

  Widget _getButton(PaymentListModel model) {
    switch (model.setleStatus) {
      case "REQUEST":
      case "FAIL":
        return Container();
      case "SUCCESS":
        return renderBtn('취소요청하기', onTap: () => _cancelOnTap(model.setleId));
      case "CANCEL_REQUEST":
      case "CANCEL_FAIL":
      case "CANCEL_SUCCESS":
      case "ROLLBACK_REQUEST":
      case "ROLLBACK_SUCCESS":
        return Container();
      default:
        return Container();
    }
  }

  _cancelOnTap(int no) async {
    try {
      final model =
          await Get.find<PaymentController>().paymentCancelRequest(no);

      final sheet = BtnBottomSheet(
        title: model.message!,
        isOneBtn: true,
      );
      await Get.bottomSheet(sheet);
      if (endCallBack != null) endCallBack!();
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }
}
