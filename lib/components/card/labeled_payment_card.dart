import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/card/payment_card.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class LabeledPaymentCard extends StatefulWidget {
  final double amount;
  final String name;
  final String label;

  LabeledPaymentCard({
    required this.amount,
    required this.name,
    required this.label,
  });

  @override
  _LabeledPaymentCardState createState() => _LabeledPaymentCardState();
}

class _LabeledPaymentCardState extends State<LabeledPaymentCard> {
  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        PaymentCard(
          primaryColor: theme.primaryColor,
          amount: widget.amount,
          name: widget.name,
          bgImage: null,
        ),
        Text(
          widget.label,
          style: TextStyle(
            fontSize: theme.fontSize15,
            color: Color(0xFF262626),
          ),
        ),
      ],
    );
  }
}
