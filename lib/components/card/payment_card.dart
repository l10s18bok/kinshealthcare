import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

enum PaymentCardType {
  // 왼쪽 카드만
  cardOnly,
  // 오른쪽 글도 같이 렌더링
  expanded,
}

class PaymentCard extends StatefulWidget {
  final double amount;
  final String name;
  final PaymentCardType type;
  final String? label;
  final ImageProvider<Object>? bgImage;
  final Color primaryColor;

  PaymentCard({
    required this.amount,
    required this.name,
    required this.bgImage,
    required this.primaryColor,
    this.label,
    this.type = PaymentCardType.cardOnly,
  }) : assert(!(type == PaymentCardType.expanded && label == null));

  @override
  _PaymentCardState createState() => _PaymentCardState();
}

class _PaymentCardState extends State<PaymentCard> {
  TextUtils textUtils = TextUtils();

  renderAmountCardAmount() {
    final theme = ThemeFactory.of(context).theme;

    return RichText(
      text: TextSpan(
        text: textUtils.numberToLocalCurrency(
          amount: widget.amount,
        ),
        style: TextStyle(
          fontSize: theme.fontSize22,
          fontWeight: theme.heavyFontWeight,
          color: Color(0xFF4C4C4C),
        ),
        children: [
          TextSpan(
            text: '원',
            style: TextStyle(
              fontSize: theme.fontSize16,
            ),
          ),
        ],
      ),
    );
  }

  renderAmountCardName() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CircleAvatar(
          radius: 7.5,
          backgroundImage: widget.bgImage,
        ),
        Container(width: 4.0),
        Text(
          widget.name,
          style: TextStyle(
            fontSize: theme.fontSize11,
            color: Color(0xFF818181),
          ),
        ),
      ],
    );
  }

  renderAmountCard() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      children: [
        Expanded(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(5.0),
            child: Container(
              decoration: BoxDecoration(
                border: Border(
                  left: BorderSide(color: widget.primaryColor, width: 7.0),
                ),
              ),
              child: Container(
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      renderAmountCardAmount(),
                      Container(height: 4.0),
                      renderAmountCardName(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        if (widget.type == PaymentCardType.expanded)
          Container(
            width: 14.0,
          ),
        if (widget.type == PaymentCardType.expanded)
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.label!,
                  style: TextStyle(
                    fontSize: theme.fontSize11,
                    fontWeight: theme.heavyFontWeight,
                    color: Color(0xFF4C4C4C),
                  ),
                ),
                Text(
                  widget.amount.toString(),
                  style: TextStyle(
                    fontSize: theme.fontSize15,
                    color: Color(0xFF262626),
                  ),
                ),
              ],
            ),
          ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return renderAmountCard();
  }
}
