import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kins_healthcare/components/animation/fade_size_animation.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/payment_user_model.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-01-25
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : PaymentRateUserCard,
 설명 : 결제 비율관리의 List Card
*/

typedef OnFocus = void Function();

// ignore: must_be_immutable
class PaymentRateUserCard extends StatelessWidget {
  PaymentRateUserCard({
    this.isSelect = false,
    required this.model,
    required this.onChanged,
    required this.focus,
    this.onFocus,
    this.animation,
    this.onBtnTap,
    this.height = 140,
  });

  final Animation<double>? animation;
  final PaymentUserModel model;
  final bool isSelect;
  final ValueChanged<String>? onChanged;
  OnFocus? onFocus;
  VoidCallback? onBtnTap;
  FocusNode focus;
  double height;

  @override
  Widget build(BuildContext context) {
    focus.addListener(_onFocusChange);
    if (animation == null) return renderListItem(context);
    return renderFadeSizeAnimation(renderListItem(context));
  }

  renderListItem(BuildContext context) {
    final backColor = isSelect ? kinsBlue14 : kinsWhite;
    final outLineColor = isSelect ? kinsBlue40 : kinsBlueD7;

    return Container(
      width: double.infinity,
      alignment: Alignment.center,
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        border: Border.all(color: outLineColor, width: 1),
        borderRadius: BorderRadius.all(Radius.circular(6.0)),
        color: backColor,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          renderUserInfo(context),
          SizedBox(height: 8),
          renderOutlineTf(context, model.percentage!, outLineColor),
        ],
      ),
    );
  }

  renderUserInfo(BuildContext context) {
    return Row(
      children: [
        CircleNetworkImage(
          path: model.imageUrl,
          imageSize: 45,
        ),
        SizedBox(width: 14),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 2),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  model.nickName ?? '',
                  style: TextStyle(
                    color: kinsBlue40,
                    fontWeight: FontWeight.w700,
                    fontSize: ThemeFactory.of(context).theme.fontSize11,
                  ),
                ),
                SizedBox(height: 7),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      model.name,
                      style: TextStyle(
                        color: kinsBlack3E,
                        fontWeight: FontWeight.w400,
                        fontSize: ThemeFactory.of(context).theme.fontSize15,
                      ),
                    ),
                    Text(
                      model.phoneNum,
                      style: TextStyle(
                        color: kinsBlack3E,
                        fontWeight: FontWeight.w400,
                        fontSize: ThemeFactory.of(context).theme.fontSize15,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        InkWell(
          child: SvgPicture.asset(
            'assets/svgs/ic/ic_minus_payment.svg',
            width: 31,
            height: 31,
          ),
          onTap: onBtnTap,
        )
      ],
    );
  }

  renderNonImage(String profile) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(25.0),
        topRight: Radius.circular(25.0),
      ),
      child: CachedNetworkImage(
        imageUrl: profile,
        fit: BoxFit.cover,
        width: 45,
        height: 45,
      ),
    );
  }

  renderOutlineTf(BuildContext context, String text, Color outLineColor) {
    outLineColor = model.isError == true ? kinsRedDC : outLineColor;

    return Container(
      height: 43,
      padding: EdgeInsets.symmetric(horizontal: 11),
      width: double.infinity,
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        border: Border.all(color: outLineColor, width: 1),
        borderRadius: BorderRadius.all(Radius.circular(6.0)),
        color: kinsWhite,
      ),
      child: Row(
        children: [
          Expanded(child: renderPlainTextField(context, text)),
          model.isError == true ? renderErrorText(context) : renderDefaultText(context),
        ],
      ),
    );
  }

  renderDefaultText(BuildContext context) {
    return Text(
      "%",
      style: TextStyle(
        color: kinsGrayFE,
        fontWeight: FontWeight.w700,
        fontSize: ThemeFactory.of(context).theme.fontSize14,
      ),
      textAlign: TextAlign.right,
    );
  }

  renderErrorText(BuildContext context) {
    return Text(
      "퍼센트 초과",
      style: TextStyle(
        color: kinsRedDC,
        fontWeight: FontWeight.w700,
        fontSize: ThemeFactory.of(context).theme.fontSize14,
      ),
      textAlign: TextAlign.right,
    );
  }

  renderPlainTextField(BuildContext context, String text) {
    return TextField(
      style: TextStyle(
        fontSize: ThemeFactory.of(context).theme.fontSize15,
        color: kinsBlack,
        fontWeight: FontWeight.w400,
      ),
      decoration: InputDecoration(
        counterText: "",
        border: InputBorder.none,
        hintText: text,
        hintStyle: TextStyle(color: kinsGrayB6),
      ),
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      cursorColor: kinsBlack,
      maxLines: 1,
      maxLength: 3,
      focusNode: focus,
      onChanged: onChanged,
    );
  }

  renderFadeSizeAnimation(Widget child) {
    return FadeSizeAnimation(
      controller: animation!,
      child: child,
      containerHeight: height,
    );
  }

  void _onFocusChange() {
    if (onFocus != null) onFocus!();
  }
}
