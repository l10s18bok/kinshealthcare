import 'dart:math';

import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

// ignore: must_be_immutable
class RequestBubbleCard extends StatefulWidget {
  /// USE 가 붙는 파라미터가 쓰실분
  final String store;
  final String address;
  final DateTime paymentDate;
  final int amount;
  final String userNameFrom;
  final String userNameUse;
  final String profileImgFrom;
  final String profileImgUse;

  RequestBubbleCard({
    Key? key,
    this.onAcceptTap,
    this.onRejectTap,
    required this.userNameFrom,
    required this.userNameUse,
    required this.profileImgFrom,
    required this.profileImgUse,
    required this.store,
    required this.address,
    required this.paymentDate,
    required this.amount,
  }) : super(key: key);

  GestureTapCallback? onAcceptTap;
  GestureTapCallback? onRejectTap;

  @override
  _RequestBubbleCardState createState() => _RequestBubbleCardState();
}

class _RequestBubbleCardState extends State<RequestBubbleCard> {
  renderAvatar({
    double containerRadius = 14.0,
    double avatarRadius = 12.0,
    Color? borderColor,
    String? imageUrl,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      width: containerRadius * 2,
      height: containerRadius * 2,
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        border: Border.all(color: theme.primaryOrangeColor, width: 1.0),
      ),
      child: Center(
        child: CircleAvatar(
          radius: avatarRadius,
          backgroundImage: imageUrl != null ? NetworkImage(imageUrl) : null,
        ),
      ),
    );
  }

  renderAcceptButton() {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Material(
          color: theme.primaryColor,
          borderRadius: BorderRadius.circular(7.0),
          child: InkWell(
            onTap: widget.onAcceptTap,
            child: Center(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 30.0,
                  vertical: 7.0,
                ),
                child: Text(
                  '수락',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  renderKeyValue({
    required String key,
    required String value,
    bool isPrimary = false,
  }) {
    final theme = ThemeFactory.of(context).theme;

    final keyStyle = TextStyle(
      fontSize: theme.fontSize12,
      color: Color(0xFF3E3E3E),
      fontWeight: theme.heavyFontWeight,
    );

    final primaryStyle = keyStyle.copyWith(
      color: theme.primaryColor,
      fontSize: theme.fontSize15,
    );

    return Row(
      children: [
        Container(
          width: 60.0,
          child: Text(
            key,
            style: keyStyle,
          ),
        ),
        Text(
          value,
          style: isPrimary
              ? primaryStyle
              : keyStyle.copyWith(
                  fontWeight: theme.primaryFontWeight,
                ),
        ),
      ],
    );
  }

  renderDivider() {
    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Container(
              color: Color(0xFFEEEEEE),
              height: 1.0,
            ),
          ),
        ),
      ],
    );
  }

  renderBubbleHeader() {
    final theme = ThemeFactory.of(context).theme;

    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IntrinsicWidth(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  '사용자',
                  style: TextStyle(
                    fontSize: theme.fontSize10,
                    color: theme.primaryOrangeColor,
                    fontWeight: theme.mediumFontWeight,
                  ),
                ),
                Container(height: 7.0),
                Row(
                  children: [
                    renderAvatar(
                      imageUrl: widget.profileImgUse,
                    ),
                    Container(width: 7.0),
                    Text(
                      widget.userNameUse,
                      style: TextStyle(
                          fontSize: theme.fontSize12, color: Color(0xFF353535)),
                    ),
                  ],
                ),
              ],
            ),
          ),
          // renderAcceptButton(),
        ],
      ),
    );
  }

  renderBody() {
    return Column(
      children: [
        renderKeyValue(
          key: '가맹점',
          value: widget.store,
        ),
        Container(height: 8.0),
        renderKeyValue(
          key: '주소',
          value: widget.address,
        ),
        Container(height: 8.0),
        renderKeyValue(
          key: '결제일',
          value:
              '${widget.paymentDate.year}-${widget.paymentDate.month}-${widget.paymentDate.day} ${widget.paymentDate.hour}:${widget.paymentDate.minute}:${widget.paymentDate.second}',
        ),
        Padding(
          padding: EdgeInsets.only(top: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              renderPrice(),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 16.0),
          child: renderButtons(),
        ),
      ],
    );
  }

  renderButtons() {
    return Row(
      children: [
        Expanded(
          child: PrimaryButton(
            label: '거절',
            padding: EdgeInsets.symmetric(vertical: 10.0),
            bgColor: Color(0xFFE9E9F3),
            txtColor: Colors.black,
            onTap: widget.onRejectTap,
          ),
        ),
        Container(width: 16.0),
        Expanded(
          child: PrimaryButton(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            label: '수락',
            onTap: widget.onAcceptTap,
          ),
        ),
      ],
    );
  }

  renderPrice() {
    final theme = ThemeFactory.of(context).theme;

    return Text(
      TextUtils().numberToLocalCurrency(
            amount: widget.amount.toDouble(),
          ) +
          '원',
      style: TextStyle(
        color: theme.primaryOrangeColor,
        fontSize: theme.fontSize17,
        fontWeight: theme.heavyFontWeight,
      ),
    );
  }

  renderRequester({
    required String name,
    required String time,
  }) {
    final theme = ThemeFactory.of(context).theme;

    final primaryStyle = TextStyle(
      fontSize: theme.fontSize10,
      color: theme.primaryColor,
      fontWeight: theme.mediumFontWeight,
    );

    final normalStyle = TextStyle(
      fontSize: theme.fontSize12,
      color: Color(0xFF353535),
    );

    final greyStyle = TextStyle(
      fontSize: theme.fontSize12,
      color: Color(0xFFA6A6A6),
    );

    return Row(
      children: [
        renderAvatar(
          avatarRadius: 18.0,
          containerRadius: 20.0,
          imageUrl: widget.profileImgFrom,
        ),
        Container(width: 8.0),
        Expanded(
          child: IntrinsicWidth(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  '요청자',
                  style: primaryStyle,
                ),
                Text(
                  widget.userNameFrom,
                  style: normalStyle,
                ),
              ],
            ),
          ),
        ),
        Text(
          time,
          style: greyStyle,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final curlTopWidth = 30.0;
    final curlHeight = 10.0;
    final curlDxOffset = 20.0;

    return Padding(
      // normalize transform
      padding: EdgeInsets.only(bottom: curlHeight),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(left: curlTopWidth + curlDxOffset),
            child: renderRequester(
              time: '방금 전',
              name: '돈잘버는 우리 형',
            ),
          ),
          CustomPaint(
            painter: BubblePainter(
              curlTopWidth: curlTopWidth,
              curlDxOffset: curlDxOffset,
              curlHeight: curlHeight,
            ),
            // normalize paint offset
            child: Transform.translate(
              offset: Offset(
                0.0,
                curlHeight,
              ),
              child: Padding(
                padding: EdgeInsets.all(14.0),
                child: Column(
                  children: [
                    renderBubbleHeader(),
                    renderDivider(),
                    renderBody(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BubblePainter extends CustomPainter {
  final double curlDxOffset;
  final double curlTopWidth;
  final double curlHeight;

  BubblePainter({
    required this.curlDxOffset,
    required this.curlTopWidth,
    required this.curlHeight,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final double height = size.height;
    final double width = size.width;

    // 뾰족 표시 높이
    final curlHeight = this.curlHeight;
    // 뾰족 표시 위 넓이
    final curlTopWidth = this.curlTopWidth;
    // 뾰족 표시 아래 넓이 비율
    final curlWidthPerc = 0.6;
    // 뾰족 표시 시작까지 거리
    final curlDxOffset = this.curlDxOffset;
    // 뾰족 표시 아래 넓이
    final curlBottomWidth = curlTopWidth * curlWidthPerc;

    canvas.translate(0.0, curlHeight);

    final paint = new Paint()
      ..color = Color(0xFFD7D8EE)
      ..strokeWidth = 1.0
      ..style = PaintingStyle.stroke;

    // 왼쪽 위 끝
    final ltP1 = Offset(0, 0);
    // 화살표 시작지점
    final ltP2 = Offset(curlDxOffset, 0);
    // 화살표 끝지점
    final ltP3 = Offset(ltP2.dx + curlBottomWidth, 0);
    // 오른쪽 위 끝
    final rtP1 = Offset(width, 0);
    // 오른쪽 아래 끝
    final rbP1 = Offset(width, height);
    // 왼쪽 아래 끝
    final lbP1 = Offset(0, height);

    // draw bubble point
    final pointPath = new Path()
      ..moveTo(ltP2.dx, 0)
      ..quadraticBezierTo(
        curlTopWidth * 0.3 + ltP2.dx,
        -curlHeight,
        curlTopWidth + ltP2.dx,
        -curlHeight,
      )
      ..quadraticBezierTo(
        (ltP3.dx + ((curlTopWidth - curlBottomWidth) * 0.2)),
        -curlHeight * 0.8,
        ltP3.dx,
        0,
      );

    canvas.drawPath(pointPath, paint);

    final arcRad = 12.0;

    final rtc = Offset(
      rtP1.dx - arcRad,
      rtP1.dy,
    );
    final rbc = Offset(
      rbP1.dx - arcRad,
      rbP1.dy - arcRad,
    );
    final lbc = Offset(
      lbP1.dx,
      lbP1.dy - arcRad,
    );
    final ltc = Offset(
      ltP1.dx,
      ltP1.dy,
    );

    final halfRad = arcRad / 2;

    canvas.drawLine(Offset(ltP1.dx + halfRad, ltP1.dy), ltP2, paint);

    canvas.drawLine(
      ltP3,
      Offset(rtP1.dx - halfRad, ltP3.dy),
      paint,
    );
    canvas.drawArc(rtc & Size(arcRad, arcRad), 0, -pi / 2, false, paint);

    canvas.drawLine(
      Offset(rtP1.dx, rtP1.dy + halfRad),
      Offset(rtP1.dx, rbP1.dy - halfRad),
      paint,
    );
    canvas.drawArc(rbc & Size(arcRad, arcRad), 0, pi / 2, false, paint);

    canvas.drawLine(
      Offset(rbP1.dx - halfRad, rbP1.dy),
      Offset(lbP1.dx + halfRad, lbP1.dy),
      paint,
    );
    canvas.drawArc(lbc & Size(arcRad, arcRad), pi, -pi / 2, false, paint);

    canvas.drawLine(
      Offset(lbP1.dx, lbP1.dy - halfRad),
      Offset(ltP1.dx, ltP1.dy + halfRad),
      paint,
    );
    canvas.drawArc(ltc & Size(arcRad, arcRad), pi, pi / 2, false, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
