import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_keyboard_bottom_sheet.dart';
import 'package:kins_healthcare/services/payment/model/payment_kkakka_list_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

const Color BLUE_COLOR = Color(0xFF6FA7FA);
const Color YELLOW_COLOR = Color(0xFFFDD84F);
const Color GREEN_COLOR = Color(0xFF6CC4BF);
const Color PURPLE_COLOR = Color(0xFF756BFF);
const Color PINK_COLOR = Color(0xFFFF9B95);

class KkakkaPaymentCard extends StatefulWidget {
  final PostPaymentKkakkaListResponse kkakka;
  final ValueSetter<double>? onAmountChange;
  final int? maxAmount;

  KkakkaPaymentCard({
    required this.kkakka,
    this.onAmountChange,
    this.maxAmount,
  });

  @override
  _KkakkaPaymentCardState createState() => _KkakkaPaymentCardState();
}

class _KkakkaPaymentCardState extends State<KkakkaPaymentCard> {
  bool isOpen = false;
  Color primaryColor = BLUE_COLOR;

  double amount = 0.0;

  renderHeader() {
    final theme = ThemeFactory.of(context).theme;

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        if (this.isOpen == true) {
          if (widget.onAmountChange != null) {
            widget.onAmountChange!(0);
            setState(() {
              amount = 0;
            });
          }
        }

        setState(() {
          this.isOpen = !this.isOpen;
        });
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 14.0, vertical: 16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              widget.kkakka.userRelCdOpponent!,
              style: TextStyle(
                fontSize: theme.fontSize14,
                fontWeight: theme.heavyFontWeight,
                color: Colors.white,
              ),
            ),
            Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
              ),
              width: 23.0,
              height: 23.0,
              child: Center(
                child: Icon(
                  this.isOpen
                      ? Icons.keyboard_arrow_up
                      : Icons.keyboard_arrow_down,
                  size: 20.0,
                  color: this.primaryColor,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  renderMainImage() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        children: [
          Expanded(
            child: Container(
              height: 300.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                    widget.kkakka.kkakkaImage!,
                  ),
                  colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.35),
                    BlendMode.srcOver,
                  ),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    textBaseline: TextBaseline.alphabetic,
                    children: [
                      Text(
                        TextUtils().numberToLocalCurrency(
                          amount: widget.kkakka.kkakkaBalance!.toDouble(),
                        ),
                        style: TextStyle(
                          fontSize: theme.fontSize32,
                          color: Colors.white,
                          fontWeight: theme.heavyFontWeight,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 4.0),
                        child: Text(
                          '원',
                          style: TextStyle(
                            fontSize: theme.fontSize20,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '유효기간 ${widget.kkakka.endDateDaysTill}일 남음',
                        style: TextStyle(
                          fontSize: theme.fontSize14,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  renderAmountField() {
    final theme = ThemeFactory.of(context).theme;

    return GestureDetector(
      onTap: () async {
        final resp = await Get.bottomSheet(
          MoneyKeyboardBottomSheet(),
          isScrollControlled: true,
        );

        if (resp is MoneyBottomSheetResult) {
          if (widget.maxAmount != null && resp.amount > widget.maxAmount!) {
            Get.snackbar('한도초과', '총액을 초과할 수 없습니다.');
            return;
          }

          if (resp.amount > widget.kkakka.kkakkaLimit!) {
            Get.snackbar('한도초과', '1회 사용 한도를 초과할 수 없습니다.');
            return;
          }

          if (resp.amount > widget.kkakka.kkakkaBalance!) {
            Get.snackbar('한도초과', '까까 잔고를 초과할 수 없습니다.');
            return;
          }

          setState(() {
            this.amount = resp.amount;
          });

          if (widget.onAmountChange != null) {
            widget.onAmountChange!(resp.amount);
          }
        }
      },
      child: Padding(
        padding:
            EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0, top: 8.0),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8.0),
            border: Border.all(
              color: Color(0xFD6D6D6),
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 14.0, vertical: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  TextUtils().numberToLocalCurrency(amount: this.amount),
                  style: TextStyle(
                    color: this.amount == 0 ? Color(0xFFCACACA) : Colors.black,
                    fontWeight: theme.heavyFontWeight,
                  ),
                ),
                Text(
                  '원',
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  renderLimit() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
      child: Row(
        children: [
          Text(
            '1회 사용한도: ${TextUtils().numberToLocalCurrency(amount: widget.kkakka.kkakkaLimit!.toDouble())}원',
            style: TextStyle(
              color: Colors.white,
              fontWeight: theme.heavyFontWeight,
            ),
          ),
        ],
      ),
    );
  }

  renderBody() {
    return Column(
      children: [
        renderMainImage(),
        renderLimit(),
        renderAmountField(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      decoration: BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.circular(10.0),
      ),
      duration: Duration(milliseconds: 200),
      child: Column(
        children: [
          renderHeader(),
          if (this.isOpen) renderBody(),
        ],
      ),
    );
  }
}
