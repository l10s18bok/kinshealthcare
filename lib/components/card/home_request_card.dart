import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class HomeRequestCard extends StatefulWidget {
  final String title;
  final String subTitle;
  final String iconPath;
  final GestureTapCallback? onTap;
  final GestureTapCallback? onPlusTap;
  final bool isPrimaryColor;

  HomeRequestCard({
    required String title,
    required String subTitle,
    required String iconPath,
    bool isPrimaryColor = false,
    GestureTapCallback? onTap,
    GestureTapCallback? onPlusTap,
  })  : this.title = title,
        this.subTitle = subTitle,
        this.iconPath = iconPath,
        this.isPrimaryColor = isPrimaryColor,
        this.onTap = onTap,
        this.onPlusTap = onPlusTap;

  @override
  _HomeRequestCardState createState() => _HomeRequestCardState();
}

class _HomeRequestCardState extends State<HomeRequestCard> {
  renderIcon() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      width: 34.0,
      height: 34.0,
      decoration: BoxDecoration(
        color: widget.isPrimaryColor
            ? Color(0xFFFFFFFF).withOpacity(0.29)
            : theme.primaryGreyBgColor,
        borderRadius: BorderRadius.circular(
          6.0,
        ),
      ),
      child: Padding(
        padding: EdgeInsets.all(7.0),
        child: SvgPicture.asset(
          widget.iconPath,
        ),
      ),
    );
  }

  renderText() {
    final theme = ThemeFactory.of(context).theme;

    Color color = Colors.black;

    if (widget.isPrimaryColor) {
      color = Colors.white;
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.title,
          style: TextStyle(
            color: color,
            fontSize: theme.fontSize12,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
        Text(
          widget.subTitle,
          style: TextStyle(
            color: color,
            fontSize: theme.fontSize10,
          ),
        ),
      ],
    );
  }

  renderPlusButton() {
    if (widget.onPlusTap != null) {
      return Positioned(
        top: 0,
        right: 0,
        child: FractionalTranslation(
          translation: Offset(0, -0.5),
          child: GestureDetector(
            onTap: widget.onPlusTap,
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_plus_main.svg',
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    final bgColor =
        widget.isPrimaryColor ? theme.primaryColor : theme.secondaryGreyBgColor;

    final borderRadius = BorderRadius.circular(
      6.0,
    );

    return Stack(
      children: [
        Ink(
          decoration: BoxDecoration(
            borderRadius: borderRadius,
            color: bgColor,
          ),
          child: InkWell(
            borderRadius: borderRadius,
            onTap: widget.onTap,
            child: Padding(
              padding: EdgeInsets.all(
                7.0,
              ),
              child: Row(
                children: [
                  renderIcon(),
                  Container(width: 8.0),
                  renderText(),
                ],
              ),
            ),
          ),
        ),
        renderPlusButton(),
      ],
    );
  }
}
