import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class JobPostCard extends StatefulWidget {
  final GestureTapCallback? onFavouriteTap;
  final GestureTapCallback? onShareTap;
  final GestureTapCallback? onTap;

  JobPostCard({
    @required this.onFavouriteTap,
    @required this.onShareTap,
    @required this.onTap,
  });

  @override
  _JobPostCardState createState() => _JobPostCardState();
}

class _JobPostCardState extends State<JobPostCard> {
  renderThumbnail() {
    return Expanded(
      flex: 1,
      child: AspectRatio(
        aspectRatio: 1,
        child: Container(
          color: Colors.grey[200],
        ),
      ),
    );
  }

  renderBody() {
    final theme = ThemeFactory.of(context).theme;

    return Expanded(
      flex: 2,
      child: Padding(
        padding: EdgeInsets.only(left: 12.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '서울 삼성병원',
                  style: TextStyle(fontSize: 14.0),
                ),
                Row(
                  children: [
                    InkWell(
                      onTap: widget.onFavouriteTap,
                      child: SizedBox(
                        height: 30,
                        width: 30,
                        child: SvgPicture.asset('assets/svgs/ic/ic_heart_unselect.svg'),
                      ),
                    ),
                    InkWell(
                      onTap: widget.onShareTap,
                      child: SizedBox(
                        height: 30,
                        width: 30,
                        child: SvgPicture.asset('assets/svgs/ic/ic_share.svg'),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              children: [
                IntrinsicWidth(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        '신입/경력직 채용',
                        style: TextStyle(
                          fontSize: theme.fontSize12,
                          color: Color(0xFF727272),
                          fontWeight: theme.heavyFontWeight,
                        ),
                      ),
                      Container(height: 6.0),
                      Row(
                        children: [
                          Text(
                            '서울전체 | 학력무관 | 정규직',
                            style: TextStyle(
                              fontSize: theme.fontSize11,
                              color: Color(0xFF727272),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: theme.primaryColor,
                      width: 1.0,
                    ),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(2.0),
                    child: Text(
                      '2020.11.30일까지',
                      style: TextStyle(
                        fontSize: theme.fontSize9,
                        color: theme.primaryColor,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      behavior: HitTestBehavior.opaque,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6.0),
          border: Border.all(color: Color(0xFFD7D8EE), width: 1.0),
        ),
        child: Padding(
          padding: EdgeInsets.all(12.0),
          child: IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                renderThumbnail(),
                renderBody(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
