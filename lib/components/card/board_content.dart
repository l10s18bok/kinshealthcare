import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/enums/screen_enums.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/ok_bottom_sheet.dart';
import 'package:kins_healthcare/controllers/comm_controller.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/local_board_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

import '../../utils/resource.dart';

/*
 작성일 : 2021-02-16
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : BoardContent,
 설명 : 게시판 형태의 펼쳐졌을 때 표시되는 Content Widget, Controller 를 통해서 직접 데이터를 받아서 표시한다.
*/

class BoardContent extends StatefulWidget {
  final LocalBoardModel model;
  final BoardType type;

  const BoardContent({
    Key? key,
    required this.model,
    required this.type,
  }) : super(key: key);

  @override
  BoardContentState createState() => BoardContentState();
}

class BoardContentState extends State<BoardContent> {
  late CommController commController;
  String? content;

  @override
  void initState() {
    super.initState();
    commController = Get.find<CommController>();

    switch (widget.type) {
      case BoardType.NOTICE:
        _getBoardContent('noticeOne');
        break;
      case BoardType.INQUIRY:
        content = widget.model.content ?? '';
        break;
      case BoardType.FAQ:
        _getBoardContent('faqOne');
        break;
      case BoardType.INFO:
        _getBoardContent('informationOne');
        break;
    }
  }

  _getBoardContent(String id) async {
    try {
      final result = await commController.postBoardOne(id, widget.model.ids!);
      content = result.data.values.content;

      setState(() {});
    } on DioError catch (e) {
      final msg = e.response!.data;
      final sheet = OkBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (content == null) return renderIndicator();
    if (BoardType.INQUIRY == widget.type) return renderQuestionContent();
    return renderContent();
  }

  renderContent() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 24, horizontal: 16),
      color: kinsGray3F,
      child: Text(
        content ?? '',
        style: TextStyle(
          color: kinsGray8A,
          fontWeight: FontWeight.w400,
          fontSize: ThemeFactory.of(context).theme.fontSize11,
        ),
      ),
    );
  }

  renderQuestionContent() {
    final isAnswer = widget.model.answer != null;

    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 24, horizontal: 16),
      color: kinsGray3F,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.model.content!,
            style: TextStyle(
              color: kinsGray8A,
              fontWeight: FontWeight.w400,
              fontSize: ThemeFactory.of(context).theme.fontSize11,
            ),
          ),
          if (isAnswer) SizedBox(height: 16),
          if (isAnswer) Container(height: 1, color: kinsGrayC2),
          if (isAnswer) SizedBox(height: 16),
          if (isAnswer) renderAnswerText(),
        ],
      ),
    );
  }

  Widget renderAnswerText() {
    return Text(
      widget.model.answer!,
      style: TextStyle(
        color: kinsGray8A,
        fontWeight: FontWeight.w400,
        fontSize: ThemeFactory.of(context).theme.fontSize11,
      ),
    );
  }

  Widget renderIndicator() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(vertical: 24, horizontal: 16),
      width: double.infinity,
      color: kinsGray3F,
      child: SizedBox(
        width: 50,
        height: 50,
        child: CircularProgressIndicator(),
      ),
    );
  }
}
