import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/card/request_approval_status_card.dart';
import 'package:kins_healthcare/services/payment/model/payment_payment_request_status.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

class NewApprovalStatusCard extends StatefulWidget {
  final dynamic _model;

  NewApprovalStatusCard({
    required dynamic model,
  })   : assert(model.runtimeType == PaymentRequestStatusResponseMe ||
            model.runtimeType == PaymentRequestStatusResponseRequest),
        this._model = model;

  @override
  _NewApprovalStatusCardState createState() => _NewApprovalStatusCardState();
}

class _NewApprovalStatusCardState extends State<NewApprovalStatusCard> {
  late final isProfile;
  late final String imageUrl;
  late final String label;
  late final String amount;
  late final StatusTypes statusType;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    final model = widget._model;

    final TextUtils textUtils = TextUtils();

    if (model is PaymentRequestStatusResponseMe) {
      isProfile = false;
      imageUrl = model.kkakkaProfileImg!;
      if (model.setlePaymentType == 'KKAKKA') {
        label = '${model.kkakkaUserName}가 준 까까';
      } else {
        label = '${model.userPaymentWayCompany}';
      }
      amount =
          textUtils.numberToLocalCurrency(amount: model.amount!.toDouble()) +
              '원';
      if (model.isApproved) {
        statusType = StatusTypes.approved;
      } else if (model.isRejected) {
        statusType = StatusTypes.rejected;
      } else {
        statusType = StatusTypes.pending;
      }
    } else if (model is PaymentRequestStatusResponseRequest) {
      isProfile = true;
      imageUrl = model.profileImgTo;
      label = '${model.userNameTo}';
      amount =
          textUtils.numberToLocalCurrency(amount: model.amount.toDouble()) +
              '원';
      if (model.isApproved) {
        statusType = StatusTypes.approved;
      } else if (model.isRejected) {
        statusType = StatusTypes.rejected;
      } else {
        statusType = StatusTypes.pending;
      }
    }
  }

  renderImage() {
    final theme = ThemeFactory.of(context).theme;

    if (isProfile) {
      return CircleAvatar(
        radius: 25.0,
        backgroundImage: NetworkImage(
          imageUrl,
        ),
      );
    } else {
      return Container(
        width: 50.0,
        height: 32.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.0),
          color: theme.primaryColor,
          image: DecorationImage(
            image: NetworkImage(
              this.imageUrl,
            ),
            fit: BoxFit.cover,
          ),
        ),
      );
    }
  }

  renderText() {
    final theme = ThemeFactory.of(context).theme;

    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            this.label,
            style: TextStyle(
              fontSize: theme.fontSize14,
            ),
          ),
          Text(
            this.amount,
            style: TextStyle(
              fontSize: theme.fontSize18,
              fontWeight: theme.heavyFontWeight,
            ),
          ),
        ],
      ),
    );
  }

  renderChip() {
    final theme = ThemeFactory.of(context).theme;

    String text = '대기중';
    Color bgColor = theme.primaryOrangeColor;
    Color txtColor = Colors.white;

    if (this.statusType == StatusTypes.approved) {
      text = '승인됨';
      bgColor = theme.primaryColor;
      txtColor = Colors.white;
    } else if (this.statusType == StatusTypes.rejected) {
      text = '거절됨';
      bgColor = Color(0xFFDEDEDF);
      txtColor = Color(0xFFABABAB);
    }

    return Container(
      decoration: BoxDecoration(
        color: bgColor,
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 13.0, vertical: 5.0),
        child: Text(
          text,
          style: TextStyle(
            color: txtColor,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        renderImage(),
        Container(width: 18.0),
        renderText(),
        renderChip(),
      ],
    );
  }
}
