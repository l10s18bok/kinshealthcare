import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-03-30
 작성자 : Mark,
 화면명 :
 경로 :
 클래스 : AddCreditCard,
 설명 : Card 추가하기를 이동하기 위햇 표시해주는 Card
*/

class AddCreditCard extends StatelessWidget {
  final String title;
  final String content;

  AddCreditCard({
    this.title = '계좌 추가',
    this.content = '계좌는 최대 3개까지 등록할수 있어요.',
  });

  @override
  Widget build(BuildContext context) {
    return DottedBorder(
      dashPattern: [4, 4],
      color: kinsGrayE1,
      strokeWidth: 2,
      radius: Radius.circular(6),
      child: renderMainContainer(context),
    );
  }

  Widget renderMainContainer(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: kinsGrayF7F,
      ),
      height: 120,
      width: 220,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          renderLogo(),
          renderTitle(context),
          renderContent(context),
        ],
      ),
    );
  }

  Widget renderLogo() {
    return SvgPicture.asset(
      'assets/svgs/ic/ic_plus_payment.svg',
      width: 36,
      height: 36,
    );
  }

  Widget renderTitle(BuildContext context) {
    return Text(
      title,
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: ThemeFactory.of(context).theme.fontSize17,
        fontWeight: FontWeight.w700,
        color: kinsBlack44,
      ),
    );
  }

  Widget renderContent(BuildContext context) {
    return Text(
      content,
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: ThemeFactory.of(context).theme.fontSize9,
        color: kinsGray90,
      ),
    );
  }
}
