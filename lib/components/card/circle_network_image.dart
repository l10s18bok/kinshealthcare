import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kins_healthcare/screens/auth/register/profile/profile_detail_screen.dart';

/*
 작성일 : 2021-03-04
 작성자 : Mark,
 화면명 : ,
 경로 : ,
 클래스 : CircleNetworkImage,
 설명 : 네트워크에서 이미지를 받아오는 동그란 이미지 위젯
*/

class CircleNetworkImage extends StatelessWidget {
  final double imageSize;
  final String? path;
  final int? userNo;
  final String? heroTag;

  CircleNetworkImage({
    required this.imageSize,
    this.path,
    this.userNo,
    this.heroTag,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: renderImage(),
      onTap: () => _userOnTap(context),
    );
  }

  Widget renderImage() {
    final pathData = path;

    if (pathData == null) return renderNonImage();
    if (!pathData.startsWith('http')) return renderNonImage();

    final tag = this.heroTag;
    if (userNo == null) return renderHttpImage(pathData);
    if (tag == null) return renderHttpImage(pathData);

    return Hero(
      tag: tag,
      child: renderHttpImage(pathData),
    );
  }

  Widget renderNonImage() {
    return SizedBox(
      height: imageSize,
      width: imageSize,
      child: SvgPicture.asset(
        'assets/svgs/ic/ic_profile_signin.svg',
        fit: BoxFit.cover,
      ),
    );
  }

  Widget renderHttpImage(String path) {
    return CachedNetworkImage(
      imageUrl: path,
      imageBuilder: (context, imageProvider) => Container(
        width: imageSize,
        height: imageSize,
        decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.circular(imageSize / 2),
          image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
        ),
      ),
      //placeholder: (context, url) => CircularProgressIndicator(),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }

  _userOnTap(BuildContext context) {
    final userNo = this.userNo;
    final url = this.path;
    if (userNo == null) return;
    if (url == null) return;

    final widget = ProfileDetailScreen(
      userRelNo: userNo,
      tagUrl: url,
      heroTag: heroTag,
    );

    Navigator.of(context).push(
      PageRouteBuilder(
        pageBuilder: (_, animation, secondaryAnimation) => widget,
        transitionsBuilder: (_, animation, secondaryAnimation, child) => child,
      ),
    );
  }
}
