import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'non_kkakka_card.dart';

/*
 작성일 : 2021-03-04
 작성자 : Mark,
 화면명 : ,
 경로 : ,
 클래스 : RadiusNetworkImage,
 설명 : 네트워크에서 이미지를 라운드 처리하는 Component
*/

class RadiusNetworkImage extends StatelessWidget {
  final String? path;
  final double imageHeight;
  final double imageWidth;
  final double radius;

  RadiusNetworkImage({
    this.path,
    required this.imageHeight,
    required this.imageWidth,
    required this.radius,
  });

  @override
  Widget build(BuildContext context) {
    return renderImage();
  }

  Widget renderImage() {
    final pathData = path;

    if (pathData == null) return renderNonImage();
    if (!pathData.startsWith('http')) return renderNonImage();
    return renderHttpImage(pathData);
  }

  Widget renderNonImage() {
    return Container(
      height: imageHeight,
      width: imageWidth,
      alignment: Alignment.center,
      child: NonKkakkaCard(title: '이미지가 없습니다.'),
    );
  }

  Widget renderHttpImage(String path) {
    return CachedNetworkImage(
      imageUrl: path,
      imageBuilder: (context, imageProvider) => Container(
        width: imageWidth,
        height: imageHeight,
        decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.circular(radius),
          image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
        ),
      ),
      //placeholder: (context, url) => CircularProgressIndicator(),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}
