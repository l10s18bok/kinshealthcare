import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/text/is_text_overflow.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/models/local/like_reply_model.dart';
import 'package:kins_healthcare/models/local/post_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

// ignore: must_be_immutable
class LikeReplyCard extends StatelessWidget {
  LikeReplyModel model;

  String type;
  late BuildContext context;
  GestureTapCallback? onMoreTap;
  GestureTapCallback? onLikeTap;

  LikeReplyCard({
    required this.model,
    required this.type,
    this.onMoreTap,
    this.onLikeTap,
  });

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return Column(
      children: [
        type == 'PaintContents' ? renderLikeRow() : Container(),
        SizedBox(height: 2),
        renderContentRow(),
        SizedBox(height: 6),
        renderReply(),
      ],
    );
  }

  Widget renderLikeRow() {
    var unlikePath = 'assets/svgs/ic/ic_heart_unselect.svg';
    var likePath = 'assets/svgs/ic/ic_heart_like.svg';
    var userinfo = Get.find<UserController>().userInfo;
    return GestureDetector(
      onTap: () {},
      child: Center(
        child: Row(
          children: [
            Container(
              width: 32,
              height: 32,
              child: InkWell(
                child: SvgPicture.asset(
                  model.likeYn == 0 ? unlikePath : likePath,
                ),
                onTap: () async => onLikeTap!(),
              ),
            ),
            SizedBox(
              width: 5,
            ),
            GestureDetector(
              onTap: () {
                Get.toNamed('/main-home/request/like',
                    arguments: model.localModel);
              },
              child: model.likeCount == -1
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        renderLikeText('처음으로', false),
                        renderLikeText('\'좋아요\'', true),
                        renderLikeText('를 눌러보세요.', false),
                      ],
                    )
                  : model.likeCount == 0 && model.likeYn == 1
                      ? renderLikeText(
                          '${userinfo!.lastname + userinfo.firstname} 님이 좋아합니다',
                          false)
                      : model.likeCount == 0 && model.likeYn == 0
                          ? renderLikeText(
                              '${model.firstLikeName} 님이 좋아합니다', false)
                          : Row(
                              children: [
                                renderLikeText(
                                    '${model.firstLikeName} 님 외', false),
                                renderLikeText('${model.likeCount!}', true),
                                renderLikeText('명이 좋아합니다.', false),
                              ],
                            ),
            )
          ],
        ),
      ),
    );
  }

  replayColum() {
    return model.firstReplImg != null
        ? GestureDetector(
            onTap: () {
              Get.toNamed('/main-home/request/reply',
                  arguments: model.localModel);
            },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(
                  radius: 9.0,
                  backgroundImage: NetworkImage(model.firstReplImg!),
                ),
                SizedBox(
                  width: 4,
                ),
                Text(
                  model.firstReplName!,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                ),
                SizedBox(
                  width: 6,
                ),
                Expanded(
                  child: Text(
                    model.firstRepl!,
                    maxLines: 5,
                    style: TextStyle(fontSize: 12),
                  ),
                )
              ],
            ),
          )
        : Container();
  }

  Widget renderLikeText(String title, bool isBold) {
    return Text(
      title,
      style: TextStyle(
        color: kinsBlack,
        fontSize: ThemeFactory.of(context).theme.fontSize13,
        fontWeight: isBold ? FontWeight.w700 : FontWeight.w400,
      ),
    );
  }

  Widget renderContentRow() {
    final message = model.message;
    final isContentOpen = model.isContentOpen ?? false;
    final width = MediaQuery.of(context).size.width * .8;
    if (message == null) return Container();

    return GestureDetector(
      child: Row(
        children: [
          Expanded(
            child: Text(
              message,
              maxLines: isContentOpen ? 5 : 1,
              overflow:
                  isContentOpen ? TextOverflow.visible : TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: ThemeFactory.of(context).theme.fontSize11,
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
          Text(
            isTextOverFlow(
                    context: context,
                    width: width,
                    message: message,
                    fontSize: 13)
                ? isContentOpen
                    ? ''
                    : '더 보기'
                : '',
            style: TextStyle(
              color: kinsGray9B,
              fontSize: ThemeFactory.of(context).theme.fontSize11,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
      onTap: onMoreTap,
    );
  }

  Widget renderReply() {
    final localModel = model.localModel;

    return InkWell(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          renderReplyContent(),
          SizedBox(height: 4),
          Text(
            '댓글 쓰기...',
            style: TextStyle(
              color: kinsBlue40,
              fontSize: ThemeFactory.of(context).theme.fontSize11,
            ),
          ),
        ],
      ),
      onTap: () {
        if (localModel == null) return;

        Get.toNamed('/main-home/request/reply', arguments: localModel);
      },
    );
  }

  Widget renderReplyContent() {
    final replyName = model.firstReplName;
    if (replyName == null) return Container();

    final replyContent = model.firstRepl;
    final path = model.firstReplImg;

    return Container(
      padding: EdgeInsets.only(top: 7),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          renderUserImage(path, 20),
          SizedBox(width: 4),
          Text(
            replyName,
            style: TextStyle(
              fontSize: ThemeFactory.of(context).theme.fontSize12,
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(width: 6),
          Expanded(
            child: Text(
              replyContent ?? '',
              maxLines: 5,
              style: TextStyle(
                fontSize: ThemeFactory.of(context).theme.fontSize12,
              ),
            ),
          ),
        ],
      ),
    );
  }

  String? numberWithComma(int? param) {
    if (param == null) return null;
    return new NumberFormat('###,###,###,###')
        .format(param)
        .replaceAll(' ', '');
  }

  Widget renderUserImage(String? path, double size) {
    if (path == null || !path.startsWith('http'))
      return renderNoUserImage(size);

    return CircleNetworkImage(
      imageSize: size,
      path: path,
    );
  }

  Widget renderNoUserImage(double size) {
    return SvgPicture.asset(
      'assets/svgs/ic/ic_profile_signin.svg',
      width: size,
      height: size,
    );
  }
}
