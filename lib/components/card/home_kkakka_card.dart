import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/models/kkakka_model.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

enum HomeKkakkaCardType {
  /// 받은 까까
  received,

  /// 발급해준 까까
  issued,
}

class HomeKkakkaCard extends StatefulWidget {
  final KkakkaModel kkakka;
  final HomeKkakkaCardType type;
  final bool isDetail;
  final dynamic heroTag;

  HomeKkakkaCard({
    required this.kkakka,
    this.type = HomeKkakkaCardType.received,
    this.isDetail = false,
    this.heroTag,
  });

  @override
  _HomeKkakkaCardState createState() => _HomeKkakkaCardState();
}

class _HomeKkakkaCardState extends State<HomeKkakkaCard> {
  String? iconPath;

  @override
  initState() {
    super.initState();
  }

  renderCommonBorder() {
    final theme = ThemeFactory.of(context).theme;

    return BorderSide(
      width: 2.0,
      color: theme.primaryGreyColor,
    );
  }

  renderOptions() {
    return SvgPicture.asset(
      'assets/svgs/ic/ic_more.svg',
    );
  }

  renderTopInfo() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            SvgPicture.asset(
              'assets/svgs/ic/${widget.kkakka.kkakkaIcName}',
            ),
            Padding(
              padding: EdgeInsets.only(left: 6.0),
              child: Text(
                widget.kkakka.kkakkaTypeKr,
                style: TextStyle(
                  fontSize: theme.fontSize12,
                  fontWeight: theme.heavyFontWeight,
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Text(
              widget.kkakka.daysTillEndDate != null
                  ? '${widget.kkakka.daysTillEndDate} 전'
                  : '사용기한 미설정',
              style: TextStyle(
                fontSize: theme.fontSize12,
              ),
            ),
            // if (widget.isDetail) renderOptions(),
          ],
        ),
      ],
    );
  }

  getPrimaryColor() {
    Color color = widget.kkakka.primaryColor;

    return color;
  }

  renderBody() {
    final theme = ThemeFactory.of(context).theme;

    final greyColor = Color(0xFF7D7D7D);

    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (widget.kkakka.kkakkaImage != null)
            Padding(
              padding: EdgeInsets.only(right: 14.0),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.circular(
                    8.0,
                  ),
                ),
                width: 65.0,
                height: 65.0,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(
                    8.0,
                  ),
                  child: CachedNetworkImage(
                    imageUrl: widget.kkakka.kkakkaImage!,
                    fit: BoxFit.cover,
                    errorWidget: (_, __, ___) => Container(),
                  ),
                ),
              ),
            ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                children: [
                  Text(
                    widget.type == HomeKkakkaCardType.issued
                        ? widget.kkakka.kkakkaPriceDisplay
                        : widget.kkakka.kkakkaBalanceDisplay,
                    style: TextStyle(
                      fontSize: theme.fontSize22,
                      fontStyle: FontStyle.italic,
                      fontWeight: theme.heavyFontWeight,
                    ),
                  ),
                  Text(
                    '원',
                    style: TextStyle(
                      fontSize: theme.fontSize22,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                      right: 6.0,
                    ),
                    child: CircleAvatar(
                      radius: 9.0,
                      backgroundImage: widget.kkakka.profileImgOpponent == null
                          ? null
                          : NetworkImage(
                              widget.kkakka.profileImgOpponent!,
                            ),
                    ),
                  ),
                  Text(
                    '${widget.kkakka.userNameOpponent}',
                    style: TextStyle(
                      color: greyColor,
                      fontSize: theme.fontSize10,
                    ),
                  ),
                  Text(
                    ' · ',
                    style: TextStyle(
                      color: greyColor,
                      fontSize: theme.fontSize10,
                    ),
                  ),
                  Text(
                    '1회한도 ${widget.kkakka.kkakkaLimitDisplay}원',
                    style: TextStyle(
                      color: greyColor,
                      fontSize: theme.fontSize10,
                    ),
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  renderHeroWrapper({
    required Widget child,
  }) {
    if (widget.heroTag != null) {
      return Hero(
        tag: widget.heroTag,
        child: Material(
          type: MaterialType.transparency,
          child: child,
        ),
      );
    } else {
      return child;
    }
  }

  renderBottomDetail() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      child: Column(
        children: [
          Container(height: 8.0),
          Divider(color: Color(0xFFE8E8E8)),
          Container(height: 12.0),
          Row(
            children: [
              Expanded(
                child: Text(
                  widget.kkakka.kkakkaMessage ?? '',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: theme.fontSize12,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  renderDeactivatedStamp() {
    if (widget.kkakka.kkakkaStatus == KkakkaStatus.DROP) {
      return Positioned.fill(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.white,
                ),
                shape: BoxShape.circle,
              ),
              child: Padding(
                padding: EdgeInsets.all(12.0),
                child: Icon(
                  Icons.timer,
                  color: Colors.white,
                  size: 80.0,
                ),
              ),
            ),
            Container(height: 12.0),
            Text(
              '사용 기간 만료',
              style: TextStyle(
                color: Colors.white,
                fontSize: 12.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  renderSlideSecondaryActions() {
    return [
      IconSlideAction(
        caption: '추가하기',
        iconWidget: Padding(
          padding: EdgeInsets.only(bottom: 4.0),
          child: SvgPicture.asset(
            'assets/svgs/ic/ic_plus_main.svg',
          ),
        ),
        onTap: () {},
      ),
      IconSlideAction(
        caption: '삭제하기',
        iconWidget: Padding(
          padding: EdgeInsets.only(bottom: 4.0),
          child: Container(
            width: 32.0,
            height: 32.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.red,
            ),
            child: Icon(
              Icons.delete_forever,
              size: 18.0,
              color: Colors.white,
            ),
          ),
        ),
        onTap: () {},
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    final borderRadius = BorderRadius.circular(4.0);

    final overlayColor = widget.kkakka.kkakkaStatus == KkakkaStatus.DROP
        ? Colors.black.withOpacity(0.74)
        : Colors.transparent;

    return ClipRRect(
      borderRadius: borderRadius,
      child: Material(
        color: Colors.white,
        borderRadius: borderRadius,
        child: InkWell(
          onTap: () {},
          borderRadius: borderRadius,
          child: renderHeroWrapper(
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    border: Border(
                      left: BorderSide(
                        color: getPrimaryColor(),
                        width: 8.0,
                      ),
                    ),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border(
                        top: renderCommonBorder(),
                        right: renderCommonBorder(),
                        bottom: renderCommonBorder(),
                      ),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(
                        12.0,
                      ),
                      child: Column(
                        children: [
                          renderTopInfo(),
                          Container(height: 21),
                          renderBody(),
                          if (widget.isDetail) renderBottomDetail(),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned.fill(
                  child: Container(
                    decoration: BoxDecoration(
                      color: overlayColor,
                    ),
                  ),
                ),
                renderDeactivatedStamp(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
