import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class PaymentStatusCard extends StatelessWidget {
  final bool isFail;
  final String text;

  PaymentStatusCard({
    required this.isFail,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Container(
        decoration: BoxDecoration(
          color: !isFail ? Color(0xFFFFF5D3) : Color(0xFFFFE7E7),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 14.0),
          child: Row(
            children: [
              SvgPicture.asset(
                'assets/svgs/ic/ic_bell.svg',
              ),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: Text(
                  text,
                  style: TextStyle(
                    fontSize: theme.fontSize13,
                    fontWeight: theme.heavyFontWeight,
                    color: isFail ? Color(0xFFFF4949) : Colors.black,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
