import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

enum FamilyRegisterCardVariant {
  smallAvatar,
  largeAvatar,
}

enum FamilyRegisterCardButtonType {
  cancelRequest,
  requestFamily,
  none,
}

/*
 * 작성일 : 2021-02-08
 * 작성자 : JC
 * 화면명 : HY_2002
 * 주요기능 : 가족선택 및 등록
 */
class FamilyRegisterCard extends StatefulWidget {
  /// 종류 [FamilyRegisterCardVariant.smallAvatar] 는 작은 아바타
  /// [FamilyRegisterCardVariant.largeAvatar] 는 큰 아바타
  final FamilyRegisterCardVariant variant;

  /// [FamilyRegisterCardButtonType.cancelRequest] 는 요청 취소 버튼을 렌더
  /// [FamilyRegisterCarddButtonType.requestFamily] 는 가족 요청 버튼을 렌더
  final FamilyRegisterCardButtonType buttonType;

  /// 이름
  final String name;

  /// 전화번호
  final String phone;

  /// 콜백
  final GestureTapCallback? onTap;

  /// 아바타 배경 색깔
  final Color? avatarBgColor;

  /// 아바타 이미지 URL
  final String? avatarImgUrl;

  FamilyRegisterCard({
    required this.name,
    required this.phone,
    this.onTap,
    this.variant = FamilyRegisterCardVariant.largeAvatar,
    this.buttonType = FamilyRegisterCardButtonType.requestFamily,
    this.avatarBgColor,
    this.avatarImgUrl,
  });

  @override
  _FamilyRegisterCardState createState() => _FamilyRegisterCardState();
}

class _FamilyRegisterCardState extends State<FamilyRegisterCard> {
  renderAvatar() {
    if (widget.variant == FamilyRegisterCardVariant.largeAvatar) {
      return CircleAvatar(
        radius: 28.0,
        child: widget.avatarImgUrl != null
            ? null
            : Text(
                widget.name.substring(
                  0,
                  1,
                ),
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
        backgroundColor: widget.avatarBgColor,
        backgroundImage: widget.avatarImgUrl == null
            ? null
            : NetworkImage(
                widget.avatarImgUrl!,
              ),
      );
    } else {
      return CircleAvatar(
        radius: 20.0,
        backgroundImage: widget.avatarImgUrl == null
            ? null
            : NetworkImage(
                widget.avatarImgUrl!,
              ),
      );
    }
  }

  renderText() {
    final theme = ThemeFactory.of(context).theme;

    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.name,
            style: TextStyle(
              fontSize: theme.fontSize14,
              fontWeight: theme.heavyFontWeight,
            ),
          ),
          Text(
            widget.phone,
            style: TextStyle(
              fontSize: theme.fontSize12,
            ),
          ),
        ],
      ),
    );
  }

  renderButton() {
    final theme = ThemeFactory.of(context).theme;

    String text = '요청취소';
    Color bgColor = Colors.white;
    Color borderColor = Color(0xFFE1E1E1);

    TextStyle style = TextStyle(
      fontSize: theme.fontSize11,
      color: Color(0xFF6D6D6D),
    );

    if (widget.buttonType == FamilyRegisterCardButtonType.requestFamily) {
      text = '가족 요청';
      style = style.copyWith(
        color: Colors.white,
        fontWeight: theme.heavyFontWeight,
      );
      bgColor = theme.primaryColor;
      borderColor = theme.primaryColor;
    }

    if (widget.buttonType == FamilyRegisterCardButtonType.none) {
      return Container();
    }

    return Material(
      borderRadius: BorderRadius.circular(4.0),
      color: bgColor,
      child: InkWell(
        onTap: widget.onTap,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4.0),
            color: bgColor,
            border: Border.all(
              color: borderColor,
              width: 1.0,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 10.0),
            child: Text(
              text,
              style: style,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        renderAvatar(),
        Container(width: 14.0),
        renderText(),
        renderButton(),
      ],
    );
  }
}
