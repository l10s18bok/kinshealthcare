import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/btn_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/simple_information_bottom_sheet.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/dialog/kkakka_send_dialog.dart';
import 'package:kins_healthcare/controllers/introduce_controller.dart';
import 'package:kins_healthcare/controllers/kkakka_controller.dart';
import 'package:kins_healthcare/controllers/suggest_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/models/local/local_kkakka_model.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/screens/timeline/suggest/suggest_profile_screen.dart';
import 'package:kins_healthcare/services/suggest/model/suggest_detail_model.dart';
import 'package:kins_healthcare/services/timeline/model/timeline_list_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:url_launcher/url_launcher.dart';

/*
 작성일 : 2021-03-19
 작성자 : Mark,
 화면명 : ,
 경로 : ,
 클래스 : TimelineBubbleCard,
 설명 : 타임라인의 Item 을 표시하기 위한 Card
*/

// ignore: must_be_immutable
class TimelineBubbleCard extends StatefulWidget {
  TimelineListModel model;

  TimelineBubbleCard({
    required this.model,
  });

  @override
  _TimelineBubbleCardState createState() => _TimelineBubbleCardState();
}

class _TimelineBubbleCardState extends State<TimelineBubbleCard> {
  final pointWidth = 13.0, pointHeight = 19.0, radius = 7.0;
  final padding = 14.0;
  late TimelineListModel model;

  @override
  void initState() {
    super.initState();
    model = widget.model;
  }

  @override
  Widget build(BuildContext context) {
    bool isLeftPointer = model.meYn == 'N';

    return CustomPaint(
      painter: BubblePainter(
        pointHeight: pointHeight,
        pointWidth: pointWidth,
        isLeftPointer: isLeftPointer,
        color: isLeftPointer ? kinsBlueE2 : kinsGrayE2E,
        radius: radius,
      ),
      child: renderBody(isLeftPointer),
    );
  }

  Widget renderBody(bool isLeftPointer) {
    final isLeft = isLeftPointer;
    final pointerMargin = this.pointWidth + padding;
    final url = model.profileImgOpponent;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 14.0),
      child: Row(
        children: [
          SizedBox(width: isLeft ? pointerMargin : padding),
          if (url != null) CircleNetworkImage(path: url, imageSize: 46),
          if (url != null) SizedBox(width: 14.0),
          Expanded(child: renderText()),
          SizedBox(width: isLeft ? padding : pointerMargin),
        ],
      ),
    );
  }

  Widget renderText() {
    return Container(
      constraints: BoxConstraints(minHeight: 46),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          renderTitleRow(),
          Text(model.message, style: _getSubLabelTextStyle()),
        ],
      ),
    );
  }

  Widget renderTitleRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(model.userNameMe, style: _getLabelTextStyle()),
        renderConformText(),
      ],
    );
  }

  TextStyle _getLabelTextStyle() {
    if (model.readYn == 'Y') {
      return TextStyle(
        fontSize: ThemeFactory.of(context).theme.fontSize12,
        fontWeight: ThemeFactory.of(context).theme.heavyFontWeight,
        color: kinsGrayF7,
      );
    } else {
      return TextStyle(
        fontSize: ThemeFactory.of(context).theme.fontSize12,
        fontWeight: ThemeFactory.of(context).theme.heavyFontWeight,
      );
    }
  }

  TextStyle _getSubLabelTextStyle() {
    if (model.readYn == 'Y') {
      return TextStyle(
        fontSize: ThemeFactory.of(context).theme.fontSize12,
        color: kinsGray95,
      );
    } else {
      return TextStyle(
        fontSize: ThemeFactory.of(context).theme.fontSize12,
        color: kinsGray61,
      );
    }
  }

  Widget renderConformText() {
    final type = model.processType;
    final processYn = model.processYn;
    if (type == null) return Container();
    if (processYn != 'Y') return Container();

    String? title;
    switch (type) {
      //까까요청
      case 'KKAKKA_REQUEST':
        title = '확인하기';
        break;
      //관계요청
      case 'RELATION_REQUEST':
        title = '확인하기';
        break;
      //제안
      case 'KKAKKA_SUGGEST':
        title = '확인하기';
        break;
      //소께해주기
      case 'INTRODUCE_REQUEST':
        title = '소개해주기 ';
        break;
      //허락하기
      case 'INTRODUCE_INVITE':
        title = '소개 허락하기 ';
        break;
      // 확인
      case 'INTRODUCE_APPROVE':
        title = '연락처 확인하기 ';
        break;
    }

    if (title == null) return Container();

    return InkWell(
      child: Row(
        children: [
          Text(
            title,
            style: TextStyle(
              fontSize: ThemeFactory.of(context).theme.fontSize11,
              fontWeight: ThemeFactory.of(context).theme.heavyFontWeight,
              color: ThemeFactory.of(context).theme.primaryColor,
            ),
          ),
          Icon(
            Icons.arrow_right,
            color: ThemeFactory.of(context).theme.primaryColor,
          ),
        ],
      ),
      onTap: () => _conformOnTap(type),
    );
  }

  _conformOnTap(String type) {
    final processParam = model.processParam;

    switch (type) {
      case 'KKAKKA_REQUEST':
        _getRequest();
        break;
      case 'RELATION_REQUEST':
        Get.toNamed('/my-kins/family');
        break;
      case 'KKAKKA_SUGGEST':
        _showDialog(processParam);
        break;
      // 요청
      case 'INTRODUCE_REQUEST':
        _routeIntroduce(processParam);
        break;
      //
      case 'INTRODUCE_INVITE':
        _getRequester(processParam);
        break;
      case 'INTRODUCE_APPROVE':
        _getPhoneNumber(processParam);
        break;
    }
  }

  _getRequest() async {
    try {
      final result =
          await Get.find<KkakkaController>().getRequest(model.processParam!);

      LocalKkakkaModel localModel = LocalKkakkaModel();
      localModel.userRelation = result.relation!.getSearchRelResponse();
      localModel.requestModel = result.request;

      Get.toNamed('/kkakka/issuance/target/detail', arguments: localModel);
    } on DioError catch (e) {
      //final msg = e.response!.data.toString();
      final msg = e.response!.data['resultMsg'];
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  _showDialog(int? suggestNo) async {
    if (suggestNo == null) return;

    final model = await getSuggestModel(suggestNo);
    if (model == null) return;

    final category = model.getCategory();
    final familyName = model.userFamilyNameOpponent ?? '';
    final name = model.userNameOpponent ?? '';
    final userName = '$familyName$name';
    final phone = model.userPhone;
    final isApprove = model.relCheck == 0;

    final result = await Get.dialog(
      AlertDialog(
        backgroundColor: Colors.transparent,
        contentPadding: const EdgeInsets.all(0),
        content: Builder(
          builder: (context) {
            return KkakkaSendDialog(
              imagePath: model.userImage,
              name: userName,
              nickName: model.userRelCdMe ?? '',
              phoneNumber: phone ?? '',
              category: category,
              limit: model.kkakkaLimit ?? 0,
              startDate: model.startDate ?? DateTime.now(),
              endDate: model.endDate ?? DateTime.now(),
              price: model.kkakkaPrice ?? 0,
              btnTitle: isApprove ? '제안 수락하기' : '소개 요청하기',
              whichKkaka: 3,
            );
          },
        ),
      ),
      barrierDismissible: false,
    );

    if (result != true) return;

    if (isApprove) {
      //2021.05.06 Andy 추가 + 수정
      final resp = await Get.find<UserController>().getUserPayCardInfo();
      if (resp.length == 0) {
        final sheet = SimpleInformationBottomSheet(message: '결제수단을 등록해주세요.');
        await Get.bottomSheet<String>(sheet);
        Get.toNamed('/payment/methods');
      } else {
        LocalKkakkaModel localModel = LocalKkakkaModel();
        localModel.suggestDetailModel = model;
        localModel.modelSet();
        Get.toNamed('/kkakka/issuance/target/detail', arguments: localModel);
      }
    } else {
      _sendIntroduce(model);
    }
  }

  Future<SuggestDetailModel?> getSuggestModel(int suggestNo) async {
    try {
      return await Get.find<SuggestController>().suggestDetail(suggestNo);
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }

    return null;
  }

  _sendIntroduce(SuggestDetailModel model) async {
    final userNo = model.userNoFrom;
    final userNoTarget = model.userNoTarget;

    try {
      final data =
          await Get.find<IntroduceController>().request(userNo!, userNoTarget!);

      final sheet = BtnBottomSheet(
        title: data.resultMsg ?? '소개 요청 성공',
        isOneBtn: true,
      );
      await Get.bottomSheet(sheet);
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  _routeIntroduce(int? introduceReqId) async {
    if (introduceReqId == null) return;

    try {
      final data =
          await Get.find<IntroduceController>().relation(introduceReqId);
      data.introduceReqId = introduceReqId;
      Get.toNamed('/timeline/suggest', arguments: data);
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  _getPhoneNumber(int? introduceReqId) async {
    if (introduceReqId == null) return;

    try {
      final model = await Get.find<IntroduceController>().phone(introduceReqId);
      final phoneNumber = model.data;
      if (phoneNumber == null) return;

      launch("tel://$phoneNumber");
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  _getRequester(int? introduceReqId) async {
    if (introduceReqId == null) return;

    try {
      final model =
          await Get.find<IntroduceController>().requester(introduceReqId);
      int no = model.userNoFrom ?? 0;
      String image = model.userImage ?? '';

      final widget = SuggestProfileScreen(
        userRelNo: no,
        tagUrl: image,
        introduceId: introduceReqId,
      );

      Navigator.of(context).push(
        PageRouteBuilder(
          pageBuilder: (_, animation, secondaryAnimation) => widget,
          transitionsBuilder: (_, animation, secondaryAnimation, child) =>
              child,
        ),
      );
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }
}

class BubblePainter extends CustomPainter {
  final bool isLeftPointer;
  final Color color;
  final double pointWidth, pointHeight;
  final double radius, pointRadius = 3.0;

  BubblePainter({
    required this.isLeftPointer,
    required this.color,
    this.pointWidth = 10.0,
    this.pointHeight = 5.0,
    this.radius = 10.0,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final width = size.width;
    final height = size.height;

    final paint = Paint()
      ..color = color
      ..strokeWidth = 1.0
      ..style = PaintingStyle.fill;

    final path =
        isLeftPointer ? pathLeft(width, height) : pathRight(width, height);
    canvas.drawPath(path, paint);
  }

  Path pathLeft(double width, double height) {
    final path = Path();
    path.moveTo(0.0, 0.0);

    path.lineTo(width - radius, 0.0);
    path.arcToPoint(
      Offset(width, radius),
      radius: Radius.circular(radius),
    );

    path.lineTo(width, height - radius);
    path.arcToPoint(
      Offset(width - radius, height),
      radius: Radius.circular(radius),
    );

    path.lineTo(pointWidth + radius, height);
    path.arcToPoint(
      Offset(pointWidth, height - radius),
      radius: Radius.circular(radius),
    );

    path.lineTo(pointWidth, pointHeight);

    path.lineTo(0.0, pointRadius);
    path.arcToPoint(
      Offset(pointRadius, 0.0),
      radius: Radius.circular(pointRadius),
    );

    return path;
  }

  Path pathRight(double width, double height) {
    final path = Path();
    path.moveTo(0.0, 0.0);

    path.lineTo(width - pointRadius, 0.0);
    path.arcToPoint(
      Offset(width, pointRadius),
      radius: Radius.circular(pointRadius),
    );
    path.lineTo(width - pointWidth, pointHeight);

    path.lineTo(width - pointWidth, height - radius);
    path.arcToPoint(
      Offset(width - pointWidth - radius, height),
      radius: Radius.circular(radius),
    );

    path.lineTo(radius, height);
    path.arcToPoint(
      Offset(0.0, height - radius),
      radius: Radius.circular(radius),
    );

    path.lineTo(0.0, radius);
    path.arcToPoint(
      Offset(radius, 0.0),
      radius: Radius.circular(radius),
    );

    return path;
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
