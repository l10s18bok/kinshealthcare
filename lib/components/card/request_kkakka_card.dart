import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/card/request_kkakka_image.dart';
import 'package:kins_healthcare/components/string/randomString.dart';
import 'package:kins_healthcare/components/text/kkakka_type_translate.dart';
import 'package:kins_healthcare/controllers/post_controller.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/models/local/post_model.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/kkakka/model/permise_me_response_model.dart';
import 'package:kins_healthcare/services/kkakka/model/kkakka_filter_response_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

import 'like_reply_card.dart';

/*
 작성일 : 2021-04-07
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : RequestKkakkaCard,
 설명 : 까까 화면의 Item Card
*/

// ignore: must_be_immutable
class RequestKkakkaCard extends StatefulWidget {
  int index;
  dynamic kkakka;
  GestureTapCallback? onTap;

  RequestKkakkaCard({
    required this.index,
    required this.kkakka,
    this.onTap,
  });

  @override
  RequestKkakkaCardSate createState() => RequestKkakkaCardSate();
}

// ignore: must_be_immutable
class RequestKkakkaCardSate extends State<RequestKkakkaCard> {
  late dynamic kkakka;

  @override
  void initState() {
    super.initState();
    kkakka = widget.kkakka;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Column(
        children: [
          SizedBox(height: 20),
          SizedBox(
            height: 44,
            // 받은까까는 상대방 이미지 보여주고
            // 보낸까까는 자신 이미지를 보
            child: renderTopBar(kkakka.runtimeType == PermiseMeResponseModel ||
                    kkakka.runtimeType == KkakkaFilterResponseModel
                ? kkakka.profileImgOpponent
                : kkakka.userImage),
          ),
          SizedBox(height: 12),
          RequestKkakkaImage(kkakka: kkakka),
          KkakkaTypeTranslate(model: kkakka).kkakkaType == KkakkaType.DONATE
              ? renderContent()
              : Container(),
          SizedBox(height: 20),
          Container(height: 1, color: kinsGrayE8E),
        ],
      ),
      onTap: widget.onTap,
    );
  }

  Widget renderTopBar(String imagePath) {
    final price = numberWithComma(kkakka.kkakkaBalance);

    return Row(
      children: [
        renderUserImage(imagePath, 44.0),
        SizedBox(width: 12),
        renderTopName(),
        Spacer(),
        Text(
          '$price ',
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize24,
            fontWeight: FontWeight.w500,
          ),
        ),
        Text(
          '원',
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize18,
            fontWeight: FontWeight.w400,
          ),
        ),
      ],
    );
  }

  Widget renderUserImage(String? path, double size) {
    if (path == null) return renderNoUserImage(size);

    return CircleNetworkImage(
      imageSize: size,
      path: path,
      userNo: kkakka.userNoOpponent,
      heroTag:
          '$path${kkakka.userNoOpponent}${RandomStrings().getRandomString(15)}',
    );
  }

  Widget renderNoUserImage(double size) {
    return SvgPicture.asset(
      'assets/svgs/ic/ic_profile_signin.svg',
      width: size,
      height: size,
    );
  }

  Widget renderTopName() {
    final name = kkakka.userNameOpponent;

    var inday = KkakkaTypeTranslate(model: kkakka).daysTillEndDate;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        Text(
          name,
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize15,
            fontWeight: FontWeight.w700,
          ),
        ),
        Text(
          inday != null ? '$inday 남음' : '사용기한 미설정',
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize11,
            fontWeight: FontWeight.w400,
            color: kinsGray7D,
          ),
        ),
      ],
    );
  }

  Widget renderContent() {
    final likeReplyModel = KkakkaTypeTranslate(model: kkakka).likeReplyModel;

    return Column(
      children: [
        SizedBox(height: 12),
        LikeReplyCard(
          type:
              KkakkaTypeTranslate(model: kkakka).kkakkaType == KkakkaType.DONATE
                  ? 'PaintContents'
                  : 'NotPaintContents',
          model: likeReplyModel,
          onMoreTap: () {
            kkakka.isContentOpen = (kkakka.isContentOpen != true);
            setState(() {});
          },
          onLikeTap: () {
            final model = likeReplyModel.localModel;
            if (model == null) return;
            _updateLike(model);
          },
        ),
      ],
    );
  }

  _updateLike(PostLocalModel model) async {
    try {
      final data = await Get.find<PostController>().updateLike(
        model.boardNo,
        model.boardType,
      );

      if (data.resultCode == 'SUCCESS') {
        final likeYn = widget.kkakka.likeYn;
        final likeCount = widget.kkakka.likeCount;

        if (likeYn == 1) {
          widget.kkakka.likeYn = 0;
          widget.kkakka.likeCount = likeCount - 1;
          setState(() {});
        } else {
          widget.kkakka.likeYn = 1;
          widget.kkakka.likeCount = likeCount + 1;
          setState(() {});
        }
      }
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  String? numberWithComma(int? param) {
    if (param == null) return null;
    return new NumberFormat('###,###,###,###')
        .format(param)
        .replaceAll(' ', '');
  }
}
