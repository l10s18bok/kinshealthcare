import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/payment_user_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

import '../../utils/resource.dart';

/*
 작성일 : 2021-02-16
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : IssuanceTargetCard,
 설명 : 한쪽 끝에 Check Box 가 있는 가족 List Item
*/

// ignore: must_be_immutable
class IssuanceTargetCard extends StatelessWidget {
  IssuanceTargetCard({
    required this.model,
    this.onTap,
  });

  final PaymentUserModel model;
  GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        padding: EdgeInsets.only(left: 16, right: 16),
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: kinsGrayEA,
              width: 1.0,
            ),
          ),
        ),
        height: 84,
        child: Row(
          children: [
            renderItemImage(model.isOnContacts),
            SizedBox(width: 16),
            Expanded(child: renderItemContent(context)),
            SizedBox(width: 16),
            renderCheckBtn(model),
          ],
        ),
      ),
      onTap: () {
        model.isChecked = !model.isChecked;
        if (onTap != null) onTap!();
      },
    );
  }

  Widget renderItemImage(bool isOnContacts) {
    return SizedBox(
      width: 56,
      height: 56,
      child: Stack(
        children: [
          Positioned.fill(child: renderNonImage(56)),
          Positioned(child: renderPhoneIcon(isOnContacts)),
        ],
      ),
    );
  }

  Widget renderNonImage(double imageHeight) {
    return SvgPicture.asset(
      'assets/svgs/ic/ic_profile_signin.svg',
      width: imageHeight,
      height: imageHeight,
    );
  }

  Widget renderPhoneIcon(bool isOnContacts) {
    if (isOnContacts) {
      return SvgPicture.asset(
        'assets/svgs/ic/ic_call_contactpage_on.svg',
        width: 19,
        height: 19,
      );
    } else {
      return SvgPicture.asset(
        'assets/svgs/ic/ic_call_contactpage_off.svg',
        width: 19,
        height: 19,
      );
    }
  }

  Widget renderItemContent(BuildContext context) {
    final name = model.name;
    final nickName = model.nickName;
    final phoneNumber = model.phoneNum;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '$name ($nickName)',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
          ),
        ),
        SizedBox(height: 10),
        Text(
          phoneNumber,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
          ),
        ),
      ],
    );
  }

  renderCheckBtn(PaymentUserModel model) {
    final backgroundColor = model.isChecked ? kinsBlue40 : kinsBlueCF;

    return Container(
      height: 26,
      child: Row(
        children: [
          AnimatedContainer(
            alignment: Alignment.center,
            duration: Duration(milliseconds: 100),
            width: 26,
            height: 26,
            decoration: BoxDecoration(
              color: backgroundColor,
              borderRadius: BorderRadius.all(Radius.circular(4)),
              border: Border.all(color: kinsGrayC2),
            ),
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_checkbox_unselect.svg',
              width: 12,
              height: 10,
            ),
          ),
        ],
      ),
    );
  }
}
