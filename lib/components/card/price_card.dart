import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/services/custom/model/custom_list_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

import 'non_kkakka_card.dart';

/*
 작성일 : 2021-04-01
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : PriceCard,
 설명 : 맞춤 광고 하단에 표시하는 상품 Grid 의 Item
*/

// ignore: must_be_immutable
class PriceCard extends StatelessWidget {
  CustomListModel model;
  final GestureTapCallback? onFavouriteTap;

  PriceCard({
    required this.model,
    this.onFavouriteTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          InkWell(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(child: renderImage(model.itemMainImage)),
                SizedBox(height: 10),
                renderTitle(context),
                SizedBox(height: 5),
                renderContent(context)
              ],
            ),
            onTap: () {
              Get.toNamed('/advertise/custom/detail', arguments: model);
            },
          ),
          Positioned(right: 4, top: 4, child: renderHeart(model.likeNo)),
        ],
      ),
    );
  }

  Widget renderImage(String? path) {
    if (path == null || !path.startsWith('http')) {
      return NonKkakkaCard(
        title: 'No Image',
        height: double.infinity,
      );
    }

    return CachedNetworkImage(
      imageUrl: path,
      fit: BoxFit.cover,
      width: double.infinity,
    );
  }

  Widget renderTitle(BuildContext context) {
    return Text(
      model.itemTitle ?? '',
      style: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: ThemeFactory.of(context).theme.fontSize14,
      ),
    );
  }

  Widget renderContent(BuildContext context) {
    return Text(
      model.itemPrice ?? '',
      style: TextStyle(
        fontSize: ThemeFactory.of(context).theme.fontSize12,
      ),
    );
  }

  Widget renderHeart(int? likeNo) {
    final unSelect = 'assets/svgs/ic/ic_heart_unselect.svg';
    final select = 'assets/svgs/ic/ic_heart_customized.svg';

    return InkWell(
      child: Container(
        width: 50,
        height: 50,
        child: Align(
          alignment: Alignment.topRight,
          child: SizedBox(
            height: 30,
            width: 30,
            child: SvgPicture.asset(
              likeNo == null ? unSelect : select,
              color: kinsWhite,
            ),
          ),
        ),
      ),
      onTap: onFavouriteTap,
    );
  }
}
