import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class QuoteCard extends StatefulWidget {
  final String text;

  QuoteCard({
    required this.text,
  });

  @override
  _QuoteCardState createState() => _QuoteCardState();
}

class _QuoteCardState extends State<QuoteCard> {
  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    final splitText = this.widget.text.split('\n');

    return Container(
      decoration: BoxDecoration(
        color: Color(0xFFEFF0F5),
        borderRadius: BorderRadius.circular(6.0),
      ),
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: splitText
              .asMap()
              .entries
              .map<Widget>(
                (e) => Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Color(0xFFD7D8EE),
                        width: 1.0,
                      ),
                    ),
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 8.0),
                    child: Row(
                      mainAxisAlignment: splitText.length - 1 == e.key
                          ? MainAxisAlignment.spaceBetween
                          : MainAxisAlignment.start,
                      children: [
                        if (e.key == 0)
                          SvgPicture.asset(
                            'assets/svgs/ic/ic_quote_start.svg',
                          ),
                        Text(
                          e.value,
                          style: TextStyle(
                            fontSize: theme.fontSize12,
                          ),
                        ),
                        if (splitText.length - 1 == e.key)
                          SvgPicture.asset(
                            'assets/svgs/ic/ic_quote_end.svg',
                          ),
                      ],
                    ),
                  ),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
