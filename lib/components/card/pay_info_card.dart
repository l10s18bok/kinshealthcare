import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class PayInfoCard extends StatefulWidget {
  final String total;
  final bool squashAmount;
  final String companyName;
  final String address;
  final String price;

  PayInfoCard({
    required this.total,
    required this.companyName,
    required this.address,
    required this.price,
    this.squashAmount = false,
  });

  @override
  _PayInfoCardState createState() => _PayInfoCardState();
}

class _PayInfoCardState extends State<PayInfoCard> {
  renderPayInfoIcon() {
    final theme = ThemeFactory.of(context).theme;

    return Center(
      child: Container(
        width: 56.0,
        height: 56.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
            color: Color(0xFFD7D8EE),
            width: 1.0,
          ),
        ),
        child: Icon(
          Icons.redeem,
          size: 30.0,
          color: theme.primaryColor,
        ),
      ),
    );
  }

  renderPayInfoKeyValuePair({
    required String key,
    required String value,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      children: [
        Container(
          width: 50.0,
          child: Row(
            children: [
              Text(
                key,
                style: TextStyle(
                  color: theme.primaryColor,
                  fontSize: theme.fontSize12,
                  fontWeight: theme.heavyFontWeight,
                ),
              ),
            ],
          ),
        ),
        Row(
          children: [
            Text(
              value,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Color(0xFF6C6C6C),
                fontSize: 11.0,
              ),
            ),
          ],
        ),
      ],
    );
  }

  renderPayInfo() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            renderPayInfoIcon(),
            Container(width: 14.0),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  renderPayInfoKeyValuePair(
                    key: '상호명',
                    value: widget.companyName,
                  ),
                  renderPayInfoKeyValuePair(
                    key: '주소',
                    value: widget.address,
                  ),
                  renderPayInfoKeyValuePair(
                    key: '금액',
                    value: widget.price,
                  ),
                  if (widget.squashAmount)
                    Padding(
                      padding: EdgeInsets.only(top: 21.0),
                      child: renderPayAmount(),
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  renderPayAmount() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          '결제할 금액',
          style: TextStyle(
            color: theme.primaryColor,
            fontWeight: theme.heavyFontWeight,
            fontSize: theme.fontSize15,
          ),
        ),
        Text(
          '${widget.total}원',
          style: TextStyle(
            fontSize: theme.fontSize15,
            fontWeight: theme.heavyFontWeight,
            color: theme.primaryOrangeColor,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        renderPayInfo(),
        if (!widget.squashAmount)
          Padding(
            padding: EdgeInsets.only(
              left: 16.0,
              right: 16.0,
              bottom: 16.0,
            ),
            child: renderPayAmount(),
          ),
      ],
    );
  }
}
