import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/card/like_reply_card.dart';
import 'package:kins_healthcare/components/text/kkakka_type_translate.dart';
import 'package:kins_healthcare/components/time/timeDiff.dart';
import 'package:kins_healthcare/controllers/post_controller.dart';
import 'package:kins_healthcare/models/local/post_model.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/post/model/post_list_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'non_kkakka_card.dart';

/*
 작성일 : 2021-04-19
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : RequestPostCard,
 설명 : 소식 List 의 소식을 표시하기 위한 Card
*/

// ignore: must_be_immutable
class RequestPostCard extends StatefulWidget {
  int index;
  PostListModel post;
  GestureTapCallback? onTap;
  GestureTapCallback? onMoreTap;
  late BuildContext context;

  RequestPostCard({
    required this.index,
    required this.post,
    this.onTap,
    this.onMoreTap,
  });

  @override
  RequestPostCardState createState() => RequestPostCardState();
}

// ignore: must_be_immutable
class RequestPostCardState extends State<RequestPostCard> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Column(
        children: [
          SizedBox(height: 20),
          SizedBox(
            height: 44,
            child: renderTopBar(),
          ),
          SizedBox(height: 12),
          renderPostImage(),
          renderContent(),
          SizedBox(height: 20),
          Container(height: 1, color: kinsGrayE8E),
        ],
      ),
      onTap: widget.onTap,
    );
  }

  Widget renderTopBar() {
    final path = widget.post.profileImg;
    final name = widget.post.userName;
    final inTime = TimeDiffUtile().getTimeDiff(widget.post.regTime, 'N');
    return Row(
      children: [
        renderUserImage(path, 44.0),
        SizedBox(width: 12),
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name ?? '',
                style: TextStyle(
                  fontSize: ThemeFactory.of(context).theme.fontSize15,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Text(
                '$inTime',
                style: TextStyle(
                  fontSize: ThemeFactory.of(context).theme.fontSize11,
                  fontWeight: FontWeight.w400,
                  color: kinsGray7D,
                ),
              ),
            ],
          ),
        ),
        widget.post.contentType == 'KKAKKA' ? renderPrice() : renderMenu()
      ],
    );
  }

  alerDialog() {
    return AlertDialog(
        actionsPadding: EdgeInsets.zero,
        insetPadding: EdgeInsets.zero,
        actions: [
          // Center(
          //   child: TextButton(
          //     onPressed: () {},
          //     child: Text('data'),
          //   ),
          // ),
          // Center(
          //   child: TextButton(
          //     onPressed: () {},
          //     child: Text('data'),
          //   ),
          // )
        ]);
  }

  Widget renderMenu() {
    String path = 'assets/svgs/ic/more.svg';
    return Expanded(
        child: Align(
            alignment: Alignment.centerRight,
            child: IconButton(
                icon: SvgPicture.asset(path, width: 28, height: 28),
                onPressed: () async {
                  // await Get.dialog(alerDialog());
                })));
  }

  Widget renderPrice() {
    return Expanded(
        child: Align(
      alignment: Alignment.centerRight,
      child: Text(
        '${KkakkaTypeTranslate(model: widget.post).price(widget.post.kkakkaBalance!.toInt())} 원',
        style: TextStyle(
          fontSize: ThemeFactory.of(context).theme.fontSize24,
          fontWeight: FontWeight.w500,
        ),
      ),
    ));
  }

  Widget renderUserImage(String? path, double size) {
    if (path == null) return renderNoUserImage(size);

    return CircleNetworkImage(
      imageSize: size,
      path: path,
      userNo: widget.post.userNo,
      heroTag: '$path${widget.post.userNo}${widget.post.regTime.hashCode}',
    );
  }

  Widget renderNoUserImage(double size) {
    return SvgPicture.asset(
      'assets/svgs/ic/ic_profile_signin.svg',
      width: size,
      height: size,
    );
  }

  Widget renderPostImage() {
    final post = widget.post;

    String? path;

    if (post.photos != null && post.photos!.length > 0) {
      path = post.photos![0];
      if (path ==
          'https://d3q7af4rsfl8rt.cloudfront.nethttps://test-image-sm.s3.ap-northeast-2.amazonaws.com/sister1.jpg') {
        path =
            'https://test-image-sm.s3.ap-northeast-2.amazonaws.com/sister1.jpg';
      }
    } else {
      path =
          'https://d3q7af4rsfl8rt.cloudfront.net/image/202105/1620879912556_KKAKKA_PIN.png';
    }

    return AspectRatio(
      aspectRatio: 328 / 180,
      child: renderImage(path),
    );
  }

  Widget renderImage(String? path) {
    if (path == null || !path.startsWith('http')) return nonImage();

    return CachedNetworkImage(
      fit: BoxFit.fitWidth,
      alignment: Alignment.center,
      imageUrl: path,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          color: kinsWhite,
          borderRadius: BorderRadius.circular(8.0),
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.cover,
            alignment: Alignment.center,
          ),
        ),
        child: widget.post.contentType == 'KKAKKA'
            ? Stack(
                children: [
                  Positioned(
                    right: 8,
                    top: 12,
                    child: renderType(),
                  ),
                ],
              )
            : Container(),
      ),
      errorWidget: (_, url, error) => nonImage(),
    );
  }

  Widget renderType() {
    final color = KkakkaTypeTranslate(model: widget.post)
        .postPrimaryColor
        .call(widget.post.kkakkaType!);
    final title = KkakkaTypeTranslate(model: widget.post)
        .postType
        .call(widget.post.kkakkaType!);

    return Container(
      alignment: Alignment.center,
      width: 61,
      height: 26,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.all(Radius.circular(4)),
      ),
      child: Text(
        title!,
        style: TextStyle(
          color: kinsWhite,
          fontWeight: FontWeight.w500,
          fontSize: ThemeFactory.of(context).theme.fontSize14,
        ),
      ),
    );
  }

  Widget nonImage() {
    return NonKkakkaCard(title: 'No Image', height: double.infinity);
  }

  Widget renderContent() {
    final post = widget.post;
    final likeReplyModel = post.likeReplyModel;

    return Column(
      children: [
        SizedBox(height: 12),
        LikeReplyCard(
          type: post.contentType != 'POST' && post.kkakkaType != 'D'
              ? 'NotPaintContents'
              : 'PaintContents',
          model: likeReplyModel,
          onMoreTap: () {
            post.isContentOpen = (post.isContentOpen != true);
            setState(() {});
          },
          onLikeTap: () {
            final model = likeReplyModel.localModel;
            if (model == null) return;
            _updateLike(model);
          },
        )
      ],
    );
  }

  _updateLike(PostLocalModel model) async {
    try {
      final data = await Get.find<PostController>().updateLike(
        model.boardNo,
        model.boardType,
      );

      final resultModel = data.data.values;

      final firstLikeName = widget.post.firstLikeName;

      widget.post.firstLikeName = firstLikeName ?? resultModel.userName;
      widget.post.likeCount = resultModel.likeTotal;
      final likeYn = widget.post.likeYn;
      widget.post.likeYn = likeYn == 1 ? 0 : 1;
      setState(() {});
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  String? numberWithComma(int? param) {
    if (param == null) return null;
    return new NumberFormat('###,###,###,###')
        .format(param)
        .replaceAll(' ', '');
  }
}
