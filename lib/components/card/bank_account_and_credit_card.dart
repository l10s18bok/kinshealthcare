import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/button/circle_check_box.dart';
import 'package:kins_healthcare/models/local/user_pay_card_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class BankAccountAndCreditCard extends StatefulWidget {
  final GestureTapCallback onTap;
  final UserPayCardModel cardModel;

  BankAccountAndCreditCard({
    required this.onTap,
    required this.cardModel,
  });

  @override
  _BankAccountAndCreditCardState createState() => _BankAccountAndCreditCardState();
}

class _BankAccountAndCreditCardState extends State<BankAccountAndCreditCard> {
  final String cardDefaultImageUrl = //은행 기본 이미지(현재 : 신한은행 로고)
      'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=088';

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return InkWell(
      onTap: widget.onTap,
      child: Container(
        width: 220,
        child: Card(
          color: widget.cardModel.cardColor ?? Color(0xFF214597),
          elevation: 7,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 14),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 20,
                      height: 20,
                      child: CircleAvatar(
                        child: Image.asset(
                          widget.cardModel.cardPath ?? 'assets/png/card_ic_sh.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    // CachedNetworkImage(
                    //   imageUrl: widget.cardModel.imgUrl ?? cardDefaultImageUrl,
                    //   fit: BoxFit.fitHeight,
                    //   alignment: Alignment.center,
                    //   width: 20,
                    //   height: 20,
                    //   errorWidget: (context, url, error) => Icon(Icons.error),
                    // ),
                    SizedBox(width: 5),
                    Text(
                      widget.cardModel.cardInfo!.paymentCorp,
                      style: TextStyle(color: theme.whiteTextColor, fontSize: theme.fontSize15, fontWeight: FontWeight.bold),
                    ),
                    Spacer(),
                    CircleCheckbox(
                      value: widget.cardModel.isSelected,
                      onChanged: null,
                      tristate: true,
                    )
                  ],
                ),
                SizedBox(height: 32),
                Text(
                  widget.cardModel.cardInfo!.paymentTypeDesc == '가상계좌' ? '계좌번호' : '카드번호',
                  style: TextStyle(color: theme.whiteTextColor, fontSize: theme.fontSize12, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 5),
                Text(
                  widget.cardModel.cardInfo!.identityHash,
                  style: TextStyle(color: theme.whiteTextColor, fontSize: theme.fontSize13, fontWeight: FontWeight.bold, letterSpacing: 3.0),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
