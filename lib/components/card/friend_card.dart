import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class FriendCard extends StatefulWidget {
  final bool isOnContacts;

  FriendCard({
    this.isOnContacts = false,
  });

  @override
  _FriendCardState createState() => _FriendCardState();
}

class _FriendCardState extends State<FriendCard> {
  renderPhoneIcon() {
    if (widget.isOnContacts) {
      return SvgPicture.asset(
        'assets/svgs/ic/ic_call_contactpage_on.svg',
      );
    } else {
      return SvgPicture.asset(
        'assets/svgs/ic/ic_call_contactpage_off.svg',
      );
    }
  }

  renderAvatar() {
    return Stack(
      children: [
        CircleAvatar(
          radius: 28.0,
        ),
        Positioned(
          left: 0,
          top: 0,
          child: renderPhoneIcon(),
        ),
      ],
    );
  }

  renderName() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      children: [
        Text(
          '어마마마 (엄마)',
          style: TextStyle(
            fontSize: theme.fontSize14,
            fontWeight: theme.heavyFontWeight,
            color: Color(0xFF262626),
          ),
        ),
      ],
    );
  }

  renderPhone() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      children: [
        Text(
          '010-1234-5678',
          style: TextStyle(
            fontSize: theme.fontSize12,
            color: Color(0xFF3E3E3E),
          ),
        ),
      ],
    );
  }

  renderText() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(left: 12.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            renderName(),
            renderPhone(),
          ],
        ),
      ),
    );
  }

  renderIcons() {
    return Row(
      children: [
        IconButton(
          icon: Icon(
            Icons.edit,
            color: Colors.grey,
          ),
          onPressed: () {},
        ),
        IconButton(
          icon: Icon(
            Icons.delete,
            color: Colors.grey,
          ),
          onPressed: () {},
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 14.0),
        child: IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              renderAvatar(),
              renderText(),
              renderIcons(),
            ],
          ),
        ),
      ),
    );
  }
}
