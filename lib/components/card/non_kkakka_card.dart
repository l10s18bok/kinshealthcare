import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-03-03
 작성자 : Mark,
 화면명 :
 경로 :
 클래스 : NonKkakkaCard,
 설명 : 까까 List 가 없는 경우 표시하는 Card
*/

// ignore: must_be_immutable
class NonKkakkaCard extends StatelessWidget {
  final String title;
  double height;
  bool isOutline;

  NonKkakkaCard({
    required this.title,
    this.height = 140,
    this.isOutline = true,
  });

  @override
  Widget build(BuildContext context) {
    if (isOutline == false) {
      return renderMainContainer(context);
    }

    return DottedBorder(
      dashPattern: [4, 4],
      color: kinsGrayE1,
      strokeWidth: 2,
      radius: const Radius.circular(4),
      child: renderMainContainer(context),
    );
  }

  Widget renderMainContainer(BuildContext context) {
    return Container(
      height: height,
      width: double.infinity,
      color: kinsGrayF7F,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          renderLogo(),
          SizedBox(height: 10),
          renderTitle(context),
        ],
      ),
    );
  }

  Widget renderLogo() {
    return SvgPicture.asset(
      'assets/svgs/ic/ic_logo_main.svg',
      color: kinsGrayCB,
      width: 42,
      height: 42,
    );
  }

  Widget renderTitle(BuildContext context) {
    return Text(
      title,
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: ThemeFactory.of(context).theme.fontSize10,
        fontWeight: FontWeight.w700,
        color: kinsGray9A,
      ),
    );
  }
}
