import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/controllers/force_controller.dart';
import 'package:kins_healthcare/services/force/model/event_under_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class PendingPaymentOverlayCard extends StatefulWidget {
  final EventUnderResponse event;

  PendingPaymentOverlayCard({
    required this.event,
  });

  @override
  _PendingPaymentOverlayCardState createState() =>
      _PendingPaymentOverlayCardState();
}

class _PendingPaymentOverlayCardState extends State<PendingPaymentOverlayCard> {
  late Timer timer;

  @override
  void initState() {
    super.initState();

    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (widget.event.timeDiffInStr == '만료') {
        Get.find<ForceController>().forceEventUnder(
          reset: true,
        );
        timer.cancel();
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();

    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return Material(
      elevation: 6.0,
      borderRadius: BorderRadius.circular(10.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 14.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Text(
                    '결제요청',
                    style: TextStyle(),
                  ),
                  Container(
                    width: 8.0,
                  ),
                  Text(
                    widget.event.parsedAmount + '원',
                    style: TextStyle(
                      fontWeight: theme.heavyFontWeight,
                      color: theme.primaryBlueColor,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    widget.event.timeDiffInStr,
                    style: TextStyle(
                      fontWeight: theme.heavyFontWeight,
                      color: theme.primaryOrangeColor,
                    ),
                  ),
                  Container(
                    width: 8.0,
                  ),
                  Text(
                    '응답하기',
                    style: TextStyle(
                      color: Color(0xFF686868),
                    ),
                  ),
                  Icon(
                    Icons.chevron_right,
                    color: Color(0xFF686868),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
