import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/radio_button/default_radio_button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/widget_utils.dart';

class DefaultRadioButtonGroup extends StatefulWidget {
  final List<DefaultRadioButton> radios;
  final String label;
  final int rowMax;
  final bool plainLayout;

  DefaultRadioButtonGroup({
    required this.radios,
    required this.label,
    this.rowMax = 4,
    this.plainLayout = false,
  });

  @override
  _DefaultRadioButtonGroupState createState() =>
      _DefaultRadioButtonGroupState();
}

class _DefaultRadioButtonGroupState extends State<DefaultRadioButtonGroup> {
  renderLabel() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      children: [
        Text(
          widget.label,
          style: TextStyle(
            fontSize: theme.fontSize13,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
      ],
    );
  }

  renderButtons() {
    final WidgetUtils util = WidgetUtils();

    if (widget.plainLayout) {
      return Row(
        children: widget.radios
            .asMap()
            .entries
            .map(
              (e) => Padding(
                padding: EdgeInsets.only(
                  right: e.key != widget.radios.length - 1 ? 20.0 : 0,
                ),
                child: e.value,
              ),
            )
            .toList(),
      );
    }

    return util.renderListWidgetsInMultipleRows(widgets: widget.radios);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        renderLabel(),
        Container(height: 12),
        renderButtons(),
      ],
    );
  }
}
