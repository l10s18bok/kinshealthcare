import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class DefaultRadioButton extends StatefulWidget {
  final String label;
  final dynamic value;
  final dynamic groupValue;
  final ValueSetter<dynamic> onTap;

  DefaultRadioButton({
    required this.label,
    required this.value,
    required this.onTap,
    required this.groupValue,
  });

  @override
  _DefaultRadioButtonState createState() => _DefaultRadioButtonState();
}

class _DefaultRadioButtonState extends State<DefaultRadioButton> {
  renderRadio() {
    final theme = ThemeFactory.of(context).theme;

    final bool isSelected = this.widget.value == this.widget.groupValue;

    final Color topColor = isSelected ? theme.primaryColor : Colors.white;

    return Stack(
      children: [
        Container(
          width: 19.0,
          height: 19.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: topColor,
            border: Border.all(
              color: isSelected ? theme.primaryColor : Color(0xFFC2C2C2),
              width: 1.0,
            ),
          ),
        ),
        if (isSelected)
          Positioned.fill(
            child: Center(
              child: Container(
                width: 7.0,
                height: 7.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
              ),
            ),
          ),
      ],
    );
  }

  renderLabel() {
    return Padding(
      padding: EdgeInsets.only(
        left: 6.0,
      ),
      child: Text(widget.label),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onTap(widget.value);
      },
      child: Row(
        children: [
          renderRadio(),
          renderLabel(),
        ],
      ),
    );
  }
}
