import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/button/check_button.dart';

class CheckRadioButton extends StatefulWidget {
  final String label;
  final dynamic value;
  final dynamic groupValue;
  final ValueSetter<dynamic> onTap;

  CheckRadioButton({
    required this.label,
    required this.value,
    required this.onTap,
    required this.groupValue,
  });

  @override
  _CheckRadioButtonState createState() => _CheckRadioButtonState();
}

class _CheckRadioButtonState extends State<CheckRadioButton> {
  @override
  Widget build(BuildContext context) {
    return CheckButton(
      label: widget.label,
      onTap: () {
        widget.onTap(widget.value);
      },
      isActive: widget.groupValue == widget.value,
    );
  }
}
