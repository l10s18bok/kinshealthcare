import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/pin_entry/main_pin_entry.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class CustomPinEntry extends StatefulWidget {
  final String? normalText;
  final String? boldText;
  final ValueSetter<String>? onChanged;
  final double? normalTextSize;
  final double? boldTextSize;
  final TextEditingController? textEditingController;
  final FocusNode? focus;

  CustomPinEntry({
    this.normalText,
    this.boldText,
    this.onChanged,
    this.normalTextSize,
    this.boldTextSize,
    this.textEditingController,
    this.focus,
  });

  @override
  _CustomPinEntryState createState() => _CustomPinEntryState();
}

class _CustomPinEntryState extends State<CustomPinEntry> {
  String? pin;

  @override
  void initState() {
    super.initState();
  }

  renderTitle() {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      children: [
        Text(
          widget.normalText ?? '',
          style: TextStyle(
            fontSize: widget.normalTextSize,
            color: Color(0xFF393939),
          ),
        ),
        Container(
          height: 10.0,
        ),
        Text(
          widget.boldText ?? '',
          style: TextStyle(
            fontSize: widget.boldTextSize,
            color: Color(0xFF393939),
            fontWeight: theme.heavyFontWeight,
          ),
        )
      ],
    );
  }

  renderPinCodeField() {
    return MainPinEntry(
      textEditingController: widget.textEditingController,
      focus: widget.focus,
      onChanged: (val) {
        setState(() {
          pin = val;
        });

        if (widget.onChanged != null) {
          widget.onChanged!(val);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        renderTitle(),
        Container(height: 48.0),
        renderPinCodeField(),
      ],
    );
  }
}
