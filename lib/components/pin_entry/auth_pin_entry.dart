import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/pin_entry/main_pin_entry.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class AuthPinEntry extends StatefulWidget {
  final String normalText;
  final String? boldText;
  final ValueSetter<String>? onChanged;

  AuthPinEntry({
    required this.normalText,
    this.boldText,
    this.onChanged,
  });

  @override
  _AuthPinEntryState createState() => _AuthPinEntryState();
}

class _AuthPinEntryState extends State<AuthPinEntry> {
  String? pin;

  @override
  void initState() {
    super.initState();
  }

  renderTitle() {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      children: [
        Text(
          widget.normalText,
          style: TextStyle(
            fontSize: theme.fontSize13,
            color: Color(0xFF393939),
          ),
        ),
        Container(
          height: 10.0,
        ),
        Text(
          widget.boldText ?? '',
          style: TextStyle(
            fontSize: theme.fontSize15,
            color: Color(0xFF393939),
            fontWeight: theme.heavyFontWeight,
          ),
        )
      ],
    );
  }

  renderPinCodeField() {
    return MainPinEntry(
      onChanged: (val) {
        setState(() {
          pin = val;
        });

        if (widget.onChanged != null) {
          widget.onChanged!(val);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        renderTitle(),
        Container(height: 48.0),
        renderPinCodeField(),
      ],
    );
  }
}
