import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class MainPinEntry extends StatefulWidget {
  final ValueChanged onChanged;
  final TextEditingController? textEditingController;
  final FocusNode? focus;

  MainPinEntry({
    required this.onChanged,
    this.textEditingController,
    this.focus,
  });

  @override
  _MainPinEntryState createState() => _MainPinEntryState();
}

class _MainPinEntryState extends State<MainPinEntry> {
  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;
    return PinCodeTextField(
      controller: widget.textEditingController,
      appContext: context,
      length: 6,
      focusNode: widget.focus,
      autoFocus: true,
      cursorColor: theme.primaryColor,
      obscuringWidget: SvgPicture.asset(
        'assets/svgs/ic/ic_password_on.svg',
      ),
      blinkWhenObscuring: true,
      animationType: AnimationType.fade,
      blinkDuration: Duration(
        milliseconds: 200,
      ),
      useHapticFeedback: true,
      hapticFeedbackTypes: HapticFeedbackTypes.light,
      keyboardType: TextInputType.number, // 2021.04.29 Andy 추가
      pinTheme: PinTheme(
        activeColor: theme.primaryColor,
        selectedColor: theme.primaryColor,
        inactiveColor: theme.primaryColor,
      ),
      obscureText: true,
      onChanged: widget.onChanged,
    );
  }
}
