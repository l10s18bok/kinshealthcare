import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/keyboard/keyboard.dart';

class MoneyInputKeyboard extends StatefulWidget {
  final EdgeInsets bodyPadding;
  final ValueSetter<String>? onKeyboardInput;
  final GestureTapCallback? onConfirm;
  final TextEditingController? textController;

  MoneyInputKeyboard({
    this.bodyPadding = const EdgeInsets.symmetric(horizontal: 16.0),
    this.onKeyboardInput,
    this.textController,
    this.onConfirm,
  });

  @override
  _MoneyInputKeyboardState createState() => _MoneyInputKeyboardState();
}

class _MoneyInputKeyboardState extends State<MoneyInputKeyboard> {
  late TextEditingController controller;

  @override
  void initState() {
    super.initState();

    controller = widget.textController ?? TextEditingController();

    controller.addListener(() {
      if (widget.onKeyboardInput != null) {
        widget.onKeyboardInput!(controller.text);
      }
    });
  }

  maxLengthValidator(String number) {
    return int.parse(number) < 1000000000000;
  }

  onNumberTap(String number) {
    final tempVal = controller.text + number;
    if (!this.maxLengthValidator(tempVal)) {
      return;
    }
    controller.text = tempVal;
  }

  onBackspaceTap() {
    if (controller.text.length < 2) {
      controller.text = '';
    } else {
      controller.text =
          controller.text.substring(0, controller.text.length - 1);
    }
  }

  onAddNumberTap(int amount) {
    int curNumber = 0;

    if (controller.text.length > 0) {
      curNumber = int.parse(controller.text);
    }
    final tempVal = (curNumber + amount).toString();

    if (!this.maxLengthValidator(tempVal)) {
      return;
    }
    controller.text = tempVal;
  }

  onSpecificAmountTap(int amount) {
    controller.text = amount.toString();
  }

  renderHeaderChip({
    required String label,
    required GestureTapCallback onTap,
  }) {
    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey[300]!,
              width: 1.0,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: 4.0,
            ),
            child: Center(
              child: Text(
                label,
              ),
            ),
          ),
        ),
      ),
    );
  }

  renderHeaders() {
    return Container(
      color: Colors.grey[200],
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 4.0,
          horizontal: 16.0,
        ),
        child: Row(
          children: [
            Expanded(
              child: renderHeaderChip(
                label: '+1만', // 2021/03/25 Andy 수정
                onTap: () {
                  onAddNumberTap(10000);
                },
              ),
            ),
            Container(width: 8.0),
            Expanded(
              child: renderHeaderChip(
                label: '+3만', // 2021/03/25 Andy 수정
                onTap: () {
                  onAddNumberTap(30000);
                },
              ),
            ),
            Container(width: 8.0),
            Expanded(
              child: renderHeaderChip(
                label: '+5만', // 2021/03/25 Andy 수정
                onTap: () {
                  onAddNumberTap(50000);
                },
              ),
            ),
            Container(width: 8.0),
            Expanded(
              child: renderHeaderChip(
                label: '+10만', // 2021/03/25 Andy 수정
                onTap: () {
                  onAddNumberTap(100000);
                },
              ),
            ),
            Container(width: 8.0),
            // 2021/03/25 Andy 수정
            // Expanded(
            //   child: renderHeaderChip(
            //     label: '전액',
            //     onTap: () {
            //       onSpecificAmountTap(999999999);
            //     },
            //   ),
            // ),
          ],
        ),
      ),
    );
  }

  renderConfirm() {
    bool isInactive = false;

    try {
      isInactive = int.parse(controller.text) < 100;
    } on FormatException catch (e) {}

    return PrimaryButton(
      label: '확인',
      onTap: isInactive ? null : widget.onConfirm,
    );
  }

  renderBody() {
    return Padding(
      padding: widget.bodyPadding,
      child: Column(
        children: [
          NumberInputKeyboard(
            onTap: onNumberTap,
            onBackspace: onBackspaceTap,
          ),
          Container(height: 16.0),
          renderConfirm(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        renderHeaders(),
        renderBody(),
      ],
    );
  }
}
