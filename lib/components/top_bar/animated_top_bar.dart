import 'dart:ui';

import 'package:flutter/cupertino.dart';

/**
 * @comment 숨겨지는 Animation을 적용하기 위한 TopBar
 * @author  이명재
 * @version 1.0
 * @since   2021-01-28
 */

class AnimatedTopBar extends StatefulWidget {
  AnimatedTopBar({
    Key? key,
    required this.child,
    required this.scrollController,
  }) : super(key: key);

  final Widget child;
  final ScrollController scrollController;
  @override
  AnimatedTopBarState createState() => AnimatedTopBarState();
}

class AnimatedTopBarState extends State<AnimatedTopBar>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation<Offset> offset;
  double preScrollOffset = 0, up = -1, down = -1;

  @override
  void initState() {
    super.initState();
    widget.scrollController.addListener(scrollListener);

    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );

    offset = Tween<Offset>(
      begin: Offset(0.0, 0.0),
      end: Offset(0.0, -1.5),
    ).animate(
      CurvedAnimation(
        parent: controller,
        curve: Curves.easeOutQuad,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: offset,
      child: widget.child,
    );
  }

  void topBarShow(bool isShow) {
    switch (controller.status) {
      case AnimationStatus.completed:
        if (isShow) controller.reverse();
        break;
      case AnimationStatus.dismissed:
        if (isShow == false) controller.forward();
        break;
      default:
    }
  }

  void scrollListener() {
    final nowOffset = widget.scrollController.offset;

    if (preScrollOffset > nowOffset && up == -1) {
      up = nowOffset;
      down = -1;
    } else if (preScrollOffset < nowOffset && down == -1) {
      up = -1;
      down = nowOffset;
    }

    if (up != -1 && (up - 50) > nowOffset) {
      topBarShow(true);
      up = -1;
      down = -1;
    }

    if (down != -1 && (down + 50) < nowOffset) {
      topBarShow(false);
      up = -1;
      down = -1;
    }

    if (down == 0) {
      topBarShow(true);
      up = -1;
      down = -1;
    }

    preScrollOffset = nowOffset;
  }
}
