import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import '../../utils/resource.dart';

/*
 작성일 : 2021-01-25
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : PreRequestTopBar,
 설명 : 약속받은, 약속해준을 위한 Back Bar, 현재 디자인 변경으로 사용가능성 낮음
*/

// ignore: must_be_immutable
class PreRequestTopBar extends StatelessWidget {
  PreRequestTopBar({
    required this.title,
    this.onRightBtnTap,
    this.onFilterBtnTap,
  });

  String title;
  GestureTapCallback? onRightBtnTap;
  GestureTapCallback? onFilterBtnTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 16),
      alignment: Alignment.centerLeft,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          renderBackBtn(),
          Row(
            children: [
              renderTitle(context, title),
              Expanded(child: Container()),
              renderFilter(),
              renderRightBtn(),
            ],
          ),
        ],
      ),
    );
  }

  renderTitle(BuildContext context, String? title) {
    if (title == null) return Container();

    return Text(
      title,
      style: TextStyle(
        color: kinsBlack,
        fontWeight: FontWeight.w700,
        fontSize: ThemeFactory.of(context).theme.fontSize22,
      ),
    );
  }

  renderFilter() {
    return InkWell(
      onTap: onFilterBtnTap,
      child: Align(
        alignment: Alignment.centerRight,
        child: SizedBox(
          width: 30,
          height: 30,
          child: SvgPicture.asset(
            'assets/svgs/ic/ic_filter.svg',
            width: 18,
            height: 18,
          ),
        ),
      ),
    );
  }

  renderBackBtn() {
    return InkWell(
      child: Container(
        alignment: Alignment.centerLeft,
        height: 44,
        width: 40,
        child: SvgPicture.asset(
          'assets/svgs/ic/ic_arrowback_common.svg',
          width: 24,
          height: 24,
          color: kinsGrayA3,
        ),
      ),
      onTap: () => Get.back(),
    );
  }

  renderRightBtn() {
    if (onRightBtnTap == null) return Container();
    return InkWell(
      child: Container(
        margin: EdgeInsets.only(left: 22),
        alignment: Alignment.centerLeft,
        height: 32,
        width: 32,
        child: SvgPicture.asset(
          'assets/svgs/ic/ic_plus_main.svg',
          width: 32,
          height: 32,
        ),
      ),
      onTap: onRightBtnTap,
    );
  }
}
