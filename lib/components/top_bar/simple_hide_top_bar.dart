import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-02-16
 * 작성자 : Andy
 * 주요기능 : Topbar 스크롤중 "<" 버튼 숨기기 (타이틀은 나타남)
 */

class SimpleHideTopBar extends StatefulWidget {
  final String? title;
  final ScrollController? scrollController;

  const SimpleHideTopBar({
    @required String? title,
    scrollController,
  })  : this.title = title,
        this.scrollController = scrollController;

  @override
  _SimpleHideTopBarState createState() => _SimpleHideTopBarState();
}

class _SimpleHideTopBarState extends State<SimpleHideTopBar> {
  bool _showTitle = true;
  bool isScrollingDown = false;
  late ScrollController _scrollController;
  @override
  void initState() {
    super.initState();
    if (widget.scrollController == null) {
      _scrollController = ScrollController();
    } else {
      _scrollController = widget.scrollController!;
    }
    _scrollController.addListener(() {
      if (_scrollController.position.userScrollDirection == ScrollDirection.reverse) {
        if (!isScrollingDown) {
          isScrollingDown = true;
          _showTitle = false;
          setState(() {});
        }
      }

      if (_scrollController.position.userScrollDirection == ScrollDirection.forward) {
        if (isScrollingDown) {
          isScrollingDown = false;
          _showTitle = true;
          setState(() {});
        }
      }
    });
  }

  @override
  void dispose() {
    if (widget.scrollController == null) _scrollController.dispose();
    super.dispose();
  }

  renderBackBtn() {
    return InkWell(
      child: Container(
        alignment: Alignment.centerLeft,
        height: 44,
        width: 40,
        child: SvgPicture.asset(
          'assets/svgs/ic/ic_arrowback_common.svg',
          width: 24,
          height: 24,
          color: kinsGrayA3,
        ),
      ),
      onTap: () => Get.back(),
    );
  }

  renderTitle() {
    if (widget.title == null) return Container();

    return Text(
      widget.title!,
      style: TextStyle(
        color: kinsBlack,
        fontWeight: FontWeight.w700,
        fontSize: ThemeFactory.of(context).theme.fontSize22,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      padding: const EdgeInsets.only(top: 16, left: 16),
      children: [
        AnimatedContainer(
          height: _showTitle ? 44.0 : 0.0,
          duration: Duration(milliseconds: 100),
          child: renderBackBtn(),
        ),
        renderTitle(),
      ],
    );
  }
}
