import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

import '../../utils/resource.dart';

/*
 작성일 : 2021-01-25
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : BackTopBar,
 설명 : 뒤로 Back 하기 위한 기본 TopBar
*/

// ignore: must_be_immutable
class BackTopBar extends StatelessWidget {
  BackTopBar({
    this.title,
    this.height,
    this.rightButtons,
    this.useDefaultPadding = true,
    this.customTitle,
    this.customOnBack,
    this.bgColor = Colors.white,
  });

  bool useDefaultPadding;
  String? title;
  double? height;
  Widget? rightButtons;
  Widget? customTitle;
  GestureTapCallback? customOnBack;
  Color bgColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: bgColor,
      padding: this.useDefaultPadding
          ? EdgeInsets.only(top: 16, left: 16.0, right: 16.0)
          : EdgeInsets.all(0),
      alignment: Alignment.centerLeft,
      width: double.infinity,
      height: height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          renderBackBtn(),
          Row(
            children: [
              renderTitle(context, title),
              rightButtons != null
                  ? Expanded(child: rightButtons!)
                  : Container(),
            ],
          ),
        ],
      ),
    );
  }

  renderTitle(BuildContext context, String? title) {
    if (customTitle != null) return this.customTitle;
    if (title == null) return Container();

    return Text(
      title,
      style: TextStyle(
        color: kinsBlack,
        fontWeight: FontWeight.w700,
        fontSize: ThemeFactory.of(context).theme.fontSize22,
      ),
    );
  }

  renderBackBtn() {
    return InkWell(
      child: Container(
        alignment: Alignment.centerLeft,
        height: 44,
        width: 40,
        child: SvgPicture.asset(
          'assets/svgs/ic/ic_arrowback_common.svg',
          width: 24,
          height: 24,
          color: kinsGrayA3,
        ),
      ),
      onTap: () => customOnBack == null ? Get.back() : customOnBack!(),
    );
  }
}
