import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/controllers/post_controller.dart';
import 'package:kins_healthcare/services/post/model/like_list_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

import '../../utils/resource.dart';

/*
 작성일 : 2021-01-25
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : BackTopBar,
 설명 : 뒤로 Back 하기 위한 기본 TopBar
*/

// ignore: must_be_immutable
class LikeTopBar extends StatelessWidget {
  LikeTopBar({
    this.height,
    this.likeCount = 0,
  });

  double? height;
  int likeCount;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 16, left: 16.0, right: 16.0),
      alignment: Alignment.centerLeft,
      width: double.infinity,
      height: height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          renderBackBtn(),
          renderLikeTitle(context),
        ],
      ),
    );
  }

  Widget renderLikeTitle(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          "좋아요",
          style: TextStyle(
            color: kinsBlack,
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize22,
          ),
        ),
        SizedBox(width: 6),
        Text(
          '$likeCount 명',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            color: kinsBlue40,
            fontSize: ThemeFactory.of(context).theme.fontSize15,
          ),
        ),
      ],
    );
  }

  renderBackBtn() {
    return InkWell(
      child: Container(
        alignment: Alignment.centerLeft,
        height: 44,
        width: 40,
        child: SvgPicture.asset(
          'assets/svgs/ic/ic_arrowback_common.svg',
          width: 24,
          height: 24,
          color: kinsGrayA3,
        ),
      ),
      onTap: () {
        final length = Get.find<PostController>().postLikeList.length;
        if (length != 0) Get.find<PostController>().removeCacheList(type: LikeListModel);
        Get.back();
      },
    );
  }
}
