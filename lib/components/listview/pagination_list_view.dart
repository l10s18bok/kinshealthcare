import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

typedef Widget PaginationListViewWidgetBuilderTypedef(
    BuildContext context, int index);

/// T -> 캐시 키에 사용되는 모델
// ignore: must_be_immutable
class PaginationListView<T extends BaseRetrofitModel> extends StatefulWidget {
  /// 아이템 카운트
  final int itemCount;

  /// separatorBuilder
  final PaginationListViewWidgetBuilderTypedef? separatorBuilder;

  /// itemBuilder
  final PaginationListViewWidgetBuilderTypedef itemBuilder;

  /// 아무것도 없을때 보여줄 widget
  final Widget? emptyWidget;

  /// 로딩시 보여줄 widget
  final Widget? loadingWidget;

  /// 마지막일때 보여줄 widget
  final Widget? lastWidget;

  /// pagination 요청
  final Function request;

  /// 캐시 관리할 컨트롤러
  final BaseController controller;

  /// 2021/03/16 Andy 추가
  final ScrollController? scrollController;

  final Axis scrollDirection;

  final String cacheKey;

  final VoidCallback? nextPageCallback;
  PaginationListView(
      {required this.itemCount,
      required this.itemBuilder,
      required this.request,
      required this.controller,
      this.scrollDirection = Axis.vertical,
      this.scrollController,
      this.separatorBuilder,
      this.lastWidget,
      this.emptyWidget,
      this.loadingWidget,
      this.cacheKey = 'default',
      this.nextPageCallback});

  @override
  _PaginationListViewState<T> createState() => _PaginationListViewState<T>();
}

class _PaginationListViewState<T> extends State<PaginationListView> {
  late String cacheKey = T.toString() + '_${widget.cacheKey}';

  wrapRefreshIndicator(Widget listView) {
    return RefreshIndicator(
      child: listView,
      onRefresh: () => widget.request(
        reset: true,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return wrapRefreshIndicator(
      ListView.separated(
        scrollDirection: widget.scrollDirection,
        itemBuilder: (context, index) {
          /// 아무것도 없을때 보여줄 widget
          if (widget.itemCount == 0) {
            if (widget.emptyWidget != null) {
              return widget.emptyWidget!;
            }

            return Container();
          }

          /// pagination 안에 들어오는 아이템들의 렌더링
          if (index < widget.itemCount) {
            return widget.itemBuilder(context, index);
          }

          if (widget.controller.cache.containsKey(cacheKey)) {
            if (widget.controller.cache[cacheKey]!.hasMore) {
              if (widget.nextPageCallback != null) {
                widget.nextPageCallback!.call();
              } else {
                widget.request.call();
              }

              if (widget.loadingWidget != null) {
                return widget.loadingWidget!;
              }

              return Container(
                child: Center(
                  child: Text('로딩중...'),
                ),
              );
            } else {
              if (widget.lastWidget != null) {
                return widget.lastWidget!;
              }

              return Container(
                child: Center(
                  child: Text(
                    '마지막 입니다.',
                  ),
                ),
              );
            }
          } else {
            if (widget.loadingWidget != null) {
              return widget.loadingWidget!;
            }

            return Container(
              child: Center(
                child: Text('로딩중...'),
              ),
            );
          }
        },
        separatorBuilder: (context, index) {
          if (index < widget.itemCount - 1) {
            if (widget.separatorBuilder != null) {
              return widget.separatorBuilder!(context, index);
            }
          }

          return Container();
        },
        itemCount: widget.itemCount + 1,
        controller: widget.scrollController,
      ),
    );
  }
}
