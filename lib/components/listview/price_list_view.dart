import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-04-01
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : PriceListView,
 설명 : 맞춤 광고 제일 위 화면에서 상품을 표시하는 ListView
*/

// ignore: must_be_immutable
class PriceListView extends StatelessWidget {
  int nowIndex;
  double itemExtent;
  List<String> list;
  FixedExtentScrollController? scrollController;
  ValueChanged<int>? endScrollCallBack;

  PriceListView({
    required this.list,
    this.nowIndex = 0,
    this.itemExtent = 260,
    this.scrollController,
    this.endScrollCallBack,
  });

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollNotification>(
      child: RotatedBox(
        quarterTurns: 3,
        child: ListWheelScrollView.useDelegate(
          controller: scrollController,
          itemExtent: itemExtent,
          onSelectedItemChanged: (int index) => nowIndex = index,
          offAxisFraction: 0,
          diameterRatio: 50,
          childDelegate: ListWheelChildBuilderDelegate(
            childCount: list.length,
            builder: (_, index) {
              return renderListItem(list[index], index);
            },
          ),
        ),
      ),
      onNotification: _onScrollNotification,
    );
  }

  _scrollAnimation(int index, ValueChanged<int>? endScrollCallBack) {
    final widgetBinding = WidgetsBinding.instance;
    final scrollController = this.scrollController;

    if (widgetBinding == null || scrollController == null) return;

    widgetBinding.addPostFrameCallback((_) {
      final duration = Duration(milliseconds: 200);
      scrollController.animateToItem(index, duration: duration, curve: Curves.linear);

      if (endScrollCallBack == null) return;
      endScrollCallBack(index);
    });
  }

  bool _onScrollNotification(ScrollNotification notification) {
    if (notification is ScrollEndNotification == false) return true;

    _scrollAnimation(nowIndex, endScrollCallBack);
    return true;
  }

  Widget renderListItem(String path, int index) {
    return RotatedBox(
      quarterTurns: 1,
      child: renderItem(path),
    );
  }

  Widget renderItem(String path) {
    Widget? child;
    final size = 250.0;
    final boxFit = BoxFit.cover;
    if (path.startsWith('http')) {
      child = CachedNetworkImage(
        imageUrl: path,
        width: size,
        height: size,
        fit: boxFit,
      );
    } else {
      child = Image.asset(
        path,
        width: size,
        height: size,
        fit: boxFit,
      );
    }

    return SizedBox(
      height: 344,
      child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(right: 8, top: 14),
            child: child,
          ),
          Positioned(left: 0, top: 250, child: renderItemText()),
        ],
      ),
    );
  }

  Widget renderItemText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(left: 5),
          child: Text(
            'Outfit Of The Day!',
            style: TextStyle(
              fontSize: 20,
              color: kinsBlack,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        Text(
          '일상과 퍼포먼스의 경계없이 모든 순간을\n함께하고 싶은 에어쿨링 New 샤론 팬츠 론칭',
          style: TextStyle(
            fontSize: 9,
            color: kinsBlack,
            fontWeight: FontWeight.w400,
          ),
        ),
      ],
    );
  }
}
