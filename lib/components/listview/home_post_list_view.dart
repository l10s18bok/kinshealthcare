import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/card/post_card.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/controllers/post_controller.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/post/model/post_list_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-04-08
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : HomeListView,
 설명 : Home에서 표시되는 listView
*/

typedef OnItemTap = void Function(List list, int index);

// ignore: must_be_immutable
class HomePostListView extends StatefulWidget {
  OnItemTap? onListItemTap;
  HomePostListView({
    this.onListItemTap,
  });

  @override
  HomePostListViewState createState() => HomePostListViewState();
}

// ignore: must_be_immutable
class HomePostListViewState extends State<HomePostListView> {
  late PostController postController;

  @override
  void initState() {
    super.initState();
    _setPostController();
  }

  _setPostController() async {
    postController = Get.find<PostController>();
    GeneralPaginationBody body =
        GeneralPaginationBody(direction: false, size: 20, sortBy: 'last');
    postController.getList(reset: true, body: body);
    // postController.addListener(_setStateEndAnimation);
  }

  @override
  void dispose() {
    super.dispose();
    // postController.removeListener(_setStateEndAnimation);
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PostController>(
        builder: (controller) => PaginationListView<PostListModel>(
              itemCount: controller.cache_postList.length == 0
                  ? 1
                  : controller.cache_postList.length,
              itemBuilder: (_, index) {
                return Column(
                  children: [
                    controller.cache_postList.length == 0
                        ? renderFirstItem(index, context)
                        : renderListItem(controller.cache_postList, index),
                  ],
                );
              },
              emptyWidget: renderNonCard(),
              controller: controller,
              request: controller.getList,
              lastWidget: Container(),
              loadingWidget: renderLoading(),
            ));
  }

  Widget renderLoading() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      height: 50,
      child: CircularProgressIndicator(),
    );
  }

  Widget renderFirstItem(int index, BuildContext context) {
    // total 수량은 0 으로 세팅되있고
    final total = postController.postToTal;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 20),
        Container(
          height: 30,
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: renderListTopBar(total, context),
        ),
        SizedBox(height: 10),
      ],
    );
  }

  Widget renderListTopBar(int length, BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        RichText(
          text: TextSpan(
            style: TextStyle(
              fontSize: ThemeFactory.of(context).theme.fontSize12,
              color: kinsBlack,
            ),
            children: <TextSpan>[
              TextSpan(text: '총'),
              TextSpan(
                text: ' $length',
                style: TextStyle(
                  fontSize: ThemeFactory.of(context).theme.fontSize14,
                  fontWeight: FontWeight.w700,
                ),
              ),
              TextSpan(text: '개'),
            ],
          ),
        ),
      ],
    );
  }

  Widget renderNonCard() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10, left: 16, right: 16, top: 20.0),
      child: NonKkakkaCard(title: '작성된 소식이 없습니다.'),
    );
  }

  Widget renderListItem(List list, int index) {
    final model = list[index];
    return Column(
      children: [
        index == 0 ? renderFirstItem(index, context) : Container(),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: RequestPostCard(
            post: model,
            onTap: () {
              if (list[index].contentType == 'KKAKKA')
                widget.onListItemTap!(list, index);
            },
            index: index,
            onMoreTap: () {
              model.isContentOpen = model.isContentOpen == false ? true : false;
              setState(() {});
            },
          ),
        ),
      ],
    );
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }
}
