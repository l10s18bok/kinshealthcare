import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/card/request_kkakka_card.dart';
import 'package:kins_healthcare/controllers/kkakka_controller.dart';
import 'package:kins_healthcare/screens/main_home/request/request_page_screen.dart';
import 'package:kins_healthcare/services/kkakka/model/sent_kkakka_list_response_model.dart';
import 'package:kins_healthcare/services/kkakka/model/sum_kkakka_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/services/kkakka/model/permise_me_response_model.dart';

import 'pagination_list_view.dart';

/*
 작성일 : 2021-04-08
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : HomeListView,
 설명 : Home에서 표시되는 listView
*/

typedef OnItemTap = void Function(List<dynamic> list, int index);

// ignore: must_be_immutable
class HomeListView extends StatelessWidget {
  final ScreenTypes type;
  SumKkakkaModel? sumModel;
  OnItemTap onListItemTap;
  KkakkaController kkakkaController;
  HomeListView({
    required this.type,
    required this.onListItemTap,
    required this.kkakkaController,
    this.sumModel,
  });

  @override
  Widget build(BuildContext context) {
    return GetBuilder<KkakkaController>(
        builder: (controller) => type == ScreenTypes.received
            ? PaginationListView<PermiseMeResponseModel>(
                controller: controller,
                request: controller.listPromiseMe,
                lastWidget: Container(),
                loadingWidget: Container(),
                itemCount: controller.cache_listPromiseMe.length == 0
                    ? 1
                    : controller.cache_listPromiseMe.length,
                itemBuilder: (_, index) {
                  return Column(
                    children: [
                      controller.cache_listPromiseMe.length == 0
                          ? renderFirstItem(context)
                          : renderListItem(
                              controller.cache_listPromiseMe, index, context),
                    ],
                  );
                },
              )
            : PaginationListView<SentKkaListResponseModel>(
                controller: controller,
                request: controller.sentList,
                lastWidget: Container(),
                loadingWidget: Container(),
                itemCount: controller.cache_sentList.length == 0
                    ? 1
                    : controller.cache_sentList.length,
                itemBuilder: (_, index) {
                  return Column(
                    children: [
                      controller.cache_sentList.length == 0
                          ? renderFirstItem(context)
                          : renderListItem(
                              controller.cache_sentList, index, context),
                    ],
                  );
                },
              ));
  }

  Widget renderFirstItem(
    BuildContext context,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 13),
        Align(
          alignment: Alignment.centerRight,
          child: renderAllBtn(context),
        ),
        SizedBox(height: 7),
        Container(
          height: 30,
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: renderListTopBar(
              type == ScreenTypes.received
                  //ddd
                  ? kkakkaController.cache_promiseTotal
                  : kkakkaController.cache_sentTotal,
              context),
        ),
        SizedBox(height: 10),
      ],
    );
  }

  Widget renderAllBtn(BuildContext context) {
    return InkWell(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            '전체보기',
            style: TextStyle(
              fontSize: ThemeFactory.of(context).theme.fontSize12,
              fontWeight: FontWeight.w700,
              color: kinsBlack4F,
            ),
          ),
          SizedBox(width: 3),
          Icon(
            Icons.arrow_forward_ios,
            color: kinsBlack4F,
            size: 12,
          ),
          SizedBox(width: 16),
        ],
      ),
      onTap: _onTapAllBtn,
    );
  }

  Widget renderListTopBar(int length, BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        RichText(
          text: TextSpan(
            style: TextStyle(
              fontSize: ThemeFactory.of(context).theme.fontSize12,
              color: kinsBlack,
            ),
            children: <TextSpan>[
              TextSpan(text: '총'),
              TextSpan(
                text: ' $length',
                style: TextStyle(
                  fontSize: ThemeFactory.of(context).theme.fontSize14,
                  fontWeight: FontWeight.w700,
                ),
              ),
              TextSpan(text: '개'),
            ],
          ),
        ),
        Spacer(),
        Text(
          "${_getSumPrice(sumModel)}원",
          style: TextStyle(
            color: kinsBlue40,
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize20,
          ),
        ),
      ],
    );
  }

  Widget renderNonCard() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10, left: 16, right: 16, top: 20.0),
      child: NonKkakkaCard(title: _getNonTitle()),
    );
  }

  Widget renderListItem(List<dynamic> list, int index, BuildContext context) {
    return Column(
      children: [
        index == 0 ? renderFirstItem(context) : Container(),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: RequestKkakkaCard(
            kkakka: list[index],
            onTap: () => onListItemTap(list, index),
            index: index,
          ),
        ),
      ],
    );
  }

  String _getSumPrice(SumKkakkaModel? model) {
    if (model == null) return '0';

    switch (type) {
      case ScreenTypes.received:
        if (model.sumMe == null) return '0';
        return NumberFormat("#,###").format(model.sumMe);
      case ScreenTypes.give:
        if (model.sumOpp == null) return '0';
        return NumberFormat("#,###").format(model.sumOpp);
      default:
        return '0';
    }
  }

  String _getNonTitle() {
    switch (type) {
      case ScreenTypes.received:
        return '아직 약속 받은 \'까까\'가 없어요.\n요청해보세요!';
      case ScreenTypes.give:
        return '아직 약속 해준 \'까까\'가 없어요.\n발행해보세요!';
      default:
        return '아직 \'까까\'가 없어요.\n요청해보세요!';
    }
  }

  _onTapAllBtn() {
    if (type == ScreenTypes.received) {
      Get.toNamed('/main-home/request-received');
    } else {
      Get.toNamed('/main-home/request-give');
    }
  }
}
