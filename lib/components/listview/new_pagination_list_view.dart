import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

typedef Widget NewPaginationListViewWidgetBuilderTypedef(
    BuildContext context, int index);

/// T -> 캐시 키에 사용되는 모델
// ignore: must_be_immutable
class NewPaginationListView<T extends BaseRetrofitModel>
    extends StatefulWidget {
  /// 아이템 카운트
  final int itemCount;

  /// separatorBuilder
  final NewPaginationListViewWidgetBuilderTypedef? separatorBuilder;

  /// itemBuilder
  final NewPaginationListViewWidgetBuilderTypedef itemBuilder;

  /// 아무것도 없을때 보여줄 widget
  final Widget? emptyWidget;

  /// 로딩시 보여줄 widget
  final Widget? loadingWidget;

  /// 마지막일때 보여줄 widget
  final Widget? lastWidget;

  /// pagination 요청
  final Function request;

  /// 캐시 관리할 컨트롤러
  final BaseController controller;

  /// 2021/03/16 Andy 추가
  final ScrollController? scrollController;

  final Axis scrollDirection;

  final String cacheKey;

  final Future<void> Function()? refreshCallback;

  NewPaginationListView(
      {required this.itemCount,
      required this.itemBuilder,
      required this.request,
      required this.controller,
      this.scrollDirection = Axis.vertical,
      this.scrollController,
      this.separatorBuilder,
      this.lastWidget,
      this.emptyWidget,
      this.loadingWidget,
      this.cacheKey = 'default',
      this.refreshCallback});

  @override
  _NewPaginationListViewState<T> createState() =>
      _NewPaginationListViewState<T>();
}

class _NewPaginationListViewState<T> extends State<NewPaginationListView> {
  late String cacheKey = T.toString() + '_${widget.cacheKey}';

  wrapRefreshIndicator(Widget listView) {
    return RefreshIndicator(
        child: listView, onRefresh: widget.refreshCallback!);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return wrapRefreshIndicator(
      ListView.separated(
        scrollDirection: widget.scrollDirection,
        itemBuilder: (context, index) {
          /// 아무것도 없을때 보여줄 widget
          ///  +1 로 시작했으니까
          if (widget.itemCount == 1) {
            if (widget.emptyWidget != null) {
              return widget.emptyWidget!;
            }
            return Container();
          }

          /// pagination 안에 들어오는 아이템들의 렌더링
          if (index < widget.itemCount) {
            return widget.itemBuilder(context, index);
          } else {
            return widget.lastWidget!;
          }
        },
        separatorBuilder: (context, index) {
          if (index < widget.itemCount - 1) {
            if (widget.separatorBuilder != null) {
              return widget.separatorBuilder!(context, index);
            }
          }

          return Container();
        },
        itemCount: widget.itemCount + 1,
        controller: widget.scrollController,
      ),
    );
  }
}
