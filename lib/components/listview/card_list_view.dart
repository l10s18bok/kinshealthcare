import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/card/add_credit_card.dart';
import 'package:kins_healthcare/components/card/credit_card.dart';
import 'package:kins_healthcare/models/local/card_model.dart';

/*
 작성일 : 2021-03-30
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : CardListView,
 설명 : Card List를 표시하고 선택하기 위한 Widget
*/

// ignore: must_be_immutable
class CardListView extends StatelessWidget {
  int nowIndex;
  double itemExtent;
  List<CardModel> list;
  FixedExtentScrollController? scrollController;
  ValueChanged<int>? endScrollCallBack;
  VoidCallback? addCardOnTap;

  CardListView({
    required this.list,
    this.nowIndex = 0,
    this.itemExtent = 220,
    this.scrollController,
    this.endScrollCallBack,
    this.addCardOnTap,
  });

  @override
  Widget build(BuildContext context) {
    scrollController = scrollController ?? FixedExtentScrollController();

    return NotificationListener<ScrollNotification>(
      child: RotatedBox(
        quarterTurns: 3,
        child: renderListView(),
      ),
      onNotification: _onScrollNotification,
    );
  }

  Widget renderListView() {
    int index = 0;

    return GestureDetector(
      child: ListWheelScrollView(
        controller: scrollController,
        itemExtent: itemExtent,
        children: list.map((item) => renderListItem(item, index++)).toList(),
        onSelectedItemChanged: (position) => nowIndex = position,
        offAxisFraction: 0,
        diameterRatio: 50,
      ),
      onTap: () => _listItemOnTap(list[nowIndex]),
    );
  }

  Widget renderListItem(CardModel model, int index) {
    return RotatedBox(
      quarterTurns: 1,
      child: model.isNon ? AddCreditCard() : CreditCard(model: model),
    );
  }

  _listItemOnTap(CardModel model) {
    final addCardOnTap = this.addCardOnTap;
    if (addCardOnTap == null) return;
    if (model.isNon == false) return;

    addCardOnTap();
  }

  _scrollAnimation(int index, ValueChanged<int>? endScrollCallBack) {
    final widgetBinding = WidgetsBinding.instance;
    final scrollController = this.scrollController;

    if (widgetBinding == null || scrollController == null) return;

    widgetBinding.addPostFrameCallback((_) {
      final duration = Duration(milliseconds: 200);
      scrollController.animateToItem(index, duration: duration, curve: Curves.linear);

      if (endScrollCallBack == null) return;
      endScrollCallBack(index);
    });
  }

  bool _onScrollNotification(ScrollNotification notification) {
    if (notification is ScrollEndNotification == false) return true;

    _scrollAnimation(nowIndex, endScrollCallBack);
    return true;
  }
}
