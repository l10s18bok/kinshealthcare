import 'package:flutter/material.dart';
import '../../utils/resource.dart';

/*
 작성일 : 2021-01-25
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : ExpansionTileCS,
 설명 : 게시판 형태의 ListView 에서 사용하기 위한 아래로 펼처지는 List Tile 출처(https://gist.github.com/diegoveloper/02424eebd4d6e06ae649b31e4ebcf53c)
*/

const Duration _kExpand = Duration(milliseconds: 200);

class ExpansionTileCS extends StatefulWidget {
  const ExpansionTileCS({
    Key? key,
    required this.title,
    this.headerColor,
    this.iconColor,
    this.trailing,
    this.initiallyExpanded = false,
    this.children = const <Widget>[],
    this.onExpansionChanged,
  }) : super(key: key);

  final Widget title;
  final Color? headerColor;
  final Color? iconColor;
  final Widget? trailing;
  final bool initiallyExpanded;
  final List<Widget> children;
  final ValueChanged<bool>? onExpansionChanged;

  @override
  _ExpansionTileState createState() => _ExpansionTileState();
}

class _ExpansionTileState extends State<ExpansionTileCS>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _iconTurns;
  late Animation<double> _heightFactor;

  static final Animatable<double> _easeInTween = CurveTween(curve: Curves.easeIn);
  static final Animatable<double> _halfTween = Tween<double>(begin: 0.0, end: 0.5);

  bool _isExpanded = false;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(duration: _kExpand, vsync: this);

    _iconTurns = _controller.drive(_halfTween.chain(_easeInTween));
    _heightFactor = _controller.drive(_easeInTween);

    _isExpanded = PageStorage.of(context)?.readState(context) ?? widget.initiallyExpanded;
    if (_isExpanded) _controller.value = 1.0;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bool closed = !_isExpanded && _controller.isDismissed;

    return AnimatedBuilder(
      animation: _controller.view,
      builder: _buildChildren,
      child: closed ? Container() : Column(children: widget.children),
    );
  }

  Widget _buildChildren(BuildContext? context, Widget? child) {
    return Container(
      decoration: BoxDecoration(color: Colors.transparent),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          renderHeader(),
          renderContent(child!),
          renderDivider(),
        ],
      ),
    );
  }

  renderHeader() {
    return Container(
      color: widget.headerColor ?? Colors.transparent,
      child: InkWell(
        child: Row(
          children: [
            Expanded(child: widget.title),
            SizedBox(width: 16),
            renderTrailing(),
            SizedBox(width: 16),
          ],
        ),
        onTap: _handleTap,
      ),
    );
  }

  renderTrailing() {
    Widget icon;
    if (widget.trailing != null) {
      icon = widget.trailing!;
    } else {
      icon = Icon(
        Icons.expand_more,
        color: widget.iconColor ?? Colors.grey,
      );
    }

    return RotationTransition(
      turns: _iconTurns,
      child: icon,
    );
  }

  renderContent(Widget child) {
    return ClipRect(
      child: Align(
        heightFactor: _heightFactor.value,
        child: child,
      ),
    );
  }

  renderDivider() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        height: 1,
        width: double.infinity,
        color: kinsGrayDD,
      ),
    );
  }

  _handleTap() {
    setState(() {
      _isExpanded = !_isExpanded;
      if (_isExpanded) {
        _controller.forward();
      } else {
        _controller.reverse().then<void>((void value) {
          if (!mounted) return;
          setState(() {});
        });
      }
      PageStorage.of(context)?.writeState(context, _isExpanded);
    });
    if (widget.onExpansionChanged != null) widget.onExpansionChanged!(_isExpanded);
  }
}
