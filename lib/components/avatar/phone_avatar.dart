import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

/*
 * 작성일 : 2021-02-04
 * 작성자 : JC
 * 화면명 : HY_2001
 * 주요기능 : 아바타 위에 등록된 번호인지 표시를 핸드폰 아이콘으로 해줌
 */
class PhoneAvatar extends StatefulWidget {
  final bool isOnContacts;

  /// 배경 이미지 URL
  final String? bgImageUrl;

  /// 2021/03/31 Andy 추가
  final bool callIconOff;

  PhoneAvatar({
    required this.isOnContacts,
    this.bgImageUrl,
    this.callIconOff = false,
  });

  @override
  _PhoneAvatarState createState() => _PhoneAvatarState();
}

class _PhoneAvatarState extends State<PhoneAvatar> {
  renderCallIcon() {
    if (widget.callIconOff) return Container();
    if (widget.isOnContacts) {
      return SvgPicture.asset(
        'assets/svgs/ic/ic_call_contactpage_on.svg',
      );
    } else {
      return SvgPicture.asset(
        'assets/svgs/ic/ic_call_contactpage_off.svg',
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CircleAvatar(
          radius: 28.0,
          backgroundImage: widget.bgImageUrl == null ? null : NetworkImage(widget.bgImageUrl!),
        ),
        Positioned(
          left: 0,
          top: 0,
          child: renderCallIcon(),
        ),
      ],
    );
  }
}
