import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/image_picker_bottom_sheet.dart';
import 'package:kins_healthcare/controllers/upload_controller.dart';

class ProfileImageAvatar extends StatefulWidget {
  final ValueSetter<File> onChange;
  final ValueSetter<String>? onUploadFinish;
  final ProgressCallback? onSendProgress;
  final String? initialUrl;

  ProfileImageAvatar({
    required this.onChange,
    this.onSendProgress,
    this.onUploadFinish,
    this.initialUrl,
  });

  @override
  _ProfileImageAvatarState createState() => _ProfileImageAvatarState();
}

class _ProfileImageAvatarState extends State<ProfileImageAvatar> {
  File? image;

  renderProfileImage() {
    if (image == null) {
      if (widget.initialUrl == null) {
        return SvgPicture.asset(
          'assets/svgs/ic/ic_profile_signin.svg',
          width: 110.0,
          height: 110.0,
        );
      } else {
        return CircleAvatar(
          radius: 55.0,
          backgroundImage: NetworkImage(
            widget.initialUrl!,
          ),
        );
      }
    } else {
      return CircleAvatar(
        radius: 55.0,
        backgroundImage: FileImage(
          image!,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        final ImagePickerBottomSheetResult? resp = await Get.bottomSheet(
          ImagePickerBottomSheet(
            processImage: true,
          ),
        );

        if (resp == null) return;

        final File file = resp.file;

        setState(() => image = file);

        widget.onChange(image!);

        final path = await Get.find<UploadController>().uploadImage(
          image: file,
        );

        if (widget.onUploadFinish != null) {
          widget.onUploadFinish!(path);
        }
      },
      child: Stack(
        children: [
          renderProfileImage(),
          Positioned(
            bottom: 0,
            right: 0,
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_camera_signin.svg',
            ),
          ),
        ],
      ),
    );
  }
}
