import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PaymentBottomSheetAvatar extends StatefulWidget {
  final double radius;
  final double padding;
  final String? imageUrl;

  PaymentBottomSheetAvatar({
    this.radius = 35.0,
    this.padding = 16.0,
    this.imageUrl,
  });

  @override
  _PaymentBottomSheetAvatarState createState() =>
      _PaymentBottomSheetAvatarState();
}

class _PaymentBottomSheetAvatarState extends State<PaymentBottomSheetAvatar> {
  renderImage() {
    if (widget.imageUrl != null) {
      return Container();
    } else {
      return SvgPicture.asset(
        'assets/svgs/ic/ic_logo_main.svg',
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
              ),
              child: Padding(
                padding: EdgeInsets.all(4.0),
                child: Container(
                  height: widget.radius * 2,
                  width: widget.radius * 2,
                  decoration: BoxDecoration(
                    color: Color(0xFFEFF0F5),
                    shape: BoxShape.circle,
                    image: widget.imageUrl == null
                        ? null
                        : DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                              widget.imageUrl!,
                            ),
                          ),
                  ),
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.all(widget.padding),
                      child: renderImage(),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
