import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:table_calendar/table_calendar.dart';

import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/services/mykins/model/get_anniversary_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class AddAnniversaryCalendar extends StatefulWidget {
  final ValueChanged<DateTime> onChanged;

  const AddAnniversaryCalendar({
    Key? key,
    required this.onChanged,
  }) : super(key: key);

  @override
  _AddAnniversaryCalendarState createState() => _AddAnniversaryCalendarState();
}

class _AddAnniversaryCalendarState extends State<AddAnniversaryCalendar> {
  late DateTime selectedDay;
  late DateTime focusedDay;

  @override
  initState() {
    super.initState();

    focusedDay = DateTime.now();
    selectedDay = DateTime.now();

    Get.find<MykinsController>().getAnniversary();
  }

  renderCalendarHeader() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 32.0),
      child: Row(
        children: [
          Text(
            '${focusedDay.year}-${focusedDay.month}',
            style: TextStyle(
              fontSize: theme.fontSize20,
            ),
          ),
          Container(width: 27.0),
          GestureDetector(
            onTap: () {
              setState(() {
                this.focusedDay = DateTime(this.focusedDay.year, this.focusedDay.month - 1, 1);
              });
            },
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_kkakkapage_left.svg',
              width: 24.0,
              height: 24.0,
              color: Color(0xFFC5C5C5),
            ),
          ),
          Container(width: 16.0),
          GestureDetector(
            onTap: () {
              setState(() {
                this.focusedDay = DateTime(this.focusedDay.year, this.focusedDay.month + 1, 1);
              });
            },
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_kkakkapage_right.svg',
              width: 24.0,
              height: 24.0,
              color: Color(0xFFC5C5C5),
            ),
          ),
        ],
      ),
    );
  }

  renderAddEvent() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      child: Material(
        color: Colors.white,
        borderRadius: BorderRadius.circular(23.5),
        child: InkWell(
          onTap: () {
            widget.onChanged(selectedDay);
            Get.back();
          },
          borderRadius: BorderRadius.circular(23.5),
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Color(0xFFD7DBEE),
                width: 1.0,
              ),
              borderRadius: BorderRadius.circular(
                23.5,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 28.0, vertical: 12.0),
              child: Text(
                '기념일 추가',
                style: TextStyle(
                  color: Color(0xFF4042AB),
                  fontSize: theme.fontSize13,
                  fontWeight: theme.heavyFontWeight,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  List loadEvents(DateTime day, List<GetAnniversaryResponse> anniversaries) {
    return anniversaries
        .where(
          (element) => element.dates == '${day.year}-${day.month.toString().padLeft(2, '0')}-${day.day.toString().padLeft(2, '0')}',
        )
        .map((x) => x.anniName)
        .toList();
  }

  renderCalendar() {
    final theme = ThemeFactory.of(context).theme;

    TextStyle dayStyle = TextStyle(
      fontSize: theme.fontSize14,
      color: Color(0xFF888888),
    );

    return GetBuilder<MykinsController>(
      builder: (c) {
        return TableCalendar(
          focusedDay: focusedDay,
          firstDay: DateTime.utc(1900, 1, 1),
          lastDay: DateTime.utc(2400, 12, 31),
          selectedDayPredicate: (day) {
            return isSameDay(selectedDay, day);
          },
          eventLoader: (DateTime day) => loadEvents(day, c.anniversaries),
          headerVisible: false,
          daysOfWeekStyle: DaysOfWeekStyle(
            dowTextFormatter: (DateTime day, _) {
              final List dowTxt = [
                'M',
                'T',
                'W',
                'T',
                'F',
                'S',
                'S',
              ];
              return dowTxt[day.weekday - 1];
            },
            weekendStyle: TextStyle(
              fontSize: theme.fontSize12,
              color: Color(0xFFFF4A4A),
              fontWeight: theme.heavyFontWeight,
            ),
            weekdayStyle: TextStyle(
              fontSize: theme.fontSize12,
              color: Color(0xFF3D3D3D),
              fontWeight: theme.heavyFontWeight,
            ),
          ),
          calendarStyle: CalendarStyle(
            selectedDecoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                color: theme.primaryOrangeColor,
                width: 1.0,
              ),
            ),
            outsideDaysVisible: false,
            selectedTextStyle: dayStyle,
            defaultTextStyle: dayStyle,
            todayDecoration: BoxDecoration(
              shape: BoxShape.circle,
              color: theme.primaryOrangeColor,
              border: Border.all(
                color: theme.primaryOrangeColor,
                width: 1.0,
              ),
            ),
          ),
          onPageChanged: (DateTime day) {
            setState(() {
              focusedDay = day;
            });
          },
          onDaySelected: (selected, focused) {
            setState(() {
              selectedDay = selected;
            });
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 320,
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            BackTopBar(
              title: '나의 기념일 달력',
            ),
            SizedBox(height: 40.0),
            renderCalendarHeader(),
            renderCalendar(),
            renderAddEvent(),
          ],
        ),
      ),
    );
  }
}
