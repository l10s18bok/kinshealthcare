import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:table_calendar/table_calendar.dart';

class MainCalendar extends StatefulWidget {
  final OnRangeSelected onRangeSelected;
  final DateTime? rangeStartDay;
  final DateTime? rangeEndDay;
  final DateTime focusedDay;

  MainCalendar({
    required this.onRangeSelected,
    this.rangeStartDay,
    this.rangeEndDay,
    required this.focusedDay,
  });

  @override
  _MainCalendarState createState() => _MainCalendarState();
}

class _MainCalendarState extends State<MainCalendar> {
  Widget? container;

  DateTime? selectedDay;
  DateTime? rangeStart;
  DateTime? rangeEnd;
  DateTime? foucused;
  String? selectedStartDate;
  String? selectedEndDate;
  // 날짜만 보여주기 위해 사용 하는지? 확인.
  bool? isWithParam;
  @override
  void initState() {
    isWithParam = true;
    selectedStartDate = '';
    selectedEndDate = '';
    super.initState();
  }

  renderCalendar() {
    final theme = ThemeFactory.of(context).theme;

    final defaultTextStyle = TextStyle(
      fontSize: theme.fontSize14,
      color: Color(0xFF888888),
    );

    return TableCalendar(
      firstDay: DateTime.utc(1900, 1, 1),
      lastDay: DateTime.utc(2400, 12, 31),
      focusedDay: widget.focusedDay,
      rangeSelectionMode: RangeSelectionMode.enforced,
      headerStyle: HeaderStyle(
        titleCentered: true,
        formatButtonVisible: false,
        titleTextFormatter: (day, _) {
          return '${day.year}∙${day.month}';
        },
      ),
      calendarBuilders: CalendarBuilders(
        dowBuilder: (_, day) {
          final dow = DateFormat('E').format(day);

          TextStyle style = TextStyle(
            fontWeight: theme.heavyFontWeight,
          );

          if (dow == 'Sun') {
            style = style.copyWith(
              color: Color(0xFFFF4A4A),
            );
          }

          if (dow == 'Sat') {
            style = style.copyWith(
              color: Color(0xFF3B6CFF),
            );
          }

          return Text(
            dow.substring(0, 1),
            textAlign: TextAlign.center,
            style: style,
          );
        },
      ),
      onRangeSelected: widget.onRangeSelected,
      onPageChanged: (focusedDay) {
        print('+++++++++++++++++change');
      },
      rangeStartDay: widget.rangeStartDay,
      rangeEndDay: widget.rangeEndDay,
      selectedDayPredicate: (day) {
        return day == selectedDay;
      },
      onDaySelected: (selected, focused) {
        setState(() {
          selectedDay = selected;
        });
      },
      calendarStyle: CalendarStyle(
        outsideDaysVisible: false,
        rangeStartDecoration: BoxDecoration(
          color: theme.primaryOrangeColor,
          shape: BoxShape.circle,
        ),
        rangeStartTextStyle: TextStyle(
          fontWeight: theme.heavyFontWeight,
          fontSize: theme.fontSize14,
          color: Colors.white,
        ),
        rangeEndDecoration: BoxDecoration(
          color: theme.primaryOrangeColor,
          shape: BoxShape.circle,
        ),
        rangeEndTextStyle: TextStyle(
          fontWeight: theme.heavyFontWeight,
          fontSize: theme.fontSize14,
          color: Colors.white,
        ),
        rangeHighlightColor: theme.primaryOrangeColor.withOpacity(0.2357),
        withinRangeTextStyle: defaultTextStyle,
        defaultTextStyle: defaultTextStyle,
        todayTextStyle: TextStyle(
          fontWeight: theme.heavyFontWeight,
          color: theme.primaryColor,
        ),
        todayDecoration: BoxDecoration(
          color: Color(0xFFD7D8EE),
          shape: BoxShape.circle,
        ),
        weekendTextStyle: defaultTextStyle,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //   children: renderRange(),
        // ),
        Container(height: 20.0),
        Container(height: 5.0, color: Color(0xFFE8E7EC)),
        renderCalendar(),
      ],
    );
  }
}
