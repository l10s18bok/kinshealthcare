import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

class PaymentAmountText extends StatefulWidget {
  final bool isPrimary;
  final double amount;
  final String label;
  final Color? customColor;

  PaymentAmountText({
    required this.amount,
    required this.label,
    this.isPrimary = false,
    this.customColor,
  });

  @override
  _PaymentAmountTextState createState() => _PaymentAmountTextState();
}

class _PaymentAmountTextState extends State<PaymentAmountText> {
  late bool isPrimary;
  late TextUtils textUtils;

  @override
  void initState() {
    super.initState();

    this.isPrimary = widget.isPrimary;
    this.textUtils = TextUtils();
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    Color color = this.isPrimary ? theme.primaryOrangeColor : Color(0xFF262626);

    if (widget.customColor != null) {
      color = widget.customColor!;
    }

    final amountStyle = TextStyle(
      color: color,
      fontWeight: theme.heavyFontWeight,
      fontSize: 20.0,
    );

    final wonStyle = amountStyle.copyWith(
      fontSize: 16.0,
      fontWeight: theme.primaryFontWeight,
    );

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          widget.label,
          style: TextStyle(
            fontSize: theme.fontSize15,
            color: Color(0xFF343434),
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              this.textUtils.numberToLocalCurrency(amount: widget.amount),
              style: amountStyle,
            ),
            Text(
              '원',
              style: wonStyle,
            ),
          ],
        ),
      ],
    );
  }
}
