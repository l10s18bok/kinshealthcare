import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

bool isTextOverFlow({
  required BuildContext context,
  required double width,
  required String message,
  required double fontSize,
}) {
  var span = TextSpan(
    text: message,
    style: TextStyle(fontSize: fontSize),
  );

  var tp = TextPainter(
    maxLines: 1,
    textAlign: TextAlign.left,
    textDirection: TextDirection.ltr,
    text: span,
  );
  var buttonWidth = 50.0;
  tp.layout(maxWidth: width + buttonWidth);
  var exceeded = tp.didExceedMaxLines;

  return exceeded;
}
