import 'package:flutter/material.dart';

class TextSwitch {
  String kkakkaTypeSwitch(String type) {
    switch (type) {
      case 'D':
        return '기부';
      case 'A':
        return '기념일';
      case 'E':
        return '교육';
      case 'M':
        return '의료';
      case 'P':
        return '용돈';
      case 'S':
        return '셀프';
      case 'W':
        return '복지';
      default:
        return '';
    }
  }
}
