import 'package:flutter/widgets.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/models/local/like_reply_model.dart';
import 'package:kins_healthcare/models/local/post_model.dart';
import 'package:kins_healthcare/services/kkakka/model/permise_me_response_model.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

class KkakkaTypeTranslate {
  KkakkaTypeTranslate({required this.model});
  dynamic model;
  final _kkakkaStatusMap = {
    '활성': KkakkaStatus.ACTIVE,
    '해제': KkakkaStatus.DROP,
    '모두사용': KkakkaStatus.USE_ALL,
    'ACTIVE': KkakkaStatus.ACTIVE,
    'DROP': KkakkaStatus.DROP,
    'USE_ALL': KkakkaStatus.USE_ALL,
  };
  final _kkakkaFilterStatusMap = {
    '요청': KkakkaFilterStatus.REQUEST,
    '승인': KkakkaFilterStatus.ACCEPTE,
    '취소': KkakkaFilterStatus.CANCEL,
    '거절': KkakkaFilterStatus.DENY,
    '활성': KkakkaFilterStatus.ACTIVE,
    '해제': KkakkaFilterStatus.DROP,
    '모두사용': KkakkaFilterStatus.USE_ALL,
  };
  final _userStatusOpponentMap = {
    '정상': UserStatus.ACTIVE,
    '탈퇴': UserStatus.DROP,
    '휴면': UserStatus.WITHDRAW,
    'ACTIVE': UserStatus.ACTIVE,
    'DROP': UserStatus.DROP,
    'WITHDRAW': UserStatus.WITHDRAW,
  };
  final _kkakkaTypeMap = {
    '기념일': KkakkaType.ANNIVERSARY,
    '기부': KkakkaType.DONATE,
    '지역': KkakkaType.AREA,
    '의료': KkakkaType.MEDICAL,
    '용돈': KkakkaType.PIN,
    'SELF': KkakkaType.SELF,
    '복지': KkakkaType.WELFARE,
    'ANNIVERSARY': KkakkaType.ANNIVERSARY,
    'DONATE': KkakkaType.DONATE,
    'AREA': KkakkaType.AREA,
    'MEDICAL': KkakkaType.MEDICAL,
    'PIN': KkakkaType.PIN,
    'WELFARE': KkakkaType.WELFARE,
  };
  final _postKkakkaTypeMap = {
    'A': KkakkaType.ANNIVERSARY,
    'D': KkakkaType.DONATE,
    'E': KkakkaType.AREA,
    'M': KkakkaType.MEDICAL,
    'P': KkakkaType.PIN,
    'S': KkakkaType.SELF,
    'W': KkakkaType.WELFARE,
  };
  get kkakkaIcName {
    Map<KkakkaType, String> dict = {
      KkakkaType.ANNIVERSARY: 'ic_anniversary_kkakka.svg',
      KkakkaType.AREA: 'ic_area_kkakka.svg',
      KkakkaType.DONATE: 'ic_donate_kkakka.svg',
      KkakkaType.MEDICAL: 'ic_medical_kkakka.svg',
      KkakkaType.PIN: 'ic_pin_kkakka.svg',
      KkakkaType.SELF: 'ic_self_kkakka.svg',
      KkakkaType.WELFARE: 'ic_welfare_kkakka.svg',
    };

    return dict[this.kkakkaType];
  }

  get primaryColor {
    Map<KkakkaType, Color> dict = {
      KkakkaType.ANNIVERSARY: Color(0xFFFF9B95),
      KkakkaType.AREA: Color(0xFFFF9B95),
      KkakkaType.DONATE: Color(0xFF8B6ADC),
      KkakkaType.MEDICAL: Color(0xFF6FA7FA),
      KkakkaType.PIN: Color(0xFF6CC4BF),
      KkakkaType.SELF: Color(0xFF6CC4BF),
      KkakkaType.WELFARE: Color(0xFF8B6ADC),
    };

    return dict[this.kkakkaType];
  }

  Color? postPrimaryColor(String type) {
    Map<String, Color> dict = {
      'A': Color(0xFFFF9B95),
      'D': Color(0xFF8B6ADC),
      'E': Color(0xFF8B6ADC),
      'M': Color(0xFF6FA7FA),
      'P': Color(0xFF6CC4BF),
      'S': Color(0xFF6CC4BF),
      'W': Color(0xFF8B6ADC),
    };

    return dict['$type'];
  }

  String? postType(String type) {
    Map<String, String> dict = {
      'A': '기념일',
      'D': '기부',
      'E': '지역',
      'M': '의료',
      'P': '용돈',
      'S': '셀프',
      'W': '복지'
    };
    return dict['$type'];
  }

  get kkakkaType {
    final type = this._kkakkaTypeMap[model.kkakkaType];

    return type ?? this._kkakkaTypeMap['기념일'];
  }

  get kkakkaStatus {
    return this._kkakkaStatusMap[model.kkakkaStatus];
  }

  get kkakkaFilterStatus {
    return this._kkakkaFilterStatusMap[model.kkakkaStatus];
  }

  get kkakkaOpponentStatus {
    assert(model.runtimeType == PermiseMeResponseModel);
    return this._userStatusOpponentMap[model.userStatusOpponent];
  }

  get daysTillEndDate {
    if (model.endDate == null) return null;
    var inday = model.endDate!.difference(DateTime.now()).inDays;
    var inHorwer = model.endDate!.difference(DateTime.now()).inHours;
    var inMinutes = model.endDate!.difference(DateTime.now()).inMinutes;
    if (inday > 0) {
      return '$inday일';
    } else if (inHorwer > 0) {
      return '$inHorwer시간';
    } else if (inMinutes > 0) {
      return '$inMinutes분';
    } else {
      return '방금';
    }
  }

  get daysTillreqDate {
    if (model.reqDate == null) return 0;
    return DateTime.now().difference(DateTime.parse(model.reqDate)).inDays;
  }

  LikeReplyModel get likeReplyModel {
    return LikeReplyModel(
      likeCount: model.likeCount,
      likeYn: model.likeYn,
      firstLikeName: model.firstLikeUserName,
      firstRepl: model.firstRepl,
      firstReplName: model.firstReplUserName,
      firstReplImg: model.firstReplImg,
      message: model.kkakkaMessage,
      isContentOpen: model.isContentOpen,
      localModel: PostLocalModel(
        boardNo: model.kkakkaId!,
        boardType: 'K',
      ),
    );
  }

  String get getStatusImg {
    switch (kkakkaStatus) {
      case KkakkaStatus.DROP:
        return 'assets/svgs/ic/ic_kkakka_active_hand.svg';
      case KkakkaStatus.USE_ALL:
        return 'assets/svgs/ic/ic_kkakka_active_money.svg';
      case KkakkaStatus.NO_PAYMENT:
        return 'assets/svgs/ic/ic_kkakka_active_time.svg';
      default:
        return 'assets/svgs/ic/ic_kkakka_active_time.svg';
    }
  }

  price(int data) {
    return data != null
        ? TextUtils().numberToLocalCurrency(
            amount: data.toDouble(),
          )
        : 0;
  }
}
