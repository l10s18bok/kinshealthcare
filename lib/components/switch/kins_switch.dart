import 'package:flutter/cupertino.dart';
import '../../utils/resource.dart';

/*
 작성일 : 2021-01-25
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : KinsSwitch,
 설명 : Custom 으로 만든 Switch Button
*/

typedef OnCheckChange = void Function(bool isShow);

// ignore: must_be_immutable
class KinsSwitch extends StatelessWidget {
  final duration = Duration(milliseconds: 100);
  final width = 50.0, height = 29.0;
  final ballSize = 25.0, ballPadding = 2.0;

  Color switchColor = kinsGrayE2;
  double switchLeft = (50.0 - 25.0) - 2;
  bool checked;
  OnCheckChange? onCheckChange;

  KinsSwitch({
    Key? key,
    this.onCheckChange,
    this.checked = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _setCheck(checked);

    return GestureDetector(
      child: AnimatedContainer(
        duration: duration,
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: switchColor,
          borderRadius: BorderRadius.all(Radius.circular(15)),
        ),
        child: renderSwitchBall(),
      ),
      onTap: () {
        if (onCheckChange == null) return;
        onCheckChange!(!checked);
      },
    );
  }

  renderSwitchBall() {
    final ballRadius = ballSize / 2;

    return Stack(
      children: [
        AnimatedPositioned(
          duration: duration,
          top: 2,
          left: switchLeft,
          child: Container(
            width: ballSize,
            height: ballSize,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(ballRadius),
              ),
              boxShadow: [
                BoxShadow(
                  color: kinsBlue44,
                  offset: Offset(2, 0),
                  blurRadius: 8,
                  spreadRadius: 0,
                )
              ],
              color: kinsWhite,
            ),
          ),
        ),
      ],
    );
  }

  _setCheck(bool checked) {
    if (checked) {
      switchColor = kinsBlue40;
      switchLeft = ballPadding;
    } else {
      switchColor = kinsGrayE2;
      switchLeft = (width - ballSize) - ballPadding;
    }
  }
}
