import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class TextTab extends StatefulWidget {
  final List<String> labels;
  final int initIndex;
  final ValueSetter<int>? onChange;
  final bool expanded;

  TextTab({
    required this.labels,
    required this.onChange,
    this.initIndex = 0,
    this.expanded = false,
  });

  @override
  _TextTabState createState() => _TextTabState();
}

class _TextTabState extends State<TextTab> {
  late int tabIndex;

  @override
  void initState() {
    super.initState();

    tabIndex = widget.initIndex;
  }

  renderTabLabel({
    required String label,
    required GestureTapCallback onTap,
    bool isPrimary = false,
  }) {
    final theme = ThemeFactory.of(context).theme;
    Color color = theme.primaryColor;

    if (!isPrimary) {
      color = Color(0xFF9C9C9C);
    }

    return InkWell(
      onTap: onTap,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            flex: widget.expanded ? 1 : 0,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: Container(
                decoration: BoxDecoration(
                  border: !isPrimary
                      ? Border()
                      : Border(
                          bottom: BorderSide(
                            color: theme.primaryColor,
                            width: 2.0,
                          ),
                        ),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 14.0),
                  child: Column(
                    children: [
                      Text(
                        label,
                        style: TextStyle(
                          color: color,
                          fontSize: theme.fontSize14,
                          fontWeight: theme.heavyFontWeight,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  renderTabs() {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Color(0xFFF0F0F0),
            width: 1.0,
          ),
        ),
      ),
      child: Row(
        children: widget.labels
            .asMap()
            .entries
            .map(
              (x) => Expanded(
                child: renderTabLabel(
                  isPrimary: x.key == tabIndex,
                  label: x.value,
                  onTap: () {
                    setState(() {
                      tabIndex = x.key;
                    });
                    if (widget.onChange != null) {
                      widget.onChange!(x.key);
                    }
                  },
                ),
              ),
            )
            .toList(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return renderTabs();
  }
}
