import 'package:flutter/cupertino.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-04-01
 작성자 : Mark,
 화면명 : (화면명),
 경로 : ,
 클래스 : CircleTap,
 설명 : CircleTap 이 PageViewIndex 가 변경될때 표시해주는 tap
*/

// ignore: must_be_immutable
class CircleTap extends StatelessWidget {
  int length;
  int currentPage;

  CircleTap({
    required this.length,
    required this.currentPage,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List<Widget>.generate(length, indicatorItem),
    );
  }

  Widget indicatorItem(int index) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      height: 10,
      width: 10,
      margin: EdgeInsets.only(right: 10),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: kinsWhite,
        ),
        shape: BoxShape.circle,
        color: (index == currentPage) ? kinsWhite : null,
      ),
    );
  }
}
