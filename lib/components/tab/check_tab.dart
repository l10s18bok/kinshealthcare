import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/button/check_button.dart';

class CheckTab extends StatefulWidget {
  final List<String> tabNames;

  // 1st param 이 tab index
  final ValueSetter<int>? onChange;

  final int initialIndex;

  CheckTab({
    required this.tabNames,
    this.onChange,
    this.initialIndex = 0,
  });

  @override
  _CheckTabState createState() => _CheckTabState();
}

class _CheckTabState extends State<CheckTab> {
  int? tabIndex;

  @override
  void initState() {
    super.initState();

    tabIndex = widget.initialIndex;
  }

  renderButtons() {
    return Row(
      children: widget.tabNames
          .asMap()
          .entries
          .map(
            (x) => CheckButton(
              label: x.value,
              isActive: this.tabIndex == x.key,
              padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 16.0),
              onTap: () {
                setState(() {
                  this.tabIndex = x.key;
                });

                if (widget.onChange != null) widget.onChange!(x.key);
              },
            ),
          )
          .toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return renderButtons();
  }
}
