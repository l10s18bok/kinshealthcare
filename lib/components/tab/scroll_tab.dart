import 'package:flutter/material.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-03-31
 작성자 : Mark,
 화면명 : (화면명),
 경로 : ,
 클래스 : ScrollTap,
 설명 : Scroll 이 이동될때 이동되는 것을 표시하기 위한 탭
*/

// ignore: must_be_immutable
class ScrollTap extends StatefulWidget {
  ScrollTap({
    Key? key,
    required this.maxCount,
    required this.scrollController,
    this.backgroundColor = Colors.transparent,
    this.barColor = kinsBlue40,
  }) : super(key: key);

  final int maxCount;
  final ScrollController scrollController;
  Color backgroundColor;
  Color barColor;

  @override
  ScrollTapState createState() => ScrollTapState();
}

class ScrollTapState extends State<ScrollTap> {
  int pageIndex = 0;
  int maxCount = 0;
  double maxWidth = 0;
  double tLVScrollPercent = 0.0;

  @override
  void initState() {
    super.initState();

    maxCount = widget.maxCount;

    widget.scrollController.addListener(() {
      final offset = widget.scrollController.offset;
      final max = widget.scrollController.position.maxScrollExtent;
      final percent = offset / max;
      final partOfScroll = 1 / maxCount;

      for (int i = 1; i <= maxCount; ++i) {
        if (partOfScroll * i >= percent) {
          pageIndex = i - 1;
          break;
        }
      }

      setState(() => tLVScrollPercent = percent);
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        maxWidth = constraints.maxWidth;
        final width = maxWidth / maxCount;
        final left = (maxWidth - width) * tLVScrollPercent;

        return Container(
          height: 2,
          child: Stack(
            children: [
              Positioned.fill(
                child: Container(height: 2, color: widget.backgroundColor),
              ),
              Positioned(
                bottom: 0,
                left: left,
                child: Container(width: width, height: 2, color: widget.barColor),
              ),
            ],
          ),
        );
      },
    );
  }
}
