class TimeDiffUtile {
  String getTimeDiff(DateTime? date, String type) {
    if (date == null) {
      return '사용기한 미설정';
    } else {
      if (type == 'K') {
        var now = DateTime.now();
        var temp = date.difference(now);
        if (temp.inDays > 0) {
          return '${temp.inDays}일 남음.';
        } else {
          return '사용기한 만료.';
        }
      } else {
        var now = DateTime.now();
        var temp = now.difference(date);
        if (temp.inDays > 0) {
          return '${temp.inDays}일 전';
        }
        if (temp.inDays == 0 && temp.inHours < 24 && temp.inHours != 0) {
          return '${temp.inHours}시간 전';
        }
        if (temp.inMinutes < 60 && temp.inMinutes > 3) {
          return '${temp.inMinutes}분 전';
        }
        if (temp.inMinutes < 3) {
          return '방금 전';
        } else {
          return '';
        }
      }
      // return '${temp.inDays},${temp.inHours},${temp.inMinutes}';
    }
  }
}
