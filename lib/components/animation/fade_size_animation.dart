import 'package:flutter/material.dart';

/*
 작성일 : 2021-01-21,
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : FadeSizeAnimation,
 설명 : AnimatedListView Item remove Animation 사라지면서 크기가 작아지는 애니메이션
*/

// ignore: must_be_immutable
class FadeSizeAnimation extends StatelessWidget {
  FadeSizeAnimation({
    Key? key,
    required this.child,
    required this.controller,
    this.containerHeight = 150,
  })  : opacity = Tween<double>(begin: 0.0, end: 1.0).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0.5, 1, curve: Curves.ease),
          ),
        ),
        height = Tween<double>(begin: 0.0, end: containerHeight).animate(
          CurvedAnimation(
            parent: controller,
            curve: Interval(0, 0.5, curve: Curves.ease),
          ),
        ),
        super(key: key);

  final Widget child;
  final Animation<double> controller;
  final Animation<double> opacity;
  final Animation<double> height;
  double containerHeight;

  Widget _buildAnimation(BuildContext? context, Widget? child) {
    return Opacity(
      opacity: opacity.value,
      child: Container(
        width: double.infinity,
        height: height.value,
        child: child,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      builder: _buildAnimation,
      animation: controller,
      child: child,
    );
  }
}
