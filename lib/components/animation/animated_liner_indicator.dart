import 'package:flutter/material.dart';

import '../../utils/resource.dart';

/*
 작성일 : 2021-02-02,
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : AnimatedLinerIndicator,
 설명 : ScrollView 이 몇 퍼센트 진행되고 있는지 상태를 Animation 으로 표현해주는 Indicator,
*/

/// Here is my library.
///
/// {@category Animation}
// ignore: must_be_immutable
class AnimatedLinerIndicator extends StatefulWidget {
  AnimatedLinerIndicator({
    Key? key,
    required this.scrollController,
    this.valueColor = kinsBlue40,
    this.backgroundColor = kinsGrayDC,
  }) : super(key: key);
  final ScrollController scrollController;
  Color valueColor;
  Color backgroundColor;

  @override
  AnimatedLinerIndicatorState createState() => AnimatedLinerIndicatorState();
}

class AnimatedLinerIndicatorState extends State<AnimatedLinerIndicator> {
  double tLVScrollPercent = 0;

  @override
  initState() {
    super.initState();

    widget.scrollController.addListener(() {
      final offset = widget.scrollController.offset;
      final max = widget.scrollController.position.maxScrollExtent;
      double percent = offset / max;

      if (percent < 0) percent = 0;
      if (percent > 1) percent = 1;
      setState(() => tLVScrollPercent = percent);
    });
  }

  @override
  Widget build(BuildContext context) {
    return LinearProgressIndicator(
      value: tLVScrollPercent,
      valueColor: AlwaysStoppedAnimation<Color>(widget.valueColor),
      backgroundColor: widget.backgroundColor,
    );
  }
}
