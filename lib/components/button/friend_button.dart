import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

enum FriendButtonVariant {
  blue,
  lightBlue,
  white,
}

class FriendButton extends StatefulWidget {
  /// [FriendButtonVariant.blue] Primary Color 배경에 흰색 글씨
  /// [FriendButtonVariant.lightBlue] 하늘색 배경에 회색 글시
  /// [FriendButtonVariant.white] 흰색 배경에 검정 글씨
  final FriendButtonVariant variant;

  /// OnTap Callback
  final GestureTapCallback? onTap;

  /// 버튼 안에 들어갈 글씨
  final String text;

  FriendButton({
    required this.variant,
    this.onTap,
    required this.text,
  });

  @override
  _FriendButtonState createState() => _FriendButtonState();
}

class _FriendButtonState extends State<FriendButton> {
  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    final borderColorMap = {
      FriendButtonVariant.blue: theme.primaryColor,
      FriendButtonVariant.lightBlue: Color(0xFFE9E9F4),
      FriendButtonVariant.white: Color(0xFFD7DBEE),
    };

    final bgMap = {
      FriendButtonVariant.blue: theme.primaryColor,
      FriendButtonVariant.lightBlue: Color(0xFFE9E9F4),
      FriendButtonVariant.white: Colors.white,
    };

    final txtColorMap = {
      FriendButtonVariant.blue: Colors.white,
      FriendButtonVariant.lightBlue: Color(0xFF696A8B),
      FriendButtonVariant.white: Color(0xFF696A8B),
    };

    final bc = borderColorMap[widget.variant];
    final bg = bgMap[widget.variant];
    final tc = txtColorMap[widget.variant];

    return ClipRRect(
      borderRadius: BorderRadius.circular(6.0),
      child: Material(
        borderRadius: BorderRadius.circular(6.0),
        color: bg,
        child: InkWell(
          onTap: widget.onTap,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6.0),
              border: Border.all(
                color: bc!,
                width: 2.0,
              ),
            ),
            child: Center(
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Text(
                  widget.text,
                  style: TextStyle(
                    fontSize: theme.fontSize13,
                    fontWeight: theme.heavyFontWeight,
                    color: tc,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
