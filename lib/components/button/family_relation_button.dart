import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class FamilyRelationButton extends StatefulWidget {
  final bool isActive;
  final String text;
  final GestureTapCallback onTap;

  FamilyRelationButton({
    required this.isActive,
    required this.text,
    required this.onTap,
  });

  @override
  _FamilyRelationButtonState createState() => _FamilyRelationButtonState();
}

class _FamilyRelationButtonState extends State<FamilyRelationButton> {
  renderButton() {
    final theme = ThemeFactory.of(context).theme;

    BoxDecoration decoration = BoxDecoration(
      border: Border.all(
        color: Color(0xFFD7D8EE),
        width: 1.0,
      ),
      borderRadius: BorderRadius.circular(6.0),
    );

    TextStyle style = TextStyle(
      color: Color(0xFFA2A2A2),
      fontSize: 15.0,
      fontWeight: theme.heavyFontWeight,
    );

    if (widget.isActive) {
      decoration = decoration.copyWith(
        border: Border.all(
          color: theme.primaryColor,
          width: 1.0,
        ),
      );

      style = style.copyWith(
        color: theme.primaryColor,
      );
    }

    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        decoration: decoration,
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 22.0),
            child: Text(
              widget.text,
              style: style,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return renderButton();
  }
}
