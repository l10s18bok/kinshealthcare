import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class ActionChipButton extends StatefulWidget {
  /// 접기 = false, 펼치기 = true
  final bool fold;

  /// 클릭시 액션
  final GestureTapCallback onTap;

  const ActionChipButton({Key? key, required this.fold, required this.onTap}) : super(key: key);
  @override
  _ActionChipButtonState createState() => _ActionChipButtonState();
}

class _ActionChipButtonState extends State<ActionChipButton> {
  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: ActionChip(
        backgroundColor: Colors.white,
        elevation: 8.0,
        padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 11),
        label: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              '세부사항 ',
              style: TextStyle(
                color: Color(0xFF4042AB),
                fontSize: theme.fontSize13,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              widget.fold ? '닫기' : '설정하기',
              style: TextStyle(
                color: Color(0xFF4042AB),
                fontSize: theme.fontSize13,
              ),
            ),
            SizedBox(
              width: 12,
            ),
            widget.fold
                ? Icon(
                    Icons.keyboard_arrow_left,
                    color: Color(0xFF96969D),
                  )
                : Icon(
                    Icons.keyboard_arrow_right,
                    color: Color(0xFF96969D),
                  )
          ],
        ),
        onPressed: widget.onTap,
        shape: StadiumBorder(
            side: BorderSide(
          width: 1,
          color: Color(0xFFD7D8EE),
        )),
      ),
    );
  }
}
