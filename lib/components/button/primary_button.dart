import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

enum PrimaryButtonTheme {
  primary,
  grey,
}

class PrimaryButton extends StatefulWidget {
  // 버튼 글자
  final String label;

  // 버튼 색깔
  final Color? bgColor;

  // 텍스트 색깔
  final Color? txtColor;

  // Button onTap
  final GestureTapCallback? onTap;

  // theme
  final PrimaryButtonTheme theme;

  // padding
  final EdgeInsets padding;

  final bool isLoading;

  PrimaryButton({
    required this.label,
    required this.onTap,
    this.isLoading = false,
    this.bgColor,
    this.txtColor,
    this.theme = PrimaryButtonTheme.primary,
    this.padding = const EdgeInsets.all(18.0),
  });

  @override
  _PrimaryButtonState createState() => _PrimaryButtonState();
}

class _PrimaryButtonState extends State<PrimaryButton> {
  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    Color bgColor = widget.theme == PrimaryButtonTheme.primary
        ? theme.primaryColor
        : Color(0xFFE0E2E6);

    if (widget.bgColor != null) {
      bgColor = widget.bgColor!;
    }

    Color txtColor = widget.theme == PrimaryButtonTheme.primary
        ? Colors.white
        : Color(0xFF868686);

    if (widget.txtColor != null) {
      txtColor = widget.txtColor!;
    }

    return Material(
      borderRadius: BorderRadius.circular(6.0),
      color: widget.onTap == null ? Color(0xFFE0E2E6) : bgColor,
      child: InkWell(
        onTap: widget.onTap,
        child: Padding(
          padding: widget.padding,
          child: Center(
            child: widget.isLoading
                ? SizedBox(
                    height: 15.0,
                    width: 15.0,
                    child: CircularProgressIndicator(),
                  )
                : Text(
                    widget.label,
                    style: TextStyle(
                      fontSize: theme.fontSize15,
                      color:
                          widget.onTap == null ? Color(0xFF868686) : txtColor,
                      fontWeight: theme.heavyFontWeight,
                    ),
                  ),
          ),
        ),
      ),
    );
  }
}
