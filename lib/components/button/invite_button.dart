import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/controllers/relation_controller.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/ok_bottom_sheet.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:cached_network_image/cached_network_image.dart';

/*
 작성일 : 2021-02-18
 작성자 : Victor
 화면명 : 
 경로 : 
 클래스 : InviteButton
 설명 : 관계 요청 버튼
*/

class InviteButton extends StatefulWidget {
  final String imagePath;
  final String invitorRelation;
  final String invitorName;
  final String inviteeRelation;
  final String inviteeName;
  final String phoneNumber;
  final bool knowsInvitor;
  final bool drawLine;
  final int invitorId;
  final VoidCallback? onDelete;

  InviteButton({
    required this.invitorId,
    this.imagePath = 'lib/screens/spinor_test/victor/sample_image/1.png',
    this.invitorRelation = '',
    this.invitorName = '',
    this.inviteeRelation = '나',
    this.inviteeName = '',
    this.phoneNumber = '010-1234-5678',
    this.knowsInvitor = false,
    this.drawLine = true,
    this.onDelete,
  });

  @override
  _InviteButtonState createState() => _InviteButtonState();
}

class _InviteButtonState extends State<InviteButton> {
  final double imageSize = 56;
  final double iconSize = 19;

  @override
  Widget build(BuildContext context) {
    final phone =
        widget.knowsInvitor ? widget.phoneNumber : '${widget.phoneNumber} (모르는 전화번호)';

    return Column(
      children: [
        space(30),
        Row(
          children: [
            Stack(
              children: [
                Container(
                  width: imageSize,
                  height: imageSize,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: CachedNetworkImage(
                      imageUrl: widget.imagePath,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  width: iconSize,
                  height: iconSize,
                  child: SvgPicture.asset(widget.knowsInvitor
                      ? 'lib/screens/spinor_test/victor/sample_image/phone_icon_lightgreen.svg'
                      : 'lib/screens/spinor_test/victor/sample_image/phone_icon_grey.svg'),
                ),
              ],
            ),
            space(15),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: relationInfo(),
                ),
                space(5),
                Text(phone),
              ],
            ),
          ],
        ),
        space(15),
        Flex(
          direction: Axis.horizontal,
          children: bottomButton(),
        ),
        space(30),
        if (widget.drawLine) Container(color: Colors.grey, height: 1),
      ],
    );
  }

  List<Widget> relationInfo() {
    final theme = ThemeFactory.of(context).theme;

    var relationStyle = TextStyle(
      fontWeight: FontWeight.bold,
      color: Colors.black,
    );
    var nameStyle = TextStyle(
      color: theme.primaryColor,
    );

    return [
      Text(
        widget.invitorRelation,
        style: relationStyle,
      ),
      space(10),
      Text(
        widget.invitorName,
        style: nameStyle,
      ),
      space(10),
      Text(
        '>',
        style: TextStyle(
          color: theme.greyPrimaryTextColor,
        ),
      ),
      space(10),
      Text(
        widget.inviteeRelation,
        style: relationStyle,
      ),
      space(10),
      Text(
        widget.inviteeName,
        style: nameStyle,
      ),
    ];
  }

  List<Widget> bottomButton() {
    final theme = ThemeFactory.of(context).theme;

    if (widget.knowsInvitor) {
      return [
        Flexible(
          flex: 8,
          child: button(
            height: 40,
            label: '관계수정',
            backColor: theme.secondaryGreyBgColor,
            textColor: theme.secondaryGreyTextColor,
            onTap: () {
              // TODO : 액션 추가
            },
          ),
        ),
        Flexible(
          flex: 1,
          child: Container(
            color: Colors.transparent,
          ),
        ),
        Flexible(
          flex: 8,
          child: button(
            height: 40,
            backColor: theme.primaryBgColor,
            textColor: theme.whiteTextColor,
            label: '수락',
            onTap: () {
              Future.microtask(() async {
                final result = await Get.find<RelationController>()
                    .relationApprove(widget.invitorId);

                if (result.resultCode == 'SUCCESS') {
                  //성공
                  final sheet = OkBottomSheet(title: '성공', content: '가족으로 추가되었습니다.');
                  Get.bottomSheet(sheet);
                  setState(() {
                    removeWidget();
                  });
                } else {
                  //실패
                  final sheet = OkBottomSheet(title: '에러', content: result.resultMsg!);
                  Get.bottomSheet(sheet);
                }
              });
            },
          ),
        ),
      ];
    } else {
      return [
        Flexible(
          child: button(
            height: 40,
            backColor: Colors.white,
            borderColor: theme.primaryColor,
            textColor: theme.primaryColor,
            label: '번호 등록',
            onTap: () {
              // TODO : 액션 추가
            },
          ),
        ),
      ];
    }
  }

  Widget space(double size) {
    return Container(
      width: size,
      height: size,
      color: Colors.transparent,
    );
  }

  Widget button({
    double width = double.infinity,
    double height = 50,
    String label = '',
    GestureTapCallback? onTap,
    Color borderColor = Colors.transparent,
    Color backColor = Colors.white,
    Color textColor = Colors.black,
  }) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: width,
        height: height,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(
            color: borderColor,
            width: 1.5,
          ),
          borderRadius: BorderRadius.circular(5),
          color: backColor,
        ),
        child: Text(
          label,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: textColor,
          ),
        ),
      ),
    );
  }

  void removeWidget() {
    if (widget.onDelete != null) widget.onDelete!();
  }
}
