import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class AdvertisementListButton extends StatefulWidget {
  /// 클릭시 액션
  final GestureTapCallback? onTap;

  /// 디렉터리 패스및 prefix 제외한 아이콘 파일이름 (assets/svgs/ic/ic_advert_hospital.svg -> hospital)
  final String icon;

  /// 제일 큰 검정 글씨
  final String label;

  /// 레이블 바로 아래 회색 글시
  final String subLabel;

  AdvertisementListButton({
    this.onTap,
    required this.icon,
    required this.label,
    required this.subLabel,
  });

  @override
  _AdvertisementListButtonState createState() => _AdvertisementListButtonState();
}

class _AdvertisementListButtonState extends State<AdvertisementListButton> {
  renderIcon() {
    return Container(
      height: 56.0,
      width: 56.0,
      decoration: BoxDecoration(
        color: Color(0xFFFFF4EF),
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: SvgPicture.asset(
          'assets/svgs/ic/ic_advert_${widget.icon}.svg',
        ),
      ),
    );
  }

  renderLabels() {
    final theme = ThemeFactory.of(context).theme;

    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            children: [
              Text(
                widget.label,
                style: TextStyle(
                  fontSize: theme.fontSize15,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text(
                widget.subLabel,
                style: TextStyle(
                  fontSize: theme.fontSize12,
                  color: Color(0xFF929292),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      behavior: HitTestBehavior.opaque,
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            renderIcon(),
            Container(width: 20.0),
            renderLabels(),
          ],
        ),
      ),
    );
  }
}
