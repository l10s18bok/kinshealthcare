import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class CheckButton extends StatefulWidget {
  final String label;
  final GestureTapCallback? onTap;
  final bool isActive;
  final EdgeInsets padding;

  CheckButton({
    required this.label,
    this.onTap,
    this.isActive = false,
    this.padding = const EdgeInsets.symmetric(vertical: 16.0),
  });

  @override
  _CheckButtonState createState() => _CheckButtonState();
}

class _CheckButtonState extends State<CheckButton> {
  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    Color iconColor = Color(0xFFA9A9A9A9);
    Color txtColor = Color(0xFF9F9F9F9F);

    if (widget.isActive) {
      iconColor = theme.primaryColor;
      txtColor = theme.primaryColor;
    }

    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: widget.onTap,
        child: Padding(
          padding: widget.padding,
          child: Row(
            children: [
              Icon(
                Icons.check,
                color: iconColor,
                size: 12.0,
              ),
              Container(width: 6.0),
              Text(
                widget.label,
                style: TextStyle(
                  color: txtColor,
                  fontSize: theme.fontSize12,
                  fontWeight:
                      widget.isActive ? theme.heavyFontWeight : theme.mediumFontWeight,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
