import 'package:cached_network_image/cached_network_image.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../utils/resource.dart';
import '../../screens/spinor_test/mark/model/box_grid_model.dart';

/*
 작성일 : 2021-01-26
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : BoxGrid,
 설명 : Box 형태의 child 를 3개씩 Grid 형태로 표시하는 위젯
*/

typedef OnLastSelectItem = void Function(BoxGridModel model);

// ignore: must_be_immutable
class BoxGrid extends StatefulWidget {
  BoxGrid({
    Key? key,
    required this.models,
    this.onLastSelectItem,
    this.isBank = false,
    this.radius = 6.0,
    this.rowCount = 3,
    this.imageSize = 42,
    this.fontSize = 12,
  }) : super(key: key);

  final List<BoxGridModel> models;
  OnLastSelectItem? onLastSelectItem;
  bool isBank;
  double radius, imageSize, fontSize;
  int rowCount;

  @override
  BoxGridState createState() => BoxGridState();
}

class BoxGridState extends State<BoxGrid> {
  BoxGridModel? selectModel;
  late int rowCount;

  @override
  Widget build(BuildContext context) {
    rowCount = widget.rowCount;
    if (rowCount < 2) rowCount = 2;
    if (rowCount > 5) rowCount = 5;

    return renderBtnGrid(widget.models);
  }

  renderBtnGrid(List<BoxGridModel> list) {
    final btnList = list.map((model) => renderBtn(model)).toList();

    List<List<Widget>> allList = [[]];
    for (int i = 0; i < btnList.length; ++i) {
      if (i % rowCount == 0) allList.add([]);

      final btn = btnList[i];
      allList[(allList.length - 1)].add(btn);
    }

    List<Widget> columnList = allList.map((rowList) => renderBtnRow(rowList)).toList();
    return Column(children: columnList);
  }

  Widget renderBtnRow(List<Widget> rowList) {
    if (rowList.length == 0) return Container();

    final padding = _getGridPadding();
    final lagePadding = _getGridLagePadding();

    List<Widget> rowItem = [];
    for (int i = 0; i < rowCount; ++i) {
      final row = rowList.length > i ? rowList[i] : Container();
      rowItem.add(Expanded(flex: 15, child: row));
      rowItem.add(SizedBox(width: widget.isBank ? lagePadding : padding));
    }

    rowItem.removeAt(rowItem.length - 1);

    return Padding(
      padding: EdgeInsets.only(bottom: padding),
      child: Row(children: rowItem),
    );
  }

  renderBtn(BoxGridModel model) {
    final borderColor = model.isSelect ? model.selectBorderColor : model.borderColor;
    final color = model.isSelect ? model.selectColor : model.backgroundColor;

    Color textColor = kinsBlue1D; // 2021.04.19 Andy 추가
    if (widget.isBank) textColor = model.isSelect ? kinsBlue1D : kinsBlack; // 2021.04.19 Andy 추가

    final imagePath = (model.isSelect && !widget.isBank) ? model.selectedImagePath : model.imagePath; // 2021.04.19 Andy 수정(기존 : model.isSelect ?)

    return InkWell(
      child: AspectRatio(
        aspectRatio: model.ratio,
        child: Container(
          alignment: Alignment.center,
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(widget.radius)),
            border: Border.all(color: borderColor, width: widget.isBank ? 2 : 1),
            color: color,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              renderRowImage(imagePath),
              SizedBox(height: imagePath == null ? 0 : 5),
              Text(
                model.title,
                style: TextStyle(
                  color: textColor,
                  fontWeight: (model.isSelect && widget.isBank) ? FontWeight.bold : FontWeight.w500, // 2021.04.19 Andy 수정(기존 : 선택없음(FontWeight.w500))
                  fontSize: _getGridFontSize(),
                ),
              ),
            ],
          ),
        ),
      ),
      onTap: () {
        _checkModels(model);
        setState(() {});
      },
    );
  }

  renderRowImage(String? path) {
    if (path == null) return Container();
    final size = _getGridImageSize();

    if (path.startsWith('http')) {
      return Padding(
        // 2021.04.19 Andy 추가
        padding: const EdgeInsets.only(left: 14, right: 14, top: 6, bottom: 11),
        child: DottedBorder(
          // 2021.04.19 Andy 추가 신규 Figma 디자인 참조
          color: Color(0xFF979797),
          strokeWidth: 1,
          child: CachedNetworkImage(
            imageUrl: path,
            fit: BoxFit.fitHeight,
            alignment: Alignment.center,
            width: double.infinity,
            height: size,
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
        ),
      );
    }

    if(widget.isBank) {
      return Image.asset(
      path,
      width: double.infinity,
      height: size,
      fit: BoxFit.fitHeight,
      alignment: Alignment.center,
    );
    }

    return SvgPicture.asset(
      path,
      width: double.infinity,
      height: size,
      fit: BoxFit.fitHeight,
      alignment: Alignment.center,
    );
  }

  _checkModels(BoxGridModel model) {
    for (BoxGridModel model in widget.models) {
      model.isSelect = false;
    }
    model.isSelect = true;
    _callBackSelectModel(model);
  }

  _callBackSelectModel(BoxGridModel model) {
    final onLastSelectModel = widget.onLastSelectItem;
    if (onLastSelectModel != null) onLastSelectModel(model);
  }

  _getGridImageSize() {
    double width = MediaQuery.of(context).size.width - 32;
    return width * (widget.imageSize / 328);
  }

  _getGridFontSize() {
    double width = MediaQuery.of(context).size.width - 32;
    return width * (widget.fontSize / 328);
  }

  _getGridPadding() {
    final paddingDp = widget.isBank ? 10 : 7;

    double width = MediaQuery.of(context).size.width - 32;
    return width * (paddingDp / 328);
  }

  _getGridLagePadding() {
    double width = MediaQuery.of(context).size.width - 32;
    return width * (14 / 328);
  }
}
