/*
 작성일 : 
 작성자 : 
 화면명 : HomeOptionModal
 경로 : 
 업데이트 :  baron 
 업데이트 날짜 : 2021-05-3
 클래스 : HomeOptionModal,
 설명 : 
 */
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/simple_information_bottom_sheet.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class HomeOptionModal extends ModalRoute<void> {
  final double x;
  final double y;
  bool? haveAccount;

  HomeOptionModal({
    required this.x,
    required this.y,
    this.haveAccount = true,
  });

  @override
  Color get barrierColor => Colors.black.withOpacity(0.6);

  @override
  bool get barrierDismissible => true;

  @override
  String? get barrierLabel => null;

  @override
  bool get maintainState => true;

  @override
  bool get opaque => false;

  @override
  // TODO: implement transitionDuration
  Duration get transitionDuration => Duration(milliseconds: 200);

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return Material(
      type: MaterialType.transparency,
      child: renderContent(context),
    );
  }

  checkAccount() async {
    try {
      final resp = await Get.find<UserController>().getUserPayCardInfo();
      if (resp.length == 0) haveAccount = false;
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  renderIconRow(
    BuildContext context, {
    required String label,
    required String imgPath,
    @required GestureTapCallback? onTap,
  }) {
    final theme = ThemeFactory.of(context).theme;
    return GestureDetector(
      onTap: onTap,
      child: Row(
        children: [
          Text(
            label,
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: theme.heavyFontWeight,
              color: Colors.white,
            ),
          ),
          Container(width: 16.0),
          Spacer(),
          Container(
            width: 35.0,
            height: 35.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: theme.primaryOrangeColor,
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: SvgPicture.asset('$imgPath', fit: BoxFit.cover),
            ),
          )
        ],
      ),
    );
  }

  renderContent(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;
    return Stack(
      children: [
        Positioned.fill(
          top: y,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IntrinsicWidth(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: Align(
                          child: Container(
                            width: 35.0,
                            height: 35.0,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle,
                            ),
                            child: Icon(
                              Icons.clear,
                              color: theme.primaryOrangeColor,
                            ),
                          ),
                          alignment: Alignment.centerRight,
                        ),
                      ),
                      Container(height: 16.0),
                      renderIconRow(
                        context,
                        label: '소식 작성하기',
                        imgPath: 'assets/svgs/ic/kkakkaHome/2.svg',
                        onTap: () async {
                          //2021.04.12 Andy 추가
                          Get.offNamed('/main-home/news-feed');
                        },
                      ),
                      Container(height: 16.0),
                      renderIconRow(
                        context,
                        label: '까까 발행하기',
                        imgPath: 'assets/svgs/ic/kkakkaHome/3.svg',
                        onTap: () async {
                          //2021.05.06 Andy 추가 + 수정
                          await checkAccount();

                          if (haveAccount!)
                            Get.offNamed('/kkakka/issuance/target');
                          else {
                            final sheet = SimpleInformationBottomSheet(
                                message: '결제수단을 등록해주세요.');
                            await Get.bottomSheet<String>(sheet);
                            Get.offNamed('/payment/methods');
                          }
                        },
                      ),
                      Container(height: 16.0),
                      renderIconRow(
                        context,
                        label: '까까 요청하기',
                        imgPath: 'assets/svgs/ic/kkakkaHome/4.svg',
                        onTap: () {
                          Get.offNamed('/kkakka/request/target');
                        },
                      ),
                      Container(height: 16.0),
                      renderIconRow(
                        context,
                        label: '기부하기',
                        imgPath: 'assets/svgs/ic/kkakkaHome/1.svg',
                        onTap: () async {
                          //2021.05.06 Andy 추가 + 수정
                          await checkAccount();
                          if (haveAccount!)
                            Get.offNamed('/kkakka/issuance/donate/target');
                          else {
                            final sheet = SimpleInformationBottomSheet(
                                message: '결제수단을 등록해주세요.');
                            await Get.bottomSheet<String>(sheet);
                            Get.offNamed('/payment/methods');
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
