import 'package:flutter/material.dart';

class CoachMarkModal extends ModalRoute<void> {
  final Widget widget;

  CoachMarkModal({
    required this.widget,
  });

  @override
  Color get barrierColor => Colors.black.withOpacity(0.75);

  @override
  bool get barrierDismissible => true;

  @override
  String? get barrierLabel => null;

  @override
  bool get maintainState => true;

  @override
  bool get opaque => false;

  @override
  Duration get transitionDuration => Duration(milliseconds: 200);

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return Material(
      type: MaterialType.transparency,
      child: Stack(
        children: [
          widget,
        ],
      ),
    );
  }
}
