import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/animation/animated_liner_indicator.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/services/kkakka/model/list_send_opponent.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

import 'card/circle_network_image.dart';

/*
 작성일 : 2021-02-02
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : PreRequestTop,
 설명 : 즐겨찾는 사람들 Components, onAddTap -> 주황색 플러스 버튼 터치시 CallBack, onListItemTap -> Components 내의 List Item 을 tap 했을 때 CallBack
*/

typedef OnListItemTap = void Function(int index);

// ignore: must_be_immutable
class RequestTop extends StatefulWidget {
  RequestTop({
    Key? key,
    this.onAddTap,
    this.onListItemTap,
    required this.list,
  }) : super(key: key);

  GestureTapCallback? onAddTap;
  OnListItemTap? onListItemTap;
  List<ListSendOpponentModel> list;

  @override
  RequestTopState createState() => RequestTopState();
}

// ignore: must_be_immutable
class RequestTopState extends State<RequestTop> {
  final scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 16),
          child: renderTopTitle(context),
        ),
        Container(height: 12.0),
        renderMain(),
      ],
    );
  }

  renderTopTitle(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;
    return Row(
      children: [
        Text(
          '최근 까까를 준 사람들',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            color: kinsBlack,
            fontSize: theme.fontSize12,
          ),
        ),
      ],
    );
  }

  renderMain() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(left: 16),
          height: 113.0,
          child: renderListView(),
        ),
        Container(height: 6.0),
        AnimatedLinerIndicator(
          scrollController: scrollController,
          valueColor: kinsOrangeF2,
          backgroundColor: kinsWhite,
        ),
      ],
    );
  }

  Widget renderListView() {
    return ListView.builder(
      controller: scrollController,
      scrollDirection: Axis.horizontal,
      itemCount: widget.list.length,
      itemBuilder: (_, index) => renderListItem(index),
    );
  }

  Widget renderListItem(int index) {
    final name = widget.list[index].userNameRel;
    final path = widget.list[index].userImgRel;
    final userNo = widget.list[index].userNoRel;

    return GestureDetector(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: EdgeInsets.only(right: 8.0),
            child: renderImageLayout(path, userNo!),
          ),
          Text(
            name ?? '',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              color: kinsGray6D,
              fontSize: ThemeFactory.of(context).theme.fontSize12,
            ),
          ),
        ],
      ),
      onTap: () => _onListItemTap(index),
    );
  }

  Widget renderImageLayout(String? path, int userNo) {
    return Container(
      height: 82,
      width: 82,
      padding: EdgeInsets.all(4),
      decoration: BoxDecoration(
        color: kinsWhite,
        borderRadius: BorderRadius.all(Radius.circular(41)),
        border: Border.all(color: kinsBlueCA),
      ),
      child: CircleNetworkImage(
        path: path,
        imageSize: 75.1,
        userNo: userNo,
        heroTag: '${path}_$userNo',
      ),
    );
  }

  _onListItemTap(int index) {
    if (widget.onListItemTap == null) return;
    widget.onListItemTap!(index);
  }
}
