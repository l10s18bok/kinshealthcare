import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class RoundedTextField extends StatefulWidget {
  final ValueChanged<String?>? onChanged;

  RoundedTextField({
    this.onChanged,
  });

  @override
  _RoundedTextFieldState createState() => _RoundedTextFieldState();
}

class _RoundedTextFieldState extends State<RoundedTextField> {
  renderSearchField() {
    final theme = ThemeFactory.of(context).theme;

    Color borderColor = Color(0xFFE8E8E8);
    Color fillColor = Color(0xFFF3F5F6);

    final inputBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(40.0),
      borderSide: BorderSide(
        color: borderColor,
        width: 1.0,
      ),
    );

    return TextField(
      onChanged: widget.onChanged,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 15),
        border: inputBorder,
        enabledBorder: inputBorder,
        focusedBorder: inputBorder,
        fillColor: fillColor,
        isDense: true,
        filled: true,
        labelText: '검색어 입력',
        labelStyle: TextStyle(
          color: Color(0xFFA1A1A1),
          fontSize: theme.fontSize14,
        ),
        floatingLabelBehavior: FloatingLabelBehavior.never,
        focusColor: borderColor,
        suffixIcon: Icon(
          Icons.search,
          color: Colors.grey,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return renderSearchField();
  }
}
