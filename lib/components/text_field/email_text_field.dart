import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/text_field/text_field.dart';
import 'package:kins_healthcare/utils/validator_utils.dart';

class EmailTextField extends StatefulWidget {
  // @spinormedia.com 끝에 디폴트로 붙이기
  final bool suffixDomain;

  final ValueSetter<String?> onSaved;

  final bool isBlack;

  /// 이메일 만들기 버튼 존재 여부
  final bool hasCreateEmail;

  /// 임의 설정 값
  final String? readOnlyValue;

  /// 커스텀 레이블
  final String customLabel;

  EmailTextField({
    required this.onSaved,
    this.isBlack = true,
    this.suffixDomain = false,
    this.hasCreateEmail = true,
    this.readOnlyValue,
    this.customLabel = '이메일',
  });

  @override
  _EmailTextFieldState createState() => _EmailTextFieldState();
}

class _EmailTextFieldState extends State<EmailTextField> {
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  onActionTap() async {
    final results = await Get.toNamed('/auth/register/email');

    if (results != null && results.email != null) {
      this.controller.text = results.email;
    }
  }

  renderBaseTextFormField() {
    return UnderlineTextFormField(
      controller: this.controller,
      validator: Validators.emailValidator,
      isBlack: widget.isBlack,
      onSaved: widget.onSaved,
      labelText: widget.customLabel,
      hintText: '정확한 이메일을 입력해주세요',
      actionText: widget.hasCreateEmail ? '이메일 만들기' : null,
      initialValue: widget.readOnlyValue,
      readOnly: widget.readOnlyValue != null,
      onActionTap: this.onActionTap,
      keyboardType: TextInputType.emailAddress,
      maxLength: 100,
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp("[a-zA-Z0-9+@.]"))
      ],
    );
  }

  renderDomainSuffixedTextField() {
    return UnderlineTextFormField(
      controller: this.controller,
      validator: Validators.emailValidator,
      isBlack: widget.isBlack,
      onSaved: widget.onSaved,
      labelText: '새 이메일 주소',
      hintText: 'name',
      initialValue: widget.readOnlyValue,
      keyboardType: TextInputType.emailAddress,
      onActionTap: this.onActionTap,
      suffixText: '@spinormedia.com',
    );
  }

  @override
  Widget build(BuildContext context) {
    if (widget.suffixDomain) {
      return renderDomainSuffixedTextField();
    } else {
      return renderBaseTextFormField();
    }
  }
}
