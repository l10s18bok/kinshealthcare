import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/text_field/underline_text_form_field.dart';
import 'package:kins_healthcare/utils/validator_utils.dart';

class SimplePwTextField extends StatefulWidget {
  final String? customLabel;

  final ValueSetter<String?> onSaved;

  final String? hintText;

  SimplePwTextField({
    required this.onSaved,
    this.customLabel,
    this.hintText,
  });

  @override
  _SimplePwTextFieldState createState() => _SimplePwTextFieldState();
}

class _SimplePwTextFieldState extends State<SimplePwTextField> {
  @override
  Widget build(BuildContext context) {
    return UnderlineTextFormField(
      labelText: widget.customLabel ?? '비밀번호',
      onSaved: widget.onSaved,
      obscureText: true,
      validator: Validators.simplePwValidator,
      hintText: widget.hintText ?? '대소문자, 숫자조합 8자리 이상',
    );
  }
}
