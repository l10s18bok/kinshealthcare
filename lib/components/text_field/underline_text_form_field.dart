import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

// ignore: must_be_immutable
class UnderlineTextFormField extends StatefulWidget {
  /// TextFormField validator
  final FormFieldValidator<String> validator;

  /// TextFormField onSaved
  final FormFieldSetter<String> onSaved;

  /// TextFormField onChange
  final ValueChanged<String>? onChange;

  /// TextFormField controller
  final TextEditingController? controller;

  /// TextFormField labelText
  final String? labelText;

  /// TextFormField hintText
  final String? hintText;

  // 2021.04.13 Andy 추가
  /// Error Text
  final String? errText;

  /// 검정테마 vs 흰색테마 true 일경우 검정
  final bool isBlack;

  /// 레이블 오른쪽 끝에 따로 버튼이 필요한경우
  final String? actionText;

  /// action 클릭시
  final GestureTapCallback? onActionTap;

  /// 커스텀 suffix
  final Widget? suffix;

  /// Suffix Text
  final String? suffixText;

  /// Obscure Text
  final bool obscureText;

  /// Sufix Icon
  final Widget? suffixIcon;

  /// Txt Alignment Vertical
  final TextAlignVertical textAlignVertical;

  /// Readonly
  final bool readOnly;

  /// initialValue
  final String? initialValue;

  /// keyboard type
  final TextInputType? keyboardType;

  /// input formatter
  final List<TextInputFormatter>? inputFormatters;

  final bool maxLengthCheck;

  final int maxLength; //2021.04.27 Andy 추가

  final FocusNode? focusNode; //2021.06.09 Andy 추가

  UnderlineTextFormField({
    required this.validator,
    required this.onSaved,
    this.onChange,
    this.obscureText = false,
    this.controller,
    this.labelText,
    this.hintText,
    this.errText,
    this.actionText,
    this.isBlack = true,
    this.onActionTap,
    this.suffix,
    this.suffixText,
    this.textAlignVertical = TextAlignVertical.top,
    this.suffixIcon,
    this.readOnly = false,
    this.initialValue,
    this.keyboardType,
    this.inputFormatters,
    this.maxLengthCheck = false,
    this.maxLength = 50,
    this.focusNode,
  });

  @override
  _UnderlineTextFormFieldState createState() => _UnderlineTextFormFieldState();
}

class _UnderlineTextFormFieldState extends State<UnderlineTextFormField> {
  late TextEditingController controller;

  var theme;

  Color? color;

  TextStyle? style;

  TextStyle? labelTextStyle;

  UnderlineInputBorder? border;

  TextStyle? hintStyle;

  @override
  void initState() {
    super.initState();

    if (widget.controller == null) {
      controller = TextEditingController(
        text: widget.initialValue != null ? widget.initialValue : '',
      );
    } else {
      controller = widget.controller!;
    }
  }

  renderAction() {
    final theme = ThemeFactory.of(context).theme;

    if (widget.actionText != null) {
      return InkWell(
        onTap: widget.onActionTap,
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: theme.primaryColor,
              width: 1.0,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 6.0,
              vertical: 4.0,
            ),
            child: Text(
              widget.actionText!,
              style: TextStyle(
                color: theme.primaryColor,
                fontSize: theme.fontSize11,
                fontWeight: theme.heavyFontWeight,
              ),
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  renderLabel() {
    if (widget.labelText != null) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            widget.labelText!,
            style: labelTextStyle,
          ),
          if (widget.actionText != null) renderAction(),
        ],
      );
    } else {
      return Container();
    }
  }

  renderSuffixIcon() {
    if (widget.suffix == null &&
        widget.suffixIcon == null &&
        !widget.readOnly) {
      return GestureDetector(
        onTap: () {
          controller.clear();
        },
        child: Container(
          width: 16.0,
          height: 16.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Color(0xFFD8D8D8),
          ),
          child: Icon(
            Icons.close,
            size: 10.0,
            color: Colors.white,
          ),
        ),
      );
    } else {
      return widget.suffix;
    }
  }

  renderTextField() {
    final theme = ThemeFactory.of(context).theme;

    final baseInputDecoration = InputDecoration(
      border: this.border,
      enabledBorder: this.border,
      focusedBorder: this.border,
      hintText: widget.hintText,
      hintStyle: this.hintStyle,
      suffixIcon: widget.suffixIcon,
      errorText: widget.errText,
      counterText: widget.maxLengthCheck ? null : '', //2021.04.27 Andy 추가
    );

    final suffixTextInputDecoration = baseInputDecoration.copyWith(
      suffixText: widget.suffixText,
      suffixStyle: TextStyle(
        color: Colors.black,
        fontSize: theme.fontSize15,
      ),
    );

    final suffixInputDecoration = baseInputDecoration.copyWith(
      suffix: renderSuffixIcon(),
    );

    if (widget.maxLengthCheck == true) {
      return TextFormField(
        controller: controller,
        validator: widget.validator,
        onSaved: widget.onSaved,
        onChanged: widget.onChange,
        style: this.style,
        readOnly: widget.readOnly ? false : widget.readOnly,
        obscureText: widget.obscureText,
        decoration: widget.suffixText != null
            ? suffixTextInputDecoration
            : suffixInputDecoration,
        cursorColor: this.color,
        textAlignVertical: widget.textAlignVertical,
        keyboardType: widget.keyboardType,
        inputFormatters:
            widget.inputFormatters == null ? [] : widget.inputFormatters,
        maxLength: widget.maxLength,
        focusNode: widget.focusNode,
      );
    } else {
      return TextFormField(
        controller: controller,
        validator: widget.validator,
        onSaved: widget.onSaved,
        onChanged: widget.onChange,
        style: this.style,
        readOnly: widget.readOnly ? false : widget.readOnly,
        obscureText: widget.obscureText,
        decoration: widget.suffixText != null
            ? suffixTextInputDecoration
            : suffixInputDecoration,
        cursorColor: this.color,
        textAlignVertical: widget.textAlignVertical,
        keyboardType: widget.keyboardType,
        inputFormatters:
            widget.inputFormatters == null ? [] : widget.inputFormatters,
        maxLength: widget.maxLength,
        focusNode: widget.focusNode,
      );
    }
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    color = widget.isBlack ? Colors.black : Colors.white;

    style = TextStyle(
      fontSize: 15.0,
      color: color,
    );

    labelTextStyle = TextStyle(
      fontSize: theme.fontSize13,
      color: color,
      fontWeight: theme.heavyFontWeight,
    );

    border = UnderlineInputBorder(
      borderSide: BorderSide(
        color: widget.isBlack ? Color(0xFF878787) : color!,
      ),
    );

    final hintColor = widget.isBlack ? Color(0xFFB6B6B6) : Color(0xFFB2ACFF);

    hintStyle = TextStyle(
      color: hintColor,
      fontSize: theme.fontSize15,
    );

    return Column(
      children: [
        renderLabel(),
        renderTextField(),
      ],
    );
  }
}
