import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/radio_button/radio_button.dart';

class SexRadioFormField extends FormField<String> {
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final String initialValue;

  SexRadioFormField({
    required this.onSaved,
    required this.validator,
    this.initialValue = 'M',
  }) : super(
          onSaved: onSaved,
          validator: validator,
          initialValue: initialValue,
          builder: (FormFieldState state) {
            return DefaultRadioButtonGroup(
              plainLayout: true,
              label: '성별',
              radios: [
                DefaultRadioButton(
                  label: '여자',
                  value: 'F',
                  onTap: (val) {
                    state.didChange('F');
                  },
                  groupValue: state.value,
                ),
                DefaultRadioButton(
                  label: '남자',
                  value: 'M',
                  onTap: (val) {
                    state.didChange('M');
                  },
                  groupValue: state.value,
                ),
              ],
            );
          },
        );
}
