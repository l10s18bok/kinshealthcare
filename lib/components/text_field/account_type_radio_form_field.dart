import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/radio_button/radio_button.dart';

class AccountTypeRadioFormField extends FormField<String> {
  // Medical - 의료 종사자
  // Ordinary - 일반인
  final FormFieldSetter<String> onSaved;
  final String initialValue;

  AccountTypeRadioFormField({
    required this.onSaved,
    this.initialValue = 'N',
  }) : super(
          onSaved: onSaved,
          initialValue: initialValue,
          builder: (FormFieldState state) {
            return DefaultRadioButtonGroup(
              plainLayout: true,
              label: '가입형태',
              radios: [
                DefaultRadioButton(
                  label: '일반인',
                  value: 'N',
                  onTap: (val) {
                    state.didChange(val);
                  },
                  groupValue: state.value,
                ),
                DefaultRadioButton(
                  label: '의료종사자',
                  value: 'Y',
                  onTap: (val) {
                    state.didChange(val);
                  },
                  groupValue: state.value,
                ),
              ],
            );
          },
        );
}
