import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kins_healthcare/utils/resource.dart';

/**
 작성일 : 2021-04-15
 작성자 : Daniel
 화면명 : HY_6002,
 클래스 : OutLineTextFieldScrollbar,
 설명 : Border를 TextField가 아닌 밖에서 하고, 스크롤 바가 있음
*/

// ignore: must_be_immutable
class OutLineTextFieldScrollbar extends StatelessWidget {
  OutLineTextFieldScrollbar({
    Key? key,
    this.controller,
    this.obscure = false,
    this.outLineColor = kinsBlack,
    this.cursorColor = kinsBlack,
    this.hintColor,
    this.textColor = Colors.black,
    this.hint,
    this.fontSize = 13,
    this.height,
    this.inputType,
    this.maxLine = 1,
    this.maxLength = 60,
    this.counterText,
    this.onChanged,
    this.isPhone = false,
    this.fontWeight,
    this.radius = 4.0,
  }) : super(key: key);

  TextEditingController? controller;
  bool obscure, isPhone;
  TextInputType? inputType;
  int maxLine;
  int maxLength;

  Color outLineColor;
  Color cursorColor;
  Color? hintColor;
  Color textColor;
  FontWeight? fontWeight;

  String? hint;
  String? counterText;

  double fontSize;
  double? height;
  double radius;
  ValueChanged<String?>? onChanged;

  @override
  Widget build(BuildContext context) {
    final ScrollController _scrollController = ScrollController();
    return Container(
      height: height,
      decoration: BoxDecoration(
        border: Border.all(
          color: Color(0xFF979797),
        ),
        borderRadius: BorderRadius.all(Radius.circular(radius)),
      ),
      padding: EdgeInsets.only(right: 9),
      child: renderRawScrollbar(_scrollController),
    );
  }

  RawScrollbar renderRawScrollbar(ScrollController _scrollController) {
    return RawScrollbar(
      isAlwaysShown: true,
      thumbColor: Color(0xFFF29061),
      thickness: 5,
      radius: Radius.circular(2.5),
      controller: _scrollController,
      child: TextField(
        scrollController: _scrollController,
        textAlignVertical: TextAlignVertical.center,
        textAlign: TextAlign.left,
        controller: controller,
        style: TextStyle(
          fontSize: fontSize,
          color: textColor,
          fontWeight: fontWeight,
        ),
        keyboardType: inputType,
        maxLines: this.maxLine,
        maxLength: this.maxLength,
        obscureText: obscure,
        onChanged: onChanged,
        decoration: InputDecoration(
          counterText: counterText,
          contentPadding: EdgeInsets.only(left: 16, top: 20),
          border: InputBorder.none,
          hintText: hint,
          hintStyle: TextStyle(color: hintColor ?? Colors.grey[300]),
        ),
        cursorColor: cursorColor,
      ),
    );
  }
}
