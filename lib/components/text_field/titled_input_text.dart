import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-02-19
 작성자 : Victor
 화면명 : 
 클래스 : TitledInputText
 경로 : 
 설명 : 제목과 설명이 있는 텍스트 입력용 위젯
*/

///제목과 설명이 있는 텍스트 입력용 위젯
// ignore: must_be_immutable
class TitledInputText extends StatefulWidget {
  /// 제목 텍스트
  final String titleText;

  /// 힌트 텍스트 (입력이 없을 경우 출력)
  final String hintText;

  /// 액션 텍스트 (입력창 오른쪽에 출력됨)
  final String actionText;

  /// 액션 텍스트 색상
  final Color actionTextColor;

  /// 액션 텍스트 클릭시의 이벤트
  final Function(String) actionTappedFunc;

  ///입력 방식
  final TextInputType inputType;

  ///비밀번호처럼 가릴지의 여부 (ture: 가림)
  final bool obscureText;

  ///최대 글자 입력 제한 (미입력시 무한)
  final int? maxLength;

  ///텍스트 입력 컨트롤러 (외부에서 넣어주는게 좋음)
  final TextEditingController? controller;

  TitledInputText({
    required this.titleText,
    this.hintText = '',
    this.actionText = '',
    this.actionTextColor = const Color(0xFF4042AB),
    required this.actionTappedFunc,
    this.inputType = TextInputType.text,
    this.obscureText = false,
    this.maxLength,
    this.controller,
  });

  @override
  State<StatefulWidget> createState() => _TitledInputTextState();
}

class _TitledInputTextState extends State<TitledInputText> {
  String get inputText => widget.controller?.text ?? '';

  @override
  Widget build(BuildContext context) {
    var _widget = Container(
      margin: EdgeInsets.all(10),
      width: double.infinity,
      child: Column(
        children: [
          title(),
          inputRow(),
          line(),
        ],
      ),
    );

    return _widget;
  }

  Widget title() {
    var theme = ThemeFactory.of(context).theme;

    return Container(
      width: double.infinity,
      child: Text(
        widget.titleText,
        textAlign: TextAlign.left,
        style: TextStyle(
          fontSize: theme.fontSize15,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget textInput() {
    var theme = ThemeFactory.of(context).theme;

    return TextField(
      keyboardType: widget.inputType,
      obscureText: widget.obscureText,
      maxLength: widget.maxLength,
      decoration: InputDecoration(
        counterText: '',
        border: InputBorder.none,
        hintText: widget.hintText,
        hintStyle: TextStyle(
          fontSize: theme.fontSize16,
          color: Colors.grey,
        ),
      ),
      controller: widget.controller,
    );
  }

  Widget actionArea() {
    var theme = ThemeFactory.of(context).theme;

    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        widget.actionTappedFunc(inputText);
      },
      child: Text(
        widget.actionText,
        textAlign: TextAlign.right,
        style: TextStyle(
          fontSize: theme.fontSize15,
          color: widget.actionTextColor,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget inputRow() {
    return Row(
      children: [
        Expanded(
          flex: 7,
          child: textInput(),
        ),
        Expanded(
          flex: 3,
          child: actionArea(),
        ),
      ],
    );
  }

  Widget line() {
    return Container(
      height: 1,
      color: Colors.black,
    );
  }
}
