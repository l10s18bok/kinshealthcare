import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/text_field/underline_text_form_field.dart';
import 'package:kins_healthcare/utils/validator_utils.dart';

class NameTextField extends StatefulWidget {
  final FormFieldSetter<String> onSavedLastName;
  final FormFieldSetter<String> onSavedFirstName;
  final String? readOnlyValueLastName;
  final String? readOnlyValueFirstName;

  NameTextField({
    required this.onSavedLastName,
    required this.onSavedFirstName,
    this.readOnlyValueLastName,
    this.readOnlyValueFirstName,
  });

  @override
  _NameTextFieldState createState() => _NameTextFieldState();
}

class _NameTextFieldState extends State<NameTextField> {
  late TextEditingController lastNameController;
  late TextEditingController firstNameController;

  @override
  void initState() {
    super.initState();

    lastNameController = TextEditingController(
      text: widget.readOnlyValueLastName == null ? '' : widget.readOnlyValueLastName,
    );
    firstNameController = TextEditingController(
      text: widget.readOnlyValueFirstName == null ? '' : widget.readOnlyValueFirstName,
    );
  }

  renderSurname() {
    return Expanded(
      flex: 1,
      child: UnderlineTextFormField(
        onSaved: widget.onSavedLastName,
        validator: Validators.lastnameValidator,
        labelText: '성',
        hintText: '성',
        readOnly: widget.readOnlyValueLastName != null,
        initialValue: widget.readOnlyValueLastName ?? '',
      ),
    );
  }

  renderName() {
    return Expanded(
      flex: 2,
      child: UnderlineTextFormField(
        onSaved: widget.onSavedFirstName,
        validator: Validators.firstnameValidator,
        labelText: '이름',
        hintText: '이름',
        readOnly: widget.readOnlyValueFirstName != null,
        initialValue: widget.readOnlyValueFirstName ?? '',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        renderSurname(),
        Container(width: 12),
        renderName(),
      ],
    );
  }
}
