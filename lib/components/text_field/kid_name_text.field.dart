import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/text_field/underline_text_form_field.dart';
import 'package:kins_healthcare/utils/validator_utils.dart';

class KidNameTextField extends StatefulWidget {
  final FormFieldSetter<String> onSavedChildNam;
  final FormFieldSetter<String> onSavedFamilyName;
  final ValueChanged<String>? onChangeLastName;
  final ValueChanged<String>? onChangeFirstName;
  final String? readOnlyValueChildName;
  final String? readOnlyValueFamilyName;

  KidNameTextField({
    required this.onSavedChildNam,
    required this.onSavedFamilyName,
    this.onChangeLastName,
    this.onChangeFirstName,
    this.readOnlyValueChildName,
    this.readOnlyValueFamilyName,
  });

  @override
  _KidNameTextFieldState createState() => _KidNameTextFieldState();
}

class _KidNameTextFieldState extends State<KidNameTextField> {
  late TextEditingController lastNameController;
  late TextEditingController firstNameController;

  @override
  void initState() {
    super.initState();

    lastNameController = TextEditingController(
      text: widget.readOnlyValueChildName == null ? '' : widget.readOnlyValueChildName,
    );
    firstNameController = TextEditingController(
      text: widget.readOnlyValueFamilyName == null ? '' : widget.readOnlyValueFamilyName,
    );
  }

  renderSurname() {
    return Expanded(
      flex: 1,
      child: UnderlineTextFormField(
        onSaved: widget.onSavedFamilyName,
        onChange: widget.onChangeLastName,
        validator: Validators.lastnameValidator,
        labelText: '성',
        hintText: '성',
        readOnly: widget.readOnlyValueFamilyName != null,
        initialValue: widget.readOnlyValueFamilyName ?? '',
      ),
    );
  }

  renderName() {
    return Expanded(
      flex: 2,
      child: UnderlineTextFormField(
        onSaved: widget.onSavedChildNam,
        onChange: widget.onChangeFirstName,
        validator: Validators.firstnameValidator,
        labelText: '이름',
        hintText: '이름',
        readOnly: widget.readOnlyValueChildName != null,
        initialValue: widget.readOnlyValueChildName ?? '',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        renderSurname(),
        Container(width: 12),
        renderName(),
      ],
    );
  }
}
