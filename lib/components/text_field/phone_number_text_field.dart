import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/drop_down/drop_down.dart';
import 'package:kins_healthcare/components/text_field/text_field.dart';
import 'package:kins_healthcare/utils/validator_utils.dart';

class PhoneNumberTextField extends StatefulWidget {
  final ValueSetter<String?> onPhoneSaved;
  final ValueSetter onCountryCodeSaved;
  final String? readOnlyValue;
  final bool showCountryCode;

  PhoneNumberTextField({
    required this.onPhoneSaved,
    required this.onCountryCodeSaved,
    this.readOnlyValue,
    this.showCountryCode = true,
  });

  @override
  _PhoneNumberTextFieldState createState() => _PhoneNumberTextFieldState();
}

class _PhoneNumberTextFieldState extends State<PhoneNumberTextField> {
  renderBody() {
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (widget.showCountryCode)
            Expanded(
              flex: 1,
              child: CountryDropDownFormField(
                label: '핸드폰 번호',
                onSaved: widget.onCountryCodeSaved,
              ),
            ),
          if (widget.showCountryCode) Container(width: 12.0),
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                UnderlineTextFormField(
                  labelText: widget.showCountryCode ? '' : '핸드폰 번호',
                  validator: Validators.phoneValidator,
                  onSaved: widget.onPhoneSaved,
                  hintText: '- 는 빼고 입력해주세요',
                  maxLength: 11,
                  keyboardType: TextInputType.phone,
                  readOnly: widget.readOnlyValue != null,
                  initialValue: widget.readOnlyValue ?? '',
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        renderBody(),
      ],
    );
  }
}
