import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/text_field/text_field.dart';

class DobTextFormField extends StatefulWidget {
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final ValueChanged<String>? onChange; // 2021.04.15 Andy 추가
  final String? readOnlyValue;

  DobTextFormField({
    required this.onSaved,
    required this.validator,
    this.onChange,
    this.readOnlyValue,
  });

  @override
  _DobTextFormFieldState createState() => _DobTextFormFieldState();
}

class _DobTextFormFieldState extends State<DobTextFormField> {
  @override
  Widget build(BuildContext context) {
    return UnderlineTextFormField(
      onSaved: widget.onSaved,
      onChange: widget.onChange,
      validator: widget.validator,
      labelText: '생년월일',
      hintText: 'YYYYMMDD',
      maxLength: 8,
      keyboardType: TextInputType.number,
      readOnly: widget.readOnlyValue != null,
      initialValue: widget.readOnlyValue ?? '',
    );
  }
}
