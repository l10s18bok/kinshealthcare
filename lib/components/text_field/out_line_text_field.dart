import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-01-22
 작성자 : Mark,
 화면명 : (화면 코드 입력),
 클래스 : OutLineTfCS,
 설명 : 바깥쪽에  Line 이 있는 TextField, Line 의 색 변경도 가능함
*/

// ignore: must_be_immutable
class OutLineTfCS extends StatelessWidget {
  OutLineTfCS({
    Key? key,
    this.controller,
    this.obscure = false,
    this.outLineColor = kinsBlack,
    this.cursorColor = kinsBlack,
    this.hintColor,
    this.textColor = Colors.black,
    this.hint,
    this.fontSize = 13,
    this.height,
    this.inputType,
    this.maxLine = 1,
    this.maxLength = 60,
    this.counterText,
    this.onChanged,
    this.isPhone = false,
    this.fontWeight,
    this.radius = 4.0,
  }) : super(key: key);

  TextEditingController? controller;
  bool obscure, isPhone;
  TextInputType? inputType;
  int maxLine;
  int maxLength;

  Color outLineColor;
  Color cursorColor;
  Color? hintColor;
  Color textColor;
  FontWeight? fontWeight;

  String? hint;
  String? counterText;

  double fontSize;
  double? height;
  double radius;
  ValueChanged<String?>? onChanged;

  @override
  Widget build(BuildContext context) {
    List<TextInputFormatter>? formatters = null;
    if (isPhone == true) {
      formatters = <TextInputFormatter>[
        LengthLimitingTextInputFormatter(11),
        FilteringTextInputFormatter.digitsOnly,
      ];
    }

    return Container(
      height: height,
      child: TextField(
        textAlignVertical: TextAlignVertical.center,
        textAlign: TextAlign.left,
        controller: controller,
        style: TextStyle(
          fontSize: fontSize,
          color: textColor,
          fontWeight: fontWeight,
        ),
        keyboardType: inputType,
        maxLines: this.maxLine,
        maxLength: this.maxLength,
        obscureText: obscure,
        onChanged: onChanged,
        decoration: InputDecoration(
          counterText: counterText,
          contentPadding: EdgeInsets.only(left: 16, top: 16),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: outLineColor),
            borderRadius: BorderRadius.all(Radius.circular(radius)),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: outLineColor),
            borderRadius: BorderRadius.all(Radius.circular(radius)),
          ),
          hintText: hint,
          hintStyle: TextStyle(color: hintColor ?? Colors.grey[300]),
        ),
        cursorColor: cursorColor,
        inputFormatters: formatters,
      ),
    );
  }
}
