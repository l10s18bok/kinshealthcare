import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/text_field/text_field.dart';
import 'package:kins_healthcare/utils/validator_utils.dart';

class PasswordTextField extends StatefulWidget {
  /// 비밀번호 확인 여부
  final bool isConfirm;

  /// 직접 레이블을 정하고 싶을때 입력
  final String? customLabel;

  final ValueSetter<String?> onSaved;

  /// true 일경우 검정테마
  final bool isBlack;

  PasswordTextField({
    required this.onSaved,
    this.isBlack = true,
    this.isConfirm = false,
    this.customLabel,
  });

  @override
  _PasswordTextFieldState createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {
  @override
  Widget build(BuildContext context) {
    return UnderlineTextFormField(
      labelText: widget.customLabel == null
          ? widget.isConfirm
              ? ''
              : '비밀번호'
          : widget.customLabel!,
      isBlack: widget.isBlack,
      onSaved: widget.onSaved,
      obscureText: true,
      validator: Validators.passwordValidator,
      hintText: widget.isConfirm ? '비밀번호 확인' : '대소문자 + 숫자 특수문자 조합 8자리 이상',
    );
  }
}
