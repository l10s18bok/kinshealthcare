import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/components/card/payment_card.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/services/payment/model/payment_kkakka_list_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class PaymentFormField extends StatefulWidget {
  final PostPaymentKkakkaListResponse kkakka;
  final ValueChanged<String>? onChanged;
  final FocusNode? focusNode;
  final String name;
  final int totalAmount;
  final FormFieldValidator<String>? validator;
  final String avatarImage;

  PaymentFormField({
    required this.kkakka,
    this.onChanged,
    required this.name,
    required this.totalAmount,
    required this.validator,
    required this.avatarImage,
    this.focusNode,
  });

  @override
  _PaymentFormFieldState createState() => _PaymentFormFieldState();
}

class _PaymentFormFieldState extends State<PaymentFormField> {
  late FocusNode focusNode;
  bool showKeyboard = false;

  @override
  initState() {
    super.initState();

    focusNode = widget.focusNode == null ? FocusNode() : widget.focusNode!;

    focusNode.addListener(() {
      setState(() {
        showKeyboard = true;
      });
    });
  }

  @override
  dispose() {
    focusNode.dispose();
    super.dispose();
  }

  renderTextFormField() {
    final theme = ThemeFactory.of(context).theme;

    final labelStyle = TextStyle(
      fontSize: theme.fontSize11,
      fontWeight: theme.heavyFontWeight,
      color: Color(0xFF4C4C4C),
    );

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              widget.kkakka.kkakkaType!,
              style: labelStyle,
            ),
            RichText(
              text: TextSpan(
                text: '1회 한도',
                style: labelStyle,
                children: [
                  TextSpan(
                    text: " ${widget.kkakka.kkakkaLimit}",
                    style: labelStyle.copyWith(
                      fontWeight: theme.mediumFontWeight,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        Container(
          height: 8.0,
        ),
        TextFormField(
          focusNode: focusNode,
          onChanged: widget.onChanged,
          style: TextStyle(
            color: Colors.black,
          ),
          validator: widget.validator,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: '0',
            hintStyle: TextStyle(
              color: Color(0xFF7E7E7E),
            ),
            suffixIcon: Padding(
              padding: EdgeInsets.only(right: 12.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '원',
                    style: TextStyle(
                      color: Color(0xFF7E7E7E),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
          ),
          keyboardType: TextInputType.number,
        ),
      ],
    );
  }

  getImageName() {
    String basePath = 'assets/svgs/ic/';
    switch (widget.kkakka.parsedType) {
      // case KkakkaType.WELFARE:
      // case KkakkaType.SELF:
      case KkakkaType.MEDICAL:
        basePath += 'ic_medical_pill.svg';
        break;
      // case KkakkaType.AREA:
      case KkakkaType.DONATE:
        basePath += 'ic_donation_bean.svg';
        break;
      case KkakkaType.ANNIVERSARY:
        basePath += 'ic_wedding_ring.svg';
        break;
      case KkakkaType.PIN:
        basePath += 'ic_cash_money.svg';
        break;
      case KkakkaType.EDUCATION:
        basePath += 'ic_education_book.svg';
        break;
      default:
        return null;
    }

    return basePath;
  }

  getColor() {
    final theme = ThemeFactory.of(context).theme;

    switch (widget.kkakka.parsedType) {
      // case KkakkaType.WELFARE:
      // case KkakkaType.SELF:
      case KkakkaType.MEDICAL:
        return theme.primarySkyBlueColor;
      // case KkakkaType.AREA:
      case KkakkaType.DONATE:
        return theme.primaryPurpleColor;
      case KkakkaType.ANNIVERSARY:
        return theme.primaryPinkColor;
      case KkakkaType.PIN:
        return theme.primaryGreenColor;
      case KkakkaType.EDUCATION:
        return theme.primaryYellowColor;
      default:
        return null;
    }
  }

  renderCard() {
    return Expanded(
      child: Container(
        color: Colors.white,
        child: Stack(
          children: [
            Positioned(
              right: 0,
              top: 0,
              child: getImageName() == null
                  ? Container()
                  : SvgPicture.asset(
                      getImageName(),
                    ),
            ),
            PaymentCard(
              primaryColor: getColor(),
              name: widget.name,
              amount: widget.totalAmount.toDouble(),
              bgImage: NetworkImage(
                widget.avatarImage,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        renderCard(),
        Container(width: 12.0),
        Expanded(
          child: renderTextFormField(),
        ),
      ],
    );
  }
}
