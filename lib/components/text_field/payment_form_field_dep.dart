import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/card/payment_card.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class PaymentFormFieldDep extends FormField<String> {
  PaymentFormFieldDep({
    FormFieldSetter<String?>? onSaved,
    FormFieldValidator<String?>? validator,
    String initialValue = '',
    ValueChanged<String?>? onChanged,
    TextEditingController? controller,
    String title = '용돈',
    String unit = '1회 한도',
    String amount = '60,000 만원',
  }) : super(
          onSaved: onSaved,
          validator: validator,
          initialValue: initialValue,
          autovalidateMode: AutovalidateMode.always,
          builder: (FormFieldState<String> state) {
            final theme = ThemeFactory.of(state.context).theme;

            renderInputFormField() {
              final labelStyle = TextStyle(
                fontSize: theme.fontSize11,
                fontWeight: theme.heavyFontWeight,
                color: Color(0xFF4C4C4C),
              );

              return Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        title,
                        style: labelStyle,
                      ),
                      RichText(
                        text: TextSpan(
                          text: unit,
                          style: labelStyle,
                          children: [
                            TextSpan(
                              text: " $amount",
                              style: labelStyle.copyWith(
                                fontWeight: theme.mediumFontWeight,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    height: 8.0,
                  ),
                  TextFormField(
                    controller: controller,
                    onChanged: (val) {
                      if (onChanged != null) onChanged(val);
                      state.didChange(val);
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      suffixText: '원',
                    ),
                  ),
                ],
              );
            }

            return Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: PaymentCard(
                        primaryColor: theme.primaryColor,
                        name: '잘생긴 우리 형',
                        amount: 30000,
                        bgImage: null,
                      ),
                    ),
                    Container(width: 12.0),
                    Expanded(
                      child: renderInputFormField(),
                    ),
                  ],
                ),
                if (state.hasError)
                  Padding(
                    padding: EdgeInsets.only(top: 4.0),
                    child: Row(
                      children: [
                        Icon(
                          Icons.info_outline,
                          size: 12.0,
                          color: Color(0xFFDC5050),
                        ),
                        Text(
                          state.errorText!,
                          style: TextStyle(
                            color: Color(0xFFDC5050),
                            fontSize: theme.fontSize13,
                          ),
                        ),
                      ],
                    ),
                  ),
              ],
            );
          },
        );
}
