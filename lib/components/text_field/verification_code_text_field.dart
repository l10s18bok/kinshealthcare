import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/text_field/underline_text_form_field.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class VerificationCodeTextField extends StatefulWidget {
  final FormFieldSetter<String?> onSaved;

  /// 몇초 남았는지
  final int countDown;

  VerificationCodeTextField({
    required this.onSaved,
    required this.countDown,
  });

  @override
  _VerificationCodeTextFieldState createState() => _VerificationCodeTextFieldState();
}

class _VerificationCodeTextFieldState extends State<VerificationCodeTextField> {
  parseCountdown() {
    return '${(widget.countDown / 60).floor().toString().padLeft(2, '0')}:${(widget.countDown % 60).toString().padLeft(2, '0')}';
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return UnderlineTextFormField(
      labelText: '인증번호',
      hintText: '인증번호 입력',
      obscureText: true,
      suffixIcon: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            '남은시간 ${parseCountdown()}',
            style: TextStyle(
              color: Color(0xFFDC5050),
              fontWeight: theme.heavyFontWeight,
              fontSize: theme.fontSize13,
            ),
          ),
        ],
      ),
      textAlignVertical: TextAlignVertical.center,
      keyboardType: TextInputType.number,
      validator: (val) {
        return null;
      },
      onSaved: widget.onSaved,
    );
  }
}
