import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/text_field/underline_text_form_field.dart';
import 'package:kins_healthcare/controllers/comm_controller.dart';
import 'package:kins_healthcare/services/comm/model/cert_request_sms_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/validator_utils.dart';

/*
 * 작성일 : 2021-03-02
 * 작성자 : JC
 * 화면명 : HR_0002
 * 경로 :
 * 클래스 : VerificationTargetTextField
 * 설명 : 핸드폰번호 인증 필드
 */
class VerificationTargetTextField extends StatefulWidget {
  /// onSaved
  final FormFieldSetter<String> onSaved;

  /// 초기값 (readonly)
  final String initialValue;

  /// 레이블
  final String label;

  /// 재전송 클릭시
  final GestureTapCallback onResendTap;

  VerificationTargetTextField({
    required this.onSaved,
    required this.initialValue,
    required this.label,
    required this.onResendTap,
  });

  @override
  _VerificationTargetTextFieldState createState() => _VerificationTargetTextFieldState();
}

class _VerificationTargetTextFieldState extends State<VerificationTargetTextField> {
  onResendCode() async {
    try {
      await Get.find<CommController>().certRequestSms(
        body: CertRequestSmsBody(
          phone: widget.initialValue,
        ),
      );

      Get.snackbar(
        '인증번호 전송',
        '${widget.initialValue}로 인증번호가 전송되었습니다.',
      );
    } on DioError catch (e) {
      Get.snackbar(
        '인증번호 전송 실패',
        e.response!.data['resultMsg'],
      );
    }
  }

  renderTextField() {
    final theme = ThemeFactory.of(context).theme;

    return UnderlineTextFormField(
      labelText: widget.label,
      validator: Validators.phoneValidator,
      onSaved: widget.onSaved,
      readOnly: true,
      keyboardType: TextInputType.number,
      initialValue: widget.initialValue,
      suffixIcon: InkWell(
        onTap: widget.onResendTap,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '인증번호 재전송',
              style: TextStyle(
                fontSize: theme.fontSize13,
                fontWeight: theme.heavyFontWeight,
                color: theme.primaryColor,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return renderTextField();
  }
}
