/// OAuth client id
///
/// {@category Const}
const String AUTH_CLIENT_ID = 'kinspay2020';

/// OAuth client secret
///
/// {@category Const}
const String AUTH_CLIENT_SECRET = 'kinspaySm2020';

/// BaeURL
///
/// {@category Const}
const String DEV_HOST = 'kins-healthcare-dev.kinsapps.com';

/// Test user email
///
/// {@category Const}
const String TEST_USER_EMAIL = 'test1@test.com';

/// Test user password
///
/// {@category Const}
const String TEST_USER_PASSWORD = 'test';

/// Grant type
///
/// {@category Const}
const String GRANT_TYPE = 'password';

/// Scope
///
/// {@category Const}
const String OAUTH_SCOPE = 'read write';

/// Whether to log Dio Requests
///
/// {@category Const}
const bool LOG_HTTP_REQUESTS = true;
