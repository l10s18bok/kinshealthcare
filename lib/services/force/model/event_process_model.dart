import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'event_process_model.g.dart';

@JsonSerializable()
class EventProcessBody extends BaseRetrofitModel {
  final int eventParam;
  final String processType;

  EventProcessBody({
    required this.eventParam,
    required this.processType,
  });

  factory EventProcessBody.fromJson(Map<String, dynamic> json) =>
      _$EventProcessBodyFromJson(json);

  Map<String, dynamic> toJson() => _$EventProcessBodyToJson(this);
}
