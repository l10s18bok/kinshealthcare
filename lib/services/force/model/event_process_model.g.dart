// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_process_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventProcessBody _$EventProcessBodyFromJson(Map<String, dynamic> json) {
  return EventProcessBody(
    eventParam: json['eventParam'] as int,
    processType: json['processType'] as String,
  );
}

Map<String, dynamic> _$EventProcessBodyToJson(EventProcessBody instance) =>
    <String, dynamic>{
      'eventParam': instance.eventParam,
      'processType': instance.processType,
    };
