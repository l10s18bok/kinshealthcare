import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

part 'event_under_model.g.dart';

@JsonSerializable()
class EventUnderResponse extends BaseRetrofitModel {
  final String amount;
  final int eventParam;
  final String setleDt;
  final String endDt;

  EventUnderResponse({
    required this.amount,
    required this.eventParam,
    required this.setleDt,
    required this.endDt,
  });

  String get parsedAmount => TextUtils().numberToLocalCurrency(
        amount: double.parse(this.amount),
      );

  DateTime get parsedSetleDt => DateTime.parse(setleDt);

  DateTime get parsedEndDt => DateTime.parse(endDt);

  String get timeDiffInStr {
    final now = DateTime.now();
    final diff = parsedEndDt.difference(now).inSeconds;

    if (diff > 59) {
      return '${(diff / 60).floor()}분 ${diff % 60}초';
    } else if (diff > 0) {
      return '${diff % 60}초';
    } else {
      return '만료';
    }
  }

  factory EventUnderResponse.fromJson(Object? json) =>
      _$EventUnderResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$EventUnderResponseToJson(this);
}
