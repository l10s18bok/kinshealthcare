import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'event_detail_model.g.dart';

@JsonSerializable()
class EventDetailBody extends BaseRetrofitModel {
  final String eventParam;

  EventDetailBody({
    required this.eventParam,
  });

  factory EventDetailBody.fromJson(Map<String, dynamic> json) =>
      _$EventDetailBodyFromJson(json);

  Map<String, dynamic> toJson() => _$EventDetailBodyToJson(this);
}

@JsonSerializable()
class EventDetailResponse extends BaseRetrofitModel {
  final EventDetailResponseEvent event;
  final String eventType;
  final EventDetailResponseInfo info;
  final EventDetailResponseRelation relation;

  EventDetailResponse({
    required this.event,
    required this.eventType,
    required this.info,
    required this.relation,
  });

  bool get isKkakka => this.eventType == 'KKAKKA';

  factory EventDetailResponse.fromJson(Object? json) =>
      _$EventDetailResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$EventDetailResponseToJson(this);
}

@JsonSerializable()
class EventDetailResponseEvent extends BaseRetrofitModel {
  final int eventId;
  final int eventParam;
  final String eventType;
  final String regDt;
  final int total;

  EventDetailResponseEvent({
    required this.eventId,
    required this.eventParam,
    required this.eventType,
    required this.regDt,
    required this.total,
  });

  factory EventDetailResponseEvent.fromJson(Map<String, dynamic> json) =>
      _$EventDetailResponseEventFromJson(json);

  Map<String, dynamic> toJson() => _$EventDetailResponseEventToJson(this);
}

@JsonSerializable()
class EventDetailResponseRelation extends BaseRetrofitModel {
  final String userRelCdMe;
  final String userRelCdOpponent;
  final String userNameOpponent;
  final String userPhoneOpponent;
  final String userStatusOpponent;
  final String profileImgOpponent;

  EventDetailResponseRelation({
    required this.userRelCdMe,
    required this.userRelCdOpponent,
    required this.userNameOpponent,
    required this.userPhoneOpponent,
    required this.userStatusOpponent,
    required this.profileImgOpponent,
  });

  factory EventDetailResponseRelation.fromJson(Map<String, dynamic> json) =>
      _$EventDetailResponseRelationFromJson(json);

  Map<String, dynamic> toJson() => _$EventDetailResponseRelationToJson(this);
}

@JsonSerializable()
class EventDetailResponseInfo extends BaseRetrofitModel {
  final int? paymentId;
  final int? setleId;
  final int? storeId;
  final int? userNoRel;
  final int? amount;
  final String? setleDt;
  final String? endDt;
  final String? storeNm;
  final String? address1;
  final String? address2;
  final int? kkakkaId;
  final int? kkakkaUserNo;
  final int? kkakkaUserNoRel;
  final String? kkakkaMessage;
  final String? kkakkaImage;

  EventDetailResponseInfo({
    this.paymentId,
    this.setleId,
    this.storeId,
    this.userNoRel,
    this.amount,
    this.setleDt,
    this.endDt,
    this.storeNm,
    this.address1,
    this.address2,
    this.kkakkaId,
    this.kkakkaUserNo,
    this.kkakkaUserNoRel,
    this.kkakkaMessage,
    this.kkakkaImage,
  });

  factory EventDetailResponseInfo.fromJson(Map<String, dynamic> json) =>
      _$EventDetailResponseInfoFromJson(json);

  Map<String, dynamic> toJson() => _$EventDetailResponseInfoToJson(this);
}
