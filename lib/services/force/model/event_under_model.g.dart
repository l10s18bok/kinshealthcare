// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_under_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventUnderResponse _$EventUnderResponseFromJson(Map<String, dynamic> json) {
  return EventUnderResponse(
    amount: json['amount'] as String,
    eventParam: json['eventParam'] as int,
    setleDt: json['setleDt'] as String,
    endDt: json['endDt'] as String,
  );
}

Map<String, dynamic> _$EventUnderResponseToJson(EventUnderResponse instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'eventParam': instance.eventParam,
      'setleDt': instance.setleDt,
      'endDt': instance.endDt,
    };
