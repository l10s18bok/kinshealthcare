// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventDetailBody _$EventDetailBodyFromJson(Map<String, dynamic> json) {
  return EventDetailBody(
    eventParam: json['eventParam'] as String,
  );
}

Map<String, dynamic> _$EventDetailBodyToJson(EventDetailBody instance) =>
    <String, dynamic>{
      'eventParam': instance.eventParam,
    };

EventDetailResponse _$EventDetailResponseFromJson(Map<String, dynamic> json) {
  return EventDetailResponse(
    event: EventDetailResponseEvent.fromJson(
        json['event'] as Map<String, dynamic>),
    eventType: json['eventType'] as String,
    info:
        EventDetailResponseInfo.fromJson(json['info'] as Map<String, dynamic>),
    relation: EventDetailResponseRelation.fromJson(
        json['relation'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$EventDetailResponseToJson(
        EventDetailResponse instance) =>
    <String, dynamic>{
      'event': instance.event,
      'eventType': instance.eventType,
      'info': instance.info,
      'relation': instance.relation,
    };

EventDetailResponseEvent _$EventDetailResponseEventFromJson(
    Map<String, dynamic> json) {
  return EventDetailResponseEvent(
    eventId: json['eventId'] as int,
    eventParam: json['eventParam'] as int,
    eventType: json['eventType'] as String,
    regDt: json['regDt'] as String,
    total: json['total'] as int,
  );
}

Map<String, dynamic> _$EventDetailResponseEventToJson(
        EventDetailResponseEvent instance) =>
    <String, dynamic>{
      'eventId': instance.eventId,
      'eventParam': instance.eventParam,
      'eventType': instance.eventType,
      'regDt': instance.regDt,
      'total': instance.total,
    };

EventDetailResponseRelation _$EventDetailResponseRelationFromJson(
    Map<String, dynamic> json) {
  return EventDetailResponseRelation(
    userRelCdMe: json['userRelCdMe'] as String,
    userRelCdOpponent: json['userRelCdOpponent'] as String,
    userNameOpponent: json['userNameOpponent'] as String,
    userPhoneOpponent: json['userPhoneOpponent'] as String,
    userStatusOpponent: json['userStatusOpponent'] as String,
    profileImgOpponent: json['profileImgOpponent'] as String,
  );
}

Map<String, dynamic> _$EventDetailResponseRelationToJson(
        EventDetailResponseRelation instance) =>
    <String, dynamic>{
      'userRelCdMe': instance.userRelCdMe,
      'userRelCdOpponent': instance.userRelCdOpponent,
      'userNameOpponent': instance.userNameOpponent,
      'userPhoneOpponent': instance.userPhoneOpponent,
      'userStatusOpponent': instance.userStatusOpponent,
      'profileImgOpponent': instance.profileImgOpponent,
    };

EventDetailResponseInfo _$EventDetailResponseInfoFromJson(
    Map<String, dynamic> json) {
  return EventDetailResponseInfo(
    paymentId: json['paymentId'] as int?,
    setleId: json['setleId'] as int?,
    storeId: json['storeId'] as int?,
    userNoRel: json['userNoRel'] as int?,
    amount: json['amount'] as int?,
    setleDt: json['setleDt'] as String?,
    endDt: json['endDt'] as String?,
    storeNm: json['storeNm'] as String?,
    address1: json['address1'] as String?,
    address2: json['address2'] as String?,
    kkakkaId: json['kkakkaId'] as int?,
    kkakkaUserNo: json['kkakkaUserNo'] as int?,
    kkakkaUserNoRel: json['kkakkaUserNoRel'] as int?,
    kkakkaMessage: json['kkakkaMessage'] as String?,
    kkakkaImage: json['kkakkaImage'] as String?,
  );
}

Map<String, dynamic> _$EventDetailResponseInfoToJson(
        EventDetailResponseInfo instance) =>
    <String, dynamic>{
      'paymentId': instance.paymentId,
      'setleId': instance.setleId,
      'storeId': instance.storeId,
      'userNoRel': instance.userNoRel,
      'amount': instance.amount,
      'setleDt': instance.setleDt,
      'endDt': instance.endDt,
      'storeNm': instance.storeNm,
      'address1': instance.address1,
      'address2': instance.address2,
      'kkakkaId': instance.kkakkaId,
      'kkakkaUserNo': instance.kkakkaUserNo,
      'kkakkaUserNoRel': instance.kkakkaUserNoRel,
      'kkakkaMessage': instance.kkakkaMessage,
      'kkakkaImage': instance.kkakkaImage,
    };
