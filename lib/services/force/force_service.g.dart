// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'force_service.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _ForceService implements ForceService {
  _ForceService(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://kins-healthcare-dev.kinsapps.com/api/force';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ListResponseModel<EventUnderResponse>> postEventUnder(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<EventUnderResponse>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/event/under',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<EventUnderResponse>.fromJson(
      _result.data!,
      (json) => EventUnderResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<SingleItemResponseModelTemp<EventDetailResponse>> postEventDetail(
      body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<SingleItemResponseModelTemp<EventDetailResponse>>(
            Options(
                    method: 'POST',
                    headers: <String, dynamic>{r'accessToken': null},
                    extra: _extra)
                .compose(_dio.options, '/event/detail',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = SingleItemResponseModelTemp<EventDetailResponse>.fromJson(
      _result.data!,
      (json) => EventDetailResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<dynamic> postEventProcess(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch(_setStreamType<dynamic>(Options(
            method: 'POST',
            headers: <String, dynamic>{r'accessToken': null},
            extra: _extra)
        .compose(_dio.options, '/event/process',
            queryParameters: queryParameters, data: _data)
        .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = _result.data!;
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
