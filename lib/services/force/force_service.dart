import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/force/model/event_detail_model.dart';
import 'package:kins_healthcare/services/force/model/event_process_model.dart';
import 'package:kins_healthcare/services/force/model/event_under_model.dart';
import 'package:retrofit/http.dart';

import '../base_service.dart';

part 'force_service.g.dart';

@RestApi(baseUrl: 'https://$DEV_HOST/api/force')
abstract class ForceService extends BaseService {
  factory ForceService(Dio dio, {String baseUrl}) = _ForceService;

  @POST('/event/under')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<EventUnderResponse>> postEventUnder(
    @Body() GeneralPaginationBody body,
  );

  @POST('/event/detail')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseModelTemp<EventDetailResponse>> postEventDetail(
    @Body() EventDetailBody body,
  );

  @POST('/event/process')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future postEventProcess(
    @Body() EventProcessBody body,
  );
}
