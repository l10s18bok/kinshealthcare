import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/comm/model/result_form_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/relation/model/approve_models.dart';
import 'package:kins_healthcare/services/relation/model/drop_models.dart';
import 'package:kins_healthcare/services/relation/model/know_together_model.dart';
import 'package:kins_healthcare/services/relation/model/post_relation_search_models.dart';
import 'package:kins_healthcare/services/relation/model/reject_models.dart';
import 'package:kins_healthcare/services/relation/model/request_models.dart';
import 'package:kins_healthcare/services/relation/model/search_models.dart';
import 'package:kins_healthcare/services/relation/model/search_rel_req_models.dart';
import 'package:kins_healthcare/services/relation/model/search_user_by_phone_models.dart';
import 'package:kins_healthcare/services/relation/relation_require_opponent_model.dart';
import 'package:retrofit/http.dart';

part 'relation_service.g.dart';

/*
 작성일 : 2021-02-16
 작성자 : phil,
 화면명 :
 경로 :
 클래스 : RelationService,
 설명 : 관계조회
*/

@RestApi(baseUrl: 'https://$DEV_HOST/api/relation')
abstract class RelationService extends BaseService {
  factory RelationService(Dio dio, {String baseUrl}) = _RelationService;

  @POST('/approve')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ResultFormModel> postApprove(
    @Body() postBody,
  );

  @POST('/searchRel')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<SearchRelResponse>> searchRel(
    @Body() GeneralPaginationBody body,
  );

  @POST('/searchRelReq')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<SearchRelReqResponse>> searchRelReq(
    @Body() GeneralPaginationBody body,
  );

  @POST('/knowTogether')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<RelationReqOpponentResponse>> relationReqOpponent(
    @Body() GeneralPaginationBody body,
  );

  @POST('/knowTogether')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<KnowTogetherListResponse>> knowTogether(
    @Body() GeneralPaginationBody body,
  );

  @POST('/approve')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> approve(
    @Body() ApproveBody body,
  );

  @POST('/searchUserByPhone')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<SearchUserByPhoneResponse>> searchUserByPhone(
    @Body() SearchUserByPhoneBody body,
  );

  @POST('/searchRelSend')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<SearchRelResponse>> searchRelSend(
    @Body() GeneralPaginationBody body,
  );

  @POST('/search')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<SearchResponse>> search();

  @POST('/request')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> request(@Body() RequestBody body);

  @POST('/modify')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> modify(@Body() RequestBody body);

  @POST('/reject')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> reject(@Body() RejectBody body);

  @POST('/drop')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future drop(@Body() DropBody body);
}
