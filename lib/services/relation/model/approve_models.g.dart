// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'approve_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ApproveBody _$ApproveBodyFromJson(Map<String, dynamic> json) {
  return ApproveBody(
    userNoOpponent: json['userNoOpponent'] as int?,
  );
}

Map<String, dynamic> _$ApproveBodyToJson(ApproveBody instance) =>
    <String, dynamic>{
      'userNoOpponent': instance.userNoOpponent,
    };
