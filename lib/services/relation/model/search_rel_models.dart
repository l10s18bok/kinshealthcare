import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'search_rel_models.g.dart';

@JsonSerializable()
class SearchRelResponse extends BaseRetrofitModel {
  final int? userNoRel;
  final String? userRelCdMeValue;
  final String? userRelCdOpponentValue;
  final String? userNameOpponent;
  final String? userPhoneOpponent;
  final String? userStatusOpponent;
  final String? profileImgOpponent;

  SearchRelResponse({
    @required this.userNoRel,
    @required this.userRelCdMeValue,
    @required this.userRelCdOpponentValue,
    @required this.userNameOpponent,
    @required this.userPhoneOpponent,
    @required this.userStatusOpponent,
    @required this.profileImgOpponent,
  });

  factory SearchRelResponse.fromJson(Map<String, dynamic> json) =>
      _$SearchRelResponseFromJson(json);

  Map<String, dynamic> toJson() => _$SearchRelResponseToJson(this);
}
