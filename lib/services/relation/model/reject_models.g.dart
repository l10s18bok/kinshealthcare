// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reject_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RejectBody _$RejectBodyFromJson(Map<String, dynamic> json) {
  return RejectBody(
    userNoOpponent: json['userNoOpponent'] as int,
  );
}

Map<String, dynamic> _$RejectBodyToJson(RejectBody instance) =>
    <String, dynamic>{
      'userNoOpponent': instance.userNoOpponent,
    };
