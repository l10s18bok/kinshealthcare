import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'search_rel_req_models.g.dart';

@JsonSerializable()
class SearchRelReqResponse extends BaseRetrofitModel {
  final int userNoRel;
  final String userRelCdMeValue;
  final String userRelCdOpponentValue;
  final String userNameOpponent;
  final String userPhoneOpponent;
  final String userStatusOpponent;
  final String img;

  SearchRelReqResponse({
    required this.userNoRel,
    required this.userRelCdMeValue,
    required this.userNameOpponent,
    required this.userPhoneOpponent,
    required this.userStatusOpponent,
    required this.userRelCdOpponentValue,
    required this.img,
  });

  factory SearchRelReqResponse.fromJson(Object? json) =>
      _$SearchRelReqResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$SearchRelReqResponseToJson(this);
}
