import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'approve_models.g.dart';

@JsonSerializable()
class ApproveBody extends BaseRetrofitModel {
  final int? userNoOpponent;

  ApproveBody({
    this.userNoOpponent,
  });

  factory ApproveBody.fromJson(Map<String, dynamic> json) => _$ApproveBodyFromJson(json);

  Map<String, dynamic> toJson() => _$ApproveBodyToJson(this);
}
