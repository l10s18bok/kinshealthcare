import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';

part 'search_user_by_phone_models.g.dart';

@JsonSerializable()
class SearchUserByPhoneResponse extends BaseRetrofitModel {
  final int userNo;
  final String userName;
  final String phone;
  final String userStatus;
  final String photo;

  SearchUserByPhoneResponse({
    required this.userNo,
    required this.userName,
    required this.phone,
    required this.userStatus,
    required this.photo,
  });

  factory SearchUserByPhoneResponse.fromJson(Object? json) =>
      _$SearchUserByPhoneResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$SearchUserByPhoneResponseToJson(this);
}

@JsonSerializable()
class SearchUserByPhoneBody extends GeneralPaginationBody {
  final String phone;

  SearchUserByPhoneBody({
    required this.phone,
    bool direction = true,
    int page = 0,
    int size = 10,
    String sortBy = "last",
  }) : super(
          direction: direction,
          page: page,
          size: size,
          sortBy: sortBy,
        );

  factory SearchUserByPhoneBody.fromJson(Map<String, dynamic> json) =>
      _$SearchUserByPhoneBodyFromJson(json);

  Map<String, dynamic> toJson() => _$SearchUserByPhoneBodyToJson(this);
}
