import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'search_models.g.dart';

@JsonSerializable()
class SearchResponse extends BaseRetrofitModel {
  final String code;
  final String nm;

  SearchResponse({
    required this.code,
    required this.nm,
  });

  factory SearchResponse.fromJson(Object? json) =>
      _$SearchResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$SearchResponseToJson(this);
}
