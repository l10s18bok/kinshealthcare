import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'drop_models.g.dart';

@JsonSerializable()
class DropBody extends BaseRetrofitModel {
  final int userNoOpponent;

  DropBody({
    required this.userNoOpponent,
  });

  factory DropBody.fromJson(Map<String, dynamic> json) =>
      _$DropBodyFromJson(json);

  Map<String, dynamic> toJson() => _$DropBodyToJson(this);
}
