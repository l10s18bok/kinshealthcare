// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'know_together_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

KnowTogetherListBody _$KnowTogetherListBodyFromJson(Map<String, dynamic> json) {
  return KnowTogetherListBody(
    userRelNo: json['userRelNo'] as int?,
    page: json['page'] as int,
    direction: json['direction'] as bool,
    sortBy: json['sortBy'] as String,
    size: json['size'] as int,
  );
}

Map<String, dynamic> _$KnowTogetherListBodyToJson(
        KnowTogetherListBody instance) =>
    <String, dynamic>{
      'direction': instance.direction,
      'page': instance.page,
      'size': instance.size,
      'sortBy': instance.sortBy,
      'userRelNo': instance.userRelNo,
    };

KnowTogetherListResponse _$KnowTogetherListResponseFromJson(
    Map<String, dynamic> json) {
  return KnowTogetherListResponse(
    userNo: json['userNo'] as int?,
    userName: json['userName'] as String?,
    userImage: json['userImage'] as String?,
    phone: json['phone'] as String?,
    userRelCdMe: json['userRelCdMe'] as String?,
  );
}

Map<String, dynamic> _$KnowTogetherListResponseToJson(
        KnowTogetherListResponse instance) =>
    <String, dynamic>{
      'userNo': instance.userNo,
      'userName': instance.userName,
      'userImage': instance.userImage,
      'phone': instance.phone,
      'userRelCdMe': instance.userRelCdMe,
    };
