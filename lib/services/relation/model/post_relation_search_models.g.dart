// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_relation_search_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostRelationSearchBody _$PostRelationSearchBodyFromJson(
    Map<String, dynamic> json) {
  return PostRelationSearchBody();
}

Map<String, dynamic> _$PostRelationSearchBodyToJson(
        PostRelationSearchBody instance) =>
    <String, dynamic>{};

SearchRelResponse _$SearchRelResponseFromJson(Map<String, dynamic> json) {
  return SearchRelResponse(
    userNameOpponent: json['userNameOpponent'] as String,
    userNoRel: json['userNoRel'] as int,
    userPhoneOpponent: json['userPhoneOpponent'] as String,
    userRelCdMeValue: json['userRelCdMeValue'] as String,
    userRelCdOpponentValue: json['userRelCdOpponentValue'] as String,
    profileImgOpponent: json['profileImgOpponent'] as String,
    userStatusOpponent: json['userStatusOpponent'] as String,
  );
}

Map<String, dynamic> _$SearchRelResponseToJson(SearchRelResponse instance) =>
    <String, dynamic>{
      'userNameOpponent': instance.userNameOpponent,
      'userNoRel': instance.userNoRel,
      'userPhoneOpponent': instance.userPhoneOpponent,
      'userRelCdMeValue': instance.userRelCdMeValue,
      'userRelCdOpponentValue': instance.userRelCdOpponentValue,
      'profileImgOpponent': instance.profileImgOpponent,
      'userStatusOpponent': instance.userStatusOpponent,
    };
