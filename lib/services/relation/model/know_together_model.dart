import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';

part 'know_together_model.g.dart';

/*
 작성일 : 2021-06-02
 작성자 : andy
 화면명 :
 경로 :
 클래스 : 함께아는 가족 리스트
 설명 :   함께아는 가족 리스트
*/

@JsonSerializable()
class KnowTogetherListBody extends GeneralPaginationBody {
  int? userRelNo;

  KnowTogetherListBody({
    this.userRelNo,
    int page = 0,
    bool direction = true,
    String sortBy = 'last',
    int size = 10,
  }) : super(
          direction: direction,
          sortBy: sortBy,
          page: page,
          size: size,
        );

  factory KnowTogetherListBody.fromJson(Map<String, dynamic> json) => _$KnowTogetherListBodyFromJson(json);

  Map<String, dynamic> toJson() => _$KnowTogetherListBodyToJson(this);

  copyWith({
    int? userRelNo,
    bool? direction,
    int? page,
    int? size,
    String? sortBy,
  }) {
    userRelNo??= this.userRelNo;
    direction ??= this.direction;
    page ??= this.page;
    size ??= this.size;
    sortBy ??= this.sortBy;

    return KnowTogetherListBody(
      userRelNo: userRelNo,
      page: page,
      direction: direction,
      size: size,
      sortBy: sortBy,
    );
  }
}

@JsonSerializable()
class KnowTogetherListResponse extends BaseRetrofitModel {
  final int? userNo;
  final String? userName;
  final String? userImage;
  final String? phone;
  final String? userRelCdMe;

  KnowTogetherListResponse({
    this.userNo,
    this.userName,
    this.userImage,
    this.phone,
    this.userRelCdMe,
  });


  factory KnowTogetherListResponse.fromJson(Object? json) => _$KnowTogetherListResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$KnowTogetherListResponseToJson(this);
}