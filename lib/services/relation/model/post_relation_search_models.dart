import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'post_relation_search_models.g.dart';

/*
 작성일 : 2021-02-16
 작성자 : phil
 화면명 :
 경로 :
 클래스 : RelationService
 설명 : 관계 조회
*/

@JsonSerializable()
class PostRelationSearchBody {
  PostRelationSearchBody();

  factory PostRelationSearchBody.fromJson(Map<String, dynamic> json) =>
      _$PostRelationSearchBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostRelationSearchBodyToJson(this);
}

@JsonSerializable()
class SearchRelResponse extends BaseRetrofitModel {
  final String userNameOpponent;
  final int userNoRel;
  final String userPhoneOpponent;
  final String userRelCdMeValue;
  final String userRelCdOpponentValue;
  final String profileImgOpponent;
  @JsonKey(name: 'userStatusOpponent')
  final String _userStatusOpponent;

  SearchRelResponse({
    required this.userNameOpponent,
    required this.userNoRel,
    required this.userPhoneOpponent,
    required this.userRelCdMeValue,
    required this.userRelCdOpponentValue,
    required this.profileImgOpponent,
    required String userStatusOpponent,
  }) : this._userStatusOpponent = userStatusOpponent;

  get userStatusOpponent {
    Map<String, bool> dict = {
      'ACTIVE': true,
    };
    return dict[this._userStatusOpponent];
  }

  factory SearchRelResponse.fromJson(Object? json) =>
      _$SearchRelResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$SearchRelResponseToJson(this);
}
