import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'reject_models.g.dart';

@JsonSerializable()
class RejectBody extends BaseRetrofitModel {
  final int userNoOpponent;

  RejectBody({
    required this.userNoOpponent,
  });

  factory RejectBody.fromJson(Map<String, dynamic> json) =>
      _$RejectBodyFromJson(json);

  Map<String, dynamic> toJson() => _$RejectBodyToJson(this);
}
