// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_rel_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchRelResponse _$SearchRelResponseFromJson(Map<String, dynamic> json) {
  return SearchRelResponse(
    userNoRel: json['userNoRel'] as int?,
    userRelCdMeValue: json['userRelCdMeValue'] as String?,
    userRelCdOpponentValue: json['userRelCdOpponentValue'] as String?,
    userNameOpponent: json['userNameOpponent'] as String?,
    userPhoneOpponent: json['userPhoneOpponent'] as String?,
    userStatusOpponent: json['userStatusOpponent'] as String?,
    profileImgOpponent: json['profileImgOpponent'] as String?,
  );
}

Map<String, dynamic> _$SearchRelResponseToJson(SearchRelResponse instance) =>
    <String, dynamic>{
      'userNoRel': instance.userNoRel,
      'userRelCdMeValue': instance.userRelCdMeValue,
      'userRelCdOpponentValue': instance.userRelCdOpponentValue,
      'userNameOpponent': instance.userNameOpponent,
      'userPhoneOpponent': instance.userPhoneOpponent,
      'userStatusOpponent': instance.userStatusOpponent,
      'profileImgOpponent': instance.profileImgOpponent,
    };
