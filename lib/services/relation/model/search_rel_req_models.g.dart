// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_rel_req_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchRelReqResponse _$SearchRelReqResponseFromJson(Map<String, dynamic> json) {
  return SearchRelReqResponse(
    userNoRel: json['userNoRel'] as int,
    userRelCdMeValue: json['userRelCdMeValue'] as String,
    userNameOpponent: json['userNameOpponent'] as String,
    userPhoneOpponent: json['userPhoneOpponent'] as String,
    userStatusOpponent: json['userStatusOpponent'] as String,
    userRelCdOpponentValue: json['userRelCdOpponentValue'] as String,
    img: json['img'] as String,
  );
}

Map<String, dynamic> _$SearchRelReqResponseToJson(
        SearchRelReqResponse instance) =>
    <String, dynamic>{
      'userNoRel': instance.userNoRel,
      'userRelCdMeValue': instance.userRelCdMeValue,
      'userRelCdOpponentValue': instance.userRelCdOpponentValue,
      'userNameOpponent': instance.userNameOpponent,
      'userPhoneOpponent': instance.userPhoneOpponent,
      'userStatusOpponent': instance.userStatusOpponent,
      'img': instance.img,
    };
