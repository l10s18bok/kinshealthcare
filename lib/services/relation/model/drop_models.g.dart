// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'drop_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DropBody _$DropBodyFromJson(Map<String, dynamic> json) {
  return DropBody(
    userNoOpponent: json['userNoOpponent'] as int,
  );
}

Map<String, dynamic> _$DropBodyToJson(DropBody instance) => <String, dynamic>{
      'userNoOpponent': instance.userNoOpponent,
    };
