// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RequestBody _$RequestBodyFromJson(Map<String, dynamic> json) {
  return RequestBody(
    userNoRel: json['userNoRel'] as int,
    userRelCdOpponent: json['userRelCdOpponent'] as String,
  );
}

Map<String, dynamic> _$RequestBodyToJson(RequestBody instance) =>
    <String, dynamic>{
      'userNoRel': instance.userNoRel,
      'userRelCdOpponent': instance.userRelCdOpponent,
    };
