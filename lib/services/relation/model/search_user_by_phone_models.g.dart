// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_user_by_phone_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchUserByPhoneResponse _$SearchUserByPhoneResponseFromJson(
    Map<String, dynamic> json) {
  return SearchUserByPhoneResponse(
    userNo: json['userNo'] as int,
    userName: json['userName'] as String,
    phone: json['phone'] as String,
    userStatus: json['userStatus'] as String,
    photo: json['photo'] as String,
  );
}

Map<String, dynamic> _$SearchUserByPhoneResponseToJson(
        SearchUserByPhoneResponse instance) =>
    <String, dynamic>{
      'userNo': instance.userNo,
      'userName': instance.userName,
      'phone': instance.phone,
      'userStatus': instance.userStatus,
      'photo': instance.photo,
    };

SearchUserByPhoneBody _$SearchUserByPhoneBodyFromJson(
    Map<String, dynamic> json) {
  return SearchUserByPhoneBody(
    phone: json['phone'] as String,
    direction: json['direction'] as bool,
    page: json['page'] as int,
    size: json['size'] as int,
    sortBy: json['sortBy'] as String,
  );
}

Map<String, dynamic> _$SearchUserByPhoneBodyToJson(
        SearchUserByPhoneBody instance) =>
    <String, dynamic>{
      'direction': instance.direction,
      'page': instance.page,
      'size': instance.size,
      'sortBy': instance.sortBy,
      'phone': instance.phone,
    };
