import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'request_models.g.dart';

@JsonSerializable()
class RequestBody extends BaseRetrofitModel {
  final int userNoRel;
  final String userRelCdOpponent;

  RequestBody({
    required this.userNoRel,
    required this.userRelCdOpponent,
  });

  factory RequestBody.fromJson(Map<String, dynamic> json) =>
      _$RequestBodyFromJson(json);

  Map<String, dynamic> toJson() => _$RequestBodyToJson(this);
}
