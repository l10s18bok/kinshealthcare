import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'relation_require_opponent_model.g.dart';

/*
 작성일 : 2021-02-19
 작성자 : Victor
 화면명 :
 클래스 : RelationRequireOpponentModel
 경로 :
 설명 : 관계요청 조회 (신청자) 통신용 모델
*/

@JsonSerializable()
class PostRelationRequireOpponent {
  ///true : 오름차순, false : 내림차순
  final bool direction;

  /// 현재 페이지 번호 (0 이상)
  final int page;

  ///페이지당 표기 개수 (기본 10)
  final int size;

  ///정렬 키워드
  ///last(기본), userNameOpponent
  final String sortBy;

  PostRelationRequireOpponent({
    this.direction = true,
    this.page = 0,
    this.size = 10,
    this.sortBy = 'last',
  });

  factory PostRelationRequireOpponent.fromJson(Map<String, dynamic> json) =>
      _$PostRelationRequireOpponentFromJson(json);

  Map<String, dynamic> toJson() => _$PostRelationRequireOpponentToJson(this);
}

@JsonSerializable()
class RelationReqOpponentResponse extends BaseRetrofitModel {
  ///상대 프로필 사진
  final String? img;

  ///상대방 이름
  final String? userNameMe;

  ///상대방 유저No
  final int? userNo;

  ///상대방 핸드폰 번호
  final String? userPhoneMe;

  ///상대방이 나를 부르는 호칭
  final String? userRelCdMeValue;

  ///내가 상대를 부르는 호칭
  final String? userRelCdOpponentValue;

  ///상대방 상태
  final String? userStatusMe;

  RelationReqOpponentResponse({
    this.img,
    this.userNameMe,
    this.userNo,
    this.userPhoneMe,
    this.userRelCdMeValue,
    this.userRelCdOpponentValue,
    this.userStatusMe,
  });

  factory RelationReqOpponentResponse.fromJson(Object? json) =>
      _$RelationReqOpponentResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$RelationReqOpponentResponseToJson(this);
}
