// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'relation_require_opponent_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostRelationRequireOpponent _$PostRelationRequireOpponentFromJson(
    Map<String, dynamic> json) {
  return PostRelationRequireOpponent(
    direction: json['direction'] as bool,
    page: json['page'] as int,
    size: json['size'] as int,
    sortBy: json['sortBy'] as String,
  );
}

Map<String, dynamic> _$PostRelationRequireOpponentToJson(
        PostRelationRequireOpponent instance) =>
    <String, dynamic>{
      'direction': instance.direction,
      'page': instance.page,
      'size': instance.size,
      'sortBy': instance.sortBy,
    };

RelationReqOpponentResponse _$RelationReqOpponentResponseFromJson(
    Map<String, dynamic> json) {
  return RelationReqOpponentResponse(
    img: json['img'] as String?,
    userNameMe: json['userNameMe'] as String?,
    userNo: json['userNo'] as int?,
    userPhoneMe: json['userPhoneMe'] as String?,
    userRelCdMeValue: json['userRelCdMeValue'] as String?,
    userRelCdOpponentValue: json['userRelCdOpponentValue'] as String?,
    userStatusMe: json['userStatusMe'] as String?,
  );
}

Map<String, dynamic> _$RelationReqOpponentResponseToJson(
        RelationReqOpponentResponse instance) =>
    <String, dynamic>{
      'img': instance.img,
      'userNameMe': instance.userNameMe,
      'userNo': instance.userNo,
      'userPhoneMe': instance.userPhoneMe,
      'userRelCdMeValue': instance.userRelCdMeValue,
      'userRelCdOpponentValue': instance.userRelCdOpponentValue,
      'userStatusMe': instance.userStatusMe,
    };
