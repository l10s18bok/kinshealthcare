import 'dart:io';

import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/upload/model/post_image_v1_models.dart';
import 'package:retrofit/http.dart';

part 'upload_service.g.dart';

@RestApi(baseUrl: 'https://$DEV_HOST/api/upload')
abstract class UploadService extends BaseService {
  factory UploadService(Dio dio, {String baseUrl}) = _UploadService;

  // TODO 데이터 정규화시 적용
  // Future<ListResponseModel<PostImageUploadV1Response>> postImageUploadV1({
  @POST('/image/v1')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  @MultiPart()
  Future<ListResponseModel<PostImageUploadV1Response>> imageV1(
    @Part() List<File> image,
  );
}
