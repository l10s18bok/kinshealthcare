import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'post_image_v1_models.g.dart';

@JsonSerializable()
class PostImageUploadV1Response extends BaseRetrofitModel {
  final String? originalFileName;
  final String? uploadFilePath;

  PostImageUploadV1Response({
    this.originalFileName,
    this.uploadFilePath,
  });

  factory PostImageUploadV1Response.fromJson(Object? json) =>
      _$PostImageUploadV1ResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PostImageUploadV1ResponseToJson(this);
}
