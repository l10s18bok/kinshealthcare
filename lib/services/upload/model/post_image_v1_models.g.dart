// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_image_v1_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostImageUploadV1Response _$PostImageUploadV1ResponseFromJson(
    Map<String, dynamic> json) {
  return PostImageUploadV1Response(
    originalFileName: json['originalFileName'] as String?,
    uploadFilePath: json['uploadFilePath'] as String?,
  );
}

Map<String, dynamic> _$PostImageUploadV1ResponseToJson(
        PostImageUploadV1Response instance) =>
    <String, dynamic>{
      'originalFileName': instance.originalFileName,
      'uploadFilePath': instance.uploadFilePath,
    };
