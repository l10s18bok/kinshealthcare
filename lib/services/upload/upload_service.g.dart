// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'upload_service.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _UploadService implements UploadService {
  _UploadService(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://kins-healthcare-dev.kinsapps.com/api/upload';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ListResponseModel<PostImageUploadV1Response>> imageV1(image) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    _data.files.addAll(image.map((i) => MapEntry(
        'image',
        MultipartFile.fromFileSync(
          i.path,
          filename: i.path.split(Platform.pathSeparator).last,
        ))));
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<PostImageUploadV1Response>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/image/v1',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<PostImageUploadV1Response>.fromJson(
      _result.data!,
      (json) => PostImageUploadV1Response.fromJson(json),
    );
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
