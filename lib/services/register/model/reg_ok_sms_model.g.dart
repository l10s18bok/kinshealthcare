// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reg_ok_sms_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegOkSmsResponse _$RegOkSmsResponseFromJson(Map<String, dynamic> json) {
  return RegOkSmsResponse(
    resultCode: json['resultCode'] as String,
    resultMsg: json['resultMsg'] as String,
  );
}

Map<String, dynamic> _$RegOkSmsResponseToJson(RegOkSmsResponse instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
    };

RegOkSmsBody _$RegOkSmsBodyFromJson(Map<String, dynamic> json) {
  return RegOkSmsBody(
    phone: json['phone'] as String?,
    certText: json['certText'] as String?,
    seq: json['seq'] as int,
  );
}

Map<String, dynamic> _$RegOkSmsBodyToJson(RegOkSmsBody instance) =>
    <String, dynamic>{
      'phone': instance.phone,
      'certText': instance.certText,
      'seq': instance.seq,
    };
