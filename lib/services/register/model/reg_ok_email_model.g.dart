// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reg_ok_email_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegOkEmailResponse _$RegOkEmailResponseFromJson(Map<String, dynamic> json) {
  return RegOkEmailResponse(
    resultCode: json['resultCode'] as String,
    resultMsg: json['resultMsg'] as String,
  );
}

Map<String, dynamic> _$RegOkEmailResponseToJson(RegOkEmailResponse instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
    };

RegOkEmailBody _$RegOkEmailBodyFromJson(Map<String, dynamic> json) {
  return RegOkEmailBody(
    email: json['email'] as String?,
    certText: json['certText'] as String?,
    seq: json['seq'] as int?,
  );
}

Map<String, dynamic> _$RegOkEmailBodyToJson(RegOkEmailBody instance) =>
    <String, dynamic>{
      'email': instance.email,
      'certText': instance.certText,
      'seq': instance.seq,
    };
