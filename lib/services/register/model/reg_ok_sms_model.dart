import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'reg_ok_sms_model.g.dart';

@JsonSerializable()
class RegOkSmsResponse extends BaseRetrofitModel {
  final String resultCode;
  final String resultMsg;

  RegOkSmsResponse({
    required this.resultCode,
    required this.resultMsg,
  });

  factory RegOkSmsResponse.fromJson(Map<String, dynamic> json) => _$RegOkSmsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RegOkSmsResponseToJson(this);
}

@JsonSerializable()
class RegOkSmsBody extends BaseRetrofitModel {
  final String? phone;
  final String? certText;
  final int seq;

  RegOkSmsBody({
    required this.phone,
    required this.certText,
    required this.seq,
  });

  factory RegOkSmsBody.fromJson(Map<String, dynamic> json) => _$RegOkSmsBodyFromJson(json);

  Map<String, dynamic> toJson() => _$RegOkSmsBodyToJson(this);
}