import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'reg_ok_email_model.g.dart';

@JsonSerializable()
class RegOkEmailResponse extends BaseRetrofitModel {
  final String resultCode;
  final String resultMsg;

  RegOkEmailResponse({
    required this.resultCode,
    required this.resultMsg,
  });

  factory RegOkEmailResponse.fromJson(Map<String, dynamic> json) => _$RegOkEmailResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RegOkEmailResponseToJson(this);
}

@JsonSerializable()
class RegOkEmailBody extends BaseRetrofitModel {
  final String? email;
  final String? certText;
  final int? seq;

  RegOkEmailBody({
    required this.email,
    required this.certText,
    required this.seq,
  });

  factory RegOkEmailBody.fromJson(Map<String, dynamic> json) => _$RegOkEmailBodyFromJson(json);

  Map<String, dynamic> toJson() => _$RegOkEmailBodyToJson(this);
}