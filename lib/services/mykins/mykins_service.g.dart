// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mykins_service.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _MykinsService implements MykinsService {
  _MykinsService(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://kins-healthcare-dev.kinsapps.com/api/mykins';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<DefaultModel> updateNotification(postBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = postBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<DefaultModel>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/updateNotification',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = DefaultModel.fromJson(_result.data!);
    return value;
  }

  @override
  Future<SingleItemResponseModel<NotificationModel>> getNotification() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<SingleItemResponseModel<NotificationModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/getNotification',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = SingleItemResponseModel<NotificationModel>.fromJson(
      _result.data!,
      (json) => NotificationModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<ListResponseModel<GetAnniversaryResponse>> getAnniversary() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<GetAnniversaryResponse>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/getAnniversary',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<GetAnniversaryResponse>.fromJson(
      _result.data!,
      (json) => GetAnniversaryResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<ListResponseModel<InquiryListModel>> inquiryList() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<InquiryListModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/inquiryList',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<InquiryListModel>.fromJson(
      _result.data!,
      (json) => InquiryListModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<DefaultModel> insertInquiry(PostInsertInquiryBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = PostInsertInquiryBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<DefaultModel>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/insertInquiry',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = DefaultModel.fromJson(_result.data!);
    return value;
  }

  @override
  Future<SingleItemResponseModel<GetExpResponse>> getExp() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<SingleItemResponseModel<GetExpResponse>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/getExp',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = SingleItemResponseModel<GetExpResponse>.fromJson(
      _result.data!,
      (json) => GetExpResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<ListResponseModel<DivisionModel>> getDivision() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<DivisionModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/getDivision',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<DivisionModel>.fromJson(
      _result.data!,
      (json) => DivisionModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<ListResponseModel<FamilyListModel>> getFamilyList() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<FamilyListModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/getFamilyList',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<FamilyListModel>.fromJson(
      _result.data!,
      (json) => FamilyListModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<DefaultModel> updateDivision(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<DefaultModel>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/updateDivision',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = DefaultModel.fromJson(_result.data!);
    return value;
  }

  @override
  Future<SingleItemResponseModel<GetUserInfoResponse>> getUserInfo() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<SingleItemResponseModel<GetUserInfoResponse>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/getUserInfo',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = SingleItemResponseModel<GetUserInfoResponse>.fromJson(
      _result.data!,
      (json) => GetUserInfoResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<DefaultModel> insertAnniversary(InsertAnniversaryBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = InsertAnniversaryBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<DefaultModel>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/insertAnniversary',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = DefaultModel.fromJson(_result.data!);
    return value;
  }

  @override
  Future<ListResponseModel<SearchMyChildrensResponse>> getChildList(
      body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<SearchMyChildrensResponse>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/getChildList',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<SearchMyChildrensResponse>.fromJson(
      _result.data!,
      (json) => SearchMyChildrensResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<RegisterResponse> registerKids(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<RegisterResponse>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/registerKids',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = RegisterResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<SingleItemResponseModel<UserInfoResponse>> getChildInfo(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<SingleItemResponseModel<UserInfoResponse>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/getChildInfo',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = SingleItemResponseModel<UserInfoResponse>.fromJson(
      _result.data!,
      (json) => UserInfoResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<GeneralPlainResponse> modifyChildInfo(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<GeneralPlainResponse>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/modifyChildInfo',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = GeneralPlainResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<GeneralPlainResponse> deleteChild(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<GeneralPlainResponse>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/deleteChild',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = GeneralPlainResponse.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
