import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/mykins/model/child_profile_update_body.dart';
import 'package:kins_healthcare/services/mykins/model/get_anniversary_models.dart';
import 'package:kins_healthcare/services/mykins/model/get_exp_models.dart';
import 'package:kins_healthcare/services/mykins/model/search_my_children_models.dart';
import 'package:kins_healthcare/services/user/model/register_model.dart';
import 'package:kins_healthcare/services/user/model/user_info_models.dart';
import 'package:retrofit/http.dart';

import 'model/division_list_body.dart';
import 'model/division_model.dart';
import 'model/family_list_model.dart';
import 'model/getUserInfo_response.dart';
import 'model/inquiry_list_model.dart';
import 'model/notification_model.dart';
import 'model/post_only_child_id_body.dart';

part 'mykins_service.g.dart';

/*
 작성일 : 2021-03-10
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : MykinsService,
 설명 : 마아킨즈 Controller 의 데이터를 받아오기 위한 Service
*/

@RestApi(baseUrl: 'https://$DEV_HOST/api/mykins')
abstract class MykinsService extends BaseService {
  factory MykinsService(Dio dio, {String baseUrl}) = _MykinsService;

  @POST('/updateNotification')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultModel> updateNotification(
    @Body() postBody,
  );

  @POST('/getNotification')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseModel<NotificationModel>> getNotification();

  @POST('/getAnniversary')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<GetAnniversaryResponse>> getAnniversary();

  @POST('/inquiryList')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<InquiryListModel>> inquiryList();

  @POST('/insertInquiry')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultModel> insertInquiry(
    @Body() PostInsertInquiryBody,
  );

  @POST('/getExp')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseModel<GetExpResponse>> getExp();

  @POST('/getDivision')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<DivisionModel>> getDivision();

  @POST('/getFamilyList')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<FamilyListModel>> getFamilyList();

  @POST('/updateDivision')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultModel> updateDivision(
    @Body() DivisionListBody body,
  );

  @POST('/getUserInfo')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseModel<GetUserInfoResponse>> getUserInfo();

  @POST('/insertAnniversary')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultModel> insertAnniversary(
    @Body() InsertAnniversaryBody,
  );

  @POST('/getChildList')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<SearchMyChildrensResponse>> getChildList(
    @Body() GeneralPaginationBody body,
  );

  @POST('/registerKids')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<RegisterResponse> registerKids(
    @Body() RegisterChildBody body,
  );

  @POST('/getChildInfo')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseModel<UserInfoResponse>> getChildInfo(
    @Body() PostOnlyChildIdBody body,
  );

  @POST('/modifyChildInfo')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> modifyChildInfo(
    @Body() ChildProfileUpdateBody body,
  );

  @POST('/deleteChild')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> deleteChild(
    @Body() PostOnlyChildIdBody body,
  );
}
