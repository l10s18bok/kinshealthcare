// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'child_profile_update_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChildProfileUpdateBody _$ChildProfileUpdateBodyFromJson(
    Map<String, dynamic> json) {
  return ChildProfileUpdateBody(
    lastname: json['lastname'] as String?,
    firstname: json['firstname'] as String?,
    birth: json['birth'] as String?,
    image: json['image'] as String?,
    childUserNO: json['childUserNO'] as String?,
    phone: json['phone'] as String?,
  );
}

Map<String, dynamic> _$ChildProfileUpdateBodyToJson(
        ChildProfileUpdateBody instance) =>
    <String, dynamic>{
      'lastname': instance.lastname,
      'firstname': instance.firstname,
      'birth': instance.birth,
      'image': instance.image,
      'childUserNO': instance.childUserNO,
      'phone': instance.phone,
    };
