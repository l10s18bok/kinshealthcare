import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'get_anniversary_models.g.dart';

@JsonSerializable()
class GetAnniversaryResponse extends BaseRetrofitModel {
  final String? anniName;
  final int? anniType;
  final int? calType;
  final int? conType;
  final String? dates;
  final int? ids;
  final String? pushDate;
  final String? relName;
  final String? relUserImage;
  final String? relUserName;

  GetAnniversaryResponse({
    required this.anniName,
    required this.anniType,
    required this.calType,
    required this.conType,
    required this.dates,
    required this.ids,
    required this.pushDate,
    required this.relName,
    required this.relUserImage,
    required this.relUserName,
  });

  factory GetAnniversaryResponse.fromJson(Object? json) =>
      _$GetAnniversaryResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$GetAnniversaryResponseToJson(this);

  get dDay {
    final today = DateTime.now();
    final anniDay = DateTime.parse(this.dates!);

    print('---------date------------');
    print(this.anniName);
    print(today);
    print(anniDay);

    final diff = today.difference(anniDay).inDays;

    if (getDaySignature(today) == getDaySignature(anniDay)) {
      return '-Day';
    }
    return diff > 0 ? '+$diff' : '${diff - 1}';
  }

  getDaySignature(DateTime day) {
    return '${day.year}-${day.month}-${day.day}';
  }

  isSelectedDay(DateTime day) {
    return '${day.year}-${day.month.toString().padLeft(2, '0')}-${day.day.toString().padLeft(2, '0')}' ==
        this.dates;
  }
}
