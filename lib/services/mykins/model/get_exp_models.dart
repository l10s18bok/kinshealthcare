import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'get_exp_models.g.dart';

@JsonSerializable()
class GetExpResponse extends BaseRetrofitModel {
  final String? userLevel;
  final double? userExp;

  GetExpResponse({
    required this.userLevel,
    required this.userExp,
  });

  factory GetExpResponse.fromJson(Object? json) =>
      _$GetExpResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$GetExpResponseToJson(this);
}
