// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_exp_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetExpResponse _$GetExpResponseFromJson(Map<String, dynamic> json) {
  return GetExpResponse(
    userLevel: json['userLevel'] as String?,
    userExp: (json['userExp'] as num?)?.toDouble(),
  );
}

Map<String, dynamic> _$GetExpResponseToJson(GetExpResponse instance) =>
    <String, dynamic>{
      'userLevel': instance.userLevel,
      'userExp': instance.userExp,
    };
