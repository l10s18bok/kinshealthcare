// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_only_child_id_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostOnlyChildIdBody _$PostOnlyChildIdBodyFromJson(Map<String, dynamic> json) {
  return PostOnlyChildIdBody(
    childUserNO: json['childUserNO'] as int?,
  );
}

Map<String, dynamic> _$PostOnlyChildIdBodyToJson(
        PostOnlyChildIdBody instance) =>
    <String, dynamic>{
      'childUserNO': instance.childUserNO,
    };
