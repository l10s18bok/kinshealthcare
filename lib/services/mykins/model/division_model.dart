import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/payment_user_model.dart';

/*
 작성일 : 2021-03-15
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : DivisionModel,
 설명 : 저장된 결제 나누기 설정 Model
*/

part 'division_model.g.dart';

@JsonSerializable()
class DivisionModel {
  int? relUserNo;
  String? relUserName;
  String? relUserImage;
  String? relName;
  String? relUserPhone;
  String? percentage;

  DivisionModel({
    this.relUserNo,
    this.relUserName,
    this.relUserImage,
    this.relName,
    this.relUserPhone,
    this.percentage,
  });

  factory DivisionModel.fromJson(Object? json) =>
      _$DivisionModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$DivisionModelToJson(this);

  PaymentUserModel get paymentUserModel {
    return PaymentUserModel(
      name: relUserName ?? '',
      phoneNum: relUserPhone ?? '',
      userRelNo: relUserNo ?? 0,
      nickName: relName,
      imageUrl: relUserImage,
      percentage: percentage ?? '0.0',
    );
  }
}
