// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_my_children_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchMyChildrensResponse _$SearchMyChildrensResponseFromJson(
    Map<String, dynamic> json) {
  return SearchMyChildrensResponse(
    childUserNO: json['childUserNO'] as int,
    childUserName: json['childUserName'] as String,
    childUserPhone: json['childUserPhone'] as String,
    childUserProfile: json['childUserProfile'] as String,
    userRelChild: json['userRelChild'] as String,
  );
}

Map<String, dynamic> _$SearchMyChildrensResponseToJson(
        SearchMyChildrensResponse instance) =>
    <String, dynamic>{
      'childUserNO': instance.childUserNO,
      'childUserName': instance.childUserName,
      'childUserPhone': instance.childUserPhone,
      'childUserProfile': instance.childUserProfile,
      'userRelChild': instance.userRelChild,
    };
