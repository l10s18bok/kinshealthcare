// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'division_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DivisionModel _$DivisionModelFromJson(Map<String, dynamic> json) {
  return DivisionModel(
    relUserNo: json['relUserNo'] as int?,
    relUserName: json['relUserName'] as String?,
    relUserImage: json['relUserImage'] as String?,
    relName: json['relName'] as String?,
    relUserPhone: json['relUserPhone'] as String?,
    percentage: json['percentage'] as String?,
  );
}

Map<String, dynamic> _$DivisionModelToJson(DivisionModel instance) =>
    <String, dynamic>{
      'relUserNo': instance.relUserNo,
      'relUserName': instance.relUserName,
      'relUserImage': instance.relUserImage,
      'relName': instance.relName,
      'relUserPhone': instance.relUserPhone,
      'percentage': instance.percentage,
    };
