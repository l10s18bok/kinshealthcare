// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'inquiry_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InquiryListModel _$InquiryListModelFromJson(Map<String, dynamic> json) {
  return InquiryListModel(
    id: json['id'] as int?,
    title: json['title'] as String?,
    content: json['content'] as String?,
    answer: json['answer'] as String?,
    answerAt: json['answer_at'] as String?,
    dates:
        json['dates'] == null ? null : DateTime.parse(json['dates'] as String),
  );
}

Map<String, dynamic> _$InquiryListModelToJson(InquiryListModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'content': instance.content,
      'answer': instance.answer,
      'answer_at': instance.answerAt,
      'dates': instance.dates?.toIso8601String(),
    };
