import 'package:json_annotation/json_annotation.dart';

part 'post_only_child_id_body.g.dart';

/*
 작성일 : 2021-04-14
 작성자 : andy
 화면명 :
 경로 :
 클래스 : PostOnlyChildIdBody
 설명 :  자녀회원 검색
*/

@JsonSerializable()
class PostOnlyChildIdBody {
  final int? childUserNO;

  PostOnlyChildIdBody({
    this.childUserNO,
  });

  factory PostOnlyChildIdBody.fromJson(Map<String, dynamic> json) => _$PostOnlyChildIdBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostOnlyChildIdBodyToJson(this);
}
