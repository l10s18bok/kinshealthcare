import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'getUserInfo_response.g.dart';

@JsonSerializable()
class GetUserInfoResponse extends BaseRetrofitModel {
  final int userNo;
  final String userName;
  final String userImage;
  final String relName;

  GetUserInfoResponse({
    required this.userNo,
    required this.userName,
    required this.userImage,
    required this.relName,
  });

  factory GetUserInfoResponse.fromJson(Object? json) =>
      _$GetUserInfoResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$GetUserInfoResponseToJson(this);
}
