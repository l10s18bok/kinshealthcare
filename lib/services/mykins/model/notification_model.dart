import 'package:json_annotation/json_annotation.dart';

/*
 작성일 : 2021-02-18
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : NotificationModel,
 설명 : 알림 설정 Model
*/

part 'notification_model.g.dart';

@JsonSerializable()
class NotificationModel {
  String? kkakkaYn;
  String? payYn;
  String? anniversaryYn;
  String? balanceYn;
  String? balanceAmount;
  String? selfKkakkaYn;
  String? selfKkakkaDay;
  String? mediKkakkaYn;
  String? mediKkakkaDay;
  String? eventYn;
  String? vibratYn;
  String? disturbYn;
  String? noDisturbFrom;
  String? noDisturbTo;

  NotificationModel({
    this.kkakkaYn,
    this.payYn,
    this.anniversaryYn,
    this.balanceYn,
    this.balanceAmount,
    this.selfKkakkaYn,
    this.selfKkakkaDay,
    this.mediKkakkaYn,
    this.mediKkakkaDay,
    this.eventYn,
    this.vibratYn,
    this.disturbYn,
    this.noDisturbFrom,
    this.noDisturbTo,
  });

  factory NotificationModel.fromJson(Object? json) =>
      _$NotificationModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$NotificationModelToJson(this);
}
