// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationModel _$NotificationModelFromJson(Map<String, dynamic> json) {
  return NotificationModel(
    kkakkaYn: json['kkakkaYn'] as String?,
    payYn: json['payYn'] as String?,
    anniversaryYn: json['anniversaryYn'] as String?,
    balanceYn: json['balanceYn'] as String?,
    balanceAmount: json['balanceAmount'] as String?,
    selfKkakkaYn: json['selfKkakkaYn'] as String?,
    selfKkakkaDay: json['selfKkakkaDay'] as String?,
    mediKkakkaYn: json['mediKkakkaYn'] as String?,
    mediKkakkaDay: json['mediKkakkaDay'] as String?,
    eventYn: json['eventYn'] as String?,
    vibratYn: json['vibratYn'] as String?,
    disturbYn: json['disturbYn'] as String?,
    noDisturbFrom: json['noDisturbFrom'] as String?,
    noDisturbTo: json['noDisturbTo'] as String?,
  );
}

Map<String, dynamic> _$NotificationModelToJson(NotificationModel instance) =>
    <String, dynamic>{
      'kkakkaYn': instance.kkakkaYn,
      'payYn': instance.payYn,
      'anniversaryYn': instance.anniversaryYn,
      'balanceYn': instance.balanceYn,
      'balanceAmount': instance.balanceAmount,
      'selfKkakkaYn': instance.selfKkakkaYn,
      'selfKkakkaDay': instance.selfKkakkaDay,
      'mediKkakkaYn': instance.mediKkakkaYn,
      'mediKkakkaDay': instance.mediKkakkaDay,
      'eventYn': instance.eventYn,
      'vibratYn': instance.vibratYn,
      'disturbYn': instance.disturbYn,
      'noDisturbFrom': instance.noDisturbFrom,
      'noDisturbTo': instance.noDisturbTo,
    };
