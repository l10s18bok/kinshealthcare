import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'insert_anniversary_body.g.dart';

@JsonSerializable()
class InsertAnniversaryBody extends BaseRetrofitModel {
  final String anniName; //varchar(40) 기념일 이름
  final int anniType; // 기념일 타입 - 0:생일 1:결혼기념일 2: 기타
  final int calType; //달력 타입 0: 양력 , 1:음력
  final String day; //기념일 날짜
  final String pushYn; // Y / N (해당 기념일 알림 on/off)
  final String
      pushDay; // datetime //기념일 알림 날짜 설정 (데이터는 "3", "1" 이런식으로 넘겨주세요! pushYn = N일땐 "0")
  final int userRelNo; //bigint(20) //기념일 대상자
  final List<InsertAnniShareUser> shareList; //공유 가족 선택

  InsertAnniversaryBody({
    required this.anniName,
    required this.anniType,
    required this.calType,
    required this.day,
    required this.pushYn,
    required this.pushDay,
    required this.userRelNo,
    required this.shareList,
  });

  factory InsertAnniversaryBody.fromJson(Map<String, dynamic> json) =>
      _$InsertAnniversaryBodyFromJson(json);

  Map<String, dynamic> toJson() => _$InsertAnniversaryBodyToJson(this);

  static int getTitleToKkakkaType(String title) {
    switch (title) {
      case '생일':
        return 0;
      case '결혼기념일':
        return 1;
      case '기타':
        return 2;
    }
    return 2;
  }

  get dDay {
    final today = DateTime.now();
    final anniDay = DateTime.parse(this.day);

    final diff = today.difference(anniDay).inDays;

    return diff > 0 ? '+$diff' : diff;
  }

  isSelectedDay(DateTime day) {
    return '${day.year}-${day.month.toString().padLeft(2, '0')}-${day.day.toString().padLeft(2, '0')}' ==
        this.day;
  }
}

@JsonSerializable()
class InsertAnniShareUser extends BaseRetrofitModel {
  final int shareUserNo;

  InsertAnniShareUser({required this.shareUserNo});

  factory InsertAnniShareUser.fromJson(Map<String, dynamic> json) =>
      _$InsertAnniShareUserFromJson(json);

  Map<String, dynamic> toJson() => _$InsertAnniShareUserToJson(this);
}
