// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'getUserInfo_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetUserInfoResponse _$GetUserInfoResponseFromJson(Map<String, dynamic> json) {
  return GetUserInfoResponse(
    userNo: json['userNo'] as int,
    userName: json['userName'] as String,
    userImage: json['userImage'] as String,
    relName: json['relName'] as String,
  );
}

Map<String, dynamic> _$GetUserInfoResponseToJson(
        GetUserInfoResponse instance) =>
    <String, dynamic>{
      'userNo': instance.userNo,
      'userName': instance.userName,
      'userImage': instance.userImage,
      'relName': instance.relName,
    };
