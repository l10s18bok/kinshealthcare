// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_anniversary_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetAnniversaryResponse _$GetAnniversaryResponseFromJson(
    Map<String, dynamic> json) {
  return GetAnniversaryResponse(
    anniName: json['anniName'] as String?,
    anniType: json['anniType'] as int?,
    calType: json['calType'] as int?,
    conType: json['conType'] as int?,
    dates: json['dates'] as String?,
    ids: json['ids'] as int?,
    pushDate: json['pushDate'] as String?,
    relName: json['relName'] as String?,
    relUserImage: json['relUserImage'] as String?,
    relUserName: json['relUserName'] as String?,
  );
}

Map<String, dynamic> _$GetAnniversaryResponseToJson(
        GetAnniversaryResponse instance) =>
    <String, dynamic>{
      'anniName': instance.anniName,
      'anniType': instance.anniType,
      'calType': instance.calType,
      'conType': instance.conType,
      'dates': instance.dates,
      'ids': instance.ids,
      'pushDate': instance.pushDate,
      'relName': instance.relName,
      'relUserImage': instance.relUserImage,
      'relUserName': instance.relUserName,
    };
