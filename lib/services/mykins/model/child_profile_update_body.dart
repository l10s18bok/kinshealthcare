import 'package:json_annotation/json_annotation.dart';

part 'child_profile_update_body.g.dart';

@JsonSerializable()
class ChildProfileUpdateBody {
  final String? lastname;
  final String? firstname;
  final String? birth;
  final String? image;
  final String? childUserNO;
  final String? phone;

  ChildProfileUpdateBody({
    this.lastname,
    this.firstname,
    this.birth,
    this.image,
    this.childUserNO,
    this.phone,
  });

  factory ChildProfileUpdateBody.fromJson(Map<String, dynamic> json) => _$ChildProfileUpdateBodyFromJson(json);

  Map<String, dynamic> toJson() => _$ChildProfileUpdateBodyToJson(this);
}
