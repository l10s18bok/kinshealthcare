import 'package:json_annotation/json_annotation.dart';

import 'package:kins_healthcare/services/common/base_model.dart';

part 'search_my_children_models.g.dart';

/*
 작성일 : 2021-04-01
 작성자 : Andy
 화면명 : HY_2001(나의 가족), HY_CHILD_LIST(나의 자녀)
 경로 :
 클래스 : SearchMyChildrensResponse
 설명 : 나의 자녀 조회
*/

@JsonSerializable()
class SearchMyChildrensResponse extends BaseRetrofitModel {
  final int childUserNO;
  final String childUserName;
  final String childUserPhone;
  final String childUserProfile;
  final String userRelChild;

  SearchMyChildrensResponse({
    required this.childUserNO,
    required this.childUserName,
    required this.childUserPhone,
    required this.childUserProfile,
    required this.userRelChild,
  });

  factory SearchMyChildrensResponse.fromJson(Object? json) => _$SearchMyChildrensResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$SearchMyChildrensResponseToJson(this);
}
