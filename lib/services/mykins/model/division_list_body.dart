import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

/*
 작성일 : 2021-03-15
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : DivisionModel,
 설명 : 저장된 결제 나누기 설정 Model
*/

part 'division_list_body.g.dart';

@JsonSerializable()
class DivisionListBody extends BaseRetrofitModel {
  List<DivisionBody>? divisionList;

  DivisionListBody({
    this.divisionList,
  });

  factory DivisionListBody.fromJson(Map<String, dynamic> json) =>
      _$DivisionListBodyFromJson(json);

  Map<String, dynamic> toJson() => _$DivisionListBodyToJson(this);
}

@JsonSerializable()
class DivisionBody extends BaseRetrofitModel {
  int? relUserNo;
  String? percentage;

  DivisionBody({
    this.relUserNo,
    this.percentage,
  });

  factory DivisionBody.fromJson(Map<String, dynamic> json) =>
      _$DivisionBodyFromJson(json);

  Map<String, dynamic> toJson() => _$DivisionBodyToJson(this);
}
