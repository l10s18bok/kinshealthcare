// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'division_list_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DivisionListBody _$DivisionListBodyFromJson(Map<String, dynamic> json) {
  return DivisionListBody(
    divisionList: (json['divisionList'] as List<dynamic>?)
        ?.map((e) => DivisionBody.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$DivisionListBodyToJson(DivisionListBody instance) =>
    <String, dynamic>{
      'divisionList': instance.divisionList,
    };

DivisionBody _$DivisionBodyFromJson(Map<String, dynamic> json) {
  return DivisionBody(
    relUserNo: json['relUserNo'] as int?,
    percentage: json['percentage'] as String?,
  );
}

Map<String, dynamic> _$DivisionBodyToJson(DivisionBody instance) =>
    <String, dynamic>{
      'relUserNo': instance.relUserNo,
      'percentage': instance.percentage,
    };
