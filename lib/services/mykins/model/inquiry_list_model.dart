import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/local_board_model.dart';

/*
 작성일 : 2021-02-08
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : QuestionModel,
 설명 : 1:1 문의 Model
*/

part 'inquiry_list_model.g.dart';

@JsonSerializable()
class InquiryListModel {
  int? id;
  String? title;
  String? content;
  String? answer; //답변내용
  @JsonKey(name: 'answer_at')
  String? answerAt; //답변여부
  DateTime? dates;

  InquiryListModel({
    this.id,
    this.title,
    this.content,
    this.answer,
    this.answerAt,
    this.dates,
  });

  get timeString {
    return DateFormat('yyyy.MM.dd').format(dates!);
  }

  LocalBoardModel get localBoardModel {
    return LocalBoardModel(
      title: this.title ?? '',
      content: this.content,
      regDate: timeString,
      answer: this.answer,
    );
  }

  factory InquiryListModel.fromJson(Object? json) =>
      _$InquiryListModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$InquiryListModelToJson(this);
}
