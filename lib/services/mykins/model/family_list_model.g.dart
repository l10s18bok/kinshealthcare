// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'family_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FamilyListModel _$FamilyListModelFromJson(Map<String, dynamic> json) {
  return FamilyListModel(
    relUserNo: json['relUserNo'] as int?,
    relUserName: json['relUserName'] as String?,
    relUserImage: json['relUserImage'] as String?,
    relName: json['relName'] as String?,
    relUserPhone: json['relUserPhone'] as String?,
  )..isChecked = json['isChecked'] as bool?;
}

Map<String, dynamic> _$FamilyListModelToJson(FamilyListModel instance) =>
    <String, dynamic>{
      'relUserNo': instance.relUserNo,
      'relUserName': instance.relUserName,
      'relUserImage': instance.relUserImage,
      'relName': instance.relName,
      'relUserPhone': instance.relUserPhone,
      'isChecked': instance.isChecked,
    };
