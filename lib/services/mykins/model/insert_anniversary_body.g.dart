// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'insert_anniversary_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InsertAnniversaryBody _$InsertAnniversaryBodyFromJson(
    Map<String, dynamic> json) {
  return InsertAnniversaryBody(
    anniName: json['anniName'] as String,
    anniType: json['anniType'] as int,
    calType: json['calType'] as int,
    day: json['day'] as String,
    pushYn: json['pushYn'] as String,
    pushDay: json['pushDay'] as String,
    userRelNo: json['userRelNo'] as int,
    shareList: (json['shareList'] as List<dynamic>)
        .map((e) => InsertAnniShareUser.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$InsertAnniversaryBodyToJson(
        InsertAnniversaryBody instance) =>
    <String, dynamic>{
      'anniName': instance.anniName,
      'anniType': instance.anniType,
      'calType': instance.calType,
      'day': instance.day,
      'pushYn': instance.pushYn,
      'pushDay': instance.pushDay,
      'userRelNo': instance.userRelNo,
      'shareList': instance.shareList,
    };

InsertAnniShareUser _$InsertAnniShareUserFromJson(Map<String, dynamic> json) {
  return InsertAnniShareUser(
    shareUserNo: json['shareUserNo'] as int,
  );
}

Map<String, dynamic> _$InsertAnniShareUserToJson(
        InsertAnniShareUser instance) =>
    <String, dynamic>{
      'shareUserNo': instance.shareUserNo,
    };
