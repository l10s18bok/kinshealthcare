import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/payment_user_model.dart';

/*
 작성일 : 2021-03-15
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : DivisionModel,
 설명 : 저장된 결제 나누기 설정 Model
*/

part 'family_list_model.g.dart';

@JsonSerializable()
class FamilyListModel {
  int? relUserNo;
  String? relUserName;
  String? relUserImage;
  String? relName;
  String? relUserPhone;
  bool? _isChecked;

  FamilyListModel({
    this.relUserNo,
    this.relUserName,
    this.relUserImage,
    this.relName,
    this.relUserPhone,
  });

  bool? get isChecked {
    return this._isChecked ?? false;
  }

  set isChecked(bool? isChecked) {
    this._isChecked = isChecked;
  }

  factory FamilyListModel.fromJson(Object? json) =>
      _$FamilyListModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$FamilyListModelToJson(this);

  PaymentUserModel get paymentUserModel {
    return PaymentUserModel(
      name: relUserName ?? '',
      phoneNum: relUserPhone ?? '',
      userRelNo: relUserNo ?? 0,
      nickName: relName,
      imageUrl: relUserImage,
      percentage: "0",
      isChecked: isChecked ?? false,
    );
  }
}
