// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timeline_list_pagination_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TimelinePaginationBody _$TimelinePaginationBodyFromJson(
    Map<String, dynamic> json) {
  return TimelinePaginationBody(
    direction: json['direction'] as bool,
    sortBy: json['sortBy'] as String,
    page: json['page'] as int,
    size: json['size'] as int,
    timelineType: json['timelineType'] as String?,
  );
}

Map<String, dynamic> _$TimelinePaginationBodyToJson(
        TimelinePaginationBody instance) =>
    <String, dynamic>{
      'direction': instance.direction,
      'page': instance.page,
      'size': instance.size,
      'sortBy': instance.sortBy,
      'timelineType': instance.timelineType,
    };
