// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timeline_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TimelineListModel _$TimelineListModelFromJson(Map<String, dynamic> json) {
  return TimelineListModel(
    timelineId: json['timelineId'] as int,
    message: json['message'] as String,
    processType: json['processType'] as String?,
    profileImgOpponent: json['profileImgOpponent'] as String?,
    timelineType: json['timelineType'] as String?,
    userNameMe: json['userNameMe'] as String,
    userNameOpponent: json['userNameOpponent'] as String?,
    processParam: json['processParam'] as int?,
    processYn: json['processYn'] as String?,
    readYn: json['readYn'] as String?,
    meYn: json['meYn'] as String?,
    timelineDate: json['timelineDate'] == null
        ? null
        : DateTime.parse(json['timelineDate'] as String),
  );
}

Map<String, dynamic> _$TimelineListModelToJson(TimelineListModel instance) =>
    <String, dynamic>{
      'timelineId': instance.timelineId,
      'message': instance.message,
      'processType': instance.processType,
      'profileImgOpponent': instance.profileImgOpponent,
      'timelineType': instance.timelineType,
      'userNameMe': instance.userNameMe,
      'userNameOpponent': instance.userNameOpponent,
      'processParam': instance.processParam,
      'processYn': instance.processYn,
      'readYn': instance.readYn,
      'meYn': instance.meYn,
      'timelineDate': instance.timelineDate?.toIso8601String(),
    };
