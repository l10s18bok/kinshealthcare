import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

/*
 작성일 : 2021-03-15
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : DivisionModel,
 설명 : 저장된 결제 나누기 설정 Model
*/

part 'timeline_list_model.g.dart';

@JsonSerializable()
class TimelineListModel extends BaseRetrofitModel {
  int timelineId;
  String message;
  String? processType;
  String? profileImgOpponent;
  String? timelineType;
  String userNameMe;
  String? userNameOpponent;
  int? processParam;
  String? processYn;
  String? readYn;
  String? meYn;
  DateTime? timelineDate;

  TimelineListModel({
    required this.timelineId,
    required this.message,
    this.processType,
    this.profileImgOpponent,
    this.timelineType,
    required this.userNameMe,
    this.userNameOpponent,
    this.processParam,
    this.processYn,
    this.readYn,
    this.meYn,
    required this.timelineDate,
  });

  factory TimelineListModel.fromJson(Object? json) =>
      _$TimelineListModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$TimelineListModelToJson(this);

  bool isSameDate(DateTime? other) {
    final dateTime = this.timelineDate;
    if (dateTime == null) return false;
    if (other == null) return false;
    return dateTime.year == other.year &&
        dateTime.month == other.month &&
        dateTime.day == other.day;
  }

  get dateString {
    final dateTime = this.timelineDate;
    if (dateTime == null) return '';

    return DateFormat('yyyy.MM.dd').format(dateTime);
  }
}
