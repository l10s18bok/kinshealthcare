import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/timeline/model/timeline_list_model.dart';
import 'package:retrofit/http.dart';

part 'timeline_service.g.dart';

/*
 작성일 : 2021-03-15
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : TimelineService,
 설명 : 타임라인 Controller 의 데이터를 받아오기 위한 Service
*/

@RestApi(baseUrl: 'https://$DEV_HOST/api/user/timeline')
abstract class TimelineService extends BaseService {
  factory TimelineService(Dio dio, {String baseUrl}) = _TimelineService;

  @POST('/list')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<TimelineListModel>> list(
    @Body() GeneralPaginationBody body,
  );

  @POST('/check')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultModel> check(
    @Body() Map<String, dynamic> body,
  );
}
