import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart' hide Response;
import 'package:kins_healthcare/components/bottom_sheet/simple_information_bottom_sheet.dart';
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/controllers/auth_controller.dart';
import 'package:kins_healthcare/services/auth/auth_service.dart';
import 'package:kins_healthcare/services/banner/banner_service.dart';
import 'package:kins_healthcare/services/cert/cert_service.dart';
import 'package:kins_healthcare/services/common/base_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/donate/donate_service.dart';
import 'package:kins_healthcare/services/emergency/emergency_service.dart';
import 'package:kins_healthcare/services/force/force_service.dart';
import 'package:kins_healthcare/services/kkakka/kkakka_service.dart';
import 'package:kins_healthcare/services/login/login_service.dart';
import 'package:kins_healthcare/services/mykins/mykins_service.dart';
import 'package:kins_healthcare/services/payment/payment_service.dart';
import 'package:kins_healthcare/services/post/post_service.dart';
import 'package:kins_healthcare/services/relation/relation_service.dart';
import 'package:kins_healthcare/services/suggest/suggest_service.dart';
import 'package:kins_healthcare/services/timeline/timeline_service.dart';
import 'package:kins_healthcare/services/upload/upload_service.dart';
import 'package:kins_healthcare/services/user/payment/bank_code_service.dart';
import 'package:kins_healthcare/services/user/payment/payment_manage_service.dart';
import 'package:kins_healthcare/services/user/user_service.dart';
import 'package:kins_healthcare/utils/encoding_utils.dart';
import 'package:kins_healthcare/utils/logger_utils.dart';
import 'package:kins_healthcare/utils/storage_utils.dart';

import 'comm/comm_service.dart';
import 'custom/custom_service.dart';
import 'introduce/introduce_service.dart';

class RootService {
  static Dio _dio = Dio()
    ..interceptors.add(
      InterceptorsWrapper(
        onRequest: onRequestWrapper,
        onResponse: onResponseWrapper,
        onError: onErrorWrapper,
      ),
    );

  static int tokenErrCount = 0;
  static final int tokenErrCountMax = 5;

  // Services
  AuthService authService;
  KkakkaService kkakkaService;
  PaymentService paymentService;
  CommService commService;
  RelationService relationService;
  UploadService uploadService;
  DonateService donateService;
  EmergencyService emergencyService;
  PaymentManageService paymentManageService;
  UserService userService;
  MykinsService mykinsService;
  TimelineService timelineService;
  CertService certService;
  LoginService loginService;
  BannerService bannerService;
  CustomService customService;
  SuggestService suggestService;
  PostService postService;
  IntroduceService introduceService;
  ForceService forceService;
  BankCodeService bankCodeService;
  RootService()
      : this.authService = AuthService(_dio),
        this.kkakkaService = KkakkaService(_dio),
        this.paymentService = PaymentService(_dio),
        this.commService = CommService(_dio),
        this.relationService = RelationService(_dio),
        this.uploadService = UploadService(_dio),
        this.donateService = DonateService(_dio),
        this.emergencyService = EmergencyService(_dio),
        this.paymentManageService = PaymentManageService(_dio),
        this.userService = UserService(_dio),
        this.mykinsService = MykinsService(_dio),
        this.timelineService = TimelineService(_dio),
        this.certService = CertService(_dio),
        this.bannerService = BannerService(_dio),
        this.customService = CustomService(_dio),
        this.loginService = LoginService(_dio),
        this.suggestService = SuggestService(_dio),
        this.postService = PostService(_dio),
        this.introduceService = IntroduceService(_dio),
        this.forceService = ForceService(_dio),
        this.bankCodeService = BankCodeService(_dio);

  static parseBody(dynamic data) {
    try {
      if (data is BaseRetrofitModel ||
          data is SingleItemResponseModel ||
          data is ListResponseModel) {
        return data.toJson();
      } else {
        print(data.runtimeType);
        if (data is List) {
          print('isList');
          final dynamicList = [];
          for (int i = 0; i < data.length; i++) {
            dynamicList.add(parseBody(data[i]));
          }
          return dynamicList;
        }

        if (data is Map) {
          for (var key in data.keys) {
            // RelationController.relationApprove 호출할때 아래 항목 때문에 NoSuchMethodError 발생해서 주석처리함
            data[key] = parseBody(data[key]);
          }
        }

        if (data is int) return data;
        if (data is double) return data;
        return data;
      }
    } catch (e) {
      customLogger.e(
        '이 에러가 난다면 해결 또는 헬프요청. 실행에는 영향 없음.'
        '\n$e',
      );
    }
  }

  static onErrorWrapper(
    DioError error,
    ErrorInterceptorHandler handler,
  ) async {
    if (error == DioErrorType.connectTimeout) {
      var sheet = SimpleInformationBottomSheet(
        message: '인터텟이 불안정 합니다.확인후 다시 시도해주세요',
      );
      await Get.bottomSheet(sheet);
    }

    if (LOG_HTTP_REQUESTS) {
      customLogger.d(
        '!!!!!!!!!!ERROR THROWN WITH FOLLOWING LOG!!!!!!!!!!\n'
        'path: ${error.requestOptions.baseUrl}${error.requestOptions.path}\n'
        'status code: ${error.response?.statusCode ?? ''}\n'
        'body: ${error.response?.data.toString() ?? ''}\n'
        'headers: ${error.response?.headers ?? ''}',
      );
    }

    bool isLoginRoute = error.requestOptions.uri.path == '/oauth/token' &&
        error.requestOptions.data['scope'] == 'read write';

    if (error.response?.statusCode == 401 && !isLoginRoute) {
      if (tokenErrCount > tokenErrCountMax) {
        await Get.find<AuthController>().logoutUser();

        Get.offAllNamed('/auth');

        return;
      }

      tokenErrCount++;
      RequestOptions options = error.requestOptions;

      try {
        final resp = await Get.find<AuthController>().refreshToken();

        options.headers = {
          ...options.headers,
          'Authorization': 'Bearer ${resp.accessToken}',
        };

        final response = await _dio.fetch(options);

        return handler.resolve(response);
      } on DioError catch (e) {
        print(e);
        Get.offAllNamed('/auth');
        return handler.reject(e);
      }
    }

    // TODO production 가기전에 무조건 고처야함 RETRY 횟수를 지정하기!

    return handler.next(error);
  }

  static onResponseWrapper(
    Response resp,
    ResponseInterceptorHandler handler,
  ) async {
    tokenErrCount = 0;

    if (LOG_HTTP_REQUESTS) {
      if (resp.data is Map) {
        var encoder = JsonEncoder.withIndent('  ');
        String message = encoder.convert(resp.data);

        customLogger.d(
          '!!!!!!!!!!RESPONSE RECEIVED WITH FOLLOWING LOG!!!!!!!!!!\n'
          'path: ${resp.requestOptions.baseUrl}${resp.requestOptions.path}\n'
          'headers: ${resp.headers}'
          'body: $message\n',
        );
      } else {
        customLogger.d(
          '!!!!!!!!!!RESPONSE RECEIVED WITH FOLLOWING LOG!!!!!!!!!!\n'
          'path: ${resp.requestOptions.baseUrl}${resp.requestOptions.path}\n'
          'body: ${resp.data}\n'
          'headers: ${resp.headers}',
        );
      }
    }

    return handler.next(resp);
  }

  static onRequestWrapper(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    final utils = StorageUtils();

    if (options.headers.containsKey('accessToken')) {
      options.headers.remove('accessToken');

      final token = await utils.readByKey(key: 'accessToken');

      options.headers.addAll({
        'Authorization': 'Bearer $token',
      });
    } else if (options.headers.containsKey('refreshToken')) {
      options.headers.remove('refreshToken');

      final token = await utils.readByKey(key: 'refreshToken');

      options.headers.addAll({
        'Authorization': 'Bearer $token',
      });
    } else if (options.headers.containsKey('basicToken')) {
      options.headers.remove('basicToken');

      final encUtils = EncodingUtils();

      final token =
          encUtils.encodeBase64(str: '$AUTH_CLIENT_ID:$AUTH_CLIENT_SECRET');

      options.headers.addAll({
        'Authorization': 'Basic $token',
      });
    }
//  param 에 있는  content-type을   option에 추가.
    if (options.headers.containsKey('content-type')) {
      final ct = options.headers['content-type'];

      options.contentType = ct;
    }

    if (options.data is BaseRetrofitModel) {
      options.data = options.data.toJson();
    }

    if (LOG_HTTP_REQUESTS) {
      customLogger.d('!!!!!!!!!!REQUEST SENT WITH FOLLOWING LOG!!!!!!!!!!\n'
          'path: ${options.baseUrl}${options.path}\n'
          'body: ${parseBody(options.data)}\n'
          'headers: ${options.headers}');
    }

    return handler.next(options);
  }
}
