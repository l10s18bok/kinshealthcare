import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/payment/model/payment_kkakka_list_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_payment_request_list_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_payment_request_reject_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_payment_request_status.dart';
import 'package:kins_healthcare/services/payment/model/payment_request_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_request_steps_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_stop_request_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_store_create_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_store_one_models.dart';
import 'package:retrofit/http.dart';

import 'model/payment_cancel_models.dart';
import 'model/payment_list_models.dart';

part 'payment_service.g.dart';

@RestApi(baseUrl: 'https://$DEV_HOST/api/payment')
abstract class PaymentService extends BaseService {
  factory PaymentService(Dio dio, {String baseUrl}) = _PaymentService;

  @POST('/request')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<PostPaymentRequestResponse> request(
    @Body() PostPaymentRequestBody body,
  );
//
  @POST('/store/one')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseModel<PostPaymentStoreOneResponse>> postStoreOne(
    //int storeId,
    @Body() PostPaymentKkakkaListBody body,
  );

  @POST('/create')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<PostPaymentStoreCreateResponse> postPaymentCreate(
    @Body() PostPaymentCreateBody body,
  );

  @POST('/kkakka/list')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<PostPaymentKkakkaListResponse>> postKkakkaList(
    @Body() PostPaymentKkakkaListBody body,
  );

  @POST('/list')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<PaymentListModel>> paymentList(
    @Body() GeneralPaginationBody body,
  );

  @POST('/cancel/request')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<PaymentCancelModel>> paymentCancelRequest(
    @Body() paymentCancelApproveBody,
  );

  @POST('/paymentRequest/step0')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> paymentRequestStep0(
    @Body() PaymentRequestSteps0Body body,
  );

  @POST('/paymentRequest/step1')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> paymentRequestStep1(
    @Body() PaymentRequestSteps1Body body,
  );

  @POST('/paymentRequest/step2')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> paymentRequestStep2(
    @Body() PaymentRequestSteps2Body body,
  );

  @POST('/paymentRequest/status')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseModel<PaymentRequestStatusResponse>> paymentRequestStatus(
    @Body() PaymentRequestStatusBody body,
  );

  @POST('/paymentRequest/status/stepN')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseModel<PaymentRequestStatusResponse>> paymentRequestStatusStepN(
    @Body() PaymentRequestStatusStepNBody body,
  );

  @POST('/stop')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> stop(
    @Body() PaymentStopBody body,
  );

  @POST('/paymentRequest/list')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<PaymentRequestListResponse>> paymentRequestList(
    @Body() GeneralPaginationBody body,
  );

  @POST('/paymentRequest/reject')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> paymentRequestReject(
    @Body() PaymentRequestRejectBody body,
  );
}
