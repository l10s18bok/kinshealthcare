import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'payment_list_models.g.dart';

@JsonSerializable()
class PaymentListModel extends BaseRetrofitModel {
  final int setleId;
  final int? storeNo;
  final int? userNo;
  final int? totalAmount;
  final String? message;
  final String? setleStatus;
  final String? userName;
  final String? userImage;
  final String? storeName;
  final String? address1;
  final String? address2;
  final DateTime? lastDate;

  PaymentListModel({
    required this.setleId,
    this.storeNo,
    this.userNo,
    this.totalAmount,
    this.message,
    this.setleStatus,
    this.userName,
    this.userImage,
    this.storeName,
    this.address1,
    this.address2,
    this.lastDate,
  });

  get daysTillLastDate {
    return DateTime.now().difference(this.lastDate!).inDays;
  }

  get hoursTillLastDate {
    return DateTime.now().difference(this.lastDate!).inHours;
  }

  get minutesTillLastDate {
    return DateTime.now().difference(this.lastDate!).inMinutes;
  }

  get lastDateString {
    if (lastDate == null) return '';
    return DateFormat('yyyy.MM.dd').format(lastDate!);
  }

  factory PaymentListModel.fromJson(Object? json) =>
      _$PaymentListModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PaymentListModelToJson(this);

  copyWith({
    String? setleStatus,
  }) {
    return PaymentListModel(
      setleId: this.setleId,
      storeNo: this.storeNo,
      userNo: this.userNo,
      totalAmount: this.totalAmount,
      message: this.message,
      setleStatus: setleStatus,
      userName: this.userName,
      userImage: this.userImage,
      storeName: this.storeName,
      address1: this.address1,
      address2: this.address2,
      lastDate: this.lastDate,
    );
  }
}
