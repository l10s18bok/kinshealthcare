// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_payment_request_list_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentRequestListBody _$PaymentRequestListBodyFromJson(
    Map<String, dynamic> json) {
  return PaymentRequestListBody(
    target: json['target'] as String,
    direction: json['direction'] as bool,
    page: json['page'] as int,
    size: json['size'] as int,
    sortBy: json['sortBy'] as String,
  );
}

Map<String, dynamic> _$PaymentRequestListBodyToJson(
        PaymentRequestListBody instance) =>
    <String, dynamic>{
      'direction': instance.direction,
      'page': instance.page,
      'size': instance.size,
      'sortBy': instance.sortBy,
      'target': instance.target,
    };

PaymentRequestListResponse _$PaymentRequestListResponseFromJson(
    Map<String, dynamic> json) {
  return PaymentRequestListResponse(
    address1: json['address1'] as String,
    address2: json['address2'] as String,
    amount: json['amount'] as int,
    comName: json['comName'] as String,
    createDateTime: json['createDateTime'] as String,
    profileImgFrom: json['profileImgFrom'] as String,
    profileImgUse: json['profileImgUse'] as String,
    requestCount: json['requestCount'] as int,
    requestStatus: json['requestStatus'] as String,
    setlePaymentId: json['setlePaymentId'] as int,
    userNameFrom: json['userNameFrom'] as String,
    userNameUse: json['userNameUse'] as String,
    storeId: json['storeId'] as int,
  );
}

Map<String, dynamic> _$PaymentRequestListResponseToJson(
        PaymentRequestListResponse instance) =>
    <String, dynamic>{
      'address1': instance.address1,
      'address2': instance.address2,
      'amount': instance.amount,
      'storeId': instance.storeId,
      'comName': instance.comName,
      'createDateTime': instance.createDateTime,
      'profileImgFrom': instance.profileImgFrom,
      'profileImgUse': instance.profileImgUse,
      'requestCount': instance.requestCount,
      'requestStatus': instance.requestStatus,
      'setlePaymentId': instance.setlePaymentId,
      'userNameFrom': instance.userNameFrom,
      'userNameUse': instance.userNameUse,
    };
