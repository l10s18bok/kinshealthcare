// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_list_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentListModel _$PaymentListModelFromJson(Map<String, dynamic> json) {
  return PaymentListModel(
    setleId: json['setleId'] as int,
    storeNo: json['storeNo'] as int?,
    userNo: json['userNo'] as int?,
    totalAmount: json['totalAmount'] as int?,
    message: json['message'] as String?,
    setleStatus: json['setleStatus'] as String?,
    userName: json['userName'] as String?,
    userImage: json['userImage'] as String?,
    storeName: json['storeName'] as String?,
    address1: json['address1'] as String?,
    address2: json['address2'] as String?,
    lastDate: json['lastDate'] == null
        ? null
        : DateTime.parse(json['lastDate'] as String),
  );
}

Map<String, dynamic> _$PaymentListModelToJson(PaymentListModel instance) =>
    <String, dynamic>{
      'setleId': instance.setleId,
      'storeNo': instance.storeNo,
      'userNo': instance.userNo,
      'totalAmount': instance.totalAmount,
      'message': instance.message,
      'setleStatus': instance.setleStatus,
      'userName': instance.userName,
      'userImage': instance.userImage,
      'storeName': instance.storeName,
      'address1': instance.address1,
      'address2': instance.address2,
      'lastDate': instance.lastDate?.toIso8601String(),
    };
