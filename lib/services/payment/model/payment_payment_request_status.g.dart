// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_payment_request_status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentRequestStatusStepNBody _$PaymentRequestStatusStepNBodyFromJson(
    Map<String, dynamic> json) {
  return PaymentRequestStatusStepNBody(
    setlePaymentId: json['setlePaymentId'] as int,
  );
}

Map<String, dynamic> _$PaymentRequestStatusStepNBodyToJson(
        PaymentRequestStatusStepNBody instance) =>
    <String, dynamic>{
      'setlePaymentId': instance.setlePaymentId,
    };

PaymentRequestStatusBody _$PaymentRequestStatusBodyFromJson(
    Map<String, dynamic> json) {
  return PaymentRequestStatusBody(
    setleId: json['setleId'] as int,
  );
}

Map<String, dynamic> _$PaymentRequestStatusBodyToJson(
        PaymentRequestStatusBody instance) =>
    <String, dynamic>{
      'setleId': instance.setleId,
    };

PaymentRequestStatusResponseRequest
    _$PaymentRequestStatusResponseRequestFromJson(Map<String, dynamic> json) {
  return PaymentRequestStatusResponseRequest(
    requestStatus: json['requestStatus'] as String,
    amount: json['amount'] as int,
    userNameTo: json['userNameTo'] as String,
    profileImgTo: json['profileImgTo'] as String,
    userRelCdTo: json['userRelCdTo'] as String,
    approveAmount: json['approveAmount'] as int?,
    requestDateTime: json['requestDateTime'] as String,
  );
}

Map<String, dynamic> _$PaymentRequestStatusResponseRequestToJson(
        PaymentRequestStatusResponseRequest instance) =>
    <String, dynamic>{
      'requestStatus': instance.requestStatus,
      'amount': instance.amount,
      'approveAmount': instance.approveAmount,
      'userNameTo': instance.userNameTo,
      'profileImgTo': instance.profileImgTo,
      'userRelCdTo': instance.userRelCdTo,
      'requestDateTime': instance.requestDateTime.toIso8601String(),
    };

PaymentRequestStatusResponseMe _$PaymentRequestStatusResponseMeFromJson(
    Map<String, dynamic> json) {
  return PaymentRequestStatusResponseMe(
    requestStatus: json['requestStatus'] as String?,
    amount: json['amount'] as int?,
    setlePaymentType: json['setlePaymentType'] as String?,
    kkakkaImg: json['kkakkaImg'] as String?,
    userPaymentWayType: json['userPaymentWayType'] as String?,
    userPaymentWayCompany: json['userPaymentWayCompany'] as String?,
    kkakkaUserName: json['kkakkaUserName'] as String?,
    kkakkaProfileImg: json['kkakkaProfileImg'] as String?,
    kkakkaUserRelCd: json['kkakkaUserRelCd'] as String?,
    requestDateTime: json['requestDateTime'] as String?,
    kkakkaType: json['kkakkaType'] as String?,
    approveAmount: json['approveAmount'] as int?,
  );
}

Map<String, dynamic> _$PaymentRequestStatusResponseMeToJson(
        PaymentRequestStatusResponseMe instance) =>
    <String, dynamic>{
      'requestStatus': instance.requestStatus,
      'setlePaymentType': instance.setlePaymentType,
      'amount': instance.amount,
      'approveAmount': instance.approveAmount,
      'kkakkaImg': instance.kkakkaImg,
      'kkakkaProfileImg': instance.kkakkaProfileImg,
      'kkakkaUserRelCd': instance.kkakkaUserRelCd,
      'kkakkaUserName': instance.kkakkaUserName,
      'userPaymentWayType': instance.userPaymentWayType,
      'userPaymentWayCompany': instance.userPaymentWayCompany,
      'requestDateTime': instance.requestDateTime,
      'kkakkaType': _$KkakkaTypeEnumMap[instance.kkakkaType],
    };

const _$KkakkaTypeEnumMap = {
  KkakkaType.ANNIVERSARY: 'ANNIVERSARY',
  KkakkaType.DONATE: 'DONATE',
  KkakkaType.AREA: 'AREA',
  KkakkaType.MEDICAL: 'MEDICAL',
  KkakkaType.PIN: 'PIN',
  KkakkaType.SELF: 'SELF',
  KkakkaType.WELFARE: 'WELFARE',
  KkakkaType.EDUCATION: 'EDUCATION',
  KkakkaType.ALL: 'ALL',
};

PaymentRequestStatusResponse _$PaymentRequestStatusResponseFromJson(
    Map<String, dynamic> json) {
  return PaymentRequestStatusResponse(
    setleStatus: json['setleStatus'] as String,
    me: (json['me'] as List<dynamic>)
        .map((e) =>
            PaymentRequestStatusResponseMe.fromJson(e as Map<String, dynamic>))
        .toList(),
    request: (json['request'] as List<dynamic>)
        .map((e) => PaymentRequestStatusResponseRequest.fromJson(
            e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$PaymentRequestStatusResponseToJson(
        PaymentRequestStatusResponse instance) =>
    <String, dynamic>{
      'setleStatus': instance.setleStatus,
      'me': instance.me,
      'request': instance.request,
    };
