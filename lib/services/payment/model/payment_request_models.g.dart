// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_request_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostPaymentRequestBodyKkakka _$PostPaymentRequestBodyKkakkaFromJson(
    Map<String, dynamic> json) {
  return PostPaymentRequestBodyKkakka(
    amount: json['amount'] as int,
    kkakkaId: json['kkakkaId'] as int,
  );
}

Map<String, dynamic> _$PostPaymentRequestBodyKkakkaToJson(
        PostPaymentRequestBodyKkakka instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'kkakkaId': instance.kkakkaId,
    };

PostPaymentRequestBody _$PostPaymentRequestBodyFromJson(
    Map<String, dynamic> json) {
  return PostPaymentRequestBody(
    setleId: json['setleId'] as int?,
  );
}

Map<String, dynamic> _$PostPaymentRequestBodyToJson(
        PostPaymentRequestBody instance) =>
    <String, dynamic>{
      'setleId': instance.setleId,
    };

PostPaymentRequestResponse _$PostPaymentRequestResponseFromJson(
    Map<String, dynamic> json) {
  return PostPaymentRequestResponse(
    message: json['message'] as String?,
    setleId: json['setleId'] as int?,
    success: json['success'] as bool?,
  );
}

Map<String, dynamic> _$PostPaymentRequestResponseToJson(
        PostPaymentRequestResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
      'setleId': instance.setleId,
      'success': instance.success,
    };
