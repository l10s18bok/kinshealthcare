import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'payment_stop_request_models.g.dart';

@JsonSerializable()
class PaymentStopBody extends BaseRetrofitModel {
  final int setleId;

  PaymentStopBody({
    required this.setleId,
  });

  factory PaymentStopBody.fromJson(Map<String, dynamic> json) =>
      _$PaymentStopBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentStopBodyToJson(this);
}
