// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_store_one_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostPaymentStoreOneResponse _$PostPaymentStoreOneResponseFromJson(
    Map<String, dynamic> json) {
  return PostPaymentStoreOneResponse(
    address1: json['address1'] as String?,
    address2: json['address2'] as String?,
    comName: json['comName'] as String?,
    id: json['id'] as int?,
  );
}

Map<String, dynamic> _$PostPaymentStoreOneResponseToJson(
        PostPaymentStoreOneResponse instance) =>
    <String, dynamic>{
      'address1': instance.address1,
      'address2': instance.address2,
      'comName': instance.comName,
      'id': instance.id,
    };
