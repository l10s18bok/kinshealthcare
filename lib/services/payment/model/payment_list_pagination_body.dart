import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';

part 'payment_list_pagination_body.g.dart';
/*
 작성일 : 2021-03-10
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : ListFilterPaginationBody,
 설명 : 까까 List 를 필터링 하기 위한 요청의 Body
*/

@JsonSerializable()
class PaymentListPaginationBody extends GeneralPaginationBody {
  final String? target;

  PaymentListPaginationBody({
    bool direction = true,
    String sortBy = "last",
    int page = 0,
    int size = 10,
    this.target,
  }) : super(
          direction: direction,
          sortBy: sortBy,
          page: page,
          size: size,
        );

  factory PaymentListPaginationBody.fromJson(Map<String, dynamic> json) =>
      _$PaymentListPaginationBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentListPaginationBodyToJson(this);

  copyWith({
    bool? direction,
    int? page,
    int? size,
    String? sortBy,
  }) {
    direction ??= this.direction;
    page ??= this.page;
    size ??= this.size;
    sortBy ??= this.sortBy;

    return PaymentListPaginationBody(
      direction: direction,
      page: page,
      size: size,
      sortBy: sortBy,
      target: this.target,
    );
  }
}
