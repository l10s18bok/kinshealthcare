import 'package:json_annotation/json_annotation.dart';

part 'payment_store_one_models.g.dart';

@JsonSerializable()
class PostPaymentStoreOneResponse {
  final String? address1;
  final String? address2;
  final String? comName;
  final int? id;

  PostPaymentStoreOneResponse({
    this.address1,
    this.address2,
    this.comName,
    this.id,
  });

  factory PostPaymentStoreOneResponse.fromJson(Object? json) =>
      _$PostPaymentStoreOneResponseFromJson(json as Map<String,dynamic>);

  Map<String, dynamic> toJson() => _$PostPaymentStoreOneResponseToJson(this);
}
