import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'payment_request_steps_models.g.dart';

@JsonSerializable()
class PaymentStepsUserPaymentWay extends BaseRetrofitModel {
  final int approveAmount;
  final int userPaymentWayId;

  PaymentStepsUserPaymentWay({
    required this.approveAmount,
    required this.userPaymentWayId,
  });

  factory PaymentStepsUserPaymentWay.fromJson(Map<String, dynamic> json) =>
      _$PaymentStepsUserPaymentWayFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentStepsUserPaymentWayToJson(this);
}

@JsonSerializable()
class PaymentStepsLocalCurrency extends BaseRetrofitModel {
  final int approveAmount;
  final String localCurrencyCode;

  PaymentStepsLocalCurrency({
    required this.approveAmount,
    required this.localCurrencyCode,
  });

  factory PaymentStepsLocalCurrency.fromJson(Map<String, dynamic> json) =>
      _$PaymentStepsLocalCurrencyFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentStepsLocalCurrencyToJson(this);
}

@JsonSerializable()
class PaymentStepsKkakka extends BaseRetrofitModel {
  final int approveAmount;
  final int kkakkaId;

  PaymentStepsKkakka({
    required this.approveAmount,
    required this.kkakkaId,
  });

  factory PaymentStepsKkakka.fromJson(Map<String, dynamic> json) =>
      _$PaymentStepsKkakkaFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentStepsKkakkaToJson(this);
}

@JsonSerializable()
class PaymentStepsApproveMe extends BaseRetrofitModel {
  final int approveTotalAmount;
  final List<PaymentStepsKkakka>? kkakkas;
  final List<PaymentStepsLocalCurrency>? localCurrencys;
  final List<PaymentStepsUserPaymentWay>? userPaymentWays;

  PaymentStepsApproveMe({
    required this.approveTotalAmount,
    this.kkakkas,
    this.localCurrencys,
    this.userPaymentWays,
  });

  factory PaymentStepsApproveMe.fromJson(Map<String, dynamic> json) =>
      _$PaymentStepsApproveMeFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentStepsApproveMeToJson(this);
}

@JsonSerializable()
class PaymentStepsToUser extends BaseRetrofitModel {
  final int amount;
  final int toUserNo;

  PaymentStepsToUser({
    required this.amount,
    required this.toUserNo,
  });

  factory PaymentStepsToUser.fromJson(Map<String, dynamic> json) =>
      _$PaymentStepsToUserFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentStepsToUserToJson(this);
}

@JsonSerializable()
class PaymentStepsRequestUser extends BaseRetrofitModel {
  final int approveTotalAmount;
  final List<PaymentStepsToUser> toUsers;

  PaymentStepsRequestUser({
    required this.approveTotalAmount,
    required this.toUsers,
  });

  factory PaymentStepsRequestUser.fromJson(Map<String, dynamic> json) =>
      _$PaymentStepsRequestUserFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentStepsRequestUserToJson(this);
}

@JsonSerializable()
class PaymentRequestSteps2Body extends PaymentRequestStepsBody {
  final int setlePaymentId;

  PaymentRequestSteps2Body({
    required int amount,
    required PaymentStepsApproveMe approveMe,
    required this.setlePaymentId,
  }) : super(
          amount: amount,
          approveMe: approveMe,
        );

  factory PaymentRequestSteps2Body.fromJson(Map<String, dynamic> json) =>
      _$PaymentRequestSteps2BodyFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentRequestSteps2BodyToJson(this);
}

@JsonSerializable()
class PaymentRequestSteps1Body extends PaymentRequestStepsBody {
  final int setlePaymentId;
  final PaymentStepsRequestUser requestUser;

  PaymentRequestSteps1Body({
    required int amount,
    required PaymentStepsApproveMe approveMe,
    required this.requestUser,
    required this.setlePaymentId,
  }) : super(
          amount: amount,
          approveMe: approveMe,
        );

  factory PaymentRequestSteps1Body.fromJson(Map<String, dynamic> json) =>
      _$PaymentRequestSteps1BodyFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentRequestSteps1BodyToJson(this);
}

@JsonSerializable()
class PaymentRequestSteps0Body extends PaymentRequestStepsBody {
  final int setleId;
  final PaymentStepsRequestUser requestUser;

  PaymentRequestSteps0Body({
    required int amount,
    required PaymentStepsApproveMe approveMe,
    required this.requestUser,
    required this.setleId,
  }) : super(
          amount: amount,
          approveMe: approveMe,
        );

  factory PaymentRequestSteps0Body.fromJson(Map<String, dynamic> json) =>
      _$PaymentRequestSteps0BodyFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentRequestSteps0BodyToJson(this);
}

@JsonSerializable()
class PaymentRequestStepsBody extends BaseRetrofitModel {
  final int amount;
  final PaymentStepsApproveMe approveMe;

  PaymentRequestStepsBody({
    required this.amount,
    required this.approveMe,
  });

  factory PaymentRequestStepsBody.fromJson(Map<String, dynamic> json) =>
      _$PaymentRequestStepsBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentRequestStepsBodyToJson(this);
}
