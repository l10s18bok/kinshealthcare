import 'package:json_annotation/json_annotation.dart';

part 'payment_store_create_models.g.dart';

@JsonSerializable()
class PostPaymentStoreCreateResponseData {
  final int? setleId;

  PostPaymentStoreCreateResponseData({this.setleId});

  factory PostPaymentStoreCreateResponseData.fromJson(Map<String, dynamic> json) =>
      _$PostPaymentStoreCreateResponseDataFromJson(json);

  Map<String, dynamic> toJson() => _$PostPaymentStoreCreateResponseDataToJson(this);
}

@JsonSerializable()
class PostPaymentStoreCreateResponse {
  final String? resultCode;
  final PostPaymentStoreCreateResponseData? data;

  PostPaymentStoreCreateResponse({
    this.resultCode,
    this.data,
  });

  factory PostPaymentStoreCreateResponse.fromJson(Map<String, dynamic> json) =>
      _$PostPaymentStoreCreateResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PostPaymentStoreCreateResponseToJson(this);
}

@JsonSerializable()
class PostPaymentCreateBody {
  final int? storeId;
  final int? totalAmount;

  PostPaymentCreateBody({
    this.storeId,
    this.totalAmount,
  });

  factory PostPaymentCreateBody.fromJson(Map<String, dynamic> json) =>
      _$PostPaymentCreateBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostPaymentCreateBodyToJson(this);
}
