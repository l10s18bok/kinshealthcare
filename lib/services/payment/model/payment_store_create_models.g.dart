// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_store_create_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostPaymentStoreCreateResponseData _$PostPaymentStoreCreateResponseDataFromJson(
    Map<String, dynamic> json) {
  return PostPaymentStoreCreateResponseData(
    setleId: json['setleId'] as int?,
  );
}

Map<String, dynamic> _$PostPaymentStoreCreateResponseDataToJson(
        PostPaymentStoreCreateResponseData instance) =>
    <String, dynamic>{
      'setleId': instance.setleId,
    };

PostPaymentStoreCreateResponse _$PostPaymentStoreCreateResponseFromJson(
    Map<String, dynamic> json) {
  return PostPaymentStoreCreateResponse(
    resultCode: json['resultCode'] as String?,
    data: json['data'] == null
        ? null
        : PostPaymentStoreCreateResponseData.fromJson(
            json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$PostPaymentStoreCreateResponseToJson(
        PostPaymentStoreCreateResponse instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'data': instance.data,
    };

PostPaymentCreateBody _$PostPaymentCreateBodyFromJson(
    Map<String, dynamic> json) {
  return PostPaymentCreateBody(
    storeId: json['storeId'] as int?,
    totalAmount: json['totalAmount'] as int?,
  );
}

Map<String, dynamic> _$PostPaymentCreateBodyToJson(
        PostPaymentCreateBody instance) =>
    <String, dynamic>{
      'storeId': instance.storeId,
      'totalAmount': instance.totalAmount,
    };
