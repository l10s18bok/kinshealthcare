import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'payment_payment_request_status.g.dart';

@JsonSerializable()
class PaymentRequestStatusStepNBody extends BaseRetrofitModel {
  final int setlePaymentId;

  PaymentRequestStatusStepNBody({
    required this.setlePaymentId,
  });

  factory PaymentRequestStatusStepNBody.fromJson(Map<String, dynamic> json) =>
      _$PaymentRequestStatusStepNBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentRequestStatusStepNBodyToJson(this);
}

@JsonSerializable()
class PaymentRequestStatusBody extends BaseRetrofitModel {
  final int setleId;

  PaymentRequestStatusBody({
    required this.setleId,
  });

  factory PaymentRequestStatusBody.fromJson(Map<String, dynamic> json) =>
      _$PaymentRequestStatusBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentRequestStatusBodyToJson(this);
}

@JsonSerializable()
class PaymentRequestStatusResponseRequest extends BaseRetrofitModel {
  final String requestStatus;
  final int amount;
  final int? approveAmount;
  final String userNameTo;
  final String profileImgTo;
  final String userRelCdTo;
  final String _requestDateTime;

  PaymentRequestStatusResponseRequest({
    required this.requestStatus,
    required this.amount,
    required this.userNameTo,
    required this.profileImgTo,
    required this.userRelCdTo,
    this.approveAmount,
    required String requestDateTime,
  }) : this._requestDateTime = requestDateTime;

  DateTime get requestDateTime {
    return DateTime.parse(this._requestDateTime);
  }

  bool get isApproved {
    return this.requestStatus == '승인';
  }

  bool get isRejected {
    return this.requestStatus == '거절';
  }

  factory PaymentRequestStatusResponseRequest.fromJson(
          Map<String, dynamic> json) =>
      _$PaymentRequestStatusResponseRequestFromJson(json);

  Map<String, dynamic> toJson() =>
      _$PaymentRequestStatusResponseRequestToJson(this);
}

@JsonSerializable()
class PaymentRequestStatusResponseMe extends BaseRetrofitModel {
  final String? requestStatus;
  final String? setlePaymentType;
  final int? amount;
  final int? approveAmount;
  final String? kkakkaImg;
  final String? kkakkaProfileImg;
  final String? kkakkaUserRelCd;
  final String? kkakkaUserName;
  final String? userPaymentWayType;
  final String? userPaymentWayCompany;
  final String? _kkakkaType;
  final String? _requestDateTime;

  final _kkakkaTypeMap = {
    '기념일': KkakkaType.ANNIVERSARY,
    '기부': KkakkaType.DONATE,
    '지역': KkakkaType.AREA,
    '의료': KkakkaType.MEDICAL,
    '용돈': KkakkaType.PIN,
    'SELF': KkakkaType.SELF,
    '복지': KkakkaType.WELFARE,
    'ANNIVERSARY': KkakkaType.ANNIVERSARY,
    'DONATE': KkakkaType.DONATE,
    'AREA': KkakkaType.AREA,
    'MEDICAL': KkakkaType.MEDICAL,
    'PIN': KkakkaType.PIN,
    'WELFARE': KkakkaType.WELFARE,
  };

  PaymentRequestStatusResponseMe({
    this.requestStatus,
    this.amount,
    this.setlePaymentType,
    this.kkakkaImg,
    this.userPaymentWayType,
    this.userPaymentWayCompany,
    this.kkakkaUserName,
    this.kkakkaProfileImg,
    this.kkakkaUserRelCd,
    String? requestDateTime,
    String? kkakkaType,
    this.approveAmount,
  })  : this._requestDateTime = requestDateTime,
        this._kkakkaType = kkakkaType;

  get requestDateTime {
    if (this._requestDateTime == null) {
      return null;
    }
    return DateTime.parse(this._requestDateTime!);
  }

  bool get isApproved {
    return this.requestStatus == '승인';
  }

  bool get isRejected {
    return this.requestStatus == '거절';
  }

  KkakkaType get kkakkaType {
    return this._kkakkaTypeMap[this._kkakkaType]!;
  }

  get kkakkaIcName {
    Map<KkakkaType, String> dict = {
      KkakkaType.ANNIVERSARY: 'ic_anniversary_kkakka.svg',
      KkakkaType.AREA: 'ic_area_kkakka.svg',
      KkakkaType.DONATE: 'ic_donate_kkakka.svg',
      KkakkaType.MEDICAL: 'ic_medical_kkakka.svg',
      KkakkaType.PIN: 'ic_pin_kkakka.svg',
      KkakkaType.SELF: 'ic_self_kkakka.svg',
      KkakkaType.WELFARE: 'ic_welfare_kkakka.svg',
    };

    return dict[this.kkakkaType];
  }

  get imageName {
    String basePath = 'assets/svgs/ic/';
    switch (this.kkakkaType) {
      // case KkakkaType.WELFARE:
      // case KkakkaType.SELF:
      case KkakkaType.MEDICAL:
        basePath += 'ic_medical_pill.svg';
        break;
      // case KkakkaType.AREA:
      case KkakkaType.DONATE:
        basePath += 'ic_donation_bean.svg';
        break;
      case KkakkaType.ANNIVERSARY:
        basePath += 'ic_wedding_ring.svg';
        break;
      case KkakkaType.PIN:
        basePath += 'ic_cash_money.svg';
        break;
      case KkakkaType.EDUCATION:
        basePath += 'ic_education_book.svg';
        break;
      default:
        return null;
    }

    return basePath;
  }

  get primaryColor {
    // TODO setup primary color options

    Map<KkakkaType, Color> dict = {
      KkakkaType.ANNIVERSARY: Color(0xFFFF9B95),
      KkakkaType.AREA: Color(0xFFFF9B95),
      KkakkaType.DONATE: Color(0xFF8B6ADC),
      KkakkaType.MEDICAL: Color(0xFF6FA7FA),
      KkakkaType.PIN: Color(0xFF6CC4BF),
      KkakkaType.SELF: Color(0xFF6CC4BF),
      KkakkaType.WELFARE: Color(0xFF8B6ADC),
    };

    return dict[this.kkakkaType];
  }

  factory PaymentRequestStatusResponseMe.fromJson(Map<String, dynamic> json) =>
      _$PaymentRequestStatusResponseMeFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentRequestStatusResponseMeToJson(this);
}

@JsonSerializable()
class PaymentRequestStatusResponse extends BaseRetrofitModel {
  final String setleStatus;
  final List<PaymentRequestStatusResponseMe> me;
  final List<PaymentRequestStatusResponseRequest> request;

  PaymentRequestStatusResponse({
    required this.setleStatus,
    required this.me,
    required this.request,
  });

  factory PaymentRequestStatusResponse.fromJson(Object? json) =>
      _$PaymentRequestStatusResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PaymentRequestStatusResponseToJson(this);
}

// @JsonSerializable()
// class PaymentRequestResponse extends BaseRetrofitModel {
//   final String requestStatus;
//   final int amount;
//   final int? approveTotalAmount;
//   final String userNameTo;
//   final String profileImgTo;
//   final String userRelCd;
//   final String _requestDateTime;
//
//   PaymentRequestResponse({
//     required this.requestStatus,
//     required this.amount,
//     required this.userNameTo,
//     required this.profileImgTo,
//     required this.userRelCd,
//     required String requestDateTime,
//     this.approveTotalAmount,
//   }) : this._requestDateTime = requestDateTime;
//
//   get requestDateTime {
//     return DateTime.parse(this._requestDateTime);
//   }
//
//   get isApproved {
//     return this.requestStatus == '승인';
//   }
//
//   factory PaymentRequestResponse.fromJson(Object? json) =>
//       _$PaymentRequestResponseFromJson(json as Map<String, dynamic>);
//
//   Map<String, dynamic> toJson() => _$PaymentRequestResponseToJson(this);
// }
