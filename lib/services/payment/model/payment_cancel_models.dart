import 'package:json_annotation/json_annotation.dart';

part 'payment_cancel_models.g.dart';

@JsonSerializable()
class PaymentCancelModel {
  final String? message;
  final int? setleId;
  final bool? success;

  PaymentCancelModel({
    this.message,
    this.setleId,
    this.success,
  });

  factory PaymentCancelModel.fromJson(Object? json) =>
      _$PaymentCancelModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PaymentCancelModelToJson(this);
}
