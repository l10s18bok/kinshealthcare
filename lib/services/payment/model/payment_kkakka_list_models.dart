import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/enums/model_enums.dart';

part 'payment_kkakka_list_models.g.dart';

@JsonSerializable()
class PostPaymentKkakkaListBody {
  final int storeId;

  PostPaymentKkakkaListBody({
    required this.storeId,
  });

  factory PostPaymentKkakkaListBody.fromJson(Map<String, dynamic> json) =>
      _$PostPaymentKkakkaListBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostPaymentKkakkaListBodyToJson(this);
}

@JsonSerializable()
class PostPaymentKkakkaListResponse {
  final int? kkakkaBalance;
  final int? kkakkaId;
  final String? kkakkaImage;
  final int? kkakkaLimit;
  final String? kkakkaMessage;
  final int? kkakkaPrice;
  final String? kkakkaStatus;
  final String? kkakkaType;
  final String? endDate;
  final String? profileImgOpponent;
  final String? startDate;
  final String? userNameOpponent;
  final String? userPhoneOpponent;
  final String? userRelCdMe;
  final String? userRelCdOpponent;
  final String? userStatusOpponent;

  PostPaymentKkakkaListResponse({
    this.kkakkaBalance,
    this.kkakkaId,
    this.kkakkaImage,
    this.kkakkaLimit,
    this.kkakkaMessage,
    this.kkakkaPrice,
    this.kkakkaStatus,
    this.kkakkaType,
    this.endDate,
    this.userNameOpponent,
    this.userPhoneOpponent,
    this.userRelCdMe,
    this.userRelCdOpponent,
    this.profileImgOpponent,
    this.startDate,
    this.userStatusOpponent,
  });

  factory PostPaymentKkakkaListResponse.fromJson(Object? json) =>
      _$PostPaymentKkakkaListResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PostPaymentKkakkaListResponseToJson(this);

  get parsedType {
    for (KkakkaType type in KkakkaType.values) {
      if (this.kkakkaType == describeEnum(type)) {
        return type;
      }
    }
  }

  get endDateDaysTill {
    if (this.endDate == null) {
      return 0;
    }

    return DateTime.parse(this.endDate!).difference(DateTime.now()).inDays;
  }
}
