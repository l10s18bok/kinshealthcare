// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_kkakka_list_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostPaymentKkakkaListBody _$PostPaymentKkakkaListBodyFromJson(
    Map<String, dynamic> json) {
  return PostPaymentKkakkaListBody(
    storeId: json['storeId'] as int,
  );
}

Map<String, dynamic> _$PostPaymentKkakkaListBodyToJson(
        PostPaymentKkakkaListBody instance) =>
    <String, dynamic>{
      'storeId': instance.storeId,
    };

PostPaymentKkakkaListResponse _$PostPaymentKkakkaListResponseFromJson(
    Map<String, dynamic> json) {
  return PostPaymentKkakkaListResponse(
    kkakkaBalance: json['kkakkaBalance'] as int?,
    kkakkaId: json['kkakkaId'] as int?,
    kkakkaImage: json['kkakkaImage'] as String?,
    kkakkaLimit: json['kkakkaLimit'] as int?,
    kkakkaMessage: json['kkakkaMessage'] as String?,
    kkakkaPrice: json['kkakkaPrice'] as int?,
    kkakkaStatus: json['kkakkaStatus'] as String?,
    kkakkaType: json['kkakkaType'] as String?,
    endDate: json['endDate'] as String?,
    userNameOpponent: json['userNameOpponent'] as String?,
    userPhoneOpponent: json['userPhoneOpponent'] as String?,
    userRelCdMe: json['userRelCdMe'] as String?,
    userRelCdOpponent: json['userRelCdOpponent'] as String?,
    profileImgOpponent: json['profileImgOpponent'] as String?,
    startDate: json['startDate'] as String?,
    userStatusOpponent: json['userStatusOpponent'] as String?,
  );
}

Map<String, dynamic> _$PostPaymentKkakkaListResponseToJson(
        PostPaymentKkakkaListResponse instance) =>
    <String, dynamic>{
      'kkakkaBalance': instance.kkakkaBalance,
      'kkakkaId': instance.kkakkaId,
      'kkakkaImage': instance.kkakkaImage,
      'kkakkaLimit': instance.kkakkaLimit,
      'kkakkaMessage': instance.kkakkaMessage,
      'kkakkaPrice': instance.kkakkaPrice,
      'kkakkaStatus': instance.kkakkaStatus,
      'kkakkaType': instance.kkakkaType,
      'endDate': instance.endDate,
      'profileImgOpponent': instance.profileImgOpponent,
      'startDate': instance.startDate,
      'userNameOpponent': instance.userNameOpponent,
      'userPhoneOpponent': instance.userPhoneOpponent,
      'userRelCdMe': instance.userRelCdMe,
      'userRelCdOpponent': instance.userRelCdOpponent,
      'userStatusOpponent': instance.userStatusOpponent,
    };
