// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_payment_request_reject_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentRequestRejectBody _$PaymentRequestRejectBodyFromJson(
    Map<String, dynamic> json) {
  return PaymentRequestRejectBody(
    setlePaymentId: json['setlePaymentId'] as int,
  );
}

Map<String, dynamic> _$PaymentRequestRejectBodyToJson(
        PaymentRequestRejectBody instance) =>
    <String, dynamic>{
      'setlePaymentId': instance.setlePaymentId,
    };
