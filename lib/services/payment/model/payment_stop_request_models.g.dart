// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_stop_request_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentStopBody _$PaymentStopBodyFromJson(Map<String, dynamic> json) {
  return PaymentStopBody(
    setleId: json['setleId'] as int,
  );
}

Map<String, dynamic> _$PaymentStopBodyToJson(PaymentStopBody instance) =>
    <String, dynamic>{
      'setleId': instance.setleId,
    };
