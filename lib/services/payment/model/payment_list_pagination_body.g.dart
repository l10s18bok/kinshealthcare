// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_list_pagination_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentListPaginationBody _$PaymentListPaginationBodyFromJson(
    Map<String, dynamic> json) {
  return PaymentListPaginationBody(
    direction: json['direction'] as bool,
    sortBy: json['sortBy'] as String,
    page: json['page'] as int,
    size: json['size'] as int,
    target: json['target'] as String?,
  );
}

Map<String, dynamic> _$PaymentListPaginationBodyToJson(
        PaymentListPaginationBody instance) =>
    <String, dynamic>{
      'direction': instance.direction,
      'page': instance.page,
      'size': instance.size,
      'sortBy': instance.sortBy,
      'target': instance.target,
    };
