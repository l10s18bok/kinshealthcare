import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'payment_request_models.g.dart';

@JsonSerializable()
class PostPaymentRequestBodyKkakka extends BaseRetrofitModel {
  final int amount;

  final int kkakkaId;

  PostPaymentRequestBodyKkakka({
    required this.amount,
    required this.kkakkaId,
  });

  factory PostPaymentRequestBodyKkakka.fromJson(Map<String, dynamic> json) =>
      _$PostPaymentRequestBodyKkakkaFromJson(json);

  Map<String, dynamic> toJson() => _$PostPaymentRequestBodyKkakkaToJson(this);
}

@JsonSerializable()
class PostPaymentRequestBody extends BaseRetrofitModel {
  final int? setleId;

  PostPaymentRequestBody({
    this.setleId,
  });

  factory PostPaymentRequestBody.fromJson(Map<String, dynamic> json) =>
      _$PostPaymentRequestBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostPaymentRequestBodyToJson(this);
}

@JsonSerializable()
class PostPaymentRequestResponse extends BaseRetrofitModel {
  final String? message;
  final int? setleId;
  final bool? success;

  PostPaymentRequestResponse({
    this.message,
    this.setleId,
    this.success,
  });

  factory PostPaymentRequestResponse.fromJson(Map<String, dynamic> json) =>
      _$PostPaymentRequestResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PostPaymentRequestResponseToJson(this);
}
