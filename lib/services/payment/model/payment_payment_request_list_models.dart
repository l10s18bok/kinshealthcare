import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';

part 'payment_payment_request_list_models.g.dart';

@JsonSerializable()
class PaymentRequestListBody extends GeneralPaginationBody {
  /// USER || STORE
  final String target;

  PaymentRequestListBody({
    this.target = 'USER',
    bool direction = true,
    int page = 0,
    int size = 10,
    String sortBy = 'last',
  }) : super(
          direction: direction,
          page: page,
          size: size,
          sortBy: sortBy,
        );

  factory PaymentRequestListBody.fromJson(Map<String, dynamic> json) =>
      _$PaymentRequestListBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentRequestListBodyToJson(this);
}

@JsonSerializable()
class PaymentRequestListResponse extends BaseRetrofitModel {
  /// 가맹점 주소 1
  final String address1;

  /// 가맹점 주소 2
  final String address2;

  /// 결제요청 금액
  final int amount;

  /// 스토어 ID
  final int storeId;

  /// 가맹점
  final String comName;

  /// 결제요청시간 (원장생성시간)
  final String createDateTime;

  /// 요청한 분 이미지
  final String profileImgFrom;

  /// 쓰실 분 이미지
  final String profileImgUse;

  /// 결제요청 request 횟수 (2인 경우 잔액 못넘김)
  final int requestCount;

  /// 결제요청 상태
  final String requestStatus;

  /// 결제요청 ID
  final int setlePaymentId;

  /// 요청한 분 이름
  final String userNameFrom;

  /// 쓰신 분 이름
  final String userNameUse;

  PaymentRequestListResponse({
    required this.address1,
    required this.address2,
    required this.amount,
    required this.comName,
    required this.createDateTime,
    required this.profileImgFrom,
    required this.profileImgUse,
    required this.requestCount,
    required this.requestStatus,
    required this.setlePaymentId,
    required this.userNameFrom,
    required this.userNameUse,
    required this.storeId,
  });

  factory PaymentRequestListResponse.fromJson(Object? json) =>
      _$PaymentRequestListResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PaymentRequestListResponseToJson(this);
}
