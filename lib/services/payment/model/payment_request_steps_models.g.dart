// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_request_steps_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentStepsUserPaymentWay _$PaymentStepsUserPaymentWayFromJson(
    Map<String, dynamic> json) {
  return PaymentStepsUserPaymentWay(
    approveAmount: json['approveAmount'] as int,
    userPaymentWayId: json['userPaymentWayId'] as int,
  );
}

Map<String, dynamic> _$PaymentStepsUserPaymentWayToJson(
        PaymentStepsUserPaymentWay instance) =>
    <String, dynamic>{
      'approveAmount': instance.approveAmount,
      'userPaymentWayId': instance.userPaymentWayId,
    };

PaymentStepsLocalCurrency _$PaymentStepsLocalCurrencyFromJson(
    Map<String, dynamic> json) {
  return PaymentStepsLocalCurrency(
    approveAmount: json['approveAmount'] as int,
    localCurrencyCode: json['localCurrencyCode'] as String,
  );
}

Map<String, dynamic> _$PaymentStepsLocalCurrencyToJson(
        PaymentStepsLocalCurrency instance) =>
    <String, dynamic>{
      'approveAmount': instance.approveAmount,
      'localCurrencyCode': instance.localCurrencyCode,
    };

PaymentStepsKkakka _$PaymentStepsKkakkaFromJson(Map<String, dynamic> json) {
  return PaymentStepsKkakka(
    approveAmount: json['approveAmount'] as int,
    kkakkaId: json['kkakkaId'] as int,
  );
}

Map<String, dynamic> _$PaymentStepsKkakkaToJson(PaymentStepsKkakka instance) =>
    <String, dynamic>{
      'approveAmount': instance.approveAmount,
      'kkakkaId': instance.kkakkaId,
    };

PaymentStepsApproveMe _$PaymentStepsApproveMeFromJson(
    Map<String, dynamic> json) {
  return PaymentStepsApproveMe(
    approveTotalAmount: json['approveTotalAmount'] as int,
    kkakkas: (json['kkakkas'] as List<dynamic>?)
        ?.map((e) => PaymentStepsKkakka.fromJson(e as Map<String, dynamic>))
        .toList(),
    localCurrencys: (json['localCurrencys'] as List<dynamic>?)
        ?.map((e) =>
            PaymentStepsLocalCurrency.fromJson(e as Map<String, dynamic>))
        .toList(),
    userPaymentWays: (json['userPaymentWays'] as List<dynamic>?)
        ?.map((e) =>
            PaymentStepsUserPaymentWay.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$PaymentStepsApproveMeToJson(
        PaymentStepsApproveMe instance) =>
    <String, dynamic>{
      'approveTotalAmount': instance.approveTotalAmount,
      'kkakkas': instance.kkakkas,
      'localCurrencys': instance.localCurrencys,
      'userPaymentWays': instance.userPaymentWays,
    };

PaymentStepsToUser _$PaymentStepsToUserFromJson(Map<String, dynamic> json) {
  return PaymentStepsToUser(
    amount: json['amount'] as int,
    toUserNo: json['toUserNo'] as int,
  );
}

Map<String, dynamic> _$PaymentStepsToUserToJson(PaymentStepsToUser instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'toUserNo': instance.toUserNo,
    };

PaymentStepsRequestUser _$PaymentStepsRequestUserFromJson(
    Map<String, dynamic> json) {
  return PaymentStepsRequestUser(
    approveTotalAmount: json['approveTotalAmount'] as int,
    toUsers: (json['toUsers'] as List<dynamic>)
        .map((e) => PaymentStepsToUser.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$PaymentStepsRequestUserToJson(
        PaymentStepsRequestUser instance) =>
    <String, dynamic>{
      'approveTotalAmount': instance.approveTotalAmount,
      'toUsers': instance.toUsers,
    };

PaymentRequestSteps2Body _$PaymentRequestSteps2BodyFromJson(
    Map<String, dynamic> json) {
  return PaymentRequestSteps2Body(
    amount: json['amount'] as int,
    approveMe: PaymentStepsApproveMe.fromJson(
        json['approveMe'] as Map<String, dynamic>),
    setlePaymentId: json['setlePaymentId'] as int,
  );
}

Map<String, dynamic> _$PaymentRequestSteps2BodyToJson(
        PaymentRequestSteps2Body instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'approveMe': instance.approveMe,
      'setlePaymentId': instance.setlePaymentId,
    };

PaymentRequestSteps1Body _$PaymentRequestSteps1BodyFromJson(
    Map<String, dynamic> json) {
  return PaymentRequestSteps1Body(
    amount: json['amount'] as int,
    approveMe: PaymentStepsApproveMe.fromJson(
        json['approveMe'] as Map<String, dynamic>),
    requestUser: PaymentStepsRequestUser.fromJson(
        json['requestUser'] as Map<String, dynamic>),
    setlePaymentId: json['setlePaymentId'] as int,
  );
}

Map<String, dynamic> _$PaymentRequestSteps1BodyToJson(
        PaymentRequestSteps1Body instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'approveMe': instance.approveMe,
      'setlePaymentId': instance.setlePaymentId,
      'requestUser': instance.requestUser,
    };

PaymentRequestSteps0Body _$PaymentRequestSteps0BodyFromJson(
    Map<String, dynamic> json) {
  return PaymentRequestSteps0Body(
    amount: json['amount'] as int,
    approveMe: PaymentStepsApproveMe.fromJson(
        json['approveMe'] as Map<String, dynamic>),
    requestUser: PaymentStepsRequestUser.fromJson(
        json['requestUser'] as Map<String, dynamic>),
    setleId: json['setleId'] as int,
  );
}

Map<String, dynamic> _$PaymentRequestSteps0BodyToJson(
        PaymentRequestSteps0Body instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'approveMe': instance.approveMe,
      'setleId': instance.setleId,
      'requestUser': instance.requestUser,
    };

PaymentRequestStepsBody _$PaymentRequestStepsBodyFromJson(
    Map<String, dynamic> json) {
  return PaymentRequestStepsBody(
    amount: json['amount'] as int,
    approveMe: PaymentStepsApproveMe.fromJson(
        json['approveMe'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$PaymentRequestStepsBodyToJson(
        PaymentRequestStepsBody instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'approveMe': instance.approveMe,
    };
