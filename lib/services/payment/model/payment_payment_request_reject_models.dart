import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'payment_payment_request_reject_models.g.dart';

@JsonSerializable()
class PaymentRequestRejectBody extends BaseRetrofitModel {
  final int setlePaymentId;

  PaymentRequestRejectBody({
    required this.setlePaymentId,
  });

  factory PaymentRequestRejectBody.fromJson(Map<String, dynamic> json) =>
      _$PaymentRequestRejectBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentRequestRejectBodyToJson(this);
}
