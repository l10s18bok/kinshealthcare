// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_cancel_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentCancelModel _$PaymentCancelModelFromJson(Map<String, dynamic> json) {
  return PaymentCancelModel(
    message: json['message'] as String?,
    setleId: json['setleId'] as int?,
    success: json['success'] as bool?,
  );
}

Map<String, dynamic> _$PaymentCancelModelToJson(PaymentCancelModel instance) =>
    <String, dynamic>{
      'message': instance.message,
      'setleId': instance.setleId,
      'success': instance.success,
    };
