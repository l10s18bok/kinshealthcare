// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bank_code_service.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _BankCodeService implements BankCodeService {
  _BankCodeService(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://kins-healthcare-dev.kinsapps.com/api/code';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ListItemResponseModelTemp<BankCodeResponse>> getBankCode() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListItemResponseModelTemp<BankCodeResponse>>(
            Options(method: 'GET', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/get/BANK_CODE',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListItemResponseModelTemp<BankCodeResponse>.fromJson(
      _result.data!,
      (json) => BankCodeResponse.fromJson(json),
    );
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
