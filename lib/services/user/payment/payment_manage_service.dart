import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/comm/model/result_form_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/user/payment/model/payment_register_card.dart';
import 'package:retrofit/http.dart';

import 'model/payment_register_account.dart';
import 'model/payment_register_auth_confirm.dart';
import 'model/payment_register_auth_request.dart';

part 'payment_manage_service.g.dart';

@RestApi(baseUrl: 'https://$DEV_HOST/api/user/payment')
abstract class PaymentManageService extends BaseService {
  factory PaymentManageService(Dio dio, {String baseUrl}) = _PaymentManageService;

  @POST('/regist/card')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ResultFormModel> postRegisterCard(
    @Body() PostRegisterCardBody body,
  );

  @POST('/authConfirm/account')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<PaymentRegAutConfirmResponse>> authConfirm(
    @Body() PostRegAuthConfirmBody body,
  );

  @POST('/authRequest/account')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<PaymentRegAuthReqResponse>> authRequest(
    @Body() PostRegAuthRequestBody body,
  );

  @POST('/regist/account')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> registerAccount(
    @Body() PostRegisterAccountBody body,
  );
}
