import 'package:json_annotation/json_annotation.dart';

import 'package:kins_healthcare/services/common/base_model.dart';

part 'payment_register_account.g.dart';

@JsonSerializable()
class PostRegisterAccountBody extends BaseRetrofitModel {
  ///계좌번호
  final String? accountEnc;

  final String? accountTradeIdEnc;

  ///
  final String? bankCode;

  ///
  final String? orderNo;

  ///
  final String? paymentAlias;

  PostRegisterAccountBody({
    this.accountEnc,
    this.accountTradeIdEnc,
    this.bankCode,
    this.orderNo,
    this.paymentAlias,
  });

  factory PostRegisterAccountBody.fromJson(Map<String, dynamic> json) => _$PostRegisterAccountBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostRegisterAccountBodyToJson(this);
}
