// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_register_card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostRegisterCardBody _$PostRegisterCardBodyFromJson(Map<String, dynamic> json) {
  return PostRegisterCardBody(
    cardNoEnc: json['cardNoEnc'] as String?,
    cardPwEnc: json['cardPwEnc'] as String?,
    paymentCorp: json['paymentCorp'] as String?,
    expireMMEnc: json['expireMMEnc'] as String?,
    expireYYEnc: json['expireYYEnc'] as String?,
    identityCut: json['identityCut'] as String?,
    paymentAlias: json['paymentAlias'] as String?,
    privateNoEnc: json['privateNoEnc'] as String?,
    userNameEnc: json['userNameEnc'] as String?,
    userType: json['userType'] as String?,
  );
}

Map<String, dynamic> _$PostRegisterCardBodyToJson(
        PostRegisterCardBody instance) =>
    <String, dynamic>{
      'cardNoEnc': instance.cardNoEnc,
      'cardPwEnc': instance.cardPwEnc,
      'paymentCorp': instance.paymentCorp,
      'expireMMEnc': instance.expireMMEnc,
      'expireYYEnc': instance.expireYYEnc,
      'identityCut': instance.identityCut,
      'paymentAlias': instance.paymentAlias,
      'privateNoEnc': instance.privateNoEnc,
      'userNameEnc': instance.userNameEnc,
      'userType': instance.userType,
    };
