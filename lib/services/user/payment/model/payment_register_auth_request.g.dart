// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_register_auth_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostRegAuthRequestBody _$PostRegAuthRequestBodyFromJson(
    Map<String, dynamic> json) {
  return PostRegAuthRequestBody(
    accountEnc: json['accountEnc'] as String?,
    bankCode: json['bankCode'] as String?,
    paymentAlias: json['paymentAlias'] as String?,
    privateNoEnc: json['privateNoEnc'] as String?,
    userNameEnc: json['userNameEnc'] as String?,
  );
}

Map<String, dynamic> _$PostRegAuthRequestBodyToJson(
        PostRegAuthRequestBody instance) =>
    <String, dynamic>{
      'accountEnc': instance.accountEnc,
      'bankCode': instance.bankCode,
      'paymentAlias': instance.paymentAlias,
      'privateNoEnc': instance.privateNoEnc,
      'userNameEnc': instance.userNameEnc,
    };

PaymentRegAuthReqResponse _$PaymentRegAuthReqResponseFromJson(
    Map<String, dynamic> json) {
  return PaymentRegAuthReqResponse(
    orderNo: json['orderNo'] as String?,
    sendDt: json['sendDt'] as String?,
  );
}

Map<String, dynamic> _$PaymentRegAuthReqResponseToJson(
        PaymentRegAuthReqResponse instance) =>
    <String, dynamic>{
      'orderNo': instance.orderNo,
      'sendDt': instance.sendDt,
    };
