// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bank_code.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BankCodeResponse _$BankCodeResponseFromJson(Map<String, dynamic> json) {
  return BankCodeResponse(
    code: json['code'] as String,
    desc: json['desc'] as String,
  );
}

Map<String, dynamic> _$BankCodeResponseToJson(BankCodeResponse instance) =>
    <String, dynamic>{
      'code': instance.code,
      'desc': instance.desc,
    };
