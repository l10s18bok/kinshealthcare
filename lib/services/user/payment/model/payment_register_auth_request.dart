import 'package:json_annotation/json_annotation.dart';

import 'package:kins_healthcare/services/common/base_model.dart';

part 'payment_register_auth_request.g.dart';

///결제수단 등록 :  계좌인증 요청(ARS 이전)
@JsonSerializable()
class PostRegAuthRequestBody extends BaseRetrofitModel {
  /// 출금계좌
  final String? accountEnc;

  /// 은행코드
  final String? bankCode;

  /// 통장별명
  final String? paymentAlias;

  /// 생년월일 or 사업자번호
  final String? privateNoEnc;

  /// 예금자명
  final String? userNameEnc;

  PostRegAuthRequestBody({
    this.accountEnc,
    this.bankCode,
    this.paymentAlias,
    this.privateNoEnc,
    this.userNameEnc,
  });

  factory PostRegAuthRequestBody.fromJson(Map<String, dynamic> json) => _$PostRegAuthRequestBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostRegAuthRequestBodyToJson(this);
}

@JsonSerializable()
class PaymentRegAuthReqResponse extends BaseRetrofitModel {
  ///
  final String? orderNo;

  ///
  final String? sendDt;

  PaymentRegAuthReqResponse({
    required this.orderNo,
    required this.sendDt,
  });

  factory PaymentRegAuthReqResponse.fromJson(Object? json) => _$PaymentRegAuthReqResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PaymentRegAuthReqResponseToJson(this);
}
