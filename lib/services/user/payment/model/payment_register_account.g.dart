// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_register_account.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostRegisterAccountBody _$PostRegisterAccountBodyFromJson(
    Map<String, dynamic> json) {
  return PostRegisterAccountBody(
    accountEnc: json['accountEnc'] as String?,
    accountTradeIdEnc: json['accountTradeIdEnc'] as String?,
    bankCode: json['bankCode'] as String?,
    orderNo: json['orderNo'] as String?,
    paymentAlias: json['paymentAlias'] as String?,
  );
}

Map<String, dynamic> _$PostRegisterAccountBodyToJson(
        PostRegisterAccountBody instance) =>
    <String, dynamic>{
      'accountEnc': instance.accountEnc,
      'accountTradeIdEnc': instance.accountTradeIdEnc,
      'bankCode': instance.bankCode,
      'orderNo': instance.orderNo,
      'paymentAlias': instance.paymentAlias,
    };
