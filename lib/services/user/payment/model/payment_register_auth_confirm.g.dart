// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_register_auth_confirm.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostRegAuthConfirmBody _$PostRegAuthConfirmBodyFromJson(
    Map<String, dynamic> json) {
  return PostRegAuthConfirmBody(
    bankCode: json['bankCode'] as String?,
    orderNo: json['orderNo'] as String?,
    sendDt: json['sendDt'] as String?,
  );
}

Map<String, dynamic> _$PostRegAuthConfirmBodyToJson(
        PostRegAuthConfirmBody instance) =>
    <String, dynamic>{
      'bankCode': instance.bankCode,
      'orderNo': instance.orderNo,
      'sendDt': instance.sendDt,
    };

PaymentRegAutConfirmResponse _$PaymentRegAutConfirmResponseFromJson(
    Map<String, dynamic> json) {
  return PaymentRegAutConfirmResponse(
    orderNo: json['orderNo'] as String?,
    accountTradeIdEnc: json['accountTradeIdEnc'] as String?,
  );
}

Map<String, dynamic> _$PaymentRegAutConfirmResponseToJson(
        PaymentRegAutConfirmResponse instance) =>
    <String, dynamic>{
      'orderNo': instance.orderNo,
      'accountTradeIdEnc': instance.accountTradeIdEnc,
    };
