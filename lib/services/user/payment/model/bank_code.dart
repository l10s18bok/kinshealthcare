import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'bank_code.g.dart';

@JsonSerializable()
class BankCodeResponse extends BaseRetrofitModel {
  final String code; // bank code
  final String desc; // bank name

  BankCodeResponse({
    required this.code,
    required this.desc,
  });

  factory BankCodeResponse.fromJson(Object? json) =>
      _$BankCodeResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$BankCodeResponseToJson(this);
}