// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_card_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentCardInfoResponse _$PaymentCardInfoResponseFromJson(
    Map<String, dynamic> json) {
  return PaymentCardInfoResponse(
    paymentWayId: json['paymentWayId'] as int,
    paymentTypeDesc: json['paymentTypeDesc'] as String,
    identityHash: json['identityHash'] as String,
    paymentAlias: json['paymentAlias'] as String,
    paymentCorp: json['paymentCorp'] as String,
  );
}

Map<String, dynamic> _$PaymentCardInfoResponseToJson(
        PaymentCardInfoResponse instance) =>
    <String, dynamic>{
      'paymentWayId': instance.paymentWayId,
      'paymentTypeDesc': instance.paymentTypeDesc,
      'identityHash': instance.identityHash,
      'paymentAlias': instance.paymentAlias,
      'paymentCorp': instance.paymentCorp,
    };
