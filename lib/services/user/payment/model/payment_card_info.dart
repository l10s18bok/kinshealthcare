import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'payment_card_info.g.dart';

@JsonSerializable()
class PaymentCardInfoResponse extends BaseRetrofitModel {
  final int paymentWayId; // card id
  final String paymentTypeDesc; // card type
  final String identityHash; //card number
  final String paymentAlias; //
  final String paymentCorp; // card name

  PaymentCardInfoResponse({
    required this.paymentWayId,
    required this.paymentTypeDesc,
    required this.identityHash,
    required this.paymentAlias,
    required this.paymentCorp,
  });

  factory PaymentCardInfoResponse.fromJson(Object? json) =>
      _$PaymentCardInfoResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PaymentCardInfoResponseToJson(this);
}
