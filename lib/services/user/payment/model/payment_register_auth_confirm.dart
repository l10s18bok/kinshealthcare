import 'package:json_annotation/json_annotation.dart';

import 'package:kins_healthcare/services/common/base_model.dart';

part 'payment_register_auth_confirm.g.dart';

///결제수단 등록 :  계좌인증 요청(ARS 이후)
@JsonSerializable()
class PostRegAuthConfirmBody extends BaseRetrofitModel {
  ///
  final String? bankCode;

  ///
  final String? orderNo;

  ///
  final String? sendDt;

  PostRegAuthConfirmBody({
    this.bankCode,
    this.orderNo,
    this.sendDt,
  });

  factory PostRegAuthConfirmBody.fromJson(Map<String, dynamic> json) => _$PostRegAuthConfirmBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostRegAuthConfirmBodyToJson(this);
}

@JsonSerializable()
class PaymentRegAutConfirmResponse extends BaseRetrofitModel {
  ///
  final String? orderNo;

  ///
  final String? accountTradeIdEnc;

  PaymentRegAutConfirmResponse({
    required this.orderNo,
    required this.accountTradeIdEnc,
  });

  factory PaymentRegAutConfirmResponse.fromJson(Object? json) => _$PaymentRegAutConfirmResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PaymentRegAutConfirmResponseToJson(this);
}
