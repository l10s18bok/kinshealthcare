import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'payment_register_card.g.dart';

@JsonSerializable()
class PostRegisterCardBody extends BaseRetrofitModel {
  ///카드번호
  final String? cardNoEnc;

  ///카드 비밀번호
  final String? cardPwEnc;

  ///결제 은행
  final String? paymentCorp;

  ///유효기간 월
  final String? expireMMEnc;

  ///유효기간 년
  final String? expireYYEnc;

  ///(설명없음, 필수아님)
  final String? identityCut;

  ///카드 별명
  final String? paymentAlias;

  ///생년월일 또는 사업자번호
  final String? privateNoEnc;

  ///카드 소유자 명
  final String? userNameEnc;

  ///고객유형 (PERSON:개인, CORPORATION:법인)
  final String? userType;

  PostRegisterCardBody({
    this.cardNoEnc,
    this.cardPwEnc,
    this.paymentCorp,
    this.expireMMEnc,
    this.expireYYEnc,
    this.identityCut,
    this.paymentAlias,
    this.privateNoEnc,
    this.userNameEnc,
    this.userType,
  });

  factory PostRegisterCardBody.fromJson(Map<String, dynamic> json) => _$PostRegisterCardBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostRegisterCardBodyToJson(this);
}
