// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'payment_manage_service.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _PaymentManageService implements PaymentManageService {
  _PaymentManageService(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://kins-healthcare-dev.kinsapps.com/api/user/payment';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ResultFormModel> postRegisterCard(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ResultFormModel>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/regist/card',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ResultFormModel.fromJson(_result.data!);
    return value;
  }

  @override
  Future<SingleItemResponseDataModel<PaymentRegAutConfirmResponse>> authConfirm(
      body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(_setStreamType<
        SingleItemResponseDataModel<PaymentRegAutConfirmResponse>>(Options(
            method: 'POST',
            headers: <String, dynamic>{r'accessToken': null},
            extra: _extra)
        .compose(_dio.options, '/authConfirm/account',
            queryParameters: queryParameters, data: _data)
        .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value =
        SingleItemResponseDataModel<PaymentRegAutConfirmResponse>.fromJson(
      _result.data!,
      (json) => PaymentRegAutConfirmResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<SingleItemResponseDataModel<PaymentRegAuthReqResponse>> authRequest(
      body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<SingleItemResponseDataModel<PaymentRegAuthReqResponse>>(
            Options(
                    method: 'POST',
                    headers: <String, dynamic>{r'accessToken': null},
                    extra: _extra)
                .compose(_dio.options, '/authRequest/account',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value =
        SingleItemResponseDataModel<PaymentRegAuthReqResponse>.fromJson(
      _result.data!,
      (json) => PaymentRegAuthReqResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<GeneralPlainResponse> registerAccount(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<GeneralPlainResponse>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/regist/account',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = GeneralPlainResponse.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
