import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/user/payment/model/bank_code.dart';
import 'package:retrofit/retrofit.dart';

part 'bank_code_service.g.dart';

@RestApi(baseUrl: 'https://$DEV_HOST/api/code')
abstract class BankCodeService extends BaseService {
  factory BankCodeService(Dio dio, {String baseUrl}) = _BankCodeService;

  @GET('/get/BANK_CODE')
  Future<ListItemResponseModelTemp<BankCodeResponse>> getBankCode();
}

