import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/register/model/reg_ok_email_model.dart';
import 'package:kins_healthcare/services/register/model/reg_ok_sms_model.dart';
import 'package:kins_healthcare/services/user/model/device_regist_models.dart';
import 'package:kins_healthcare/services/user/model/email_create_models.dart';
import 'package:kins_healthcare/services/user/model/password_modify_models.dart';
import 'package:kins_healthcare/services/user/model/profile_img_regist_models.dart';
import 'package:kins_healthcare/services/user/model/register_model.dart';
import 'package:kins_healthcare/services/user/model/simple_pw_check_model.dart';
import 'package:kins_healthcare/services/user/model/simple_pw_create.dart';
import 'package:kins_healthcare/services/user/model/simple_pw_modify_models.dart';
import 'package:kins_healthcare/services/user/model/user_info_models.dart';
import 'package:kins_healthcare/services/user/payment/model/payment_card_info.dart';
import 'package:retrofit/http.dart';

import 'model/user_profile_models.dart';

part 'user_service.g.dart';

@RestApi(baseUrl: 'https://$DEV_HOST/api/user')
abstract class UserService extends BaseService {
  factory UserService(Dio dio, {String baseUrl}) = _UserService;

  @POST('/profileImg/regist')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> profileImgRegist(
    @Body() ProfileImgRegistBody body,
  );

  @POST('/register/emailCreate')
  Future<GeneralPlainResponse> emailCreate(
    @Body() EmailCreateBody body,
  );

  @POST('/register')
  Future<RegisterResponse> register(
    @Body() RegisterBody body,
  );

  @POST('/userInfo')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseModel<UserInfoResponse>> userInfo();

  @POST('/getProfileInfo')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<UserProfileResponse>> getProfileInfo(
    @Body() UserProfileBody body,
  );

  @POST('/simplepwCreate')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SimplePwCheckModel> simplePwCreate(
    @Body() SimplePwCreateBody body,
  );

  @POST('/simplepwCheck')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SimplePwCheckModel> simplePwCheck();

  @POST('/simplepwModify')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> simplePwModify(
    @Body() SimplePwModifyBody body,
  );

  @POST('/passwordModify')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> passwordModify(
    @Body() PasswordModifyBody body,
  );

  @POST('/dropUser')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> dropUser();

  @POST('/payment/list')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<PaymentCardInfoResponse>> userPayCardInfo(
    @Body() GeneralPaginationBody body,
  );

  @POST('/device/regist')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> deviceRegist(
    @Body() DeviceRegistBody body,
  );

  @POST('/userModify')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> userModify(
    @Body() UserInfoResponse body,
  );

  @POST('/register/cert/ok/email')
  Future<RegOkEmailResponse> regOkEmail(
    @Body() RegOkEmailBody body,
  );

  @POST('/register/cert/ok/sms')
  Future<RegOkSmsResponse> regOkSms(
    @Body() RegOkSmsBody body,
  );

  @POST('/register/cert/ok/email/child')
  Future<RegOkEmailResponse> regChildOkEmail(
    @Body() RegOkEmailBody body,
  );
}
