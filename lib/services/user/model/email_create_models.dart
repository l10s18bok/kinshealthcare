import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'email_create_models.g.dart';

@JsonSerializable()
class EmailCreateBody extends BaseRetrofitModel {
  final String? lastname;
  final String? firstname;
  final String? id;
  final String? password;
  final String? nation;
  final String? question;
  final String? answer;

  EmailCreateBody({
    @required this.lastname,
    @required this.firstname,
    @required this.id,
    @required this.password,
    @required this.nation,
    @required this.question,
    @required this.answer,
  });

  factory EmailCreateBody.fromJson(Map<String, dynamic> json) =>
      _$EmailCreateBodyFromJson(json);

  Map<String, dynamic> toJson() => _$EmailCreateBodyToJson(this);
}
