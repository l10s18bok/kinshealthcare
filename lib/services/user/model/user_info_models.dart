import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'user_info_models.g.dart';

@JsonSerializable()
class UserInfoResponse extends BaseRetrofitModel {
  final String firstname;
  final String lastname;
  final String? birth;
  final String? email;
  final String phone;
  final String image;

  UserInfoResponse({
    this.firstname = '',
    this.lastname = '',
    required this.birth,
    required this.email,
    required this.phone,
    required this.image,
  });

  factory UserInfoResponse.fromJson(Object? json) =>
      _$UserInfoResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$UserInfoResponseToJson(this);
}
