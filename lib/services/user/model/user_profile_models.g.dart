// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_profile_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserProfileBody _$UserProfileBodyFromJson(Map<String, dynamic> json) {
  return UserProfileBody(
    userRelNo: json['userRelNo'] as int,
  );
}

Map<String, dynamic> _$UserProfileBodyToJson(UserProfileBody instance) =>
    <String, dynamic>{
      'userRelNo': instance.userRelNo,
    };

UserProfileResponse _$UserProfileResponseFromJson(Map<String, dynamic> json) {
  return UserProfileResponse(
    userName: json['userName'] as String?,
    userImage: json['userImage'] as String?,
    userLevel: json['userLevel'] as int?,
    count: json['count'] as int?,
  );
}

Map<String, dynamic> _$UserProfileResponseToJson(
        UserProfileResponse instance) =>
    <String, dynamic>{
      'userName': instance.userName,
      'userImage': instance.userImage,
      'userLevel': instance.userLevel,
      'count': instance.count,
    };
