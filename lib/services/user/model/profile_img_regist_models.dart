import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'profile_img_regist_models.g.dart';

@JsonSerializable()
class ProfileImgRegistBody extends BaseRetrofitModel {
  final String? profileImg;

  ProfileImgRegistBody({
    required this.profileImg,
  });

  factory ProfileImgRegistBody.fromJson(Map<String, dynamic> json) =>
      _$ProfileImgRegistBodyFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileImgRegistBodyToJson(this);
}
