// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterBody _$RegisterBodyFromJson(Map<String, dynamic> json) {
  return RegisterBody(
    id: json['id'] as String?,
    password: json['password'] as String?,
    nation: json['nation'] as String?,
    lastname: json['lastname'] as String?,
    firstname: json['firstname'] as String?,
    birth: json['birth'] as String?,
    gender: json['gender'] as String?,
    phone: json['phone'] as String?,
    mediYn: json['mediYn'] as String?,
  );
}

Map<String, dynamic> _$RegisterBodyToJson(RegisterBody instance) =>
    <String, dynamic>{
      'id': instance.id,
      'password': instance.password,
      'nation': instance.nation,
      'lastname': instance.lastname,
      'firstname': instance.firstname,
      'birth': instance.birth,
      'gender': instance.gender,
      'phone': instance.phone,
      'mediYn': instance.mediYn,
    };

RegisterResponse _$RegisterResponseFromJson(Map<String, dynamic> json) {
  return RegisterResponse(
    resultCode: json['resultCode'] as String?,
    resultMsg: json['resultMsg'] as String?,
    type: json['type'] as String?,
    data: json['data'] as int?,
  );
}

Map<String, dynamic> _$RegisterResponseToJson(RegisterResponse instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
      'type': instance.type,
      'data': instance.data,
    };

RegisterChildBody _$RegisterChildBodyFromJson(Map<String, dynamic> json) {
  return RegisterChildBody(
    id: json['id'] as String?,
    password: json['password'] as String?,
    nation: json['nation'] as String?,
    birth: json['birth'] as String?,
    gender: json['gender'] as String?,
    phone: json['phone'] as String?,
    lastname: json['lastname'] as String?,
    firstname: json['firstname'] as String?,
  );
}

Map<String, dynamic> _$RegisterChildBodyToJson(RegisterChildBody instance) =>
    <String, dynamic>{
      'id': instance.id,
      'password': instance.password,
      'nation': instance.nation,
      'lastname': instance.lastname,
      'firstname': instance.firstname,
      'birth': instance.birth,
      'gender': instance.gender,
      'phone': instance.phone,
    };
