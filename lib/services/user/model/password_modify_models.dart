import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'password_modify_models.g.dart';

@JsonSerializable()
class PasswordModifyBody extends BaseRetrofitModel {
  /// 기존 비밀번호
  final String? pw;

  /// 새 비밀번호
  final String? pwNew;

  /// 새 비밀번호 확인
  final String? pwNewCheck;

  PasswordModifyBody({
    @required this.pw,
    @required this.pwNew,
    @required this.pwNewCheck,
  });

  factory PasswordModifyBody.fromJson(Map<String, dynamic> json) =>
      _$PasswordModifyBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PasswordModifyBodyToJson(this);
}
