import 'package:json_annotation/json_annotation.dart';

part 'user_profile_models.g.dart';

@JsonSerializable()
class UserProfileBody {
  final int userRelNo;

  UserProfileBody({
    required this.userRelNo,
  });

  factory UserProfileBody.fromJson(Map<String, dynamic> json) => _$UserProfileBodyFromJson(json);

  Map<String, dynamic> toJson() => _$UserProfileBodyToJson(this);
}

@JsonSerializable()
class UserProfileResponse {
  final String? userName;
  final String? userImage;
  final int? userLevel;
  final int? count;

  UserProfileResponse({
    this.userName,
    this.userImage,
    this.userLevel,
    this.count,
  });

  factory UserProfileResponse.fromJson(Object? json) => _$UserProfileResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$UserProfileResponseToJson(this);
}
