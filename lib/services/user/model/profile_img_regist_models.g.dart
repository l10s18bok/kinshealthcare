// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_img_regist_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProfileImgRegistBody _$ProfileImgRegistBodyFromJson(Map<String, dynamic> json) {
  return ProfileImgRegistBody(
    profileImg: json['profileImg'] as String?,
  );
}

Map<String, dynamic> _$ProfileImgRegistBodyToJson(
        ProfileImgRegistBody instance) =>
    <String, dynamic>{
      'profileImg': instance.profileImg,
    };
