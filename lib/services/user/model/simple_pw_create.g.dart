// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'simple_pw_create.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SimplePwCreateBody _$SimplePwCreateBodyFromJson(Map<String, dynamic> json) {
  return SimplePwCreateBody(
    simplePwNew: json['simplePwNew'] as String?,
  );
}

Map<String, dynamic> _$SimplePwCreateBodyToJson(SimplePwCreateBody instance) =>
    <String, dynamic>{
      'simplePwNew': instance.simplePwNew,
    };
