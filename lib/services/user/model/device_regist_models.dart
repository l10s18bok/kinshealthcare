import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'device_regist_models.g.dart';

@JsonSerializable()
class DeviceRegistBody extends BaseRetrofitModel {
  final String devUuid;
  final String pushToken;
  final String userDeviceOs;

  DeviceRegistBody({
    required this.devUuid,
    required this.pushToken,
    required this.userDeviceOs,
  });

  factory DeviceRegistBody.fromJson(Map<String, dynamic> json) =>
      _$DeviceRegistBodyFromJson(json);

  Map<String, dynamic> toJson() => _$DeviceRegistBodyToJson(this);
}
