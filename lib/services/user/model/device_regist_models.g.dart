// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_regist_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeviceRegistBody _$DeviceRegistBodyFromJson(Map<String, dynamic> json) {
  return DeviceRegistBody(
    devUuid: json['devUuid'] as String,
    pushToken: json['pushToken'] as String,
    userDeviceOs: json['userDeviceOs'] as String,
  );
}

Map<String, dynamic> _$DeviceRegistBodyToJson(DeviceRegistBody instance) =>
    <String, dynamic>{
      'devUuid': instance.devUuid,
      'pushToken': instance.pushToken,
      'userDeviceOs': instance.userDeviceOs,
    };
