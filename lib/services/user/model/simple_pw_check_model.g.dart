// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'simple_pw_check_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SimplePwCheckModel _$SimplePwCheckModelFromJson(Map<String, dynamic> json) {
  return SimplePwCheckModel(
    resultCode: json['resultCode'] as String,
    resultMsg: json['resultMsg'] as String?,
    data: json['data'] as bool?,
  );
}

Map<String, dynamic> _$SimplePwCheckModelToJson(SimplePwCheckModel instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
      'data': instance.data,
    };
