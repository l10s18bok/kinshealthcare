import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'simple_pw_create.g.dart';

@JsonSerializable()
class SimplePwCreateBody extends BaseRetrofitModel {
  final String? simplePwNew;

  SimplePwCreateBody({
    required this.simplePwNew,
  });

  factory SimplePwCreateBody.fromJson(Map<String, dynamic> json) =>
      _$SimplePwCreateBodyFromJson(json);

  Map<String, dynamic> toJson() => _$SimplePwCreateBodyToJson(this);
}
