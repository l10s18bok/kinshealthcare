// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'password_modify_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PasswordModifyBody _$PasswordModifyBodyFromJson(Map<String, dynamic> json) {
  return PasswordModifyBody(
    pw: json['pw'] as String?,
    pwNew: json['pwNew'] as String?,
    pwNewCheck: json['pwNewCheck'] as String?,
  );
}

Map<String, dynamic> _$PasswordModifyBodyToJson(PasswordModifyBody instance) =>
    <String, dynamic>{
      'pw': instance.pw,
      'pwNew': instance.pwNew,
      'pwNewCheck': instance.pwNewCheck,
    };
