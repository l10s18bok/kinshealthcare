// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'simple_pw_modify_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SimplePwModifyBody _$SimplePwModifyBodyFromJson(Map<String, dynamic> json) {
  return SimplePwModifyBody(
    simplePwNew: json['simplePwNew'] as String?,
  );
}

Map<String, dynamic> _$SimplePwModifyBodyToJson(SimplePwModifyBody instance) =>
    <String, dynamic>{
      'simplePwNew': instance.simplePwNew,
    };
