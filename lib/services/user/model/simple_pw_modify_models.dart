import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'simple_pw_modify_models.g.dart';

@JsonSerializable()
class SimplePwModifyBody extends BaseRetrofitModel {
  final String? simplePwNew;

  SimplePwModifyBody({
    required this.simplePwNew,
  });

  factory SimplePwModifyBody.fromJson(Map<String, dynamic> json) =>
      _$SimplePwModifyBodyFromJson(json);

  Map<String, dynamic> toJson() => _$SimplePwModifyBodyToJson(this);
}
