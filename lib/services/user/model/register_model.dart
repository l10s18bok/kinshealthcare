import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'register_model.g.dart';

/*
 * 작성일 : 2021-02-19
 * 작성자 : JC
 * API 담당 : 올리비아
 */
@JsonSerializable()
class RegisterBody extends BaseRetrofitModel {
  /// 이메일
  final String? id;

  /// 비밀번호
  final String? password;

  /// 국가 (+82)
  final String? nation;

  /// 성
  final String? lastname;

  /// 이름
  final String? firstname;

  /// 생년월일
  final String? birth;

  /// 성별
  final String? gender;

  /// 전화번호
  final String? phone;

  /// 의료인 여부
  final String? mediYn;

  RegisterBody({
    required this.id,
    required this.password,
    required this.nation,
    required this.lastname,
    required this.firstname,
    required this.birth,
    required this.gender,
    required this.phone,
    required this.mediYn,
  });

  factory RegisterBody.fromJson(Map<String, dynamic> json) => _$RegisterBodyFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterBodyToJson(this);
}

/*
 * 작성일 : 2021-05-10
 * 작성자 : Andy
 * API 담당 : 올리비아
 */

@JsonSerializable()
class RegisterResponse extends BaseRetrofitModel {
  final String? resultCode;
  final String? resultMsg;
  final String? type;
  final int? data;

  RegisterResponse({
    this.resultCode,
    this.resultMsg,
    this.type,
    this.data,
  });

  factory RegisterResponse.fromJson(Map<String, dynamic> json) => _$RegisterResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterResponseToJson(this);
}

/*
 * 작성일 : 2021-04-09
 * 작성자 : Andy
 * API 담당 : 올리비아
 */

@JsonSerializable()
class RegisterChildBody extends BaseRetrofitModel {
  /// 이메일
  final String? id;

  /// 비밀번호
  final String? password;

  /// 국가 (+82)
  final String? nation;

  /// 가족명
  final String? lastname; //userFamilyName;

  /// 이름
  final String? firstname; //userName;

  /// 생년월일
  final String? birth;

  /// 성별
  final String? gender;

  /// 전화번호
  final String? phone;
  

  RegisterChildBody(
      {required this.id,
      required this.password,
      required this.nation,
      required this.birth,
      required this.gender,
      required this.phone,
      required this.lastname,
      required this.firstname});

  factory RegisterChildBody.fromJson(Map<String, dynamic> json) => _$RegisterChildBodyFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterChildBodyToJson(this);
}
