import 'package:json_annotation/json_annotation.dart';

part 'simple_pw_check_model.g.dart';

@JsonSerializable()
class SimplePwCheckModel {
  final String resultCode;
  final String? resultMsg;
  final bool? data;

  SimplePwCheckModel({
    required this.resultCode,
    this.resultMsg,
    this.data,
  });

  factory SimplePwCheckModel.fromJson(Object? json) =>
      _$SimplePwCheckModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$SimplePwCheckModelToJson(this);
}
