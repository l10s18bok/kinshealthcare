// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_info_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserInfoResponse _$UserInfoResponseFromJson(Map<String, dynamic> json) {
  return UserInfoResponse(
    firstname: json['firstname'] as String,
    lastname: json['lastname'] as String,
    birth: json['birth'] as String?,
    email: json['email'] as String?,
    phone: json['phone'] as String,
    image: json['image'] as String,
  );
}

Map<String, dynamic> _$UserInfoResponseToJson(UserInfoResponse instance) =>
    <String, dynamic>{
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'birth': instance.birth,
      'email': instance.email,
      'phone': instance.phone,
      'image': instance.image,
    };
