// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_oauth_token_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostTokenResponse _$PostTokenResponseFromJson(Map<String, dynamic> json) {
  return PostTokenResponse(
    accessToken: json['access_token'] as String?,
    tokenType: json['token_type'] as String?,
    refreshToken: json['refresh_token'] as String?,
    expiresIn: json['expires_in'] as int?,
    scope: json['scope'] as String?,
  );
}

Map<String, dynamic> _$PostTokenResponseToJson(PostTokenResponse instance) =>
    <String, dynamic>{
      'access_token': instance.accessToken,
      'token_type': instance.tokenType,
      'refresh_token': instance.refreshToken,
      'expires_in': instance.expiresIn,
      'scope': instance.scope,
    };

BaseTokenBody _$BaseTokenBodyFromJson(Map<String, dynamic> json) {
  return BaseTokenBody(
    grantType: json['grant_type'] as String,
  );
}

Map<String, dynamic> _$BaseTokenBodyToJson(BaseTokenBody instance) =>
    <String, dynamic>{
      'grant_type': instance.grantType,
    };

RefreshTokenBody _$RefreshTokenBodyFromJson(Map<String, dynamic> json) {
  return RefreshTokenBody(
    refreshToken: json['refresh_token'] as String,
    grantType: json['grant_type'] as String,
  );
}

Map<String, dynamic> _$RefreshTokenBodyToJson(RefreshTokenBody instance) =>
    <String, dynamic>{
      'grant_type': instance.grantType,
      'refresh_token': instance.refreshToken,
    };

AccessTokenBody _$AccessTokenBodyFromJson(Map<String, dynamic> json) {
  return AccessTokenBody(
    username: json['username'] as String,
    password: json['password'] as String,
    scope: json['scope'] as String,
    grantType: json['grant_type'] as String,
  );
}

Map<String, dynamic> _$AccessTokenBodyToJson(AccessTokenBody instance) =>
    <String, dynamic>{
      'grant_type': instance.grantType,
      'username': instance.username,
      'password': instance.password,
      'scope': instance.scope,
    };
