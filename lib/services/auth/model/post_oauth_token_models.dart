import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'post_oauth_token_models.g.dart';

@JsonSerializable()
class PostTokenResponse extends BaseRetrofitModel {
  @JsonKey(name: 'access_token')
  final String? accessToken;

  @JsonKey(name: 'token_type')
  final String? tokenType;

  @JsonKey(name: 'refresh_token')
  final String? refreshToken;

  @JsonKey(name: 'expires_in')
  final int? expiresIn;
  final String? scope;

  PostTokenResponse({
    required this.accessToken,
    required this.tokenType,
    required this.refreshToken,
    required this.expiresIn,
    required this.scope,
  });

  factory PostTokenResponse.fromJson(Map<String, dynamic> json) =>
      _$PostTokenResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PostTokenResponseToJson(this);
}

@JsonSerializable()
class BaseTokenBody extends BaseRetrofitModel {
  @JsonKey(name: 'grant_type')
  final String grantType;

  BaseTokenBody({
    required this.grantType,
  });

  factory BaseTokenBody.fromJson(Map<String, dynamic> json) =>
      _$BaseTokenBodyFromJson(json);

  Map<String, dynamic> toJson() => _$BaseTokenBodyToJson(this);
}

@JsonSerializable()
class RefreshTokenBody extends BaseTokenBody {
  @JsonKey(name: 'refresh_token')
  final String refreshToken;

  RefreshTokenBody({
    required this.refreshToken,
    String grantType = 'refresh_token',
  }) : super(grantType: 'refresh_token');

  factory RefreshTokenBody.fromJson(Map<String, dynamic> json) =>
      _$RefreshTokenBodyFromJson(json);

  Map<String, dynamic> toJson() => _$RefreshTokenBodyToJson(this);
}

@JsonSerializable()
class AccessTokenBody extends BaseTokenBody {
  final String username;

  final String password;

  final String scope;

  AccessTokenBody({
    required this.username,
    required this.password,
    required this.scope,
    String grantType = 'password',
  }) : super(grantType: 'password');

  factory AccessTokenBody.fromJson(Map<String, dynamic> json) =>
      _$AccessTokenBodyFromJson(json);

  Map<String, dynamic> toJson() => _$AccessTokenBodyToJson(this);
}
