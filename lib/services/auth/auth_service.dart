import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/auth/model/post_oauth_token_models.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:retrofit/retrofit.dart';

part 'auth_service.g.dart';

@RestApi(baseUrl: 'https://$DEV_HOST/oauth')
abstract class AuthService extends BaseService {
  factory AuthService(Dio dio, {String baseUrl}) = _AuthService;

  @POST('/token')
  @Headers(<String, dynamic>{
    'basicToken': true,
    'content-type': 'application/x-www-form-urlencoded',
  })
  Future<PostTokenResponse> postToken(
    @Body() BaseTokenBody body,
  );
}
