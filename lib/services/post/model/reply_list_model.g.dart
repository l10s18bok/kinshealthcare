// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reply_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReplyListModel _$ReplyListModelFromJson(Map<String, dynamic> json) {
  return ReplyListModel(
    repNo: json['repNo'] as int?,
    userNo: json['userNo'] as int?,
    userName: json['userName'] as String?,
    userImg: json['userImg'] as String?,
    userRel: json['userRel'] as String?,
    content: json['content'] as String?,
    date: json['date'] == null ? null : DateTime.parse(json['date'] as String),
    likeYn: json['likeYn'] as String?,
    likeTotal: json['likeTotal'] as int?,
    likeUserName: json['likeUserName'] as String?,
  );
}

Map<String, dynamic> _$ReplyListModelToJson(ReplyListModel instance) =>
    <String, dynamic>{
      'repNo': instance.repNo,
      'userNo': instance.userNo,
      'userName': instance.userName,
      'userImg': instance.userImg,
      'userRel': instance.userRel,
      'content': instance.content,
      'date': instance.date?.toIso8601String(),
      'likeYn': instance.likeYn,
      'likeTotal': instance.likeTotal,
      'likeUserName': instance.likeUserName,
    };
