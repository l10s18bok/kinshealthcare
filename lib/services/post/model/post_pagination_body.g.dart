// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_pagination_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostPaginationBody _$PostPaginationBodyFromJson(Map<String, dynamic> json) {
  return PostPaginationBody(
    direction: json['direction'] as bool,
    sortBy: json['sortBy'] as String,
    page: json['page'] as int,
    size: json['size'] as int,
    boardNo: json['boardNo'] as int?,
    boardType: json['boardType'] as String?,
  );
}

Map<String, dynamic> _$PostPaginationBodyToJson(PostPaginationBody instance) =>
    <String, dynamic>{
      'direction': instance.direction,
      'page': instance.page,
      'size': instance.size,
      'sortBy': instance.sortBy,
      'boardNo': instance.boardNo,
      'boardType': instance.boardType,
    };
