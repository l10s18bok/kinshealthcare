// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'like_update_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LikeUpdateModel _$LikeUpdateModelFromJson(Map<String, dynamic> json) {
  return LikeUpdateModel(
    userNo: json['userNo'] as int?,
    userName: json['userName'] as String?,
    likeTotal: json['likeTotal'] as int?,
  );
}

Map<String, dynamic> _$LikeUpdateModelToJson(LikeUpdateModel instance) =>
    <String, dynamic>{
      'userNo': instance.userNo,
      'likeTotal': instance.likeTotal,
      'userName': instance.userName,
    };
