// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_register_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostRegisterBody _$PostRegisterBodyFromJson(Map<String, dynamic> json) {
  return PostRegisterBody(
    message: json['message'] as String?,
    photos: (json['photos'] as List<dynamic>?)
        ?.map((e) => Photo.fromJson(e as Object))
        .toList(),
    postType: json['postType'] as String?,
  );
}

Map<String, dynamic> _$PostRegisterBodyToJson(PostRegisterBody instance) =>
    <String, dynamic>{
      'message': instance.message,
      'photos': instance.photos,
      'postType': instance.postType,
    };

Photo _$PhotoFromJson(Map<String, dynamic> json) {
  return Photo(
    photo: json['photo'] as String?,
  );
}

Map<String, dynamic> _$PhotoToJson(Photo instance) => <String, dynamic>{
      'photo': instance.photo,
    };
