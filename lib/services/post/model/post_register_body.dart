import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

/*
 작성일 : 2021-03-15
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : PostRegisterBody,
 설명 : post를 입력하기 위한 Post Body
*/

part 'post_register_body.g.dart';

@JsonSerializable()
class PostRegisterBody extends BaseRetrofitModel {
  String? message;
  List<Photo>? photos;
  String? postType;

  PostRegisterBody({this.message, this.photos, this.postType});

  factory PostRegisterBody.fromJson(Object? json) =>
      _$PostRegisterBodyFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PostRegisterBodyToJson(this);
}

@JsonSerializable()
class Photo extends BaseRetrofitModel {
  String? photo;

  Photo({
    this.photo,
  });

  factory Photo.fromJson(Object? json) =>
      _$PhotoFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PhotoToJson(this);
}
