import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';

part 'post_pagination_body.g.dart';
/*
 작성일 : 2021-03-10
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : ListFilterPaginationBody,
 설명 : 까까 List 를 필터링 하기 위한 요청의 Body
*/

@JsonSerializable()
class PostPaginationBody extends GeneralPaginationBody {
  final int? boardNo;
  final String? boardType;

  PostPaginationBody({
    bool direction = false,
    String sortBy = 'id',
    int page = 0,
    int size = 10,
    required this.boardNo,
    required this.boardType,
  }) : super(
          direction: direction,
          sortBy: sortBy,
          page: page,
          size: size,
        );

  factory PostPaginationBody.fromJson(Map<String, dynamic> json) =>
      _$PostPaginationBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostPaginationBodyToJson(this);

  copyWith({
    bool? direction,
    int? page,
    int? size,
    String? sortBy,
  }) {
    direction ??= this.direction;
    page ??= this.page;
    size ??= this.size;
    sortBy ??= this.sortBy;

    return PostPaginationBody(
      direction: direction,
      page: page,
      size: size,
      sortBy: sortBy,
      boardNo: this.boardNo,
      boardType: this.boardType,
    );
  }
}
