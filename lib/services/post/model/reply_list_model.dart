import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

/*
 작성일 : 2021-04-19
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : PostListModel,
 설명 : 소식 List를 표시하기 위한 Model
*/

part 'reply_list_model.g.dart';

@JsonSerializable()
class ReplyListModel extends BaseRetrofitModel {
  int? repNo;
  int? userNo;
  String? userName;
  String? userImg;
  String? userRel;
  String? content;
  DateTime? date;
  String? likeYn;
  int? likeTotal;
  String? likeUserName;

  ReplyListModel({
    this.repNo,
    this.userNo,
    this.userName,
    this.userImg,
    this.userRel,
    this.content,
    this.date,
    this.likeYn,
    this.likeTotal,
    this.likeUserName,
  });

  get daysTillEndDate {
    print(this.date);
    print(this.date);
    print(this.date);
    print(this.date);
    print(this.date);
    if (this.date == null) return null;
    var inday = DateTime.now().difference(this.date!).inDays;
    var inHorwer = DateTime.now().difference(this.date!).inHours;
    var inMinutes = DateTime.now().difference(this.date!).inMinutes;
    if (inday > 0) {
      return '$inday일';
    } else if (inHorwer > 0) {
      return '$inHorwer시간';
    } else if (inMinutes > 0) {
      return '$inMinutes분';
    } else {
      return '방금';
    }
  }

  factory ReplyListModel.fromJson(Object? json) =>
      _$ReplyListModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$ReplyListModelToJson(this);
}
