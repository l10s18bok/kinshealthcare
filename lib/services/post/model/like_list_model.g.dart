// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'like_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LikeListModel _$LikeListModelFromJson(Map<String, dynamic> json) {
  return LikeListModel(
    userNo: json['userNo'] as int?,
    userName: json['userName'] as String?,
    userRel: json['userRel'] as String?,
    userImg: json['userImg'] as String?,
  );
}

Map<String, dynamic> _$LikeListModelToJson(LikeListModel instance) =>
    <String, dynamic>{
      'userNo': instance.userNo,
      'userName': instance.userName,
      'userRel': instance.userRel,
      'userImg': instance.userImg,
    };
