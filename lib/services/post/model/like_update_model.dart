import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

/*
 작성일 : 2021-04-19
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : PostListModel,
 설명 : 소식 List를 표시하기 위한 Model
*/

part 'like_update_model.g.dart';

@JsonSerializable()
class LikeUpdateModel extends BaseRetrofitModel {
  int? userNo;
  int? likeTotal;
  String? userName;

  LikeUpdateModel({
    this.userNo,
    this.userName,
    this.likeTotal,
  });

  factory LikeUpdateModel.fromJson(Object? json) =>
      _$LikeUpdateModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$LikeUpdateModelToJson(this);
}
