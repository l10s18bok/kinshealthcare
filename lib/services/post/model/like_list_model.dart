import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

/*
 작성일 : 2021-04-19
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : PostListModel,
 설명 : 소식 List를 표시하기 위한 Model
*/

part 'like_list_model.g.dart';

@JsonSerializable()
class LikeListModel extends BaseRetrofitModel {
  int? userNo;
  String? userName;
  String? userRel;
  String? userImg;

  LikeListModel({
    this.userNo,
    this.userName,
    this.userRel,
    this.userImg,
  });

  factory LikeListModel.fromJson(Object? json) =>
      _$LikeListModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$LikeListModelToJson(this);
}
