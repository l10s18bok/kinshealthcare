// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostListModel _$PostListModelFromJson(Map<String, dynamic> json) {
  return PostListModel(
    idNo: json['idNo'] as int?,
    userNo: json['userNo'] as int?,
    contentType: json['contentType'] as String?,
    kkakkaType: json['kkakkaType'] as String?,
    profileImg: json['profileImg'] as String?,
    userName: json['userName'] as String?,
    message: json['message'] as String?,
    regTime: json['regTime'] == null
        ? null
        : DateTime.parse(json['regTime'] as String),
    openInfo: json['openInfo'] as String?,
    likeCount: json['likeCount'] as int?,
    firstLikeName: json['firstLikeName'] as String?,
    firstRepl: json['firstRepl'] as String?,
    firstReplName: json['firstReplName'] as String?,
    firstReplImg: json['firstReplImg'] as String?,
    photos:
        (json['photos'] as List<dynamic>?)?.map((e) => e as String).toList(),
    kkakkaBalance: json['kkakkaBalance'] as int?,
    kkakkaEndDate: json['kkakkaEndDate'] as String?,
    kkakkaLimit: json['kkakkaLimit'] as int?,
    kkakkaPrice: json['kkakkaPrice'] as int?,
    kkakkaStartDate: json['kkakkaStartDate'] as String?,
    kkakkaStatus: json['kkakkaStatus'] as String?,
    likeYn: json['likeYn'] as int?,
    isTapMenu: json['isTapMenu'] as bool?,
  )..isContentOpen = json['isContentOpen'] as bool?;
}

Map<String, dynamic> _$PostListModelToJson(PostListModel instance) =>
    <String, dynamic>{
      'idNo': instance.idNo,
      'userNo': instance.userNo,
      'contentType': instance.contentType,
      'kkakkaType': instance.kkakkaType,
      'profileImg': instance.profileImg,
      'userName': instance.userName,
      'message': instance.message,
      'regTime': instance.regTime?.toIso8601String(),
      'openInfo': instance.openInfo,
      'likeCount': instance.likeCount,
      'likeYn': instance.likeYn,
      'firstLikeName': instance.firstLikeName,
      'firstRepl': instance.firstRepl,
      'firstReplName': instance.firstReplName,
      'firstReplImg': instance.firstReplImg,
      'photos': instance.photos,
      'isContentOpen': instance.isContentOpen,
      'isTapMenu': instance.isTapMenu,
      'kkakkaStartDate': instance.kkakkaStartDate,
      'kkakkaEndDate': instance.kkakkaEndDate,
      'kkakkaStatus': instance.kkakkaStatus,
      'kkakkaPrice': instance.kkakkaPrice,
      'kkakkaLimit': instance.kkakkaLimit,
      'kkakkaBalance': instance.kkakkaBalance,
    };
