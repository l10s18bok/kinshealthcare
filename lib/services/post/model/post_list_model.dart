import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/models/local/like_reply_model.dart';
import 'package:kins_healthcare/models/local/post_model.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

/*
 작성일 : 2021-04-19
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : PostListModel,
 설명 : 소식 List를 표시하기 위한 Model
*/

part 'post_list_model.g.dart';

@JsonSerializable()
class PostListModel extends BaseRetrofitModel {
  int? idNo;
  int? userNo;
  String? contentType;
  String? kkakkaType;
  String? profileImg;
  String? userName;
  String? message;
  DateTime? regTime;
  String? openInfo;
  int? likeCount;
  int? likeYn;
  String? firstLikeName;
  String? firstRepl;
  String? firstReplName;
  String? firstReplImg;
  List<String>? photos;
  bool? isContentOpen;
  bool? isTapMenu;
  //baron 추가 .
  String? kkakkaStartDate;
  String? kkakkaEndDate;
  String? kkakkaStatus;
  int? kkakkaPrice;
  int? kkakkaLimit;
  int? kkakkaBalance;

  PostListModel(
      {this.idNo,
      this.userNo,
      this.contentType,
      this.kkakkaType,
      this.profileImg,
      this.userName,
      this.message,
      this.regTime,
      this.openInfo,
      this.likeCount,
      this.firstLikeName,
      this.firstRepl,
      this.firstReplName,
      this.firstReplImg,
      this.photos,
      this.kkakkaBalance,
      this.kkakkaEndDate,
      this.kkakkaLimit,
      this.kkakkaPrice,
      this.kkakkaStartDate,
      this.kkakkaStatus,
      this.likeYn,
      this.isTapMenu = false});

  factory PostListModel.fromJson(Object? json) =>
      _$PostListModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PostListModelToJson(this);

  get dateString {
    final dateTime = this.regTime;
    if (dateTime == null) return '';

    return DateFormat('yyyy.MM.dd').format(dateTime);
  }

  LikeReplyModel get likeReplyModel {
    return LikeReplyModel(
      likeCount: this.likeCount,
      likeYn: this.likeYn,
      firstLikeName: this.firstLikeName,
      firstRepl: this.firstRepl,
      firstReplName: this.firstReplName,
      firstReplImg: this.firstReplImg,
      message: this.message,
      isContentOpen: this.isContentOpen,
      isTabMenu: this.isTapMenu,
      localModel: PostLocalModel(
        boardNo: this.idNo!,
        boardType: this.contentType == 'KKAKKA' ? 'K' : 'N',
      ),
    );
  }
}
