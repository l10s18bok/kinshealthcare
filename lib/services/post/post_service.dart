import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/post/model/like_update_model.dart';
import 'package:kins_healthcare/services/post/model/reply_list_model.dart';
import 'package:retrofit/http.dart';

import 'model/like_list_model.dart';
import 'model/post_list_model.dart';
import 'model/post_register_body.dart';

/*
 작성일 : 2021-04-14
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : PostService,
 설명 :  PostController 의 데이터를 받아오기 위한 Service
*/

part 'post_service.g.dart';

@RestApi(baseUrl: 'https://$DEV_HOST/api/post')
abstract class PostService extends BaseService {
  factory PostService(Dio dio, {String baseUrl}) = _PostService;

  @POST('/register')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultModel> register(
    @Body() PostRegisterBody body,
  );

  @POST('/list')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<PostListModel>> list(
    @Body() GeneralPaginationBody body,
  );

  @POST('/getLikeList')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<LikeListModel>> getLikeList(
    @Body() GeneralPaginationBody body,
  );

  @POST('/getReplyList')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<ReplyListModel>> getReplyList(
    @Body() GeneralPaginationBody body,
  );

  @POST('/insertReply')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultModel> insertReply(
    @Body() Map<String, dynamic> body,
  );

  @POST('/updateLike')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseModel<LikeUpdateModel>> updateLike(
    @Body() Map<String, dynamic> body,
  );
}
