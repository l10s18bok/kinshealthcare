import 'package:flutter/cupertino.dart';
import 'package:json_annotation/json_annotation.dart';

part 'post_donate_detail_models.g.dart';

/*
 작성일 : 2021-02-23
 작성자 : phil
 화면명 :
 경로 :
 클래스 : PostDonateBody
 설명 : 기부 기관 조회
*/

@JsonSerializable()
class PostDonateDetailBody {
  final int? donationId;
  final String? appType;

  PostDonateDetailBody({
    @required this.donationId,
    @required this.appType,
  });

  factory PostDonateDetailBody.fromJson(Map<String, dynamic> json) =>
      _$PostDonateDetailBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostDonateDetailBodyToJson(this);
}

@JsonSerializable()
class PostDonateDetailResponse {
  final int? donationId;
  String? userName;
  String? donationContents;
  double? sumPrice;
  double? totalPrice;
  String? startDt;
  String? endDt;
  List? photoData;

  PostDonateDetailResponse({
    this.donationId,
    this.userName,
    this.donationContents,
    this.sumPrice,
    this.totalPrice,
    this.startDt,
    this.endDt,
    this.photoData,
  });

  factory PostDonateDetailResponse.fromJson(Object? json) =>
      _$PostDonateDetailResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PostDonateDetailResponseToJson(this);
}
