import 'package:json_annotation/json_annotation.dart';

import 'package:kins_healthcare/services/common/base_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';

part 'post_donate_models.g.dart';

/*
 작성일 : 2021-02-23
 작성자 : phil
 화면명 :
 경로 :
 클래스 : PostDonateBody
 설명 : 기부 기관 조회
*/

@JsonSerializable()
class PostDonateBody extends GeneralPaginationBody {
  final String? appType;

  PostDonateBody({
    this.appType = 'H',
    int? page,
    bool? direction,
    String? sortBy,
    int? size,
  }) : super();

  factory PostDonateBody.fromJson(Map<String, dynamic> json) => _$PostDonateBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostDonateBodyToJson(this);

  copyWith({
    String? appType,
    bool? direction,
    int? page,
    int? size,
    String? sortBy,
  }) {
    appType ??= this.appType;
    direction ??= this.direction;
    page ??= this.page;
    size ??= this.size;
    sortBy ??= this.sortBy;

    return PostDonateBody(appType: appType, page: page, direction: super.direction, size: super.size, sortBy: super.sortBy);
  }
}

@JsonSerializable() //'기부기관 조회' 신규 모델
class NewDonateResponse extends BaseRetrofitModel {
  final int? donationId;
  final String? userName;
  final String? donationTitle;
  final int? percentage;
  final int? totalPrice;
  final String? logoImage;
  final List<String>? photo;

  NewDonateResponse({
    this.donationId,
    this.userName,
    this.donationTitle,
    this.percentage,
    this.totalPrice,
    this.logoImage,
    this.photo,
  });

  factory NewDonateResponse.fromJson(Object? json) => _$NewDonateResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$NewDonateResponseToJson(this);
}

@JsonSerializable() //'기부기관 조회' 예전 모델
class PostDonateResponse extends BaseRetrofitModel {
  final int? donationId;
  final String? userName;
  final String? donationTitle;
  final int? sumPrice;
  final int? totalPrice;

  PostDonateResponse({
    this.donationId,
    this.userName,
    this.donationTitle,
    this.sumPrice,
    this.totalPrice,
  });

  factory PostDonateResponse.fromJson(Object? json) => _$PostDonateResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PostDonateResponseToJson(this);
}
