// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'donate_service.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _DonateService implements DonateService {
  _DonateService(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://kins-healthcare-dev.kinsapps.com/api/donation';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ListResponseModel<PostDonateResponse>> postDonationList(
      PostDonateBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = PostDonateBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<PostDonateResponse>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/list',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<PostDonateResponse>.fromJson(
      _result.data!,
      (json) => PostDonateResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<ListResponseModel<NewDonateResponse>> list(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<NewDonateResponse>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/list',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<NewDonateResponse>.fromJson(
      _result.data!,
      (json) => NewDonateResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<SingleItemResponseDataModel<PostDonateDetailResponse>>
      postDonateDetail(PostDonateDetailBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = PostDonateDetailBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<SingleItemResponseDataModel<PostDonateDetailResponse>>(
            Options(
                    method: 'POST',
                    headers: <String, dynamic>{r'accessToken': null},
                    extra: _extra)
                .compose(_dio.options, '/detail',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value =
        SingleItemResponseDataModel<PostDonateDetailResponse>.fromJson(
      _result.data!,
      (json) => PostDonateDetailResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<PostKkakkaResponse> sendDonation(PostKkakkaDonateSendBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = PostKkakkaDonateSendBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PostKkakkaResponse>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/send',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PostKkakkaResponse.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
