// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_donate_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostDonateBody _$PostDonateBodyFromJson(Map<String, dynamic> json) {
  return PostDonateBody(
    appType: json['appType'] as String?,
    page: json['page'] as int?,
    direction: json['direction'] as bool?,
    sortBy: json['sortBy'] as String?,
    size: json['size'] as int?,
  );
}

Map<String, dynamic> _$PostDonateBodyToJson(PostDonateBody instance) =>
    <String, dynamic>{
      'direction': instance.direction,
      'page': instance.page,
      'size': instance.size,
      'sortBy': instance.sortBy,
      'appType': instance.appType,
    };

NewDonateResponse _$NewDonateResponseFromJson(Map<String, dynamic> json) {
  return NewDonateResponse(
    donationId: json['donationId'] as int?,
    userName: json['userName'] as String?,
    donationTitle: json['donationTitle'] as String?,
    percentage: json['percentage'] as int?,
    totalPrice: json['totalPrice'] as int?,
    logoImage: json['logoImage'] as String?,
    photo: (json['photo'] as List<dynamic>?)?.map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$NewDonateResponseToJson(NewDonateResponse instance) =>
    <String, dynamic>{
      'donationId': instance.donationId,
      'userName': instance.userName,
      'donationTitle': instance.donationTitle,
      'percentage': instance.percentage,
      'totalPrice': instance.totalPrice,
      'logoImage': instance.logoImage,
      'photo': instance.photo,
    };

PostDonateResponse _$PostDonateResponseFromJson(Map<String, dynamic> json) {
  return PostDonateResponse(
    donationId: json['donationId'] as int?,
    userName: json['userName'] as String?,
    donationTitle: json['donationTitle'] as String?,
    sumPrice: json['sumPrice'] as int?,
    totalPrice: json['totalPrice'] as int?,
  );
}

Map<String, dynamic> _$PostDonateResponseToJson(PostDonateResponse instance) =>
    <String, dynamic>{
      'donationId': instance.donationId,
      'userName': instance.userName,
      'donationTitle': instance.donationTitle,
      'sumPrice': instance.sumPrice,
      'totalPrice': instance.totalPrice,
    };
