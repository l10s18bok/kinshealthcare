// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_donate_detail_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostDonateDetailBody _$PostDonateDetailBodyFromJson(Map<String, dynamic> json) {
  return PostDonateDetailBody(
    donationId: json['donationId'] as int?,
    appType: json['appType'] as String?,
  );
}

Map<String, dynamic> _$PostDonateDetailBodyToJson(
        PostDonateDetailBody instance) =>
    <String, dynamic>{
      'donationId': instance.donationId,
      'appType': instance.appType,
    };

PostDonateDetailResponse _$PostDonateDetailResponseFromJson(
    Map<String, dynamic> json) {
  return PostDonateDetailResponse(
    donationId: json['donationId'] as int?,
    userName: json['userName'] as String?,
    donationContents: json['donationContents'] as String?,
    sumPrice: (json['sumPrice'] as num?)?.toDouble(),
    totalPrice: (json['totalPrice'] as num?)?.toDouble(),
    startDt: json['startDt'] as String?,
    endDt: json['endDt'] as String?,
    photoData: json['photoData'] as List<dynamic>?,
  );
}

Map<String, dynamic> _$PostDonateDetailResponseToJson(
        PostDonateDetailResponse instance) =>
    <String, dynamic>{
      'donationId': instance.donationId,
      'userName': instance.userName,
      'donationContents': instance.donationContents,
      'sumPrice': instance.sumPrice,
      'totalPrice': instance.totalPrice,
      'startDt': instance.startDt,
      'endDt': instance.endDt,
      'photoData': instance.photoData,
    };
