import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/donate/post_donate_detail_models.dart';
import 'package:kins_healthcare/services/donate/post_donate_models.dart';
import 'package:kins_healthcare/services/kkakka/model/kkakka_request_models.dart';
import 'package:retrofit/http.dart';

part 'donate_service.g.dart';

/*
 작성일 : 2021-02-23
 작성자 : phil,
 화면명 :
 경로 :
 클래스 : DonateService,
 설명 : 관계조회
*/

@RestApi(baseUrl: 'https://$DEV_HOST/api/donation')
abstract class DonateService extends BaseService {
  factory DonateService(Dio dio, {String baseUrl}) = _DonateService;

  @POST('/list')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<PostDonateResponse>> postDonationList(
    @Body() PostDonateBody,
  );

  @POST('/list')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<NewDonateResponse>> list(
    @Body() GeneralPaginationBody body,
  );

  @POST('/detail')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<PostDonateDetailResponse>> postDonateDetail(
    @Body() PostDonateDetailBody,
  );

  @POST('/send')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<PostKkakkaResponse> sendDonation(
    @Body() PostKkakkaDonateSendBody,
  );
}
