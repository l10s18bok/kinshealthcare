import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/login/model/find_id_model.dart';
import 'package:kins_healthcare/services/login/model/find_password_model.dart';
import 'package:retrofit/http.dart';

part 'login_service.g.dart';

@RestApi(baseUrl: 'https://$DEV_HOST/api/login')
abstract class LoginService extends BaseService {
  factory LoginService(Dio dio, {String baseUrl}) = _LoginService;

  @POST('/findId')
  Future<ListResponseModel<FindIdResponse>> findId(
    @Body() FindIdBody body,
  );

  @POST('/findPassword')
  Future<ListResponseModel<FindPasswordResponse>> findPassword(
    @Body() FindPasswordBody body,
  );
}
