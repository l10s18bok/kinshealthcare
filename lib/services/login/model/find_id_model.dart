import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'find_id_model.g.dart';

@JsonSerializable()
class FindIdResponse extends BaseRetrofitModel {
  final String? email;

  FindIdResponse({
    required this.email,
  });

  factory FindIdResponse.fromJson(Object? json) =>
      _$FindIdResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$FindIdResponseToJson(this);
}

@JsonSerializable()
class FindIdBody extends BaseRetrofitModel {
  /// 이름
  final String? firstname;

  /// 성
  final String? lastname;

  /// +82 한국일경우
  final String? nation;

  /// 전화번호
  final String? phone;

  FindIdBody({
    required this.firstname,
    required this.lastname,
    required this.nation,
    required this.phone,
  });

  factory FindIdBody.fromJson(Map<String, dynamic> json) =>
      _$FindIdBodyFromJson(json);

  Map<String, dynamic> toJson() => _$FindIdBodyToJson(this);
}
