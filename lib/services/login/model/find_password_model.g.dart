// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'find_password_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FindPasswordBody _$FindPasswordBodyFromJson(Map<String, dynamic> json) {
  return FindPasswordBody(
    email: json['email'] as String?,
    nation: json['nation'] as String?,
    phone: json['phone'] as String?,
  );
}

Map<String, dynamic> _$FindPasswordBodyToJson(FindPasswordBody instance) =>
    <String, dynamic>{
      'email': instance.email,
      'nation': instance.nation,
      'phone': instance.phone,
    };

FindPasswordResponse _$FindPasswordResponseFromJson(Map<String, dynamic> json) {
  return FindPasswordResponse(
    password: json['password'] as String?,
  );
}

Map<String, dynamic> _$FindPasswordResponseToJson(
        FindPasswordResponse instance) =>
    <String, dynamic>{
      'password': instance.password,
    };
