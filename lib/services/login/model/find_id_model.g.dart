// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'find_id_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FindIdResponse _$FindIdResponseFromJson(Map<String, dynamic> json) {
  return FindIdResponse(
    email: json['email'] as String?,
  );
}

Map<String, dynamic> _$FindIdResponseToJson(FindIdResponse instance) =>
    <String, dynamic>{
      'email': instance.email,
    };

FindIdBody _$FindIdBodyFromJson(Map<String, dynamic> json) {
  return FindIdBody(
    firstname: json['firstname'] as String?,
    lastname: json['lastname'] as String?,
    nation: json['nation'] as String?,
    phone: json['phone'] as String?,
  );
}

Map<String, dynamic> _$FindIdBodyToJson(FindIdBody instance) =>
    <String, dynamic>{
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'nation': instance.nation,
      'phone': instance.phone,
    };
