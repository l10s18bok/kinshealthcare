import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'find_password_model.g.dart';

@JsonSerializable()
class FindPasswordBody extends BaseRetrofitModel {
  final String? email;
  final String? nation;
  final String? phone;

  FindPasswordBody({
    @required this.email,
    @required this.nation,
    @required this.phone,
  });

  factory FindPasswordBody.fromJson(Map<String, dynamic> json) =>
      _$FindPasswordBodyFromJson(json);

  Map<String, dynamic> toJson() => _$FindPasswordBodyToJson(this);
}

@JsonSerializable()
class FindPasswordResponse extends BaseRetrofitModel {
  final String? password;

  FindPasswordResponse({
    @required this.password,
  });

  factory FindPasswordResponse.fromJson(Object? json) =>
      _$FindPasswordResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$FindPasswordResponseToJson(this);
}
