import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:retrofit/http.dart';

import 'model/banner_list_model.dart';

part 'banner_service.g.dart';

/*
 작성일 : 2021-03-31
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : BannerService,
 설명 : 광고 정보를 받아오기 위한 Service
*/

@RestApi(baseUrl: 'https://$DEV_HOST/api/banner')
abstract class BannerService extends BaseService {
  factory BannerService(Dio dio, {String baseUrl}) = _BannerService;

  @POST('/list')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<BannerListModel>> list(
    @Body() Map<String, dynamic> body,
  );
}
