// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banner_service.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _BannerService implements BannerService {
  _BannerService(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://kins-healthcare-dev.kinsapps.com/api/banner';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ListResponseModel<BannerListModel>> list(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body);
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<BannerListModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/list',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<BannerListModel>.fromJson(
      _result.data!,
      (json) => BannerListModel.fromJson(json),
    );
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
