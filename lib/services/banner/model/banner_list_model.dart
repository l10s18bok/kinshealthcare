import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

/*
 작성일 : 2021-03-31
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : BannerListModel,
 설명 : 광고 List 를 받아오기 위한 Model
*/

part 'banner_list_model.g.dart';

@JsonSerializable()
class BannerListModel extends BaseRetrofitModel {
  int? bannerNo;
  String? bannerUrl;
  String? bannerImage;
  String? bannerDesc;

  BannerListModel({
    required this.bannerNo,
    this.bannerUrl,
    this.bannerImage,
    this.bannerDesc,
  });

  factory BannerListModel.fromJson(Object? json) =>
      _$BannerListModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$BannerListModelToJson(this);
}
