// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banner_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BannerListModel _$BannerListModelFromJson(Map<String, dynamic> json) {
  return BannerListModel(
    bannerNo: json['bannerNo'] as int?,
    bannerUrl: json['bannerUrl'] as String?,
    bannerImage: json['bannerImage'] as String?,
    bannerDesc: json['bannerDesc'] as String?,
  );
}

Map<String, dynamic> _$BannerListModelToJson(BannerListModel instance) =>
    <String, dynamic>{
      'bannerNo': instance.bannerNo,
      'bannerUrl': instance.bannerUrl,
      'bannerImage': instance.bannerImage,
      'bannerDesc': instance.bannerDesc,
    };
