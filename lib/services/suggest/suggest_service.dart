import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:retrofit/http.dart';
import 'package:dio/dio.dart' hide Headers;

import 'model/suggest_detail_model.dart';
import 'model/suggest_user_body.dart';

part 'suggest_service.g.dart';

@RestApi(baseUrl: 'https://$DEV_HOST/api/suggest')
abstract class SuggestService extends BaseService {
  factory SuggestService(Dio dio, {String baseUrl}) = _SuggestService;

  @POST('/user')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultModel> suggestUser(
    @Body() SuggestUserBody body,
  );

  @POST('/Detail')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<SuggestDetailModel>> suggestDetail(
    @Body() Map<String, dynamic> body,
  );
}
