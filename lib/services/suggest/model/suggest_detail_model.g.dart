// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'suggest_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SuggestDetailModel _$SuggestDetailModelFromJson(Map<String, dynamic> json) {
  return SuggestDetailModel(
    suggestNo: json['suggestNo'] as int?,
    userNoFrom: json['userNoFrom'] as int?,
    userNoTarget: json['userNoTarget'] as int?,
    kkakkaStatus: json['kkakkaStatus'] as String?,
    startDate: json['startDate'] == null
        ? null
        : DateTime.parse(json['startDate'] as String),
    endDate: json['endDate'] == null
        ? null
        : DateTime.parse(json['endDate'] as String),
    kkakkaPrice: json['kkakkaPrice'] as int?,
    kkakkaLimit: json['kkakkaLimit'] as int?,
    kkakkaMessage: json['kkakkaMessage'] as String?,
    kkakkaImage: json['kkakkaImage'] as String?,
    userNameOpponent: json['userNameOpponent'] as String?,
    userFamilyNameOpponent: json['userFamilyNameOpponent'] as String?,
    userImage: json['userImage'] as String?,
    userPhone: json['userPhone'] as String?,
    userRelCdMe: json['userRelCdMe'] as String?,
    relCheck: json['relCheck'] as int?,
  )..kkakkaType = json['kkakkaType'] as String?;
}

Map<String, dynamic> _$SuggestDetailModelToJson(SuggestDetailModel instance) =>
    <String, dynamic>{
      'suggestNo': instance.suggestNo,
      'userNoFrom': instance.userNoFrom,
      'userNoTarget': instance.userNoTarget,
      'kkakkaType': instance.kkakkaType,
      'kkakkaStatus': instance.kkakkaStatus,
      'startDate': instance.startDate?.toIso8601String(),
      'endDate': instance.endDate?.toIso8601String(),
      'kkakkaPrice': instance.kkakkaPrice,
      'kkakkaLimit': instance.kkakkaLimit,
      'kkakkaMessage': instance.kkakkaMessage,
      'kkakkaImage': instance.kkakkaImage,
      'userNameOpponent': instance.userNameOpponent,
      'userFamilyNameOpponent': instance.userFamilyNameOpponent,
      'userImage': instance.userImage,
      'userPhone': instance.userPhone,
      'userRelCdMe': instance.userRelCdMe,
      'relCheck': instance.relCheck,
    };
