import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'suggest_user_body.g.dart';

@JsonSerializable()
class SuggestUserBody extends BaseRetrofitModel {
  final int? kkakkaNo; // 까까 번호
  final List<UserNo>? users;

  SuggestUserBody({
    this.kkakkaNo,
    this.users,
  });

  factory SuggestUserBody.fromJson(Object? json) =>
      _$SuggestUserBodyFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$SuggestUserBodyToJson(this);
}

@JsonSerializable()
class UserNo {
  final int? userNoSuggest;
  UserNo({
    this.userNoSuggest,
  });

  factory UserNo.fromJson(Object? json) =>
      _$UserNoFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$UserNoToJson(this);
}
