import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/box_grid_model.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

/*
 작성일 : 2021-03-15
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : DivisionModel,
 설명 : 저장된 결제 나누기 설정 Model
*/

part 'suggest_detail_model.g.dart';

@JsonSerializable()
class SuggestDetailModel extends BaseRetrofitModel {
  int? suggestNo;
  int? userNoFrom;
  int? userNoTarget;
  String? kkakkaType;
  String? kkakkaStatus;
  DateTime? startDate;
  DateTime? endDate;
  int? kkakkaPrice;
  int? kkakkaLimit;
  String? kkakkaMessage;
  String? kkakkaImage;
  String? userNameOpponent;
  String? userFamilyNameOpponent;
  String? userImage;
  String? userPhone;
  String? userRelCdMe;
  int? relCheck;

  SuggestDetailModel({
    this.suggestNo,
    this.userNoFrom,
    this.userNoTarget,
    this.kkakkaStatus,
    this.startDate,
    this.endDate,
    this.kkakkaPrice,
    this.kkakkaLimit,
    this.kkakkaMessage,
    this.kkakkaImage,
    this.userNameOpponent,
    this.userFamilyNameOpponent,
    this.userImage,
    this.userPhone,
    this.userRelCdMe,
    this.relCheck,
  });

  factory SuggestDetailModel.fromJson(Object? json) => _$SuggestDetailModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$SuggestDetailModelToJson(this);

  get startDateString {
    final dateTime = this.startDate;
    if (dateTime == null) return '';

    return DateFormat('yyyy.MM.dd').format(dateTime);
  }

  get endDateString {
    final dateTime = this.endDate;
    if (dateTime == null) return '';

    return DateFormat('yyyy.MM.dd').format(dateTime);
  }

  BoxGridModel getCategory() {
    switch (kkakkaType) {
      case '기념일':
        return BoxGridModel(
          title: '기념일',
          imagePath: 'assets/svgs/ic/ic_anniversary_publish_unselect.svg',
        );
      case '의료비':
        return BoxGridModel(
          title: '의료비',
          imagePath: 'assets/svgs/ic/ic_medical_publish_unselect.svg',
        );
      case '용돈':
        return BoxGridModel(
          title: '용돈',
          imagePath: 'assets/svgs/ic/ic_money_publish_unselect.svg',
        );
      default:
    }
    return BoxGridModel(
      title: '기념일',
      imagePath: 'assets/svgs/ic/ic_anniversary_publish_unselect.svg',
    );
  }
}
