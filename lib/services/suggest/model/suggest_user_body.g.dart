// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'suggest_user_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SuggestUserBody _$SuggestUserBodyFromJson(Map<String, dynamic> json) {
  return SuggestUserBody(
    kkakkaNo: json['kkakkaNo'] as int?,
    users: (json['users'] as List<dynamic>?)
        ?.map((e) => UserNo.fromJson(e as Object))
        .toList(),
  );
}

Map<String, dynamic> _$SuggestUserBodyToJson(SuggestUserBody instance) =>
    <String, dynamic>{
      'kkakkaNo': instance.kkakkaNo,
      'users': instance.users,
    };

UserNo _$UserNoFromJson(Map<String, dynamic> json) {
  return UserNo(
    userNoSuggest: json['userNoSuggest'] as int?,
  );
}

Map<String, dynamic> _$UserNoToJson(UserNo instance) => <String, dynamic>{
      'userNoSuggest': instance.userNoSuggest,
    };
