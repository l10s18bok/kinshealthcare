// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'general_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SingleItemResponseModelData<T> _$SingleItemResponseModelDataFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) {
  return SingleItemResponseModelData<T>(
    values: fromJsonT(json['values']),
  );
}

Map<String, dynamic> _$SingleItemResponseModelDataToJson<T>(
  SingleItemResponseModelData<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'values': toJsonT(instance.values),
    };

SingleItemResponseModel<T> _$SingleItemResponseModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) {
  return SingleItemResponseModel<T>(
    resultCode: json['resultCode'] as String,
    data: SingleItemResponseModelData.fromJson(
        json['data'] as Map<String, dynamic>, (value) => fromJsonT(value)),
  );
}

Map<String, dynamic> _$SingleItemResponseModelToJson<T>(
  SingleItemResponseModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'data': instance.data.toJson(
        (value) => toJsonT(value),
      ),
    };

ListResponseModelData<T> _$ListResponseModelDataFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) {
  return ListResponseModelData<T>(
    values: (json['values'] as List<dynamic>).map(fromJsonT).toList(),
    total: json['total'] as int?,
  );
}

Map<String, dynamic> _$ListResponseModelDataToJson<T>(
  ListResponseModelData<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'total': instance.total,
      'values': instance.values.map(toJsonT).toList(),
    };

ListResponseModel<T> _$ListResponseModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) {
  return ListResponseModel<T>(
    resultCode: json['resultCode'] as String,
    data: ListResponseModelData.fromJson(
        json['data'] as Map<String, dynamic>, (value) => fromJsonT(value)),
  );
}

Map<String, dynamic> _$ListResponseModelToJson<T>(
  ListResponseModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'data': instance.data.toJson(
        (value) => toJsonT(value),
      ),
    };

SingleItemResponseDataModel<T> _$SingleItemResponseDataModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) {
  return SingleItemResponseDataModel<T>(
    resultCode: json['resultCode'] as String,
    data: fromJsonT(json['data']),
  );
}

Map<String, dynamic> _$SingleItemResponseDataModelToJson<T>(
  SingleItemResponseDataModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'data': toJsonT(instance.data),
    };

SingleItemResponseModelTemp<T> _$SingleItemResponseModelTempFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) {
  return SingleItemResponseModelTemp<T>(
    resultCode: json['resultCode'] as String,
    data: fromJsonT(json['data']),
  );
}

Map<String, dynamic> _$SingleItemResponseModelTempToJson<T>(
  SingleItemResponseModelTemp<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'data': toJsonT(instance.data),
    };

ListItemResponseModelTemp<T> _$ListItemResponseModelTempFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) {
  return ListItemResponseModelTemp<T>(
    resultCode: json['resultCode'] as String,
    data: (json['data'] as List<dynamic>).map(fromJsonT).toList(),
  );
}

Map<String, dynamic> _$ListItemResponseModelTempToJson<T>(
  ListItemResponseModelTemp<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'data': instance.data.map(toJsonT).toList(),
    };

GeneralPlainResponse _$GeneralPlainResponseFromJson(Map<String, dynamic> json) {
  return GeneralPlainResponse(
    resultCode: json['resultCode'] as String,
    resultMsg: json['resultMsg'] as String,
  );
}

Map<String, dynamic> _$GeneralPlainResponseToJson(
        GeneralPlainResponse instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
    };
