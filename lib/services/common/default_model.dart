import 'package:json_annotation/json_annotation.dart';

/*
 작성일 : 2021-02-08
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : DefaultModel,
 설명 : data 를 dynamic 형태로 받는 기본적인 형태의 model
*/

part 'default_model.g.dart';

@JsonSerializable()
class DefaultModel {
  final String resultCode;
  final String? resultMsg;

  DefaultModel({
    required this.resultCode,
    this.resultMsg,
  });

  factory DefaultModel.fromJson(Object? json) =>
      _$DefaultModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$DefaultModelToJson(this);
}
