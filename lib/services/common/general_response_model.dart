import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'general_response_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class SingleItemResponseModelData<T> {
  final T values;

  SingleItemResponseModelData({
    required this.values,
  });

  factory SingleItemResponseModelData.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$SingleItemResponseModelDataFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) =>
      _$SingleItemResponseModelDataToJson(this, toJsonT);
}

@JsonSerializable(genericArgumentFactories: true)
class SingleItemResponseModel<T> {
  final String resultCode;
  final SingleItemResponseModelData<T> data;

  SingleItemResponseModel({
    required this.resultCode,
    required this.data,
  });

  factory SingleItemResponseModel.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$SingleItemResponseModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) =>
      _$SingleItemResponseModelToJson(this, toJsonT);
}

@JsonSerializable(genericArgumentFactories: true)
class ListResponseModelData<T> {
  final int? total;
  final List<T> values;

  ListResponseModelData({
    required this.values,
    required this.total,
  });

  factory ListResponseModelData.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$ListResponseModelDataFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) =>
      _$ListResponseModelDataToJson(this, toJsonT);
}

@JsonSerializable(genericArgumentFactories: true)
class ListResponseModel<T> {
  final String resultCode;
  final ListResponseModelData<T> data;

  ListResponseModel({
    required this.resultCode,
    required this.data,
  });

  factory ListResponseModel.fromJson(
          Object json, T Function(Object? json) fromJsonT) =>
      _$ListResponseModelFromJson(json as Map<String, dynamic>, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) =>
      _$ListResponseModelToJson(this, toJsonT);

  get isSuccess {
    return this.resultCode == 'SUCCESS';
  }
}

@JsonSerializable(genericArgumentFactories: true)
class SingleItemResponseDataModel<T> {
  final String resultCode;
  final T data;

  SingleItemResponseDataModel({
    required this.resultCode,
    required this.data,
  });

  factory SingleItemResponseDataModel.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$SingleItemResponseDataModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) =>
      _$SingleItemResponseDataModelToJson(this, toJsonT);
}

/// TODO API 정규화가 끝나기 전에 임시로 사용하는 모델입니다.
/// 계속 강조하지만 아래 모델 또는 SingleItemResponseModel 로
/// 단일화가 돼야합니다.
@JsonSerializable(genericArgumentFactories: true)
class SingleItemResponseModelTemp<T> {
  final String resultCode;
  final T data;

  SingleItemResponseModelTemp({
    required this.resultCode,
    required this.data,
  });

  factory SingleItemResponseModelTemp.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$SingleItemResponseModelTempFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) =>
      _$SingleItemResponseModelTempToJson(this, toJsonT);
}

@JsonSerializable(genericArgumentFactories: true)
class ListItemResponseModelTemp<T> {
  final String resultCode;
  final List<T> data;

  ListItemResponseModelTemp({
    required this.resultCode,
    required this.data,
  });

  factory ListItemResponseModelTemp.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$ListItemResponseModelTempFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) =>
      _$ListItemResponseModelTempToJson(this, toJsonT);
}

@JsonSerializable()
class GeneralPlainResponse extends BaseRetrofitModel {
  final String resultCode;
  final String resultMsg;

  GeneralPlainResponse({
    required this.resultCode,
    required this.resultMsg,
  });

  factory GeneralPlainResponse.fromJson(Map<String, dynamic> json) =>
      _$GeneralPlainResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GeneralPlainResponseToJson(this);
}
