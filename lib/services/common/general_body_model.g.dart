// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'general_body_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GeneralPaginationBody _$GeneralPaginationBodyFromJson(
    Map<String, dynamic> json) {
  return GeneralPaginationBody(
    direction: json['direction'] as bool,
    page: json['page'] as int,
    size: json['size'] as int,
    sortBy: json['sortBy'] as String,
  );
}

Map<String, dynamic> _$GeneralPaginationBodyToJson(
        GeneralPaginationBody instance) =>
    <String, dynamic>{
      'direction': instance.direction,
      'page': instance.page,
      'size': instance.size,
      'sortBy': instance.sortBy,
    };
