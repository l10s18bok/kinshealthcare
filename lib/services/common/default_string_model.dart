import 'package:json_annotation/json_annotation.dart';

/*
 작성일 : 2021-04-19
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : DefaultStringModel,
 설명 : data 를 String 형태로 받는 model
*/

part 'default_string_model.g.dart';

@JsonSerializable()
class DefaultStringModel {
  final String resultCode;
  final String? resultMsg;
  final String? data;

  DefaultStringModel({
    required this.resultCode,
    this.resultMsg,
    this.data,
  });

  factory DefaultStringModel.fromJson(Object? json) =>
      _$DefaultStringModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$DefaultStringModelToJson(this);
}
