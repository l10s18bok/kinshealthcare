// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'default_string_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DefaultStringModel _$DefaultStringModelFromJson(Map<String, dynamic> json) {
  return DefaultStringModel(
    resultCode: json['resultCode'] as String,
    resultMsg: json['resultMsg'] as String?,
    data: json['data'] as String?,
  );
}

Map<String, dynamic> _$DefaultStringModelToJson(DefaultStringModel instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
      'data': instance.data,
    };
