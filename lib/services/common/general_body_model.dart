import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'general_body_model.g.dart';

@JsonSerializable()
class GeneralPaginationBody extends BaseRetrofitModel {
  final bool direction;
  int page;
  final int size;
  final String sortBy;

  GeneralPaginationBody({
    this.direction = true,
    this.page = 0,
    this.size = 10,
    this.sortBy = "last",
  });

  factory GeneralPaginationBody.fromJson(Map<String, dynamic> json) =>
      _$GeneralPaginationBodyFromJson(json);

  Map<String, dynamic> toJson() => _$GeneralPaginationBodyToJson(this);

  copyWith({
    bool? direction,
    int? page,
    int? size,
    String? sortBy,
  }) {
    direction ??= this.direction;
    page ??= this.page;
    size ??= this.size;
    sortBy ??= this.sortBy;

    return GeneralPaginationBody(
      direction: direction,
      page: page,
      size: size,
      sortBy: sortBy,
    );
  }
}
