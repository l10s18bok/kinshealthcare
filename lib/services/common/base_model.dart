import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

part 'base_model.g.dart';

@JsonSerializable()
class BaseRetrofitModel{

  @JsonKey(ignore: true)
  TextUtils textUtils = TextUtils();

  BaseRetrofitModel();

  factory BaseRetrofitModel.fromJson(Map<String, dynamic> json) =>
      _$BaseRetrofitModelFromJson(json);

  Map<String, dynamic> toJson() => _$BaseRetrofitModelToJson(this);
}
