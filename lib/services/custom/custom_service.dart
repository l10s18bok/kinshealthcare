import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';

import 'package:retrofit/http.dart';

import '../common/general_response_model.dart';
import 'model/custom_detail_model.dart';
import 'model/custom_list_model.dart';
import 'model/like_register_model.dart';

part 'custom_service.g.dart';

/*
 작성일 : 2021-03-31
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : BannerService,
 설명 : 광고 정보를 받아오기 위한 Service
*/

@RestApi(baseUrl: 'https://$DEV_HOST/api/custom')
abstract class CustomService extends BaseService {
  factory CustomService(Dio dio, {String baseUrl}) = _CustomService;

  @POST('/item/list')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<CustomListModel>> list(
    @Body() Map<String, dynamic> body,
  );

  @POST('/item/detail')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<CustomDetailModel>> detail(
    @Body() Map<String, dynamic> body,
  );

  @POST('/like/register')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<LikeRegisterModel>> likeRegister(
    @Body() Map<String, dynamic> body,
  );

  @POST('/like/delete')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultModel> likeDelete(
    @Body() Map<String, dynamic> body,
  );
}
