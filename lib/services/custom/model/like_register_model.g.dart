// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'like_register_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LikeRegisterModel _$LikeRegisterModelFromJson(Map<String, dynamic> json) {
  return LikeRegisterModel(
    likeNo: json['likeNo'] as int,
  );
}

Map<String, dynamic> _$LikeRegisterModelToJson(LikeRegisterModel instance) =>
    <String, dynamic>{
      'likeNo': instance.likeNo,
    };
