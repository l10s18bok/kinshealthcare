// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'custom_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomListModel _$CustomListModelFromJson(Map<String, dynamic> json) {
  return CustomListModel(
    itemNo: json['itemNo'] as int?,
    itemTitle: json['itemTitle'] as String?,
    itemContent: json['itemContent'] as String?,
    itemPrice: json['itemPrice'] as String?,
    itemMainImage: json['itemMainImage'] as String?,
    likeNo: json['likeNo'] as int?,
  );
}

Map<String, dynamic> _$CustomListModelToJson(CustomListModel instance) =>
    <String, dynamic>{
      'itemNo': instance.itemNo,
      'itemTitle': instance.itemTitle,
      'itemContent': instance.itemContent,
      'itemPrice': instance.itemPrice,
      'itemMainImage': instance.itemMainImage,
      'likeNo': instance.likeNo,
    };
