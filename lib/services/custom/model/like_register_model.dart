import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

/*
 작성일 : 2021-03-31
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : BannerListModel,
 설명 : 광고 List 를 받아오기 위한 Model
*/

part 'like_register_model.g.dart';

@JsonSerializable()
class LikeRegisterModel extends BaseRetrofitModel {
  int likeNo;

  LikeRegisterModel({
    required this.likeNo,
  });

  factory LikeRegisterModel.fromJson(Object? json) =>
      _$LikeRegisterModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$LikeRegisterModelToJson(this);
}
