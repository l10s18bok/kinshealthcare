import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

/*
 작성일 : 2021-03-31
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : BannerListModel,
 설명 : 광고 List 를 받아오기 위한 Model
*/

part 'custom_list_model.g.dart';

@JsonSerializable()
class CustomListModel extends BaseRetrofitModel {
  int? itemNo;
  String? itemTitle;
  String? itemContent;
  String? itemPrice;
  String? itemMainImage;
  int? likeNo;

  CustomListModel({
    this.itemNo,
    this.itemTitle,
    this.itemContent,
    this.itemPrice,
    this.itemMainImage,
    this.likeNo,
  });

  factory CustomListModel.fromJson(Object? json) =>
      _$CustomListModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$CustomListModelToJson(this);
}
