import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

/*
 작성일 : 2021-03-31
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : BannerListModel,
 설명 : 광고 List 를 받아오기 위한 Model
*/

part 'custom_detail_model.g.dart';

@JsonSerializable()
class CustomDetailModel extends BaseRetrofitModel {
  String? itemTitle;
  String? itemContent;
  String? itemPrice;
  String? itemMainImage;
  List<String>? itemSubImages;
  DateTime? regDate;

  CustomDetailModel({
    this.itemTitle,
    this.itemContent,
    this.itemPrice,
    this.itemMainImage,
    this.itemSubImages,
    this.regDate,
  });

  factory CustomDetailModel.fromJson(Object? json) =>
      _$CustomDetailModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$CustomDetailModelToJson(this);

  get regDateString {
    final dateTime = this.regDate;
    if (dateTime == null) return '';

    return DateFormat('yyyy.MM.dd').format(dateTime);
  }
}
