// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'custom_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomDetailModel _$CustomDetailModelFromJson(Map<String, dynamic> json) {
  return CustomDetailModel(
    itemTitle: json['itemTitle'] as String?,
    itemContent: json['itemContent'] as String?,
    itemPrice: json['itemPrice'] as String?,
    itemMainImage: json['itemMainImage'] as String?,
    itemSubImages: (json['itemSubImages'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
    regDate: json['regDate'] == null
        ? null
        : DateTime.parse(json['regDate'] as String),
  );
}

Map<String, dynamic> _$CustomDetailModelToJson(CustomDetailModel instance) =>
    <String, dynamic>{
      'itemTitle': instance.itemTitle,
      'itemContent': instance.itemContent,
      'itemPrice': instance.itemPrice,
      'itemMainImage': instance.itemMainImage,
      'itemSubImages': instance.itemSubImages,
      'regDate': instance.regDate?.toIso8601String(),
    };
