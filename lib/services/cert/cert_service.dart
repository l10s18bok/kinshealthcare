import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/cert/model/simple_password_certify_model.dart';
import 'package:kins_healthcare/services/cert/model/user_certify_models.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/register/model/reg_ok_sms_model.dart';
import 'package:retrofit/http.dart';

part 'cert_service.g.dart';

@RestApi(baseUrl: 'https://$DEV_HOST/api/cert')
abstract class CertService extends BaseService {
  factory CertService(Dio dio, {String baseUrl}) = _CertService;

  @POST('/userCertify')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> userCertify(
    @Body() UserCertifyBody body,
  );

  @POST('/simplepwCertify')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> simplepwCertify(
    @Body() SimplePwCertifyBody body,
  );

  @POST('/ok/sms/child')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<RegOkSmsResponse> regChildOkSms(
    @Body() RegOkSmsBody body,
  );
}
