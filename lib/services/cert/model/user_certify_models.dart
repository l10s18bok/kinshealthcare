import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'user_certify_models.g.dart';

@JsonSerializable()
class UserCertifyBody extends BaseRetrofitModel {
  final String pw;

  UserCertifyBody({
    required this.pw,
  });

  factory UserCertifyBody.fromJson(Map<String, dynamic> json) =>
      _$UserCertifyBodyFromJson(json);

  Map<String, dynamic> toJson() => _$UserCertifyBodyToJson(this);
}
