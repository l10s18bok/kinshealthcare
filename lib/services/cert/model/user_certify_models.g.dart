// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_certify_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserCertifyBody _$UserCertifyBodyFromJson(Map<String, dynamic> json) {
  return UserCertifyBody(
    pw: json['pw'] as String,
  );
}

Map<String, dynamic> _$UserCertifyBodyToJson(UserCertifyBody instance) =>
    <String, dynamic>{
      'pw': instance.pw,
    };
