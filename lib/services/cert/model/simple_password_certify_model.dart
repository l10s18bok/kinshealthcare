import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'simple_password_certify_model.g.dart';

@JsonSerializable()
class SimplePwCertifyBody extends BaseRetrofitModel {
  final String simplePw;

  SimplePwCertifyBody({
    required this.simplePw,
  });

  factory SimplePwCertifyBody.fromJson(Map<String, dynamic> json) =>
      _$SimplePwCertifyBodyFromJson(json);

  Map<String, dynamic> toJson() => _$SimplePwCertifyBodyToJson(this);
}
