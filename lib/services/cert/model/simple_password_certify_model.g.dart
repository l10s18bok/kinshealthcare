// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'simple_password_certify_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SimplePwCertifyBody _$SimplePwCertifyBodyFromJson(Map<String, dynamic> json) {
  return SimplePwCertifyBody(
    simplePw: json['simplePw'] as String,
  );
}

Map<String, dynamic> _$SimplePwCertifyBodyToJson(
        SimplePwCertifyBody instance) =>
    <String, dynamic>{
      'simplePw': instance.simplePw,
    };
