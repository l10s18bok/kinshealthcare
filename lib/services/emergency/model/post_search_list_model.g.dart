// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_search_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostSearchListBody _$PostSearchListBodyFromJson(Map<String, dynamic> json) {
  return PostSearchListBody(
    hsptlName: json['hsptlName'] as String?,
    page: json['page'] as int,
    direction: json['direction'] as bool,
    sortBy: json['sortBy'] as String,
    size: json['size'] as int,
  );
}

Map<String, dynamic> _$PostSearchListBodyToJson(PostSearchListBody instance) =>
    <String, dynamic>{
      'direction': instance.direction,
      'page': instance.page,
      'size': instance.size,
      'sortBy': instance.sortBy,
      'hsptlName': instance.hsptlName,
    };

PostSearchListResponse _$PostSearchListResponseFromJson(
    Map<String, dynamic> json) {
  return PostSearchListResponse(
    adres1: json['adres1'] as String?,
    adres2: json['adres2'] as String?,
    bizrno: json['bizrno'] as String?,
    emrrmYn: json['emrrmYn'] as String?,
    hsptlArea: json['hsptlArea'] as String?,
    hsptlId: json['hsptlId'] as int?,
    hsptlName: json['hsptlName'] as String?,
    hsptlType: json['hsptlType'] as String?,
    intrcn: json['intrcn'] as String?,
    phone: json['phone'] as String?,
    photo1: json['photo1'] as String?,
    recomendYn: json['recomendYn'] as String?,
    rprsntv: json['rprsntv'] as String?,
    url: json['url'] as String?,
    wkdayE: json['wkdayE'] as String?,
    wkdayS: json['wkdayS'] as String?,
    wkendE: json['wkendE'] as String?,
    wkendS: json['wkendS'] as String?,
  );
}

Map<String, dynamic> _$PostSearchListResponseToJson(
        PostSearchListResponse instance) =>
    <String, dynamic>{
      'adres1': instance.adres1,
      'adres2': instance.adres2,
      'bizrno': instance.bizrno,
      'emrrmYn': instance.emrrmYn,
      'hsptlArea': instance.hsptlArea,
      'hsptlId': instance.hsptlId,
      'hsptlName': instance.hsptlName,
      'hsptlType': instance.hsptlType,
      'intrcn': instance.intrcn,
      'phone': instance.phone,
      'photo1': instance.photo1,
      'recomendYn': instance.recomendYn,
      'rprsntv': instance.rprsntv,
      'url': instance.url,
      'wkdayE': instance.wkdayE,
      'wkdayS': instance.wkdayS,
      'wkendE': instance.wkendE,
      'wkendS': instance.wkendS,
    };
