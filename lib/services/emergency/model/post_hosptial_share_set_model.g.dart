// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_hosptial_share_set_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostHosptialShareSetBody _$PostHosptialShareSetBodyFromJson(
    Map<String, dynamic> json) {
  return PostHosptialShareSetBody(
    hsptlId: json['hsptlId'] as int?,
    users: (json['users'] as List<dynamic>?)
        ?.map((e) => HsptlShareUser.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$PostHosptialShareSetBodyToJson(
        PostHosptialShareSetBody instance) =>
    <String, dynamic>{
      'hsptlId': instance.hsptlId,
      'users': instance.users,
    };

PostOnlySuccessResponse _$PostOnlySuccessResponseFromJson(
    Map<String, dynamic> json) {
  return PostOnlySuccessResponse(
    resultCode: json['resultCode'] as String?,
    resultMsg: json['resultMsg'] as String?,
  );
}

Map<String, dynamic> _$PostOnlySuccessResponseToJson(
        PostOnlySuccessResponse instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
    };

HsptlShareUser _$HsptlShareUserFromJson(Map<String, dynamic> json) {
  return HsptlShareUser(
    percentage: json['percentage'] as int?,
    userRelNo: json['userRelNo'] as int?,
  );
}

Map<String, dynamic> _$HsptlShareUserToJson(HsptlShareUser instance) =>
    <String, dynamic>{
      'percentage': instance.percentage,
      'userRelNo': instance.userRelNo,
    };
