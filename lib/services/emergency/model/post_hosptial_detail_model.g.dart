// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_hosptial_detail_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostOnlyHsptlIdBody _$PostOnlyHsptlIdBodyFromJson(Map<String, dynamic> json) {
  return PostOnlyHsptlIdBody(
    hsptlId: json['hsptlId'] as int?,
  );
}

Map<String, dynamic> _$PostOnlyHsptlIdBodyToJson(
        PostOnlyHsptlIdBody instance) =>
    <String, dynamic>{
      'hsptlId': instance.hsptlId,
    };

PostHosptialDetailResponse _$PostHosptialDetailResponseFromJson(
    Map<String, dynamic> json) {
  return PostHosptialDetailResponse(
    adres1: json['adres1'] as String?,
    adres2: json['adres2'] as String?,
    bizrno: json['bizrno'] as String?,
    emrrmYn: json['emrrmYn'] as String?,
    hsptlArea: json['hsptlArea'] as String?,
    hsptlName: json['hsptlName'] as String?,
    hsptlType: json['hsptlType'] as String?,
    intrcn: json['intrcn'] as String?,
    phone: json['phone'] as String?,
    photo: (json['photo'] as List<dynamic>?)?.map((e) => e as String).toList(),
    recomendYn: json['recomendYn'] as String?,
    rprsntv: json['rprsntv'] as String?,
    url: json['url'] as String?,
    wkdayE: json['wkdayE'] as String?,
    wkdayS: json['wkdayS'] as String?,
    wkendE: json['wkendE'] as String?,
    wkendS: json['wkendS'] as String?,
  );
}

Map<String, dynamic> _$PostHosptialDetailResponseToJson(
        PostHosptialDetailResponse instance) =>
    <String, dynamic>{
      'adres1': instance.adres1,
      'adres2': instance.adres2,
      'bizrno': instance.bizrno,
      'emrrmYn': instance.emrrmYn,
      'hsptlArea': instance.hsptlArea,
      'hsptlName': instance.hsptlName,
      'hsptlType': instance.hsptlType,
      'intrcn': instance.intrcn,
      'phone': instance.phone,
      'photo': instance.photo,
      'recomendYn': instance.recomendYn,
      'rprsntv': instance.rprsntv,
      'url': instance.url,
      'wkdayE': instance.wkdayE,
      'wkdayS': instance.wkdayS,
      'wkendE': instance.wkendE,
      'wkendS': instance.wkendS,
    };
