// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_hosptial_share_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostHosptialShareListBody _$PostHosptialShareListBodyFromJson(
    Map<String, dynamic> json) {
  return PostHosptialShareListBody(
    hsptlId: json['hsptlId'] as int?,
  );
}

Map<String, dynamic> _$PostHosptialShareListBodyToJson(
        PostHosptialShareListBody instance) =>
    <String, dynamic>{
      'hsptlId': instance.hsptlId,
    };

PostHosptialShareListResponse _$PostHosptialShareListResponseFromJson(
    Map<String, dynamic> json) {
  return PostHosptialShareListResponse(
    userRelNo: json['userRelNo'] as int?,
    percentage: json['percentage'] as int?,
    userName: json['userName'] as String?,
    userPhone: json['userPhone'] as String?,
    photo: json['photo'] as String?,
    userRelCdMeValue: json['userRelCdMeValue'] as String?,
  );
}

Map<String, dynamic> _$PostHosptialShareListResponseToJson(
        PostHosptialShareListResponse instance) =>
    <String, dynamic>{
      'userRelNo': instance.userRelNo,
      'percentage': instance.percentage,
      'userName': instance.userName,
      'userPhone': instance.userPhone,
      'photo': instance.photo,
      'userRelCdMeValue': instance.userRelCdMeValue,
    };
