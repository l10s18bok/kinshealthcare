import 'package:json_annotation/json_annotation.dart';

part 'post_hosptial_detail_model.g.dart';

/*
 작성일 : 2021-02-24
 작성자 : andy
 화면명 :
 경로 :
 클래스 : EmergencyService
 설명 :   병원상세
*/

@JsonSerializable()
class PostOnlyHsptlIdBody {
  final int? hsptlId;

  PostOnlyHsptlIdBody({
    this.hsptlId,
  });

  factory PostOnlyHsptlIdBody.fromJson(Map<String, dynamic> json) => _$PostOnlyHsptlIdBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostOnlyHsptlIdBodyToJson(this);
}

@JsonSerializable()
class PostHosptialDetailResponse {
  final String? adres1;
  final String? adres2;
  final String? bizrno;
  final String? emrrmYn;
  final String? hsptlArea;
  final String? hsptlName;
  final String? hsptlType;
  final String? intrcn;
  final String? phone;
  final List<String>? photo;
  final String? recomendYn;
  final String? rprsntv;
  final String? url;
  final String? wkdayE;
  final String? wkdayS;
  final String? wkendE;
  final String? wkendS;

  PostHosptialDetailResponse({
    this.adres1,
    this.adres2,
    this.bizrno,
    this.emrrmYn,
    this.hsptlArea,
    this.hsptlName,
    this.hsptlType,
    this.intrcn,
    this.phone,
    this.photo,
    this.recomendYn,
    this.rprsntv,
    this.url,
    this.wkdayE,
    this.wkdayS,
    this.wkendE,
    this.wkendS,
  });

  factory PostHosptialDetailResponse.fromJson(Object? json) => _$PostHosptialDetailResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PostHosptialDetailResponseToJson(this);
}
