import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';

part 'post_search_list_model.g.dart';

/*
 작성일 : 2021-02-24
 작성자 : andy
 화면명 :
 경로 :
 클래스 : EmergencyService
 설명 :   병원검색리스트
*/

@JsonSerializable()
class PostSearchListBody extends GeneralPaginationBody {
  String? hsptlName;

  PostSearchListBody({
    this.hsptlName,
    int page = 0,
    bool direction = true,
    String sortBy = 'last',
    int size = 10,
  }) : super(
          direction: direction,
          sortBy: sortBy,
          page: page,
          size: size,
        );

  factory PostSearchListBody.fromJson(Map<String, dynamic> json) => _$PostSearchListBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostSearchListBodyToJson(this);

  copyWith({
    String? hsptName,
    bool? direction,
    int? page,
    int? size,
    String? sortBy,
  }) {
    hsptlName ??= this.hsptlName;
    direction ??= this.direction;
    page ??= this.page;
    size ??= this.size;
    sortBy ??= this.sortBy;

    return PostSearchListBody(
      hsptlName: hsptlName,
      page: page,
      direction: direction,
      size: size,
      sortBy: sortBy,
    );
  }
}

@JsonSerializable()
class PostSearchListResponse extends BaseRetrofitModel {
  final String? adres1;
  final String? adres2;
  final String? bizrno;
  final String? emrrmYn;
  final String? hsptlArea;
  final int? hsptlId;
  final String? hsptlName;
  final String? hsptlType;
  final String? intrcn;
  final String? phone;
  final String? photo1;
  final String? recomendYn;
  final String? rprsntv;
  final String? url;
  final String? wkdayE;
  final String? wkdayS;
  final String? wkendE;
  final String? wkendS;

  PostSearchListResponse({
    this.adres1,
    this.adres2,
    this.bizrno,
    this.emrrmYn,
    this.hsptlArea,
    this.hsptlId,
    this.hsptlName,
    this.hsptlType,
    this.intrcn,
    this.phone,
    this.photo1,
    this.recomendYn,
    this.rprsntv,
    this.url,
    this.wkdayE,
    this.wkdayS,
    this.wkendE,
    this.wkendS,
  });

  factory PostSearchListResponse.fromJson(Object? json) => _$PostSearchListResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PostSearchListResponseToJson(this);
}
