import 'package:json_annotation/json_annotation.dart';

import 'package:kins_healthcare/services/common/base_model.dart';

part 'post_hosptial_share_set_model.g.dart';
/*
 작성일 : 2021-03-08
 작성자 : andy
 화면명 :
 경로 :
 클래스 : EmergencyShareSetting
 설명 :   병원 나누기 설정
*/

@JsonSerializable()
class PostHosptialShareSetBody extends BaseRetrofitModel {
  final int? hsptlId;
  final List<HsptlShareUser>? users;

  PostHosptialShareSetBody({this.hsptlId, this.users});

  factory PostHosptialShareSetBody.fromJson(Map<String, dynamic> json) => _$PostHosptialShareSetBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostHosptialShareSetBodyToJson(this);
}

@JsonSerializable()
class PostOnlySuccessResponse extends BaseRetrofitModel {
  //final String data;
  final String? resultCode;
  final String? resultMsg;

  PostOnlySuccessResponse({this.resultCode, this.resultMsg});

  factory PostOnlySuccessResponse.fromJson(Map<String, dynamic> json) => _$PostOnlySuccessResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PostOnlySuccessResponseToJson(this);
}

@JsonSerializable()
class HsptlShareUser extends BaseRetrofitModel {
  final int? percentage;
  final int? userRelNo;

  HsptlShareUser({this.percentage, this.userRelNo});

  factory HsptlShareUser.fromJson(Map<String, dynamic> json) => _$HsptlShareUserFromJson(json);

  Map<String, dynamic> toJson() => _$HsptlShareUserToJson(this);
}
