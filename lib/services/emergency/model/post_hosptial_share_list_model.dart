import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'post_hosptial_share_list_model.g.dart';
/*
 작성일 : 2021-03-08
 작성자 : andy
 화면명 :
 경로 :
 클래스 : EmergencyShare
 설명 :   병원 나누기
*/

@JsonSerializable()
class PostHosptialShareListBody {
  final int? hsptlId;

  PostHosptialShareListBody({this.hsptlId});

  factory PostHosptialShareListBody.fromJson(Map<String, dynamic> json) => _$PostHosptialShareListBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostHosptialShareListBodyToJson(this);
}

@JsonSerializable()
class PostHosptialShareListResponse extends BaseRetrofitModel {
  final int? userRelNo;
  final int? percentage;
  final String? userName;
  final String? userPhone;
  final String? photo;
  final String? userRelCdMeValue;

  PostHosptialShareListResponse({this.userRelNo, this.percentage, this.userName, this.userPhone, this.photo, this.userRelCdMeValue});

  factory PostHosptialShareListResponse.fromJson(Object? json) => _$PostHosptialShareListResponseFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PostHosptialShareListResponseToJson(this);
}
