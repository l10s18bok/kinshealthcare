import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/emergency/model/post_hosptial_detail_model.dart';
import 'package:kins_healthcare/services/emergency/model/post_hosptial_share_list_model.dart';
import 'package:kins_healthcare/services/emergency/model/post_search_list_model.dart';
import 'package:retrofit/http.dart';

import 'model/post_hosptial_share_set_model.dart';

part 'emergency_service.g.dart';

/*
 작성일 : 2021-02-24
 작성자 : andy,
 화면명 :
 경로 :
 클래스 : Emergency Service,
 설명 : 병원조회
*/

@RestApi(baseUrl: 'https://$DEV_HOST/api/hsptl')
abstract class EmergencyService extends BaseService {
  factory EmergencyService(Dio dio, {String baseUrl}) = _EmergencyService;

  @POST('/list/request') // 등록된 병원 목록
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<PostSearchListResponse>> postRequestList(
    @Body() GeneralPaginationBody body,
  );

  @POST('/list') // 병원검색(병원찾기)
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<PostSearchListResponse>> postFindList(
    @Body() GeneralPaginationBody body,
  );

  @POST('/detail') // 병원상세
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<PostHosptialDetailResponse>> postDetail(
    @Body() PostOnlyHsptlIdBody body,
  );

  @POST('/userRelPer/detail') // 병원비 나누기 목록 리스트 조회
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<PostHosptialShareListResponse>> postShareDetail(
    @Body() PostHostptialShareListBody,
  );

  @POST('/userRelPer/regist') // 병원비 나누기 목록 리스트 저장
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<PostOnlySuccessResponse> postShareRegist(
    @Body() PostHostptialShareSetBody,
  );

  @POST('/userRelPer/update') // 병원비 나누기 목록 리스트 수정
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<PostOnlySuccessResponse> postShareUpdate(
    @Body() PostHostptialShareSetBody,
  );

  @POST('/userRelPer/delete') // 등록된 병원 목록 삭제
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<PostOnlySuccessResponse> postShareDelete(
    @Body() PostOnlyHsptlIdBody,
  );
}
