// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'emergency_service.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _EmergencyService implements EmergencyService {
  _EmergencyService(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://kins-healthcare-dev.kinsapps.com/api/hsptl';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ListResponseModel<PostSearchListResponse>> postRequestList(
      body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<PostSearchListResponse>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/list/request',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<PostSearchListResponse>.fromJson(
      _result.data!,
      (json) => PostSearchListResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<ListResponseModel<PostSearchListResponse>> postFindList(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<PostSearchListResponse>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/list',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<PostSearchListResponse>.fromJson(
      _result.data!,
      (json) => PostSearchListResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<SingleItemResponseDataModel<PostHosptialDetailResponse>> postDetail(
      body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<SingleItemResponseDataModel<PostHosptialDetailResponse>>(
            Options(
                    method: 'POST',
                    headers: <String, dynamic>{r'accessToken': null},
                    extra: _extra)
                .compose(_dio.options, '/detail',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value =
        SingleItemResponseDataModel<PostHosptialDetailResponse>.fromJson(
      _result.data!,
      (json) => PostHosptialDetailResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<ListResponseModel<PostHosptialShareListResponse>> postShareDetail(
      PostHostptialShareListBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = PostHostptialShareListBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<PostHosptialShareListResponse>>(
            Options(
                    method: 'POST',
                    headers: <String, dynamic>{r'accessToken': null},
                    extra: _extra)
                .compose(_dio.options, '/userRelPer/detail',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<PostHosptialShareListResponse>.fromJson(
      _result.data!,
      (json) => PostHosptialShareListResponse.fromJson(json),
    );
    return value;
  }

  @override
  Future<PostOnlySuccessResponse> postShareRegist(
      PostHostptialShareSetBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = PostHostptialShareSetBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PostOnlySuccessResponse>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/userRelPer/regist',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PostOnlySuccessResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<PostOnlySuccessResponse> postShareUpdate(
      PostHostptialShareSetBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = PostHostptialShareSetBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PostOnlySuccessResponse>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/userRelPer/update',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PostOnlySuccessResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<PostOnlySuccessResponse> postShareDelete(PostOnlyHsptlIdBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = PostOnlyHsptlIdBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PostOnlySuccessResponse>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/userRelPer/delete',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PostOnlySuccessResponse.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
