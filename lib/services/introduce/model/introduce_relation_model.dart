import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

/*
 작성일 : 2021-04-20
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : IntroduceRelationModel,
 설명 : 소개해주기 디테일 Model
*/

part 'introduce_relation_model.g.dart';

@JsonSerializable()
class IntroduceRelationModel extends BaseRetrofitModel {
  int? introduceReqId;
  int? userNoTarget;
  String? userRelCdMeTarget;
  String? userNameTarget;
  String? userImgTarget;
  int? userNoTo;
  String? userRelCdMeTo;
  String? userNameTo;
  String? userImgTo;

  IntroduceRelationModel({
    this.userNoTarget,
    this.userRelCdMeTarget,
    this.userNameTarget,
    this.userImgTarget,
    this.userNoTo,
    this.userRelCdMeTo,
    this.userNameTo,
    this.userImgTo,
  });

  factory IntroduceRelationModel.fromJson(Object? json) =>
      _$IntroduceRelationModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$IntroduceRelationModelToJson(this);
}
