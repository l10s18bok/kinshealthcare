// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'introduce_relation_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IntroduceRelationModel _$IntroduceRelationModelFromJson(
    Map<String, dynamic> json) {
  return IntroduceRelationModel(
    userNoTarget: json['userNoTarget'] as int?,
    userRelCdMeTarget: json['userRelCdMeTarget'] as String?,
    userNameTarget: json['userNameTarget'] as String?,
    userImgTarget: json['userImgTarget'] as String?,
    userNoTo: json['userNoTo'] as int?,
    userRelCdMeTo: json['userRelCdMeTo'] as String?,
    userNameTo: json['userNameTo'] as String?,
    userImgTo: json['userImgTo'] as String?,
  )..introduceReqId = json['introduceReqId'] as int?;
}

Map<String, dynamic> _$IntroduceRelationModelToJson(
        IntroduceRelationModel instance) =>
    <String, dynamic>{
      'introduceReqId': instance.introduceReqId,
      'userNoTarget': instance.userNoTarget,
      'userRelCdMeTarget': instance.userRelCdMeTarget,
      'userNameTarget': instance.userNameTarget,
      'userImgTarget': instance.userImgTarget,
      'userNoTo': instance.userNoTo,
      'userRelCdMeTo': instance.userRelCdMeTo,
      'userNameTo': instance.userNameTo,
      'userImgTo': instance.userImgTo,
    };
