// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'introduce_requester_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IntroduceRequesterModel _$IntroduceRequesterModelFromJson(
    Map<String, dynamic> json) {
  return IntroduceRequesterModel(
    userNoFrom: json['userNoFrom'] as int?,
    userImage: json['userImage'] as String?,
  );
}

Map<String, dynamic> _$IntroduceRequesterModelToJson(
        IntroduceRequesterModel instance) =>
    <String, dynamic>{
      'userNoFrom': instance.userNoFrom,
      'userImage': instance.userImage,
    };
