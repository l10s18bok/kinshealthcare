import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/common/default_string_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:retrofit/http.dart';

import 'model/introduce_relation_model.dart';
import 'model/introduce_requester_model.dart';

part 'introduce_service.g.dart';

/*
 작성일 : 2021-04-19
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : IntroduceService,
 설명 : 소개하기 Controller에 model을 제공하는 Service
*/

@RestApi(baseUrl: 'https://$DEV_HOST/api/introduce')
abstract class IntroduceService extends BaseService {
  factory IntroduceService(Dio dio, {String baseUrl}) = _IntroduceService;

  @POST('/request')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultModel> request(
    @Body() Map<String, dynamic> body,
  );

  @POST('/relation')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<IntroduceRelationModel>> relation(
    @Body() Map<String, dynamic> body,
  );

  @POST('/message')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultModel> message(
    @Body() Map<String, dynamic> body,
  );

  @POST('/approve')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultModel> approve(
    @Body() Map<String, dynamic> body,
  );

  @POST('/phone')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultStringModel> phone(
    @Body() Map<String, dynamic> body,
  );

  @POST('/requester')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<IntroduceRequesterModel>> requester(
    @Body() Map<String, dynamic> body,
  );
}
