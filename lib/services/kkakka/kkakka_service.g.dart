// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'kkakka_service.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _KkakkaService implements KkakkaService {
  _KkakkaService(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://kins-healthcare-dev.kinsapps.com/api/kkakka';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ListResponseModel<SentKkaListResponseModel>> sentList(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<SentKkaListResponseModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/list',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<SentKkaListResponseModel>.fromJson(
      _result.data!,
      (json) => SentKkaListResponseModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<ListResponseModel<PermiseMeResponseModel>> listPromiseMe(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<PermiseMeResponseModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/listPromiseMe',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<PermiseMeResponseModel>.fromJson(
      _result.data!,
      (json) => PermiseMeResponseModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<ListResponseModel<KkakkaModel>> listRequest() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<KkakkaModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/list/request',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<KkakkaModel>.fromJson(
      _result.data!,
      (json) => KkakkaModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<PostKkakkaResponse> postRequest(PostKkakkaRequestRequestBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = PostKkakkaRequestRequestBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PostKkakkaResponse>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/request',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PostKkakkaResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<SingleItemResponseDataModel<KkakkaModel>> postSendKkakkaSuggest(
      PostKkakkaIssuanceSendBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = PostKkakkaIssuanceSendBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<SingleItemResponseDataModel<KkakkaModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/send',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = SingleItemResponseDataModel<KkakkaModel>.fromJson(
      _result.data!,
      (json) => KkakkaModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<ListResponseModel<KkakkaFilterResponseModel>> kkakkaListFilter(
      body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<KkakkaFilterResponseModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/listFilter',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<KkakkaFilterResponseModel>.fromJson(
      _result.data!,
      (json) => KkakkaFilterResponseModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<ListResponseModel<ListSendOpponentModel>> listSendOpponent(
      PostListSendOpponentBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = PostListSendOpponentBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<ListSendOpponentModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/list/send/opponent',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<ListSendOpponentModel>.fromJson(
      _result.data!,
      (json) => ListSendOpponentModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<SingleItemResponseDataModel<SumKkakkaModel>> sumKkakka() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<SingleItemResponseDataModel<SumKkakkaModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/sumKkakka',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = SingleItemResponseDataModel<SumKkakkaModel>.fromJson(
      _result.data!,
      (json) => SumKkakkaModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<SingleItemResponseDataModel<KkakkaModel>> kkakkaDetail(
      kkakkaDetailBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = kkakkaDetailBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<SingleItemResponseDataModel<KkakkaModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/kkakkaDetail',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = SingleItemResponseDataModel<KkakkaModel>.fromJson(
      _result.data!,
      (json) => KkakkaModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<DefaultModel> statusUpdate(statusUpdateBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = statusUpdateBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<DefaultModel>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/statusUpdate',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = DefaultModel.fromJson(_result.data!);
    return value;
  }

  @override
  Future<SingleItemResponseDataModel<GetRequestModel>> getRequest(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body);
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<SingleItemResponseDataModel<GetRequestModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/getRequest',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = SingleItemResponseDataModel<GetRequestModel>.fromJson(
      _result.data!,
      (json) => GetRequestModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<ListResponseModel<ListRequestModel>> requestList(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.removeWhere((k, v) => v == null);
    final _data = <String, dynamic>{};
    _data.addAll(body?.toJson() ?? <String, dynamic>{});
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<ListRequestModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/list/request',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<ListRequestModel>.fromJson(
      _result.data!,
      (json) => ListRequestModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<GetReqKkakkaModel> getReqKkakka(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body);
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<GetReqKkakkaModel>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/getReqKkakka',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = GetReqKkakkaModel.fromJson(_result.data!);
    return value;
  }

  @override
  Future<dynamic> signature(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch(_setStreamType<dynamic>(Options(
            method: 'POST',
            headers: <String, dynamic>{r'accessToken': null},
            extra: _extra)
        .compose(_dio.options, '/signature',
            queryParameters: queryParameters, data: _data)
        .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = _result.data!;
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
