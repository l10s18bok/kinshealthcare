import 'package:dio/dio.dart' hide Headers;
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/models/kkakka_model.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/kkakka/model/kkakka_request_list_models.dart';
import 'package:kins_healthcare/services/kkakka/model/kkakka_request_models.dart';
import 'package:kins_healthcare/services/kkakka/model/sent_kkakka_list_response_model.dart';
import 'package:retrofit/http.dart';

import 'model/get_req_kkakka_model.dart';
import 'model/get_request_models.dart';
import 'model/list_send_opponent.dart';
import 'model/signature_models.dart';
import 'model/sum_kkakka_model.dart';
import 'model/kkakka_filter_response_model.dart';
import 'model/permise_me_response_model.dart';

part 'kkakka_service.g.dart';

@RestApi(baseUrl: 'https://$DEV_HOST/api/kkakka')
abstract class KkakkaService extends BaseService {
  factory KkakkaService(Dio dio, {String baseUrl}) = _KkakkaService;

  @POST('/list')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  // 원래 API ---  ​/api​/kkakka​/list (까까 받은 목록)
  // 현재 API ---    ---            (까까 발급 목록) merge되는데로 수정 예정.
  Future<ListResponseModel<SentKkaListResponseModel>> sentList(
    @Body() GeneralPaginationBody body,
  );

  @POST('/listPromiseMe')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<PermiseMeResponseModel>> listPromiseMe(
    @Body() GeneralPaginationBody body,
  );

  @POST('/list/request')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<KkakkaModel>> listRequest();

  @POST('/request')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<PostKkakkaResponse> postRequest(
    @Body() PostKkakkaRequestRequestBody,
  );

  // @POST('/send')
  // @Headers(<String, dynamic>{
  //   'accessToken': true,
  // })
  // Future<PostKkakkaResponse> postSend(
  //   @Body() PostKkakkaIssuanceSendBody,
  // );

  @POST('/send')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<KkakkaModel>> postSendKkakkaSuggest(
    @Body() PostKkakkaIssuanceSendBody,
  );

  @POST('/listFilter')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<KkakkaFilterResponseModel>> kkakkaListFilter(
    @Body() GeneralPaginationBody body,
  );

  @POST('/list/send/opponent')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<ListSendOpponentModel>> listSendOpponent(
    @Body() PostListSendOpponentBody,
  );

  @POST('/sumKkakka')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<SumKkakkaModel>> sumKkakka();

  @POST('/kkakkaDetail')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<KkakkaModel>> kkakkaDetail(
    @Body() kkakkaDetailBody,
  );

  @POST('/statusUpdate')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<DefaultModel> statusUpdate(
    @Body() statusUpdateBody,
  );

  @POST('/getRequest')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseDataModel<GetRequestModel>> getRequest(
    @Body() Map<String, dynamic> body,
  );

  @POST('/list/request')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<ListRequestModel>> requestList(
    @Body() GeneralPaginationBody? body,
  );

  @POST('/getReqKkakka')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GetReqKkakkaModel> getReqKkakka(
    @Body() Map<String, dynamic> body,
  );
  @POST('/signature')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future signature(@Body() SignatureBody body);
}
