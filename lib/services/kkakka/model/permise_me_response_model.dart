import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'permise_me_response_model.g.dart';

@JsonSerializable()
class PermiseMeResponseModel extends BaseRetrofitModel {
  DateTime? endDate;
  DateTime? startDate;
  String? firstRepl;
  String? firstReplImg;
  String? firstReplUserName;
  String? firstLikeUserName;
  String? kkakkaImage;
  String? kkakkaMessage;
  String? kkakkaStatus;
  String? kkakkaSignature;
  String? kkakkaType;
  String? profileImgOpponent;
  String? userNameOpponent;
  String? userPhoneOpponent;
  String? userRelCdOpponent;
  String? userStatusOpponent;
  String? userRelCdMe;
  int? kkakkaBalance;
  int? kkakkaId;
  int? kkakkaLimit;
  int? kkakkaPrice;
  int? likeCount;
  int? likeYn;
  int? userNoOpponent;
  // 기능 구현하기 위해 추가 된 변수.
  bool? isContentOpen;
  PermiseMeResponseModel(
      {this.endDate,
      this.firstRepl,
      this.firstReplImg,
      this.firstReplUserName,
      this.kkakkaBalance,
      this.kkakkaId,
      this.kkakkaImage,
      this.kkakkaLimit,
      this.kkakkaMessage,
      this.kkakkaPrice,
      this.kkakkaSignature,
      this.kkakkaStatus,
      this.kkakkaType,
      this.likeCount,
      this.likeYn,
      this.profileImgOpponent,
      this.startDate,
      this.userNameOpponent,
      this.userNoOpponent,
      this.userPhoneOpponent,
      this.userRelCdOpponent,
      this.userStatusOpponent,
      this.firstLikeUserName,
      this.userRelCdMe,
      this.isContentOpen});
  factory PermiseMeResponseModel.fromJson(Object? json) =>
      _$PermiseMeResponseModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$PermiseMeResponseModelToJson(this);
}
