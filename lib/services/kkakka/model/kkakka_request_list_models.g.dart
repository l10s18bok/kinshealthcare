// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'kkakka_request_list_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListRequestModel _$ListRequestModelFromJson(Map<String, dynamic> json) {
  return ListRequestModel(
    endDate: json['endDate'] as String?,
    kkakkaImage: json['kkakkaImage'] as String?,
    kkakkaMessage: json['kkakkaMessage'] as String?,
    kkakkaPrice: json['kkakkaPrice'] as int?,
    kkakkaReqId: json['kkakkaReqId'] as int?,
    kkakkaReqStatus: json['kkakkaReqStatus'] as String?,
    kkakkaType: json['kkakkaType'] as String?,
    profileImgOpponent: json['profileImgOpponent'] as String?,
    reqDate: json['reqDate'] as String?,
    startDate: json['startDate'] as String?,
    userNameOpponent: json['userNameOpponent'] as String?,
    userNoOpponent: json['userNoOpponent'] as int?,
    userPhoneOpponent: json['userPhoneOpponent'] as String?,
    userStatusOpponent: json['userStatusOpponent'] as String?,
  );
}

Map<String, dynamic> _$ListRequestModelToJson(ListRequestModel instance) =>
    <String, dynamic>{
      'endDate': instance.endDate,
      'kkakkaImage': instance.kkakkaImage,
      'kkakkaMessage': instance.kkakkaMessage,
      'kkakkaPrice': instance.kkakkaPrice,
      'kkakkaReqId': instance.kkakkaReqId,
      'kkakkaReqStatus': instance.kkakkaReqStatus,
      'kkakkaType': instance.kkakkaType,
      'profileImgOpponent': instance.profileImgOpponent,
      'reqDate': instance.reqDate,
      'startDate': instance.startDate,
      'userNameOpponent': instance.userNameOpponent,
      'userNoOpponent': instance.userNoOpponent,
      'userPhoneOpponent': instance.userPhoneOpponent,
      'userStatusOpponent': instance.userStatusOpponent,
    };
