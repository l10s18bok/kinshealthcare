// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_kkakka_list_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostApiKkakkaListBody _$PostApiKkakkaListBodyFromJson(
    Map<String, dynamic> json) {
  return PostApiKkakkaListBody(
    direction: json['direction'] as bool,
    page: json['page'] as int,
    size: json['size'] as int,
    sortBy: json['sortBy'] as String,
  );
}

Map<String, dynamic> _$PostApiKkakkaListBodyToJson(
        PostApiKkakkaListBody instance) =>
    <String, dynamic>{
      'direction': instance.direction,
      'page': instance.page,
      'size': instance.size,
      'sortBy': instance.sortBy,
    };
