// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sent_kkakka_list_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SentKkaListResponseModel _$SentKkaListResponseModelFromJson(
    Map<String, dynamic> json) {
  return SentKkaListResponseModel(
    endDate: json['endDate'] == null
        ? null
        : DateTime.parse(json['endDate'] as String),
    startDate: json['startDate'] == null
        ? null
        : DateTime.parse(json['startDate'] as String),
    firstLikeUserName: json['firstLikeUserName'] as String?,
    firstRepl: json['firstRepl'] as String?,
    firstReplImg: json['firstReplImg'] as String?,
    firstReplUserName: json['firstReplUserName'] as String?,
    kkakkaBalance: json['kkakkaBalance'] as int?,
    kkakkaId: json['kkakkaId'] as int?,
    kkakkaImage: json['kkakkaImage'] as String?,
    kkakkaLimit: json['kkakkaLimit'] as int?,
    kkakkaMessage: json['kkakkaMessage'] as String?,
    kkakkaPrice: json['kkakkaPrice'] as int?,
    kkakkaSignature: json['kkakkaSignature'] as String?,
    kkakkaStatus: json['kkakkaStatus'] as String?,
    kkakkaType: json['kkakkaType'] as String?,
    likeCount: json['likeCount'] as int?,
    likeYn: json['likeYn'] as int?,
    userImage: json['userImage'] as String?,
    userName: json['userName'] as String?,
    isContentOpen: json['isContentOpen'] as bool?,
    userNameOpponent: json['userNameOpponent'] as String?,
    userNoOpponent: json['userNoOpponent'] as int?,
    userPhoneOpponent: json['userPhoneOpponent'] as String?,
    userRelCdMe: json['userRelCdMe'] as String?,
    userRelCdOpponent: json['userRelCdOpponent'] as String?,
    userStatusOpponent: json['userStatusOpponent'] as String?,
  );
}

Map<String, dynamic> _$SentKkaListResponseModelToJson(
        SentKkaListResponseModel instance) =>
    <String, dynamic>{
      'endDate': instance.endDate?.toIso8601String(),
      'startDate': instance.startDate?.toIso8601String(),
      'firstLikeUserName': instance.firstLikeUserName,
      'firstRepl': instance.firstRepl,
      'firstReplImg': instance.firstReplImg,
      'firstReplUserName': instance.firstReplUserName,
      'kkakkaImage': instance.kkakkaImage,
      'kkakkaMessage': instance.kkakkaMessage,
      'kkakkaSignature': instance.kkakkaSignature,
      'kkakkaStatus': instance.kkakkaStatus,
      'kkakkaType': instance.kkakkaType,
      'userImage': instance.userImage,
      'userName': instance.userName,
      'kkakkaBalance': instance.kkakkaBalance,
      'kkakkaId': instance.kkakkaId,
      'kkakkaLimit': instance.kkakkaLimit,
      'kkakkaPrice': instance.kkakkaPrice,
      'likeCount': instance.likeCount,
      'likeYn': instance.likeYn,
      'userNoOpponent': instance.userNoOpponent,
      'userRelCdMe': instance.userRelCdMe,
      'userRelCdOpponent': instance.userRelCdOpponent,
      'userStatusOpponent': instance.userStatusOpponent,
      'userNameOpponent': instance.userNameOpponent,
      'userPhoneOpponent': instance.userPhoneOpponent,
      'isContentOpen': instance.isContentOpen,
    };
