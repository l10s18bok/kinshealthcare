// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_send_opponent.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListSendOpponentModel _$ListSendOpponentModelFromJson(
    Map<String, dynamic> json) {
  return ListSendOpponentModel(
    userNoRel: json['userNoRel'] as int?,
    userNameRel: json['userNameRel'] as String?,
    userImgRel: json['userImgRel'] as String?,
  );
}

Map<String, dynamic> _$ListSendOpponentModelToJson(
        ListSendOpponentModel instance) =>
    <String, dynamic>{
      'userNoRel': instance.userNoRel,
      'userNameRel': instance.userNameRel,
      'userImgRel': instance.userImgRel,
    };
