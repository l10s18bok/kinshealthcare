// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'kkakka_filter_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

KkakkaFilterResponseModel _$KkakkaFilterResponseModelFromJson(
    Map<String, dynamic> json) {
  return KkakkaFilterResponseModel(
    endDate: json['endDate'] == null
        ? null
        : DateTime.parse(json['endDate'] as String),
    kkakkaBalance: json['kkakkaBalance'] as int?,
    kkakkaId: json['kkakkaId'] as int?,
    kkakkaImage: json['kkakkaImage'] as String?,
    kkakkaLimit: json['kkakkaLimit'] as int?,
    kkakkaMessage: json['kkakkaMessage'] as String?,
    kkakkaPrice: json['kkakkaPrice'] as int?,
    kkakkaSignature: json['kkakkaSignature'] as String?,
    kkakkaStatus: json['kkakkaStatus'] as String?,
    kkakkaType: json['kkakkaType'] as String?,
    profileImgOpponent: json['profileImgOpponent'] as String?,
    startDate: json['startDate'] == null
        ? null
        : DateTime.parse(json['startDate'] as String),
    userNameOpponent: json['userNameOpponent'] as String?,
    userNoOpponent: json['userNoOpponent'] as int?,
    userPhoneOpponent: json['userPhoneOpponent'] as String?,
    userRelCdMe: json['userRelCdMe'] as String?,
    userRelCdOpponent: json['userRelCdOpponent'] as String?,
    userStatusOpponent: json['userStatusOpponent'] as String?,
    firstLikeUserName: json['firstLikeUserName'] as String?,
    firstRepl: json['firstRepl'] as String?,
    firstReplImg: json['firstReplImg'] as String?,
    firstReplUserName: json['firstReplUserName'] as String?,
    likeCount: json['likeCount'] as int?,
    likeYn: json['likeYn'] as int?,
    isContentOpen: json['isContentOpen'] as bool?,
  );
}

Map<String, dynamic> _$KkakkaFilterResponseModelToJson(
        KkakkaFilterResponseModel instance) =>
    <String, dynamic>{
      'endDate': instance.endDate?.toIso8601String(),
      'startDate': instance.startDate?.toIso8601String(),
      'kkakkaImage': instance.kkakkaImage,
      'kkakkaMessage': instance.kkakkaMessage,
      'kkakkaSignature': instance.kkakkaSignature,
      'kkakkaStatus': instance.kkakkaStatus,
      'kkakkaType': instance.kkakkaType,
      'profileImgOpponent': instance.profileImgOpponent,
      'userNameOpponent': instance.userNameOpponent,
      'userPhoneOpponent': instance.userPhoneOpponent,
      'userRelCdMe': instance.userRelCdMe,
      'userRelCdOpponent': instance.userRelCdOpponent,
      'userStatusOpponent': instance.userStatusOpponent,
      'kkakkaBalance': instance.kkakkaBalance,
      'kkakkaId': instance.kkakkaId,
      'kkakkaLimit': instance.kkakkaLimit,
      'kkakkaPrice': instance.kkakkaPrice,
      'userNoOpponent': instance.userNoOpponent,
      'likeYn': instance.likeYn,
      'likeCount': instance.likeCount,
      'firstLikeUserName': instance.firstLikeUserName,
      'firstRepl': instance.firstRepl,
      'firstReplUserName': instance.firstReplUserName,
      'firstReplImg': instance.firstReplImg,
      'isContentOpen': instance.isContentOpen,
    };
