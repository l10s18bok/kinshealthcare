// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signature_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignatureBody _$SignatureBodyFromJson(Map<String, dynamic> json) {
  return SignatureBody(
    kkakkaId: json['kkakkaId'] as int,
    signature: json['signature'] as String,
  );
}

Map<String, dynamic> _$SignatureBodyToJson(SignatureBody instance) =>
    <String, dynamic>{
      'kkakkaId': instance.kkakkaId,
      'signature': instance.signature,
    };
