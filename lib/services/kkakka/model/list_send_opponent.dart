import 'package:json_annotation/json_annotation.dart';

part 'list_send_opponent.g.dart';

@JsonSerializable()
class ListSendOpponentModel {
  final int? userNoRel;
  final String? userNameRel;
  final String? userImgRel;

  ListSendOpponentModel({
    this.userNoRel,
    this.userNameRel,
    this.userImgRel,
  });

  factory ListSendOpponentModel.fromJson(Object? json) =>
      _$ListSendOpponentModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$ListSendOpponentModelToJson(this);
}
