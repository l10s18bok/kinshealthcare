import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'api_kkakka_list_models.g.dart';

@JsonSerializable()
class PostApiKkakkaListBody extends BaseRetrofitModel {
  final bool direction;
  final int page;
  final int size;
  final String sortBy;

  PostApiKkakkaListBody({
    this.direction = true,
    this.page = 0,
    this.size = 20,
    this.sortBy = 'id',
  });

  factory PostApiKkakkaListBody.fromJson(Map<String, dynamic> json) =>
      _$PostApiKkakkaListBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostApiKkakkaListBodyToJson(this);
}
