import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';
import 'package:kins_healthcare/services/relation/model/post_relation_search_models.dart';

part 'get_request_models.g.dart';

/*
 작성일 : 2021-04-05
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : GetRequestModel,
 설명 : 타임라인에서 까까 발행하기를 선택한 경우 표시하기 위한 Data
*/

@JsonSerializable()
class GetRequestModel extends BaseRetrofitModel {
  RequestModel? request;
  RelationModel? relation;

  GetRequestModel({
    this.request,
    this.relation,
  });

  factory GetRequestModel.fromJson(Object? json) =>
      _$GetRequestModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$GetRequestModelToJson(this);
}

@JsonSerializable()
class RequestModel extends BaseRetrofitModel {
  int? userRelNo;
  String? kkakkaType;
  String? kkakkaReqStatus;
  DateTime? startDate;
  DateTime? endDate;
  int? kkakkaPrice;
  String? kkakkaImage;

  RequestModel({
    this.userRelNo,
    this.kkakkaType,
    this.kkakkaReqStatus,
    this.startDate,
    this.endDate,
    this.kkakkaPrice,
    this.kkakkaImage,
  });

  factory RequestModel.fromJson(Object? json) =>
      _$RequestModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$RequestModelToJson(this);
}

@JsonSerializable()
class RelationModel extends BaseRetrofitModel {
  int? userNoRel;
  String? userRelCdMeValue;
  String? userRelCdOpponentValue;
  String? userNameOpponent;
  String? userPhoneOpponent;
  String? userStatusOpponent;
  String? profileImgOpponent;

  RelationModel({
    this.userNoRel,
    this.userRelCdMeValue,
    this.userRelCdOpponentValue,
    this.userNameOpponent,
    this.userPhoneOpponent,
    this.userStatusOpponent,
    this.profileImgOpponent,
  });

  factory RelationModel.fromJson(Object? json) =>
      _$RelationModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$RelationModelToJson(this);

  SearchRelResponse getSearchRelResponse() {
    return SearchRelResponse(
      userNameOpponent: this.userNameOpponent!,
      userNoRel: this.userNoRel!,
      userPhoneOpponent: this.userPhoneOpponent!,
      userRelCdMeValue: this.userRelCdMeValue!,
      userRelCdOpponentValue: this.userRelCdOpponentValue!,
      profileImgOpponent: this.profileImgOpponent!,
      userStatusOpponent: this.userStatusOpponent!,
    );
  }
}
