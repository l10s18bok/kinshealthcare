import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'kkakka_request_models.g.dart';

@JsonSerializable()
class PostKkakkaIssuanceSendBody {
  final String? kkakkaImage; // 이미지
  final String? kkakkaMessage; //메세지
  final int? kkakkaLimit; //1회 한도 금액
  final int? kkakkaPrice; //금액z
  final String? kkakkaType; //까까 유형(code)
  final String? endDate; //종료일
  final String? startDate; //시작일
  final int? userNoRel; //상대 유저No
  final int? userPaymentWayId1; //결제수단 1 ID
  final int? userPaymentWayId2; //결제수단 2 ID

  PostKkakkaIssuanceSendBody({
    this.kkakkaImage,
    this.kkakkaMessage,
    this.kkakkaLimit,
    this.kkakkaPrice,
    this.kkakkaType,
    this.endDate,
    this.startDate,
    this.userNoRel,
    this.userPaymentWayId1,
    this.userPaymentWayId2,
  });

  factory PostKkakkaIssuanceSendBody.fromJson(Map<String, dynamic> json) => _$PostKkakkaIssuanceSendBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostKkakkaIssuanceSendBodyToJson(this);
}

@JsonSerializable()
class PostKkakkaResponse extends BaseRetrofitModel {
  //final String data;
  final String? resultCode;
  final String? resultMsg;

  PostKkakkaResponse({this.resultCode, this.resultMsg});
  factory PostKkakkaResponse.fromJson(Map<String, dynamic> json) => _$PostKkakkaResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PostKkakkaResponseToJson(this);
}

@JsonSerializable()
class PostKkakkaRequestRequestBody {
  final String? kkakkaImage; // 이미지
  final String? kkakkaMessage;
  final int? kkakkaPrice;
  final String? kkakkaType;
  final String? endDate;
  final String? startDate;
  final int? userNoRel;

  PostKkakkaRequestRequestBody({
    this.kkakkaImage,
    this.kkakkaMessage,
    this.kkakkaPrice,
    this.kkakkaType,
    this.endDate,
    this.startDate,
    this.userNoRel,
  });

  factory PostKkakkaRequestRequestBody.fromJson(Map<String, dynamic> json) => _$PostKkakkaRequestRequestBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostKkakkaRequestRequestBodyToJson(this);
}

@JsonSerializable()
class PostKkakkaDonateSendBody {
  final int? donationId; // 기부대상 ID
  final String? kkakkaImage; // 이미지
  final String? kkakkaMessage; //메세지
  final int? kkakkaPrice; //금액
  final String? endDate; //종료일
  final String? startDate; //시작일
  final int? userPaymentWayId1; //결제수단 1 ID
  final int? userPaymentWayId2; //결제수단 2 ID

  PostKkakkaDonateSendBody({
    this.kkakkaImage,
    this.kkakkaMessage,
    this.kkakkaPrice,
    this.endDate,
    this.startDate,
    this.donationId,
    this.userPaymentWayId1,
    this.userPaymentWayId2,
  });

  factory PostKkakkaDonateSendBody.fromJson(Map<String, dynamic> json) => _$PostKkakkaDonateSendBodyFromJson(json);

  Map<String, dynamic> toJson() => _$PostKkakkaDonateSendBodyToJson(this);
}
