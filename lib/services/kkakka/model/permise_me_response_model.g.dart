// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'permise_me_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PermiseMeResponseModel _$PermiseMeResponseModelFromJson(
    Map<String, dynamic> json) {
  return PermiseMeResponseModel(
    endDate: json['endDate'] == null
        ? null
        : DateTime.parse(json['endDate'] as String),
    firstRepl: json['firstRepl'] as String?,
    firstReplImg: json['firstReplImg'] as String?,
    firstReplUserName: json['firstReplUserName'] as String?,
    kkakkaBalance: json['kkakkaBalance'] as int?,
    kkakkaId: json['kkakkaId'] as int?,
    kkakkaImage: json['kkakkaImage'] as String?,
    kkakkaLimit: json['kkakkaLimit'] as int?,
    kkakkaMessage: json['kkakkaMessage'] as String?,
    kkakkaPrice: json['kkakkaPrice'] as int?,
    kkakkaSignature: json['kkakkaSignature'] as String?,
    kkakkaStatus: json['kkakkaStatus'] as String?,
    kkakkaType: json['kkakkaType'] as String?,
    likeCount: json['likeCount'] as int?,
    likeYn: json['likeYn'] as int?,
    profileImgOpponent: json['profileImgOpponent'] as String?,
    startDate: json['startDate'] == null
        ? null
        : DateTime.parse(json['startDate'] as String),
    userNameOpponent: json['userNameOpponent'] as String?,
    userNoOpponent: json['userNoOpponent'] as int?,
    userPhoneOpponent: json['userPhoneOpponent'] as String?,
    userRelCdOpponent: json['userRelCdOpponent'] as String?,
    userStatusOpponent: json['userStatusOpponent'] as String?,
    firstLikeUserName: json['firstLikeUserName'] as String?,
    userRelCdMe: json['userRelCdMe'] as String?,
    isContentOpen: json['isContentOpen'] as bool?,
  );
}

Map<String, dynamic> _$PermiseMeResponseModelToJson(
        PermiseMeResponseModel instance) =>
    <String, dynamic>{
      'endDate': instance.endDate?.toIso8601String(),
      'startDate': instance.startDate?.toIso8601String(),
      'firstRepl': instance.firstRepl,
      'firstReplImg': instance.firstReplImg,
      'firstReplUserName': instance.firstReplUserName,
      'firstLikeUserName': instance.firstLikeUserName,
      'kkakkaImage': instance.kkakkaImage,
      'kkakkaMessage': instance.kkakkaMessage,
      'kkakkaStatus': instance.kkakkaStatus,
      'kkakkaSignature': instance.kkakkaSignature,
      'kkakkaType': instance.kkakkaType,
      'profileImgOpponent': instance.profileImgOpponent,
      'userNameOpponent': instance.userNameOpponent,
      'userPhoneOpponent': instance.userPhoneOpponent,
      'userRelCdOpponent': instance.userRelCdOpponent,
      'userStatusOpponent': instance.userStatusOpponent,
      'userRelCdMe': instance.userRelCdMe,
      'kkakkaBalance': instance.kkakkaBalance,
      'kkakkaId': instance.kkakkaId,
      'kkakkaLimit': instance.kkakkaLimit,
      'kkakkaPrice': instance.kkakkaPrice,
      'likeCount': instance.likeCount,
      'likeYn': instance.likeYn,
      'userNoOpponent': instance.userNoOpponent,
      'isContentOpen': instance.isContentOpen,
    };
