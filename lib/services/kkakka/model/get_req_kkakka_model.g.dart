// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_req_kkakka_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetReqKkakkaModel _$GetReqKkakkaModelFromJson(Map<String, dynamic> json) {
  return GetReqKkakkaModel(
    data: json['data'] as int?,
    resultCode: json['resultCode'] as String?,
    resultMsg: json['resultMsg'] as String?,
    type: json['type'] as String,
  );
}

Map<String, dynamic> _$GetReqKkakkaModelToJson(GetReqKkakkaModel instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
      'type': instance.type,
      'data': instance.data,
    };
