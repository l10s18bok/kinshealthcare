import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';
part 'sent_kkakka_list_response_model.g.dart';

// sentTabView 용 model
@JsonSerializable()
class SentKkaListResponseModel extends BaseRetrofitModel {
  DateTime? endDate;
  DateTime? startDate;
  String? firstLikeUserName;
  String? firstRepl;
  String? firstReplImg;
  String? firstReplUserName;
  String? kkakkaImage;
  String? kkakkaMessage;
  String? kkakkaSignature;
  String? kkakkaStatus;
  String? kkakkaType;
  String? userImage;
  String? userName;
  int? kkakkaBalance;
  int? kkakkaId;
  int? kkakkaLimit;
  int? kkakkaPrice;
  int? likeCount;
  int? likeYn;
  //2021-06-03 baron 추가.
  //api response 변경으로 추가.
  int? userNoOpponent;
  String? userRelCdMe;
  String? userRelCdOpponent;
  String? userStatusOpponent;
  String? userNameOpponent;
  String? userPhoneOpponent;

  // 기능 구현하기 위해 추가 된 변수.
  bool? isContentOpen;
  SentKkaListResponseModel(
      {this.endDate,
      this.startDate,
      this.firstLikeUserName,
      this.firstRepl,
      this.firstReplImg,
      this.firstReplUserName,
      this.kkakkaBalance,
      this.kkakkaId,
      this.kkakkaImage,
      this.kkakkaLimit,
      this.kkakkaMessage,
      this.kkakkaPrice,
      this.kkakkaSignature,
      this.kkakkaStatus,
      this.kkakkaType,
      this.likeCount,
      this.likeYn,
      this.userImage,
      this.userName,
      this.isContentOpen,
      this.userNameOpponent,
      this.userNoOpponent,
      this.userPhoneOpponent,
      this.userRelCdMe,
      this.userRelCdOpponent,
      this.userStatusOpponent});
  factory SentKkaListResponseModel.fromJson(Object? json) =>
      _$SentKkaListResponseModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$SentKkaListResponseModelToJson(this);
}
