import 'package:json_annotation/json_annotation.dart';
part 'get_req_kkakka_model.g.dart';

@JsonSerializable()
class GetReqKkakkaModel {
  String? resultCode;
  String? resultMsg;
  String type = 'single';
  int? data;

  GetReqKkakkaModel(
      {this.data, this.resultCode, this.resultMsg, required this.type});
  factory GetReqKkakkaModel.fromJson(Object? json) =>
      _$GetReqKkakkaModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$GetReqKkakkaModelToJson(this);
}
