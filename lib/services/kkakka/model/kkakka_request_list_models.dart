import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'kkakka_request_list_models.g.dart';

@JsonSerializable()
class ListRequestModel extends BaseRetrofitModel {
  final String? endDate; //종료일
  final String? kkakkaImage; // 이미지
  final String? kkakkaMessage; //메세지
  final int? kkakkaPrice; //금액z
  final int? kkakkaReqId;
  final String? kkakkaReqStatus; //메세지
  final String? kkakkaType; //까까 유형(code)
  final String? profileImgOpponent; //1회 한도 금액
  final String? reqDate;
  final String? startDate;
  final String? userNameOpponent; //시작일
  final int? userNoOpponent;
  final String? userPhoneOpponent; //결제수단 1 ID
  final String? userStatusOpponent; //결제수단 2 ID

  ListRequestModel(
      {this.endDate,
      this.kkakkaImage,
      this.kkakkaMessage,
      this.kkakkaPrice,
      this.kkakkaReqId,
      this.kkakkaReqStatus,
      this.kkakkaType,
      this.profileImgOpponent,
      this.reqDate,
      this.startDate,
      this.userNameOpponent,
      this.userNoOpponent,
      this.userPhoneOpponent,
      this.userStatusOpponent});

  factory ListRequestModel.fromJson(Object? json) =>
      _$ListRequestModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$ListRequestModelToJson(this);
}
