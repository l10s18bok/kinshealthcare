import 'package:json_annotation/json_annotation.dart';

/*
 작성일 : 2021-03-03
 작성자 : Mark,
 화면명 : ,
 경로 : ,
 클래스 : SumKkakkaModel,
 설명 : 까까 받은 금액/ 까까 준 금액을 받아오는 Model
*/

part 'sum_kkakka_model.g.dart';

@JsonSerializable()
class SumKkakkaModel {
  final int? sumMe;
  final int? sumOpp;

  SumKkakkaModel({
    this.sumMe,
    this.sumOpp,
  });

  factory SumKkakkaModel.fromJson(Object? json) =>
      _$SumKkakkaModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$SumKkakkaModelToJson(this);
}
