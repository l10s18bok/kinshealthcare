import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';
part 'kkakka_filter_response_model.g.dart';

@JsonSerializable()
class KkakkaFilterResponseModel extends BaseRetrofitModel {
  DateTime? endDate;
  DateTime? startDate;
  String? kkakkaImage;
  String? kkakkaMessage;
  String? kkakkaSignature;
  String? kkakkaStatus;
  String? kkakkaType;
  String? profileImgOpponent;
  String? userNameOpponent;
  String? userPhoneOpponent;
  String? userRelCdMe;
  String? userRelCdOpponent;
  String? userStatusOpponent;
  int? kkakkaBalance;
  int? kkakkaId;
  int? kkakkaLimit;
  int? kkakkaPrice;
  int? userNoOpponent;
  //2021-06-02 api 추가. baron
  int? likeYn;
  int? likeCount;
  String? firstLikeUserName;
  String? firstRepl;
  String? firstReplUserName;
  String? firstReplImg;
  bool? isContentOpen;

  KkakkaFilterResponseModel(
      {this.endDate,
      this.kkakkaBalance,
      this.kkakkaId,
      this.kkakkaImage,
      this.kkakkaLimit,
      this.kkakkaMessage,
      this.kkakkaPrice,
      this.kkakkaSignature,
      this.kkakkaStatus,
      this.kkakkaType,
      this.profileImgOpponent,
      this.startDate,
      this.userNameOpponent,
      this.userNoOpponent,
      this.userPhoneOpponent,
      this.userRelCdMe,
      this.userRelCdOpponent,
      this.userStatusOpponent,
      this.firstLikeUserName,
      this.firstRepl,
      this.firstReplImg,
      this.firstReplUserName,
      this.likeCount,
      this.likeYn,
      this.isContentOpen});
  factory KkakkaFilterResponseModel.fromJson(Object? json) =>
      _$KkakkaFilterResponseModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$KkakkaFilterResponseModelToJson(this);
}
