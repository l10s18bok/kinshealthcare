// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_request_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetRequestModel _$GetRequestModelFromJson(Map<String, dynamic> json) {
  return GetRequestModel(
    request: json['request'] == null
        ? null
        : RequestModel.fromJson(json['request'] as Object),
    relation: json['relation'] == null
        ? null
        : RelationModel.fromJson(json['relation'] as Object),
  );
}

Map<String, dynamic> _$GetRequestModelToJson(GetRequestModel instance) =>
    <String, dynamic>{
      'request': instance.request,
      'relation': instance.relation,
    };

RequestModel _$RequestModelFromJson(Map<String, dynamic> json) {
  return RequestModel(
    userRelNo: json['userRelNo'] as int?,
    kkakkaType: json['kkakkaType'] as String?,
    kkakkaReqStatus: json['kkakkaReqStatus'] as String?,
    startDate: json['startDate'] == null
        ? null
        : DateTime.parse(json['startDate'] as String),
    endDate: json['endDate'] == null
        ? null
        : DateTime.parse(json['endDate'] as String),
    kkakkaPrice: json['kkakkaPrice'] as int?,
    kkakkaImage: json['kkakkaImage'] as String?,
  );
}

Map<String, dynamic> _$RequestModelToJson(RequestModel instance) =>
    <String, dynamic>{
      'userRelNo': instance.userRelNo,
      'kkakkaType': instance.kkakkaType,
      'kkakkaReqStatus': instance.kkakkaReqStatus,
      'startDate': instance.startDate?.toIso8601String(),
      'endDate': instance.endDate?.toIso8601String(),
      'kkakkaPrice': instance.kkakkaPrice,
      'kkakkaImage': instance.kkakkaImage,
    };

RelationModel _$RelationModelFromJson(Map<String, dynamic> json) {
  return RelationModel(
    userNoRel: json['userNoRel'] as int?,
    userRelCdMeValue: json['userRelCdMeValue'] as String?,
    userRelCdOpponentValue: json['userRelCdOpponentValue'] as String?,
    userNameOpponent: json['userNameOpponent'] as String?,
    userPhoneOpponent: json['userPhoneOpponent'] as String?,
    userStatusOpponent: json['userStatusOpponent'] as String?,
    profileImgOpponent: json['profileImgOpponent'] as String?,
  );
}

Map<String, dynamic> _$RelationModelToJson(RelationModel instance) =>
    <String, dynamic>{
      'userNoRel': instance.userNoRel,
      'userRelCdMeValue': instance.userRelCdMeValue,
      'userRelCdOpponentValue': instance.userRelCdOpponentValue,
      'userNameOpponent': instance.userNameOpponent,
      'userPhoneOpponent': instance.userPhoneOpponent,
      'userStatusOpponent': instance.userStatusOpponent,
      'profileImgOpponent': instance.profileImgOpponent,
    };
