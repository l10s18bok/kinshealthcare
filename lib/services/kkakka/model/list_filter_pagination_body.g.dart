// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_filter_pagination_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListFilterPaginationBody _$ListFilterPaginationBodyFromJson(
    Map<String, dynamic> json) {
  return ListFilterPaginationBody(
    direction: json['direction'] as bool,
    sortBy: json['sortBy'] as String,
    page: json['page'] as int,
    size: json['size'] as int,
    status: json['status'] as String?,
    kkakkaFilterType: json['kkakkaFilterType'] as String?,
    type: json['type'] as String?,
    fromDate: json['fromDate'] as String?,
    toDate: json['toDate'] as String?,
  );
}

Map<String, dynamic> _$ListFilterPaginationBodyToJson(
        ListFilterPaginationBody instance) =>
    <String, dynamic>{
      'direction': instance.direction,
      'page': instance.page,
      'size': instance.size,
      'sortBy': instance.sortBy,
      'status': instance.status,
      'kkakkaFilterType': instance.kkakkaFilterType,
      'type': instance.type,
      'fromDate': instance.fromDate,
      'toDate': instance.toDate,
    };
