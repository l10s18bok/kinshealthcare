import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'signature_models.g.dart';

@JsonSerializable()
class SignatureBody extends BaseRetrofitModel {
  final int kkakkaId;
  final String signature;

  SignatureBody({
    required this.kkakkaId,
    required this.signature,
  });

  factory SignatureBody.fromJson(Map<String, dynamic> json) =>
      _$SignatureBodyFromJson(json);

  Map<String, dynamic> toJson() => _$SignatureBodyToJson(this);
}
