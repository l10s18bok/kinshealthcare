// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sum_kkakka_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SumKkakkaModel _$SumKkakkaModelFromJson(Map<String, dynamic> json) {
  return SumKkakkaModel(
    sumMe: json['sumMe'] as int?,
    sumOpp: json['sumOpp'] as int?,
  );
}

Map<String, dynamic> _$SumKkakkaModelToJson(SumKkakkaModel instance) =>
    <String, dynamic>{
      'sumMe': instance.sumMe,
      'sumOpp': instance.sumOpp,
    };
