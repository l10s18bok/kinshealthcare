import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';

part 'list_filter_pagination_body.g.dart';
/*
 작성일 : 2021-03-10
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : ListFilterPaginationBody,
 설명 : 까까 List 를 필터링 하기 위한 요청의 Body
*/

@JsonSerializable()
class ListFilterPaginationBody extends GeneralPaginationBody {
  final String? status;
  final String? kkakkaFilterType;
  final String? type;
  final String? fromDate;
  final String? toDate;

  ListFilterPaginationBody({
    bool direction = false,
    String sortBy = 'id',
    int page = 0,
    int size = 10,
    this.status,
    this.kkakkaFilterType,
    this.type,
    this.fromDate,
    this.toDate,
  }) : super(
          direction: direction,
          sortBy: sortBy,
          page: page,
          size: size,
        );

  factory ListFilterPaginationBody.fromJson(Map<String, dynamic> json) =>
      _$ListFilterPaginationBodyFromJson(json);

  Map<String, dynamic> toJson() => _$ListFilterPaginationBodyToJson(this);

  copyWith({
    bool? direction,
    int? page,
    int? size,
    String? sortBy,
  }) {
    direction ??= this.direction;
    page ??= this.page;
    size ??= this.size;
    sortBy ??= this.sortBy;

    return ListFilterPaginationBody(
      direction: direction,
      page: page,
      size: size,
      sortBy: sortBy,
      status: this.status,
      kkakkaFilterType: this.kkakkaFilterType,
      type: this.type,
      fromDate: this.fromDate,
      toDate: this.toDate,
    );
  }
}
