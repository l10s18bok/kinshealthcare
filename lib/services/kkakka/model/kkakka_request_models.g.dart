// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'kkakka_request_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostKkakkaIssuanceSendBody _$PostKkakkaIssuanceSendBodyFromJson(
    Map<String, dynamic> json) {
  return PostKkakkaIssuanceSendBody(
    kkakkaImage: json['kkakkaImage'] as String?,
    kkakkaMessage: json['kkakkaMessage'] as String?,
    kkakkaLimit: json['kkakkaLimit'] as int?,
    kkakkaPrice: json['kkakkaPrice'] as int?,
    kkakkaType: json['kkakkaType'] as String?,
    endDate: json['endDate'] as String?,
    startDate: json['startDate'] as String?,
    userNoRel: json['userNoRel'] as int?,
    userPaymentWayId1: json['userPaymentWayId1'] as int?,
    userPaymentWayId2: json['userPaymentWayId2'] as int?,
  );
}

Map<String, dynamic> _$PostKkakkaIssuanceSendBodyToJson(
        PostKkakkaIssuanceSendBody instance) =>
    <String, dynamic>{
      'kkakkaImage': instance.kkakkaImage,
      'kkakkaMessage': instance.kkakkaMessage,
      'kkakkaLimit': instance.kkakkaLimit,
      'kkakkaPrice': instance.kkakkaPrice,
      'kkakkaType': instance.kkakkaType,
      'endDate': instance.endDate,
      'startDate': instance.startDate,
      'userNoRel': instance.userNoRel,
      'userPaymentWayId1': instance.userPaymentWayId1,
      'userPaymentWayId2': instance.userPaymentWayId2,
    };

PostKkakkaResponse _$PostKkakkaResponseFromJson(Map<String, dynamic> json) {
  return PostKkakkaResponse(
    resultCode: json['resultCode'] as String?,
    resultMsg: json['resultMsg'] as String?,
  );
}

Map<String, dynamic> _$PostKkakkaResponseToJson(PostKkakkaResponse instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
    };

PostKkakkaRequestRequestBody _$PostKkakkaRequestRequestBodyFromJson(
    Map<String, dynamic> json) {
  return PostKkakkaRequestRequestBody(
    kkakkaImage: json['kkakkaImage'] as String?,
    kkakkaMessage: json['kkakkaMessage'] as String?,
    kkakkaPrice: json['kkakkaPrice'] as int?,
    kkakkaType: json['kkakkaType'] as String?,
    endDate: json['endDate'] as String?,
    startDate: json['startDate'] as String?,
    userNoRel: json['userNoRel'] as int?,
  );
}

Map<String, dynamic> _$PostKkakkaRequestRequestBodyToJson(
        PostKkakkaRequestRequestBody instance) =>
    <String, dynamic>{
      'kkakkaImage': instance.kkakkaImage,
      'kkakkaMessage': instance.kkakkaMessage,
      'kkakkaPrice': instance.kkakkaPrice,
      'kkakkaType': instance.kkakkaType,
      'endDate': instance.endDate,
      'startDate': instance.startDate,
      'userNoRel': instance.userNoRel,
    };

PostKkakkaDonateSendBody _$PostKkakkaDonateSendBodyFromJson(
    Map<String, dynamic> json) {
  return PostKkakkaDonateSendBody(
    kkakkaImage: json['kkakkaImage'] as String?,
    kkakkaMessage: json['kkakkaMessage'] as String?,
    kkakkaPrice: json['kkakkaPrice'] as int?,
    endDate: json['endDate'] as String?,
    startDate: json['startDate'] as String?,
    donationId: json['donationId'] as int?,
    userPaymentWayId1: json['userPaymentWayId1'] as int?,
    userPaymentWayId2: json['userPaymentWayId2'] as int?,
  );
}

Map<String, dynamic> _$PostKkakkaDonateSendBodyToJson(
        PostKkakkaDonateSendBody instance) =>
    <String, dynamic>{
      'donationId': instance.donationId,
      'kkakkaImage': instance.kkakkaImage,
      'kkakkaMessage': instance.kkakkaMessage,
      'kkakkaPrice': instance.kkakkaPrice,
      'endDate': instance.endDate,
      'startDate': instance.startDate,
      'userPaymentWayId1': instance.userPaymentWayId1,
      'userPaymentWayId2': instance.userPaymentWayId2,
    };
