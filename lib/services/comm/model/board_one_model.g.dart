// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'board_one_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BoardOneModel _$BoardOneModelFromJson(Map<String, dynamic> json) {
  return BoardOneModel(
    id: json['id'] as int?,
    title: json['title'] as String?,
    content: json['content'] as String?,
    dates:
        json['dates'] == null ? null : DateTime.parse(json['dates'] as String),
  );
}

Map<String, dynamic> _$BoardOneModelToJson(BoardOneModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'content': instance.content,
      'dates': instance.dates?.toIso8601String(),
    };
