import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'cert_ok_email_model.g.dart';

@JsonSerializable()
class CertOkEmailResponse extends BaseRetrofitModel {
  final String resultCode;
  final String resultMsg;

  CertOkEmailResponse({
    required this.resultCode,
    required this.resultMsg,
  });

  factory CertOkEmailResponse.fromJson(Map<String, dynamic> json) => _$CertOkEmailResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CertOkEmailResponseToJson(this);
}

@JsonSerializable()
class CertOkEmailBody extends BaseRetrofitModel {
  final String? email;
  final String? certText;

  CertOkEmailBody({
    required this.email,
    required this.certText,
  });

  factory CertOkEmailBody.fromJson(Map<String, dynamic> json) => _$CertOkEmailBodyFromJson(json);

  Map<String, dynamic> toJson() => _$CertOkEmailBodyToJson(this);
}
