import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'result_form_model.g.dart';

/*
 작성일 : 2021-03-05
 작성자 : Victor
 화면명 : 
 클래스 : ResultFormModel
 경로 : 
 설명 : 통신 모델 중 ResultForm를 구현한 것
*/

@JsonSerializable()
class ResultFormModel extends BaseRetrofitModel {
  final String? resultCode;
  final String? resultMsg;

  ResultFormModel({
    this.resultCode,
    this.resultMsg,
  });

  factory ResultFormModel.fromJson(Map<String, dynamic> json) =>
      _$ResultFormModelFromJson(json);

  Map<String, dynamic> toJson() => _$ResultFormModelToJson(this);
}
