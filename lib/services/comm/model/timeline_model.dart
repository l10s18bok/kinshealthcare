import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'timeline_model.g.dart';

@JsonSerializable()
class TimelineListBody extends BaseRetrofitModel {
  final String? viewType;
  final String appType;

  TimelineListBody({
    this.viewType,
    this.appType = 'H',
  });

  factory TimelineListBody.fromJson(Map<String, dynamic> json) =>
      _$TimelineListBodyFromJson(json);

  Map<String, dynamic> toJson() => _$TimelineListBodyToJson(this);
}
