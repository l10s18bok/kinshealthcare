// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cert_ok_email_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CertOkEmailResponse _$CertOkEmailResponseFromJson(Map<String, dynamic> json) {
  return CertOkEmailResponse(
    resultCode: json['resultCode'] as String,
    resultMsg: json['resultMsg'] as String,
  );
}

Map<String, dynamic> _$CertOkEmailResponseToJson(
        CertOkEmailResponse instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
    };

CertOkEmailBody _$CertOkEmailBodyFromJson(Map<String, dynamic> json) {
  return CertOkEmailBody(
    email: json['email'] as String?,
    certText: json['certText'] as String?,
  );
}

Map<String, dynamic> _$CertOkEmailBodyToJson(CertOkEmailBody instance) =>
    <String, dynamic>{
      'email': instance.email,
      'certText': instance.certText,
    };
