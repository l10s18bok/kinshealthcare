import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'cert_request_email_model.g.dart';

@JsonSerializable()
class CertRequestEmailResponse extends BaseRetrofitModel {
  final String resultCode;
  final String resultMsg;

  CertRequestEmailResponse({required this.resultCode, required this.resultMsg});

  factory CertRequestEmailResponse.fromJson(Map<String, dynamic> json) =>
      _$CertRequestEmailResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CertRequestEmailResponseToJson(this);
}

@JsonSerializable()
class CertRequestEmailBody extends BaseRetrofitModel {
  final String email;

  CertRequestEmailBody({
    required this.email,
  });

  factory CertRequestEmailBody.fromJson(Map<String, dynamic> json) =>
      _$CertRequestEmailBodyFromJson(json);

  Map<String, dynamic> toJson() => _$CertRequestEmailBodyToJson(this);
}
