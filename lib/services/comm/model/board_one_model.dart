import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';

/*
 작성일 : 2021-02-08
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : BoardOneModel,
 설명 : 게시판 형태의 Detail 을 받아올 때 Model
*/

part 'board_one_model.g.dart';

@JsonSerializable()
class BoardOneModel {
  int? id;
  String? title;
  String? content;
  DateTime? dates;

  BoardOneModel({
    this.id,
    this.title,
    this.content,
    this.dates,
  });

  get timeString {
    return DateFormat('yyyy.MM.dd').format(dates!);
  }

  factory BoardOneModel.fromJson(Object? json) =>
      _$BoardOneModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$BoardOneModelToJson(this);
}
