import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'cert_ok_sms_model.g.dart';

@JsonSerializable()
class CertOkSmsResponse extends BaseRetrofitModel {
  final String resultCode;
  final String resultMsg;

  CertOkSmsResponse({
    required this.resultCode,
    required this.resultMsg,
  });

  factory CertOkSmsResponse.fromJson(Map<String, dynamic> json) => _$CertOkSmsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CertOkSmsResponseToJson(this);
}

@JsonSerializable()
class CertOkSmsBody extends BaseRetrofitModel {
  final String? phone;
  final String? certText;

  CertOkSmsBody({
    required this.phone,
    required this.certText,
  });

  factory CertOkSmsBody.fromJson(Map<String, dynamic> json) => _$CertOkSmsBodyFromJson(json);

  Map<String, dynamic> toJson() => _$CertOkSmsBodyToJson(this);
}
