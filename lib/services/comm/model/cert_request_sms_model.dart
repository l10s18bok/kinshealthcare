import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/services/common/base_model.dart';

part 'cert_request_sms_model.g.dart';

@JsonSerializable()
class CertRequestSmsResponse extends BaseRetrofitModel {
  final String resultCode;
  final String resultMsg;

  CertRequestSmsResponse({
    required this.resultCode,
    required this.resultMsg,
  });

  factory CertRequestSmsResponse.fromJson(Map<String, dynamic> json) =>
      _$CertRequestSmsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CertRequestSmsResponseToJson(this);
}

@JsonSerializable()
class CertRequestSmsBody extends BaseRetrofitModel {
  final String phone;

  CertRequestSmsBody({
    required this.phone,
  });

  factory CertRequestSmsBody.fromJson(Map<String, dynamic> json) =>
      _$CertRequestSmsBodyFromJson(json);

  Map<String, dynamic> toJson() => _$CertRequestSmsBodyToJson(this);
}
