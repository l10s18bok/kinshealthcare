// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'result_form_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResultFormModel _$ResultFormModelFromJson(Map<String, dynamic> json) {
  return ResultFormModel(
    resultCode: json['resultCode'] as String?,
    resultMsg: json['resultMsg'] as String?,
  );
}

Map<String, dynamic> _$ResultFormModelToJson(ResultFormModel instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
    };
