// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timeline_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TimelineListBody _$TimelineListBodyFromJson(Map<String, dynamic> json) {
  return TimelineListBody(
    viewType: json['viewType'] as String?,
    appType: json['appType'] as String,
  );
}

Map<String, dynamic> _$TimelineListBodyToJson(TimelineListBody instance) =>
    <String, dynamic>{
      'viewType': instance.viewType,
      'appType': instance.appType,
    };
