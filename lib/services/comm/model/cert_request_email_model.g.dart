// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cert_request_email_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CertRequestEmailResponse _$CertRequestEmailResponseFromJson(
    Map<String, dynamic> json) {
  return CertRequestEmailResponse(
    resultCode: json['resultCode'] as String,
    resultMsg: json['resultMsg'] as String,
  );
}

Map<String, dynamic> _$CertRequestEmailResponseToJson(
        CertRequestEmailResponse instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
    };

CertRequestEmailBody _$CertRequestEmailBodyFromJson(Map<String, dynamic> json) {
  return CertRequestEmailBody(
    email: json['email'] as String,
  );
}

Map<String, dynamic> _$CertRequestEmailBodyToJson(
        CertRequestEmailBody instance) =>
    <String, dynamic>{
      'email': instance.email,
    };
