// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cert_request_sms_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CertRequestSmsResponse _$CertRequestSmsResponseFromJson(
    Map<String, dynamic> json) {
  return CertRequestSmsResponse(
    resultCode: json['resultCode'] as String,
    resultMsg: json['resultMsg'] as String,
  );
}

Map<String, dynamic> _$CertRequestSmsResponseToJson(
        CertRequestSmsResponse instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
    };

CertRequestSmsBody _$CertRequestSmsBodyFromJson(Map<String, dynamic> json) {
  return CertRequestSmsBody(
    phone: json['phone'] as String,
  );
}

Map<String, dynamic> _$CertRequestSmsBodyToJson(CertRequestSmsBody instance) =>
    <String, dynamic>{
      'phone': instance.phone,
    };
