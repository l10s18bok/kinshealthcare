// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cert_ok_sms_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CertOkSmsResponse _$CertOkSmsResponseFromJson(Map<String, dynamic> json) {
  return CertOkSmsResponse(
    resultCode: json['resultCode'] as String,
    resultMsg: json['resultMsg'] as String,
  );
}

Map<String, dynamic> _$CertOkSmsResponseToJson(CertOkSmsResponse instance) =>
    <String, dynamic>{
      'resultCode': instance.resultCode,
      'resultMsg': instance.resultMsg,
    };

CertOkSmsBody _$CertOkSmsBodyFromJson(Map<String, dynamic> json) {
  return CertOkSmsBody(
    phone: json['phone'] as String?,
    certText: json['certText'] as String?,
  );
}

Map<String, dynamic> _$CertOkSmsBodyToJson(CertOkSmsBody instance) =>
    <String, dynamic>{
      'phone': instance.phone,
      'certText': instance.certText,
    };
