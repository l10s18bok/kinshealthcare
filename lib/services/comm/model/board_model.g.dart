// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'board_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BoardModel _$BoardModelFromJson(Map<String, dynamic> json) {
  return BoardModel(
    ids: json['ids'] as int?,
    title: json['title'] as String?,
    dates:
        json['dates'] == null ? null : DateTime.parse(json['dates'] as String),
  );
}

Map<String, dynamic> _$BoardModelToJson(BoardModel instance) =>
    <String, dynamic>{
      'ids': instance.ids,
      'title': instance.title,
      'dates': instance.dates?.toIso8601String(),
    };
