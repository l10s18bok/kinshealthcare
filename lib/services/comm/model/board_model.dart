import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/local_board_model.dart';

/*
 작성일 : 2021-02-08
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : BoardModel,
 설명 : 게시판 형태의 List 를 받아올때의 Model
*/

part 'board_model.g.dart';

@JsonSerializable()
class BoardModel {
  int? ids;
  String? title;
  DateTime? dates;

  BoardModel({
    this.ids,
    this.title,
    this.dates,
  });

  get timeString {
    return DateFormat('yyyy.MM.dd').format(dates!);
  }

  LocalBoardModel get localBoardModel {
    return LocalBoardModel(
      title: this.title ?? '',
      ids: this.ids,
      regDate: timeString,
    );
  }

  factory BoardModel.fromJson(Object? json) =>
      _$BoardModelFromJson(json as Map<String, dynamic>);

  Map<String, dynamic> toJson() => _$BoardModelToJson(this);
}
