// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'email_create_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EmailCreateBody _$EmailCreateBodyFromJson(Map<String, dynamic> json) {
  return EmailCreateBody(
    lastname: json['lastname'] as String?,
    firstname: json['firstname'] as String?,
    id: json['id'] as String?,
    password: json['password'] as String?,
    nation: json['nation'] as String?,
    question: json['question'] as String?,
    answer: json['answer'] as String?,
  );
}

Map<String, dynamic> _$EmailCreateBodyToJson(EmailCreateBody instance) =>
    <String, dynamic>{
      'lastname': instance.lastname,
      'firstname': instance.firstname,
      'id': instance.id,
      'password': instance.password,
      'nation': instance.nation,
      'question': instance.question,
      'answer': instance.answer,
    };
