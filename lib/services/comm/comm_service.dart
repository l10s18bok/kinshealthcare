import 'package:dio/dio.dart' hide Headers;
import 'package:flutter/material.dart';
import 'package:kins_healthcare/consts/network.dart';
import 'package:kins_healthcare/models/timeline.dart';
import 'package:kins_healthcare/services/base_service.dart';
import 'package:kins_healthcare/services/comm/model/board_model.dart';
import 'package:kins_healthcare/services/comm/model/cert_ok_email_model.dart';
import 'package:kins_healthcare/services/comm/model/cert_ok_sms_model.dart';
import 'package:kins_healthcare/services/comm/model/cert_request_email_model.dart';
import 'package:kins_healthcare/services/comm/model/cert_request_sms_model.dart';
import 'package:kins_healthcare/services/comm/model/timeline_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/mykins/model/inquiry_list_model.dart';
import 'package:kins_healthcare/services/user/model/simple_pw_create.dart';
import 'package:retrofit/http.dart';

import 'model/board_one_model.dart';
import 'model/result_form_model.dart';

part 'comm_service.g.dart';

/*
 작성일 : 2021-02-08
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : CommService,
 설명 : 공통 Controller 의 데이터를 받아오기 위한 Service
*/

@RestApi(baseUrl: 'https://$DEV_HOST/api/comm')
abstract class CommService extends BaseService {
  factory CommService(Dio dio, {String baseUrl}) = _CommService;

  @POST('/inquiryNoanwser')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<InquiryListModel>> inquiryNoanwser();

  @POST('/{id}')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ListResponseModel<BoardModel>> boardList(@Path() String id);

  @POST('/{id}')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<SingleItemResponseModel<BoardOneModel>> boardOne(
    @Path() String id,
    @Body() postBody,
  );

  @POST('/cert/request/email')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ResultFormModel> postRequestEmailVerification(
    @Body() postBody,
  );

  @POST('/cert/ok/email')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ResultFormModel> postOkEmailVerification(
    @Body() postBody,
  );

  @POST('/cert/request/sms')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ResultFormModel> postRequestSMSVerification(
    @Body() postBody,
  );

  @POST('/cert/ok/sms')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<ResultFormModel> postOkSMSVerification(
    @Body() postBody,
  );

  @POST('/timelineList')
  Future<ListItemResponseModelTemp<TimelineModel>> timelineList(
    @Body() TimelineListBody body,
  );

  @POST('/cert/request/email')
  Future<CertRequestEmailResponse> certRequestEmail(
    @Body() CertRequestEmailBody body,
  );

  @POST('/cert/ok/email')
  Future<CertOkEmailResponse> certOkEmail(
    @Body() CertOkEmailBody body,
  );

  @POST('/cert/request/sms')
  Future<CertRequestSmsResponse> certRequestSms(
    @Body() CertRequestSmsBody body,
  );

  @POST('/cert/ok/sms')
  Future<CertOkSmsResponse> certOkSms(
    @Body() CertOkSmsBody body,
  );

  @POST('/simplepwCreate')
  @Headers(<String, dynamic>{
    'accessToken': true,
  })
  Future<GeneralPlainResponse> simplePwCreate(
    @Body() SimplePwCreateBody body,
  );
}
