// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comm_service.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _CommService implements CommService {
  _CommService(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://kins-healthcare-dev.kinsapps.com/api/comm';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<ListResponseModel<InquiryListModel>> inquiryNoanwser() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<InquiryListModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/inquiryNoanwser',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<InquiryListModel>.fromJson(
      _result.data!,
      (json) => InquiryListModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<ListResponseModel<BoardModel>> boardList(id) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListResponseModel<BoardModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/$id',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListResponseModel<BoardModel>.fromJson(
      _result.data!,
      (json) => BoardModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<SingleItemResponseModel<BoardOneModel>> boardOne(id, postBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = postBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<SingleItemResponseModel<BoardOneModel>>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/$id',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = SingleItemResponseModel<BoardOneModel>.fromJson(
      _result.data!,
      (json) => BoardOneModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<ResultFormModel> postRequestEmailVerification(postBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = postBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ResultFormModel>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/cert/request/email',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ResultFormModel.fromJson(_result.data!);
    return value;
  }

  @override
  Future<ResultFormModel> postOkEmailVerification(postBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = postBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ResultFormModel>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/cert/ok/email',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ResultFormModel.fromJson(_result.data!);
    return value;
  }

  @override
  Future<ResultFormModel> postRequestSMSVerification(postBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = postBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ResultFormModel>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/cert/request/sms',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ResultFormModel.fromJson(_result.data!);
    return value;
  }

  @override
  Future<ResultFormModel> postOkSMSVerification(postBody) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = postBody;
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ResultFormModel>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/cert/ok/sms',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ResultFormModel.fromJson(_result.data!);
    return value;
  }

  @override
  Future<ListItemResponseModelTemp<TimelineModel>> timelineList(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<ListItemResponseModelTemp<TimelineModel>>(
            Options(method: 'POST', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/timelineList',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = ListItemResponseModelTemp<TimelineModel>.fromJson(
      _result.data!,
      (json) => TimelineModel.fromJson(json),
    );
    return value;
  }

  @override
  Future<CertRequestEmailResponse> certRequestEmail(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CertRequestEmailResponse>(
            Options(method: 'POST', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/cert/request/email',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CertRequestEmailResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<CertOkEmailResponse> certOkEmail(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CertOkEmailResponse>(
            Options(method: 'POST', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/cert/ok/email',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CertOkEmailResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<CertRequestSmsResponse> certRequestSms(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CertRequestSmsResponse>(
            Options(method: 'POST', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/cert/request/sms',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CertRequestSmsResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<CertOkSmsResponse> certOkSms(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<CertOkSmsResponse>(
            Options(method: 'POST', headers: <String, dynamic>{}, extra: _extra)
                .compose(_dio.options, '/cert/ok/sms',
                    queryParameters: queryParameters, data: _data)
                .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = CertOkSmsResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<GeneralPlainResponse> simplePwCreate(body) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(body.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<GeneralPlainResponse>(Options(
                method: 'POST',
                headers: <String, dynamic>{r'accessToken': null},
                extra: _extra)
            .compose(_dio.options, '/simplepwCreate',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = GeneralPlainResponse.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
