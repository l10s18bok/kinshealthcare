import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:kins_healthcare/controllers/auth_controller.dart';
import 'package:kins_healthcare/controllers/cert_controller.dart';
import 'package:kins_healthcare/controllers/comm_controller.dart';
import 'package:kins_healthcare/controllers/donate_controller.dart';
import 'package:kins_healthcare/controllers/force_controller.dart';
import 'package:kins_healthcare/controllers/introduce_controller.dart';
import 'package:kins_healthcare/controllers/kkakka_controller.dart';
import 'package:kins_healthcare/controllers/local_controller.dart';
import 'package:kins_healthcare/controllers/login_controller.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/controllers/relation_controller.dart';
import 'package:kins_healthcare/controllers/suggest_controller.dart';
import 'package:kins_healthcare/controllers/timeline_controller.dart';
import 'package:kins_healthcare/controllers/upload_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/screens/advertise/custom/custom_detail_screen.dart';
import 'package:kins_healthcare/screens/advertise/custom/custom_list_screen.dart';
import 'package:kins_healthcare/screens/advertise/nurse/detail/detail_screen.dart';
import 'package:kins_healthcare/screens/advertise/nurse/favourite/favourite_screen.dart';
import 'package:kins_healthcare/screens/advertise/nurse/public/public_screen.dart';
import 'package:kins_healthcare/screens/advertise/nurse/resume/resume_screen.dart';
import 'package:kins_healthcare/screens/auth/auth_screen.dart'
    as auth_auth_screen;
import 'package:kins_healthcare/screens/auth/register/email/email_screen.dart'
    as auth_email;
import 'package:kins_healthcare/screens/auth/register/phone/phone_screen.dart'
    as register_phone;
import 'package:kins_healthcare/screens/auth/register/profile/profile_screen.dart'
    as register_profile;
import 'package:kins_healthcare/screens/auth/register/register_screen.dart';
import 'package:kins_healthcare/screens/auth/reset/forgot/forgot_screen.dart';
import 'package:kins_healthcare/screens/auth/verify/email/email_screen.dart'
    as verify_email;
import 'package:kins_healthcare/screens/auth/verify/phone/phone_screen.dart'
    as verify_phone;
import 'package:kins_healthcare/screens/auth/verify/pin/pin_screen.dart'
    as verify_pin;
import 'package:kins_healthcare/screens/friends/blocked/blocked_screen.dart';
import 'package:kins_healthcare/screens/friends/friends_screen.dart';
import 'package:kins_healthcare/screens/friends/request/request_screen.dart'
    as friends_request;
import 'package:kins_healthcare/screens/friends/unknown/unknown_screen.dart';
import 'package:kins_healthcare/screens/guide/coach_mark_home_screen.dart';
import 'package:kins_healthcare/screens/guide/guide_screen.dart';
import 'package:kins_healthcare/screens/kkakka/issuance/donate/donate_detail.dart';
import 'package:kins_healthcare/screens/kkakka/issuance/donate/donate_screen.dart';
import 'package:kins_healthcare/screens/kkakka/issuance/donate/donate_target_screen.dart';
import 'package:kins_healthcare/screens/kkakka/issuance/donate/kkakka_donate_screen.dart';
import 'package:kins_healthcare/screens/kkakka/issuance/issuance_screen.dart';
import 'package:kins_healthcare/screens/kkakka/issuance/issuance_send_screen.dart';
import 'package:kins_healthcare/screens/kkakka/issuance/issuance_target_screen.dart';
import 'package:kins_healthcare/screens/kkakka/issuance/suggest_target_screen.dart';
import 'package:kins_healthcare/screens/kkakka/request/kkakka_request_screen.dart';
import 'package:kins_healthcare/screens/kkakka/request/request_target.dart';
import 'package:kins_healthcare/screens/kkakka/request/select_request_type.dart';
import 'package:kins_healthcare/screens/main_home/request/detail/request_see_more.dart';
import 'package:kins_healthcare/screens/main_home/request/like/like_screen.dart';
import 'package:kins_healthcare/screens/main_home/request/news_feed/news_feed_screen.dart';
import 'package:kins_healthcare/screens/main_home/request/reply/reply_screen.dart';
import 'package:kins_healthcare/screens/main_home/request/request_page_screen.dart';
import 'package:kins_healthcare/screens/main_tabs.dart';
import 'package:kins_healthcare/screens/my_kins/age/age_screen.dart';
import 'package:kins_healthcare/screens/my_kins/anniversary/anniversary_screen.dart';
import 'package:kins_healthcare/screens/my_kins/anniversary/register/add_anniversary_screen.dart';
import 'package:kins_healthcare/screens/my_kins/auth/auth_screen.dart'
    as my_kins_auth;
import 'package:kins_healthcare/screens/my_kins/board/board_detail_screen.dart';
import 'package:kins_healthcare/screens/my_kins/board/insert_question_screen.dart';
import 'package:kins_healthcare/screens/my_kins/board/tow_page_screen.dart';
import 'package:kins_healthcare/screens/my_kins/certification/certification_method_screen.dart';
import 'package:kins_healthcare/screens/my_kins/delete_account/delete_account_screen.dart';
import 'package:kins_healthcare/screens/my_kins/family/List/family_list_screen.dart'
    as family_list;
import 'package:kins_healthcare/screens/my_kins/family/family_screen.dart';
import 'package:kins_healthcare/screens/my_kins/family/register/register_screen.dart'
    as my_kins_register;
import 'package:kins_healthcare/screens/my_kins/family/register/relation/relation_screen.dart'
    as relation_screen;
import 'package:kins_healthcare/screens/my_kins/family/register/relation/user/user_screen.dart';
import 'package:kins_healthcare/screens/my_kins/family/remote/remote_screen.dart'
    as family_remote;
import 'package:kins_healthcare/screens/my_kins/my_kins_screen.dart';
import 'package:kins_healthcare/screens/my_kins/notification/notification_setting_screen.dart';
import 'package:kins_healthcare/screens/my_kins/profile/profile_screen.dart';
import 'package:kins_healthcare/screens/my_kins/reset/password/password_screen.dart'
    as my_kins_reset_password;
import 'package:kins_healthcare/screens/my_kins/reset/pin/reset_pin_screen.dart';
import 'package:kins_healthcare/screens/my_kins/search/password/password_screen.dart'
    as my_kins_search_password;
import 'package:kins_healthcare/screens/payment/asking/asking_screen.dart';
import 'package:kins_healthcare/screens/payment/asking/family/family_screen.dart'
    as payment_asking;
import 'package:kins_healthcare/screens/payment/confirm/confirm_screen.dart';
import 'package:kins_healthcare/screens/payment/done/done_screen.dart';
import 'package:kins_healthcare/screens/payment/emergency/detail/hospital_detail.dart';
import 'package:kins_healthcare/screens/payment/emergency/emergency_reg_screen.dart';
import 'package:kins_healthcare/screens/payment/emergency/search/find_hospital_screen.dart';
import 'package:kins_healthcare/screens/payment/emergency/share/emergency_share_screen.dart';
import 'package:kins_healthcare/screens/payment/fail/fail_screen.dart';
import 'package:kins_healthcare/screens/payment/input_amount/input_amount_screen.dart';
import 'package:kins_healthcare/screens/payment/list/list_screen.dart';
import 'package:kins_healthcare/screens/payment/methods/methods.dart';
import 'package:kins_healthcare/screens/payment/methods/register/account_reg_pageList.dart';
import 'package:kins_healthcare/screens/payment/methods/register/card/add_card.dart';
import 'package:kins_healthcare/screens/payment/methods/register/card/bank_select_screen.dart';
import 'package:kins_healthcare/screens/payment/qr/qr_screen.dart';
import 'package:kins_healthcare/screens/payment/requests/requests_screen.dart';
import 'package:kins_healthcare/screens/payment/share/share_screen.dart';
import 'package:kins_healthcare/screens/payment/share/share_target_screen.dart';
import 'package:kins_healthcare/screens/payment/wait/proceed/proceed_screen.dart';
import 'package:kins_healthcare/screens/payment/wait/wait_screen.dart';
import 'package:kins_healthcare/screens/spinor_test/jc/demo_scren.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/card_list.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/get_transition/slide_up_transition.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/family_relations_screen.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/mark_test.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/top_bar_change_test.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/top_bar_nested_list.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/top_bar_sliver_list.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/top_bar_test.dart';
import 'package:kins_healthcare/screens/spinor_test/phil/screens/phil_test.dart';
import 'package:kins_healthcare/screens/spinor_test/splash_test.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/victor_test.dart';
import 'package:kins_healthcare/screens/splash/splash_screen.dart';
import 'package:kins_healthcare/screens/splash/test_splash_screen.dart';
import 'package:kins_healthcare/screens/temp/calendar_screen.dart';
import 'package:kins_healthcare/screens/timeline/suggest/suggest_screen.dart';
import 'package:kins_healthcare/services/root_service.dart';
import 'controllers/banner_controller.dart';
import 'controllers/custom_controller.dart';
import 'controllers/emergency_controller.dart';
import 'controllers/post_controller.dart';
import 'screens/kkakka/requestList/kkakka_request_list_screen.dart';
import 'screens/spinor_test/andy/andy_test.dart';
import 'screens/spinor_test/mark/mark_test.dart';
import 'screens/spinor_test/phil/screens/phil_test.dart';
import 'screens/spinor_test/splash_test.dart';
import 'screens/spinor_test/victor/victor_test.dart';

void main() async {
  await WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  initializeDateFormatting('ko_KR', null).then((value) => runApp(Root()));
}

class Root extends StatefulWidget {
  @override
  _RootState createState() => _RootState();
}

class _RootState extends State<Root> {
  FlutterLocalNotificationsPlugin? flutterLocalNotificationsPlugin;

  /// Android local notification channel id
  String channelId = 'kins_notification_channel';

  /// Android local notification channel name
  String channelName = 'kins_notification';

  /// Android local notification channel description
  String channelDescription = 'android local notification channel';

  /// Variable for checking whether the App is run first time or not.
  late bool checkFirstRun;

  AndroidNotificationChannel? channel;
  @override
  void initState() {
    super.initState();
    initControllers();
    initFirebaseMessaging();
  }

  initFirebaseMessaging() async {
    FirebaseMessaging messaging = FirebaseMessaging.instance;

    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: true,
      provisional: false,
      sound: true,
    );

    if (Platform.isAndroid) {
      await setupLocalNotification();
    }

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      handleForegroundFirebaseMessage();
    }

    handleFirebaseMessageBackgroundOpened();
  }

  handleFirebaseMessagingTokenRefresh() async {
    FirebaseMessaging.instance.onTokenRefresh.listen((token) {
      Get.find<UserController>().deviceRegist();
    });
  }

  handleFirebaseMessageBackgroundOpened() async {
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    print('firebase message opened in background');
    print(initialMessage);
  }

  handleForegroundFirebaseMessage() {
    FirebaseMessaging.onMessage.listen(
      (RemoteMessage message) {
        handleAndroidForegroundFirebaseMessage(message);
      },
    );
  }

  setupLocalNotification() async {
    print('setupLocalNotification');
    AndroidInitializationSettings androidInitializationSettings =
        AndroidInitializationSettings('app_icon');

    final InitializationSettings initializationSettings =
        InitializationSettings(
      android: androidInitializationSettings,
    );

    channel = AndroidNotificationChannel(
      channelId, // id
      channelName, // title
      channelDescription, // description
      importance: Importance.max,
    );

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    await flutterLocalNotificationsPlugin!.initialize(
      initializationSettings,
      onSelectNotification: onSelectLocalNotification,
    );

    await flutterLocalNotificationsPlugin!
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel!);
  }

  Future<dynamic> onSelectLocalNotification(String? payload) async {
    print('on select notification');
    print(payload);
  }

  handleAndroidForegroundFirebaseMessage(RemoteMessage message) {
    final sound = message.notification?.android?.sound ?? 'noSound';
    final soundIos = message.notification?.apple?.sound ?? 'noSound';
    print('handleAndroidForegroundFirebaseMessage()');
    print('message.data : ${message.notification?.title ?? 'no Title'}');
    print('message.data : ${message.notification?.body ?? 'no Body'}');
    print('message.Sound : $sound');
    print('message.soundIos : $soundIos');
    print('message.data : ${message.data}');

    RemoteNotification? notification = message.notification;
    AndroidNotification? android = message.notification?.android;

    if (notification != null &&
        android != null &&
        flutterLocalNotificationsPlugin != null) {
      flutterLocalNotificationsPlugin!.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
            channel!.id,
            channel!.name,
            channel!.description,
            sound: RawResourceAndroidNotificationSound(sound),
            playSound: true,
          ),
        ),
      );
    }
  }

  RootService initServices() {
    RootService rootService = RootService();

    return rootService;
  }

  void initControllers() {
    final services = initServices();

    Get.put(
      ForceController(
        rootService: services,
      ),
    );

    Get.put(
      LoginController(
        rootService: services,
      ),
    );

    Get.put(
      CertController(
        rootService: services,
      ),
    );

    Get.put(
      UserController(
        rootService: services,
      ),
    );

    Get.put(
      UploadController(
        rootService: services,
      ),
    );

    Get.put(
      AuthController(
        rootService: services,
      ),
    );

    Get.put(
      KkakkaController(
        rootService: services,
      ),
    );

    Get.put(
      SuggestController(
        rootService: services,
      ),
    );

    Get.put(
      PaymentController(
        rootService: services,
      ),
    );
    Get.put(
      CommController(
        rootService: services,
      ),
    );
    Get.put(
      DonateController(
        rootService: services,
      ),
    );
    Get.put(
      RelationController(
        rootService: services,
      ),
    );

    Get.put(
      EmergencyController(
        rootService: services,
      ),
    );
    Get.put(
      MykinsController(
        rootService: services,
      ),
    );
    Get.put(
      TimelineController(
        rootService: services,
      ),
    );
    Get.put(
      BannerController(
        rootService: services,
      ),
    );
    Get.put(
      CustomController(
        rootService: services,
      ),
    );
    Get.put(
      PostController(
        rootService: services,
      ),
    );
    Get.put(
      IntroduceController(
        rootService: services,
      ),
    );
    Get.put(
      LocalController(),
    );
  }

  List<GetPage> renderPages() {
    return [
      // [ID] Splash
      // [Name] 스플래시 스크린
      GetPage(
        name: '/test-splash',
        page: () => TestSplashScreen(),
      ),
      // [ID] Splash
      // [Name] 스플래시 스크린
      GetPage(
        name: '/',
        page: () => SplashScreen(),
      ),
      // [ID] HL_0001
      // [Name] Login Page
      GetPage(
        name: '/auth',
        page: () => auth_auth_screen.AuthScreen(),
      ),
      // [ID] HR_0001
      // [Name] 회원가입

      GetPage(
        name: '/auth/register',
        page: () => RegisterScreen(
          title: 'adult',
        ),
      ),

      // [ID] HR_0001
      // [Name] 자녀회원가입

      GetPage(
        name: '/auth/register-kid',
        page: () => RegisterScreen(
          title: 'kid',
        ),
      ),

      // [ID] HR_0002-3
      // [Name] 회원가입 핸드폰 번호 입력
      GetPage(
        name: '/auth/register/phone',
        page: () => register_phone.PhoneScreen(),
      ),
      // [ID] HM_0001
      // [Name] 메일주소 만들기
      GetPage(
        name: '/auth/register/email',
        page: () => auth_email.EmailScreen(),
      ),
      // [ID] HR_0004
      // [NAME] 프로필 사진 등록
      GetPage(
        name: '/auth/register/profile',
        page: () => register_profile.ProfileScreen(),
      ),
      // [ID] HR_0002
      // [NAME] 이메일 인증하기
      GetPage(
        name: '/auth/verify/email',
        page: () => verify_email.EmailScreen(),
      ),
      // [ID] HR_0003
      // [NAME] 휴대폰 인증하기
      // type - signUp || phone
      GetPage(
        name: '/auth/verify/phone/:type',
        page: () => verify_phone.PhoneScreen(),
      ),
      // [ID] HR_0005
      // [NAME] 간편 비밀번호
      GetPage(
        name: '/auth/verify/pin',
        page: () => verify_pin.PinScreen(),
      ),
      // [ID] HL_IP_0002 & HL_IP_0001
      // [NAME] 아이디 비밀번호 찾기
      // type - email || password
      // default - email
      GetPage(
        name: '/auth/reset/:type',
        page: () => ForgotScreen(),
      ),
      // [ID] HF_0001
      // [NAME] 나의 가족
      GetPage(
        name: '/friends',
        page: () => FriendsScreen(),
      ),
      // [ID] HF_0001
      // [NAME] 회원님을 초대한 모른 사람들
      GetPage(
        name: '/friends/unknown',
        page: () => UnknownScreen(),
      ),
      // [ID] HF_0001
      // [NAME] 모른척한 사람들3
      GetPage(
        name: '/friends/blocked',
        page: () => BlockedScreen(),
      ),
      // [ID] HF_0001
      // [NAME] 관계 재요청
      GetPage(
        name: '/friends/request',
        page: () => friends_request.RequestScreen(),
      ),
      // [ID] HH_0001
      // [NAME] 홈탭

      GetPage(
        // 코치마크 홈탭
        name: '/guide-screen',
        page: () => GuideScreen(),
      ),
      // [ID] onboarding
      // [Name] 앱 소개 화면

      GetPage(
        // 코치마크 홈탭
        name: '/coach-mark/main-home',
        page: () => CoachMarkMainTabs(),
      ),
      // [ID] HH_0001-2_coachmark01
      // [Name] 코치마크 홈

      GetPage(
        // 홈탭
        name: '/main-home',
        page: () => MainTabs(),
      ),
      // [ID] HH_1001
      // [Name] 요청받은
      // GetPage(
      //   // 홈탭
      //   //
      //   // Type
      //   // 약속 받은 - request-received
      //   // 약속 해준 - request-give
      //   name: '/main-home/:type',
      //   page: () => RequestScreen(),
      // ),
      // [ID] HP_0001
      // [NAME] 결제
      GetPage(
        // 결제탭
        name: '/payment',
        page: () => MainTabs(
          initialTabIndex: 0,
        ),
      ),
      // [ID] HP_1001
      // [NAME] QR Scan
      GetPage(
        // 결제탭
        name: '/payment/qr',
        page: () => QrScreen(),
      ),
      // [ID] HP_1002
      // [NAME] 결제 입력
      GetPage(
        // 결제탭
        name: '/payment/qr/input-amount',
        page: () => InputAmountScreen(),
      ),
      // [ID] HP_1003
      // [Name] 까까로 결제
      GetPage(
        name: '/payment/confirm',
        page: () => ConfirmScreen(),
      ),
      // [ID] HP_3001
      // [Name] 결제수단 관리
      GetPage(
        name: '/payment/methods',
        page: () => MethodsScreen(),
      ),
      // [ID] HP_2001
      // [Name] 받은 결제요청
      GetPage(
        name: '/payment/requests',
        page: () => RequestsScreen(),
      ),
      // [ID] HP_1011
      // [Name] 추가 결제요청
      GetPage(
        name: '/payment/asking',
        page: () => AskingScreen(),
      ),
      // [ID] 미정
      // [Name] 모자란 금액 가족에 요청하기
      GetPage(
        name: '/payment/asking/family',
        page: () => payment_asking.FamilyScreen(),
      ),
      // [ID] HP_1012
      // [Name] 결제승인 대기중
      GetPage(
        name: '/payment/wait',
        page: () => WaitScreen(),
      ),
      // [ID] HP_1013
      // [Name] 결제 준비 완료
      GetPage(
        name: '/payment/wait/proceed',
        page: () => ProceedScreen(),
      ),
      // [ID] HP_1014
      // [Name] 결제 완료
      GetPage(
        name: '/payment/done',
        page: () => DoneScreen(),
      ),
      // [ID] HP_1015
      // [Name] 결제 실패
      GetPage(
        name: '/payment/fail',
        page: () => FailScreen(),
      ),
      // [ID] HY_1001
      // [Name] 본인확인
      GetPage(
        name: '/my-kins/auth',
        page: () => my_kins_auth.AuthScreen(),
      ),
      // [ID] HY_1002
      // [Name] 회원정보
      GetPage(
        name: '/my-kins/profile',
        page: () => ProfileScreen(title: 'adult',),
      ),
      // [ID]
      // [Name] 자녀회원정보 
      GetPage(
        name: '/my-kins/profile/kid',
        page: () => ProfileScreen(title: 'kid',),
        // page: () => ChildProfileScreen(),
      ),
      // [ID] HY_2001
      // [Name] 나의 가족
      GetPage(
        name: '/my-kins/family',
        page: () => FamilyScreen(
          title: 'family',
        ),
      ),
      // [ID] HY_CHILD_LIST
      // [Name] 나의 자녀
      GetPage(
        name: '/my-kins/family-children',
        page: () => FamilyScreen(
          title: 'children',
        ),
      ),
      // [ID] HY_2002
      // [Name] 주소록 가족등록 가족선택 및 등록
      GetPage(
        name: '/my-kins/family/register',
        page: () => my_kins_register.RegisterScreen(),
      ),
      // [ID] HY_2002_P
      // [Name] 가족관계 선택
      GetPage(
        name: '/my-kins/family/register/relation/',
        page: () => relation_screen.RelationScreen(),
      ),
      // [ID] 5013_어머니 선택
      // [Name] 가족 관계 유저 선택
      GetPage(
        name: '/my-kins/family/register/relation/user',
        page: () => UserScreen(),
      ),
      // [ID] HY_2002-2
      // [Name] 서버 주소록 리스트에서 선택
      GetPage(
        name: '/my-kins/family/remote',
        page: () => family_remote.RemoteScreen(),
      ),
      // [ID] 미정
      // [Name] 나와 함께하는 가족목록
      GetPage(
        name: '/mykins/family/list',
        page: () => family_list.FamilyListScreen(),
      ),

      // [ID] HY_3001
      // [Name] 나의 기념일 달력
      GetPage(
        name: '/my-kins/anniversary',
        page: () => AnniversaryScreen(),
      ),
      // [ID] HY_8003
      // [Name] 간편 비밀번호 재설정
      GetPage(
        name: '/my-kins/reset/pin',
        page: () => ResetPinScreen(),
      ),
      // [ID] HY_8004
      // [Name] 비밀번호 찾기
      GetPage(
        name: '/my-kins/search/password',
        page: () => my_kins_search_password.PasswordScreen(),
      ),
      // [ID] HY_8004
      // [Name] 비밀번호 찾기
      GetPage(
        name: '/my-kins/reset/password',
        page: () => my_kins_reset_password.PasswordScreen(),
      ),
      // [ID] HY_1003
      // [Name] 회원탈퇴
      GetPage(
        name: '/my-kins/delete-account',
        page: () => DeleteAccountScreen(),
      ),
      // [ID] HY_A001
      // [Name] 킨즈나이테
      GetPage(
        name: '/my-kins/age',
        page: () => AgeScreen(),
      ),
      // [ID] HP_0002
      // [Name] 결제목록
      GetPage(
        name: '/payment/list',
        page: () => ListScreen(),
      ),
      // [ID] HP 1003
      // [Name] 까까 결제
      GetPage(
        name: '/payment/confirm',
        page: () => ConfirmScreen(),
      ),
      GetPage(
        // 타임라인탭
        name: '/timeline',
        page: () => MainTabs(
          initialTabIndex: 1,
        ),
      ),
      GetPage(
        // 맞춤정보탭
        name: '/advertise',
        page: () => MainTabs(
          initialTabIndex: 3,
        ),
      ),
      // [ID] HN_1001
      // [Name] 맞춤공고
      GetPage(
        name: '/advertise/nurse/resume',
        page: () => ResumeScreen(),
      ),
      // [ID] HN_1002
      // [Name] 공고 상세정보
      GetPage(
        name: '/advertise/nurse/detail',
        page: () => DetailScreen(),
      ),
      // [ID] HN_I_0001
      // [Name] 관심공고
      GetPage(
        name: '/advertise/nurse/favourite',
        page: () => FavouriteScreen(),
      ),
      // [ID] HN_S_0001
      // [Name] 공유할 가족 선택
      GetPage(
        name: '/advertise/nurse/public',
        page: () => PublicScreen(),
      ),
      // [ID] HN_2001
      // [Name] 맞춤광고
      GetPage(
        name: '/advertise/custom/list',
        page: () => AdListScreen(),
      ),
      // [ID] HN_2002
      // [Name] 맞춤광고 자세히보기
      GetPage(
        name: '/advertise/custom/detail',
        page: () => CustomDetailScreen(),
      ),
      GetPage(
        name: '/my-kins',
        page: () => MainTabs(
          initialTabIndex: 4,
        ),
      ),
      // [ID] HU_3001
      GetPage(
        name: '/kkakka/issuance/donate',
        page: () => DonateScreen(),
      ),
      // [ID] HU_3001(신규)
      GetPage(
        name: '/kkakka/issuance/donate/target',
        page: () => DonateTargetScreen(),
      ),

      // [ID] HU_3001_2
      // [Name] HU_3001 기부 까까 발행 자세히 보기
      GetPage(
        name: '/kkakka/issuance/donate/detail',
        page: () => DonateDetail(),
      ),

      // [ID] HU_3003
      // [Name] HU_3003 기부 까까 발행
      GetPage(
        name: '/kkakka/issuance/donate/edit',
        page: () => KkakkaDonateScreen(),
      ),

      // TODO spinor 테스트용 삭제예정!
      GetPage(
        name: '/splash-test',
        page: () => SplashTest(),
      ),
      // TODO spinor 테스트용 삭제예정!
      GetPage(
        name: '/spinor/andy-test',
        page: () => AndyTest(),
      ),
      GetPage(
        // [ID] HS_0001
        // [Name] 까까요청(유형선택)
        name: '/kkakka/request',
        page: () => SelectRequestType(),
      ),
      GetPage(
        // [ID] HS_3001
        // [Name] 까까 요청(대상선정)
        name: '/kkakka/request/target',
        page: () => RequestTarget(),
      ),
      GetPage(
        // [ID] HS_3002
        // [Name] 까까요청(내용작성)
        name: '/kkakka/request/target/edit',
        page: () => KkakkaRequestScreen(),
      ),
      GetPage(
        // [ID] HU_4002
        // [Name] 까까발행(내용작성)
        name: '/kkakka/issuance/target/detail',
        page: () => IssuanceSendScreen(),
      ),
      // [ID] HU_4001[제안하기]
      // [Name] 제안하기(내용작성)
      GetPage(
        name: '/kkakka/issuance/suggest/target',
        page: () => SuggestTarget(),
      ),
      GetPage(
        // [ID] HI_0003
        // [Name] 까까 발행(자세히보기)
        name: '/timeline/suggest',
        page: () => SuggestScreen(),
      ),
      GetPage(
        // [ID] HD_0001
        // [Name] 까까 발행(자세히보기)
        name: '/main-home/request/detail',
        page: () => RequestSeeMore(),
      ),
      GetPage(
        // [ID] HU_6001
        // [Name] 소식 작성하기
        name: '/main-home/news-feed',
        page: () => NewsFeedScreen(),
      ),
      GetPage(
        // [ID] HH_Like
        // [Name] 좋아요한 사람들
        name: '/main-home/request/like',
        page: () => LikeScreen(),
      ),
      GetPage(
        // [ID] HH_REPL
        // [Name] 댓글
        name: '/main-home/request/reply',
        page: () => ReplyScreen(),
      ),
      GetPage(
        // [ID] HP_3002
        // [Name] 카드등록(결제카드추가)
        name: '/payment/methods/register/card',
        page: () => AddCard(),
      ),
      GetPage(
        // [ID] HP_3004
        // [Name] 계좌등록(은행선택)
        name: '/payment/methods/register/bank',
        page: () => BankSelectScreen(),
      ),
      GetPage(
        // [ID] HY_3003 + HY_3004
        // [Name] 기념일 등록 및 공개가족선택
        name: '/my-kins/anniversary/register/public',
        page: () => AddAnniversaryScreen(),
      ),
      GetPage(
        // [ID] HP_5001
        // [Name] 응급병원관리(응급병원등록)
        name: '/payment/emergency',
        page: () => EmergencyRegScreen(),
      ),
      GetPage(
        // [ID] HP_5002
        // [Name] 응급병원관리(응급병원찾기)
        name: '/payment/emergency/search',
        page: () => FindHospitalScreen(),
      ),
      GetPage(
        // [ID] HP_5003
        // [Name] 병원 상세정보
        name: '/payment/emergency/detail',
        page: () => HospitalDetail(),
      ),
      GetPage(
        // [ID] HP_5004
        // [Name] 응급병원 나누기 설정
        name: '/payment/emergency/share-register',
        page: () => EmergencyShareScreen(hospitalSetup: HospitalSetup.REGISTER),
      ),

      GetPage(
        // [ID] HP_5004
        // [Name] 응급병원 나누기 수정
        name: '/payment/emergency/share-update',
        page: () => EmergencyShareScreen(hospitalSetup: HospitalSetup.UPDATE),
      ),

      GetPage(
        // [ID] HP_3004, HP_3005, HP_3006
        // [Name] HP_3004 (계좌등록 => 은행선택), HP_3005(계좌등록 => 계좌번호입력), HP_3006(계좌등록 => ARS인증))
        name: '/payment/methods/register/account_reg_pageList',
        page: () => AccountRegPageList(),
      ),
      // GetPage(
      //   // [ID] HP_3005
      //   // [Name] 계좌번호 입력
      //   name: '/payment/methods/register/account',
      //   page: () => EnterAccountNumber(),
      // ),

      // TODO spinor 테스트용 삭제예정!
      GetPage(
        name: '/spinor/mark-test',
        page: () => MarkTest(),
      ),
      ...markTestPage,
      // [ID] HY_4001
      // [Name] 알림설정
      GetPage(
        name: '/my-kins/notification-setting',
        page: () => NotificationSettingScreen(),
      ),
      // [ID] HY_5001/HY_5002
      // [Name] 공지사항/이용안내
      GetPage(
        name: '/my-kins/notice',
        page: () => TowPageScreen(type: TowPageScreen.towPageTypeNotice),
      ),
      // [ID] HY_6001/HY_7001
      // [Name] 1:1문의/FAQ
      GetPage(
        name: '/my-kins/faq',
        page: () => TowPageScreen(type: TowPageScreen.towPageTypeInfo),
      ),
      // [ID] HY_5001_2
      // [Name] 공지 상세보기
      GetPage(
        name: '/my-kins/notice-detail/:id',
        page: () => BoardDetailScreen(),
      ),
      // [ID] HY_8001
      // [Name] 인증수단관리
      GetPage(
        name: '/my-kins/certification-method',
        page: () => CertificationMethodScreen(),
      ),
      // [ID] HY_6002
      // [Name] 문의등록
      GetPage(
        name: '/my-kins/faq/insert_question',
        page: () => InsertQuestionScreen(),
      ),
      // [ID] HP_4001
      // [Name] 결제나누기설정
      GetPage(
        name: '/payment/share',
        page: () => ShareScreen(),
      ),
      // [ID]
      // [Name] 결제나누기설정 가족 리스트
      GetPage(
        name: '/payment/share-target',
        page: () => ShareTargetScreen(),
      ),
      // [ID] HU_0001
      // [Name] 까까발행
      GetPage(
        name: '/kkakka/issuance',
        page: () => IssuanceScreen(),
      ),
      // [ID] HU_4001
      // [Name] 까까발행
      GetPage(
        name: '/kkakka/issuance/target',
        page: () => IssuanceTargetScreen(),
      ),
      // [ID] HH_3001
      // [Name] 약속받은
      GetPage(
        name: '/main-home/request-received',
        page: () => RequestScreenPage(type: ScreenTypes.received),
      ),
      // [ID] HH_4001
      // [Name] 약속해준
      GetPage(
        name: '/main-home/request-give',
        page: () => RequestScreenPage(type: ScreenTypes.give),
      ),
      // TODO spinor 테스트용 삭제예정!
      GetPage(
        name: '/spinor/phil-test',
        page: () => PhilTest(),
      ),

      // TODO spinor 테스트용 삭제예정!
      GetPage(
        name: '/spinor/victor-test',
        page: () => VictorTest(),
      ),
      // TODO 테스트용 지우기!
      GetPage(
        name: '/spinor/jc-test',
        page: () => DemoScreen(),
      ),
      // [ID] HU_1002
      GetPage(
        name: '/calendar',
        page: () => CalendarScreen(),
      ),
      GetPage(
        name: '/kkakka/list-request',
        page: () => KkakkaRequestListScreen(),
      ),
    ];
  }

  final List markTestPage = [
    GetPage(
      name: '/my-kins/notice-mark',
      page: () => TowPageScreen(type: TowPageScreen.towPageTypeNotice),
      customTransition: SlideUpTransitions(),
    ),
    // [ID] HY_6001/HY_7001
    // [Name] 1:1문의/FAQ
    GetPage(
      name: '/my-kins/faq-mark',
      page: () => TowPageScreen(type: TowPageScreen.towPageTypeInfo),
      customTransition: SlideUpTransitions(),
    ),
    // [ID]
    // [Name] 가족관계도
    GetPage(
      name: '/spinor/mark-test/family_relations',
      page: () => FamilyRelationsScreen(),
    ),
    // [ID]
    // [Name] topBarTest
    GetPage(
      name: '/spinor/mark-test/top-bar-test',
      page: () => TopBarTest(),
    ),
    GetPage(
      name: '/spinor/mark-test/top-nested-bar-list',
      page: () => TopNestedBarList(),
    ),
    GetPage(
      name: '/spinor/mark-test/top-bar-sliver-list--test',
      page: () => TopSliverList(),
    ),
    GetPage(
      name: '/spinor/mark-test/top-bar-change-test',
      page: () => TopBarChangeTest(),
    ),
    // [ID] HY_0001
    // [Name] 마이킨즈 Mark
    GetPage(
      name: '/spinor/mark-test/my-kins-mark',
      page: () => MyKinsScreen(),
    ),
    // [ID]
    // [Name] 카드 선택 Example
    GetPage(
      name: '/spinor/mark-test/card-list',
      page: () => CardList(),
      transition: Transition.downToUp,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        fontFamily: 'NotoSans',
      ),
      initialRoute: '/',
      debugShowCheckedModeBanner: false,
      getPages: renderPages(),
    );
  }
}
