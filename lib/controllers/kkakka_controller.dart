import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/models/kkakka_model.dart';
import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/kkakka/model/get_request_models.dart';
import 'package:kins_healthcare/services/kkakka/model/kkakka_filter_response_model.dart';
import 'package:kins_healthcare/services/kkakka/model/kkakka_request_list_models.dart';
import 'package:kins_healthcare/services/kkakka/model/kkakka_request_models.dart';
import 'package:kins_healthcare/services/kkakka/model/list_filter_pagination_body.dart';
import 'package:kins_healthcare/services/kkakka/model/list_send_opponent.dart';
import 'package:kins_healthcare/services/kkakka/model/sent_kkakka_list_response_model.dart';
import 'package:kins_healthcare/services/kkakka/model/signature_models.dart';
import 'package:kins_healthcare/services/kkakka/model/sum_kkakka_model.dart';
import 'package:kins_healthcare/services/kkakka/model/permise_me_response_model.dart';
import 'package:kins_healthcare/services/root_service.dart';

class KkakkaController extends BaseController {
  // List<KkakkaModel> receivedKkakkas = [];
  // //보낸까까 list
  // List<KkakkaModel> issuedKkakkas = [];
  // List<KkakkaModel> filterKkakkas = [];

  KkakkaController({
    required RootService rootService,
  }) : super(rootService);
  ListFilterPaginationBody? filterBody;
  String? filterkey;

  /// baron 2021-05-30
  /// request [/api/kkakka/list] data.
  Future<List<SentKkaListResponseModel>> sentList({
    GeneralPaginationBody? body,
    bool reset = false,
  }) async {
    final resp = await this.paginate<SentKkaListResponseModel>(
      request: this.services.kkakkaService.sentList,
      body: body,
      reset: reset,
    );
    return resp;
  }

  Future<void> switchFilter(bool isReceived) async {
    final format = DateFormat('yyyy-MM-dd');
    if (isReceived) {
      filterBody = ListFilterPaginationBody(
        kkakkaFilterType: 'REQME',
      );
    } else {
      filterBody = ListFilterPaginationBody(
          kkakkaFilterType: 'SENDOPPO',
          fromDate: format.format(
            DateTime.now().subtract(Duration(days: 30)),
          ),
          toDate: format.format(DateTime.now()));
    }
    update();
  }

  /// baron 2021-05-30
  /// request [/api/kkakka/listPromiseMe] data.
  Future<List<PermiseMeResponseModel>> listPromiseMe({
    GeneralPaginationBody? body,
    bool reset = false,
  }) async {
    final resp = await this.paginate<PermiseMeResponseModel>(
      request: this.services.kkakkaService.listPromiseMe,
      body: body,
      reset: reset,
    );
    return resp;
  }

  /// baron 2021-05-30
  /// request [/api/kkakka/list/request] data.
  Future<List<ListRequestModel>> requestList({
    GeneralPaginationBody? body,
    bool reset = false,
  }) async {
    final resp = await this.paginate<ListRequestModel>(
      request: this.services.kkakkaService.requestList,
      body: body,
      reset: reset,
    );
    return resp;
  }

  /// baron 2021-05-30
  /// get pagination cache data By [ListRequestModel]
  get cache_requestList {
    return this.generateGetter(type: ListRequestModel);
  }

  /// baron 2021-05-30
  /// get pagination cache data By [PermiseMeResponseModel]
  get cache_listPromiseMe {
    return this.generateGetter(type: PermiseMeResponseModel);
  }

  /// baron 2021-05-30
  /// get pagination cache data By [PermiseMeResponseModel]
  get cache_sentList {
    return this.generateGetter(type: SentKkaListResponseModel);
  }

  get cache_promiseTotal {
    return this.getTotal(type: PermiseMeResponseModel);
  }

  get cache_sentTotal {
    return this.getTotal(type: SentKkaListResponseModel);
  }

  /// baron 2021-05-31
  /// get pagination cache data By [KkakkaModel]  with [listFilterPage]Function
  get cache_receiveDefaultFilter {
    return this.generateGetter(
        type: KkakkaFilterResponseModel, key: 'receiveddefault');
  }

  /// baron 2021-05-31
  /// get pagination cache data By [KkakkaModel]  with [listFilterPage]Function
  get cache_giveDefaultFilter {
    return this
        .generateGetter(type: KkakkaFilterResponseModel, key: 'givedefault');
  }

  /// baron 2021-05-31
  /// get pagination cache data By [KkakkaModel]  with [listFilterPage]Function
  get cache_receiveFilter {
    return this
        .generateGetter(type: KkakkaFilterResponseModel, key: 'receivedfilter');
  }

  /// baron 2021-05-31
  /// get pagination cache data By [KkakkaModel]  with [listFilterPage]Function
  get cache_giveFilter {
    return this
        .generateGetter(type: KkakkaFilterResponseModel, key: 'givefilter');
  }

  Future<SingleItemResponseDataModel<KkakkaModel>> sendKkakkaSuggest(
      PostKkakkaIssuanceSendBody kkakkaBody) async {
    final SingleItemResponseDataModel<KkakkaModel> resp = await super
        .services
        .kkakkaService
        .postSendKkakkaSuggest(kkakkaBody.toJson());
    return resp;
  }

  Future<String> requestKkakka(PostKkakkaRequestRequestBody kkakkaBody) async {
    final resp =
        await super.services.kkakkaService.postRequest(kkakkaBody.toJson());
    return resp.resultCode!;
  }

  // body에 들어가는 검색조건이 호출 할때 만다 변경 되여서 body를 update 해줍니다.
  // baron 2021-05-30
  Future<void> updateFilterBody(ListFilterPaginationBody body) async {
    filterBody = body;
    update();
  }

  // body에 들어가는 필터key를 update 해줍니다.
  // baron 2021-05-30
  Future<void> setFilterKey(String key) async {
    filterkey = key;
    update();
  }

  Future<List<KkakkaFilterResponseModel>> kkakkaListFilter({
    GeneralPaginationBody? body,
    bool reset = false,
    String key = 'default',
  }) async {
    final resp = await this.paginate<KkakkaFilterResponseModel>(
      request: this.services.kkakkaService.kkakkaListFilter,
      body: body ??= getFilterBody,
      reset: reset,
      key: filterkey!,
    );

    return resp;
  }

  Future<List<ListSendOpponentModel>> listSendOpponent() async {
    final postBody = {
      "direction": true,
      "page": 0,
      "size": 30,
      "sortBy": "id",
    };

    final resp = await services.kkakkaService.listSendOpponent(postBody);

    return resp.data.values;
  }

  Future<SumKkakkaModel> sumKkakka() async {
    final resp = await services.kkakkaService.sumKkakka();
    return resp.data;
  }

  signature({
    required int kkakkaId,
    required String signature,
  }) async {
    final resp = await this.services.kkakkaService.signature(
          SignatureBody(
            kkakkaId: kkakkaId,
            signature: signature,
          ),
        );

    return resp;
  }

  Future<dynamic> kkakkaDetail(int id) async {
    final postBody = {"kkakkaId": id};
    final resp = await services.kkakkaService.kkakkaDetail(postBody);

    return resp.data;
  }

  Future<DefaultModel> statusUpdate(int id, bool isSetActive) async {
    final body = {
      "activeYn": isSetActive ? 'Y' : 'N',
      "kkakkaId": id,
    };

    final resp = await services.kkakkaService.statusUpdate(body);

    return resp;
  }

  Future<GetRequestModel> getRequest(int kkakkaReqId) async {
    final body = {"kkakkaReqId": kkakkaReqId};

    final resp = await services.kkakkaService.getRequest(body);

    return resp.data;
  }

  Future<int> getReqKkakka() async {
    final resp = await services.kkakkaService.getReqKkakka({});

    return resp.data!;
  }

  get cache_listRequest {
    return this.generateGetter(type: ListRequestModel);
  }

  get getFilterBody {
    return filterBody;
  }
}
