import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/root_service.dart';
import 'package:kins_healthcare/services/suggest/model/suggest_detail_model.dart';
import 'package:kins_healthcare/services/suggest/model/suggest_user_body.dart';

class SuggestController extends BaseController {
  SuggestController({
    required RootService rootService,
  }) : super(rootService);

  Future<DefaultModel> suggestUser(int paramKkakkaNo, List<UserNo> paramUsers) async {
    final SuggestUserBody body =
        SuggestUserBody(kkakkaNo: paramKkakkaNo, users: paramUsers);

    final resp = await services.suggestService.suggestUser(body);

    return resp;
  }

  Future<SuggestDetailModel> suggestDetail(int suggestNo) async {
    Map<String, dynamic> body = {'suggestNo': suggestNo};
    final resp = await services.suggestService.suggestDetail(body);

    return resp.data;
  }
}
