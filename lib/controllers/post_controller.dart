/*
 작성일 : 2021-04-14
 작성자 : Mark,
 화면명 : (화면명),
 경로 :  /kinshealthcare-flutter/lib/controllers/post_controller.dart
 업데이트 :  baron 
 업데이트 날짜 : 2021-05-3
 클래스 : PostController,
 설명 : PostController Service 에 연결하고 View 에 Model 을 제공하는 Controller
 */

import 'dart:io';
import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/post/model/like_list_model.dart';
import 'package:kins_healthcare/services/post/model/like_update_model.dart';
import 'package:kins_healthcare/services/post/model/post_list_model.dart';
import 'package:kins_healthcare/services/post/model/post_register_body.dart';
import 'package:kins_healthcare/services/post/model/reply_list_model.dart';
import 'package:kins_healthcare/services/root_service.dart';

import 'base_controller.dart';

class PostController extends BaseController {
  PostController({
    required RootService rootService,
  }) : super(rootService);

  Future<DefaultModel> register(
    String? message,
    List<File>? files,
  ) async {
    final photos = await imageUpload(files);

    final postRegisterBody =
        PostRegisterBody(message: message, photos: photos, postType: 'A');

    return services.postService.register(postRegisterBody);
  }

// baron_20210503
  Future<List<Photo>> imageUpload(List<File>? files) async {
    if (files == null) return [];
    final resp = await this.services.uploadService.imageV1(files);
    List<Photo> temp = [];
    resp.data.values.forEach(
        (response) => temp.add(Photo(photo: response.uploadFilePath!)));
    // print('++++++++++++++++${temp[0]}');
    return temp;
  }

  Future<List<PostListModel>> getList({
    GeneralPaginationBody? body,
    bool reset = false,
  }) async {
    final resp = await this.paginate<PostListModel>(
      request: services.postService.list,
      body: body,
      reset: reset,
    );

    return resp;
  }

  get postList {
    return this.generateGetter(type: PostListModel);
  }

  Future<List<LikeListModel>> getLikeList({
    GeneralPaginationBody? body,
    bool reset = false,
    String key = 'default',
  }) async {
    final resp = await this.paginate<LikeListModel>(
      request: this.services.postService.getLikeList,
      body: body,
      reset: reset,
      key: key,
    );

    return resp;
  }

  Future<List<ReplyListModel>> getReplyList({
    GeneralPaginationBody? body,
    bool reset = false,
    String key = 'default',
  }) async {
    final resp = await this.paginate<ReplyListModel>(
      request: this.services.postService.getReplyList,
      body: body,
      reset: reset,
      key: key,
    );

    return resp;
  }

  Future<DefaultModel> insertReply(
    int boardNo,
    String boardType,
    String content,
  ) async {
    final body = {
      'boardNo': boardNo,
      'boardType': boardType,
      'content': content,
    };

    final resp = await this.services.postService.insertReply(body);
    return resp;
  }

  removeCacheList({required Type type}) {
    this.resetCache(type: type);
  }


  Future<SingleItemResponseModel<LikeUpdateModel>> updateLike(
    int boardNo,
    String boardType,
  ) async {
    final body = {
      "boardNo": boardNo,
      "boardType": boardType,
    };

    final resp = await this.services.postService.updateLike(body);
    return resp;
  }

  get postLikeList {
    return this.generateGetter(type: LikeListModel);
  }

  get postLikeToTal {
    final Type type = LikeListModel;
    return this.cache[type.toString() + '_default']?.total ?? 0;
  }

  get postToTal {
    final Type type = PostListModel;
    return this.cache[type.toString() + '_default']?.total ?? 0;
  }

  get postReplyList {
    return this.generateGetter(type: ReplyListModel);
  }

  get cache_postList {
    return this.generateGetter(type: PostListModel);
  }
}
