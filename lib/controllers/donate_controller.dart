import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/services/donate/post_donate_detail_models.dart';
import 'package:kins_healthcare/services/donate/post_donate_models.dart';
import 'package:kins_healthcare/services/kkakka/model/kkakka_request_models.dart';
import 'package:kins_healthcare/services/root_service.dart';

/*
 작성일 : 2021-02-23
 작성자 : phil
 화면명 :
 경로 :
 클래스 : DonateController
 설명 : 기부 기관 조회
*/

class DonateController extends BaseController {
  String appType = "H";
  int size = 10;
  List<PostDonateResponse> donations = [];
  PostDonateDetailResponse? donation;

  DonateController({
    required RootService rootService,
  }) : super(rootService);

  // 기부기관 조회 이전 스크린 컨트롤러(삭제 예정)
  Future<List<PostDonateResponse>> listDonations() async {
    final resp = await super.services.donateService.postDonationList(
          PostDonateBody(appType: this.appType),
        );

    this.donations = resp.data.values;

    update();
    return this.donations;
  }

  // 기부기관 조회 신규 스크린 컨트롤러
  Future<List<NewDonateResponse>> list({
    PostDonateBody? body,
    bool reset = false,
  }) async {
    final resp = await this.paginate<NewDonateResponse>(
      request: this.services.donateService.list,
      body: body,
      reset: reset,
    );
    return resp;
  }

  get regDonateList {
    return this.generateGetter(type: NewDonateResponse);
  }

  Future<PostDonateDetailResponse> detailDonate(int id) async {
    final resp = await super.services.donateService.postDonateDetail(
          PostDonateDetailBody(appType: this.appType, donationId: id),
        );

    this.donation = resp.data;

    update();
    return donation!;
  }

  Future<String> sendDonation(PostKkakkaDonateSendBody kkakkaBody) async {
    final resp = await super.services.donateService.sendDonation(kkakkaBody.toJson());
    return resp.resultCode!;
  }
}
