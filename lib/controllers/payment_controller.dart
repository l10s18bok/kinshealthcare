import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/services/comm/model/result_form_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/payment/model/payment_cancel_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_kkakka_list_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_list_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_payment_request_list_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_payment_request_reject_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_payment_request_status.dart';
import 'package:kins_healthcare/services/payment/model/payment_request_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_request_steps_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_stop_request_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_store_create_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_store_one_models.dart';
import 'package:kins_healthcare/services/root_service.dart';
import 'package:kins_healthcare/services/user/payment/model/bank_code.dart';
import 'package:kins_healthcare/services/user/payment/model/payment_register_account.dart';
import 'package:kins_healthcare/services/user/payment/model/payment_register_auth_confirm.dart';
import 'package:kins_healthcare/services/user/payment/model/payment_register_auth_request.dart';
import 'package:kins_healthcare/services/user/payment/model/payment_register_card.dart';
import 'package:kins_healthcare/utils/encryption/hight_ctr.dart';
import 'package:kins_healthcare/utils/text_utils.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class PaymentController extends BaseController {
  bool isQrScanning = false;
  PostPaymentStoreOneResponse? storeInfo;
  int amount = 0;
  int? setleId;
  String? setleStatus;
  int paymentStep = 0;
  int? setlePaymentId;
  String amountDisplay = '';
  List<PostPaymentKkakkaListResponse> kkakkas = [];

  /// 결제 직접 관련 변수들
  /// approveMe.kkakkas
  List<PaymentStepsKkakka> paymentBodyKkakkas = [];

  /// approveMe.localCurrencys
  List<PaymentStepsLocalCurrency> paymentBodyLocalCurrencies = [];

  /// approveMe.userPaymentWays
  List<PaymentStepsUserPaymentWay> paymentBodyUserPaymentWay = [];

  /// requestUser.toUsers
  List<PaymentStepsToUser> paymentBodyToUsers = [];

  /// approveMe.approveTotalAmount (내가 결제)
  int approveMeAmount = 0;

  /// requestUser.approveTotalAmount (다른 사람에게 요청)
  int requestUserAmount = 0;

  /// 결제 진행상태
  List<dynamic> paymentStatuses = [];

  PaymentController({
    required RootService rootService,
  }) : super(rootService);

  resetPaymentBodies() {
    this.paymentBodyKkakkas = [];
    this.paymentBodyLocalCurrencies = [];
    this.paymentBodyUserPaymentWay = [];
    this.paymentBodyToUsers = [];
    this.paymentStatuses = [];
    this.approveMeAmount = 0;
    this.requestUserAmount = 0;
    update();
  }

  setNStepPrerequisites({
    required int amount,
    required int step,
    required String address1,
    required String address2,
    required String comName,
    required int setlePaymentId,
    required int storeId,
  }) {
    this.amount = amount;
    this.amountDisplay = TextUtils().numberToLocalCurrency(
      amount: amount.toDouble(),
    );
    this.paymentStep = step;
    this.setlePaymentId = setlePaymentId;
    this.storeInfo = PostPaymentStoreOneResponse(
      address1: address1,
      address2: address2,
      comName: comName,
      id: storeId,
    );
    update();
  }

  setAmount(int amount) {
    this.amount = amount;
    this.amountDisplay = TextUtils().numberToLocalCurrency(amount: amount.toDouble());
    update();
  }

  setPaymentStep(int step) {
    this.paymentStep = step;
    update();
  }

  setSetlePaymentId(int id) {
    this.setlePaymentId = id;
    update();
  }

  getPaymentRequestList({
    GeneralPaginationBody? body,
    bool reset = false,
    String key = 'default',
  }) async {
    if (body == null) {
      body = GeneralPaginationBody(
        sortBy: 'id',
      );
    }

    final resp = this.paginate<PaymentRequestListResponse>(
      request: this.services.paymentService.paymentRequestList,
      body: body,
      reset: reset,
      key: key,
    );

    return resp;
  }

  get paymentRequestList {
    return this.generateGetter(
      type: PaymentRequestListResponse,
    );
  }

  rejectPaymentRequest({
    required int setlePaymentId,
  }) async {
    await this.services.paymentService.paymentRequestReject(
          PaymentRequestRejectBody(
            setlePaymentId: setlePaymentId,
          ),
        );

    this.removeItemFromCache(
      type: PaymentRequestListResponse,
      removeFunction: (element) => element.setlePaymentId == setlePaymentId,
    );
  }

  reset() {
    this.isQrScanning = false;
    this.storeInfo = null;
    this.amount = 0;
    update();
  }

  /// HP_1003 에서 가족에게 요청하기 클릭시
  /// 필수로 부르는 함수
  /// [amount] 는 내가 결제할 금액
  /// [requestAmount] 는 다른 사람에게 요청할 금액
  updateSelfPayments({
    // required int amount,
    required int requestAmount,
    required List<PaymentStepsKkakka> kkakkas,
    required List<PaymentStepsLocalCurrency> localCurrencies,
    required List<PaymentStepsUserPaymentWay> userPayments,
  }) {
    resetPaymentBodies();
    // this.approveMeAmount = amount;
    this.requestUserAmount = requestAmount;
    this.paymentBodyKkakkas = kkakkas;
    this.paymentBodyLocalCurrencies = localCurrencies;
    this.paymentBodyUserPaymentWay = userPayments;

    this.update();
  }

  cancelPayment() async {
    await this.services.paymentService.stop(
          PaymentStopBody(
            setleId: this.setleId!,
          ),
        );
  }

  /// 모자란금액 요청에서 다음 클릭시
  /// 필수로 부르는 함수
  updateRequestPayments({
    required List<PaymentStepsToUser> toUsers,
  }) {
    this.paymentBodyToUsers = toUsers;

    update();
  }

  Future<List<dynamic>> checkPaymentStatusN() async {
    if (this.setlePaymentId == null) {
      throw Exception('setlePaymentId 가 null 입니다.');
    }

    final resp = await this.services.paymentService.paymentRequestStatusStepN(
          PaymentRequestStatusStepNBody(
            setlePaymentId: this.setlePaymentId!,
          ),
        );

    this.setleStatus = resp.data.values.setleStatus;

    this.paymentStatuses = [
      ...resp.data.values.me,
      ...resp.data.values.request,
    ];
    update();

    return this.paymentStatuses;
  }

  /// PaymentRequestResponseMe || PaymentRequestResponseRequest
  Future<List<dynamic>> checkPaymentStatus() async {
    late SingleItemResponseModel<PaymentRequestStatusResponse> resp;

    if (this.paymentStep == 0) {
      resp = await this.services.paymentService.paymentRequestStatus(
            PaymentRequestStatusBody(
              setleId: this.setleId!,
            ),
          );
    } else {
      resp = await this.services.paymentService.paymentRequestStatusStepN(
            PaymentRequestStatusStepNBody(
              setlePaymentId: this.setlePaymentId!,
            ),
          );
    }

    this.setleStatus = resp.data.values.setleStatus;

    this.paymentStatuses = [
      ...resp.data.values.me,
      ...resp.data.values.request,
    ];
    update();

    return this.paymentStatuses;
  }

  makePaymentStepN() async {
    print('--------kkkakka---------');
    print(this.paymentBodyKkakkas.map((e) => '${e.kkakkaId}:${e.approveAmount}'));
    print('--------payment ways--------');
    print(this.paymentBodyUserPaymentWay.map((e) => '${e.userPaymentWayId}:${e.approveAmount}'));
    int meTotal = 0;
    int requestTotal = 0;

    if (this.paymentBodyKkakkas.length > 0) {
      meTotal += this.paymentBodyKkakkas.fold(
            0,
            (previousValue, element) => previousValue + element.approveAmount,
          );
    }

    if (this.paymentBodyLocalCurrencies.length > 0) {
      meTotal += this.paymentBodyLocalCurrencies.fold(
            0,
            (previousValue, element) => previousValue + element.approveAmount,
          );
    }

    if (this.paymentBodyUserPaymentWay.length > 0) {
      meTotal += this.paymentBodyUserPaymentWay.fold(
            0,
            (previousValue, element) => previousValue + element.approveAmount,
          );
    }

    if (this.paymentBodyToUsers.length > 0) {
      requestTotal += this.paymentBodyToUsers.fold(
            0,
            (previousValue, element) => previousValue + element.amount,
          );
    }

    GeneralPlainResponse? resp;

    if (this.paymentStep == 0) {
      resp = await this.services.paymentService.paymentRequestStep0(
            PaymentRequestSteps0Body(
              amount: this.amount,
              approveMe: PaymentStepsApproveMe(
                approveTotalAmount: meTotal,
                kkakkas: this.paymentBodyKkakkas.length == 0 ? null : this.paymentBodyKkakkas,
                localCurrencys: this.paymentBodyLocalCurrencies.length == 0 ? null : this.paymentBodyLocalCurrencies,
                userPaymentWays: this.paymentBodyUserPaymentWay.length == 0 ? null : this.paymentBodyUserPaymentWay,
              ),
              requestUser: PaymentStepsRequestUser(
                approveTotalAmount: requestTotal,
                toUsers: this.paymentBodyToUsers,
              ),
              setleId: this.setleId!,
            ),
          );
    } else if (this.paymentStep == 1) {
      resp = await this.services.paymentService.paymentRequestStep1(
            PaymentRequestSteps1Body(
              amount: this.amount,
              approveMe: PaymentStepsApproveMe(
                approveTotalAmount: meTotal,
                kkakkas: this.paymentBodyKkakkas.length == 0 ? null : this.paymentBodyKkakkas,
                localCurrencys: this.paymentBodyLocalCurrencies.length == 0 ? null : this.paymentBodyLocalCurrencies,
                userPaymentWays: this.paymentBodyUserPaymentWay.length == 0 ? null : this.paymentBodyUserPaymentWay,
              ),
              requestUser: PaymentStepsRequestUser(
                approveTotalAmount: requestTotal,
                toUsers: this.paymentBodyToUsers,
              ),
              setlePaymentId: this.setlePaymentId!,
            ),
          );
    } else if (this.paymentStep == 2) {
      resp = await this.services.paymentService.paymentRequestStep2(
            PaymentRequestSteps2Body(
              amount: this.amount,
              approveMe: PaymentStepsApproveMe(
                approveTotalAmount: meTotal,
                kkakkas: this.paymentBodyKkakkas.length == 0 ? null : this.paymentBodyKkakkas,
                localCurrencys: this.paymentBodyLocalCurrencies.length == 0 ? null : this.paymentBodyLocalCurrencies,
                userPaymentWays: this.paymentBodyUserPaymentWay.length == 0 ? null : this.paymentBodyUserPaymentWay,
              ),
              setlePaymentId: this.setlePaymentId!,
            ),
          );
    }

    return resp;
  }

  onAmountInput({String? val}) {
    if (val != null && val.length > 0) {
      this.amount = int.parse(val);

      final formatter = NumberFormat('#,###');
      this.amountDisplay = formatter.format(int.parse(val));
    } else {
      this.amount = 0;
      this.amountDisplay = '';
    }

    update();

    return this.amountDisplay;
  }

  Future<void> requestPayment() async {
    await super.services.paymentService.request(
          PostPaymentRequestBody(
            setleId: this.setleId,
          ),
        );
  }

  Future<void> createPayment() async {
    final resp = await super.services.paymentService.postPaymentCreate(
          PostPaymentCreateBody(
            storeId: this.storeInfo!.id,
            totalAmount: amount,
          ),
        );

    this.setleId = resp.data?.setleId;

    update();
  }

  Future<List<PostPaymentKkakkaListResponse>> listKkakkas() async {
    final resp = await super.services.paymentService.postKkakkaList(
          PostPaymentKkakkaListBody(storeId: this.storeInfo!.id!),
        );

    this.kkakkas = resp.data.values;

    update();

    return this.kkakkas;
  }

  forceSetBarcodeId(int id) async {
    final resp = await super.services.paymentService.postStoreOne(PostPaymentKkakkaListBody(storeId: id));

    this.storeInfo = resp.data.values;

    this.isQrScanning = false;

    this.paymentStep = 0;

    update();
  }

  onQrScan(Barcode barcode) async {
    if (this.isQrScanning) {
      return false;
    }

    try {
      isQrScanning = true;

      update();

      final id = barcode.code.split('=')[1].replaceAll("\"", "");

      final resp = await super.services.paymentService.postStoreOne(PostPaymentKkakkaListBody(storeId: int.parse(id)));

      this.storeInfo = resp.data.values;

      this.isQrScanning = false;

      update();

      return true;
    } catch (e) {
      // handle error
      this.isQrScanning = false;
      update();

      return false;
    }
  }

  bool isValid(String? value, {int length = 0}) {
    var isValid = value?.isNotEmpty ?? false;

    if (!isValid) {
      return false;
    }

    if (length > 1) {
      return value!.length == length;
    }

    return true;
  }

  Future<ResultFormModel?> registerCard({
    required String ownerName,
    required String privateNumber,
    required String cardNickname,
    required String companyName,
    required String cardNum,
    required String cardDateYear,
    required String cardDateMonth,
    required String password,
  }) async {
    //간단한 유효성 재검사
    if (!(isValid(ownerName) &&
        isValid(privateNumber) &&
        isValid(cardNickname) &&
        isValid(companyName) &&
        isValid(cardNum, length: 16) &&
        isValid(cardDateYear, length: 2) &&
        isValid(cardDateMonth, length: 2) &&
        isValid(password, length: 2))) {
      print('Error : wrong input parameter on registerCard');
      return null;
    }

    String userType;

    if (privateNumber.length == 6) {
      userType = 'PERSON';
    } else if (privateNumber.length == 10) {
      userType = 'CORPORATION';
    } else {
      print('Error : wrong input parameter on registerCard');
      return null;
    }
    //Hight ctr 암호화용 정보
    //String charset = "utf-8";
    String useKey = "HllSpnrMdKnspp8#";
    String vector = "ZrntFc9!";

    var key = HightCtr.stringToByteArray(useKey);
    var vec = HightCtr.stringToByteArray(vector);

    var hight = HightCtr();

    encrypt(String input) => base64Encode(hight.HIGHT_CTR_Encrypt(key, vec, HightCtr.stringToByteArray(input), 0, input.length));

    var cardNoEnc = encrypt(cardNum);
    var cardPwEnc = encrypt(password);
    var expireMMEnc = encrypt(cardDateMonth);
    var expireYYEnc = encrypt(cardDateYear);
    var privateEnc = encrypt(privateNumber);
    var ownerNameEnc = encrypt(ownerName);

    var params = PostRegisterCardBody(
      cardNoEnc: cardNoEnc,
      cardPwEnc: cardPwEnc,
      expireMMEnc: expireMMEnc,
      expireYYEnc: expireYYEnc,
      paymentAlias: cardNickname,
      paymentCorp: companyName,
      privateNoEnc: privateEnc,
      userNameEnc: ownerNameEnc,
      userType: userType,
    );

    final resp = await services.paymentManageService.postRegisterCard(params);

    return resp;
  }

  // 계좌등록 2021.04.13 Andy 추가
  Future<GeneralPlainResponse?> registerAccount({
    required String accountE,
    required String bankC,
    required String accountTradeIdE,
    required String orderN,
    required String paymentA,
  }) async {
    if (!(isValid(accountE) && isValid(bankC) && isValid(accountTradeIdE) && isValid(orderN) && isValid(paymentA))) {
      print('Error : wrong input parameter on registerAccount');
      return null;
    }

    String useKey = "HllSpnrMdKnspp8#";
    String vector = "ZrntFc9!";

    var key = HightCtr.stringToByteArray(useKey);
    var vec = HightCtr.stringToByteArray(vector);

    var hight = HightCtr();

    encrypt(String input) => base64Encode(hight.HIGHT_CTR_Encrypt(key, vec, HightCtr.stringToByteArray(input), 0, input.length));

    var accountEnc = encrypt(accountE);
    //var accountTradeIdEnc = encrypt(accountTradeIdE);
    // var backCode = encrypt(backC);
    // var orderNo = encrypt(orderN);
    // var paymentAlias = encrypt(paymentA);

    var params = PostRegisterAccountBody(
      accountEnc: accountEnc,
      accountTradeIdEnc: accountTradeIdE,
      bankCode: bankC,
      orderNo: orderN,
      paymentAlias: paymentA,
    );

    final resp = await services.paymentManageService.registerAccount(params);

    return resp;
  }

  /// 은행코드 가져오기
  Future<List<BankCodeResponse>> getBankCode() async {
    final code = await services.bankCodeService.getBankCode();
    return code.data;
  }

  /// 계좌등록 : ARS 요청전
  Future<PaymentRegAuthReqResponse?> authRequest({
    required String paymentA,
    required String accountE,
    required String bankC,
    required String privateNo,
    required String usrNameE,
  }) async {
    // if (!(isValid(paymentA) && isValid(accountE) && isValid(bankC) && isValid(privateNo) && isValid(usrNameE))) {
    //   print('paymentA : $paymentA   accountE : $accountE   backC : $bankC  privateNo : $privateNo   usrNameE $usrNameE');
    //   return null;
    // }

    String useKey = "HllSpnrMdKnspp8#";
    String vector = "ZrntFc9!";

    var key = HightCtr.stringToByteArray(useKey);
    var vec = HightCtr.stringToByteArray(vector);

    var hight = HightCtr();

    encrypt(String input) => base64Encode(hight.HIGHT_CTR_Encrypt(key, vec, HightCtr.stringToByteArray(input), 0, input.length));

    var accountEnc = encrypt(accountE);
    var privateNoEnc = encrypt(privateNo);
    var userNameEnc = encrypt(usrNameE);
    // var backCode = encrypt(bankC);
    // var paymentAlias = encrypt(paymentA);

    var params = PostRegAuthRequestBody(
      accountEnc: accountEnc,
      bankCode: bankC,
      paymentAlias: paymentA,
      privateNoEnc: privateNoEnc,
      userNameEnc: userNameEnc,
    );

    final resp = await services.paymentManageService.authRequest(params);

    if (resp.resultCode != 'SUCCESS') return null;

    return resp.data;
  }

  /// 계좌등록 : ARS 요청후
  Future<PaymentRegAutConfirmResponse?> authConfirm({
    required String bankC,
    required String orderN,
    required String sendD,
  }) async {
    if (!(isValid(bankC) && isValid(orderN) && isValid(sendD))) {
      print('Error : wrong input parameter on ');
      return null;
    }

    // String useKey = "HllSpnrMdKnspp8#";
    // String vector = "ZrntFc9!";

    // var key = HightCtr.stringToByteArray(useKey);
    // var vec = HightCtr.stringToByteArray(vector);

    // var hight = HightCtr();

    // encrypt(String input) => base64Encode(hight.HIGHT_CTR_Encrypt(key, vec, HightCtr.stringToByteArray(input), 0, input.length));

    // var backCode = encrypt(backC);
    // var orderNo = encrypt(orderN);
    // var sendDt = encrypt(sendD);

    var params = PostRegAuthConfirmBody(
      bankCode: bankC,
      orderNo: orderN,
      sendDt: sendD,
    );

    final resp = await services.paymentManageService.authConfirm(params);

    return resp.data;
  }

  Future<List<PaymentListModel>> getPaymentList({
    GeneralPaginationBody? body,
    bool reset = false,
  }) async {
    final resp = await this.paginate<PaymentListModel>(
      request: this.services.paymentService.paymentList,
      body: body,
      reset: reset,
    );

    return resp;
  }

  Future<PaymentCancelModel> paymentCancelRequest(int id) async {
    final body = {"setleId": id};
    final model = await services.paymentService.paymentCancelRequest(body);

    return model.data;
  }

  List get paymentList {
    return this.generateGetter(type: PaymentListModel);
    // if (this.cache.containsKey(PaymentListModel)) {
    //   return this.cache[PaymentListModel]!.items;
    // } else {
    //   return [];
    // }
  }
}
