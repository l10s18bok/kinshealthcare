import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/force/model/event_detail_model.dart';
import 'package:kins_healthcare/services/force/model/event_process_model.dart';
import 'package:kins_healthcare/services/force/model/event_under_model.dart';
import 'package:kins_healthcare/services/root_service.dart';

/// /force 라우트 관련 모든 기능을 구현한 컨트롤러
///
/// - 작성자 : JC
/// - 작성일 : 2021-05-01
///
/// {@category Controller}
class ForceController extends BaseController {
  ForceController({
    required RootService rootService,
  }) : super(rootService);

  forceEventProcess({
    required int eventParam,
    required bool isCheck,
  }) async {
    await this.services.forceService.postEventProcess(
          EventProcessBody(
            eventParam: eventParam,
            processType: isCheck ? 'CHECK' : 'IGNORE',
          ),
        );
  }

  forceEventDetail({
    required String eventParam,
  }) async {
    final resp = await this.services.forceService.postEventDetail(
          EventDetailBody(eventParam: eventParam),
        );

    return resp.data;
  }

  forceEventUnder({
    GeneralPaginationBody? body,
    bool reset = false,
  }) async {
    final resp = await this.paginate<EventUnderResponse>(
      request: this.services.forceService.postEventUnder,
      body: body,
      reset: reset,
    );

    return resp;
  }

  List<dynamic> get events => this.generateGetter(
        type: EventUnderResponse,
      );
}
