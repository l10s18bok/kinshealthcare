import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/services/login/model/find_id_model.dart';
import 'package:kins_healthcare/services/login/model/find_password_model.dart';
import 'package:kins_healthcare/services/root_service.dart';

class LoginController extends BaseController {
  LoginController({
    required RootService rootService,
  }) : super(rootService);

  // TODO API 에서 메세지 보내주는 형태로 변경되면 snackbar로 메세지 받을 수 있도록 수정하기
  findId({
    required FindIdBody body,
  }) async {
    final resp = await services.loginService.findId(body);

    return resp;
  }

  // TODO API 에서 메세지 보내주는 형태로 변경되면 snackbar로 메세지 받을 수 있도록 수정하기
  findPassword({
    required FindPasswordBody body,
  }) async {
    final resp = await services.loginService.findPassword(body);

    return resp;
  }
}
