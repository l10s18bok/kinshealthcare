import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/services/cert/model/simple_password_certify_model.dart';
import 'package:kins_healthcare/services/cert/model/user_certify_models.dart';
import 'package:kins_healthcare/services/register/model/reg_ok_sms_model.dart';
import 'package:kins_healthcare/services/root_service.dart';

class CertController extends BaseController {
  CertController({
    required RootService rootService,
  }) : super(rootService);

  userCertify({
    required UserCertifyBody body,
  }) async {
    final resp = await this.services.certService.userCertify(body);

    return resp.resultCode;
  }

  simplepwCertify({
    required SimplePwCertifyBody body,
  }) async {
    final resp = await this.services.certService.simplepwCertify(body);

    return resp.resultCode;
  }

  Future<RegOkSmsResponse> regChildOkSmS({
    required RegOkSmsBody body,
  }) async {
    final resp = services.certService.regChildOkSms(body);
    return resp;
  }
}
