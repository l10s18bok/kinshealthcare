import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/models/timeline.dart';
import 'package:kins_healthcare/services/comm/model/board_model.dart';
import 'package:kins_healthcare/services/comm/model/board_one_model.dart';
import 'package:kins_healthcare/services/comm/model/cert_ok_email_model.dart';
import 'package:kins_healthcare/services/comm/model/cert_ok_sms_model.dart';
import 'package:kins_healthcare/services/comm/model/cert_request_email_model.dart';
import 'package:kins_healthcare/services/comm/model/cert_request_sms_model.dart';
import 'package:kins_healthcare/services/comm/model/result_form_model.dart';
import 'package:kins_healthcare/services/comm/model/timeline_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/mykins/model/inquiry_list_model.dart';
import 'package:kins_healthcare/services/root_service.dart';

/*
 작성일 : 2021-02-08
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : CommController,
 설명 : 공통 Controller Service 에 연결하고 View 에 Model 을 제공하는 Controller
*/
class CommController extends BaseController {
  CommController({
    required RootService rootService,
  }) : super(rootService);

  Future<List<InquiryListModel>> postInquiryNoAnswerList() async {
    final model = await services.commService.inquiryNoanwser();
    final questionModel = model.data.values;
    return questionModel;
  }

  Future<List<BoardModel>> postBoardList(String id) async {
    final model = await services.commService.boardList(id);
    final lists = model.data.values;
    return lists;
  }

  Future<SingleItemResponseModel<BoardOneModel>> postBoardOne(String id, int boardId) {
    final postBody = {"id": boardId};
    return services.commService.boardOne(id, postBody);
  }

  Future<ResultFormModel> postVerifyEmail(String mail) async {
    final postBody = {'email': mail};
    return await services.commService.postRequestEmailVerification(postBody);
  }

  Future<ResultFormModel> postOkEmail(String mail, String number) async {
    final postBody = {'certText': number, 'email': mail};
    return await services.commService.postOkEmailVerification(postBody);
  }

  Future<ResultFormModel> postVerifySMS(String phone) async {
    final postBody = {'phone': phone};
    return await services.commService.postRequestSMSVerification(postBody);
  }

  Future<ResultFormModel> postOkSMS(String phone, String number) async {
    final postBody = {'certText': number, 'phone': phone};
    return await services.commService.postOkSMSVerification(postBody);
  }

  Future<List<TimelineModel>> postTimelineList() async {
    final resp = await services.commService.timelineList(TimelineListBody());

    return resp.data;
  }

  certRequestEmail({
    required CertRequestEmailBody body,
  }) {
    return services.commService.certRequestEmail(body);
  }

  certOkEmail({
    required CertOkEmailBody body,
  }) {
    return services.commService.certOkEmail(body);
  }

  certRequestSms({
    required CertRequestSmsBody body,
  }) {
    return services.commService.certRequestSms(body);
  }

  certOkSmS({
    required CertOkSmsBody body,
  }) {
    return services.commService.certOkSms(body);
  }
}
