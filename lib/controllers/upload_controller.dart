import 'dart:io';

import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/services/root_service.dart';

class UploadController extends BaseController {
  bool isLoading = false;
  double sendProgress = 0.0;

  UploadController({
    required RootService rootService,
  }) : super(rootService);

  Future<String> uploadImage({
    required File image,
  }) async {
    try {
      isLoading = true;
      sendProgress = 0.0;
      update();

      final resp = await this.services.uploadService.imageV1([image]);

      this.isLoading = false;
      this.sendProgress = 0.0;
      update();

      return resp.data.values[0].uploadFilePath!;
    } catch (e) {
      this.isLoading = false;
      this.sendProgress = 0.0;
      update();

      rethrow;
    }
  }
}
