import 'package:get/get.dart';
import 'package:kins_healthcare/services/common/base_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/common/general_response_model.dart';
import 'package:kins_healthcare/services/root_service.dart';
import 'package:kins_healthcare/utils/storage_utils.dart';

typedef Future<ListResponseModel<BaseRetrofitModel>> PaginationRequestTypedef(
  GeneralPaginationBody body,
);

class PaginationCache {
  final GeneralPaginationBody body;
  final List items;
  final bool hasMore;
  final int total;

  PaginationCache({
    required this.body,
    required this.items,
    this.hasMore = true,
    this.total = 0,
  });
}

abstract class BaseController extends GetxController {
  /// 네트워크 서비스
  final RootService services;
  final StorageUtils storageUtils = StorageUtils();

  final Map<String, PaginationCache> cache = {};

  BaseController(
    RootService rootService,
  ) : this.services = rootService;

  resetCache({
    required Type type,
    String key = 'default',
  }) {
    if (this.cache.containsKey(type.toString() + '_$key')) {
      this.cache.remove(type.toString() + '_$key');
    } else {
      throw Exception(
          'cannot find cache items with provided key : ${type}_$key');
    }
    update();
  }

  removeItemFromCache({
    required Type type,
    required bool Function(dynamic) removeFunction,
    String key = 'default',
  }) {
    final cacheKey = type.toString() + '_$key';

    if (this.cache.containsKey(cacheKey)) {
      this.cache[cacheKey]!.items.removeWhere(removeFunction);
    }

    update();
  }

  getTotal({
    required Type type,
    String key = 'default',
  }) {
    if (this.cache.containsKey(type.toString() + '_$key')) {
      return this.cache[type.toString() + '_$key']!.total;
    } else {
      return 0;
    }
  }

  generateGetter({
    required Type type,
    String key = 'default',
  }) {
    if (this.cache.containsKey(type.toString() + '_$key')) {
      return this.cache[type.toString() + '_$key']!.items;
    } else {
      return [];
    }
  }

  paginate<T extends BaseRetrofitModel>({
    required PaginationRequestTypedef request,
    GeneralPaginationBody? body,
    bool reset = false,
    String key = 'default',
  }) async {
    // 바디 재구성하기
    if (body == null) {
      final typeCache = this.cache.containsKey(T.toString() + '_$key')
          ? this.cache[T.toString() + '_$key']
          : null;

      body = typeCache == null ? GeneralPaginationBody() : typeCache.body;
    }

    if (reset) {
      body = body.copyWith(
        page: 0,
      );
    } else {
      body = body.copyWith(
        page: body.page + 1,
      );
    }

    List<T> prevCache = this.cache.containsKey(T.toString() + '_$key')
        ? List<T>.from(
            this.cache[T.toString() + '_$key']!.items,
          )
        : [];
    List<T> newCache = [];

    final resp = await request(body!);

    final List<T> data = resp.data.values as List<T>;

    newCache = data;

    // 리스트 캐시부터 업데이트
    if (reset) {
      cache[T.toString() + '_$key'] = PaginationCache(
        body: body,
        items: newCache,
        hasMore: data.length == body.size,
        total: resp.data.total!,
      );
    } else {
      cache[T.toString() + '_$key'] = PaginationCache(
        body: body,
        items: [
          ...prevCache,
          ...newCache,
        ],
        hasMore: data.length == body.size,
        total: resp.data.total!,
      );
    }

    update();

    return newCache;
  }
}
