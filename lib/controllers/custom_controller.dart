import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/custom/model/custom_detail_model.dart';
import 'package:kins_healthcare/services/custom/model/custom_list_model.dart';
import 'package:kins_healthcare/services/custom/model/like_register_model.dart';
import 'package:kins_healthcare/services/root_service.dart';
import 'base_controller.dart';

/*
 작성일 : 2021-03-31
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : BannerController,
 설명 : 광고 정보를 받아오기 위한 Controller
*/

class CustomController extends BaseController {
  CustomController({
    required RootService rootService,
  }) : super(rootService);

  List<CustomListModel> customList = [];

  Future<List<CustomListModel>?> getList({
    required int page,
  }) async {
    final body = {
      "direction": true,
      "itemTitle": "",
      "itemType": "F",
      "page": page,
      "size": 10,
      "sortBy": "late"
    };

    if (page == 0) customList.clear();

    final resp = await services.customService.list(body);
    final values = resp.data.values;
    if (values.length == 0) return null;

    customList.addAll(values);
    return customList;
  }

  Future<CustomDetailModel> detail(int itemNo) async {
    final body = {"itemNo": itemNo};
    final resp = await services.customService.detail(body);
    return resp.data;
  }

  Future<LikeRegisterModel> likeRegister(int itemNo) async {
    final body = {"itemNo": itemNo};
    final resp = await services.customService.likeRegister(body);
    return resp.data;
  }

  Future<DefaultModel> likeDelete(int itemNo, int likeNo) async {
    final body = {
      "itemNo": itemNo,
      "likeNo": likeNo,
    };
    final resp = await services.customService.likeDelete(body);
    return resp;
  }
}
