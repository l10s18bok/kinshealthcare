import 'package:get/get.dart';
import 'package:kins_healthcare/models/local/local_model.dart';

class LocalController extends GetxController {
  LocalModel localModel = LocalModel();

  updateUser(String test) {
    // print("변경 전 " + localModel.nickName);
    this.localModel.nickName = test;
    print("변경 후 " + this.localModel.nickName!);
  }
}
