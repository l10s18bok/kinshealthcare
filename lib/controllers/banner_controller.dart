import 'package:kins_healthcare/services/banner/model/banner_list_model.dart';
import 'package:kins_healthcare/services/root_service.dart';
import 'base_controller.dart';

/*
 작성일 : 2021-03-31
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : BannerController,
 설명 : 광고 정보를 받아오기 위한 Controller
*/

class BannerController extends BaseController {
  BannerController({
    required RootService rootService,
  }) : super(rootService);

  Future<List<BannerListModel>> getList() async {
    final body = {
      "bannerType": "CF",
      "direction": true,
      "page": 0,
      "size": 10,
      "sortBy": "last"
    };

    final resp = await services.bannerService.list(body);

    return resp.data.values;
  }
}
