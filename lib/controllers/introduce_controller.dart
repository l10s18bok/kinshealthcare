import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/common/default_string_model.dart';
import 'package:kins_healthcare/services/introduce/model/introduce_relation_model.dart';
import 'package:kins_healthcare/services/introduce/model/introduce_requester_model.dart';
import 'package:kins_healthcare/services/root_service.dart';

import 'base_controller.dart';

/*
 작성일 : 2021-04-20
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : IntroduceController,
 설명 : 소개하기 Controller Service 에 연결하고 View 에 Model 을 제공하는 Controller
*/

class IntroduceController extends BaseController {
  IntroduceController({
    required RootService rootService,
  }) : super(rootService);

  Future<DefaultModel> request(int userNo, int userTargetNo) async {
    final body = {
      "userNoRel": userNo,
      "userNoTarget": userTargetNo,
    };

    final model = await this.services.introduceService.request(body);
    return model;
  }

  Future<IntroduceRelationModel> relation(int introduceId) async {
    final body = {"introduceReqId": introduceId};

    final model = await this.services.introduceService.relation(body);
    return model.data;
  }

  Future<DefaultModel> message(int introduceId, String message) async {
    final body = {
      "introduceReqId": introduceId,
      "introduceMsg": message,
    };

    final model = await this.services.introduceService.message(body);
    return model;
  }

  Future<DefaultModel> approve(int introduceId) async {
    final body = {"introduceReqId": introduceId};

    final model = await this.services.introduceService.approve(body);
    return model;
  }

  Future<DefaultStringModel> phone(int introduceId) async {
    final body = {"introduceReqId": introduceId};

    final model = await this.services.introduceService.phone(body);
    return model;
  }

  Future<IntroduceRequesterModel> requester(int introduceId) async {
    final body = {"introduceReqId": introduceId};

    final model = await this.services.introduceService.requester(body);
    return model.data;
  }
}
