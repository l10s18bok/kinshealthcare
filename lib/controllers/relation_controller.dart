import 'package:dio/dio.dart';
import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/services/comm/model/result_form_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/relation/model/drop_models.dart';
import 'package:kins_healthcare/services/relation/model/know_together_model.dart';
import 'package:kins_healthcare/services/relation/model/post_relation_search_models.dart';
import 'package:kins_healthcare/services/relation/model/reject_models.dart';
import 'package:kins_healthcare/services/relation/model/request_models.dart';
import 'package:kins_healthcare/services/relation/model/search_models.dart';
import 'package:kins_healthcare/services/relation/model/search_rel_req_models.dart';
import 'package:kins_healthcare/services/relation/model/search_user_by_phone_models.dart';
import 'package:kins_healthcare/services/relation/relation_require_opponent_model.dart';
import 'package:kins_healthcare/services/root_service.dart';

/*
 작성일 : 2021-02-08
 작성자 : phil
 화면명 :
 경로 :
 클래스 : RelationController
 설명 : 관계 조회
*/

class RelationController extends BaseController {
  List<RelationReqOpponentResponse> relationRequireOpponents = [];
  List<SearchRelReqResponse> relationReq = [];
  List<SearchUserByPhoneResponse> userDataByPhone = [];
  List<SearchResponse> searchResponses = [];

  RelationController({
    required RootService rootService,
  }) : super(rootService);

  Future<ResultFormModel> relationApprove(int userNoOpponent) async {
    try {
      this.removeItemFromCache(
        type: RelationReqOpponentResponse,
        removeFunction: (opp) => opp.userNo == userNoOpponent,
      );

      final postBody = {'userNoOpponent': userNoOpponent};

      final resp = await services.relationService.postApprove(postBody);

      update();

      return resp;
    } catch (e) {
      rethrow;
    }
  }

  drop({required int userNoOpponent}) async {
    try {
      this.removeItemFromCache(
        type: SearchRelResponse,
        removeFunction: (opp) => opp.userNoRel == userNoOpponent,
      );

      await this.services.relationService.drop(
            DropBody(userNoOpponent: userNoOpponent),
          );
      update();
    } catch (e) {
      rethrow;
    }
  }

  // Future<List<SearchRelResponse>> listRelations() async {
  //   final resp = await super.services.relationService.searchRel(
  //         PostRelationSearchBody(),
  //       );
  //
  //   this.relations = resp.data.values;
  //
  //   update();
  //
  //   return this.relations;
  // }

  Future<List<SearchRelReqResponse>> searchRelReq({
    GeneralPaginationBody? body,
    bool reset = true,
    String key = 'default',
  }) async {
    final resp = await super.paginate<SearchRelReqResponse>(
      request: super.services.relationService.searchRelReq,
      body: body,
      reset: reset,
      key: key,
    );
    // final resp = await super.services.relationService.searchRelReq(body);

    this.relationReq = resp;

    update();

    return this.relationReq;
  }

  get searchRelReqResults {
    return this.generateGetter(
      type: SearchRelReqResponse,
    );
  }

  Future<List<RelationReqOpponentResponse>> listRelationReqOpponents({
    GeneralPaginationBody? body,
    bool reset = false,
  }) async {
    final resp = await this.paginate<RelationReqOpponentResponse>(
      request: this.services.relationService.relationReqOpponent,
      body: body,
      reset: reset,
    );

    return resp;
  }

  Future<List<KnowTogetherListResponse>> knowTogether({
    KnowTogetherListBody? body,
    bool reset = false,
  }) async {
    final resp = await this.paginate<KnowTogetherListResponse>(
      request: this.services.relationService.knowTogether,
      body: body,
      reset: reset,
    );

    return resp;
  }

  Future<List<SearchRelResponse>> searchRel({
    GeneralPaginationBody? body,
    bool reset = false,
  }) async {
    final resp = await this.paginate<SearchRelResponse>(
      request: this.services.relationService.searchRel,
      body: body,
      reset: reset,
    );

    update();
    return resp;
  }

  Future<List<SearchRelResponse>> searchRelSend({
    GeneralPaginationBody? body,
    bool reset = false,
  }) async {
    final resp = await this.paginate<SearchRelResponse>(
      request: this.services.relationService.searchRelSend,
      body: body,
      reset: reset,
    );

    update();
    return resp;
  }

  reject(int userId) async {
    final resp = await this.services.relationService.reject(
          RejectBody(
            userNoOpponent: userId,
          ),
        );

    this.cache['SearchRelReqResponse_default']!.items.removeWhere(
          (element) => element.userNoRel == userId,
        );

    update();

    return resp;
  }

  get relationsSend {
    return this.generateGetter(type: SearchRelResponse);
    // if (this.cache.containsKey(SearchRelResponse)) {
    //   return this.cache[SearchRelResponse]!.items;
    // } else {
    //   return [];
    // }
  }

  get relations {
    return this.generateGetter(type: SearchRelResponse);
    // if (this.cache.containsKey(SearchRelResponse)) {
    //   return this.cache[SearchRelResponse]!.items;
    // } else {
    //   return [];
    // }
  }

  get relationReqOpponents {
    return this.generateGetter(type: RelationReqOpponentResponse);
    // if (this.cache.containsKey(RelationReqOpponentResponse)) {
    //   return this.cache[RelationReqOpponentResponse]!.items;
    // } else {
    //   return [];
    // }
  }

    get knowTogetherList {
    return this.generateGetter(
      type: KnowTogetherListResponse,
    );
  }

  request({
    required int userNoRel,
    required String userRelCdOpponent,
  }) async {
    final resp = await this.services.relationService.request(
          RequestBody(
            userNoRel: userNoRel,
            userRelCdOpponent: userRelCdOpponent,
          ),
        );

    return resp;
  }

  modify({
    required int userNoRel,
    required String userRelCdOpponent,
  }) async {
    final resp = await this.services.relationService.modify(
          RequestBody(
            userNoRel: userNoRel,
            userRelCdOpponent: userRelCdOpponent,
          ),
        );

    return resp;
  }

  searchUserByPhone({
    required SearchUserByPhoneBody body,
  }) async {
    try {
      this.userDataByPhone = [];
      update();

      final resp = await this.services.relationService.searchUserByPhone(body);

      this.userDataByPhone = resp.data.values;
      update();

      return resp.data.values;
    } on DioError catch (_) {
      this.userDataByPhone = [];
      update();

      rethrow;
    } catch (_) {
      this.userDataByPhone = [];
      update();

      rethrow;
    }
  }

  Future<List<SearchResponse>> search() async {
    this.searchResponses = [];
    update();

    final resp = await this.services.relationService.search();

    this.searchResponses = resp.data.values;
    update();

    return resp.data.values;
  }
}
