import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/emergency/model/post_hosptial_detail_model.dart';
import 'package:kins_healthcare/services/emergency/model/post_hosptial_share_list_model.dart';
import 'package:kins_healthcare/services/emergency/model/post_hosptial_share_set_model.dart';
import 'package:kins_healthcare/services/emergency/model/post_search_list_model.dart';
import 'package:kins_healthcare/services/root_service.dart';

/*
 작성일 : 2021-02-24
 작성자 : Andy
 화면명 :
 경로 :
 클래스 : EmergencyController
 설명 : 병원관련
*/

class EmergencyController extends BaseController {
  EmergencyController({
    required RootService rootService,
  }) : super(rootService);

  listRegHosptial({
    GeneralPaginationBody? body,
    bool reset = false,
  }) async {
    await this.paginate<PostSearchListResponse>(
      request: this.services.emergencyService.postRequestList,
      body: body,
      reset: reset,
      key: 'EmHome',
    );
  }

  get regHosptialList {
    return this.generateGetter(type: PostSearchListResponse, key: 'EmHome');
  }

  findHosptialList({
    PostSearchListBody? body,
    bool reset = false,
  }) async {
    await this.paginate<PostSearchListResponse>(
      request: this.services.emergencyService.postFindList,
      body: body,
      reset: reset,
    );
  }

  get postSearchList {
    return this.generateGetter(
      type: PostSearchListResponse,
    );
  }

  hosptialShareDelete({required PostOnlyHsptlIdBody reqBody}) async {
    try {
      this.removeItemFromCache(
        type: PostSearchListResponse,
        key: 'EmHome',
        removeFunction: (opp) => opp.hsptlId == reqBody.hsptlId,
      );
    } catch (e) {
      rethrow;
    }

    await super.services.emergencyService.postShareDelete(reqBody.toJson());
    update();
  }

  Future<PostHosptialDetailResponse> detailHosptial({required PostOnlyHsptlIdBody body}) async {
    final resp = await super.services.emergencyService.postDetail(body);
    return resp.data;
  }

  Future<List<PostHosptialShareListResponse>> listHosptialShare({required int hsptlId}) async {
    final reqBody = PostHosptialShareListBody(hsptlId: hsptlId);
    final resp = await super.services.emergencyService.postShareDetail(reqBody.toJson());
    if (resp.isSuccess) {
      return resp.data.values;
    }
    return [];
  }

  Future<PostOnlySuccessResponse> hosptialShareRegist({required PostHosptialShareSetBody reqBody}) async {
    final resp = await super.services.emergencyService.postShareRegist(reqBody.toJson());
    return resp;
  }

  Future<PostOnlySuccessResponse> hosptialShareUpdate({required PostHosptialShareSetBody reqBody}) async {
    final resp = await super.services.emergencyService.postShareUpdate(reqBody.toJson());
    return resp;
  }
}
