import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/root_service.dart';
import 'package:kins_healthcare/services/timeline/model/timeline_list_model.dart';

import 'base_controller.dart';

/*
 작성일 : 2021-03-15
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : TimelineController,
 설명 : 타임라인 Controller Service 에 연결하고 View 에 Model 을 제공하는 Controller
*/

class TimelineController extends BaseController {
  TimelineController({
    required RootService rootService,
  }) : super(rootService);

  Future<List<TimelineListModel>> getList({
    GeneralPaginationBody? body,
    bool reset = false,
  }) async {
    final resp = await this.paginate<TimelineListModel>(
      request: services.timelineService.list,
      body: body,
      reset: reset,
    );

    return resp;
  }

  Future<DefaultModel> check(int timelineId) {
    return services.timelineService.check({"timelineId": timelineId});
  }

  get timelineList {
    return this.generateGetter(type: TimelineListModel);
    // if (this.cache.containsKey(TimelineListModel)) {
    //   return this.cache[TimelineListModel]!.items;
    // } else {
    //   return [];
    // }
  }
}
