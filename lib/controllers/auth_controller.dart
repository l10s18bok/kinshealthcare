import 'dart:async';

import 'package:kins_healthcare/controllers/controllers.dart';
import 'package:kins_healthcare/models/models.dart';
import 'package:kins_healthcare/services/auth/model/post_oauth_token_models.dart';
import 'package:kins_healthcare/services/root_service.dart';
import 'package:kins_healthcare/utils/storage_utils.dart';

/// /auth 라우트 관련 모든 기능을 구현한 컨트롤러
///
/// - 작성자 : 여러명
/// - 작성일 : 2021-05-01
///
/// {@category Controller}
class AuthController extends BaseController {
  UserModel? currentUser;

  AuthController({
    required RootService rootService,
  }) : super(rootService);

  logoutUser() async {
    await this.storageUtils.deleteByKey(key: 'accessToken');
    await this.storageUtils.deleteByKey(key: 'refreshToken');
  }

  /// [email] 과 [password] 를 String 으로 받음 리턴값은 아래
  loginUser({
    required String email,
    required String password,
  }) async {
    final body = AccessTokenBody(
      username: email,
      password: password,
      scope: 'read write',
    );

    final resp = await super.services.authService.postToken(body);

    await this
        .storageUtils
        .writeByKey(key: 'accessToken', value: resp.accessToken!);
    await this
        .storageUtils
        .writeByKey(key: 'refreshToken', value: resp.refreshToken!);

    return resp;
  }

  Future<PostTokenResponse> refreshToken() async {
    final utils = StorageUtils();

    final token = await utils.readByKey(key: 'refreshToken');

    if (token == null || token.length == 0) {
      throw Exception('token not found');
    }

    final body = RefreshTokenBody(refreshToken: token);
    final resp = await this.services.authService.postToken(body);

    await utils.writeByKey(key: 'accessToken', value: resp.accessToken!);
    await utils.writeByKey(key: 'refreshToken', value: resp.refreshToken!);

    return resp;
  }
}
