import 'package:kins_healthcare/services/common/default_model.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/mykins/model/child_profile_update_body.dart';
import 'package:kins_healthcare/services/mykins/model/division_list_body.dart';
import 'package:kins_healthcare/services/mykins/model/division_model.dart';
import 'package:kins_healthcare/services/mykins/model/family_list_model.dart';
import 'package:kins_healthcare/services/mykins/model/getUserInfo_response.dart';
import 'package:kins_healthcare/services/mykins/model/get_anniversary_models.dart';
import 'package:kins_healthcare/services/mykins/model/get_exp_models.dart';
import 'package:kins_healthcare/services/mykins/model/inquiry_list_model.dart';
import 'package:kins_healthcare/services/mykins/model/insert_anniversary_body.dart';
import 'package:kins_healthcare/services/mykins/model/notification_model.dart';
import 'package:kins_healthcare/services/mykins/model/post_only_child_id_body.dart';
import 'package:kins_healthcare/services/mykins/model/search_my_children_models.dart';
import 'package:kins_healthcare/services/root_service.dart';
import 'package:kins_healthcare/services/user/model/register_model.dart';
import 'package:kins_healthcare/services/user/model/user_info_models.dart';

import 'base_controller.dart';

/*
 작성일 : 2021-03-10
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : MykinsController,
 설명 : 마이킨즈 Controller Service 에 연결하고 View 에 Model 을 제공하는 Controller
*/

class MykinsController extends BaseController {
  List<GetAnniversaryResponse> anniversaries = [];
  List<InquiryListModel>? inquiryList;

  // 2021.06.04 Andy add
  UserInfoResponse? myChildInfo;

  GetExpResponse? exp;

  MykinsController({
    required RootService rootService,
  }) : super(rootService);

  Future<DefaultModel> updateNotification(
    String kkakkaYn,
    String payYn,
    String anniversaryYn,
    String balanceYn,
    String? balanceAmount,
    String selfKkakkaYn,
    String? selfKkakkaDay,
    String mediKkakkaYn,
    String? mediKkakkaDay,
    String eventYn,
    String disturbYn,
    String? noDisturbFrom,
    String? noDisturbTo,
    String? vibratYn,
  ) {
    final postBody = {
      "kkakkaYn": kkakkaYn,
      "payYn": payYn,
      "anniversaryYn": anniversaryYn,
      "balanceYn": balanceYn,
      "balanceAmount": balanceAmount,
      "selfKkakkaYn": selfKkakkaYn,
      "selfKkakkaDay": selfKkakkaDay,
      "mediKkakkaYn": mediKkakkaYn,
      "mediKkakkaDay": mediKkakkaDay,
      "eventYn": eventYn,
      "vibratYn": vibratYn,
      "disturbYn": disturbYn,
      "noDisturbFrom": noDisturbFrom,
      "noDisturbTo": noDisturbTo,
    };

    return services.mykinsService.updateNotification(postBody);
  }

  Future<NotificationModel> postNotification() async {
    final model = await services.mykinsService.getNotification();
    return model.data.values;
  }

  getAnniversary() async {
    final resp = await services.mykinsService.getAnniversary();

    this.anniversaries = resp.data.values;

    update();

    return resp.data.values;
  }

  Future<List<InquiryListModel>> postInquiryList() async {
    final model = await services.mykinsService.inquiryList();

    this.inquiryList = model.data.values;
    update();
    return model.data.values;
  }

  Future<DefaultModel> postInsertInquiry(
    String title,
    String name,
    String content,
    String password,
    String useAt,
  ) {
    final postBody = {
      "title": title, //제목
      "name": name,
      "content": content, //내용
      "password": password,
      "use_at": useAt //YN
    };

    return services.mykinsService.insertInquiry(postBody);
  }

  Future<List<DivisionModel>> getDivision() async {
    final data = await services.mykinsService.getDivision();
    return data.data.values;
  }

  Future<List<FamilyListModel>> getFamilyList() async {
    final data = await services.mykinsService.getFamilyList();
    return data.data.values;
  }

  Future<DefaultModel> updateDivision(List<DivisionBody> list) async {
    final data = await services.mykinsService.updateDivision(DivisionListBody(
      divisionList: list,
    ));
    return data;
  }

  Future<GetUserInfoResponse> getUserInfo() async {
    final resp = await services.mykinsService.getUserInfo();
    return resp.data.values;
  }

  Future<DefaultModel> insertAnniversary(InsertAnniversaryBody body) async {
    final resp = await services.mykinsService.insertAnniversary(body);
    return resp;
  }

  getExp() async {
    final resp = await this.services.mykinsService.getExp();

    this.exp = resp.data.values;
    update();

    return resp.data.values;
  }

  Future<List<SearchMyChildrensResponse>> getChildList({
    GeneralPaginationBody? body,
    bool reset = false,
  }) async {
    final resp = await this.paginate<SearchMyChildrensResponse>(
      request: this.services.mykinsService.getChildList,
      body: body,
      reset: reset,
    );

    update();
    return resp;
  }

  get childList {
    return this.generateGetter(type: SearchMyChildrensResponse);
  }

  registerKids({
    required RegisterChildBody body,
  }) async {
    final resp = await services.mykinsService.registerKids(body);

    return resp;
  }

  childInfo({required PostOnlyChildIdBody body}) async {
    final resp = await services.mykinsService.getChildInfo(body);
    myChildInfo = resp.data.values;
    //update();
    return myChildInfo;
  }

  setMyChildInfo(UserInfoResponse? child) {
    this.myChildInfo = child;
  }

  modifyChildInfo({required ChildProfileUpdateBody body}) async {
    final resp = await services.mykinsService.modifyChildInfo(body);

    return resp;
  }

  deleteChild({required PostOnlyChildIdBody body}) async {
    try {
      this.removeItemFromCache(
        type: SearchMyChildrensResponse,
        removeFunction: (opp) => opp.childUserNO == body.childUserNO,
      );
      await services.mykinsService.deleteChild(body);
      update();
    } catch (e) {
      rethrow;
    }
  }
}
