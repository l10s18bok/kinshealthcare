import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/register/model/reg_ok_email_model.dart';
import 'package:kins_healthcare/services/register/model/reg_ok_sms_model.dart';
import 'package:kins_healthcare/services/root_service.dart';
import 'package:kins_healthcare/services/user/model/device_regist_models.dart';
import 'package:kins_healthcare/services/user/model/email_create_models.dart';
import 'package:kins_healthcare/services/user/model/password_modify_models.dart';
import 'package:kins_healthcare/services/user/model/profile_img_regist_models.dart';
import 'package:kins_healthcare/services/user/model/register_model.dart';
import 'package:kins_healthcare/services/user/model/simple_pw_create.dart';
import 'package:kins_healthcare/services/user/model/simple_pw_modify_models.dart';
import 'package:kins_healthcare/services/user/model/user_info_models.dart';
import 'package:kins_healthcare/services/user/model/user_profile_models.dart';
import 'package:kins_healthcare/services/user/payment/model/payment_card_info.dart';

class UserController extends BaseController {
  bool loading = false;

  UserInfoResponse? userInfo;

  UserController({
    required RootService rootService,
  }) : super(rootService);

  registerProfileImg({required String path}) async {
    try {
      loading = true;
      update();

      final res = await this.services.userService.profileImgRegist(
            ProfileImgRegistBody(profileImg: path),
          );

      loading = false;

      update();

      return res;
    } catch (e) {
      loading = false;
      update();
    }
  }

  emailCreate({
    required EmailCreateBody body,
  }) async {
    final resp = await services.userService.emailCreate(body);

    return resp.resultMsg;
  }

  register({
    required RegisterBody body,
  }) async {
    final resp = await services.userService.register(body);

    return resp;
  }

  getUserInfo() async {
    await services.userService.userInfo().then((value) {
      userInfo = value.data.values;
      update();
    });

    return userInfo;
  }

  setUserInfo(UserInfoResponse? user) {
    this.userInfo = user;
  }

  getUserProfile({
    required UserProfileBody body,
  }) async {
    final resp = await services.userService.getProfileInfo(body);

    return resp.data;
  }

  createPinCode({
    required String pin,
  }) async {
    final resp = await services.userService.simplePwCreate(
      SimplePwCreateBody(simplePwNew: pin),
    );

    return resp.resultMsg;
  }

  verifySimplePassword() async {
    final resp = await this.services.userService.simplePwCheck();
    return resp.data;
  }

  updateSimplePassword({
    required String password,
  }) async {
    final resp = await this.services.userService.simplePwModify(
          SimplePwModifyBody(simplePwNew: password),
        );

    return resp.resultMsg;
  }

  updatePassword({
    required PasswordModifyBody body,
  }) async {
    final resp = await this.services.userService.passwordModify(body);

    return resp.resultMsg;
  }

  dropUser() async {
    await this.services.userService.dropUser();
  }

  get paymentCardInfos {
    return this.generateGetter(
      type: PaymentCardInfoResponse,
    );
  }

  getUserPayCardInfo({
    GeneralPaginationBody? body,
    bool reset = true,
    String key = 'default',
  }) async {
    final resp = await paginate<PaymentCardInfoResponse>(
      request: this.services.userService.userPayCardInfo,
      reset: reset,
      key: key,
      body: body,
    );

    return resp;
    // final resp = await services.userService.userPayCardInfo();
    // if (resp.resultCode == 'SUCCESS') {
    //   return resp.data.values;
    // }
    // return [];
  }

  deviceRegist() async {
    String? devUuid;
    String? userDeviceOs;

    final deviceInfo = await DeviceInfoPlugin();

    if (Platform.isIOS) {
      final iosInfo = await deviceInfo.iosInfo;

      devUuid = iosInfo.identifierForVendor;
      userDeviceOs = 'IOS';
    } else if (Platform.isAndroid) {
      final andInfo = await deviceInfo.androidInfo;

      devUuid = andInfo.androidId;
      userDeviceOs = 'ANDROID';
    }

    final String? token = await FirebaseMessaging.instance.getToken();

    if (token == null || devUuid == null || userDeviceOs == null) {
      print(
        'UserController.deviceRegist\nnull 값이 존재합니다.\ntoken:$token\ndevUuid:$devUuid\nuserDeviceOs:$userDeviceOs',
      );
      return;
    }

    final resp = await services.userService.deviceRegist(
      DeviceRegistBody(
        devUuid: devUuid,
        pushToken: token,
        userDeviceOs: userDeviceOs,
      ),
    );

    return resp;
  }

  userModify({
    required UserInfoResponse body,
  }) {
    return services.userService.userModify(body);
  }

  regOkEmail({
    required RegOkEmailBody body,
  }) {
    return services.userService.regOkEmail(body);
  }

  regOkSmS({
    required RegOkSmsBody body,
  }) {
    return services.userService.regOkSms(body);
  }

  regChildOkEmail({
    required RegOkEmailBody body,
  }) {
    return services.userService.regChildOkEmail(body);
  }
}
