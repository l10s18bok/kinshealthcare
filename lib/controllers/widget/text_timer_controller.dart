import 'dart:async';

import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';

/*
 작성일 : 2021-02-19
 작성자 : Victor
 화면명 : 
 클래스 : TextTimerController
 경로 : 
 설명 : 텍스트 전달 및 공유 타이머 사용 용도
*/

///텍스트 전달 및 공유 타이머 사용 용도
class TextTimerController extends GetxController {
  RxMap<String, String> _msgList = RxMap<String, String>();

  Timer? _timer;

  TextTimerController() : super();

  ///키 입력
  String getString(String key) {
    if (_msgList.containsKey(key)) {
      return _msgList[key]!;
    } else {
      return '';
    }
  }

  void setString(String key, String value) {
    _msgList[key] = value;
  }

  void startTimer(Function(Timer) callback) {
    stopTimer();

    _timer = Timer.periodic(
      Duration(seconds: 1),
      callback,
    );
  }

  void stopTimer() {
    _timer?.cancel();
  }
}
