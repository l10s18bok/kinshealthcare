import 'package:intl/intl.dart';

class TextUtils {
  String numberToLocalCurrency({
    required double amount,
  }) {
    final fmt = NumberFormat('#,###');
    return fmt.format(amount);
  }

  ///전화번호 숫자 문자열에 하이픈(-) 추가
  ///한국형 전화번호만 지원함
  ///맨 앞에 국제번호나 특수기호 있는거 인식못함
  static String addHyponToPhoneNumber(String phone) {
    final regex = RegExp('\(^02.{0}|^01.{1}|^[0-9]{3})');

    final num1 = regex.firstMatch(phone)!.end;
    final num2 = phone.length - 4;

    if (num1 < 1) {
      return phone;
    } else {
      return '${phone.substring(0, num1)}-${phone.substring(num1, num2)}-${phone.substring(num2, phone.length)}';
    }
  }
}
