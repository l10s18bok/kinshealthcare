import 'dart:collection';
import 'package:kins_healthcare/models/base_model.dart';

class CacheUtils<T extends BaseModel> {
  /// 실제 데이터 캐싱에 사용
  final HashMap<int, T> _itemsCache = HashMap();

  /// 데이터의 순서 캐싱에 사용
  final HashMap<String, LinkedHashSet> _orderCache = HashMap();

  /// 네트워크 로딩 일괄적으로 관리
  final HashMap<String, bool> _loadingMap = HashMap();

  /// [_itemsCache] 업데이트 함수
  ///
  /// 이미 없는데이터는 집어넣고 있는 데이터는 업데이트
  /// 현재 API 구조로 ID 기반 캐시 사용 불가능
  // HashMap<int, T> updateCache({
  //   @required List<T> items,
  // }) {
  // assert(items != null);
  // assert(items.length > 0);
  //
  // for (var item in items) {
  //   this._itemsCache.update(
  //     item.id,
  //         (e) => item,
  //     ifAbsent: () => item,
  //   );
  // }
  //
  // return HashMap.from(
  //   this._itemsCache,
  // );
  // }

  /// [id] 로 캐시에서 데이터 하나 가져오기
  getItemById({
    required int id,
  }) {
    return this._itemsCache[id];
  }

  /// [ids] 에 해당되는 모든 데이터를 List 형태로 리턴
  ///
  /// 존재하지 않는 id 가 입력되더라도 에러를 던지지 않는다.
  /// [ids] 길이와 return 값의 길이를 비교해서 가져오지 못한
  /// 데이터가 있는지 확인 가능.
  List<T> getItemsByIds({
    required List<int> ids,
  }) {
    List<T> items = [];

    for (int id in ids) {
      final value = this._itemsCache[id];
      if (value == null) continue;
      items.add(value);
    }

    return items;
  }

  /// [_orderCache]를 사용해서 [_itemsCache] 의 데이터 추출
  List<T> getItemsByOrder({
    String orderKey = 'default',
  }) {
    final ids = this._orderCache[orderKey];

    List<T> items = [];

    if (ids == null) return items;

    for (int id in ids) {
      final value = this._itemsCache[id];
      if (value == null) continue;
      items.add(value);
    }

    return items;
  }

  /// [_orderCache] 업데이트 함수
  ///
  /// [ids] 는 캐시의 유니크 키, [isAppend] 가 true 일 경우
  /// 현재 캐시에 그대로 추가하고 false 일 경우 cache 를 삭제 후
  /// 추가. [addToEnd] 가 true 일 경우 끝에부터 추가하고 false 일
  /// 경우 앞에부터 추가
  LinkedHashSet updateOrderCache({
    required List<int> ids,
    String cacheKey = 'default',
    bool isAppend = false,
    bool addToEnd = true,
  }) {
    assert(ids.length > 0);

    final LinkedHashSet newCache = LinkedHashSet();

    if (!isAppend || this._orderCache[cacheKey] == null) {
      newCache.addAll(ids);
    } else {
      if (addToEnd) {
        newCache.addAll([
          ...this._orderCache[cacheKey]!,
          ids,
        ]);
      } else {
        newCache.addAll([
          ids,
          ...this._orderCache[cacheKey]!,
        ]);
      }
    }

    this._orderCache[cacheKey] = newCache;

    return LinkedHashSet.from(newCache);
  }

  /// 로딩 해시맵 업데이트 함수
  ///
  /// [key] 는 로딩 상태를 저장할 유니크 키
  void updateLoading({
    required bool isLoading,
    String key = 'default',
  }) {
    _loadingMap[key] = isLoading;
  }

  /// 로딩 상태 가져오는 함수
  bool isLoading({
    String key = 'default',
  }) {
    return _loadingMap[key] ?? false;
  }
}
