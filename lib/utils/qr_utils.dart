class QrUtils {
  /// [code] 는 ID="1" 형태로 들어옴
  extractId({
    required String code,
  }) {
    final split = code.split('=');

    if (split.length != 2) {
      throw Exception('malformed code');
    }

    final id = split[1].toString().replaceAll("\"", "");

    return id;
  }
}
