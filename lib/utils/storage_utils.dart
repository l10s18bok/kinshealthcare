import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class StorageUtils {
  final storage = new FlutterSecureStorage();

  StorageUtils._privateConstructor();

  static final StorageUtils _instance = StorageUtils._privateConstructor();

  factory StorageUtils() {
    return _instance;
  }

  /** kevin 2021-05-17 추가
   * 로그아웃등의 기능이 동작하지 않아 전체 데이타 삭제를 추가하였음.
   */
  Future<void> deleteAll ()async {
    await this.storage.deleteAll();
  }

  readByKey({
    required String key,
  }) async {
    return await this.storage.read(key: key);
  }

  deleteByKey({
    required String key,
  }) async {
    return await this.storage.delete(key: key);
  }

  writeByKey({
    required String key,
    required String value,
  }) async {
    return await this.storage.write(key: key, value: value);
  }
}
