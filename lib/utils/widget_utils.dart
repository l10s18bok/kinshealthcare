import 'package:flutter/material.dart';

class WidgetUtils {
  // 한 row 에 균등하게 버튼 배치
  // 모자라는 칸은 Container 로 채우기
  renderListWidgetsInMultipleRows({
    required List<Widget> widgets,
    int rowMax = 4,
    double lineHeight = 10.0,
    double widgetSpacing = 0.0,
  }) {
    print('test');
    final rows = widgets.asMap().entries.fold(<dynamic>[], (List? t, e) {
      // 맨처음에 리스트 하나 추가
      if (t == null) return null;

      if (t.isEmpty) {
        t.add([]);
      }
      final curIdx = t.length - 1;
      List<dynamic> curList = t[curIdx];

      // 최대치에 닿으면 다음 리스트 추가
      // 아니면 현재 리스트에 추가
      if (curList.length == rowMax) {
        List<dynamic> newList = [e.value];

        t.add(newList);
        curList = newList;
      } else {
        curList.add(e.value);
      }

      // max 에서 부족한 위젯은 Container 로 채우기
      if (e.key == widgets.length - 1 && curList.length < rowMax) {
        curList.addAll(List.filled(rowMax - curList.length, Container()));
      }

      return t;
    });

    if (rows == null) return Container();

    for (int i = 0; i < rows.length; i++) {
      List row = rows[i];
      rows[i] = row.asMap().entries.fold([], (List? t, e) {
        if (t == null) return null;

        if (e.key < row.length - 1) {
          t.add(
            Expanded(
              child: e.value,
            ),
          );
          t.add(
            Container(width: widgetSpacing),
          );
        } else {
          t.add(
            Expanded(
              child: e.value,
            ),
          );
        }

        return t;
      });
    }

    return Column(
      children: List<Widget>.from(
        rows.asMap().entries.map(
              (entry) => Column(
                children: [
                  Row(
                    children: List<Widget>.from(
                      entry.value.map(
                        (x) => x,
                      ),
                    ),
                  ),
                  // 마지막 줄 제외하고 패딩 추가
                  if (entry.key != rows.length - 1)
                    Container(height: lineHeight),
                ],
              ),
            ),
      ),
    );
  }

  ///위젯의 크기를 측정한다.
  ///렌더링 끝난 다음에만 측정할 수 있다.
  ///
  ///참고 링크 : https://stackoverflow.com/questions/49307677/how-to-get-height-of-a-widget
  static Size getWidgetSize(BuildContext context, Widget widget) {
    final box = context.findRenderObject() as RenderBox;
    return box.size;
  }
}
