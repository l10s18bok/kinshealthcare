import 'package:dio/dio.dart';
import 'package:get/get.dart';

class SnackbarUtils {
  parseDioErrorMessage({
    required DioError error,
    required String title,
  }) {
    Get.snackbar(
      title,
      error.response!.data['resultMsg'],
    );
  }
}
