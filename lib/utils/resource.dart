import 'dart:ui';

import 'package:kins_healthcare/models/local/card_model.dart';

/**
 * @author  이명재
 * @version 1.0
 * @since   2021-01-21
 */

const Color kinsWhite = Color(0xffffffff);

const Color kinsBlack = Color(0xff000000);
const Color kinsBlack14 = Color(0x0d000000);
const Color kinsBlack0D = Color(0xff0D0D0D);

const Color kinsBlack16 = Color(0xFF161616);
const Color kinsBlack1D = Color(0xFF1D1D1D);

const Color kinsBlack23 = Color(0xff232323);
const Color kinsBlack26 = Color(0xFF262626);
const Color kinsBlack30 = Color(0xff303030);
const Color kinsBlack3E = Color(0xFF3e3e3e);
const Color kinsBlack35 = Color(0xff353535);
const Color kinsBlack36 = Color(0xff363636);
const Color kinsBlack44 = Color(0xff444444);
const Color kinsBlack4F = Color(0xff4F4F4F);
const Color kinsBlack45 = Color(0xff454545);
const Color kinsBlack5A = Color(0xff5a5a5a);
const Color kinsBlack5B = Color(0xFF5B5B5B);
const Color kinsBlack63 = Color(0xff636363);
const Color kinsBlack6D = Color(0xff6D6D6D);
const Color kinsBlack87 = Color(0xff878787);
const Color kinsBlack7F = Color(0xff7F7F7F);
const Color kinsBlack72 = Color(0xff727272);
const Color kinsBlack09 = Color(0xff090a26);

const Color kinsGrayE4 = Color(0xffe4e4e4);
const Color kinsGrayE2E = Color(0xffE2E2E9);
const Color kinsGrayE1 = Color(0xffE1E2E8);
const Color kinsGrayEF = Color(0xffEFEFF4);
const Color kinsGrayEA = Color(0xffEAEAEA);
const Color kinsGrayE8 = Color(0xffe8e8e8);
const Color kinsGrayEC = Color(0xffececec);
const Color kinsGrayE2 = Color(0xffe2e2e2);
const Color kinsGrayE8E = Color(0xffe8e7ec);
const Color kinsGrayF3 = Color(0xfff3f4f6);
const Color kinsGrayF2 = Color(0xfff2f2f4);
const Color kinsGrayFE = Color(0xff7e7e7e);
const Color kinsGray3F = Color(0xfff3f3f3);
const Color kinsGrayF7 = Color(0xff7f7f7f);
const Color kinsGray53 = Color(0xff535353);
const Color kinsGrayF7F = Color(0xffF7F8F9);
const Color kinsGray7D = Color(0xff7D7D7D);
const Color kinsGrayB9 = Color(0xffb9b9b9);
const Color kinsGrayB6 = Color(0xffb6b6b6);
const Color kinsGrayDD = Color(0xffdde1e2);
const Color kinsGrayD8 = Color(0xffD8D8D8);
const Color kinsGrayD1 = Color(0xffD1D2D6);
const Color kinsGrayDC = Color(0xffDCDCE5);
const Color kinsGrayC2 = Color(0xffc2c3c8);
const Color kinsGrayCB = Color(0xffCBCBCB);
const Color kinsGray96 = Color(0xff969696);
const Color kinsGray90 = Color(0xff909090);
const Color kinsGray9B = Color(0xFF9B9B9B);
const Color kinsGray98 = Color(0xFF989898);
const Color kinsGray95 = Color(0xff959595);
const Color kinsGray94 = Color(0xff949494);
const Color kinsGray9A = Color(0xff9A9A9A);
const Color kinsGray6E = Color(0xff6e6e6e);
const Color kinsGrayEE = Color(0xffEEEFF3);
const Color kinsGray6D = Color(0xff6d6d6d);
const Color kinsGray6F = Color(0xFF6F6F6F);
const Color kinsGray6A = Color(0xFF6A6A6A);
const Color kinsGray61 = Color(0xFF616161);
const Color kinsGrayE9 = Color(0xFFE9E9E9);
const Color kinsGray63 = Color(0xff636363);
const Color kinsGray8A = Color(0xff8a8a8a);
const Color kinsGray81 = Color(0xff818181);
const Color kinsGrayA3 = Color(0xFFa3a3a3);

const Color kinsBlueD7 = Color(0xffd7d8ee);
const Color kinsBlueEF = Color(0xffEFF0F5);
const Color kinsBlueE8 = Color(0xffE8E7EC);
const Color kinsBlueDD = Color(0xffdddeed);
const Color kinsBlue3F = Color(0xfff3f4f6);
const Color kinsBlueA2 = Color(0xffa2a4d4);
const Color kinsBlueCA = Color(0xffCACAE7);
const Color kinsBlueB8 = Color(0xffb8b9d5);
const Color kinsBlue40 = Color(0xff4042ab);
const Color kinsBlueCF = Color(0xffCFCFE2);
const Color kinsBlue14 = Color(0x144042ab);
const Color kinsBlue1D = Color(0xff1d1f75);
const Color kinsBlue44 = Color(0x22444444);
const Color kinsBlueE2 = Color(0xFFE2E3F4);

const Color kinsYellowFD = Color(0xffFDD84F);

const Color kinsPuple8B = Color(0xff8B6ADC);

const Color kinsPinkFF = Color(0xffFF9B95);

const Color kinsOrangeF2 = Color(0xffF29061);

const Color kinsBlue6F = Color(0xff6FA7FA);

const Color kinsGreen6C = Color(0xff6CC4BF);

const Color kinsRedDC = Color(0xffDC5050);

final logoPath01 = 'assets/svgs/ic/ic_img_logo_01.svg';
final logoPath02 = 'assets/svgs/ic/ic_img_logo_02.svg';
final logoPath03 = 'assets/svgs/ic/ic_img_logo_03.svg';
final logoPath04 = 'assets/svgs/ic/ic_img_logo_04.svg';

final bcCard = CardModel(
  title: '비씨카드',
  imagePath: 'assets/png/card_ic_bc.png',
  color: Color(0xFFC83534),
  cardNumber: '비씨카드 Deep Dream Card',
);
final bnkCard = CardModel(
  title: '부산카드',
  imagePath: 'assets/png/card_ic_bnk.png',
  color: Color(0xFF969696),
  cardNumber: '부산카드 Deep Dream Card',
);
final ctCard = CardModel(
  title: '씨티카드',
  imagePath: 'assets/png/card_ic_ct.png',
  color: Color(0xFFE6E6E6),
  cardNumber: '씨티카드 Deep Dream Card',
  textColor: Color(0xFF000000),
);
final dgbCard = CardModel(
  title: '대구카드',
  imagePath: 'assets/png/card_ic_dgb.png',
  color: Color(0xFFCBD6E4),
  cardNumber: '대구카드 Deep Dream Card',
  textColor: Color(0xFF000000),
);
final ehCard = CardModel(
  title: '외환카드',
  imagePath: 'assets/png/card_ic_eh.png',
  color: Color(0xFFC1E1F2),
  cardNumber: '외환카드 Deep Dream Card',
  textColor: Color(0xFF000000),
);
final gjCard = CardModel(
  title: '광주카드',
  imagePath: 'assets/png/card_ic_gj.png',
  color: Color(0xFF002C6A),
  cardNumber: '광주카드 Deep Dream Card',
);
final hdCard = CardModel(
  title: '현대카드',
  imagePath: 'assets/png/card_ic_hd.png',
  color: Color(0xFF259BFE),
  cardNumber: '현대카드 Deep Dream Card',
);
final hnCard = CardModel(
  title: '하나카드',
  imagePath: 'assets/png/card_ic_hn.png',
  color: Color(0xFF008375),
  cardNumber: '신한 카드 Deep Dream Card',
);
final ibkCard = CardModel(
  title: 'IBK카드',
  imagePath: 'assets/png/card_ic_ibk.png',
  color: Color(0xFF748A93),
  cardNumber: 'IBK카드 Deep Dream Card',
);
final jbCard = CardModel(
  title: '전북카드',
  imagePath: 'assets/png/card_ic_jb.png',
  color: Color(0xFF000E5C),
  cardNumber: '전북카드 Deep Dream Card',
);
final kakaoCard = CardModel(
  title: '카카오뱅크 카드',
  imagePath: 'assets/png/card_ic_kakao.png',
  color: Color(0xFF3B1F1E),
  cardNumber: '카카오뱅크 카드 Deep Dream Card',
);
final kbCard = CardModel(
  title: 'KB국민카드',
  imagePath: 'assets/png/card_ic_kb.png',
  color: Color(0xFF776C61),
  cardNumber: 'KB국민카드 Deep Dream Card',
);
final kBankCard = CardModel(
  title: '케이뱅크 카드',
  imagePath: 'assets/png/card_ic_k_bank.png',
  color: Color(0xFFE2E2E2),
  cardNumber: '케이뱅크 카드 Deep Dream Card',
  textColor: Color(0xFF000000),
);
final kdbCard = CardModel(
  title: 'KDB산업 카드',
  imagePath: 'assets/png/card_ic_kdb.png',
  color: Color(0xFF90C2E9),
  cardNumber: 'KDB산업 카드 Deep Dream Card',
);
final lotteCard = CardModel(
  title: '롯데카드',
  imagePath: 'assets/png/card_ic_lotte.png',
  color: Color(0xFF6F10C1),
  cardNumber: '롯데카드 Deep Dream Card',
);
final nhCard = CardModel(
  title: '농협카드',
  imagePath: 'assets/png/card_ic_nh.png',
  color: Color(0xFF04A54C),
  cardNumber: '농협카드 Deep Dream Card',
);
final sbiCard = CardModel(
  title: 'SBI저축 카드',
  imagePath: 'assets/png/card_ic_sbi.png',
  color: Color(0xFFE6E6E6),
  cardNumber: 'SBI저축 Deep Dream Card',
  textColor: Color(0xFF000000),
);
final scCard = CardModel(
  title: 'SC제일 카드',
  imagePath: 'assets/png/card_ic_sc.png',
  color: Color(0xFF004E6D),
  cardNumber: 'SC제일 카드 Deep Dream Card',
);
final shCard = CardModel(
  title: '신한카드',
  imagePath: 'assets/png/card_ic_sh.png',
  color: Color(0xFF214597),
  cardNumber: '신한 카드 Deep Dream Card',
);
final sinhCard = CardModel(
  title: '신협카드',
  imagePath: 'assets/png/card_ic_sinh.png',
  color: Color(0xFF92ABC2),
  cardNumber: '신협카드 Deep Dream Card',
);
final smeCard = CardModel(
  title: '새마을카드',
  imagePath: 'assets/png/card_ic_sme.png',
  color: Color(0xFFE6E6E6),
  cardNumber: '새마을카드 Deep Dream Card',
  textColor: Color(0xFF000000),
);
final ssCard = CardModel(
  title: '삼성카드',
  imagePath: 'assets/png/card_ic_ss.png',
  color: Color(0xFF014DA1),
  cardNumber: '삼성카드 Deep Dream Card',
);
final suhCard = CardModel(
  title: '수협카드',
  imagePath: 'assets/png/card_ic_suh.png',
  color: Color(0xFF8CCBFF),
  cardNumber: '신한 카드 Deep Dream Card',
);
final ucgCard = CardModel(
  title: '우체국카드',
  imagePath: 'assets/png/card_ic_ucg.png',
  color: Color(0xFFB6A17B),
  cardNumber: '우체국카드 Deep Dream Card',
);
final urCard = CardModel(
  title: '우리카드',
  imagePath: 'assets/png/card_ic_ur.png',
  color: Color(0xFF0067AC),
  cardNumber: '우리카드 Deep Dream Card',
);
final nonCard = CardModel(
  title: '',
  imagePath: '',
  color: Color(0xFFF7F8F9),
  isNon: true,
);

// 이하 Andy 추가 2021.05.31
    Map<String, Color> bankColor = {
      "비씨": Color(0xFFC83534),
      "B032": Color(0xFF969696), //부산은행
      "B027": Color(0xFF000000), //씨티은행
      "B031": Color(0xFF000000), //대구은행
      "B005": Color(0xFF000000), //외환은행
      "B034": Color(0xFF002C6A), //광주은행
      "현대": Color(0xFF259BFE),
      "B081": Color(0xFF008375), //하나은행
      "B003": Color(0xFF748A93), //기업은행
      "B037": Color(0xFF000E5C), //전북은행
      "B090": Color(0xFF3B1F1E), //카카오뱅크
      "B004": Color(0xFF776C61), //국민은행
      "B089": Color(0xFF000000), //케이뱅크
      "B002": Color(0xFF90C2E9), //KDB산업
      "롯데": Color(0xFF6F10C1),
      "B011": Color(0xFF04A54C), //농협
      "SBI저축": Color(0xFF000000),
      "B023": Color(0xFF004E6D), //SC제일은행
      "B088": Color(0xFF214597), //신한은행
      "신협": Color(0xFF92ABC2),
      "B045": Color(0xFFE6E6E6), //새마을금고
      "삼성": Color(0xFF014DA1),
      "B007": Color(0xFF8CCBFF), //수협
      "B071": Color(0xFFB6A17B), //우체국
      "B020": Color(0xFFFF0067AC), //우리은행
      "국민카드": Color(0xFF776C61),
    };

    returnBankColor({String bankCode = 'B088'}) {
      return bankColor[bankCode];
    }

   Map<String, Color> bankNameColor = {
      "비씨": Color(0xFFC83534),
      "부산은행": Color(0xFF969696), //부산은행
      "씨티은행": Color(0xFF000000), //씨티은행
      "대구은행": Color(0xFF000000), //대구은행
      "외환은행": Color(0xFF000000), //외환은행
      "광주은행": Color(0xFF002C6A), //광주은행
      "현대": Color(0xFF259BFE),
      "하나은행": Color(0xFF008375), //하나은행
      "기업은행": Color(0xFF748A93), //기업은행
      "전북은행": Color(0xFF000E5C), //전북은행
      "카카오뱅크": Color(0xFF3B1F1E), //카카오뱅크
      "국민은행": Color(0xFF776C61), //국민은행
      "B089": Color(0xFF000000), //케이뱅크
      "산업은행": Color(0xFF90C2E9), //산업은행
      "롯데": Color(0xFF6F10C1),
      "농협중앙": Color(0xFF04A54C), //농협중앙
      "SBI저축": Color(0xFF000000),
      "제일은행": Color(0xFF004E6D), //SC제일은행
      "신한은행": Color(0xFF214597), //신한은행
      "신협": Color(0xFF92ABC2),
      "새마을금고": Color(0xFFE6E6E6), //새마을금고
      "삼성": Color(0xFF014DA1),
      "수협": Color(0xFF8CCBFF), //수협
      "우체국": Color(0xFFB6A17B), //우체국
      "우리은행": Color(0xFFFF0067AC), //우리은행
      "국민카드": Color(0xFF776C61),
    };

    returnBankNameColor({String bankName = '신한은행'}) {
      return bankNameColor[bankName];
    }

    returnBankLogo({String bankCode = 'B088'}) {
      return bankLogo[bankCode];
    }

    Map<String, String> bankLogo = {
      "비씨카드": 'assets/png/card_ic_bc.png',
      "B032": 'assets/png/card_ic_bnk.png',//부산은행
      "B027": 'assets/png/card_ic_ct.png',//씨티은행
      "B031": 'assets/png/card_ic_dgb.png',//대구은행
      "B005": 'assets/png/card_ic_eh.png',//외환은행
      "B034": 'assets/png/card_ic_gj.png', //광주은행
      "현대카드": 'assets/png/card_ic_hd.png',
      "B081": 'assets/png/card_ic_hn.png', //하나은행
      "B003": 'assets/png/card_ic_ibk.png', //IBK기업은행
      "B037": 'assets/png/card_ic_jb.png', //전북은행
      "B090": 'assets/png/card_ic_kakao.png', //카카오뱅크
      "B004": 'assets/png/card_ic_kb.png', // 국민은행
      "B089": 'assets/png/card_ic_k_bank.png', //케이뱅크
      "B002": 'assets/png/card_ic_kdb.png', //KDB산업은행
      "롯데카드": 'assets/png/card_ic_lotte.png',
      "B011": 'assets/png/card_ic_nh.png', //농협
      "SBI저축은행": 'assets/png/card_ic_sbi.png',
      "B023": 'assets/png/card_ic_sc.png', //SC제일은행
      "B088": 'assets/png/card_ic_sh.png', //신한은행
      "신협": 'assets/png/card_ic_sinh.png',
      "B045": 'assets/png/card_ic_sme.png', //새마을금고
      "삼성카드": 'assets/png/card_ic_ss.png',
      "B007": 'assets/png/card_ic_suh.png', //수협
      "B071": 'assets/png/card_ic_ucg.png', //우체국
      "B020": 'assets/png/card_ic_ur.png', //우리은행
      "국민카드": 'assets/png/card_ic_kb.png',
    };