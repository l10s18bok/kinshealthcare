// import 'package:dio/dio.dart';
// import 'package:kins_healthcare/utils/storage_utils.dart';

// class HttpUtils {
//   final _dio;

//   HttpUtils()
//       : this._dio = Dio()
//           ..interceptors.add(
//             InterceptorsWrapper(
//               onRequest: onRequestWrapper,
//               onResponse: onResponseWrapper,
//               onError: onErrorWrapper,
//             ),
//           );

//   static onErrorWrapper(
//     DioError error,
//     ErrorInterceptorHandler handler,
//   ) {
//     print('------ERROR THROWN WITH FOLLOWING LOG------');
//     print('path: ${error.requestOptions.baseUrl}${error.requestOptions.path}');

//     final response = error.response;
//     if (response != null) print('body: ${response.data.toString()}');
//     if (response != null) print('headers: ${response.headers}');
//     print('------ERROR THROWN INFO END------');
//     print('');
//   }

//   static onResponseWrapper(
//     Response resp,
//     ResponseInterceptorHandler handler,
//   ) async {
//     print('------RESPONSE RECEIVED WITH FOLLOWING LOG------');
//     print('path: ${resp.requestOptions.baseUrl}${resp.requestOptions.path}');
//     print('body: ${resp.data}');
//     print('headers: ${resp.headers}');
//     print('------RESPONSE RECEIVED INFO END------');
//     print('');
//   }

//   static onRequestWrapper(
//     RequestOptions options,
//     RequestInterceptorHandler handler,
//   ) async {
//     if (options.headers.containsKey('accessToken')) {
//       options.headers.remove('accessToken');

//       final utils = StorageUtils();

//       final token = await utils.readByKey(key: 'accessToken');

//       options.headers.addAll({
//         'Authorization': 'Bearer $token',
//       });
//     } else if (options.headers.containsKey('refreshToken')) {
//       // add this when refreshing api is finished.
//     }

//     print('------REQUEST SENT WITH FOLLOWING LOG------');
//     print('path: ${options.baseUrl}${options.path}');
//     print('body: ${options.data}');
//     print('headers: ${options.headers}');
//     print('------REQUEST SENT INFO END------');
//     print('');

//     return options;
//   }

//   /// [encrypt] 로 암호화 후 요청 날리고 [decrypt] 로 복호화 후 리턴
//   ///
//   /// 암호화 해서 전송해야 하는 모든 요소를 [encrypt] 의 파라미터로
//   /// 제공 (body, header 등). 결과값이 날라오면 [decrypt] 한 후
//   /// 리턴
//   Future<Response> get({
//     required String path,
//     Map<String, dynamic>? headers,
//   }) async {
//     encrypt();

//     final resp = await _dio.get(
//       path,
//       options: Options(
//         headers: headers ?? {},
//       ),
//     );

//     decrypt();

//     return resp;
//   }

//   constructOptions({
//     required bool incAccessToken,
//     required bool incRefreshToken,
//     required Map<String, dynamic> headers,
//   }) {
//     String contentType = 'application/json';

//     final resp = new Map<String, dynamic>.from({
//       ...headers,
//     });

//     if (headers.containsKey('content-type')) {
//       contentType = headers['content-type'];
//       headers.remove('content-type');
//     }

//     if (incAccessToken) {
//       resp.addAll({
//         'accessToken': true,
//       });
//     } else if (incRefreshToken) {
//       resp.addAll({
//         'refreshToken': true,
//       });
//     }

//     return Options(
//       contentType: contentType,
//       headers: resp,
//     );
//   }

//   Future<Response> post({
//     required String path,
//     Map<String, dynamic>? body,
//     Map<String, dynamic>? headers,
//     bool incAccessToken = false,
//     bool incRefreshToken = false,
//     Function(int sent, int total)? onSendProgress,
//   }) async {
//     assert(
//       !(incAccessToken && incRefreshToken),
//       'cannot include both accessToken and refreshToken',
//     );

//     body ??= {};
//     headers ??= {};
//     onSendProgress ??= (int sent, int total) {};

//     encrypt();

//     final options = this.constructOptions(
//       incAccessToken: incAccessToken,
//       incRefreshToken: incRefreshToken,
//       headers: headers,
//     );

//     final resp = await _dio.post(
//       path,
//       data: body,
//       options: options,
//       onSendProgress: onSendProgress,
//     );

//     decrypt();

//     return resp;
//   }

//   Future<Response> put({
//     required String path,
//     FormData? body,
//     Map<String, dynamic>? headers,
//   }) async {
//     body ??= FormData();
//     headers ??= {};

//     encrypt();

//     final resp = await _dio.put(
//       path,
//       data: body,
//       options: Options(
//         headers: headers,
//       ),
//     );

//     decrypt();

//     return resp;
//   }

//   Future<Response> delete({
//     required String path,
//     FormData? body,
//     Map<String, dynamic>? headers,
//   }) async {
//     encrypt();

//     body ??= FormData();
//     headers ??= {};

//     final resp = await _dio.delete(
//       path,
//       data: body,
//       options: Options(
//         headers: headers,
//       ),
//     );

//     decrypt();

//     return resp;
//   }

//   /// AES256 암호화
//   encrypt() {}

//   /// AES256 복호화
//   decrypt() {}
// }
