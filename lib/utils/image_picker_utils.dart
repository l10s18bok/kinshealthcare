import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class ImagePickerUtils {
  final picker = ImagePicker();
  final BuildContext context;

  ImagePickerUtils.of(this.context);

  Future<File?> pickImage({
    required ImageSource imageSource,
    bool processImage = false,
  }) async {
    final theme = ThemeFactory.of(context).theme;

    final PickedFile? pickedFile = await this.picker.getImage(
          source: imageSource,
        );

    // 고른 이미지 없으면 return
    if (pickedFile == null) return null;

    File? file;

    if (processImage) {
      file = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatioPresets: [],
        cropStyle: CropStyle.circle,
        androidUiSettings: AndroidUiSettings(
          toolbarTitle: '이미지 크롭하기',
          toolbarColor: theme.primaryColor,
          toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.square,
        ),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ),
      );
    } else {
      file = File(pickedFile.path);
    }

    return file;
  }
}
