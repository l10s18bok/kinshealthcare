/*
 * 작성일 : 2021-02-21
 * 작성자 : JC
 * 화면명 :
 * 경로 :
 * 클래스 : Validators
 * 설명 : 텍스트필드 validator 함수들
 */
import 'package:flutter/cupertino.dart';

class Validators {
  static bool isEmail(String? mail) => RegExp(r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,5}$").hasMatch(mail!);

  /// 이메일
  static String? emailValidator(String? val) {
    //     final RegExp regexp = RegExp(r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,5}$");

    // return regexp.hasMatch(val!) ? null : '정확한 이메일을 입력해주세요.';
    return null;
  }

  /// 비밀번호
  static String? passwordValidator(String? val) {
    RegExp regexp = new RegExp(r"^(?=.*[A-Z])(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$");
    return regexp.hasMatch(val!) ? null : "비밀번호는 대소문자 + 숫자 특수문자 조합 8자리 이상입니다.";
  }

  /// 이름 성만 검증
  static String? lastnameValidator(String? val) {
    return null;
  }

  /// 이름 성 빼고 검증
  static String? firstnameValidator(String? val) {
    return null;
  }

  /// 이름 전부 검증
  static String? nameValidator(String? val) {
    return null;
  }

  /// 핸드폰번호 검증
  static String? phoneValidator(String? val) {
    return null;
  }

  /// 생년월일 검증
  static String? dobValidator(String? val) {
    return null;
  }

  /// 성별 검증
  static String? sexValidator(String? val) {
    return null;
  }

  /// 간편비밀번호 검증
  static String? simplePwValidator(String? val) {
    return null;
  }

  ///baron  2021-05-09
  /// 숫자 콤마 포맷
  static String? numberFormat(int number) {
    String newString = '';
    String separator = ',';
    for (int i = '$number'.length - 1; i >= 0; i--) {
      if (('$number'.length - 1 - i) % 3 == 0 && i != '$number'.length - 1) newString = separator + newString;
      newString = '$number'[i] + newString;
    }
    return newString;
  }
}
