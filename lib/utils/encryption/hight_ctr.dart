/*
 작성일 : 2021-03-05
 작성자 : Victor
 화면명 : 
 클래스 : HightCtr
 경로 : 
 설명 : HIGHT CTR 방식 암복호화 클래스
*/

//	int[] : Int32List
//	byte[] : Uint8List

import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'dart:io';

final int _KISA_DECRYPT = 0;
final int _KISA_ENCRYPT = 1;

final BIG_ENDIAN = Endian.big;
final LITTLE_ENDIAN = Endian.little;

final num INT_RANGE_MAX = pow(2, 32);

///KISA HIGHT CTR 알고리즘의 dart 구현
///
///출처 : https://seed.kisa.or.kr/kisa/Board/18/detailView.do
///
///사용법 1 : [HIGHT_CTR_Encrypt] (암호화) -> [HIGHT_CTR_Decrypt] (복호화)
///
///사용법 2 : [HIGHT_CTR_init] (초기화) -> [HIGHT_CTR_Process] (처리) -> [HIGHT_CTR_Close] (정리) -> 완료 ([HIGHT_CTR_init]에 넣는 인자에 따라 암호화/복호화를 구별함)
///
///사용 예제는 [testHight] 함수 참조
class HightCtr {
  final ENDIAN = Endian.big;

  final Uint8List F0 = Uint8List.fromList([
    0x00,
    0x86,
    0x0D,
    0x8B,
    0x1A,
    0x9C,
    0x17,
    0x91,
    0x34,
    0xB2,
    0x39,
    0xBF,
    0x2E,
    0xA8,
    0x23,
    0xA5,
    0x68,
    0xEE,
    0x65,
    0xE3,
    0x72,
    0xF4,
    0x7F,
    0xF9,
    0x5C,
    0xDA,
    0x51,
    0xD7,
    0x46,
    0xC0,
    0x4B,
    0xCD,
    0xD0,
    0x56,
    0xDD,
    0x5B,
    0xCA,
    0x4C,
    0xC7,
    0x41,
    0xE4,
    0x62,
    0xE9,
    0x6F,
    0xFE,
    0x78,
    0xF3,
    0x75,
    0xB8,
    0x3E,
    0xB5,
    0x33,
    0xA2,
    0x24,
    0xAF,
    0x29,
    0x8C,
    0x0A,
    0x81,
    0x07,
    0x96,
    0x10,
    0x9B,
    0x1D,
    0xA1,
    0x27,
    0xAC,
    0x2A,
    0xBB,
    0x3D,
    0xB6,
    0x30,
    0x95,
    0x13,
    0x98,
    0x1E,
    0x8F,
    0x09,
    0x82,
    0x04,
    0xC9,
    0x4F,
    0xC4,
    0x42,
    0xD3,
    0x55,
    0xDE,
    0x58,
    0xFD,
    0x7B,
    0xF0,
    0x76,
    0xE7,
    0x61,
    0xEA,
    0x6C,
    0x71,
    0xF7,
    0x7C,
    0xFA,
    0x6B,
    0xED,
    0x66,
    0xE0,
    0x45,
    0xC3,
    0x48,
    0xCE,
    0x5F,
    0xD9,
    0x52,
    0xD4,
    0x19,
    0x9F,
    0x14,
    0x92,
    0x03,
    0x85,
    0x0E,
    0x88,
    0x2D,
    0xAB,
    0x20,
    0xA6,
    0x37,
    0xB1,
    0x3A,
    0xBC,
    0x43,
    0xC5,
    0x4E,
    0xC8,
    0x59,
    0xDF,
    0x54,
    0xD2,
    0x77,
    0xF1,
    0x7A,
    0xFC,
    0x6D,
    0xEB,
    0x60,
    0xE6,
    0x2B,
    0xAD,
    0x26,
    0xA0,
    0x31,
    0xB7,
    0x3C,
    0xBA,
    0x1F,
    0x99,
    0x12,
    0x94,
    0x05,
    0x83,
    0x08,
    0x8E,
    0x93,
    0x15,
    0x9E,
    0x18,
    0x89,
    0x0F,
    0x84,
    0x02,
    0xA7,
    0x21,
    0xAA,
    0x2C,
    0xBD,
    0x3B,
    0xB0,
    0x36,
    0xFB,
    0x7D,
    0xF6,
    0x70,
    0xE1,
    0x67,
    0xEC,
    0x6A,
    0xCF,
    0x49,
    0xC2,
    0x44,
    0xD5,
    0x53,
    0xD8,
    0x5E,
    0xE2,
    0x64,
    0xEF,
    0x69,
    0xF8,
    0x7E,
    0xF5,
    0x73,
    0xD6,
    0x50,
    0xDB,
    0x5D,
    0xCC,
    0x4A,
    0xC1,
    0x47,
    0x8A,
    0x0C,
    0x87,
    0x01,
    0x90,
    0x16,
    0x9D,
    0x1B,
    0xBE,
    0x38,
    0xB3,
    0x35,
    0xA4,
    0x22,
    0xA9,
    0x2F,
    0x32,
    0xB4,
    0x3F,
    0xB9,
    0x28,
    0xAE,
    0x25,
    0xA3,
    0x06,
    0x80,
    0x0B,
    0x8D,
    0x1C,
    0x9A,
    0x11,
    0x97,
    0x5A,
    0xDC,
    0x57,
    0xD1,
    0x40,
    0xC6,
    0x4D,
    0xCB,
    0x6E,
    0xE8,
    0x63,
    0xE5,
    0x74,
    0xF2,
    0x79,
    0xFF
  ]);

  final Uint8List F1 = Uint8List.fromList([
    0x00,
    0x58,
    0xB0,
    0xE8,
    0x61,
    0x39,
    0xD1,
    0x89,
    0xC2,
    0x9A,
    0x72,
    0x2A,
    0xA3,
    0xFB,
    0x13,
    0x4B,
    0x85,
    0xDD,
    0x35,
    0x6D,
    0xE4,
    0xBC,
    0x54,
    0x0C,
    0x47,
    0x1F,
    0xF7,
    0xAF,
    0x26,
    0x7E,
    0x96,
    0xCE,
    0x0B,
    0x53,
    0xBB,
    0xE3,
    0x6A,
    0x32,
    0xDA,
    0x82,
    0xC9,
    0x91,
    0x79,
    0x21,
    0xA8,
    0xF0,
    0x18,
    0x40,
    0x8E,
    0xD6,
    0x3E,
    0x66,
    0xEF,
    0xB7,
    0x5F,
    0x07,
    0x4C,
    0x14,
    0xFC,
    0xA4,
    0x2D,
    0x75,
    0x9D,
    0xC5,
    0x16,
    0x4E,
    0xA6,
    0xFE,
    0x77,
    0x2F,
    0xC7,
    0x9F,
    0xD4,
    0x8C,
    0x64,
    0x3C,
    0xB5,
    0xED,
    0x05,
    0x5D,
    0x93,
    0xCB,
    0x23,
    0x7B,
    0xF2,
    0xAA,
    0x42,
    0x1A,
    0x51,
    0x09,
    0xE1,
    0xB9,
    0x30,
    0x68,
    0x80,
    0xD8,
    0x1D,
    0x45,
    0xAD,
    0xF5,
    0x7C,
    0x24,
    0xCC,
    0x94,
    0xDF,
    0x87,
    0x6F,
    0x37,
    0xBE,
    0xE6,
    0x0E,
    0x56,
    0x98,
    0xC0,
    0x28,
    0x70,
    0xF9,
    0xA1,
    0x49,
    0x11,
    0x5A,
    0x02,
    0xEA,
    0xB2,
    0x3B,
    0x63,
    0x8B,
    0xD3,
    0x2C,
    0x74,
    0x9C,
    0xC4,
    0x4D,
    0x15,
    0xFD,
    0xA5,
    0xEE,
    0xB6,
    0x5E,
    0x06,
    0x8F,
    0xD7,
    0x3F,
    0x67,
    0xA9,
    0xF1,
    0x19,
    0x41,
    0xC8,
    0x90,
    0x78,
    0x20,
    0x6B,
    0x33,
    0xDB,
    0x83,
    0x0A,
    0x52,
    0xBA,
    0xE2,
    0x27,
    0x7F,
    0x97,
    0xCF,
    0x46,
    0x1E,
    0xF6,
    0xAE,
    0xE5,
    0xBD,
    0x55,
    0x0D,
    0x84,
    0xDC,
    0x34,
    0x6C,
    0xA2,
    0xFA,
    0x12,
    0x4A,
    0xC3,
    0x9B,
    0x73,
    0x2B,
    0x60,
    0x38,
    0xD0,
    0x88,
    0x01,
    0x59,
    0xB1,
    0xE9,
    0x3A,
    0x62,
    0x8A,
    0xD2,
    0x5B,
    0x03,
    0xEB,
    0xB3,
    0xF8,
    0xA0,
    0x48,
    0x10,
    0x99,
    0xC1,
    0x29,
    0x71,
    0xBF,
    0xE7,
    0x0F,
    0x57,
    0xDE,
    0x86,
    0x6E,
    0x36,
    0x7D,
    0x25,
    0xCD,
    0x95,
    0x1C,
    0x44,
    0xAC,
    0xF4,
    0x31,
    0x69,
    0x81,
    0xD9,
    0x50,
    0x08,
    0xE0,
    0xB8,
    0xF3,
    0xAB,
    0x43,
    0x1B,
    0x92,
    0xCA,
    0x22,
    0x7A,
    0xB4,
    0xEC,
    0x04,
    0x5C,
    0xD5,
    0x8D,
    0x65,
    0x3D,
    0x76,
    0x2E,
    0xC6,
    0x9E,
    0x17,
    0x4F,
    0xA7,
    0xFF
  ]);

  final int BLOCK_SIZE_HIGHT = 8;
  final int BLOCK_SIZE_HIGHT_INT = 2;

  ///플러터에서의 String 변환용 함수 (String -> byte)
  static Uint8List stringToByteArray(String input) {
    return Uint8List.fromList(utf8.encode(input));
  }

  ///플러터에서의 String 변환용 함수 (byte -> String)
  static String byteArrayToString(Uint8List input) {
    return utf8.decode(input, allowMalformed: true);
  }

  final Uint8List Delta = Uint8List.fromList([
    0x5a,
    0x6d,
    0x36,
    0x1b,
    0x0d,
    0x06,
    0x03,
    0x41,
    0x60,
    0x30,
    0x18,
    0x4c,
    0x66,
    0x33,
    0x59,
    0x2c,
    0x56,
    0x2b,
    0x15,
    0x4a,
    0x65,
    0x72,
    0x39,
    0x1c,
    0x4e,
    0x67,
    0x73,
    0x79,
    0x3c,
    0x5e,
    0x6f,
    0x37,
    0x5b,
    0x2d,
    0x16,
    0x0b,
    0x05,
    0x42,
    0x21,
    0x50,
    0x28,
    0x54,
    0x2a,
    0x55,
    0x6a,
    0x75,
    0x7a,
    0x7d,
    0x3e,
    0x5f,
    0x2f,
    0x17,
    0x4b,
    0x25,
    0x52,
    0x29,
    0x14,
    0x0a,
    0x45,
    0x62,
    0x31,
    0x58,
    0x6c,
    0x76,
    0x3b,
    0x1d,
    0x0e,
    0x47,
    0x63,
    0x71,
    0x78,
    0x7c,
    0x7e,
    0x7f,
    0x3f,
    0x1f,
    0x0f,
    0x07,
    0x43,
    0x61,
    0x70,
    0x38,
    0x5c,
    0x6e,
    0x77,
    0x7b,
    0x3d,
    0x1e,
    0x4f,
    0x27,
    0x53,
    0x69,
    0x34,
    0x1a,
    0x4d,
    0x26,
    0x13,
    0x49,
    0x24,
    0x12,
    0x09,
    0x04,
    0x02,
    0x01,
    0x40,
    0x20,
    0x10,
    0x08,
    0x44,
    0x22,
    0x11,
    0x48,
    0x64,
    0x32,
    0x19,
    0x0c,
    0x46,
    0x23,
    0x51,
    0x68,
    0x74,
    0x3a,
    0x5d,
    0x2e,
    0x57,
    0x6b,
    0x35,
    0x5a,
  ]);

  void BLOCK_XOR_HIGHT(Int32List source, int out_value_offset, Int32List inValue1, int in_value1_offset, Int32List inValue2, int in_value2_offset) {
    source[out_value_offset + 0] = (in_value1_offset < inValue1.length ? inValue1[in_value1_offset + 0] : 0) ^ (in_value2_offset < inValue2.length ? inValue2[in_value2_offset + 0] : 0);
    source[out_value_offset + 1] = (in_value1_offset + 1 < inValue1.length ? inValue1[in_value1_offset + 1] : 0) ^ (in_value2_offset + 1 < inValue2.length ? inValue2[in_value2_offset + 1] : 0);
  }

  void UpdateCounter_for_HIGHT(Int32List pbOUT, int pbOUT_offset, int nIncreaseValue, int nMin) {
    var bszBackup = 0;
    int i;

    if (0 > nMin) return;

    if (0 < nMin) {
      var b = _Common.get_byte_for_int(pbOUT, pbOUT_offset * 4 + nMin, ENDIAN);
      bszBackup = b & 0x0ff;
      _Common.set_byte_for_int(pbOUT, pbOUT_offset * 4 + nMin, (b + nIncreaseValue), ENDIAN);
    }

    for (i = nMin; i > 1; --i) {
      if (bszBackup <= ((_Common.get_byte_for_int(pbOUT, pbOUT_offset * 4 + i, ENDIAN)) & 0x0ff)) {
        return;
      } else {
        var b = _Common.get_byte_for_int(pbOUT, pbOUT_offset * 4 + i - 1, ENDIAN);
        bszBackup = b & 0x0ff;
        _Common.set_byte_for_int(pbOUT, pbOUT_offset * 4 + i - 1, (b + 1), ENDIAN);
      }
    }

    var b = _Common.get_byte_for_int(pbOUT, pbOUT_offset * 4 + 0, ENDIAN);
    bszBackup = b & 0x0ff;
    _Common.set_byte_for_int(pbOUT, pbOUT_offset * 4 + 0, (b + nIncreaseValue), ENDIAN);
  }

  void EncIni_Transformation(Int32List t, int x0, int x2, int x4, int x6, int mk0, int mk1, int mk2, int mk3) {
    t[0] = ((0x0ff & x0) + (0x0ff & mk0)) & 0x0ff;
    t[2] = ((0x0ff & x2) ^ (0x0ff & mk1)) & 0x0ff;
    t[4] = ((0x0ff & x4) + (0x0ff & mk2)) & 0x0ff;
    t[6] = ((0x0ff & x6) ^ (0x0ff & mk3)) & 0x0ff;
  }

  void EncFin_Transformation(Uint8List out, int x0, int x2, int x4, int x6, int mk0, int mk1, int mk2, int mk3) {
    out[0] = (x0 + mk0);
    out[2] = (x2 ^ mk1);
    out[4] = (x4 + mk2);
    out[6] = (x6 ^ mk3);
  }

  void Round(Int32List x, int i7, int i6, int i5, int i4, int i3, int i2, int i1, int i0, Uint8List key, int key_offset) {
    x[i1] = (x[i1] + ((F1[x[i0]] ^ key[key_offset + 0]) & 0x0ff)) & 0x0ff;
    x[i3] = (x[i3] ^ ((F0[x[i2]] + key[key_offset + 1]) & 0x0ff)) & 0x0ff;
    x[i5] = (x[i5] + ((F1[x[i4]] ^ key[key_offset + 2]) & 0x0ff)) & 0x0ff;
    x[i7] = (x[i7] ^ ((F0[x[i6]] + key[key_offset + 3]) & 0x0ff)) & 0x0ff;
  }

  void KISA_HIGHT_ECB_encrypt_forCTR(Uint8List pbszIN_Key128, Uint8List pbszUserKey, Int32List inValue, int in_offset, Int32List out, int out_offset) {
    var in_length = inValue.length - in_offset;
    var out_length = out.length - out_offset;
    var b_in = Uint8List(in_length * 4);
    var b_out = Uint8List(out_length * 4);

    for (var i = 0; i < in_length; i++) {
      _Common.int_to_byte(b_in, i * 4, inValue, in_offset + i, ENDIAN);
    }

    for (var i = 0; i < out_length; i++) {
      _Common.int_to_byte(b_out, i * 4, out, out_offset + i, ENDIAN);
    }

    KISA_HIGHT_ECB_encrypt_forCTR_2(pbszIN_Key128, pbszUserKey, b_in, b_out);

    for (var i = 0; i < in_length; i++) {
      _Common.byte_to_int(inValue, in_offset + i, b_in, i * 4, ENDIAN);
    }

    for (var i = 0; i < out_length; i++) {
      _Common.byte_to_int(out, out_offset + i, b_out, i * 4, ENDIAN);
    }
  }

  void KISA_HIGHT_ECB_encrypt_forCTR_2(Uint8List pbszIN_Key128, Uint8List pbszUserKey, final Uint8List inValue, Uint8List out) {
    var t = Int32List.fromList([0, 0, 0, 0, 0, 0, 0, 0]);
    Uint8List key, key2;
    var key_offset = 0;

    key = pbszIN_Key128;
    key2 = pbszUserKey;

    t[1] = inValue[1];
    t[3] = inValue[3];
    t[5] = inValue[5];
    t[7] = inValue[7];
    EncIni_Transformation(t, inValue[0], inValue[2], inValue[4], inValue[6], key2[12], key2[13], key2[14], key2[15]);

    Round(t, 7, 6, 5, 4, 3, 2, 1, 0, key, key_offset);
    key_offset += 4; // 1
    Round(t, 6, 5, 4, 3, 2, 1, 0, 7, key, key_offset);
    key_offset += 4; // 2
    Round(t, 5, 4, 3, 2, 1, 0, 7, 6, key, key_offset);
    key_offset += 4; // 3
    Round(t, 4, 3, 2, 1, 0, 7, 6, 5, key, key_offset);
    key_offset += 4; // 4
    Round(t, 3, 2, 1, 0, 7, 6, 5, 4, key, key_offset);
    key_offset += 4; // 5
    Round(t, 2, 1, 0, 7, 6, 5, 4, 3, key, key_offset);
    key_offset += 4; // 6
    Round(t, 1, 0, 7, 6, 5, 4, 3, 2, key, key_offset);
    key_offset += 4; // 7
    Round(t, 0, 7, 6, 5, 4, 3, 2, 1, key, key_offset);
    key_offset += 4; // 8
    Round(t, 7, 6, 5, 4, 3, 2, 1, 0, key, key_offset);
    key_offset += 4; // 9
    Round(t, 6, 5, 4, 3, 2, 1, 0, 7, key, key_offset);
    key_offset += 4; // 10
    Round(t, 5, 4, 3, 2, 1, 0, 7, 6, key, key_offset);
    key_offset += 4; // 11
    Round(t, 4, 3, 2, 1, 0, 7, 6, 5, key, key_offset);
    key_offset += 4; // 12
    Round(t, 3, 2, 1, 0, 7, 6, 5, 4, key, key_offset);
    key_offset += 4; // 13
    Round(t, 2, 1, 0, 7, 6, 5, 4, 3, key, key_offset);
    key_offset += 4; // 14
    Round(t, 1, 0, 7, 6, 5, 4, 3, 2, key, key_offset);
    key_offset += 4; // 15
    Round(t, 0, 7, 6, 5, 4, 3, 2, 1, key, key_offset);
    key_offset += 4; // 16
    Round(t, 7, 6, 5, 4, 3, 2, 1, 0, key, key_offset);
    key_offset += 4; // 17
    Round(t, 6, 5, 4, 3, 2, 1, 0, 7, key, key_offset);
    key_offset += 4; // 18
    Round(t, 5, 4, 3, 2, 1, 0, 7, 6, key, key_offset);
    key_offset += 4; // 19
    Round(t, 4, 3, 2, 1, 0, 7, 6, 5, key, key_offset);
    key_offset += 4; // 20
    Round(t, 3, 2, 1, 0, 7, 6, 5, 4, key, key_offset);
    key_offset += 4; // 21
    Round(t, 2, 1, 0, 7, 6, 5, 4, 3, key, key_offset);
    key_offset += 4; // 22
    Round(t, 1, 0, 7, 6, 5, 4, 3, 2, key, key_offset);
    key_offset += 4; // 23
    Round(t, 0, 7, 6, 5, 4, 3, 2, 1, key, key_offset);
    key_offset += 4; // 24
    Round(t, 7, 6, 5, 4, 3, 2, 1, 0, key, key_offset);
    key_offset += 4; // 25
    Round(t, 6, 5, 4, 3, 2, 1, 0, 7, key, key_offset);
    key_offset += 4; // 26
    Round(t, 5, 4, 3, 2, 1, 0, 7, 6, key, key_offset);
    key_offset += 4; // 27
    Round(t, 4, 3, 2, 1, 0, 7, 6, 5, key, key_offset);
    key_offset += 4; // 28
    Round(t, 3, 2, 1, 0, 7, 6, 5, 4, key, key_offset);
    key_offset += 4; // 29
    Round(t, 2, 1, 0, 7, 6, 5, 4, 3, key, key_offset);
    key_offset += 4; // 30
    Round(t, 1, 0, 7, 6, 5, 4, 3, 2, key, key_offset);
    key_offset += 4; // 31
    Round(t, 0, 7, 6, 5, 4, 3, 2, 1, key, key_offset); // 32

    EncFin_Transformation(out, t[1], t[3], t[5], t[7], key2[0], key2[1], key2[2], key2[3]);

    out[1] = t[2];
    out[3] = t[4];
    out[5] = t[6];
    out[7] = t[0];
  }

  Int32List chartoint32_for_HIGHT_CTR(Uint8List inValue, int inLen) {
    Int32List data;
    int len, i;

    if (inLen % 4 > 0) {
      len = ((inLen / 4) + 1).floor();
    } else {
      len = (inLen / 4).floor();
    }

    data = Int32List(len);

    for (i = 0; i < len; i++) {
      _Common.byte_to_int(data, i, inValue, i * 4, ENDIAN);
    }

    return data;
  }

  Uint8List int32tochar_for_HIGHT_CTR(Int32List inValue, int inLen) {
    Uint8List data;
    int i;

    data = Uint8List(inLen);
    if (ENDIAN != BIG_ENDIAN) {
      for (i = 0; i < inLen; i++) {
        data[i] = (inValue[(i / 4).floor()] >> ((i % 4) * 8));
      }
    } else {
      for (i = 0; i < inLen; i++) {
        data[i] = (inValue[(i / 4).floor()] >> ((3 - (i % 4)) * 8));
      }
    }

    return data;
  }

  /// HIGHT CTR 초기화 함수
  ///
  /// [pInfo] 알고리즘 운영을 위한 클래스, [enc] 암복호화 모드 지정, [pUserKey] 암호화 키(16 bytes), [pbszCTR] 초기화 벡터(8 bytes)
  void HIGHT_CTR_init(KISA_HIGHT_INFO pInfo, KISA_ENC_DEC enc, Uint8List pUserKey, Uint8List pbszCTR) {
    int i, j;

    pInfo.encrypt = enc.value;
    _Common.memcpy(pInfo.ivec, pbszCTR, 8, ENDIAN);
    _Common.arraycopy(pInfo.userKey, pUserKey, 16);

    for (i = 0; i < BLOCK_SIZE_HIGHT; i++) {
      for (j = 0; j < BLOCK_SIZE_HIGHT; j++) {
        pInfo.hight_key.key_data[16 * i + j] = (pUserKey[(j - i) & 7] + Delta[16 * i + j]);
      }

      for (j = 0; j < BLOCK_SIZE_HIGHT; j++) {
        pInfo.hight_key.key_data[16 * i + j + 8] = (pUserKey[((j - i) & 7) + 8] + Delta[16 * i + j + 8]);
      }
    }
  }

  /// HIGHT CTR 암복호화 함수
  ///
  /// [pInfo] [HIGHT_CTR_init]으로 초기화한 알고리즘 운영을 위한 클래스, [inValue] 입력하는 값, [inLen] 입력값의 길이, [out] 출력되는 값, [outLen] 출력값에 저장된 데이터 길이
  void HIGHT_CTR_Process(
    KISA_HIGHT_INFO? pInfo,
    Int32List? inValue,
    int inLen,
    Int32List? out,
    Int32List outLen,
  ) {
    Int32List pdwCounter;
    var nCurrentCount = 0;
    var in_offset = 0;
    var out_offset = 0;
    var pdwCounter_offset = 0;

    if (null == pInfo || null == inValue || null == out || 0 > inLen) return;

    pdwCounter = pInfo.ivec;

    while (nCurrentCount < inLen) {
      KISA_HIGHT_ECB_encrypt_forCTR(pInfo.hight_key.key_data, pInfo.userKey, pdwCounter, pdwCounter_offset, out, out_offset);
      BLOCK_XOR_HIGHT(out, out_offset, inValue, in_offset, out, out_offset);

      UpdateCounter_for_HIGHT(pdwCounter, pdwCounter_offset, 1, (BLOCK_SIZE_HIGHT - 1));
      nCurrentCount += BLOCK_SIZE_HIGHT;
      in_offset += BLOCK_SIZE_HIGHT_INT;
      out_offset += BLOCK_SIZE_HIGHT_INT;
    }

    outLen[0] = nCurrentCount;
    pInfo.buffer_length = inLen - outLen[0];
  }

  /// HIGHT CTR 종료 및 패딩 처리 함수
  ///
  /// [pInfo] [HIGHT_CTR_init]으로 초기화한 알고리즘 운영을 위한 클래스, [out] 최종 출력 블록이 저장되는 버퍼, [out_offset] 출력 버퍼의 시작 오프셋, [outLen] 출력 버퍼에 저장된 데이터의 길이
  void HIGHT_CTR_Close(KISA_HIGHT_INFO pInfo, Int32List out, int out_offset, Int32List outLen) {
    var nPaddngLeng = -(pInfo.buffer_length);
    int i;

    for (i = nPaddngLeng; i > 0; i--) {
      _Common.set_byte_for_int(out, out_offset - i, 0x00, ENDIAN);
    }
    outLen[0] = nPaddngLeng;
  }

  /// HIGHT CTR 암호화 함수
  ///
  /// [pbszUserKey] 암호화 키(16 bytes), [pbszCTR] 초기화 벡터(8 bytes), [message] 암호화할 메시지, [message_offset] 메시지 시작 오프셋, [message_length] 메시지 길이
  Uint8List HIGHT_CTR_Encrypt(Uint8List pbszUserKey, Uint8List pbszCTR, Uint8List message, int message_offset, int message_length) {
    var nOutLeng = Int32List.fromList([0]);
    var nPaddingLeng = Int32List.fromList([0]);
    var info = KISA_HIGHT_INFO();
    Int32List outbuf;
    Int32List data;
    Uint8List cdata;
    var outlen = 0;

    var nInputTextPadding = (BLOCK_SIZE_HIGHT - (message_length % BLOCK_SIZE_HIGHT)) % BLOCK_SIZE_HIGHT;
    var newpbszInputText = Uint8List(message_length + nInputTextPadding);
    //System.arraycopy(message, message_offset, newpbszInputText, 0, message_length);
    List.copyRange(newpbszInputText, 0, message, message_offset, message_length);

    var pbszOutputText = Uint8List(message_length);

    HIGHT_CTR_init(info, KISA_ENC_DEC.KISA_ENCRYPT, pbszUserKey, pbszCTR);

    outlen = (((newpbszInputText.length / BLOCK_SIZE_HIGHT)) * BLOCK_SIZE_HIGHT_INT).floor();
    outbuf = Int32List(outlen);
    data = chartoint32_for_HIGHT_CTR(newpbszInputText, message_length);

    HIGHT_CTR_Process(info, data, message_length, outbuf, nOutLeng);

    HIGHT_CTR_Close(info, outbuf, nOutLeng[0], nPaddingLeng);

    cdata = int32tochar_for_HIGHT_CTR(outbuf, nOutLeng[0] - nPaddingLeng[0]);
    _Common.arraycopy(pbszOutputText, cdata, nOutLeng[0] - nPaddingLeng[0]);

    //data.clear();
    //cdata.clear();
    //outbuf.clear();

    return pbszOutputText;
  }

  /// HIGHT CTR 복호화 함수
  ///
  /// [pbszUserKey] 암호화 키(16 bytes), [pbszCTR] 초기화 벡터(8 bytes), [message] 암호화할 메시지, [message_offset] 메시지 시작 오프셋, [message_length] 메시지 길이
  Uint8List HIGHT_CTR_Decrypt(
    Uint8List pbszUserKey,
    Uint8List pbszCTR,
    Uint8List message,
    int message_offset,
    int message_length,
  ) {
    var nOutLeng = Int32List.fromList([0]);
    var nPaddingLeng = Int32List.fromList([0]);
    var info = KISA_HIGHT_INFO();
    Int32List outbuf;
    Int32List data;
    Uint8List cdata;
    var outlen = 0;

    var nInputTextPadding = (BLOCK_SIZE_HIGHT - (message_length % BLOCK_SIZE_HIGHT)) % BLOCK_SIZE_HIGHT;
    var newpbszInputText = Uint8List(message_length + nInputTextPadding);
    //System.arraycopy(message, message_offset, newpbszInputText, 0, message_length);
    List.copyRange(newpbszInputText, 0, message, message_offset, message_length);

    var pbszOutputText = Uint8List(message_length);

    HIGHT_CTR_init(info, KISA_ENC_DEC.KISA_ENCRYPT, pbszUserKey, pbszCTR);

    outlen = (((newpbszInputText.length / BLOCK_SIZE_HIGHT)) * BLOCK_SIZE_HIGHT_INT).floor();
    outbuf = Int32List(outlen);
    data = chartoint32_for_HIGHT_CTR(newpbszInputText, message_length);

    HIGHT_CTR_Process(info, data, message_length, outbuf, nOutLeng);

    HIGHT_CTR_Close(info, outbuf, nOutLeng[0], nPaddingLeng);

    cdata = int32tochar_for_HIGHT_CTR(outbuf, nOutLeng[0] - nPaddingLeng[0]);
    _Common.arraycopy(pbszOutputText, cdata, nOutLeng[0] - nPaddingLeng[0]);

    //data.clear();
    //cdata.clear();
    //outbuf.clear();

    return pbszOutputText;
  }

  void testHight() {
    var pbUserKey = Uint8List.fromList([0x88, 0xE3, 0x4F, 0x8F, 0x08, 0x17, 0x79, 0xF1, 0xE9, 0xF3, 0x94, 0x37, 0x0A, 0xD4, 0x05, 0x89]);

    // input plaintext to be encrypted
    var pbData = Uint8List.fromList([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x0C, 0x0D, 0x0E, 0x0F]);

    var bszCTR = Uint8List.fromList([0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x0FE]);

    int PLAINTEXT_LENGTH;
    int CIPHERTEXT_LENGTH;

    /**************************************************************************************************
		 * 방법 1
		 **************************************************************************************************/

    PLAINTEXT_LENGTH = CIPHERTEXT_LENGTH = 5;

    stdout.write('[ Test HIGHT reference code CTR]  방법 1 ' '\n');
    stdout.write('\n');
    stdout.write('[ Test Encrypt mode ]' '\n');
    stdout.write('Key\t\t: ');
    for (var i = 0; i < 16; i++) {
      stdout.write(_Integer.toHexString(0xff & pbUserKey[i]) + ' ');
    }
    stdout.write('\n');
    stdout.write('Plaintext\t: ');
    for (var i = 0; i < PLAINTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & pbData[i]) + ' ');
    }
    stdout.write('\n');
    stdout.write('CTR\t\t: ');
    for (var i = 0; i < 8; i++) {
      stdout.write(_Integer.toHexString(0xff & bszCTR[i]) + ' ');
    }
    stdout.write('\n');

    // Encryption

    var defaultCipherText = HIGHT_CTR_Encrypt(pbUserKey, bszCTR, pbData, 0, PLAINTEXT_LENGTH);

    var defaultPlainText = HIGHT_CTR_Decrypt(pbUserKey, bszCTR, defaultCipherText, 0, CIPHERTEXT_LENGTH);

    stdout.write('\n\nCiphertext(Enc)\t: ');
    for (var i = 0; i < CIPHERTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & defaultCipherText[i]) + ' ');
    }
    stdout.write('\n');

    stdout.write('Plaintext(Dec)\t: ');
    for (var i = 0; i < PLAINTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & defaultPlainText[i]) + ' ');
    }
    stdout.write('\n');

    var test_num = 0;
    for (var i = 0; i < 10; i++) {
      defaultCipherText = HIGHT_CTR_Encrypt(pbUserKey, bszCTR, pbData, 0, PLAINTEXT_LENGTH + test_num);
      defaultPlainText = HIGHT_CTR_Decrypt(pbUserKey, bszCTR, defaultCipherText, 0, CIPHERTEXT_LENGTH + test_num);

      stdout.write('\n\nCiphertext(Enc)\t: ');
      for (var j = 0; j < CIPHERTEXT_LENGTH + test_num; j++) {
        stdout.write(_Integer.toHexString(0xff & defaultCipherText[j]) + ' ');
      }
      stdout.write('\n');

      stdout.write('Plaintext(Dec)\t: ');
      for (var j = 0; j < PLAINTEXT_LENGTH + test_num; j++) {
        stdout.write(_Integer.toHexString(0xff & defaultPlainText[j]) + ' ');
      }
      stdout.write('\n');

      test_num++;

      //defaultCipherText.clear();
      //defaultPlainText.clear();
    }

    PLAINTEXT_LENGTH = CIPHERTEXT_LENGTH = 20;

    defaultCipherText = HIGHT_CTR_Encrypt(pbUserKey, bszCTR, pbData, 0, PLAINTEXT_LENGTH);
    defaultPlainText = HIGHT_CTR_Decrypt(pbUserKey, bszCTR, defaultCipherText, 0, CIPHERTEXT_LENGTH);

    stdout.write('\n\nCiphertext(Enc)\t: ');
    for (var i = 0; i < CIPHERTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & defaultCipherText[i]) + ' ');
    }
    stdout.write('\n');

    stdout.write('Plaintext(Dec)\t: ');
    for (var i = 0; i < PLAINTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & defaultPlainText[i]) + ' ');
    }
    stdout.write('\n');

    /**************************************************************************************************
		 * 방법 2
		 **************************************************************************************************/

    /***********************
	     * 테스트 벡터 1
	     ***********************/

    PLAINTEXT_LENGTH = CIPHERTEXT_LENGTH = 5;

    stdout.write('\n\n[ Test HIGHT reference code ]  방법 2 ' '\n');
    stdout.write('\n\n');
    stdout.write('[ Test Encrypt mode ]' '\n');
    stdout.write('Key\t\t: ');
    for (var i = 0; i < 16; i++) {
      stdout.write(_Integer.toHexString(0xff & pbUserKey[i]) + ' ');
    }
    stdout.write('\n');
    stdout.write('Plaintext\t: ');
    for (var i = 0; i < PLAINTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & pbData[i]) + ' ');
    }
    stdout.write('\n');

    var info = KISA_HIGHT_INFO();
    var pdmessage_length = PLAINTEXT_LENGTH;

    var process_blockLeng = 32;
    var outbuf = Int32List(process_blockLeng);

    HIGHT_CTR_init(info, KISA_ENC_DEC.KISA_ENCRYPT, pbUserKey, bszCTR);

    int j;
    Int32List data;
    Uint8List cdata;
    var nRetOutLeng = Int32List.fromList([0]);
    var nPaddingLeng = Int32List.fromList([0]);
    var pbszPlainText = Uint8List(process_blockLeng);
    var pbszCipherText = Uint8List(pdmessage_length);

    for (j = 0; j < pdmessage_length - process_blockLeng;) {
      //System.arraycopy(pbData, j, pbszPlainText, 0, process_blockLeng);
      List.copyRange(pbszPlainText, 0, pbData, j, process_blockLeng);
      data = chartoint32_for_HIGHT_CTR(pbszPlainText, process_blockLeng);

      HIGHT_CTR_Process(info, data, process_blockLeng, outbuf, nRetOutLeng);

      cdata = int32tochar_for_HIGHT_CTR(outbuf, nRetOutLeng[0]);
      //System.arraycopy(cdata, 0, pbszCipherText, j, nRetOutLeng[0]);
      List.copyRange(pbszCipherText, j, cdata, 0, nRetOutLeng[0]);
      j += nRetOutLeng[0];
    }

    var remainleng = pdmessage_length % process_blockLeng;
    if (remainleng == 0) {
      remainleng = process_blockLeng;
    }
    //System.arraycopy(pbData, j, pbszPlainText, 0, remainleng);
    List.copyRange(pbszPlainText, 0, pbData, j, remainleng);
    data = chartoint32_for_HIGHT_CTR(pbszPlainText, remainleng);
    HIGHT_CTR_Process(info, data, remainleng, outbuf, nRetOutLeng);
    HIGHT_CTR_Close(info, outbuf, nRetOutLeng[0], nPaddingLeng);
    cdata = int32tochar_for_HIGHT_CTR(outbuf, nRetOutLeng[0] - nPaddingLeng[0]);
    //System.arraycopy(cdata, 0, pbszCipherText, j, nRetOutLeng[0] - nPaddingLeng[0]);
    List.copyRange(pbszCipherText, j, cdata, 0, nRetOutLeng[0] - nPaddingLeng[0]);
    j += nRetOutLeng[0];

    stdout.write('Ciphertext(Enc)\t: ');
    for (var i = 0; i < CIPHERTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & pbszCipherText[i]) + ' ');
    }
    stdout.write('\n');

    //data.clear();
    //cdata.clear();
    //outbuf.clear();

    /*************
		 * 복호화
		 */

    info = KISA_HIGHT_INFO();
    pdmessage_length = pbszCipherText.length;

    process_blockLeng = 32;
    outbuf = Int32List(process_blockLeng);

    HIGHT_CTR_init(info, KISA_ENC_DEC.KISA_DECRYPT, pbUserKey, bszCTR);

    var cipherText = Uint8List(process_blockLeng);
    pbszPlainText = Uint8List(pdmessage_length);

    for (j = 0; j < pdmessage_length - process_blockLeng;) {
      //System.arraycopy(pbszCipherText, j, cipherText, 0, process_blockLeng);
      List.copyRange(cipherText, 0, pbszCipherText, j, process_blockLeng);
      data = chartoint32_for_HIGHT_CTR(cipherText, process_blockLeng);
      HIGHT_CTR_Process(info, data, process_blockLeng, outbuf, nRetOutLeng);
      cdata = int32tochar_for_HIGHT_CTR(outbuf, nRetOutLeng[0]);
      //System.arraycopy(cdata, 0, pbszPlainText, j, nRetOutLeng[0]);
      List.copyRange(pbszPlainText, j, cdata, 0, nRetOutLeng[0]);
      j += nRetOutLeng[0];
    }

    remainleng = pdmessage_length % process_blockLeng;
    if (remainleng == 0) {
      remainleng = process_blockLeng;
    }
    //System.arraycopy(pbszCipherText, j, cipherText, 0, remainleng);
    List.copyRange(cipherText, 0, pbszCipherText, j, remainleng);
    data = chartoint32_for_HIGHT_CTR(cipherText, remainleng);
    HIGHT_CTR_Process(info, data, remainleng, outbuf, nRetOutLeng);
    HIGHT_CTR_Close(info, outbuf, nRetOutLeng[0], nPaddingLeng);
    cdata = int32tochar_for_HIGHT_CTR(outbuf, nRetOutLeng[0] - nPaddingLeng[0]);
    //System.arraycopy(cdata, 0, pbszPlainText, j, nRetOutLeng[0] - nPaddingLeng[0]);
    List.copyRange(pbszPlainText, j, cdata, 0, nRetOutLeng[0] - nPaddingLeng[0]);
    j += nRetOutLeng[0];

    stdout.write('Plaintext(Dec)\t: ');
    for (var i = 0; i < PLAINTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & pbszPlainText[i]) + ' ');
    }
    stdout.write('\n');

    //data.clear();
    //cdata.clear();
    //outbuf.clear();

    /***********************
	     * 테스트 벡터 2
	     ***********************/

    PLAINTEXT_LENGTH = CIPHERTEXT_LENGTH = 10;

    stdout.write('\nKey\t\t: ');
    for (var i = 0; i < 16; i++) {
      stdout.write(_Integer.toHexString(0xff & pbUserKey[i]) + ' ');
    }
    stdout.write('\n');
    stdout.write('Plaintext\t: ');
    for (var i = 0; i < PLAINTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & pbData[i]) + ' ');
    }
    stdout.write('\n');

    info = KISA_HIGHT_INFO();
    pdmessage_length = PLAINTEXT_LENGTH;

    process_blockLeng = 32;
    outbuf = Int32List(process_blockLeng);

    HIGHT_CTR_init(info, KISA_ENC_DEC.KISA_ENCRYPT, pbUserKey, bszCTR);

    pbszPlainText = Uint8List(process_blockLeng);
    pbszCipherText = Uint8List(pdmessage_length);

    for (j = 0; j < pdmessage_length - process_blockLeng;) {
      //System.arraycopy(pbData, j, pbszPlainText, 0, process_blockLeng);
      List.copyRange(pbszPlainText, 0, pbData, j, process_blockLeng);
      data = chartoint32_for_HIGHT_CTR(pbszPlainText, process_blockLeng);
      HIGHT_CTR_Process(info, data, process_blockLeng, outbuf, nRetOutLeng);
      cdata = int32tochar_for_HIGHT_CTR(outbuf, nRetOutLeng[0]);
      //System.arraycopy(cdata, 0, pbszCipherText, j, nRetOutLeng[0]);
      List.copyRange(pbszCipherText, j, cdata, 0, nRetOutLeng[0]);
      j += nRetOutLeng[0];
    }

    remainleng = pdmessage_length % process_blockLeng;
    if (remainleng == 0) {
      remainleng = process_blockLeng;
    }
    //System.arraycopy(pbData, j, pbszPlainText, 0, remainleng);
    List.copyRange(pbszPlainText, 0, pbData, j, remainleng);
    data = chartoint32_for_HIGHT_CTR(pbszPlainText, remainleng);
    HIGHT_CTR_Process(info, data, remainleng, outbuf, nRetOutLeng);
    HIGHT_CTR_Close(info, outbuf, nRetOutLeng[0], nPaddingLeng);
    cdata = int32tochar_for_HIGHT_CTR(outbuf, nRetOutLeng[0] - nPaddingLeng[0]);
    //System.arraycopy(cdata, 0, pbszCipherText, j, nRetOutLeng[0] - nPaddingLeng[0]);
    List.copyRange(pbszCipherText, j, cdata, 0, nRetOutLeng[0] - nPaddingLeng[0]);
    j += nRetOutLeng[0];

    stdout.write('Ciphertext(Enc)\t: ');
    for (var i = 0; i < CIPHERTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & pbszCipherText[i]) + ' ');
    }
    stdout.write('\n');

    //data.clear();
    //cdata.clear();
    //outbuf.clear();

    /*************
		 * 복호화
		 */

    info = KISA_HIGHT_INFO();
    pdmessage_length = pbszCipherText.length;

    process_blockLeng = 32;
    outbuf = Int32List(process_blockLeng);

    HIGHT_CTR_init(info, KISA_ENC_DEC.KISA_DECRYPT, pbUserKey, bszCTR);

    cipherText = Uint8List(process_blockLeng);
    pbszPlainText = Uint8List(pdmessage_length);

    for (j = 0; j < pdmessage_length - process_blockLeng;) {
      //System.arraycopy(pbszCipherText, j, cipherText, 0, process_blockLeng);
      List.copyRange(cipherText, 0, pbszCipherText, j, process_blockLeng);
      data = chartoint32_for_HIGHT_CTR(cipherText, process_blockLeng);
      HIGHT_CTR_Process(info, data, process_blockLeng, outbuf, nRetOutLeng);
      cdata = int32tochar_for_HIGHT_CTR(outbuf, nRetOutLeng[0]);
      //System.arraycopy(cdata, 0, pbszPlainText, j, nRetOutLeng[0]);
      List.copyRange(pbszPlainText, j, cdata, 0, nRetOutLeng[0]);
      j += nRetOutLeng[0];
    }

    remainleng = pdmessage_length % process_blockLeng;
    if (remainleng == 0) {
      remainleng = process_blockLeng;
    }
    //System.arraycopy(pbszCipherText, j, cipherText, 0, remainleng);
    List.copyRange(cipherText, 0, pbszCipherText, j, remainleng);
    data = chartoint32_for_HIGHT_CTR(cipherText, remainleng);
    HIGHT_CTR_Process(info, data, remainleng, outbuf, nRetOutLeng);
    HIGHT_CTR_Close(info, outbuf, nRetOutLeng[0], nPaddingLeng);
    cdata = int32tochar_for_HIGHT_CTR(outbuf, nRetOutLeng[0] - nPaddingLeng[0]);
    //System.arraycopy(cdata, 0, pbszPlainText, j, nRetOutLeng[0] - nPaddingLeng[0]);
    List.copyRange(pbszPlainText, j, cdata, 0, nRetOutLeng[0] - nPaddingLeng[0]);
    j += nRetOutLeng[0];

    stdout.write('Plaintext(Dec)\t: ');
    for (var i = 0; i < PLAINTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & pbszPlainText[i]) + ' ');
    }
    stdout.write('\n');

    //data.clear();
    //cdata.clear();
    //outbuf.clear();

    /***********************
	     * 테스트 벡터 3
	     ***********************/

    PLAINTEXT_LENGTH = CIPHERTEXT_LENGTH = 19;

    stdout.write('\nKey\t\t: ');
    for (var i = 0; i < 16; i++) {
      stdout.write(_Integer.toHexString(0xff & pbUserKey[i]) + ' ');
    }
    stdout.write('\n');
    stdout.write('Plaintext\t: ');
    for (var i = 0; i < PLAINTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & pbData[i]) + ' ');
    }
    stdout.write('\n');

    info = KISA_HIGHT_INFO();
    pdmessage_length = PLAINTEXT_LENGTH;

    process_blockLeng = 32;
    outbuf = Int32List(process_blockLeng);

    HIGHT_CTR_init(info, KISA_ENC_DEC.KISA_ENCRYPT, pbUserKey, bszCTR);

    pbszPlainText = Uint8List(process_blockLeng);
    pbszCipherText = Uint8List(pdmessage_length);

    for (j = 0; j < pdmessage_length - process_blockLeng;) {
      //System.arraycopy(pbData, j, pbszPlainText, 0, process_blockLeng);
      List.copyRange(pbszPlainText, 0, pbData, j, process_blockLeng);
      data = chartoint32_for_HIGHT_CTR(pbszPlainText, process_blockLeng);
      HIGHT_CTR_Process(info, data, process_blockLeng, outbuf, nRetOutLeng);
      cdata = int32tochar_for_HIGHT_CTR(outbuf, nRetOutLeng[0]);
      //System.arraycopy(cdata, 0, pbszCipherText, j, nRetOutLeng[0]);
      List.copyRange(pbszCipherText, j, cdata, 0, nRetOutLeng[0]);
      j += nRetOutLeng[0];
    }

    remainleng = pdmessage_length % process_blockLeng;
    if (remainleng == 0) {
      remainleng = process_blockLeng;
    }
    //System.arraycopy(pbData, j, pbszPlainText, 0, remainleng);
    List.copyRange(pbszPlainText, 0, pbData, j, remainleng);
    data = chartoint32_for_HIGHT_CTR(pbszPlainText, remainleng);
    HIGHT_CTR_Process(info, data, remainleng, outbuf, nRetOutLeng);
    HIGHT_CTR_Close(info, outbuf, nRetOutLeng[0], nPaddingLeng);
    cdata = int32tochar_for_HIGHT_CTR(outbuf, nRetOutLeng[0] - nPaddingLeng[0]);
    //System.arraycopy(cdata, 0, pbszCipherText, j, nRetOutLeng[0] - nPaddingLeng[0]);
    List.copyRange(pbszCipherText, j, cdata, 0, nRetOutLeng[0] - nPaddingLeng[0]);
    j += nRetOutLeng[0];

    stdout.write('Ciphertext(Enc)\t: ');
    for (var i = 0; i < CIPHERTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & pbszCipherText[i]) + ' ');
    }
    stdout.write('\n');

    //data.clear();
    //cdata.clear();
    //outbuf.clear();

    /*************
		 * 복호화
		 */

    info = KISA_HIGHT_INFO();
    pdmessage_length = pbszCipherText.length;

    process_blockLeng = 32;
    outbuf = Int32List(process_blockLeng);

    HIGHT_CTR_init(info, KISA_ENC_DEC.KISA_DECRYPT, pbUserKey, bszCTR);

    cipherText = Uint8List(process_blockLeng);
    pbszPlainText = Uint8List(pdmessage_length);

    for (j = 0; j < pdmessage_length - process_blockLeng;) {
      //System.arraycopy(pbszCipherText, j, cipherText, 0, process_blockLeng);
      List.copyRange(cipherText, 0, pbszCipherText, j, process_blockLeng);
      data = chartoint32_for_HIGHT_CTR(cipherText, process_blockLeng);
      HIGHT_CTR_Process(info, data, process_blockLeng, outbuf, nRetOutLeng);
      cdata = int32tochar_for_HIGHT_CTR(outbuf, nRetOutLeng[0]);
      //System.arraycopy(cdata, 0, pbszPlainText, j, nRetOutLeng[0]);
      List.copyRange(pbszPlainText, j, cdata, 0, nRetOutLeng[0]);
      j += nRetOutLeng[0];
    }

    remainleng = pdmessage_length % process_blockLeng;
    if (remainleng == 0) {
      remainleng = process_blockLeng;
    }
    //System.arraycopy(pbszCipherText, j, cipherText, 0, remainleng);
    List.copyRange(cipherText, 0, pbszCipherText, j, remainleng);
    data = chartoint32_for_HIGHT_CTR(cipherText, remainleng);
    HIGHT_CTR_Process(info, data, remainleng, outbuf, nRetOutLeng);
    HIGHT_CTR_Close(info, outbuf, nRetOutLeng[0], nPaddingLeng);
    cdata = int32tochar_for_HIGHT_CTR(outbuf, nRetOutLeng[0] - nPaddingLeng[0]);
    //System.arraycopy(cdata, 0, pbszPlainText, j, nRetOutLeng[0] - nPaddingLeng[0]);
    List.copyRange(pbszPlainText, j, cdata, 0, nRetOutLeng[0] - nPaddingLeng[0]);
    j += nRetOutLeng[0];

    stdout.write('Plaintext(Dec)\t: ');
    for (var i = 0; i < PLAINTEXT_LENGTH; i++) {
      stdout.write(_Integer.toHexString(0xff & pbszPlainText[i]) + ' ');
    }
    stdout.write('\n');

    //data.clear();
    //cdata.clear();
    //outbuf.clear();
  }
}

///암호화, 복호화 선택용 클래스
class KISA_ENC_DEC {
  int value;

  KISA_ENC_DEC(this.value);

  ///암호화
  static final KISA_ENC_DEC KISA_ENCRYPT = KISA_ENC_DEC(_KISA_ENCRYPT);

  ///복호화
  static final KISA_ENC_DEC KISA_DECRYPT = KISA_ENC_DEC(_KISA_DECRYPT);
}

class KISA_HIGHT_KEY {
  Uint8List key_data = Uint8List(128);

  void init() {
    for (var i = 0; i < key_data.length; i++) {
      key_data[i] = 0;
    }
  }
}

/// HIGHT CTR 알고리즘 운영용 클래스
class KISA_HIGHT_INFO {
  late int encrypt;
  Int32List ivec = Int32List(2);
  KISA_HIGHT_KEY hight_key = KISA_HIGHT_KEY();
  Uint8List userKey = Uint8List(16);
  Int32List cbc_buffer = Int32List(2);
  late int buffer_length;
  Int32List cbc_last_block = Int32List(2);

  KISA_HIGHT_INFO() {
    encrypt = 0;
    ivec[0] = 0;
    ivec[1] = 0;
    hight_key.init();
    userKey[0] = 0;
    userKey[1] = 0;
    userKey[2] = 0;
    userKey[3] = 0;
    userKey[4] = 0;
    userKey[5] = 0;
    userKey[6] = 0;
    userKey[7] = 0;
    userKey[8] = 0;
    userKey[9] = 0;
    userKey[10] = 0;
    userKey[11] = 0;
    userKey[12] = 0;
    userKey[13] = 0;
    userKey[14] = 0;
    userKey[15] = 0;
    cbc_buffer[0] = cbc_buffer[1] = 0;
    buffer_length = 0;
    cbc_last_block[0] = cbc_last_block[1] = 0;
  }
}

class _Common {
  static void arraycopy(Uint8List dst, Uint8List src, int length) {
    for (var i = 0; i < length; i++) {
      dst[i] = src[i];
    }
  }

  static void memcpy(Int32List dst, Uint8List src, int length, Endian endian) {
    var iLen = (length / 4).floor();
    for (var i = 0; i < iLen; i++) {
      byte_to_int(dst, i, src, i * 4, endian);
    }
  }

  static void set_byte_for_int(Int32List dst, int b_offset, int value, Endian endian) {
    if (endian == BIG_ENDIAN) {
      var shift_value = (3 - b_offset % 4) * 8;
      var mask_value = 0x0ff << shift_value;
      var mask_value2 = ~mask_value;
      var value2 = (value & 0x0ff) << shift_value;
      dst[(b_offset / 4).floor()] = (dst[(b_offset / 4).floor()] & mask_value2) | (value2 & mask_value);
    } else {
      var shift_value = (b_offset % 4) * 8;
      var mask_value = 0x0ff << shift_value;
      var mask_value2 = ~mask_value;
      var value2 = (value & 0x0ff) << shift_value;
      dst[(b_offset / 4).floor()] = (dst[(b_offset / 4).floor()] & mask_value2) | (value2 & mask_value);
    }
  }

  static int get_byte_for_int(Int32List src, int b_offset, Endian endian) {
    if (endian == BIG_ENDIAN) {
      var shift_value = (3 - b_offset % 4) * 8;
      var mask_value = 0x0ff << shift_value;
      var value = (src[(b_offset / 4).floor()] & mask_value) >> shift_value;
      return value;
    } else {
      var shift_value = (b_offset % 4) * 8;
      var mask_value = 0x0ff << shift_value;
      var value = (src[(b_offset / 4).floor()] & mask_value) >> shift_value;
      return value;
    }
  }

  static void byte_to_int(Int32List dst, int dst_offset, Uint8List src, int src_offset, Endian endian) {
    if (endian == BIG_ENDIAN) {
      dst[dst_offset] = ((0x0ff & src[src_offset]) << 24) | ((0x0ff & src[src_offset + 1]) << 16) | ((0x0ff & src[src_offset + 2]) << 8) | ((0x0ff & src[src_offset + 3]));
    } else {
      dst[dst_offset] = ((0x0ff & src[src_offset])) | ((0x0ff & src[src_offset + 1]) << 8) | ((0x0ff & src[src_offset + 2]) << 16) | ((0x0ff & src[src_offset + 3]) << 24);
    }
  }

  static void int_to_byte(Uint8List dst, int dst_offset, Int32List src, int src_offset, Endian endian) {
    int_to_byte_unit(dst, dst_offset, src[src_offset], endian);
  }

  static void int_to_byte_unit(Uint8List dst, int dst_offset, int src, Endian endian) {
    if (endian == BIG_ENDIAN) {
      dst[dst_offset] = ((src >> 24) & 0x0ff);
      dst[dst_offset + 1] = ((src >> 16) & 0x0ff);
      dst[dst_offset + 2] = ((src >> 8) & 0x0ff);
      dst[dst_offset + 3] = ((src) & 0x0ff);
    } else {
      dst[dst_offset] = ((src) & 0x0ff);
      dst[dst_offset + 1] = ((src >> 8) & 0x0ff);
      dst[dst_offset + 2] = ((src >> 16) & 0x0ff);
      dst[dst_offset + 3] = ((src >> 24) & 0x0ff);
    }
  }
}

class _Integer {
  static String toHexString(int i) {
    return i.toRadixString(16);
  }
}
