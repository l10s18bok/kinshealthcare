import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/services.dart';

/*
 작성일 : 2021-02-23
 작성자 : Victor
 화면명 : 
 클래스 : HapticUtils
 경로 : 
 설명 : 기기 진동 처리를 위한 클래스
*/

///기기 진동 처리를 위한 클래스
class HapticController {
  static const MethodChannel _channel = const MethodChannel('haptic_controller');

  //getter & setter
  ///햅틱 시간 읽기
  static Future<double> get hapticTime async {
    final double result = await _channel.invokeMethod('hapticTime');
    return result;
  }

  ///햅틱 시간 설정
  static void setHapticTime(double value) async {
    await _channel.invokeMethod('hapticTime', {'set': value});
  }

  ///햅틱 강도 읽기
  static Future<double> get hapticIntensity async {
    final double result = await _channel.invokeMethod('hapticIntensity');
    return result;
  }

  ///햅틱 강도 설정
  static void setHapticIntensity(double value) {
    _channel.invokeMethod('hapticIntensity', {'set': value});
  }

  ///햅틱 가능 여부 확인
  static Future<bool> get canHaptic async {
    final bool result = await _channel.invokeMethod('canHaptic');
    return result;
  }

  //functions
  ///단순 햅틱 실행을 원할때 호출한다.
  ///햅틱 설정 바꾸고 싶으면 [setHapticTime]과 [setHapticIntensity] 사용할 것.
  static void haptic() async {
    await _channel.invokeMethod('haptic');
  }

  ///햅틱을 연속해서 패턴으로 재생시킬려면 이 함수를 사용한다. 입력받는 3개의 배열 길이는 맞추는게 좋다. (네이티브에서 어느정도 방지 처리는 했음)
  ///
  ///[delayTime]은 몇초 후에 패턴이 재생될지를 정하고 [duration]은 햅틱 재생 길이를 정한다. [intensities]는 햅틱 강도를 결정한다
  ///
  ///4/20 기준으로 delayTime의 간격보다 duration이 크면 논리적 오류가 발생하므로 주의할것. 필요하면 네이티브 삭제하고 해당 문구 삭제 요망.
  ///
  ///OS 버전이나 기기 종류에 따라 지원 안될수도 있으니 [hapticTime]으로 체크하고 필요하다면 체크용 변수를 네이티브에 추가하고 연결해서 사용 요망.
  static void hapticPattern({required List<double> delayTime, List<double> duration = const [0.1], List<double> intensities = const [1]}) async {
    await _channel.invokeMethod(
        'hapticPattern', <String, Float64List>{'delayTime': Float64List.fromList(delayTime), 'duration': Float64List.fromList(duration), 'intensities': Float64List.fromList(intensities)});
  }
}

//이하 네이티브 코드 연동 혹은 수정하려는 사람을 위한 설명
//일단 이거 읽으세요
//https://flutter-ko.dev/docs/development/platform-integration/platform-channels
//
//이 코드와 연결되는 코드 위치는 다음과 같음
//Android : android/app/src/main/kotlin/com/example/spinor_app/MainActivity.kt
//iOS : ios/Runner/AppDelegate.swift
//
//실제 네이티브 코드는 다음 위치에 있음
//Android : android/app/src/main/kotlin/com/example/spinor_app/Haptic.kt
//iOS : ios/Runner/Haptic/Haptic.swift
//
//플러그인으로 별도로 관리하려고 했는데 공식 플러그인 페이지에 출시 안하고 뽑는 방법을 몰라서 걍 프로젝트에 집어넣음
//뺄줄 알고 빼고싶으면 빼서 연결해도 됨
