import 'package:json_annotation/json_annotation.dart';

class LocalModel {
  String userRelCdOpponentValue;
  String userNameOpponent;
  String userPhoneOpponent;
  @JsonKey(name: 'userStatusOpponent')
  String _userStatusOpponent;
  bool isChecked;

  // final _userStatusOpponentMap = {
  //   'ACTIVE': true,
  // };

  LocalModel({
    required this.userRelCdOpponentValue,
    required this.userNameOpponent,
    required this.userPhoneOpponent,
    required String userStatusOpponent,
    this.isChecked = false,
  }) : this._userStatusOpponent = userStatusOpponent;

  get userStatusOpponent {
    Map<String, bool> dict = {
      'ACTIVE': true,
    };
    return dict[this._userStatusOpponent];
  }

// get userStatusOpponent {
//   Map<UserStatus, bool> dict = {
//     UserStatus.ACTIVE : true,
//     UserStatus.DROP : false,
//     UserStatus.WITHDRAW : false,
//   };

// switch(this._userStatusOpponent){
//   case 'ACTIVE':
//     return UserStatus.ACTIVE;
//   case 'DROP':
//     return UserStatus.DROP;
//   case 'WITHDRAW':
//     return UserStatus.WITHDRAW;
//   default:
//     return false;
// }

// return dict[this._userStatusOpponent];
}
