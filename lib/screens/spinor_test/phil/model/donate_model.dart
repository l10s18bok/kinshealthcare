class DonateModel {
  int? id;
  String title;
  String subTitle;
  String iconPath;
  String treeLevel;
  double percent;

  DonateModel({
    this.id,
    required this.title,
    required this.subTitle,
    required this.iconPath,
    required this.treeLevel,
    required this.percent,
  });
}
