import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/**
 * @author  김민석
 * @version 1.0
 * @since   2021-01-25
 */

class SubTitle extends StatefulWidget {
  final String title;

  SubTitle({
    required String title,
  }) : this.title = title;

  @override
  _SubTitleState createState() => _SubTitleState();
}

class _SubTitleState extends State<SubTitle> {
  // @override
  // void initState() {
  //   super.initState();
  // }

  subText() {
    final theme = ThemeFactory.of(context).theme;

    return Text(widget.title,
        style: TextStyle(
            color: Color(0xff303030),
            fontWeight: FontWeight.w700,
            fontFamily: "AppleSDGothicNeo",
            fontStyle: FontStyle.normal,
            fontSize: theme.fontSize17));
  }

  @override
  Widget build(BuildContext context) {
    return subText();
  }
}
