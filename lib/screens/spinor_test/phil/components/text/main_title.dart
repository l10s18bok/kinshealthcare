import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/**
 * @author  김민석
 * @version 1.0
 * @since   2021-01-25
 */

class MainTitle extends StatefulWidget {
  final String title;

  MainTitle({
    required String title,
  }) : this.title = title;

  @override
  _MainTitleState createState() => _MainTitleState();
}

class _MainTitleState extends State<MainTitle> {
  // @override
  // void initState() {
  //   super.initState();
  // }

  mainText() {
    final theme = ThemeFactory.of(context).theme;

    return Text(widget.title,
        style: TextStyle(
            color: Color(0xff000000),
            fontWeight: FontWeight.w700,
            fontFamily: "AppleSDGothicNeo",
            fontStyle: FontStyle.normal,
            fontSize: theme.fontSize22));
  }

  @override
  Widget build(BuildContext context) {
    return mainText();
  }
}
