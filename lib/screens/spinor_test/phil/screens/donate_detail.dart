// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:kins_healthcare/layouts/title_layout.dart';
// import 'package:kins_healthcare/screens/spinor_test/phil/components/text/sub_title.dart';
// import 'package:kins_healthcare/themes/theme_factory.dart';
// import 'package:kins_healthcare/components/button/button.dart';
// import 'package:get/get.dart';
//
// class DonateDetail extends StatefulWidget {
//   @override
//   _DonateDetailState createState() => _DonateDetailState();
// }
//
// class _DonateDetailState extends State<DonateDetail> {
//   ScrollController topListViewController;
//   GlobalKey topListViewKey;
//
//   double tLVScrollPercent;
//   double scrollWidth;
//   double scrollXPos;
//
//   @override
//   void initState() {
//     super.initState();
//
//     topListViewKey = GlobalKey();
//
//     tLVScrollPercent = 0;
//     topListViewController = ScrollController();
//     scrollWidth = 50.0;
//     scrollXPos = 0;
//
//     topListViewController.addListener(() {
//       final box = topListViewKey.currentContext.findRenderObject() as RenderBox;
//       final tWidth = box.size.width;
//
//       double perc = topListViewController.offset /
//           topListViewController.position.maxScrollExtent;
//
//       if (perc < 0) {
//         perc = 0;
//       }
//
//       if (perc > 1) {
//         perc = 1;
//       }
//
//       setState(() {
//         tLVScrollPercent = perc;
//         scrollXPos = (tWidth - scrollWidth) * perc;
//       });
//     });
//   }
//
//   renderDonateDetail() {
//     final rootPath = 'assets/svgs/ic/';
//     final theme = ThemeFactory.of(context).theme;
//
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//       children: [
//         Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Text('사랑의 열매',
//                 style: TextStyle(
//                   color: Color(0xff4042ab),
//                   fontWeight: FontWeight.w700,
//                   fontFamily: "AppleSDGothicNeo",
//                   fontStyle: FontStyle.normal,
//                   fontSize: theme.fontSize17,
//                 )),
//             Container(height: 4),
//             Text('결식아동을 도웁시다',
//                 style: TextStyle(
//                   color: Color(0xff3e3e3e),
//                   fontWeight: FontWeight.w400,
//                   fontFamily: "AppleSDGothicNeo",
//                   fontStyle: FontStyle.normal,
//                   fontSize: theme.fontSize11,
//                 )),
//           ],
//         ),
//         Column(
//           children: [
//             SvgPicture.asset(
//               rootPath + 'ic_tree_level_01.svg',
//               height: 37,
//               width: 37,
//             ),
//             Container(height: 4),
//             Text("3단계",
//                 style: TextStyle(
//                   color: Color(0xff3e3e3e),
//                   fontWeight: FontWeight.w700,
//                   fontFamily: "AppleSDGothicNeo",
//                   fontStyle: FontStyle.normal,
//                   fontSize: theme.fontSize10,
//                 ),
//                 textAlign: TextAlign.center)
//           ],
//         )
//       ],
//     );
//   }
//
//   renderDonatePrice() {
//     final theme = ThemeFactory.of(context).theme;
//
//     return Column(
//       children: [
//         Row(
//           children: [
//             Text("모은 금액",
//                 style: TextStyle(
//                     color: Color(0xff303030),
//                     fontWeight: FontWeight.w700,
//                     fontFamily: "AppleSDGothicNeo",
//                     fontStyle: FontStyle.normal,
//                     fontSize: theme.fontSize12)),
//             Container(width: 8),
//             Text("30,000원",
//                 style: TextStyle(
//                     color: Color(0xff303030),
//                     fontWeight: FontWeight.w400,
//                     fontFamily: "AppleSDGothicNeo",
//                     fontStyle: FontStyle.normal,
//                     fontSize: theme.fontSize12)),
//           ],
//         ),
//         Container(height: 6),
//         Row(
//           children: [
//             Text("목표 금액",
//                 style: TextStyle(
//                     color: Color(0xff303030),
//                     fontWeight: FontWeight.w700,
//                     fontFamily: "AppleSDGothicNeo",
//                     fontStyle: FontStyle.normal,
//                     fontSize: theme.fontSize12)),
//             Container(width: 8),
//             Text("10,000,000원",
//                 style: TextStyle(
//                     color: Color(0xff4042ab),
//                     fontWeight: FontWeight.w700,
//                     fontFamily: "AppleSDGothicNeo",
//                     fontStyle: FontStyle.normal,
//                     fontSize: theme.fontSize12))
//           ],
//         )
//       ],
//     );
//   }
//
//   renderProgress() {
//     return Stack(
//       children: [
//         Container(
//             height: 7,
//             decoration: BoxDecoration(
//                 borderRadius: BorderRadius.all(Radius.circular(3.5)),
//                 color: const Color(0xffe4e4ed))),
//         Container(
//             width: 240,
//             height: 7,
//             decoration: BoxDecoration(
//                 borderRadius: BorderRadius.all(Radius.circular(3.5)),
//                 color: const Color(0xff4042ab))),
//       ],
//     );
//   }
//
//   renderDescription() {
//     final theme = ThemeFactory.of(context).theme;
//
//     return Column(
//       mainAxisAlignment: MainAxisAlignment.center,
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         Text("모금 소개",
//             style: TextStyle(
//                 color: Color(0xff2e2e2e),
//                 fontWeight: FontWeight.w700,
//                 fontFamily: "AppleSDGothicNeo",
//                 fontStyle: FontStyle.normal,
//                 fontSize: theme.fontSize12)),
//         Container(height: 10),
//         Stack(
//           children: [
//             Container(
//                 height: 100,
//                 decoration: BoxDecoration(
//                     borderRadius: BorderRadius.all(Radius.circular(7)),
//                     border: Border.all(
//                         color: const Color(0xffccd1d5), width: 1.4))),
//             Container(
//               padding: EdgeInsets.only(left: 15, right: 15, top: 15),
//               child: Text("어린이의 정학 및 결식 아동을 위해 사용되는 기부금 입니다. 많은 도움 부탁드려요",
//                   style: TextStyle(
//                     color: Color(0xff262626),
//                     fontWeight: FontWeight.w400,
//                     fontFamily: "AppleSDGothicNeo",
//                     fontStyle: FontStyle.normal,
//                     fontSize: theme.fontSize15,
//                   )),
//             )
//           ],
//         ),
//         Container(height: 34),
//       ],
//     );
//   }
//
//   renderImageText() {
//     final theme = ThemeFactory.of(context).theme;
//     return Text("관련 이미지",
//         style: TextStyle(
//             color: Color(0xff2e2e2e),
//             fontWeight: FontWeight.w700,
//             fontFamily: "AppleSDGothicNeo",
//             fontStyle: FontStyle.normal,
//             fontSize: theme.fontSize12));
//   }
//
//   renderImageContents() {
//     return IntrinsicHeight(
//       child: Row(
//         children: [renderImages()],
//       ),
//     );
//   }
//
//   renderImages() {
//     return Expanded(
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           Container(
//             height: 106,
//             child: ListView(
//               controller: topListViewController,
//               scrollDirection: Axis.horizontal,
//               children: List.filled(
//                 5,
//                 Padding(
//                   padding: EdgeInsets.only(right: 8.0),
//                   child: SvgPicture.asset(
//                     'assets/svgs/ic/ic_tree_level_01.svg',
//                     height: 106,
//                     width: 200,
//                   ),
//                 ),
//               ),
//             ),
//           ),
//           Container(
//             height: 15.0,
//           ),
//           Expanded(
//             child: Row(
//               children: [
//                 Expanded(
//                   child: renderIndicator(),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
//
//   renderIndicator() {
//     return Stack(
//       key: topListViewKey,
//       children: [
//         Container(
//           height: 4.0,
//           color: Colors.grey[300],
//         ),
//         AnimatedPositioned(
//           left: scrollXPos,
//           child: Container(
//             height: 4.0,
//             width: scrollWidth,
//             color: const Color(0xfff29061),
//           ),
//           duration: Duration(
//             milliseconds: 0,
//           ),
//         ),
//       ],
//     );
//   }
//
//   //바텀 버튼
//   renderButton() {
//     return PrimaryButton(
//       label: '다음',
//       onTap: () {
//         Get.toNamed(
//           '/spinor/phil-test/donate-select/donate-detail',
//         );
//       },
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     final theme = ThemeFactory.of(context).theme;
//
//     return TitleLayout(
//       title: '까까로 기부',
//       body: Column(
//         children: [
//           Container(
//             padding: EdgeInsets.symmetric(horizontal: 16),
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 SubTitle(title: '기부 내용을\n확인해주세요.'),
//                 Container(height: 20),
//                 renderDonateDetail(),
//               ],
//             ),
//           ),
//           Container(height: 18),
//           Container(height: 6.0, color: theme.primaryGreyColor),
//           Container(height: 18),
//           Expanded(
//             child: ListView(
//               padding: EdgeInsets.symmetric(horizontal: 16),
//               physics: ClampingScrollPhysics(),
//               children: [
//                 renderDonatePrice(),
//                 Container(height: 16),
//                 renderProgress(),
//                 Container(height: 34),
//                 renderDescription(),
//                 renderImageText(),
//                 renderImageContents(),
//               ],
//             ),
//           ),
//           Container(
//             padding: EdgeInsets.symmetric(horizontal: 16),
//             child: renderButton(),
//           )
//         ],
//       ),
//     );
//   }
// }
