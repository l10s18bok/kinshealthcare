import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';

// TODO phil 테스트용 삭제예정!
/**
 * @author  김민석
 * @version 1.0
 * @since   2021-01-22
 */

class PhilTest extends StatefulWidget {
  @override
  _PhilTestState createState() => _PhilTestState();
}

class _PhilTestState extends State<PhilTest> {
  @override
  void initState() {
    super.initState();

    // final donateSelect = GetPage(
    //   // [ID] HU_3001
    //   // [NAME] 기부 - 대상선정
    //   name: '/spinor/phil-test/donate-select',
    //   page: () => DonateSCreen(),
    // );
    // Get.addPage(donateSelect);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 16.0,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextButton(
                style: TextButton.styleFrom(backgroundColor: Colors.amberAccent),
                onPressed: () {
                  Get.toNamed(
                    '/spinor/phil-test/donate-select',
                  );
                },
                child: Text(
                  '/spinor/phil-test/donate-select',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
              TextButton(
                style: TextButton.styleFrom(backgroundColor: Colors.amberAccent),
                onPressed: () {
                  Get.toNamed(
                    '/spinor/phil-test/bottom-sheet',
                  );
                },
                child: Text(
                  '/spinor/phil-test/bottom-sheet',
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
