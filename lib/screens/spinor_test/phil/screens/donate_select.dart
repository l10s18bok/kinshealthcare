// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:kins_healthcare/layouts/title_layout.dart';
// import 'package:kins_healthcare/screens/spinor_test/phil/components/card/donate_request_card.dart';
// import 'package:kins_healthcare/themes/theme_factory.dart';
// import 'package:kins_healthcare/components/button/button.dart';
// import 'package:get/get.dart';
//
// /**
//  * @id      HU_3001
//  * @name    /kkakka/issuance/donate
//  * @author  필
//  * @version 1.0
//  * @since   2021-02-08
//  */
//
// class DonateSCreen extends StatefulWidget {
//   @override
//   _DonateSCreenState createState() => _DonateSCreenState();
// }
//
// class _DonateSCreenState extends State<DonateSCreen> {
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   int curRequestId = 0;
//
//   //까까 받을 대상자를 선택해 주세요
//   renderSubTitle() {
//     final theme = ThemeFactory.of(context).theme;
//
//     return Text("까까 받을\n대상자를 선택해주세요",
//         style: TextStyle(
//             color: Color(0xff303030),
//             fontWeight: FontWeight.w700,
//             fontFamily: "AppleSDGothicNeo",
//             fontStyle: FontStyle.normal,
//             fontSize: theme.fontSize17));
//   }
//
// //  기부기관
//   renderRequestButtons() {
//     final rootPath = 'assets/svgs/ic/';
//
//     return Column(
//       children: [
//         Row(
//           children: [
//             Expanded(
//               child: DonateRequestCard(
//                 title: '사랑의 열매',
//                 subTitle: '결식아동을 도웁시다',
//                 iconPath: rootPath + 'ic_tree_level_01.svg',
//                 isPrimaryColor: curRequestId == 1,
//                 onTap: () {
//                   setState(() {
//                     curRequestId = 1;
//                   });
//                 },
//               ),
//             ),
//           ],
//         ),
//         Container(height: 10.0),
//         Row(
//           children: [
//             Expanded(
//               child: DonateRequestCard(
//                 title: '유니세프',
//                 subTitle: '아프리카에 결식아동을 도웁시다',
//                 iconPath: rootPath + 'ic_tree_level_01.svg',
//                 isPrimaryColor: curRequestId == 2,
//                 onTap: () {
//                   setState(() {
//                     curRequestId = 2;
//                   });
//                 },
//               ),
//             ),
//           ],
//         ),
//         Container(height: 10.0),
//         Row(
//           children: [
//             Expanded(
//               child: DonateRequestCard(
//                 title: '구세군',
//                 subTitle: '결식아동을 도웁시다',
//                 iconPath: rootPath + 'ic_tree_level_01.svg',
//                 isPrimaryColor: curRequestId == 3,
//                 onTap: () {
//                   setState(() {
//                     curRequestId = 3;
//                   });
//                 },
//               ),
//             ),
//           ],
//         ),
//         Container(height: 10.0),
//         Row(
//           children: [
//             Expanded(
//               child: DonateRequestCard(
//                 title: '사랑의 열매',
//                 subTitle: '결식아동을 도웁시다',
//                 iconPath: rootPath + 'ic_tree_level_01.svg',
//                 isPrimaryColor: curRequestId == 4,
//                 onTap: () {
//                   setState(() {
//                     curRequestId = 4;
//                   });
//                 },
//               ),
//             ),
//           ],
//         ),
//       ],
//     );
//   }
//
//   //바텀 버튼
//   renderButton() {
//     return PrimaryButton(
//       label: '다음',
//       onTap: () {
//         Get.toNamed(
//           '/spinor/phil-test/donate-select/donate-detail',
//         );
//       },
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return TitleLayout(
//       title: '까까로 기부',
//       body: Padding(
//         padding: EdgeInsets.symmetric(
//           horizontal: 16.0,
//         ),
//         child: Column(
//           mainAxisSize: MainAxisSize.max,
//           children: [
//             Expanded(
//               child: ListView(
//                 physics: ClampingScrollPhysics(),
//                 children: [
//                   renderSubTitle(),
//                   Container(height: 30.0),
//                   renderRequestButtons(),
//                   Container(height: 10.0),
//                 ],
//               ),
//             ),
//             renderButton(),
//             Container(height: 8.0),
//           ],
//         ),
//       ),
//     );
//   }
// }
