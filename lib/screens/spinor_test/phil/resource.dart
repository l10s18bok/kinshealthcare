//기부 셈플 데이터
import 'package:kins_healthcare/screens/spinor_test/phil/model/donate_model.dart';

List<DonateModel> mDonateModel = [
  DonateModel(
    title: '사랑의 열매',
    subTitle: '결식 아동을 도웁시다',
    iconPath: 'assets/svgs/ic/ic_tree_level_01.svg',
    treeLevel: '2단계',
    percent: 70,
  ),
  DonateModel(
    title: '유니세프',
    subTitle: '아프리카에 결식 아동을 도웁시다.',
    iconPath: 'assets/svgs/ic/ic_tree_level_01.svg',
    treeLevel: '1단계',
    percent: 60,
  ),
  DonateModel(
    title: '구세',
    subTitle: '결식 아동을 도웁시다',
    iconPath: 'assets/svgs/ic/ic_tree_level_01.svg',
    treeLevel: '3단계',
    percent: 50,
  ),
  DonateModel(
    title: '스피너미디어',
    subTitle: '결식 아동을 도웁시다',
    iconPath: 'assets/svgs/ic/ic_tree_level_01.svg',
    treeLevel: '3단계',
    percent: 40,
  ),
];