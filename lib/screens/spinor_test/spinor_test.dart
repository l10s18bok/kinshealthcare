import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/screens/temp/calendar_screen.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2020-01-12
 * 작성자 : JC
 * 화면명 : Splash
 * 주요기능 : 스플래시 페이지
 */
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    /*
    * TODO 여기서 토큰체크 및 테마 세팅 후 로그인 또는 메인 페이지로 이동
    * */
    // Future.microtask(() async {
    //   await Get.find<AuthController>().loginUser(
    //     email: TEST_USER_EMAIL,
    //     password: TEST_USER_PASSWORD,
    //   );
    //
    //   // TODO 스플래시에서 홈으로 또는 login 으로 보내기
    // });
  }

  renderLogoImage() {
    return SvgPicture.asset(
      'assets/svgs/img/logo_splash.svg',
      width: 172.0,
      height: 32.0,
    );
  }

  renderSlogan() {
    final theme = ThemeFactory.of(context).theme;

    return RichText(
      text: TextSpan(
        text: '가족사랑, ',
        children: [
          TextSpan(
            text: '킨즈헬스케어',
            style: TextStyle(
              fontSize: theme.fontSize12,
              fontWeight: theme.heavyFontWeight,
            ),
          ),
        ],
        style: TextStyle(
          color: theme.whiteTextColor,
          fontSize: theme.fontSize12,
          fontWeight: theme.primaryFontWeight,
        ),
      ),
    );
  }

  renderTempButtons() {
    return Expanded(
      child: SingleChildScrollView(
        child: SafeArea(
          bottom: true,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              // TODO spinor 테스트용 삭제예정!
              TextButton(
                onPressed: () {
                  Get.toNamed(
                    '/main-home',
                  );
                },
                child: Text(
                  '임시버튼>홈',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              // TODO spinor 테스트용 삭제예정!
              TextButton(
                onPressed: () {
                  Get.toNamed(
                    '/auth',
                  );
                },
                child: Text(
                  '임시버튼>로그인',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              // TODO spinor 테스트용 삭제예정!
              TextButton(
                onPressed: () {
                  Get.toNamed(
                    '/auth/reset/email',
                  );
                },
                child: Text(
                  '임시버튼>아아디/비번 찾기',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              // TODO spinor 테스트용 삭제예정!
              TextButton(
                onPressed: () {
                  Get.toNamed(
                    '/friends/request',
                  );
                },
                child: Text(
                  '임시버튼>가족관계 요청',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              // TODO spinor 테스트용 삭제예정!
              TextButton(
                onPressed: () async {
                  final result = await Get.toNamed(
                    '/calendar',
                  );

                  print((result as CalendarScreenResult).range);
                },
                child: Text(
                  '임시버튼>캘린더',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              // TODO spinor 테스트용 삭제예정!
              TextButton(
                onPressed: () {
                  Get.toNamed(
                    '/splash-test',
                  );
                },
                child: Text(
                  '임시버튼>SpinorTest',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return Scaffold(
      backgroundColor: theme.primaryColor,
      body: Center(
        child: Padding(
          padding: EdgeInsets.only(
            top: 250,
          ),
          child: Column(
            children: [
              renderLogoImage(),
              Container(height: 11),
              renderSlogan(),

              //TODO Production 갈때 꼭 지울것!
              renderTempButtons(),
            ],
          ),
        ),
      ),
    );
  }
}
