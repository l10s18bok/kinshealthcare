import 'package:kins_healthcare/controllers/base_controller.dart';
import 'package:kins_healthcare/screens/spinor_test/baron/service/myService.dart';

import 'package:get/get.dart';

class Mycontroller extends BaseController {
  Mycontroller(Myservice rootService) : super(rootService);
  var inquiryNoanwserData;

  Future<dynamic> showAllController() async {
    return null;
  }

  //inquiryNoanwser
  // api/comm/inquiryNoanwser
  Future<dynamic> getCommInquiryNoanwserData() async {
    var response = await this.services.commService.inquiryNoanwser();
    inquiryNoanwserData = response.data.values.obs;
    var resultCode = response.resultCode;
    update();
    return resultCode == 200 ? inquiryNoanwserData : false;
  }
}
