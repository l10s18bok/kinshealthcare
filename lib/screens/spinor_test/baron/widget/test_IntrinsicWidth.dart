import 'package:flutter/material.dart';

/*
 작성일 : 2021-06-03
 작성자 : (작성자 이름 입력)
 화면명 : (화면 코드 입력)
 경로 : 
 클래스 : TestIntrinsicwidth
 설명 : (클래스 설명 입력)
*/

class TestIntrinsicwidth extends StatefulWidget {
  @override
  _TestIntrinsicwidthState createState() => _TestIntrinsicwidthState();
}

class _TestIntrinsicwidthState extends State<TestIntrinsicwidth> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 300,
        child: Column(
          children: [
            Expanded(
                flex: 3,
                child: Container(
                  height: 50,
                  decoration: BoxDecoration(color: Colors.amber),
                  child: Center(child: Text('data')),
                )),
            Expanded(
                flex: 1,
                child: Container(
                  height: 50,
                  decoration:
                      BoxDecoration(color: Colors.amber, border: Border.all()),
                  child: Center(child: Text('data')),
                )),
            Container(
              height: 150,
              child: Text('?'),
            )
          ],
        ));
  }
}
