// import 'dart:async';

// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:flutter_svg/svg.dart';
// import 'package:get/instance_manager.dart';
// import 'package:kins_healthcare/components/card/circle_network_image.dart';
// import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
// import 'package:kins_healthcare/components/listview/new_pagination_list_view.dart';
// import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
// import 'package:kins_healthcare/controllers/post_controller.dart';
// import 'package:kins_healthcare/models/local/post_model.dart';
// import 'package:kins_healthcare/services/post/model/new_post_list_model.dart';
// import 'package:kins_healthcare/themes/theme_factory.dart';

// /*
//  작성일 : 2021-05-10
//  작성자 : (baron)
//  화면명 : (화면 코드 입력)
//  경로 :
//  클래스 : Paginationviewtest
//  설명 : (pagination Test only!~  )
// */

// class Paginationviewtest extends StatefulWidget {
//   @override
//   _PaginationviewtestState createState() => _PaginationviewtestState();
// }

// class _PaginationviewtestState extends State<Paginationviewtest> {
//   PostController? postController;
//   int itemSizeForScrollController = 0;
//   int pageIndex = 0;
//   double maxPositonPixels = 0.0;
//   int indexOfListView = 0;
//   bool? islaoding;
//   bool? onButtomRefresh = false;
//   int? textMaxLine = 1;
//   DateTime? lastClickTime;
//   bool showOption = false;
//   int likeCount = 0;

//   @override
//   void initState() {
//     postController = Get.find<PostController>();

//     initResult();

//     super.initState();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//   }

//   /// 어디까지 봤는지 ? position을 기억해주는 function
//   setMaxPositonPixels(double pixels) {
//     if (pixels > maxPositonPixels) maxPositonPixels = pixels;
//     setState(() {});
//   }

//   Future<void> refresh() async {
//     await requestPostList(reset: true).then((value) {
//       isDataAsync();
//     });
//   }

//   /// text가 한줄에 들어오는지 확인 하는 function.
//   Widget isTextOverFlow(
//       {required BuildContext context,
//       required double width,
//       required String message,
//       required double fontSize,
//       required int index}) {
//     var showmoreButtonWidth = 50.0;
//     return LayoutBuilder(builder: (context, size) {
//       var span = TextSpan(
//         text: message,
//         style: TextStyle(fontSize: fontSize),
//       );

//       var tp = TextPainter(
//         maxLines: 1,
//         // maxLines: maxLineSimulatorList[index],
//         textAlign: TextAlign.left,
//         textDirection: TextDirection.ltr,
//         text: span,
//       );

//       tp.layout(maxWidth: width);
//       var exceeded = tp.didExceedMaxLines;
//       return exceeded
//           ? postController!.isShowMoreButton[index]!
//               ? Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Container(
//                       width: width,
//                       child: Text(
//                         message,
//                         overflow: TextOverflow.ellipsis,
//                         style: TextStyle(fontSize: fontSize),
//                         maxLines: postController!.maxLineList[index],
//                       ),
//                     ),
//                     Container(
//                       width: showmoreButtonWidth,
//                       child: InkWell(
//                           onTap: () {
//                             postController!
//                                 .updateTextShow(index, true)
//                                 .then((value) => setState(() {}));
//                           },
//                           child: Text(
//                             '더 보기',
//                             style: TextStyle(fontSize: 10, color: Colors.grey),
//                           )),
//                     )
//                   ],
//                 )
//               : Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceAround,
//                   children: [
//                     InkWell(
//                       onTap: () {
//                         postController!
//                             .updateTextShow(index, false)
//                             .then((value) => setState(() {}));
//                         ;
//                       },
//                       child: Container(
//                         width: width,
//                         child: Text(
//                           message,
//                           overflow: TextOverflow.ellipsis,
//                           maxLines: postController!.maxLineList[index],
//                         ),
//                       ),
//                     ),
//                   ],
//                 )
//           : Row(
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//               children: [
//                 InkWell(
//                   child: Container(
//                     alignment: Alignment.centerLeft,
//                     width: width,
//                     child: Text(
//                       message,
//                       maxLines: 1,
//                     ),
//                   ),
//                 )
//               ],
//             );
//     });
//   }

//   /// controller에 list data null인지 확인. null이면 다시 서버요청. 아니면 가져오기.
//   /// baron 2021-05-13
//   Future<void> initResult() async {

//   }

//   /// listView  top으로 jump 해주는 function.

//   /// 처음으로 호출시  pagination  0 부터 시작. 아니면  pagination +1 ;
//   /// baron
//   Future<void> requestPostList({required bool reset}) async {
//     await postController!.postListRequest(reset: reset).then((list) async {
//       lastClickTime = DateTime.now();
//       islaoding = false;
//       isDataAsync().then((value) => setState(() => {}));

//       ///add kkakkaDetail페이지 request.

//       print(' result!!! ${postController!.result.length}');
//     });
//   }

//   bool isKkakka(int index) {
//     var exits = postController!.result[index]!.kkakkaPrice != null &&
//         postController!.result[index]!.kkakkaBalance != null &&
//         postController!.result[index]!.kkakkaType != null;

//     return exits;
//   }

//   Future<bool> isDataAsync() async {
//     if (postController!.result.isNotEmpty &&
//         postController!.isShowMoreButton.length ==
//             postController!.isShowOptionButton.length) {
//       setState(() {});
//       return true;
//     }
//     return Future.delayed(
//         Duration(seconds: 2),
//         () =>
//             postController!.result.isNotEmpty &&
//             postController!.isShowMoreButton.length ==
//                 postController!.isShowOptionButton.length);
//   }

//   String getTimeLeft(DateTime? startDate, String type) {
//     if (type == 'K') {
//       if (startDate == null) {
//         return '사용기한 미설정';
//       } else {
//         var now = DateTime.now();
//         var temp = now.difference(startDate);
//         return '${temp.inDays}일 남음.';
//         // return '${temp.inDays},${temp.inHours},${temp.inMinutes}';
//       }
//     } else {
//       var now = DateTime.now();
//       var temp = now.difference(startDate!);
//       if (temp.inDays > 0) {
//         return '${temp.inDays}일 전';
//       } else if (temp.inHours > 0) {
//         return '${temp.inHours}시간 전';
//       } else if (temp.inMinutes < 60 && temp.inMinutes > 3) {
//         return '${temp.inMinutes}분 전';
//       } else if (temp.inMinutes < 3) {
//         return '방금 전';
//       } else {
//         return 'time error';
//       }
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     var length = postController!.theNewPostList.length;
//     return Container(
//         height: MediaQuery.of(context).size.height * .88,
//         child: NotificationListener<ScrollNotification>(
//           onNotification: (scrollNotification) {
//             if (scrollNotification is ScrollStartNotification) {
//             } else if (scrollNotification is ScrollUpdateNotification) {
//             } else if (scrollNotification is ScrollEndNotification) {
//               // postController!.resetShowMoreButton(index);
//               if (scrollNotification.metrics.maxScrollExtent ==
//                   scrollNotification.metrics.pixels) {
//                 if (!islaoding!) {
//                   islaoding = true;
//                   setState(() {
//                     nextPage();
//                   });
//                 }
//               }

//               setMaxPositonPixels(scrollNotification.metrics.pixels);
//               // if (maxPositonPixels - scrollNotification.metrics.pixels > 600) {
//               //   postController!.resetShowMoreButton();
//               // }
//               // print('Scroll End: ${scrollNotification.metrics.pixels}');
//             }
//             return false;
//           },
//           child: FutureBuilder<bool>(
//               future: isDataAsync(),
//               initialData: false,
//               builder: (context, snapshot) {
//                 if (snapshot.data!) {
//                   return PaginationListView<TheNewPostListModel>(
//                       controller: postController!,
//                       scrollController: ,
//                       itemCount: length == 0 ? 1 : length,
//                       itemBuilder: (context, index) {
//                         return Container(
//                           height: 50,
//                           child: Text('$index'),
//                         );
//                       },
//                       emptyWidget: NonKkakkaCard(
//                         title: '아직  \'소식\'이나  \'까까\'가 없어요.\n요청해보세요!',
//                       ),
//                       lastWidget: Container(
//                           child: Center(
//                         child: Text('last'),
//                       )),
//                       request: postController!.postListRequest,
//                     );
//                 } else {
//                   return Center(child: CircularProgressIndicator());
//                 }
//               }),
//         ));
//   }
// }
