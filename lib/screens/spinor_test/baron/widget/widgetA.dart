import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/instance_manager.dart';
import 'package:kins_healthcare/screens/spinor_test/baron/controller/mycontroller.dart';
import 'package:kins_healthcare/screens/spinor_test/baron/service/myService.dart';

class WidgetA extends StatefulWidget {
  @override
  _WidgetAState createState() => _WidgetAState();
}

class _WidgetAState extends State<WidgetA> {
  List<String> orderList = [
    '발행순',
    '발행역순',
    '시작일자순',
    '시작일자역순',
    '종료일자순',
    '종료일자역순'
  ];
  //구분 (까까 필터링 조건)
  String divisionValue = '전체';
  List<String> divisionList = ['전체', '요청한', '요청받은', '발행한', '발행받은'];
  //용도 (까까 유형)
  String purposeValue = '전체';
  List<String> purposeList = ['전체', '기념일', '기부', '지역', '의료', '용돈', '셀프', '복지'];
  //상태 (까까 상태)
  // 2021/3/2 기준 활성, 해제만 사용
  String statusValue = '전체';
  List<String> statusList = ['전체', '활성', '해제'];
  int index = 0;
  @override
  Widget build(BuildContext context) {
    var controller = Get.put(Mycontroller(Myservice()));

    return Container(
      child: GetBuilder<Mycontroller>(builder: (_) {
        return Center(
          child: Container(
            child: DropdownButton<String>(
              value: divisionList[index],
              items: divisionList.map((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value, style: TextStyle(color: Colors.amber)),
                );
              }).toList(),
              onChanged: (data) {
                setState(() {
                  index = divisionList.indexOf(data!);
                });
              },
            ),
          ),
        );
      }),
    );
  }
}
