import 'package:kins_healthcare/services/root_service.dart';

class Myservice extends RootService {
  static final Myservice _singleton = Myservice._internal();

  factory Myservice() {
    return _singleton;
  }

  Myservice._internal();
}
