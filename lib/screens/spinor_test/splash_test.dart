import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/screens/spinor_test/baron/home.dart';
import 'package:kins_healthcare/screens/spinor_test/jc/demo_scren.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

import 'baron/home.dart';

// TODO phil 테스트용 삭제예정!
class SplashTest extends StatefulWidget {
  @override
  _SplashTestState createState() => _SplashTestState();
}

class _SplashTestState extends State<SplashTest> {
  @override
  void initState() {
    super.initState();

    // Future.microtask(() {
    //   c.loginUser(email: 'jc@spinormedia.com', password: 'codefactory');
    // });
  }

  renderLogoImage() {
    return SvgPicture.asset(
      'assets/svgs/img/logo_splash.svg',
      width: 172.0,
      height: 32.0,
    );
  }

  renderSlogan() {
    final theme = ThemeFactory.of(context).theme;

    return RichText(
      text: TextSpan(
        text: '가족사랑, ',
        children: [
          TextSpan(
            text: '킨즈헬스케어',
            style: TextStyle(
              fontSize: theme.fontSize12,
              fontWeight: theme.heavyFontWeight,
            ),
          ),
        ],
        style: TextStyle(
          color: theme.whiteTextColor,
          fontSize: theme.fontSize12,
          fontWeight: theme.primaryFontWeight,
        ),
      ),
    );
  }

  renderTempButtons() {
    return Expanded(
      child: SingleChildScrollView(
        child: SafeArea(
          bottom: true,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TextButton(
                onPressed: () {
                  Get.toNamed(
                    '/spinor/andy-test',
                  );
                },
                child: Text(
                  'andy',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              TextButton(
                onPressed: () {
                  Get.toNamed(
                    '/spinor/mark-test',
                  );
                },
                child: Text(
                  'mark',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              TextButton(
                onPressed: () {
                  Get.toNamed(
                    '/spinor/phil-test',
                  );
                },
                child: Text(
                  'phil',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              TextButton(
                onPressed: () {
                  Get.toNamed(
                    '/spinor/victor-test',
                  );
                },
                child: Text(
                  'victor',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              TextButton(
                onPressed: () {
                  Get.to(DemoScreen());
                },
                child: Text(
                  'JC',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              TextButton(
                onPressed: () {
                  Get.to(Myhome());
                },
                child: Text(
                  'baron',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return Scaffold(
      backgroundColor: theme.primaryColor,
      body: Center(
        child: Padding(
          padding: EdgeInsets.only(
            top: 250,
          ),
          child: Column(
            children: [
              renderLogoImage(),
              Container(height: 11),
              renderSlogan(),

              //TODO Production 갈때 꼭 지울것!
              renderTempButtons(),
            ],
          ),
        ),
      ),
    );
  }
}
