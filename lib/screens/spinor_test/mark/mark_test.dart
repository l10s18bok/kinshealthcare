import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/components/bottom_sheet/btn_bottom_sheet.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import '../../../components/bottom_sheet/sub_btn_sheet.dart';
import 'card_list.dart';
import 'model/my_kins_row_model.dart';

/*
 작성일 : 2021-01-21
 작성자 : Mark,
 화면명 : (화면명),
 경로 : mark_test
 클래스 : MarkTest,
 설명 : Mark 테스트 화면
*/

// TODO mark 테스트용 삭제예정!
class MarkTest extends StatefulWidget {
  @override
  _MarkTestState createState() => _MarkTestState();
}

class _MarkTestState extends State<MarkTest> {
  final topModelList = [
    MyKinsRowModel(
        svgPath: 'assets/svgs/ic/ic_profile_setting.svg', title: '내 정보 관리'),
    MyKinsRowModel(
        svgPath: 'assets/svgs/ic/ic_family_setting.svg', title: '나의 가족'),
    MyKinsRowModel(
        svgPath: 'assets/svgs/ic/ic_anniversary_setting.svg', title: '기념일 관리'),
    MyKinsRowModel(
        svgPath: 'assets/svgs/ic/ic_tree_setting.svg', title: '킨즈 나이테'),
  ];

  final bottomModelList = [
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_message_setting.svg',
      title: '인증수단 관리',
      pageName: '/my-kins/certification-method',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_alarm_setting.svg',
      title: '알림 설정',
      pageName: '/my-kins/notification-setting',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_notice_setting.svg',
      title: '공지사항/이용안내',
      pageName: '/my-kins/notice-mark',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_faq_setting.svg',
      title: '1:1문의/FAQ',
      pageName: '/my-kins/faq-mark',
    ),
    MyKinsRowModel(
        svgPath: 'assets/svgs/ic/ic_advertise_setting.svg', title: '광고문의'),
  ];

  final testModelList = [
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_profile_setting.svg',
      title: '관계 정정',
      pageName: '/spinor/mark-test/family_relations',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_family_setting.svg',
      title: '결제비율 관리',
      pageName: '/payment/share',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_anniversary_setting.svg',
      title: '까까 발행',
      pageName: '/kkakka/issuance',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_tree_setting.svg',
      title: 'Top Bar Test',
      pageName: '/spinor/mark-test/top-bar-test',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_tree_setting.svg',
      title: 'Top Bar Nested List Test',
      pageName: '/spinor/mark-test/top-nested-bar-list',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_tree_setting.svg',
      title: 'Top Bar Sliver List Test',
      pageName: '/spinor/mark-test/top-bar-sliver-list--test',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_tree_setting.svg',
      title: 'Top Bar Change Test',
      pageName: '/spinor/mark-test/top-bar-change-test',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_message_setting.svg',
      title: '마이킨즈 수정',
      pageName: '/spinor/mark-test/my-kins-mark',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_alarm_setting.svg',
      title: '응급병원등록 팝업',
      pageName: 'bottom-sheet-test',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_alarm_setting.svg',
      title: '발행완료 팝업',
      pageName: 'sub-bottom-sheet-test',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_notice_setting.svg',
      title: '약속받은 금액',
      pageName: '/main-home/request-received',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_faq_setting.svg',
      title: '약속해준 금액',
      pageName: '/main-home/request-give',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_anniversary_setting.svg',
      title: '카드 선택 Example',
      pageName: '/spinor/mark-test/card-list',
    ),
    MyKinsRowModel(
      svgPath: 'assets/svgs/ic/ic_profile_setting.svg',
      title: '소리 테스트',
      pageName: '/spinor/mark-test/audio-test',
    ),
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Padding(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 62),
            renderTitle(),
            SizedBox(height: 22),
            Expanded(child: renderContent()),
          ],
        ),
      ),
    );
  }

  renderTitle() {
    return Text(
      '마이 킨즈',
      style: TextStyle(
        color: Color(0xff000000),
        fontSize: ThemeFactory.of(context).theme.fontSize22,
        fontWeight: FontWeight.w700,
      ),
    );
  }

  renderContent() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ...topModelList.map((model) => renderSettingRow(model)).toList(),
          SizedBox(height: 18),
          Container(height: 1, width: double.infinity, color: kinsGrayE4),
          SizedBox(height: 18),
          ...bottomModelList.map((model) => renderSettingRow(model)).toList(),
          SizedBox(height: 18),
          Container(height: 1, width: double.infinity, color: kinsGrayE4),
          SizedBox(height: 18),
          ...testModelList.map((model) => renderSettingRow(model)).toList(),
          InkWell(
            child: SizedBox(
              height: 120,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  renderLogoutTitle(),
                  renderVersionTitle(),
                ],
              ),
            ),
            onTap: () => Get.back(),
          ),
        ],
      ),
    );
  }

  renderSettingRow(MyKinsRowModel model) {
    return InkWell(
      child: Container(
        width: double.infinity,
        height: 44,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              height: 28,
              width: 28,
              child: renderRowImage(model.svgPath),
            ),
            SizedBox(width: 20),
            Text(
              model.title,
              style: TextStyle(
                fontSize: ThemeFactory.of(context).theme.fontSize15,
                color: kinsBlack63,
                fontWeight: FontWeight.w400,
              ),
            ),
          ],
        ),
      ),
      onTap: () => _onItemTap(model),
    );
  }

  renderRowImage(String path) {
    return SvgPicture.asset(
      path,
      width: 28,
      height: 28,
    );
  }

  renderLogoutTitle() {
    return Text(
      '로그아웃',
      style: TextStyle(
        fontSize: ThemeFactory.of(context).theme.fontSize14,
        color: kinsGray8A,
        fontWeight: FontWeight.w400,
      ),
    );
  }

  renderVersionTitle() {
    return Text(
      'ver 1.0.0',
      style: TextStyle(
        fontSize: ThemeFactory.of(context).theme.fontSize12,
        color: kinsGray6E,
        fontWeight: FontWeight.w700,
      ),
    );
  }

  Future<String?> showBottomSheet() async {
    final sheet = BtnBottomSheet(
      boldTitle: '[세브란스병원]',
      title: '을\n응급병원으로 등록하시겠습니까?',
    );
    final selectTitle = await Get.bottomSheet<String>(sheet);

    if (selectTitle != 'OK') return selectTitle;

    final sheet02 = BtnBottomSheet(
      boldTitle: '[세브란스병원]',
      title: '이\n내 응급병원으로 등록되었습니다.',
      isOneBtn: true,
    );
    final finalData = await Get.bottomSheet<String>(sheet02);

    return finalData;
  }

  Future<String?> showBottomSubBtnSheet() async {
    final sheet = SubBtnBottomSheet(
      boldTitle: '박보건',
      blueTitle: '[돈잘버는오빠]',
      boldTitle02: '에게',
      title: '까까 발행이 완료되었습니다.\n송금하기 기능이 있습니다.\n가족들에게 송금해보시겠어요?',
      subBtnTitle: '송금하기',
    );
    final selectTitle = await Get.bottomSheet<String>(sheet);

    return selectTitle;
  }

  _onItemTap(MyKinsRowModel model) {
    final String? pageName = model.pageName;
    if (pageName == null) return;
    if (pageName == 'bottom-sheet-test') return showBottomSheet();
    if (pageName == 'sub-bottom-sheet-test') return showBottomSubBtnSheet();
    if (pageName == '/spinor/mark-test/card-list' && Platform.isIOS) {
      Navigator.of(context).push(
        CupertinoPageRoute(
            builder: (context) => CardList(), fullscreenDialog: true),
      );
      return;
    }

    Get.toNamed(pageName);
  }

  Route createRoute() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => CardList(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }
}
