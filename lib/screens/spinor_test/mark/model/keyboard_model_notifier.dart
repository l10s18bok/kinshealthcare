import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

/*
 작성일 : 2021-01-25
 작성자 : Mark,
 화면명 : ,
 경로 : ,
 클래스 : KeyboardModelNotifier,
 설명 : Custom Keyboard 사용시에 Scroll 이동할 Widget 의 Key 값과 Keyboard Show 여부를 공유하기 위한 Notifier
*/

class KeyboardModelNotifier extends ChangeNotifier {
  KeyboardModelNotifier(
    this._value,
  );

  bool _value;
  bool get value => _value;
  set value(bool newValue) {
    if (_value == newValue) return;
    _value = newValue;
    notifyListeners();
  }

  setIsVisible(bool newValue, GlobalKey key) {
    this.key = key;

    if (_value == newValue) return;
    _value = newValue;
    notifyListeners();
  }

  GlobalKey? key;
}
