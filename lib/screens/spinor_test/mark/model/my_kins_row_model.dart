/*
 작성일 : 2021-01-25
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : MyKinsRowModel,
 설명 : MarkTest 위젯을 위한 Model
*/

class MyKinsRowModel {
  String svgPath;
  String title;
  String? pageName;

  MyKinsRowModel({
    required this.svgPath,
    required this.title,
    this.pageName,
  });
}
