/*
 작성일 : 2021-01-25
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : LocalBoardModel,
 설명 : Board 위젯을 위한 Model
*/

class LocalBoardModel {
  String title;
  String? content;
  String? answer;
  String regDate;
  int? ids;

  LocalBoardModel({
    required this.title,
    required this.regDate,
    this.content,
    this.answer,
    this.ids,
  });
}
