import 'package:kins_healthcare/services/mykins/model/division_list_body.dart';

/*
 작성일 : 2021-01-25
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : PaymentUserModel,
 설명 : PaymentRate 위젯을 위한 Model
*/

class PaymentUserModel {
  PaymentUserModel({
    required this.name,
    required this.phoneNum,
    required this.userRelNo,
    this.nickName,
    this.imageUrl,
    this.percentage = '0.0',
    this.isChecked = false,
    this.isOnContacts = false,
    this.isError = false,
  });

  final String name;
  final String phoneNum;
  final int userRelNo;
  String? nickName;
  String? imageUrl;
  String? percentage;
  bool isChecked;
  bool isOnContacts;
  bool isError;

  toJson() {
    return {
      "name": name,
      "phoneNum": phoneNum,
      "userRelNo": userRelNo,
      "nickName": nickName,
      "imageUrl": imageUrl,
      "percentage": percentage,
      "isChecked": isChecked,
      "isOnContacts": isOnContacts,
    };
  }

  DivisionBody get divisionBody {
    return DivisionBody(
      relUserNo: this.userRelNo,
      percentage: this.percentage,
    );
  }
}
