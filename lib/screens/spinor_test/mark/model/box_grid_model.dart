import 'package:flutter/material.dart';
import '../../../../utils/resource.dart';

/*
 작성일 : 2021-01-26
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : BoxGridModel,
 설명 : BoxGrid 위한 Model
*/

class BoxGridModel {
  BoxGridModel({
    required this.title,
    this.bankCode,
    this.imagePath,
    this.selectedImagePath,
    this.borderColor = kinsBlueD7,
    this.selectBorderColor = kinsBlueA2,
    this.backgroundColor = kinsWhite,
    this.selectColor = kinsBlueDD,
    this.ratio = 1,
    this.isSelect = false,
  });

  final String title;
  String? bankCode;
  String? imagePath;
  String? selectedImagePath;
  Color borderColor;
  Color selectBorderColor;
  Color backgroundColor;
  Color selectColor;
  double ratio;
  bool isSelect;
}
