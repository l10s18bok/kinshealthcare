/*
 작성일 : 2021-01-25
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : GroupItemModel,
 설명 : FamilyDiagram 위젯을 위한 Model
*/

class GroupItemModel {
  String firstTitle;
  String secondTitle;
  String? thirdTitle;
  String? fourthTitle;
  String? distance;
  String firstPath;
  String secondPath;
  List<String>? firstAddTitle;
  List<String>? secondAddTitle;

  GroupItemModel({
    required this.firstTitle,
    required this.secondTitle,
    this.thirdTitle,
    this.fourthTitle,
    this.distance,
    this.firstPath = 'assets/svgs/ic/ic_img_logo_01.svg',
    this.secondPath = 'assets/svgs/ic/ic_img_logo_02.svg',
    this.firstAddTitle,
    this.secondAddTitle,
  });
}
