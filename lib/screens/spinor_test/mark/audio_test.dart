// import 'package:audioplayers/audioplayers.dart';
// import 'package:flutter/material.dart';

// class AudioTest extends StatefulWidget {
//   @override
//   _AudioTestState createState() => _AudioTestState();
// }

// class _AudioTestState extends State<AudioTest> {
//   late AudioCache _audioCache;

//   @override
//   void initState() {
//     super.initState();
//     _audioCache = AudioCache(
//       prefix: "assets/audio/",
//       fixedPlayer: AudioPlayer()..setReleaseMode(ReleaseMode.STOP),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(title: Text("Music play")),
//         body: ElevatedButton(
//           onPressed: () => _audioCache.play('alert.mp3'),
//           child: Text("Play Audio"),
//         ),
//       ),
//     );
//   }
// }
