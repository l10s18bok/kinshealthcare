import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';

import '../../../components/grid/box_grid.dart';
import 'model/box_grid_model.dart';

/*
 작성일 : 2021-02-08
 작성자 : Mark,
 화면명 : (화면명),
 경로 : /spinor/mark-test/top-bar-test
 클래스 : TopBarTest,
 설명 : TopBar Example 파일
*/

class TopBarTest extends StatefulWidget {
  @override
  TopBarTestState createState() => TopBarTestState();
}

class TopBarTestState extends State<TopBarTest> {
  List<BoxGridModel> models = [];

  @override
  void initState() {
    super.initState();
    _setModel();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollLayout(
      topBar: renderTopBar(),
      body: renderMain(),
    );
  }

  Widget renderTopBar() {
    return BackTopBar(
      title: 'TopBarTest',
      rightButtons: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [renderPlusBtn()],
      ),
    );
  }

  Widget renderPlusBtn() {
    return InkWell(
      child: Container(
        width: 50,
        color: Colors.transparent,
        child: Icon(Icons.add),
      ),
      onTap: () => setState(() => _setModel()),
    );
  }

  Widget renderMain() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          SizedBox(height: 20),
          Expanded(child: BoxGrid(models: models)),
          renderSubmitBtn(),
        ],
      ),
    );
  }

  Widget renderSubmitBtn() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      height: 60,
      child: PrimaryButton(
        label: '다음',
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
      ),
    );
  }

  _setModel() {
    for (int i = 0; i < 5; ++i) {
      models.add(
        BoxGridModel(
          title: '교육비',
          imagePath: 'assets/svgs/ic/ic_education_publish_unselect.svg',
          selectedImagePath: 'assets/svgs/ic/ic_education_publish_select.svg',
          ratio: 105 / 90,
        ),
      );
    }
  }
}
