import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

import '../../../components/grid/box_grid.dart';
import 'layout/bar_change_custom_layout.dart';
import 'model/box_grid_model.dart';

/*
 작성일 : 2021-02-08
 작성자 : Mark,
 화면명 : (화면명),
 경로 : /spinor/mark-test/top-bar-change-test
 클래스 : TopBarChangeTest,
 설명 : TopBar Example 파일
*/

class TopBarChangeTest extends StatefulWidget {
  @override
  TopBarChangeTestState createState() => TopBarChangeTestState();
}

class TopBarChangeTestState extends State<TopBarChangeTest> {
  final double collapsedHeight = 50.0, expandedHeight = 100.0;
  bool pinned = true, floating = false;
  List<BoxGridModel> models = [];

  @override
  void initState() {
    super.initState();
    _setModel();
  }

  @override
  Widget build(BuildContext context) {
    return BarChangeCustomLayout(
      collapsedHeight: collapsedHeight,
      expandedHeight: expandedHeight,
      collapsedTopBar: renderCollapsedTop(),
      expandedTopBar: renderExpandedTop(),
      body: renderMain(),
      floating: floating,
      pinned: pinned,
    );
  }

  Widget renderCollapsedTop() {
    return BackTopBar(
      title: 'TopBarTest',
      height: expandedHeight,
      rightButtons: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          renderPlusBtn(),
          renderFloatingBtn(),
          renderPinnedBtn(),
        ],
      ),
    );
  }

  Widget renderPlusBtn() {
    return InkWell(
      child: Container(
        width: 50,
        color: Colors.transparent,
        child: Icon(Icons.add),
      ),
      onTap: () {
        _setModel();
        setState(() {});
      },
    );
  }

  Widget renderFloatingBtn() {
    return InkWell(
      child: Container(
        width: 50,
        child: Icon(
          Icons.ad_units,
          color: floating ? kinsGreen6C : kinsOrangeF2,
        ),
      ),
      onTap: () => setState(() => floating = !floating),
    );
  }

  Widget renderPinnedBtn() {
    return InkWell(
      child: Container(
        width: 50,
        child: Icon(
          Icons.present_to_all_outlined,
          color: pinned ? kinsGreen6C : kinsOrangeF2,
        ),
      ),
      onTap: () => setState(() => pinned = !pinned),
    );
  }

  Widget renderExpandedTop() {
    return Container(
      alignment: Alignment.bottomLeft,
      padding: EdgeInsets.symmetric(horizontal: 16),
      color: kinsBlue40,
      child: Text(
        'TopBarTest',
        style: TextStyle(
          color: kinsWhite,
          fontWeight: FontWeight.w700,
          fontSize: ThemeFactory.of(context).theme.fontSize22,
        ),
      ),
    );
  }

  renderMain() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          SizedBox(height: 20),
          Expanded(child: BoxGrid(models: models)),
          renderSubmitBtn(),
        ],
      ),
    );
  }

  _setModel() {
    for (int i = 0; i < 6; ++i) {
      models.add(
        BoxGridModel(
          title: '교육비',
          imagePath: 'assets/svgs/ic/ic_education_publish_unselect.svg',
          selectedImagePath: 'assets/svgs/ic/ic_education_publish_select.svg',
          ratio: 105 / 90,
        ),
      );
    }
  }

  Widget renderSubmitBtn() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      height: 60,
      child: PrimaryButton(
        label: '다음',
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
      ),
    );
  }
}
