import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/sample_recource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

import '../../../components/bottom_sheet/relation_choice_sheet.dart';
import '../../../components/top_bar/back_top_bar.dart';
import 'components/family_diagram.dart';

/*
 작성일 : 2021-01-21
 작성자 : Mark,
 화면명 : (화면명),
 경로 : /spinor/mark-test/family_relations
 클래스 : FamilyRelationsScreen,
 설명 : 가족관계등록을 위한 Screen
*/

class FamilyRelationsScreen extends StatefulWidget {
  @override
  _FamilyRelationState createState() => _FamilyRelationState();
}

class _FamilyRelationState extends State<FamilyRelationsScreen> {
  String selectTitle = '엄마';
  final zoomContainerHeight = 280.0;
  bool isMan = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final queryData = MediaQuery.of(context);
    final ratio = (queryData.size.width - 32) / zoomContainerHeight;

    return DefaultLayout(
      body: renderIntrinsicHeight(
        child: Column(
          children: [
            BackTopBar(title: '관계 정정'),
            Expanded(child: renderMain(ratio)),
          ],
        ),
      ),
    );
  }

  Widget renderIntrinsicHeight({required Widget child}) {
    final queryData = MediaQuery.of(context);
    final minHeight = queryData.size.height - queryData.padding.top;

    return SingleChildScrollView(
      child: IntrinsicHeight(
        child: Container(
          constraints: BoxConstraints(minHeight: minHeight),
          child: child,
        ),
      ),
    );
  }

  Widget renderMain(double ratio) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(height: 20),
          renderSubTitle(),
          SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              renderUserInfo('요청 보낼 관계', selectTitle, color: kinsBlue40),
              SizedBox(width: 26),
              renderUserInfo('나', '나', size: 71, onTap: _nameOnTap),
            ],
          ),
          SizedBox(height: 18),
          FamilyDiagram(
            isMan: isMan,
            ratio: ratio,
            mEndGroupModel: mEndGroupModel,
            mSideGroupModel: mSideGroupModel,
            fEndGroupModel: fEndGroupModel,
            fSideGroupModel: fSideGroupModel,
            topGroupModel: isMan ? topManGroupModel : topGroupModel,
            bottomGroupModel: isMan ? mBottomGroupModel : fBottomGroupModel,
            zoomContainerHeight: zoomContainerHeight,
            onItemTap: _relationItemTap,
          ),
          Expanded(
            child: Container(
              alignment: Alignment.bottomCenter,
              child: renderSubmitBtn(),
            ),
          ),
        ],
      ),
    );
  }

  renderSubTitle() {
    return Text(
      '''인물을 선택 후, 아래의 '가족관계도'에서\n관계를 선택해보세요!(직접입력도 가능)''',
      style: TextStyle(
        color: kinsGray6D,
        fontWeight: FontWeight.w700,
        fontSize: ThemeFactory.of(context).theme.fontSize14,
      ),
    );
  }

  _showSelectTitle(String? title) {
    if (title == null) return;
    setState(() => selectTitle = title);
  }

  Widget renderUserInfo(
    String title,
    String selectItem, {
    Color? color,
    double? size,
    VoidCallback? onTap,
  }) {
    return Container(
      width: 120,
      height: 170,
      child: Column(
        children: [
          Text(
            title,
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w700,
              color: color ?? kinsBlack26,
            ),
          ),
          Expanded(
            child: Center(
              child: renderImageWidget(borderColor: color, size: size),
            ),
          ),
          InkWell(
            child: renderNameWidget(selectItem, borderColor: color),
            onTap: onTap,
          ),
        ],
      ),
    );
  }

  Widget renderImageWidget({Color? borderColor, double? size = 87}) {
    double height = size!;
    double radiusCircular = (height + 12) / 2;
    double borderWidth = borderColor == null ? 0 : 2;

    return Container(
      padding: EdgeInsets.all(4),
      decoration: BoxDecoration(
        border: Border.all(
          color: borderColor ?? Colors.white,
          width: borderWidth,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(radiusCircular),
        ),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.all(
          Radius.circular(radiusCircular),
        ),
        child: renderNonImage(height),
      ),
    );
  }

  Widget renderNonImage(double imageHeight) {
    return SvgPicture.asset(
      'assets/svgs/ic/ic_profile_signin.svg',
      width: imageHeight,
      height: imageHeight,
    );
  }

  Widget renderNameWidget(String selectItem, {Color? borderColor}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      width: 130,
      height: 37,
      decoration: BoxDecoration(
        border: Border.all(
          color: borderColor ?? kinsBlueD7,
          width: 2,
        ),
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
      ),
      child: Row(
        children: [
          Text(
            selectItem,
            style: TextStyle(fontSize: 14, color: kinsBlack3E),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: Icon(Icons.arrow_drop_down, size: 30, color: kinsBlue40),
            ),
          )
        ],
      ),
    );
  }

  Widget renderSubmitBtn() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      height: 60,
      child: PrimaryButton(
        label: '정정요청하기',
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
      ),
    );
  }

  Future<String?> showBottomSheet(
    String title,
    String firstName,
    String secondName, {
    List<String>? addList,
  }) async {
    final sheet = RelationsChoiceSheet(
      title: title,
      firstName: firstName,
      secondName: secondName,
      addNameList: addList,
    );
    String? choiceTitle = await Get.bottomSheet<String>(sheet);

    return choiceTitle;
  }

  _nameOnTap() async {
    String? setData = await showBottomSheet('성별을 선택해주세요 :)', '남자', '여자');
    if (setData == null) return;

    isMan = setData == '남자';
    setState(() {});
  }

  _relationItemTap(
    String firstName,
    String? secondName, {
    List<String>? addList,
  }) async {
    if (secondName == null || secondName == '') {
      _showSelectTitle(firstName);
      return;
    }

    String? showTitle = await showBottomSheet(
      '가족 관계를 선택해주세요 :)',
      firstName,
      secondName,
      addList: addList,
    );
    _showSelectTitle(showTitle);
  }
}
