import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-01-28
 작성자 : Mark,
 화면명 : (화면명),
 경로 : /spinor/mark-test/my-kins
 클래스 : MyKinsScreen,
 설명 : MyKinsScreen Test
*/

class MyKinsScreen extends StatefulWidget {
  @override
  MyKinsState createState() => MyKinsState();
}

class MyKinsState extends State<MyKinsScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          renderTopColumn(),
          Container(
            width: double.infinity,
            height: 6,
            color: kinsBlue3F,
          ),
          renderBottomColumn(),
        ],
      ),
    );
  }

  renderTopColumn() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 62),
          renderTitle(),
          SizedBox(height: 20),
          renderUser(),
          SizedBox(height: 30),
        ],
      ),
    );
  }

  renderTitle() {
    return Text(
      '마이 킨즈',
      style: TextStyle(
        color: kinsBlack,
        fontWeight: FontWeight.w700,
        fontSize: ThemeFactory.of(context).theme.fontSize22,
      ),
    );
  }

  renderUser() {
    return Container(
      height: 50,
      width: double.infinity,
      child: Row(
        children: [
          renderNonImage(50),
          SizedBox(width: 10),
          renderUserTitle(),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: Icon(
                Icons.arrow_forward_ios,
                size: 12,
              ),
            ),
          ),
          SizedBox(width: 23),
        ],
      ),
    );
  }

  Widget renderNonImage(double imageHeight) {
    return SvgPicture.asset(
      'assets/svgs/ic/ic_profile_signin.svg',
      width: imageHeight,
      height: imageHeight,
    );
  }

  Widget renderUserTitle() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          '홍길동',
          style: TextStyle(
            color: kinsBlack,
            fontWeight: FontWeight.w400,
            fontSize: ThemeFactory.of(context).theme.fontSize16,
          ),
        ),
        SizedBox(height: 6),
        Text(
          '010.1234.5678',
          style: TextStyle(
            color: kinsGray6D,
            fontWeight: FontWeight.w400,
            fontSize: ThemeFactory.of(context).theme.fontSize11,
          ),
        ),
      ],
    );
  }

  renderBottomColumn() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            renderBtnRow(),
            Container(
              width: double.infinity,
              height: 1,
              color: kinsGrayE4,
            ),
            SizedBox(height: 10),
            renderRow('assets/svgs/ic/ic_message_setting.svg', '인증수단 관리'),
            renderRow('assets/svgs/ic/ic_alarm_setting.svg', '알림 설정'),
            renderRow('assets/svgs/ic/ic_notice_setting.svg', '공지사항 / 이용안내'),
            renderRow('assets/svgs/ic/ic_faq_setting.svg', '1:1문의/FAQ'),
            renderRow('assets/svgs/ic/ic_advertise_setting.svg', '광고문의'),
            SizedBox(height: 28),
            Container(
              padding: EdgeInsets.all(16),
              alignment: Alignment.centerRight,
              child: Text(
                "로그아웃",
                style: TextStyle(
                  color: kinsGray8A,
                  fontWeight: FontWeight.w400,
                  fontSize: ThemeFactory.of(context).theme.fontSize14,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  renderBtnRow() {
    return Container(
      width: double.infinity,
      height: 107,
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          renderBtn('assets/svgs/ic/ic_family_setting.svg', '나의 가족'),
          SizedBox(width: 44),
          renderBtn('assets/svgs/ic/ic_anniversary_setting.svg', '기념일 관리'),
          SizedBox(width: 44),
          renderBtn('assets/svgs/ic/ic_tree_setting.svg', '킨즈 나이테'),
        ],
      ),
    );
  }

  renderBtn(String imagePath, String title) {
    return InkWell(
      child: Padding(
        padding: EdgeInsets.all(5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SvgPicture.asset(imagePath, width: 48, height: 48),
            SizedBox(height: 8),
            Text(
              title,
              style: TextStyle(
                color: kinsBlack44,
                fontWeight: FontWeight.w400,
                fontSize: ThemeFactory.of(context).theme.fontSize12,
              ),
            )
          ],
        ),
      ),
      onTap: () {},
    );
  }

  renderRow(String imagePath, String title) {
    return InkWell(
      child: Container(
        height: 40,
        width: double.infinity,
        child: Row(
          children: [
            SvgPicture.asset(imagePath, width: 28, height: 28),
            SizedBox(width: 20),
            Text(
              title,
              style: TextStyle(
                color: kinsBlack44,
                fontWeight: FontWeight.w400,
                fontSize: ThemeFactory.of(context).theme.fontSize15,
              ),
            ),
          ],
        ),
      ),
      onTap: () {},
    );
  }
}
