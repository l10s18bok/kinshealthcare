import 'package:flutter/cupertino.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/backdrop/backdrop.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/text_field/money_input.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/keyboard_model_notifier.dart';

/*
 작성일 : 2021-02-25
 작성자 : Mark,
 화면명 : ,
 경로 : ,
 클래스 : CustomKeyboardLayout,
 설명 : Custom Keyboard 를 사용하기 위한 Layout,
*/

class KeyboardLayout extends StatefulWidget {
  KeyboardLayout({
    Key? key,
    required this.body,
    required this.scrollController,
    required this.keyboardVisibleNotifier,
    required this.customTec,
    required this.onKeyboardInput,
  }) : super(key: key);

  final Widget body;
  final ScrollController scrollController;
  final KeyboardModelNotifier keyboardVisibleNotifier;
  final TextEditingController customTec;
  final ValueSetter<String?> onKeyboardInput;

  @override
  KeyboardLayoutState createState() => KeyboardLayoutState();
}

class KeyboardLayoutState extends State<KeyboardLayout> {
  double backdropHeight = 380;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: GestureDetector(
        child: Backdrop(
          frontLayer: renderKeyboard(),
          backLayer: widget.body,
          panelVisible: widget.keyboardVisibleNotifier,
          frontPanelHeight: backdropHeight,
          frontHeaderHeight: 0,
          frontHeaderVisibleClosed: false,
          scrollController: widget.scrollController,
        ),
        onTap: () => widget.keyboardVisibleNotifier.value = false,
      ),
      onWillPop: () async {
        if (widget.keyboardVisibleNotifier.value == false) return true;
        widget.keyboardVisibleNotifier.value = false;
        return false;
      },
    );
  }

  renderKeyboard() {
    return Column(
      key: ValueKey<int>(1),
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        MoneyInput(
          onKeyboardInput: widget.onKeyboardInput,
          onConfirm: _onKeyboardConfirm,
          height: backdropHeight,
          textController: widget.customTec,
        ),
      ],
    );
  }

  _onKeyboardConfirm() {
    widget.keyboardVisibleNotifier.value = false;
  }
}
