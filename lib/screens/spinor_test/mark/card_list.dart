import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/card/add_credit_card.dart';
import 'package:kins_healthcare/components/card/credit_card.dart';
import 'package:kins_healthcare/components/listview/card_list_view.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/models/local/card_model.dart';
import 'package:kins_healthcare/utils/resource.dart';

import 'components/bottom_sheet/error_bottom_sheet.dart';

/*
 작성일 : 2021-03-29
 작성자 : Mark,
 화면명 : ,
 경로 : ,
 클래스 : CardList,
 설명 : 카드를 선택하는 ListView 작업 Example
*/

class CardList extends StatefulWidget {
  static const String routeName = '/examples/card_list';

  @override
  CardListState createState() => CardListState();
}

class CardListState extends State<CardList> {
  int nowIndex = 0;
  final List<CardModel> list = [];

  @override
  void initState() {
    super.initState();
    list.addAll([bcCard, bnkCard, ctCard, dgbCard, ehCard]);
    list.addAll([gjCard, hdCard, hnCard, ibkCard, jbCard]);
    list.addAll([kakaoCard, kbCard, kBankCard, kdbCard, lotteCard]);
    list.addAll([nhCard, sbiCard, scCard, shCard, sinhCard]);
    list.addAll([smeCard, ssCard, suhCard, ucgCard, urCard]);
    list.add(nonCard);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            BackTopBar(title: 'Card List'),
            renderListView(),
            Expanded(child: renderSelectCard()),
          ],
        ),
      ),
    );
  }

  Widget renderListView() {
    return SizedBox(
      width: double.infinity,
      height: 130,
      child: CardListView(
        list: list,
        nowIndex: nowIndex,
        endScrollCallBack: _endScrollCallBack,
        addCardOnTap: _addCardOnTap,
      ),
    );
  }

  Widget renderSelectCard() {
    final model = list[nowIndex];
    if (model.isNon) return Center(child: AddCreditCard());
    return Center(child: CreditCard(width: 320, height: 200, model: model));
  }

  _endScrollCallBack(int index) {
    nowIndex = index;
    setState(() {});
  }

  _addCardOnTap() {
    final sheet = ErrorBottomSheet(
      title: '_addCardOnTap()',
      content: 'Card Add OnTap 호출됨',
    );
    Get.bottomSheet(sheet);
  }
}
