import 'package:kins_healthcare/utils/resource.dart';

import 'model/group_item_model.dart';

/**
 * @author  이명재
 * @version 1.0
 * @since   2021-01-22
 */

//가족관계도 셈플 데이터
List<GroupItemModel> mEndGroupModel = [
  GroupItemModel(
    distance: '4촌',
    firstTitle: '고모할아버지',
    thirdTitle: '고모할머니',
    secondTitle: '이모할아버지',
    fourthTitle: '이모할머니',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
  GroupItemModel(
    distance: '4촌',
    firstTitle: '맏할아버지',
    thirdTitle: '맏할머니',
    secondTitle: '외할아버지',
    fourthTitle: '외할머니',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
  GroupItemModel(
    distance: '5촌',
    firstTitle: '(이름)아저씨',
    thirdTitle: '(이름)아주머니',
    secondTitle: '(이름)아저씨',
    fourthTitle: '(이름)아주머니',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
  GroupItemModel(
    distance: '5촌',
    firstTitle: '(이름)아저씨',
    thirdTitle: '(이름)아주머니',
    secondTitle: '외아저씨',
    fourthTitle: '외아주머니',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
];

List<GroupItemModel> mSideGroupModel = [
  GroupItemModel(
    distance: '3촌',
    firstTitle: '고모',
    thirdTitle: '고모부',
    secondTitle: '이모',
    fourthTitle: '이모부',
  ),
  GroupItemModel(
    distance: '3촌',
    firstTitle: '큰아버지',
    thirdTitle: '작은아버지',
    secondTitle: '외삼촌',
    fourthTitle: '외숙모',
    firstAddTitle: ['큰어머니', '작은어머니'],
  ),
  GroupItemModel(
    distance: '4촌',
    firstTitle: '고종사촌',
    secondTitle: '이종사촌',
  ),
  GroupItemModel(
    distance: '4촌',
    firstTitle: '친삼촌',
    secondTitle: '외종삼촌',
  ),
  GroupItemModel(
    distance: '5촌',
    firstTitle: '고종사촌',
    thirdTitle: '조카',
    secondTitle: '이종사촌',
    fourthTitle: '조카',
  ),
  GroupItemModel(
    distance: '5촌',
    firstTitle: '친사촌',
    thirdTitle: '조카',
    secondTitle: '외종사촌',
    fourthTitle: '조카',
  ),
];

List<GroupItemModel> fEndGroupModel = [
  GroupItemModel(
    distance: '4촌',
    firstTitle: '고모할아버지',
    thirdTitle: '고모할머니',
    secondTitle: '이모할아버지',
    fourthTitle: '이모할머니',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
  GroupItemModel(
    distance: '4촌',
    firstTitle: '맏할아버지',
    thirdTitle: '맏할머니',
    secondTitle: '외할아버지',
    fourthTitle: '외할머니',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
  GroupItemModel(
    distance: '5촌',
    firstTitle: '(이름)아저씨',
    thirdTitle: '(이름)아주머니',
    secondTitle: '(이름)아저씨',
    fourthTitle: '(이름)아주머니',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
  GroupItemModel(
    distance: '5촌',
    firstTitle: '(이름)아저씨',
    thirdTitle: '(이름)아주머니',
    secondTitle: '외아저씨',
    fourthTitle: '외아주머니',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
];

List<GroupItemModel> fSideGroupModel = [
  GroupItemModel(
    distance: '3촌',
    firstTitle: '고모',
    thirdTitle: '고모부',
    secondTitle: '이모',
    fourthTitle: '이모부',
  ),
  GroupItemModel(
    distance: '3촌',
    firstTitle: '큰아버지',
    thirdTitle: '작은아버지',
    secondTitle: '외삼촌',
    fourthTitle: '외숙모',
    firstAddTitle: ['큰어머니', '작은어머니'],
  ),
  GroupItemModel(
    distance: '4촌',
    firstTitle: '고종사촌',
    secondTitle: '이종사촌',
  ),
  GroupItemModel(
    distance: '4촌',
    firstTitle: '친삼촌',
    secondTitle: '외종삼촌',
  ),
  GroupItemModel(
    distance: '5촌',
    firstTitle: '고종사촌',
    thirdTitle: '조카',
    secondTitle: '이종사촌',
    fourthTitle: '조카',
  ),
  GroupItemModel(
    distance: '5촌',
    firstTitle: '친사촌',
    thirdTitle: '조카',
    secondTitle: '외종사촌',
    fourthTitle: '조카',
  ),
];

List<GroupItemModel> topGroupModel = [
  GroupItemModel(
    distance: '3촌',
    firstTitle: '증조 할아버지',
    thirdTitle: '증조 할머니',
    secondTitle: '외증조 할아버지',
    fourthTitle: '외증조 할머니',
  ),
  GroupItemModel(
    distance: '3촌',
    firstTitle: '증조 할아버지',
    thirdTitle: '증조 할머니',
    secondTitle: '외증조 할아버지',
    fourthTitle: '외증조 할머니',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
  GroupItemModel(
    distance: '2촌',
    firstTitle: '할아버지',
    thirdTitle: '할머니',
    secondTitle: '외할아버지',
    fourthTitle: '외할머니',
  ),
  GroupItemModel(
    distance: '2촌',
    firstTitle: '할아버지',
    thirdTitle: '할머니',
    secondTitle: '외할아버지',
    fourthTitle: '외할머니',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
  GroupItemModel(
    distance: '1촌',
    firstTitle: '시아버지',
    secondTitle: '시어머니',
  ),
  GroupItemModel(
    distance: '1촌',
    firstTitle: '아버지',
    secondTitle: '어머니',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
];

List<GroupItemModel> topManGroupModel = [
  GroupItemModel(
    distance: '3촌',
    firstTitle: '증조 할아버지',
    thirdTitle: '증조 할머니',
    secondTitle: '외증조 할아버지',
    fourthTitle: '외증조 할머니',
  ),
  GroupItemModel(
    distance: '3촌',
    firstTitle: '증조 할아버지',
    thirdTitle: '증조 할머니',
    secondTitle: '외증조 할아버지',
    fourthTitle: '외증조 할머니',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
  GroupItemModel(
    distance: '2촌',
    firstTitle: '할아버지',
    thirdTitle: '할머니',
    secondTitle: '외할아버지',
    fourthTitle: '외할머니',
  ),
  GroupItemModel(
    distance: '2촌',
    firstTitle: '할아버지',
    thirdTitle: '할머니',
    secondTitle: '외할아버지',
    fourthTitle: '외할머니',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
  GroupItemModel(
    distance: '1촌',
    firstTitle: '아버지',
    secondTitle: '어머니',
  ),
  GroupItemModel(
    distance: '1촌',
    firstTitle: '장인어른',
    secondTitle: '장모님',
    firstPath: logoPath03,
    secondPath: logoPath04,
  ),
];

List<GroupItemModel> fBottomGroupModel = [
  GroupItemModel(
    distance: '',
    firstTitle: '형님',
    thirdTitle: '아주버님',
    secondTitle: '아주버님',
    fourthTitle: '형님',
    firstPath: logoPath01,
    secondPath: logoPath01,
  ),
  GroupItemModel(
    distance: '',
    firstTitle: '아가씨',
    thirdTitle: '서방님',
    secondTitle: '도련님',
    fourthTitle: '동서',
    firstPath: logoPath01,
    secondPath: logoPath01,
  ),
  GroupItemModel(
    distance: '3촌(조카)',
    firstTitle: '자매',
    thirdTitle: '조카/조카딸',
    secondTitle: '형제',
    fourthTitle: '조카/조카딸',
    firstPath: logoPath01,
    secondPath: logoPath01,
  ),
  GroupItemModel(
    distance: '4촌',
    firstTitle: '이손자',
    thirdTitle: '이손녀',
    secondTitle: '증손자',
    fourthTitle: '증손녀',
    firstPath: logoPath01,
    secondPath: logoPath01,
  ),
  GroupItemModel(
    distance: '1촌',
    firstTitle: '아들',
    secondTitle: '딸',
  ),
  GroupItemModel(
    distance: '2촌',
    firstTitle: '손자',
    thirdTitle: '손녀',
    secondTitle: '외손자',
    fourthTitle: '외손녀',
  ),
  GroupItemModel(
    distance: '',
    firstTitle: '언니',
    thirdTitle: '형부',
    secondTitle: '오빠',
    fourthTitle: '올케',
    firstPath: logoPath02,
    secondPath: logoPath02,
  ),
  GroupItemModel(
    distance: '',
    firstTitle: '여동생',
    thirdTitle: '제부',
    secondTitle: '남동생',
    fourthTitle: '올케',
    firstPath: logoPath02,
    secondPath: logoPath02,
  ),
  GroupItemModel(
    distance: '3촌(조카)',
    firstTitle: '자매',
    thirdTitle: '조카/조카딸',
    secondTitle: '형제',
    fourthTitle: '조카/조카딸',
    firstPath: logoPath02,
    secondPath: logoPath02,
  ),
  GroupItemModel(
    distance: '4촌',
    firstTitle: '이손자',
    thirdTitle: '이손녀',
    secondTitle: '증손자',
    fourthTitle: '증손녀',
    firstPath: logoPath02,
    secondPath: logoPath02,
  ),
];

List<GroupItemModel> mBottomGroupModel = [
  GroupItemModel(
    distance: '',
    firstTitle: '누나',
    thirdTitle: '매형',
    secondTitle: '형',
    fourthTitle: '형수',
    firstPath: logoPath01,
    secondPath: logoPath01,
  ),
  GroupItemModel(
    distance: '',
    firstTitle: '여동생',
    thirdTitle: '매제',
    secondTitle: '남동생',
    fourthTitle: '제수',
    firstPath: logoPath01,
    secondPath: logoPath01,
  ),
  GroupItemModel(
    distance: '3촌(조카)',
    firstTitle: '자매',
    thirdTitle: '조카/조카딸',
    secondTitle: '형제',
    fourthTitle: '조카/조카딸',
    firstPath: logoPath01,
    secondPath: logoPath01,
  ),
  GroupItemModel(
    distance: '4촌',
    firstTitle: '이손자',
    thirdTitle: '이손녀',
    secondTitle: '증손자',
    fourthTitle: '증손녀',
    firstPath: logoPath01,
    secondPath: logoPath01,
  ),
  GroupItemModel(
    distance: '1촌',
    firstTitle: '아들',
    secondTitle: '딸',
  ),
  GroupItemModel(
    distance: '2촌',
    firstTitle: '손자',
    thirdTitle: '손녀',
    secondTitle: '외손자',
    fourthTitle: '외손녀',
  ),
  GroupItemModel(
    distance: '',
    firstTitle: '처형',
    thirdTitle: '형님',
    secondTitle: '형님',
    fourthTitle: '아주버님',
    firstPath: logoPath02,
    secondPath: logoPath02,
  ),
  GroupItemModel(
    distance: '',
    firstTitle: '처제',
    thirdTitle: '동서',
    secondTitle: '처남',
    fourthTitle: '처남댁',
    firstPath: logoPath02,
    secondPath: logoPath02,
  ),
  GroupItemModel(
    distance: '3촌(조카)',
    firstTitle: '자매',
    thirdTitle: '조카/조카딸',
    secondTitle: '형제',
    fourthTitle: '조카/조카딸',
    firstPath: logoPath02,
    secondPath: logoPath02,
  ),
  GroupItemModel(
    distance: '4촌',
    firstTitle: '이손자',
    thirdTitle: '이손녀',
    secondTitle: '증손자',
    fourthTitle: '증손녀',
    firstPath: logoPath02,
    secondPath: logoPath02,
  ),
];

//게시판 셈플 데이터

final noticeTitle = '킨즈 오픈이벤트 진행합니다 [2020.09~10] 2줄 테스트 입니다. 2줄 테스트 입니다.';
final infoTitle = '개인정보처리방침에 관한 사항입니다. 예시 텍스트 2줄 테스트 테스트 테스트 테스트';

final regDate = '2020.11.30';
final content = '''안녕하세요, 킨즈입니다.



12월 30일 부터 킨즈헬스가 정상오픈합니다.

그래서 이렇게 안내를 드리고자 예시 텍스트입니다예시 텍스트입니다예시 텍스트입니다예시 텍스트입니다예시 텍스트입니다예시 텍스트입니다.

예시 텍스트입니다예시 텍스트입니다예시 텍스트입니다예시 텍스트입니다예시 텍스트입니다''';

final kkakkaJson = '''{
      "data" : [
      {
              "kkakkaId": 111,
              "kkakkaType": "의료",
              "kkakkaStatus": "활성",
              "startDate": "2021-01-23",
              "endDate": "2021-03-07",
              "kkakkaPrice": 10000,
              "kkakkaLimit": 10000,
              "kkakkaBalance": 6774,
              "kkakkaMessage": "message1",
              "kkakkaImage": "https://test-image-sm.s3.ap-northeast-2.amazonaws.com/medi.jpg",
              "userRelCdMe": "형",
              "userRelCdOpponent": "남동생",
              "userStatusOpponent": "정상",
              "userNameOpponent": "박소담",
              "userPhoneOpponent": "01012345678",
              "profileImgOpponent": "https://test-image-sm.s3.ap-northeast-2.amazonaws.com/sister2.jpg"
            },
            {
              "kkakkaId": 112,
              "kkakkaType": "기부",
              "kkakkaStatus": "활성",
              "startDate": "2021-01-05",
              "endDate": "2021-04-21",
              "kkakkaPrice": 10000,
              "kkakkaLimit": 10000,
              "kkakkaBalance": 3412,
              "kkakkaMessage": "mes2",
              "kkakkaImage": "https://test-image-sm.s3.ap-northeast-2.amazonaws.com/donation.jpg",
              "userRelCdMe": "형",
              "userRelCdOpponent": "남동생",
              "userStatusOpponent": "정상",
              "userNameOpponent": "안정환",
              "userPhoneOpponent": "01012345678",
              "profileImgOpponent": "https://test-image-sm.s3.ap-northeast-2.amazonaws.com/father1.jpeg"
            },
            {
              "kkakkaId": 113,
              "kkakkaType": "기념일",
              "kkakkaStatus": "활성",
              "startDate": "2021-01-05",
              "endDate": "2021-03-22",
              "kkakkaPrice": 10000,
              "kkakkaLimit": 10000,
              "kkakkaBalance": 3329,
              "kkakkaMessage": "message3",
              "kkakkaImage": "https://test-image-sm.s3.ap-northeast-2.amazonaws.com/wedding.jpg",
              "userRelCdMe": "형",
              "userRelCdOpponent": "남동생",
              "userStatusOpponent": "정상",
              "userNameOpponent": "소유진",
              "userPhoneOpponent": "01012345678",
              "profileImgOpponent": "https://test-image-sm.s3.ap-northeast-2.amazonaws.com/mother.jpg"
            },
            {
            "kkakkaBalance": 7800,
            "kkakkaId": 0,
            "kkakkaImage": null,
            "kkakkaLimit": 0,
            "kkakkaMessage": "공부열심히해서 시험잘보자 우리 동생!",
            "kkakkaPrice": 12000,
            "kkakkaStatus": "활성",
            "kkakkaType": "SELF",
            "endDate": "2021-03-22",
            "profileImgOpponent": "string",
            "startDate": "2021-01-05",
            "userNameOpponent": "string",
            "userPhoneOpponent": "string",
            "userRelCdMe": "string",
            "userRelCdOpponent": "string",
            "userStatusOpponent": "string"
            },
            {
            "kkakkaBalance": 95000,
            "kkakkaId": 0,
            "kkakkaImage": null,
            "kkakkaLimit": 0,
            "kkakkaMessage": "많이 먹어야해 이럴때! 알지? 요즘 많이 목먹는거 같아서 속상하다 이자식아!",
            "kkakkaPrice": 30000,
            "kkakkaStatus": "해제",
            "kkakkaType": "용돈",
            "endDate": "2021-03-22",
            "profileImgOpponent": "string",
            "startDate": "2021-01-05",
            "userNameOpponent": "string",
            "userPhoneOpponent": "string",
            "userRelCdMe": "string",
            "userRelCdOpponent": "string",
            "userStatusOpponent": "string"
            },
            {
            "kkakkaBalance": 2000,
            "kkakkaId": 0,
            "kkakkaImage": null,
            "kkakkaLimit": 0,
            "kkakkaMessage": "공부열심히해서 시험잘보자 우리 동생!",
            "kkakkaPrice": 12000,
            "kkakkaStatus": "활성",
            "kkakkaType": "의료",
            "endDate": "2021-03-22",
            "profileImgOpponent": "string",
            "startDate": "2021-01-05",
            "userNameOpponent": "string",
            "userPhoneOpponent": "string",
            "userRelCdMe": "string",
            "userRelCdOpponent": "string",
            "userStatusOpponent": "string"
            },
            {
            "kkakkaBalance": 57600,
            "kkakkaId": 0,
            "kkakkaImage": null,
            "kkakkaLimit": 0,
            "kkakkaMessage": "많이 먹어야해 이럴때! 알지? 요즘 많이 목먹는거 같아서 속상하다 이자식아!",
            "kkakkaPrice": 30000,
            "kkakkaStatus": "모두사용",
            "kkakkaType": "기념일",
            "endDate": "2021-03-22",
            "profileImgOpponent": "string",
            "startDate": "2021-01-05",
            "userNameOpponent": "string",
            "userPhoneOpponent": "string",
            "userRelCdMe": "string",
            "userRelCdOpponent": "string",
            "userStatusOpponent": "string"
            },
            {
            "kkakkaBalance": 1203000,
            "kkakkaId": 0,
            "kkakkaImage": null,
            "kkakkaLimit": 0,
            "kkakkaMessage": "공부열심히해서 시험잘보자 우리 동생!",
            "kkakkaPrice": 12000,
            "kkakkaStatus": "활성",
            "kkakkaType": "기부",
            "endDate": "2021-03-22",
            "profileImgOpponent": "string",
            "startDate": "2021-01-05",
            "userNameOpponent": "string",
            "userPhoneOpponent": "string",
            "userRelCdMe": "string",
            "userRelCdOpponent": "string",
            "userStatusOpponent": "string"
            }]
      }''';

final questionJson = '''{
      "resultCode" : "SUCCESS",
      "data" : {
        "total": 10,
        "values":[
          {
           "id":1,
           "title":"1:1 문의 제목",
           "content":"1:1 문의 내용",
           "answer":"1:1 문의 답변 내용 입니다.", 
           "answer_at":"Y", 
           "dates":"2021-03-22"
           },
          {
           "id":1,
           "title":"1:1 문의 제목",
           "content":"1:1 문의 내용",
           "answer":"1:1 문의 답변 내용 입니다.", 
           "answer_at":"Y", 
           "dates":"2021-03-22"
           },
           {
           "id":1,
           "title":"1:1 문의 제목",
           "content":"1:1 문의 내용",
           "answer":"1:1 문의 답변 내용 입니다.", 
           "answer_at":"Y", 
           "dates":"2021-03-22"
           },
           {
           "id":1,
           "title":"1:1 문의 제목",
           "content":"1:1 문의 내용",
           "answer":"1:1 문의 답변 내용 입니다.", 
           "answer_at":"Y", 
           "dates":"2021-03-22"
           },
           {
           "id":1,
           "title":"1:1 문의 제목",
           "content":"1:1 문의 내용",
           "answer":"1:1 문의 답변 내용 입니다.", 
           "answer_at":"Y", 
           "dates":"2021-03-22"
           },
           {
           "id":1,
           "title":"1:1 문의 제목",
           "content":"1:1 문의 내용",
           "answer":"1:1 문의 답변 내용 입니다.", 
           "answer_at":"Y", 
           "dates":"2021-03-22"
           },
           {
           "id":1,
           "title":"1:1 문의 제목",
           "content":"1:1 문의 내용",
           "answer":"1:1 문의 답변 내용 입니다.", 
           "answer_at":"Y", 
           "dates":"2021-03-22"
           },
           {
           "id":1,
           "title":"1:1 문의 제목",
           "content":"1:1 문의 내용",
           "answer":"1:1 문의 답변 내용 입니다.", 
           "answer_at":"Y", 
           "dates":"2021-03-22"
           },
           {
           "id":1,
           "title":"1:1 문의 제목",
           "content":"1:1 문의 내용",
           "answer":"1:1 문의 답변 내용 입니다.", 
           "answer_at":"Y", 
           "dates":"2021-03-22"
           },
           {
           "id":1,
           "title":"1:1 문의 제목",
           "content":"1:1 문의 내용",
           "answer":"1:1 문의 답변 내용 입니다.", 
           "answer_at":"Y", 
           "dates":"2021-03-22"
           } 
         ]
      }
}''';
