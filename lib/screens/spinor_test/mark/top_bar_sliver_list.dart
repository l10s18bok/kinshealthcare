import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/payment_user_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

import '../../../components/card/issuance_target_card.dart';
import '../../../components/top_bar/back_top_bar.dart';
import 'layout/custom_list_layout.dart';

/*
 작성일 : 2021-02-10
 작성자 : Mark,
 화면명 : (화면명),
 경로 : /spinor/mark-test/top-bar-sliver-list--test
 클래스 : TopSliverList,
 설명 : TopBar SliverList Example 파일
*/

class TopSliverList extends StatefulWidget {
  @override
  TopSliverListState createState() => TopSliverListState();
}

class TopSliverListState extends State<TopSliverList> {
  bool isChecked = false;
  List<PaymentUserModel> userList = [];

  @override
  void initState() {
    super.initState();

    userList = _getUserList();
  }

  @override
  Widget build(BuildContext context) {
    return CustomListLayout(
      topBar: renderTopBar(),
      body: renderSliverListView(),
      bottomBtn: renderSubmitBtn(),
    );
  }

  Widget renderTopBar() {
    return BackTopBar(
      title: '까까 발행',
      rightButtons: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [renderPlusBtn()],
      ),
    );
  }

  Widget renderPlusBtn() {
    return InkWell(
      child: Container(
        width: 50,
        color: Colors.transparent,
        child: Icon(Icons.add),
      ),
      onTap: () {
        for (int i = 0; i < 10; ++i) {
          userList.addAll(_getUserList());
        }
        setState(() {});
      },
    );
  }

  Widget renderSliverListView() {
    return SliverList(
      delegate: SliverChildBuilderDelegate(
        (context, index) {
          final modelIndex = index - 1;
          if (modelIndex == -1) return renderFirstItem();

          return IssuanceTargetCard(
            model: userList[modelIndex],
            onTap: () => setState(() {}),
          );
        },
        childCount: userList.length + 1,
      ),
    );
  }

  Widget renderFirstItem() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          renderTitle(),
          SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget renderTitle() {
    return Text(
      '까까 받을\n대상자를 선택하세요.',
      style: TextStyle(
        color: kinsBlack30,
        fontWeight: FontWeight.w700,
        fontSize: ThemeFactory.of(context).theme.fontSize17,
      ),
    );
  }

  Widget renderSubmitBtn() {
    return Container(
      margin: EdgeInsets.only(bottom: 16, left: 16, right: 16),
      height: 60,
      child: PrimaryButton(
        label: '다음',
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
      ),
    );
  }

  List<PaymentUserModel> _getUserList() {
    return [
      PaymentUserModel(
        name: '아빠',
        phoneNum: '010.1234.5678',
        nickName: '아바마마',
        isOnContacts: true,
        userRelNo: 0,
      ),
      PaymentUserModel(
        name: '누나',
        phoneNum: '010.1234.5678',
        nickName: '이쁘니',
        isOnContacts: false,
        userRelNo: 0,
      ),
      PaymentUserModel(
        name: '딸',
        phoneNum: '010.1234.5678',
        nickName: '서니',
        isOnContacts: true,
        userRelNo: 0,
      ),
    ];
  }
}
