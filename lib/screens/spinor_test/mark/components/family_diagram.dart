import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/zoom/zoom_widget.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/group_item_model.dart';
import '../../../../utils/resource.dart';

/**
 * @author  이명재
 * @version 1.0
 * @since   2021-01-21
 */

/*
 작성일 : 2021-01-21
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : FamilyDiagram,
 설명 : 가족 관계도를 표시하는 widget
*/

typedef OnItemTap = void Function(
  String firstName,
  String? secondName, {
  List<String>? addList,
});

// ignore: must_be_immutable
class FamilyDiagram extends StatelessWidget {
  double zoomContainerHeight, ratio;
  bool isMan;
  List<GroupItemModel> mEndGroupModel, mSideGroupModel;
  List<GroupItemModel> fEndGroupModel, fSideGroupModel;
  List<GroupItemModel> topGroupModel, bottomGroupModel;
  OnItemTap? onItemTap;

  FamilyDiagram({
    required this.ratio,
    required this.mEndGroupModel,
    required this.mSideGroupModel,
    required this.fEndGroupModel,
    required this.fSideGroupModel,
    required this.topGroupModel,
    required this.bottomGroupModel,
    required this.isMan,
    this.zoomContainerHeight = 320,
    this.onItemTap,
  });

  @override
  Widget build(BuildContext context) {
    double zoomViewWidth = 3600;
    double zoomViewHeight = 3600 / ratio;

    return Container(
      width: double.infinity,
      height: zoomContainerHeight,
      decoration: BoxDecoration(
        border: Border.all(color: kinsBlue44, width: 1),
        borderRadius: BorderRadius.all(
          Radius.circular(6),
        ),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(6),
        child: ZoomCS(
          width: zoomViewWidth,
          height: zoomViewHeight,
          enableScroll: false,
          initZoom: 0.0,
          onPositionUpdate: (Offset position) {},
          onScaleUpdate: (double scale, double zoom) {},
          child: renderContentRow(),
        ),
      ),
    );
  }

  Widget renderContentRow() {
    return Container(
      alignment: Alignment.center,
      child: Row(
        children: [
          Container(
            width: 700,
            child: renderEndColumn(mEndGroupModel),
          ),
          Container(
            width: 700,
            child: renderSideColumn(mSideGroupModel),
          ),
          Container(
            width: 800,
            child: renderCenterColumn(topGroupModel, bottomGroupModel),
          ),
          Container(
            width: 700,
            child: renderSideColumn(fSideGroupModel),
          ),
          Container(
            width: 700,
            child: renderEndColumn(fEndGroupModel),
          ),
        ],
      ),
    );
  }

  Widget renderEndColumn(List<GroupItemModel> endModel) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        renderNoLineTowItemRow(endModel[0], endModel[1]),
        renderHorizontalLine(50),
        renderNoLineTowItemRow(endModel[2], endModel[3]),
        SizedBox(height: 300),
      ],
    );
  }

  Widget renderSideColumn(List<GroupItemModel> sideGroupModel) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: 150),
        renderNoLineTowItemRow(sideGroupModel[0], sideGroupModel[1]),
        renderHorizontalLine(50),
        renderNoLineTowItemRow(sideGroupModel[2], sideGroupModel[3]),
        renderHorizontalLine(50),
        renderNoLineTowItemRow(sideGroupModel[4], sideGroupModel[5]),
      ],
    );
  }

  Widget renderCenterColumn(
    List<GroupItemModel> topGroupModel,
    List<GroupItemModel> bottomGroupModel,
  ) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        renderBasicText('해당하는 아이콘을 누르시면 가족관계 선택이 됩니다.', fontSize: 30),
        SizedBox(height: 20),
        renderTitle(),
        SizedBox(height: 50),
        renderCenterTopColumn(topGroupModel),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              height: 20,
              alignment: Alignment.center,
              child: renderHorizontalLine(double.infinity),
            ),
            Container(
              height: 20,
              alignment: Alignment.center,
              child: renderHorizontalLine(double.infinity),
            ),
          ],
        ),
        renderMainGroup(),
        renderCenterBottomRow(bottomGroupModel),
      ],
    );
  }

  Widget renderCenterTopColumn(List<GroupItemModel> topGroupModel) {
    return Column(
      children: [
        renderFullTowItemRow(topGroupModel[0], topGroupModel[1]),
        renderFullTowItemRow(topGroupModel[2], topGroupModel[3]),
        renderFullLineRow(topGroupModel[4], topGroupModel[5]),
      ],
    );
  }

  Widget renderCenterBottomRow(List<GroupItemModel> bottomGroupModel) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              renderGroupItemNoLine(bottomGroupModel[0]),
              renderGroupItemNoLine(bottomGroupModel[1]),
              renderGroupItemNoLine(bottomGroupModel[2]),
              renderGroupItemNoLine(bottomGroupModel[3]),
            ],
          ),
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            renderChildGroupItem(bottomGroupModel[4]),
            renderChildGroupItem(bottomGroupModel[5]),
          ],
        ),
        Expanded(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              renderGroupItemNoLine(bottomGroupModel[6]),
              renderGroupItemNoLine(bottomGroupModel[7]),
              renderGroupItemNoLine(bottomGroupModel[8]),
              renderGroupItemNoLine(bottomGroupModel[9]),
            ],
          ),
        ),
      ],
    );
  }

  Widget renderMainGroup() {
    return SizedBox(
      height: 100,
      child: Row(
        children: [
          Expanded(
            flex: 4,
            child: Container(
              alignment: Alignment.centerRight,
              child: SvgPicture.asset(
                logoPath01,
                width: 100,
                height: 100,
              ),
            ),
          ),
          Expanded(flex: 3, child: renderUserColumn()),
          Expanded(
            flex: 4,
            child: Container(
              alignment: Alignment.centerLeft,
              child: SvgPicture.asset(
                logoPath02,
                width: 100,
                height: 100,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget renderUserColumn() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              renderBasicText(isMan ? '나' : '남편'),
              renderBasicText(isMan ? '아내' : '나'),
            ],
          ),
        ),
        renderVerticalLine(double.infinity),
        renderHorizontalLine(50),
      ],
    );
  }

  Widget renderFullLineRow(GroupItemModel firstModel, GroupItemModel secondModel) {
    return Stack(
      children: [
        renderFullTowItemRow(firstModel, secondModel),
        Positioned.fill(
          child: Center(
            child: Row(
              children: [
                Expanded(flex: 1, child: renderVerticalLine(double.infinity)),
                Expanded(flex: 9, child: Container()),
                Expanded(flex: 1, child: renderVerticalLine(double.infinity)),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget renderFullTowItemRow(GroupItemModel? firstModel, GroupItemModel? secondModel) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        firstModel != null ? renderGroupItem(firstModel) : Container(),
        secondModel != null ? renderGroupItem(secondModel) : Container(),
      ],
    );
  }

  Widget renderNoLineTowItemRow(GroupItemModel? firstModel, GroupItemModel? secondModel) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        firstModel != null ? renderGroupItemNoLine(firstModel) : Container(),
        secondModel != null ? renderGroupItemNoLine(secondModel) : Container(),
      ],
    );
  }

  Widget renderGroupItemNoLine(GroupItemModel model) {
    final containerWidth = 300.0;

    return Container(
      width: containerWidth,
      child: Column(
        children: [
          SizedBox(height: 5),
          renderCenterText(model.distance),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              renderRouteWidget(
                model.firstTitle,
                model.thirdTitle,
                model.firstPath,
                model.firstAddTitle,
              ),
              renderRouteWidget(
                model.secondTitle,
                model.fourthTitle,
                model.secondPath,
                model.secondAddTitle,
              ),
            ],
          ),
          renderLineText(model.firstTitle, model.secondTitle, isNoLine: true),
          renderLineText(model.thirdTitle, model.fourthTitle, isNoLine: true),
        ],
      ),
    );
  }

  Widget renderGroupItem(GroupItemModel model) {
    final containerWidth = 300.0;

    return Container(
      width: containerWidth,
      child: Column(
        children: [
          SizedBox(height: 20),
          renderCenterText(model.distance),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              renderRouteWidget(
                model.firstTitle,
                model.thirdTitle,
                model.firstPath,
                model.firstAddTitle,
              ),
              renderRouteWidget(
                model.secondTitle,
                model.fourthTitle,
                model.secondPath,
                model.secondAddTitle,
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              renderHorizontalLine(25),
              renderHorizontalLine(25),
            ],
          ),
          renderVerticalLine(containerWidth / 2),
          renderLineText(model.firstTitle, model.secondTitle),
          renderLineText(model.thirdTitle, model.fourthTitle),
          Container(
            height: 10,
            alignment: Alignment.center,
            child: renderHorizontalLine(double.infinity),
          ),
        ],
      ),
    );
  }

  Widget renderChildGroupItem(GroupItemModel model) {
    final containerWidth = 300.0;

    return Container(
      width: containerWidth,
      child: Column(
        children: [
          Container(
            height: 20,
            alignment: Alignment.center,
            child: renderHorizontalLine(double.infinity),
          ),
          renderLineText(model.distance, ''),
          renderVerticalLine(containerWidth / 2),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              renderHorizontalLine(25),
              renderHorizontalLine(25),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              renderRouteWidget(
                model.firstTitle,
                model.thirdTitle,
                model.firstPath,
                model.firstAddTitle,
              ),
              renderRouteWidget(
                model.secondTitle,
                model.fourthTitle,
                model.secondPath,
                model.secondAddTitle,
              ),
            ],
          ),
          renderLineText(model.firstTitle, model.secondTitle, isNoLine: true),
          renderLineText(model.thirdTitle, model.fourthTitle, isNoLine: true),
        ],
      ),
    );
  }

  Widget renderLineText(String? firstTitle, String? secondTitle,
      {bool isNoLine = false}) {
    if (firstTitle == null || firstTitle == '') return Container();
    if (secondTitle == null) secondTitle = '';

    return Container(
      height: 30,
      width: double.infinity,
      alignment: Alignment.center,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(child: renderCenterText(firstTitle)),
          isNoLine == true ? Container() : renderHorizontalLine(double.infinity),
          Expanded(child: renderCenterText(secondTitle)),
        ],
      ),
    );
  }

  Widget renderCenterText(String? title) {
    if (title == null) return Container();

    return Center(child: renderBasicText(title));
  }

  Widget renderBasicText(String title, {double fontSize = 20}) {
    return Text(title, style: TextStyle(fontSize: fontSize));
  }

  Widget renderTitle() {
    return SizedBox(
      width: 1000,
      height: 70,
      child: Center(
        child: Text(
          "가족관계도(여자)",
          style: TextStyle(
            fontSize: 50,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Widget renderVerticalLine(double width) {
    return Container(color: Colors.black, width: width, height: 1);
  }

  Widget renderHorizontalLine(double height) {
    return Container(color: Colors.black, width: 1, height: height);
  }

  Widget renderRouteWidget(
    String firstName,
    String? secondName,
    String imagePath,
    List<String>? addList,
  ) {
    return ClipRRect(
      borderRadius: BorderRadius.all(
        Radius.circular(45),
      ),
      child: Material(
        color: Colors.white,
        child: InkWell(
          splashColor: Colors.grey,
          child: Padding(
            padding: EdgeInsets.all(10),
            child: SvgPicture.asset(
              imagePath,
              width: 70,
              height: 70,
            ),
          ),
          onTap: () {
            if (onItemTap == null) return;
            onItemTap!(firstName, secondName, addList: addList);
          },
        ),
      ),
    );
  }
}
