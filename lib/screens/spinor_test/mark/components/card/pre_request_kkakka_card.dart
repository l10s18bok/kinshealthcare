import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/models/kkakka_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import '../../../../../utils/resource.dart';

/*
 작성일 : 2021-02-02
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : PreRequestKkakkaCard,
 설명 : 까까 화면의 Item Card
*/

// ignore: must_be_immutable
class PreRequestKkakkaCard extends StatelessWidget {
  PreRequestKkakkaCard({
    required this.kkakka,
    this.onTap,
  });

  KkakkaModel kkakka;
  late BuildContext context;
  GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    this.context = context;

    return InkWell(
      child: Container(
        width: double.infinity,
        height: 140,
        child: Stack(
          children: [
            Positioned.fill(
              child: Container(
                padding: EdgeInsets.only(left: 8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                  border: Border.all(color: kinsGrayEE, width: 2),
                ),
                child: Padding(
                  padding: EdgeInsets.only(top: 12, left: 14, right: 14),
                  child: renderKkakkaContent(),
                ),
              ),
            ),
            Positioned(
              child: Container(
                width: 8,
                height: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(4),
                    bottomLeft: Radius.circular(4),
                  ),
                  color: _getLineColor(),
                ),
              ),
            ),
            renderNotActivity(),
          ],
        ),
      ),
      onTap: onTap,
    );
  }

  Widget renderKkakkaContent() {
    return Column(
      children: [
        renderKkakkaTop(),
        SizedBox(height: 16),
        SizedBox(
          height: 65,
          child: renderKkakka(),
        ),
      ],
    );
  }

  Widget renderKkakkaTop() {
    return Row(
      children: [
        SvgPicture.asset(
          _getIconPath(),
          width: 26,
          height: 26,
        ),
        SizedBox(width: 6),
        Text(
          _getTitle(),
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize12,
            fontWeight: FontWeight.w700,
          ),
        ),
        Expanded(child: Container()),
        Text(
          kkakka.daysTillEndDate != null
              ? '${kkakka.daysTillEndDate} 남음'
              : '사용기한 미설정',
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize12,
            fontWeight: FontWeight.w400,
            color: kinsGray7D,
          ),
        ),
      ],
    );
  }

  Widget renderKkakka() {
    final kkakkaImagePath = kkakka.kkakkaImage;
    return Row(
      children: [
        renderImage(kkakkaImagePath),
        SizedBox(width: 14),
        Container(
          padding: EdgeInsets.symmetric(vertical: 4),
          child: renderMidContent(),
        )
      ],
    );
  }

  Widget renderImage(String? path) {
    if (path == null || !path.startsWith('http')) return renderMidNoImage();
    return CachedNetworkImage(
      imageUrl: path,
      imageBuilder: (context, imageProvider) => Container(
        width: 65.0,
        height: 65.0,
        decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.circular(
            8.0,
          ),
          image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
        ),
      ),
      //placeholder: (context, url) => CircularProgressIndicator(),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }

  Widget renderMidNoImage() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey,
        borderRadius: BorderRadius.circular(
          8.0,
        ),
      ),
      width: 65.0,
      height: 65.0,
      child: Center(
        child: Text(
          'Image',
        ),
      ),
    );
  }

  Widget renderMidContent() {
    final price = numberWithComma(kkakka.kkakkaBalance);
    final name = kkakka.userNameOpponent ?? '이름';
    final path = kkakka.profileImgOpponent;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: [
        Text(
          '$price 원',
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize20,
            fontWeight: FontWeight.w700,
          ),
        ),
        Row(
          children: [
            renderUserImage(path),
            SizedBox(width: 6),
            Text(
              name,
              style: TextStyle(
                fontSize: ThemeFactory.of(context).theme.fontSize12,
                fontWeight: FontWeight.w400,
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget renderUserImage(String? path) {
    if (path == null || !path.startsWith('http')) return renderNoUserImage(18);
    return CachedNetworkImage(
      imageUrl: path,
      imageBuilder: (context, imageProvider) => Container(
        width: 18.0,
        height: 18.0,
        decoration: BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.circular(
            8.0,
          ),
          image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
        ),
      ),
      //placeholder: (context, url) => CircularProgressIndicator(),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }

  Widget renderNoUserImage(double imageHeight) {
    return SvgPicture.asset(
      'assets/svgs/ic/ic_profile_signin.svg',
      width: imageHeight,
      height: imageHeight,
    );
  }

  Widget renderNotActivity() {
    if (kkakka.kkakkaStatus == KkakkaStatus.ACTIVE) return Container();

    final title = _getNotTitle();
    final path = _getNotIconPath();

    return Positioned.fill(
      child: Opacity(
        opacity: 0.4796338762555803,
        child: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            color: kinsBlack30,
          ),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    color: kinsWhite,
                    fontWeight: FontWeight.w700,
                    fontSize: ThemeFactory.of(context).theme.fontSize12,
                  ),
                ),
                SizedBox(height: 10),
                SvgPicture.asset(path, width: 78, height: 78),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Color _getLineColor() {
    final type = kkakka.kkakkaType;
    if (type == KkakkaType.ANNIVERSARY) return kinsPinkFF;
    if (type == KkakkaType.DONATE) return kinsPuple8B;
    if (type == KkakkaType.AREA) return kinsGreen6C;
    if (type == KkakkaType.MEDICAL) return kinsBlue6F;
    if (type == KkakkaType.PIN) return kinsGreen6C;
    if (type == KkakkaType.SELF) return kinsYellowFD;
    if (type == KkakkaType.WELFARE) return kinsGreen6C;
    return kinsYellowFD;
  }

  String _getIconPath() {
    final type = kkakka.kkakkaType;
    if (type == KkakkaType.ANNIVERSARY)
      return 'assets/svgs/ic/ic_anniversary_kkakka.svg';
    if (type == KkakkaType.DONATE) return 'assets/svgs/ic/ic_donate_kkakka.svg';
    if (type == KkakkaType.AREA) return 'assets/svgs/ic/ic_pin_kkakka.svg';
    if (type == KkakkaType.MEDICAL)
      return 'assets/svgs/ic/ic_medical_kkakka.svg';
    if (type == KkakkaType.PIN) return 'assets/svgs/ic/ic_pin_kkakka.svg';
    if (type == KkakkaType.SELF)
      return 'assets/svgs/ic/ic_education_kkakka.svg';
    if (type == KkakkaType.WELFARE) return 'assets/svgs/ic/ic_pin_kkakka.svg';
    return 'assets/svgs/ic/ic_education_kkakka.svg';
  }

  String _getTitle() {
    final type = kkakka.kkakkaType;
    if (type == KkakkaType.ANNIVERSARY) return '기념일';
    if (type == KkakkaType.DONATE) return '기부';
    if (type == KkakkaType.AREA) return '지역';
    if (type == KkakkaType.MEDICAL) return '의료비';
    if (type == KkakkaType.PIN) return '용돈';
    if (type == KkakkaType.SELF) return '교육비';
    if (type == KkakkaType.WELFARE) return '복지';
    return '교육';
  }

  String _getNotIconPath() {
    final type = kkakka.kkakkaStatus;
    if (type == KkakkaStatus.DROP)
      return 'assets/svgs/ic/ic_empire_content.svg';
    if (type == KkakkaStatus.USE_ALL)
      return 'assets/svgs/ic/ic_stop_content.svg';
    if (type == KkakkaStatus.NO_PAYMENT)
      return 'assets/svgs/ic/ic_stop_content.svg';
    return 'assets/svgs/ic/ic_stop_content.svg';
  }

  String _getNotTitle() {
    final type = kkakka.kkakkaStatus;
    if (type == KkakkaStatus.DROP) return '사용 정지';
    if (type == KkakkaStatus.USE_ALL) return '모든 금액 사용';
    if (type == KkakkaStatus.NO_PAYMENT) return '사용 정지';
    return '사용 기간 만료';
  }

  String? numberWithComma(int? param) {
    if (param == null) return null;
    return new NumberFormat('###,###,###,###')
        .format(param)
        .replaceAll(' ', '');
  }
}
