import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/routes/custom_transition.dart';

/*
 작성일 : 2021-02-16
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : SizeTransitions,
 설명 : Get 화면 이동시 Size 가 변환되는 Animation 을 주기 위한 CustomTransition
*/

class SizeTransitions extends CustomTransition {
  @override
  Widget buildTransition(
    BuildContext context,
    Curve? curve,
    Alignment? alignment,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    return Align(
      alignment: Alignment.center,
      child: SizeTransition(
        sizeFactor: CurvedAnimation(
          parent: animation,
          curve: curve ?? Curves.linear,
        ),
        child: child,
      ),
    );
  }
}
