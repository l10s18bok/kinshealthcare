import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/routes/custom_transition.dart';

/*
 작성일 : 2021-02-16
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : SlideUpTransitions,
 설명 : Get 화면 이동시 Slide up Animation 을 주기 위한 CustomTransition
*/

class SlideUpTransitions extends CustomTransition {
  @override
  Widget buildTransition(
    BuildContext context,
    Curve? curve,
    Alignment? alignment,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    var begin = Offset(0.0, 1.0);
    var end = Offset.zero;
    var curve = Curves.ease;

    var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

    return SlideTransition(
      position: animation.drive(tween),
      child: child,
    );
  }
}
