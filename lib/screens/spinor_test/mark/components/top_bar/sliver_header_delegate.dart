import 'dart:math';

import 'package:flutter/cupertino.dart';

/*
 작성일 : 2021-02-16
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : SliverHeaderDelegateCS,
 설명 : 2개의 Child 와 2개의 높이를 지정하면 SliverHeader 가 올라갈때 Header 가 서서히 변화하는 Header Delegate
*/

class SliverHeaderDelegateCS extends SliverPersistentHeaderDelegate {
  SliverHeaderDelegateCS({
    required this.minHeight,
    required this.maxHeight,
    required this.maxChild,
    required this.minChild,
  });
  final double minHeight, maxHeight;
  final Widget maxChild, minChild;

  late double visibleMainHeight, animationVal, width;

  @override
  bool shouldRebuild(SliverHeaderDelegateCS oldDelegate) => true;
  @override
  double get minExtent => minHeight;
  @override
  double get maxExtent => max(maxHeight, minHeight);

  double scrollAnimationValue(double shrinkOffset) {
    double maxScrollAllowed = maxExtent - minExtent;

    return ((maxScrollAllowed - shrinkOffset) / maxScrollAllowed).clamp(0, 1).toDouble();
  }

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    width = MediaQuery.of(context).size.width;
    visibleMainHeight = max(maxExtent - shrinkOffset, minExtent);
    animationVal = scrollAnimationValue(shrinkOffset);

    return Container(
      height: visibleMainHeight,
      width: MediaQuery.of(context).size.width,
      color: Color(0xFFFFFFFF),
      child: Stack(
        children: <Widget>[
          getMinTop(),
          animationVal != 0 ? getMaxTop() : Container(),
        ],
      ),
    );
  }

  Widget getMaxTop() {
    return Positioned(
      bottom: 0.0,
      child: Opacity(
        opacity: animationVal,
        child: SizedBox(
          height: maxHeight,
          width: width,
          child: maxChild,
        ),
      ),
    );
  }

  Widget getMinTop() {
    return Opacity(
      opacity: 1 - animationVal,
      child: Container(height: visibleMainHeight, width: width, child: minChild),
    );
  }
}
