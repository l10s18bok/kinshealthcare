import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/keyboard/keyboard.dart';
/*
 작성일 : 2021-02-25
 작성자 : Mark,
 화면명 : ,
 경로 : ,
 클래스 : MoneyInput,
 설명 : Custom Keyboard 가격 입력을 위해서 JC 작성 코드를 복사해서 수정한 Widget,
*/

class MoneyInput extends StatefulWidget {
  final EdgeInsets bodyPadding;
  final ValueSetter<String?>? onKeyboardInput;
  final GestureTapCallback? onConfirm;
  final TextEditingController? textController;
  final double? height;

  MoneyInput({
    this.bodyPadding = const EdgeInsets.symmetric(horizontal: 16.0),
    this.onKeyboardInput,
    this.textController,
    this.onConfirm,
    this.height,
  });

  @override
  _MoneyInputState createState() => _MoneyInputState();
}

class _MoneyInputState extends State<MoneyInput> {
  late TextEditingController controller;

  @override
  void initState() {
    super.initState();

    controller = widget.textController ?? TextEditingController();

    controller.addListener(() {
      if (widget.onKeyboardInput != null) {
        widget.onKeyboardInput!(controller.text);
      }
    });
  }

  onNumberTap(String number) {
    controller.text = controller.text + number;
  }

  onBackspaceTap() {
    if (controller.text.length < 2) {
      controller.text = '';
    } else {
      controller.text = controller.text.substring(0, controller.text.length - 1);
    }
  }

  onAddNumberTap(int amount) {
    int curNumber = 0;

    if (controller.text.length > 0) {
      curNumber = int.parse(controller.text);
    }

    controller.text = (curNumber + amount).toString();
  }

  onSpecificAmountTap(int amount) {
    controller.text = amount.toString();
  }

  renderHeaderChip({
    required String label,
    required GestureTapCallback onTap,
  }) {
    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey[300]!,
              width: 1.0,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: 4.0,
            ),
            child: Center(
              child: Text(
                label,
              ),
            ),
          ),
        ),
      ),
    );
  }

  renderHeaders() {
    return Container(
      color: Colors.grey[200],
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 8.0,
          horizontal: 16.0,
        ),
        child: Row(
          children: [
            Expanded(
              child: renderHeaderChip(
                label: '+1만',
                onTap: () {
                  onAddNumberTap(10000);
                },
              ),
            ),
            Container(width: 8.0),
            Expanded(
              child: renderHeaderChip(
                label: '+10만',
                onTap: () {
                  onAddNumberTap(100000);
                },
              ),
            ),
            Container(width: 8.0),
            Expanded(
              child: renderHeaderChip(
                label: '+100만',
                onTap: () {
                  onAddNumberTap(1000000);
                },
              ),
            ),
            Container(width: 8.0),
            Expanded(
              child: renderHeaderChip(
                label: '+1000만',
                onTap: () {
                  onAddNumberTap(10000000);
                },
              ),
            ),
            Container(width: 8.0),
            Expanded(
              child: renderHeaderChip(
                label: '전액',
                onTap: () {
                  onSpecificAmountTap(999999999);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  renderConfirm() {
    return PrimaryButton(
      label: '확인',
      onTap: widget.onConfirm,
    );
  }

  renderBody() {
    return Padding(
      padding: widget.bodyPadding,
      child: Column(
        children: [
          Expanded(
            child: NumberInputKeyboard(
              onTap: onNumberTap,
              onBackspace: onBackspaceTap,
            ),
          ),
          renderConfirm(),
          SizedBox(height: 16),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      child: Column(
        children: [
          renderHeaders(),
          Expanded(
            child: renderBody(),
          ),
        ],
      ),
    );
  }
}
