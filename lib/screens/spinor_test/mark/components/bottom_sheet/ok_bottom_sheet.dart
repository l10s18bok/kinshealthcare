import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

import '../../../../../utils/resource.dart';

/*
 작성일 : 2021-02-09
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : OkBottomSheet,
 설명 : Title 과 버튼이 1개인 기본적인 형태의 Bottom Sheet
*/

// ignore: must_be_immutable
class OkBottomSheet extends StatelessWidget {
  OkBottomSheet({
    required this.title,
    required this.content,
  });

  final String title;
  final String content;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      width: double.infinity,
      color: Colors.transparent,
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(26.0),
          topRight: Radius.circular(26.0),
        ),
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      titleText(context, title),
                      SizedBox(height: 10),
                      titleText(context, content),
                    ],
                  ),
                ),
              ),
              PrimaryButton(
                label: '확인',
                onTap: () => Get.back(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget titleText(BuildContext context, String data) {
    return Text(
      data,
      style: TextStyle(
        color: kinsBlack,
        fontSize: ThemeFactory.of(context).theme.fontSize17,
      ),
    );
  }
}
