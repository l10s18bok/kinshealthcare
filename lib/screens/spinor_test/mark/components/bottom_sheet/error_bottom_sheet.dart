import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import '../../../../../utils/resource.dart';

/*
 작성일 : 2021-02-23
 작성자 : Mark,
 화면명 : (화면명),
 경로 : 없음
 클래스 : ErrorBottomSheet,
 설명 : Error 을 표시하기 위한 Bottom Sheet
*/

// ignore: must_be_immutable
class ErrorBottomSheet extends StatelessWidget {
  ErrorBottomSheet({
    required this.title,
    required this.content,
  });

  final String title;
  final String content;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      width: double.infinity,
      color: Colors.transparent,
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(26.0),
          topRight: Radius.circular(26.0),
        ),
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              titleText(context, title),
              SizedBox(height: 10),
              Expanded(child: scrollMain(context)),
              PrimaryButton(label: '확인', onTap: () => Get.back()),
            ],
          ),
        ),
      ),
    );
  }

  Widget scrollMain(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          titleText(context, content),
        ],
      ),
    );
  }

  Widget titleText(BuildContext context, String data) {
    return Text(
      data,
      style: TextStyle(
        color: kinsBlack,
        fontSize: ThemeFactory.of(context).theme.fontSize17,
      ),
    );
  }
}
