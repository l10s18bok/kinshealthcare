import 'package:flutter/material.dart';

class ToggleBankButton extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ToggleBankButtonState();
}

class _ToggleBankButtonState extends State<ToggleBankButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(10),
      width: 100,
      height: 90,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.black,
          width: 3,
        ),
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: Colors.green,
      ),
      child: Column(
        children: [
          Expanded(
            flex: 6,
            child: Text('1'),
          ),
          Expanded(
            flex: 4,
            child: Text('2'),
          ),
        ],
      ),
    );
  }
}
