import 'package:flutter/material.dart';
/*
 * 작성일 : 2021-02-05
 * 작성자 : Victor
 * 주요기능 : 숫자로 페이지 표기
 */

///원형 인디케이터 같은 느낌으로 페이지 표시하기
///페이지 개수만큼의 원을 출력하되 인덱스에 해당하는 페이지는 강조되어 표시된다. (페이지 숫자도 출력함)
class CirclePage extends StatefulWidget {
  ///총 페이지 개수
  final int pageCount;

  ///현재 페이지 인덱스 (2/5 기준으로 필요없다고 생각해서 final로 선언함. 필요하면 변경할 것)
  final int curPage;

  ///일반 원 색상
  final Color circleColor;

  ///인덱스에 해당하는 원 색상
  final Color selectedCircleColor;

  CirclePage({
    this.pageCount = 1,
    this.curPage = 0,
    this.circleColor = Colors.grey,
    this.selectedCircleColor = Colors.blue,
  });

  @override
  State<StatefulWidget> createState() => _CirclePage();
}

class _CirclePage extends State<CirclePage> {
  final double bigCircleSize = 21 * 1.2;
  final double smallCircleSize = 10 * 1.2;
  final double space = 10 * 1.2;

  Widget? circleText(String? text) {
    if (text == null || text == '') {
      return null;
    } else {
      return Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      );
    }
  }

  Widget circle({Color color = Colors.grey, String? text, double size = 10}) {
    return Container(
      width: size,
      height: size,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        color: color,
      ),
      child: circleText(text),
    );
  }

  List<Widget> childWidget() {
    var _index = widget.curPage < 0
        ? 0
        : widget.curPage >= widget.pageCount
            ? widget.pageCount
            : widget.curPage;

    var result = [] as List<Widget>;

    for (int i = 0; i < widget.pageCount; i++) {
      if (i == _index) {
        result.add(circle(
            color: widget.selectedCircleColor, text: '${i + 1}', size: bigCircleSize));
      } else {
        result.add(circle(color: widget.circleColor, size: smallCircleSize));
      }

      if (i != widget.pageCount - 1) {
        result.add(Container(width: space, height: space));
      }
    }

    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: childWidget(),
    );
  }
}
