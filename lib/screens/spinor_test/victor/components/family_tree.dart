// First Created : 2021/1/27
// Author : Jinsu Gu (paser2@gmail.com)
// TODO : 크기 제한, 인터랙션 추가

import 'package:flutter/material.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/data_model/custom_class.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/data_model/enums.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/data_model/person_info.dart';

//패밀리 트리 위젯
//
//현재 최소한의 구현만 해놓았으므로 필요하면 그때 확장해야 됨
class FamilyTree extends StatelessWidget {
  final List<PersonInfo?> personInfoList;

  final double widgetWidth;
  final double widgetHeight;

  final double itemWidth;
  final double itemHeight;
  final double intervalWidth;
  final double intervalHeight;
  final double lineStroke;

  final Alignment align;
  final EdgeInsets minimum;

  final Offset? center;

  FamilyTree(
    this.personInfoList, {
    this.itemWidth = 100,
    this.itemHeight = 100,
    this.intervalWidth = 100,
    this.intervalHeight = 100,
    this.lineStroke = 4,
    this.align = Alignment.center,
    this.minimum = const EdgeInsets.all(10.0),
    this.widgetWidth = 0,
    this.widgetHeight = 0,
    this.center,
  });

  _PaintInfo _calculateItemInfo(Offset _center) {
    PersonInfo? myself = personInfoList.firstWhere((item) => item!.isCenter, orElse: () {
      return null;
    });

    if (myself == null) {
      return _PaintInfo();
    } else {
      return _calculate(_center, myself);
    }
  }

  _PaintInfo _calculate(Offset _center, PersonInfo _startInfo) {
    var result = _PaintInfo();

    var pointInfoList = []; //내부 교차점 저장용
    var queue = UniqueQueue<PersonInfo>(); //처리용

    var wi = (intervalWidth + itemWidth) / 2;
    var hi = (intervalHeight + itemHeight) / 2;

    queue.push(_startInfo);

    while (queue.isNotEmpty) {
      var personInfo = queue.pop();
      var pointInfo = _PointInfo(personInfo!);

      //중점 구하기, 깊이 계산
      var itemCenter = _center;

      for (var item in pointInfoList) {
        if (item.info.isRelated(personInfo)) {
          var relation = item.info.getRelation(personInfo);
          var category = _convert(relation);
          var base = item.pointList.firstWhere((x) => x.first == category, orElse: () {
            return null;
          });

          if (base != null) {
            switch (category) {
              case _RelationCategory.parent:
                pointInfo.depth = item.depth - 1;
                itemCenter = _getCenter(personInfo, category, base.second, wi, hi);
                break;
              case _RelationCategory.partner:
                pointInfo.depth = item.depth;
                itemCenter = _getCenter(personInfo, category, base.second, wi, hi);
                break;
              case _RelationCategory.child:
                pointInfo.depth = item.depth + 1;
                var cur = pointInfoList
                    .where((a) => a.depth == pointInfo.depth)
                    .length
                    .toDouble();
                var center = item.info.childCount / 2 + 0.5;
                itemCenter = _getCenter(personInfo, category, base.second, wi, hi,
                    cur: cur, middle: center);
                break;
              default:
            }
          }

          break;
        }
      }

      //아이템 정보 저장
      var rect =
          Rect.fromCenter(center: itemCenter, width: itemWidth, height: itemHeight);
      result.add(itemInfo: _ItemInfo(personInfo, rect));

      var cs = itemCenter; //child start point (partner end point)

      //파트너 선 계산, 점 저장
      if (personInfo.hasRelation(Relation.partner)) {
        var diff = personInfo.gender == Gender.male ? intervalWidth : -intervalWidth;

        cs = Offset(itemCenter.dx + diff, itemCenter.dy);

        if (cs != itemCenter) {
          pointInfo.add(_RelationCategory.partner, cs);
          result.add(lineInfo: _LineInfo(itemCenter, cs));
        }
      }

      //자식 선 계산, 점 저장
      if (personInfo.hasRelation(Relation.son, Relation.daughter)) {
        var ce = Offset(cs.dx, cs.dy + hi);
        pointInfo.add(_RelationCategory.child, ce);
        result.add(lineInfo: _LineInfo(cs, ce));
      }

      //부모 선 계산, 점 저장
      if (personInfo.hasRelation(Relation.father, Relation.mother)) {
        var p = Offset(itemCenter.dx, itemCenter.dy - hi);
        pointInfo.add(_RelationCategory.parent, p);
        result.add(lineInfo: _LineInfo(itemCenter, p));

        var parentInfo = personInfo.getPersonInfo(Relation.father, Relation.mother);
        if (parentInfo != null && pointInfoList.length > 0) {
          var parentPointInfo =
              pointInfoList.firstWhere((item) => item.info == parentInfo);
          if (parentPointInfo != null) {
            var pcp = parentPointInfo.find(_RelationCategory.child).second;
            if (pcp != null) {
              result.add(lineInfo: _LineInfo(pcp, p));
            }
          }
        }
      }

      //관련자 큐에 추가
      for (var i in personInfo.relationList) {
        if (i == null) continue;
        if (!pointInfoList.any((a) => a.info == i.second)) {
          if (i.first == Relation.partner) {
            queue.addFirst(i.second);
          } else {
            queue.push(i.second);
          }
        }
      }

      //점 정보 추가
      pointInfoList.add(pointInfo);
    }

    return result;
  }

  _RelationCategory _convert(Relation relation) {
    switch (relation) {
      case Relation.father:
      case Relation.mother:
        return _RelationCategory.parent;
      case Relation.partner:
        return _RelationCategory.partner;
      case Relation.son:
      case Relation.daughter:
        return _RelationCategory.child;
      default:
        return _RelationCategory.none;
    }
  }

  Offset _getCenter(PersonInfo _baseInfo, _RelationCategory _category, Offset _base,
      double _intervalX, double _intervalY,
      {double? cur, double? middle}) {
    var x = 0.0;
    var y = 0.0;
    switch (_category) {
      case _RelationCategory.parent:
        x = _baseInfo.hasRelation(Relation.partner) ? _intervalX : 0;
        if (_baseInfo.gender == Gender.male) x = -x;
        y = -_intervalY;
        break;
      case _RelationCategory.partner:
        x = _baseInfo.gender == Gender.male ? -_intervalX : _intervalX;
        break;
      case _RelationCategory.child:
        if (cur == null || middle == null) return _base;

        x = (cur - middle + 1) * (_intervalX + itemWidth);
        y = _intervalY;
        break;
      default:
        return _base;
    }

    return Offset(_base.dx + x, _base.dy + y);
  }

  EdgeInsets _getEdge(
      Offset _center, List<_ItemInfo?> _itemInfoList, double _width, double _height,
      {EdgeInsets margin = const EdgeInsets.all(20)}) {
    var rect = Rect.fromCenter(center: _center, width: itemWidth, height: itemHeight);

    var t = rect.top;
    var b = rect.bottom;
    var l = rect.left;
    var r = rect.right;

    for (var item in _itemInfoList) {
      item = item as _ItemInfo;
      t = t > item.rect.top ? item.rect.top : t;
      b = b < item.rect.bottom ? item.rect.bottom : b;
      l = l > item.rect.left ? item.rect.left : l;
      r = r < item.rect.right ? item.rect.right : r;
    }

    t = _center.dy - margin.top < t ? t - _center.dx + margin.top : 0;
    b = (_height - _center.dy - margin.bottom) < b ? b - _height + margin.bottom : 0;
    l = _center.dx - margin.left < l ? l - _center.dx + margin.left : 0;
    r = (_width - _center.dx - margin.right) < r ? r - _width + margin.right : 0;

    return EdgeInsets.fromLTRB(l, t, r, b);
  }

  @override
  Widget build(BuildContext context) {
    var _center = center;
    var _padding = MediaQuery.of(context).padding;

    double _width = widgetWidth;
    double _height = widgetHeight;

    if (_center == null) {
      _width = _width == 0
          ? MediaQuery.of(context).size.width - _padding.left - _padding.right
          : _width;
      _height = _height == 0
          ? MediaQuery.of(context).size.height - _padding.top - _padding.bottom
          : _height;

      _center = Offset(_width / 2, _height / 2);
    }

    var paintInfo = _calculateItemInfo(_center);

    var tree = _FamilyTreePainter(paintInfo: paintInfo);

    var _t = _getEdge(_center, paintInfo.itemInfoList, _width, _height);
    print(_t.toString());

    return Scaffold(
        body: Align(
      alignment: align,
      child: InteractiveViewer(
        alignPanAxis: true,
        boundaryMargin: _t,
        minScale: 0.5,
        maxScale: 2.0,
        child: CustomMultiChildLayout(
            delegate: _MultiChildLayoutDelegate(
              idList: tree.getIdList(),
            ),
            children: tree.getTree()),
      ),
    ));
  }
}

//내부 클래스

enum _RelationCategory {
  parent,
  partner,
  child,
  none,
}

class _PointInfo {
  PersonInfo info;
  int _depth = 0;
  List<Pair<_RelationCategory, Offset>?> pointList = [];

  _PointInfo(
    this.info, {
    _RelationCategory? relation,
    Offset? offset,
  }) {
    if (relation != null && offset != null) {
      pointList.add(Pair(relation, offset));
    }
  }

  set depth(int value) => this._depth = value;
  int get depth => _depth;

  void add(_RelationCategory relation, Offset offset) =>
      pointList.add(Pair(relation, offset));

  Pair<_RelationCategory, Offset>? find(_RelationCategory relation) =>
      pointList.firstWhere((item) => item!.first == relation, orElse: () {
        return null;
      });
}

class _LineInfo {
  Offset startPoint;
  Offset endPoint;

  _LineInfo(this.startPoint, this.endPoint);
}

class _ItemInfo {
  Rect rect;
  PersonInfo info;

  _ItemInfo(this.info, this.rect);

  Offset get center => rect.center;

  @override
  String toString() {
    return '${info.name} : ${rect.toString()}';
  }
}

class _PaintInfo {
  List<_ItemInfo?> itemInfoList;
  List<_LineInfo?> lineInfoList;

  _PaintInfo()
      : itemInfoList = [],
        lineInfoList = [];

  void add({_ItemInfo? itemInfo, _LineInfo? lineInfo}) {
    if (itemInfo != null) {
      itemInfoList.add(itemInfo);
    }

    if (lineInfo != null) {
      lineInfoList.add(lineInfo);
    }
  }

  _ItemInfo? find(PersonInfo info) =>
      itemInfoList.firstWhere((item) => item!.info == info, orElse: () {
        return null;
      });
}

class _MultiChildLayoutDelegate extends MultiChildLayoutDelegate {
  final Offset? position;
  final List<Pair<String, Object>> idList;

  _MultiChildLayoutDelegate({this.position, this.idList = const []});
  @override
  void performLayout(Size size) {
    idList.asMap().forEach((index, item) {
      var id = item.first;
      var info = item.second;
      if (hasChild(id)) {
        if (info is Offset) {
          //선
          layoutChild(id, BoxConstraints.tightFor());
          positionChild(id, Offset.zero);
        } else if (info is Rect) {
          //아이템
          layoutChild(
              id, BoxConstraints.tightFor(width: info.width, height: info.height));
          positionChild(id, info.topLeft);
        }
      }
    });
  }

  @override
  bool shouldRelayout(covariant _MultiChildLayoutDelegate oldDelegate) {
    return oldDelegate.position != position;
  }
}

class _FamilyTreePainter {
  final _PaintInfo paintInfo;

  _FamilyTreePainter({required this.paintInfo});

  List<Pair<String, Object>> getIdList() {
    List<Pair<String, Object>> result = [];

    paintInfo.lineInfoList.asMap().forEach((index, item) {
      result.add(Pair<String, Offset>('line$index', item!.startPoint));
    });

    paintInfo.itemInfoList.asMap().forEach((index, item) {
      item = item as _ItemInfo;
      var base = item.rect;

      result.add(Pair<String, Rect>('${item.info.name}_back$index', base));
      result.add(
          Pair<String, Rect>('${item.info.name}_text$index', base.shift(Offset(0, -10))));
      result.add(Pair<String, Rect>('${item.info.name}_border$index', base));
    });

    return result;
  }

  List<Widget> getTree() {
    List<Widget> result = [];

    paintInfo.lineInfoList.asMap().forEach((index, item) {
      result.add(
        LayoutId(
          id: 'line$index',
          child: Center(
            child: CustomPaint(
              painter: _LinePainter(item!.startPoint, item.endPoint),
            ),
          ),
        ),
      );
    });

    paintInfo.itemInfoList.asMap().forEach((index, item) {
      result.add(
        LayoutId(
          id: '${item!.info.name}_back$index',
          child: Center(
            child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  color: Colors.white,
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Image.asset(item.info.imagePath),
                )),
          ),
        ),
      );

      result.add(
        LayoutId(
          id: '${item.info.name}_text$index',
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Stack(
              children: [
                Text(
                  item.info.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    foreground: Paint()
                      ..style = PaintingStyle.stroke
                      ..strokeWidth = 6
                      ..color = Colors.black,
                  ),
                ),
                Text(
                  item.info.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ),
          ),
        ),
      );

      result.add(
        LayoutId(
          id: '${item.info.name}_border$index',
          child: Center(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                border:
                    Border.all(color: Colors.black, width: 4, style: BorderStyle.solid),
              ),
            ),
          ),
        ),
      );
    });

    return result;
  }
}

class _LinePainter extends CustomPainter {
  final Offset? point1;
  final Offset? point2;
  final Color lineColor;
  final double lineStroke;

  _LinePainter(
    this.point1,
    this.point2, {
    this.lineColor = Colors.black,
    this.lineStroke = 4,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final point1 = this.point1;
    final point2 = this.point2;
    if (point1 != null && point2 != null) {
      final linePaint = Paint()
        ..color = lineColor
        ..strokeWidth = lineStroke;

      var sp = getStartPoint(point1, point2);
      var ep = sp == point1 ? point2 : point1;

      if (point1 != point2) {
        double dx = point1.dy == point2.dy ? lineStroke / 2 : 0;
        double dy = point1.dx == point2.dx ? lineStroke / 2 : 0;

        sp = sp.translate(-dx, -dy);
        ep = ep.translate(dx, dy);
      }

      canvas.drawLine(sp, ep, linePaint);
    }
  }

  Offset getStartPoint(Offset p1, Offset p2) {
    if (p1.dx < p2.dx) {
      return p1;
    } else if (p1.dx > p2.dx) {
      return p2;
    } else {
      return p1.dy < p2.dy ? p1 : p2;
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
