// First Created : 2021/1/29
// Author : Jinsu Gu (paser2@gmail.com)

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/data_model/child_widget_info.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/layouts/interactive_scroll.dart';

List<Widget> _widgettest = [
  Text('111111'),
  Text('222222'),
  Text('111111'),
  Text('222222'),
  Text('111111'),
  Text('222222'),
  Text('111111'),
  Text('222222'),
  Text('111111'),
  Text('222222'),
  Text('111111'),
  Text('222222'),
  Text('111111'),
  Text('222222'),
  Text('111111'),
  Text('111111'),
  Text('222222'),
  Text('111111'),
  Text('222222'),
  Text('111111'),
];

class InteractiveScrollWidgetTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<ChildWidgetInfo> _widget = [];
    var random = new Random();

    var footer = Text('hello');

    Widget getHeader(VoidCallback? func) {
      return Container(
        alignment: Alignment.centerLeft,
        child: BackButton(
          color: Colors.white,
          onPressed: func,
        ),
      );
    }

    for (var item in _widgettest) {
      var height = 30 + random.nextDouble() * 70;

      _widget.add(ChildWidgetInfo(
          widget: Container(
            child: item,
            width: 500,
            height: height,
            color: Color.fromARGB(
                255, random.nextInt(100), random.nextInt(100), random.nextInt(100)),
          ),
          height: height));
    }

    return MaterialApp(
      home: InteractiveScrollWidget(
        floatingHeaderWidget: ChildWidgetInfo(
            widget: getHeader(() {
              Navigator.pop(context);
            }),
            height: 50,
            color: Colors.purple),
        pinnedHeaderWidget: ChildWidgetInfo(
            widget: Text('pinned header'), height: 50, color: Colors.greenAccent),
        bodyWidgetList: _widget,
        footerWidget: ChildWidgetInfo(widget: footer, height: 100, color: Colors.grey),
      ),
    );
  }
}
