// First Created : 2021/1/29
// Author : Jinsu Gu (paser2@gmail.com)

import 'package:flutter/material.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/components/family_tree.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/data_model/enums.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/data_model/person_info.dart';

class FamilyTreeTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    PersonInfo info1 = PersonInfo(gender: Gender.male, name: "me", imagePath: 'lib/screens/spinor_test/victor/sample_image/2.png', isCenter: true); //me
    PersonInfo info2 = PersonInfo(gender: Gender.male, name: "father"); //father
    PersonInfo info3 = PersonInfo(gender: Gender.female, name: "mother"); //mother
    PersonInfo info4 = PersonInfo(gender: Gender.male, name: "son"); //son

    PersonInfo info5 = PersonInfo(gender: Gender.female, name: "darling", imagePath: 'lib/screens/spinor_test/victor/sample_image/1.png');
    PersonInfo info6 = PersonInfo(gender: Gender.female, name: "daguhter", imagePath: 'lib/screens/spinor_test/victor/sample_image/3.png');
    PersonInfo info7 = PersonInfo(gender: Gender.female, name: "daguhter");

    PersonInfo infoT1 = PersonInfo(gender: Gender.female, name: "test1");
    PersonInfo infoT2 = PersonInfo(gender: Gender.female, name: "test2");

    info1
        .addRelationPair(Relation.father, info2)
        .addRelationPair(Relation.mother, info3)
        .addRelationPair(Relation.son, info4)
        .addRelationPair(Relation.partner, info5)
        .addRelationPair(Relation.daughter, info6)
        .addRelationPair(Relation.daughter, info7);
    info2.addRelationPair(Relation.partner, info3);

    info4.addRelationPair(Relation.daughter, infoT1);
    infoT1.addRelationPair(Relation.daughter, infoT2);

    var personInfoList = [info1, info2, info3, info4];

    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: FamilyTree(
        personInfoList,
      ),
    );
  }
}
