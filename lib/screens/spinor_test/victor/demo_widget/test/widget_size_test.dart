import 'dart:math';

import 'package:flutter/material.dart';
import 'package:kins_healthcare/utils/widget_utils.dart';

/*
 작성일 : 2021-02-24
 작성자 : Victor
 화면명 : 
 경로 : 
 클래스 : WidgetSizeTest
 설명 : 위젯 크기 실험소
*/

class WidgetSizeTest extends StatefulWidget {
  @override
  _WidgetSizeTestState createState() => _WidgetSizeTestState();
}

class _WidgetSizeTestState extends State<WidgetSizeTest> {
  String outText = '선택한 위젯의 크기가 표시됩니다';

  Widget? fixed;
  Widget? compileFixed;

  var targetWidth = 100.0;
  var targetHeight = 50.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final r = Random();
    final width = (r.nextInt(100) + 50.0);
    final height = (r.nextInt(50) + 20.0);

    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              height: 100,
              child: Text(outText),
            ),
            space(),
            _FixedWidget(getSize1),
            space(),
            _UnfixedWidget(width, height, getSize1),
            space(),
            _ChangingWidget(targetWidth, targetHeight, getSize1),
            space(),
            randomButton(),
          ],
        ),
      ),
    );
  }

  Widget space() {
    return Container(
      width: 10,
      height: 10,
      color: Colors.transparent,
    );
  }

  Widget randomButton() {
    return InkWell(
      onTap: () {
        Random r = Random();
        setState(() {
          targetWidth = 50.0 + r.nextInt(100);
          targetHeight = 25.0 + r.nextInt(100);
        });
        print('clicked');
      },
      child: Container(
        width: 200,
        height: 50,
        color: Colors.grey,
        alignment: Alignment.center,
        child: Text('클릭하여 위젯 크기 임의로 변경'),
      ),
    );
  }

  void getSize1(BuildContext context, Widget widget) {
    //렌더링 끝난 다음에만 측정할 수 있음
    var size = WidgetUtils.getWidgetSize(context, widget);
    setState(() {
      outText = '$size';
    });
  }
}

class _FixedWidget extends StatefulWidget {
  final Function(BuildContext context, Widget widget) onClick;

  _FixedWidget(this.onClick);

  @override
  State<StatefulWidget> createState() => _FixedWidgetState();
}

class _FixedWidgetState extends State<_FixedWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.onClick(context, widget);
      },
      child: Container(
        width: 100,
        height: 45,
        color: Colors.yellow,
      ),
    );
  }
}

class _UnfixedWidget extends StatefulWidget {
  final double width;
  final double height;
  final Function(BuildContext context, Widget widget) onClick;

  _UnfixedWidget(this.width, this.height, this.onClick);

  @override
  State<StatefulWidget> createState() => _UnfixedWidgetState();
}

class _UnfixedWidgetState extends State<_UnfixedWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.onClick(context, widget);
      },
      child: Container(
        width: widget.width,
        height: widget.height,
        color: Colors.blue,
      ),
    );
  }
}

class _ChangingWidget extends StatelessWidget {
  final double width;
  final double height;
  final Function(BuildContext context, Widget widget) onClick;

  _ChangingWidget(this.width, this.height, this.onClick);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onClick(context, this);
      },
      child: Container(
        width: width,
        height: height,
        color: Colors.red,
      ),
    );
  }
}
