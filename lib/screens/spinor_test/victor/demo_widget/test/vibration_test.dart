import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/*
 작성일 : 2021-02-19
 작성자 : Victor
 화면명 : 
 경로 : 
 클래스 : VibrationTest
 설명 : 진동 테스트
*/
typedef TapHandler = void Function();

enum HapticLevel {
  none,
  light,
  middle,
  heavy,
}

class VibrationTest extends StatefulWidget {
  @override
  _VibrationTestState createState() => _VibrationTestState();
}

class _VibrationTestState extends State<VibrationTest> {
  HapticLevel _hapticLevel = HapticLevel.none;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(16),
          child: buttonPad(),
        ),
      ),
    );
  }

  Widget buttonPad() {
    return Flex(
      direction: Axis.vertical,
      children: [
        Flex(
          direction: Axis.horizontal,
          children: [
            Flexible(
                child: button('1', onTapHandler: () {
              doHaptic();
            })),
            Flexible(
                child: button('2', onTapHandler: () {
              doHaptic();
            })),
            Flexible(
                child: button('3', onTapHandler: () {
              doHaptic();
            })),
          ],
        ),
        Flex(
          direction: Axis.horizontal,
          children: [
            Flexible(
                child: button('4', onTapHandler: () {
              doHaptic();
            })),
            Flexible(
                child: button('5', onTapHandler: () {
              doHaptic();
            })),
            Flexible(
                child: button('6', onTapHandler: () {
              doHaptic();
            })),
          ],
        ),
        Flex(
          direction: Axis.horizontal,
          children: [
            Flexible(
                child: button('7', onTapHandler: () {
              doHaptic();
            })),
            Flexible(
                child: button('8', onTapHandler: () {
              doHaptic();
            })),
            Flexible(
                child: button('9', onTapHandler: () {
              doHaptic();
            })),
          ],
        ),
        space(15),
        Flex(
          direction: Axis.horizontal,
          children: [
            Flexible(
                child: button('진동없음', onTapHandler: () {
              setHaptic(HapticLevel.none);
            })),
            Flexible(
                child: button('약한진동', onTapHandler: () {
              setHaptic(HapticLevel.light);
            })),
            Flexible(
                child: button('보통진동', onTapHandler: () {
              setHaptic(HapticLevel.middle);
            })),
            Flexible(
                child: button('강한진동', onTapHandler: () {
              setHaptic(HapticLevel.heavy);
            })),
          ],
        ),
        space(15),
        Flex(
          direction: Axis.horizontal,
          children: [
            Flexible(
                child: button('selectionClick', onTapHandler: () {
              HapticFeedback.selectionClick();
            })),
            Flexible(
                child: button('vibrate', onTapHandler: () {
              HapticFeedback.vibrate();
            })),
          ],
        ),
      ],
    );
  }

  Widget button(String text, {TapHandler? onTapHandler}) {
    return InkWell(
      onTap: onTapHandler,
      child: Container(
        alignment: Alignment.center,
        height: 50,
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.black,
            width: 1.5,
          ),
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
        ),
        child: Text(text),
      ),
    );
  }

  Widget space(double size) {
    return Container(
      width: size,
      height: size,
      color: Colors.transparent,
    );
  }

  void setHaptic(HapticLevel level) {
    _hapticLevel = level;
    doHaptic();
  }

  void doHaptic() {
    switch (_hapticLevel) {
      case HapticLevel.light:
        HapticFeedback.lightImpact();
        break;
      case HapticLevel.middle:
        HapticFeedback.mediumImpact();
        break;
      case HapticLevel.heavy:
        HapticFeedback.heavyImpact();
        break;
      default:
    }
  }
}
