/*
 * 작성일 : 2021-02-05
 * 작성자 : Victor
 * 화면명 : HP_3004
 * 주요기능 : 은행 선택
 */

import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/layouts/title_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/components/circle_page.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/components/toggle_bank_button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class BankSelect extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BankSelectState();
}

class _BankSelectState extends State<BankSelect> {
  Widget circlePage() {
    var theme = ThemeFactory.of(context).theme;

    return CirclePage(
      curPage: 0,
      pageCount: 3,
      circleColor: theme.primaryGreyBgColor,
      selectedCircleColor: theme.primaryBlueColor,
    );
  }

  void BankListBody() {}

  void BankIcon() {}

  renderNextButton() {
    return PrimaryButton(
      label: '다음',
      onTap: () {},
    );
  }

  @override
  Widget build(BuildContext context) {
    return TitleLayout(
      title: '계좌등록',
      customActionWidget: circlePage(),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(children: [
          ToggleBankButton(),
          renderNextButton(),
        ]),
      ),
    );
  }  
}