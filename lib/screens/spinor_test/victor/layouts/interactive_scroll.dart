import 'package:flutter/material.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/data_model/child_widget_info.dart';

///헤더 및 푸터가 내부 스크롤에 맞춰서 사라지거나 나타나게 하는 예제
///
///현제 제약
///헤더는 SliverPersistentHeader 사용함
///헤더 개수 고정
///스크롤 방향과 사라지는 조건 고정됨
///스크롤 배경 구현 안함
class InteractiveScrollWidget extends StatefulWidget {
  ///첫번째 헤더 위젯 (스크롤에 따라 변경됨)
  final ChildWidgetInfo? floatingHeaderWidget;

  ///두번째 헤더 위젯 (고정됨)
  final ChildWidgetInfo? pinnedHeaderWidget;

  ///본문에 표시할 위젯 목록
  final List<ChildWidgetInfo> bodyWidgetList;

  ///하단 푸터 위젯 (스크롤에 따라 변경됨)
  final ChildWidgetInfo footerWidget;

  ///하단 푸터 스크롤 속도
  final double bottomSpeed;

  InteractiveScrollWidget({
    required this.footerWidget,
    this.floatingHeaderWidget,
    this.pinnedHeaderWidget,
    this.bodyWidgetList = const [],
    this.bottomSpeed = 1,
  });

  @override
  State<StatefulWidget> createState() => _InteractiveScrollWidgetState();
}

class _InteractiveScrollWidgetState extends State<InteractiveScrollWidget> {
  double _bottomPadding = 0;
  late double _prevY;

  bool getScrollNotificationFunc(ScrollNotification scrollNotification) {
    if (scrollNotification is ScrollStartNotification) {
      setState(() {
        _prevY = scrollNotification.dragDetails!.globalPosition.dy;
      });
    } else if (scrollNotification is ScrollUpdateNotification) {
      setState(() {
        if (scrollNotification.dragDetails != null &&
            scrollNotification.dragDetails?.globalPosition != null) {
          _bottomPadding += (scrollNotification.dragDetails!.globalPosition.dy - _prevY) *
              widget.bottomSpeed;
          _bottomPadding = _bottomPadding > 0.0 ? 0.0 : _bottomPadding;
          _bottomPadding = _bottomPadding < -widget.footerWidget.height
              ? -widget.footerWidget.height
              : _bottomPadding;

          _prevY = scrollNotification.dragDetails!.globalPosition.dy;
        }
      });
    }
    return true;
  }

  NotificationListener<ScrollNotification> getListener(double bodyWidth) {
    List<Widget> _widgets = [];

    final floatingHeaderWidget = widget.floatingHeaderWidget;
    if (floatingHeaderWidget != null) {
      _widgets.add(SliverPersistentHeader(
        pinned: false,
        floating: true,
        delegate: _HeaderDelegate(
          floatingHeaderWidget.widget,
          height: floatingHeaderWidget.height,
          backColor: floatingHeaderWidget.color,
        ),
      ));
    }

    final pinnedHeaderWidget = widget.pinnedHeaderWidget;
    if (pinnedHeaderWidget != null) {
      _widgets.add(SliverPersistentHeader(
        pinned: true,
        floating: false,
        delegate: _HeaderDelegate(
          pinnedHeaderWidget.widget,
          height: pinnedHeaderWidget.height,
          backColor: pinnedHeaderWidget.color,
        ),
      ));
    }

    if (widget.bodyWidgetList.length > 0) {
      _widgets.add(SliverList(
          delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          return Container(
            alignment: Alignment.center,
            width: bodyWidth,
            height: widget.bodyWidgetList[index].height,
            child: widget.bodyWidgetList[index].widget,
          );
        },
        childCount: widget.bodyWidgetList.length,
      )));
    }

    return NotificationListener<ScrollNotification>(
      onNotification: getScrollNotificationFunc,
      child: CustomScrollView(
        slivers: _widgets,
      ),
    );
  }

  Widget getFooter(
      double _width, double _height, Color _backColor, Widget? _childWidget) {
    if (_childWidget == null) {
      return Container(
        width: 0,
        height: 0,
      );
    } else {
      return _Footer(
        widget: Container(
          alignment: Alignment.center,
          width: _width,
          height: _height,
          color: _backColor,
          child: _childWidget,
        ),
        width: _width,
        height: _height,
        bottomPadding: _bottomPadding,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        child: Stack(children: <Widget>[
          Builder(
            builder: (BuildContext contect) {
              return getListener(size.width);
            },
          ),
          getFooter(size.width, widget.footerWidget.height, widget.footerWidget.color,
              widget.footerWidget.widget),
        ]),
      ),
    );
  }
}

class _HeaderDelegate extends SliverPersistentHeaderDelegate {
  final EdgeInsets padding;
  final Widget widget;
  final double height;
  final double extend;
  final Color backColor;

  _HeaderDelegate(this.widget,
      {this.padding = const EdgeInsets.all(10),
      this.height = 50,
      this.extend = 0,
      this.backColor = Colors.yellow});

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: backColor,
      padding: padding,
      child: Container(
        height: height,
        child: widget,
      ),
    );
  }

  @override
  double get maxExtent => padding.top + padding.bottom + height + extend;

  @override
  double get minExtent => padding.top + padding.bottom + height;

  @override
  bool shouldRebuild(covariant _HeaderDelegate oldDelegate) {
    return padding != oldDelegate.padding ||
        widget != oldDelegate.widget ||
        height != oldDelegate.height;
  }
}

class _Footer extends StatefulWidget {
  final Widget? widget;
  final double width;
  final double height;
  final double bottomPadding;

  _Footer({
    this.widget,
    required this.width,
    this.height = 50,
    this.bottomPadding = 0,
  });

  @override
  State<StatefulWidget> createState() => _FooterDelegate();
}

class _FooterDelegate extends State<_Footer> {
  _FooterDelegate();

  @override
  Widget build(BuildContext context) {
    if (widget.widget == null) {
      return Container(
        width: 0,
        height: 0,
      );
    } else {
      return Positioned(
        child: widget.widget!,
        width: widget.width,
        height: widget.height,
        bottom: widget.bottomPadding,
      );
    }
  }
}
