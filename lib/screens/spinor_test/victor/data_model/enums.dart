// First Created : 2021/1/29
// Author : Jinsu Gu (paser2@gmail.com)

/// 관계 설정용 enum
///
/// 모든 관계는 이 관계로 변환한 뒤에 FamilyTree에 넣어줘야 한다.
enum Relation {
  father,
  mother,
  partner,
  son,
  daughter,
  none,
}

/// 성별 지정용
enum Gender {
  male,
  female,
}