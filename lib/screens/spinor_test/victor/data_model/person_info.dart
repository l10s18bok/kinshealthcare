// First Created : 2021/1/29
// Author : Jinsu Gu (paser2@gmail.com)

import 'package:kins_healthcare/screens/spinor_test/victor/data_model/custom_class.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/data_model/enums.dart';

/// 인물 정보 저장용
/// FamilyTree에 인자로 저장할 때 무조건 isCenter 값을 한개 설정해 주어야 한다. (1개가 아니면 오류 생김)
class PersonInfo {
  final Gender gender;
  final String name;
  final String imagePath;
  final bool isCenter;

  List<Pair<Relation, PersonInfo>?> _relationList;

  /// 생성자
  ///
  /// FamilyTree에 인자로 저장할 때 무조건 isCenter 값을 한개 설정해 주어야 한다. (1개가 아니면 오류 생김)
  PersonInfo(
      {required this.gender,
      required this.name,
      this.isCenter = false,
      this.imagePath = 'lib/screens/spinor_test/victor/sample_image/avatar.png'})
      : _relationList = [];

  bool hasRelation(Relation relation, [Relation? relation2]) {
    if (relation2 != null) {
      return _relationList
          .any((item) => item!.first == relation || item.first == relation2);
    } else {
      return _relationList.any((item) => item!.first == relation);
    }
  }

  bool isRelated(PersonInfo info) => _relationList.any((item) => item!.second == info);

  List<Pair<Relation, PersonInfo>?> get relationList => _relationList;

  int get childCount => _relationList
      .where((a) => a!.first == Relation.son || a.first == Relation.daughter)
      .length;

  /// 관계 설정을 위해서 사용한다
  ///
  /// 가능하면 Relation에 해당하는 관계는 모두 연결해주는게 좋다.
  ///
  /// 함수 내에서 양쪽 처리를 수행하므로 a->b나 b->a 한번만 수행해주도록 하자.
  PersonInfo addRelationPair(Relation relation, PersonInfo relationTarget) {
    var type = Relation.none;

    switch (relation) {
      case Relation.father:
      case Relation.mother:
        type = gender == Gender.male ? Relation.son : Relation.daughter;
        break;
      case Relation.partner:
        type = Relation.partner;
        break;
      case Relation.son:
      case Relation.daughter:
        type = gender == Gender.male ? Relation.father : Relation.mother;
        break;
      default:
        return this;
    }

    this.addRelation(relation, relationTarget);

    if (type != Relation.none) {
      relationTarget.addRelation(type, this);
    }

    return this;
  }

  void addRelation(Relation relation, PersonInfo relationTarget) =>
      _relationList.add(Pair(relation, relationTarget));

  PersonInfo? getPersonInfo(Relation relation, [Relation? relation2]) {
    Pair<Relation, PersonInfo>? item;

    if (relation2 != null) {
      item = _relationList.firstWhere(
          (item) => item!.first == relation || item.first == relation2, orElse: () {
        return null;
      });
    } else {
      item = _relationList.firstWhere((item) => item!.first == relation, orElse: () {
        return null;
      });
    }

    if (item != null) {
      return item.second;
    } else {
      return null;
    }
  }

  Relation getRelation(PersonInfo info) {
    var item = _relationList.firstWhere((item) => item!.second == info, orElse: () {
      return null;
    });

    if (item != null) {
      return item.first;
    } else {
      return Relation.none;
    }
  }
}
