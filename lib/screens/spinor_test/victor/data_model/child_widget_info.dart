// First Created : 2021/1/29
// Author : Jinsu Gu (paser2@gmail.com)

import 'package:flutter/material.dart';

class ChildWidgetInfo {
  final Widget widget;
  final double height;
  final Color color;

  ChildWidgetInfo({required this.widget, this.height = 50, this.color = Colors.white});
}
