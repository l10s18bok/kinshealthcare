// First Created : 2021/1/29
// Author : Jinsu Gu (paser2@gmail.com)

import 'dart:collection';

class Pair<T, U> {
  T _first;
  U _second;

  Pair(this._first, this._second);

  T get first => _first;
  U get second => _second;
}

class UniqueQueue<T> {
  Queue<T> _queue;

  UniqueQueue() : _queue = Queue<T>();

  void addFirst(T item) {
    if (item != null && !_queue.contains(item)) {
      _queue.addFirst(item);
    }
  }

  void push(T item) {
    if (item != null && !_queue.contains(item)) {
      _queue.add(item);
    }
  }

  T? pop() {
    if (_queue.length >= 1) {
      return _queue.removeFirst();
    } else {
      return null;
    }
  }

  bool get isEmpty => _queue.isEmpty;
  bool get isNotEmpty => _queue.isNotEmpty;
}
