import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/category_filter.dart';
import 'package:kins_healthcare/screens/auth/register/register_complete_bottom_sheet.dart';
import 'package:kins_healthcare/screens/auth/verify/email/mail_verification.dart';
import 'package:kins_healthcare/screens/auth/verify/phone/phone_verification.dart';
import 'package:kins_healthcare/screens/friends/invite/friend_invited.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/demo_widget/numbered/banking/bank_select.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/demo_widget/test/family_tree_test.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/demo_widget/test/interactive_scroll_test.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/demo_widget/test/vibration_test.dart';
import 'package:kins_healthcare/screens/spinor_test/victor/demo_widget/test/widget_size_test.dart';

/*
 작성일 : 2021-02-17
 작성자 : Victor
 화면명 : 
 클래스 : VictorTest
 경로 : 
 설명 : 테스트 화면
*/

class VictorTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo List',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter Demo List'),
        ),
        body: SafeArea(
          child: DemoList(),
        ),
      ),
    );
  }
}

class DemoList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _DemoListState();
}

class _DemoListState extends State<DemoList> {
  Widget getButton({
    required String text,
    required Widget nextWidget,
    bool isPopup = false,
  }) {
    var size = MediaQuery.of(context).size;
    var width = size.width;

    return Container(
      margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
      alignment: Alignment.center,
      width: width,
      child: ElevatedButton(
        child: Text(text),
        onPressed: () {
          if (isPopup) {
            Get.bottomSheet(nextWidget, isScrollControlled: true);
          } else {
            Get.to(() => nextWidget);
          }
        },
        style: ButtonStyle(
          minimumSize: MaterialStateProperty.all(Size(width, 50)),
        ),
      ),
    );
  }

  Widget screenList() {
    return Column(
      children: [
        getButton(
          text: '계좌 등록 과정 일부 (deprecated)',
          nextWidget: BankSelect(),
        ),
        getButton(
          text: '필터링',
          nextWidget: CategoryFilter(),
          isPopup: true,
        ),
      ],
    );
  }

  Widget appliedScreenList() {
    return Column(
      children: [
        getButton(
          text: '휴대폰 인증',
          nextWidget: PhoneVerification(),
        ),
        getButton(
          text: '이메일 인증',
          nextWidget: EmailVerification(),
        ),
        getButton(
          text: '가입완료',
          nextWidget: RegisterCompleteBottomSheet(),
          isPopup: true,
        ),
        getButton(
          text: '받은 가족관계 요청',
          nextWidget: FriendInvited(),
        ),
      ],
    );
  }

  Widget testList() {
    return Column(
      children: [
        getButton(
          text: 'Interactive Scroll Widget 데모',
          nextWidget: InteractiveScrollWidgetTest(),
        ),
        getButton(
          text: 'Family Tree 데모',
          nextWidget: FamilyTreeTest(),
        ),
        getButton(
          text: '진동 테스트',
          nextWidget: VibrationTest(),
        ),
        getButton(
          text: '위젯 크기 측정 테스트',
          nextWidget: WidgetSizeTest(),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          flexibleSpace: SafeArea(
            child: TabBar(
              tabs: [
                Tab(text: '앱 화면 (구현중)'),
                Tab(text: '앱 화면 (완료)'),
                Tab(text: '실험실'),
              ],
            ),
          ),
        ),
        body: TabBarView(
          children: [
            screenList(),
            appliedScreenList(),
            testList(),
          ],
        ),
      ),
    );
  }
}
