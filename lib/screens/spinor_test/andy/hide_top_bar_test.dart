import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/layouts/layouts.dart';

import '../../../components/top_bar/simple_hide_top_bar.dart';

/*
 * 작성일 : 2021-02-16
 * 작성자 : Andy
 * 주요기능 : 탑바 스크롤중 "<" 버튼 숨기기 기능 간단한 사용설명서
 */

class HideTopBarTest extends StatefulWidget {
  @override
  _HideTopBarTestState createState() => _HideTopBarTestState();
}

class _HideTopBarTestState extends State<HideTopBarTest> {
  late ScrollController _scrollController; // 필수

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController(); //필수
  }

  @override
  void dispose() {
    _scrollController.dispose(); //  필수
    super.dispose();
  }

  renderListFamily() {
    return ListView.separated(
      shrinkWrap: true, // 필요에 따라 ..필수아님
      physics: NeverScrollableScrollPhysics(), // 리스트뷰 추가시 필요에따라 스크롤 되지 않도록 설정해야함.
      itemCount: 30,
      separatorBuilder: (_, idx) {
        return Divider(
          height: 5.0,
          color: Colors.grey,
        );
      },
      itemBuilder: (_, idx) {
        return ListTile(
          title: Text('이쁘니 (누나)'),
        );
      },
    );
  }

  renderBottomBtn() {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      height: 55.0,
      child: PrimaryButton(
        label: '등록',
        onTap: () {},
      ),
    );
  }

  renderTopBar() {
    return Column(
      children: [
        SimpleHideTopBar(
          title: '새 기념일 추가하기', // 필수
          scrollController: _scrollController, // 필수
        ),
        SizedBox(height: 16.0), // Topbar 하단 간격 필요에따라 조정
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Column(
        // Column 은 Topbar와 Body를 분리하기 위해 필수
        children: [
          renderTopBar(),
          Expanded(
            // ListView 이거나 SingleChildScrollView 이후부터 Body
            child: ListView(
              controller: _scrollController, // 스크롤 포지션을 Topbar에 전달 ..필수
              children: [
                renderListFamily(), // 테스트 리스트뷰 본체
                renderBottomBtn(), // Bottom Button 은 알아서 ...
              ],
            ),
          ),
        ],
      ),
    );
  }
}
