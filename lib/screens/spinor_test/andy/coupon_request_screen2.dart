import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_keyboard_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/sub_btn_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/grid/box_grid.dart';
import 'package:kins_healthcare/components/top_bar/simple_hide_top_bar.dart';
import 'package:kins_healthcare/controllers/kkakka_controller.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/models/local/local_kkakka_model.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/box_grid_model.dart';
import 'package:kins_healthcare/services/kkakka/model/kkakka_request_models.dart';
import 'package:kins_healthcare/services/relation/model/post_relation_search_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

/*
 * 작성일 : 2021-02-17
 * 작성자 : Andy
 * 화면명 : HS_3002(까까 요청)
 * 클래스 : KkakkaRequestScreen
 * 경로  : /kkakka/request/target/edit
 * 설명 : 까까 요청의 내용작성
 */

class KkakkaRequestScreen2 extends StatefulWidget {
  @override
  _KkakkaRequestScreen2State createState() => _KkakkaRequestScreen2State();
}

class _KkakkaRequestScreen2State extends State<KkakkaRequestScreen2> {
  final _formGlobalKey = GlobalKey<FormState>();
  late TextEditingController _singleLineController;

  // TextEditingController _requsetAmountController;
  // FocusNode _focus = FocusNode();
  late ScrollController _scrollController;

  final models = [
    BoxGridModel(
      title: '기념일',
      imagePath: 'assets/svgs/ic/ic_anniversary_publish_unselect.svg',
      selectedImagePath: 'assets/svgs/ic/ic_anniversary_publish_select.svg',
      ratio: 105 / 90,
    ),
    BoxGridModel(
      title: '용돈',
      imagePath: 'assets/svgs/ic/ic_money_publish_unselect.svg',
      selectedImagePath: 'assets/svgs/ic/ic_money_publish_select.svg',
      ratio: 105 / 90,
    ),
    BoxGridModel(
      title: '의료비',
      imagePath: 'assets/svgs/ic/ic_medical_publish_unselect.svg',
      selectedImagePath: 'assets/svgs/ic/ic_medical_publish_select.svg',
      ratio: 105 / 90,
    ),
  ];

  final TextUtils textUtils = TextUtils();

  late SearchRelResponse _user;
  late LocalKkakkaModel localKkakkaModel;
  bool checkFlag = false;
  String _requestAmountText = '';
  String kkakkaTitle = '';

  //String _kkakkaPrice = '';

  @override
  void initState() {
    super.initState();
    _singleLineController = TextEditingController();
    // _requsetAmountController = TextEditingController();
    // _focus.addListener(_onFocusChange);
    _scrollController = ScrollController();
    localKkakkaModel = Get.arguments;
    if (localKkakkaModel.userRelation != null) {
      _user = localKkakkaModel.userRelation!;
    } else {
      _user = SearchRelResponse(
          profileImgOpponent: '',
          userNameOpponent: '',
          userRelCdOpponentValue: '',
          userPhoneOpponent: '',
          userRelCdMeValue: '',
          userNoRel: 0,
          userStatusOpponent: '');
    }
  }

  @override
  void dispose() {
    _singleLineController.dispose();
    // _requsetAmountController.dispose();
    // _focus.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  _onGridSelectModel(BoxGridModel model) {
    setState(() {
      checkFlag = true;
    });
    kkakkaTitle = model.title;
  }

  _sendData() {
    final kkakkaType = localKkakkaModel.kkakkaType;
    final priceL =
        _requestAmountText.split(',')[0] + _requestAmountText.split(',')[1];
    final priceKkakka = int.tryParse(priceL) ?? 0;
    final sendData = PostKkakkaRequestRequestBody(
      kkakkaMessage: _singleLineController.text,
      kkakkaPrice: priceKkakka,
      kkakkaType: kkakkaType,
      endDate: '2021-03-23',
      startDate: '2021-02-23',
      userNoRel: _user.userNoRel,
    );
    print('전송데이터 : ${sendData.toJson()}');
    return sendData;
  }

  _showBottomSubBtnSheet() async {
    Future.microtask(() async {
      try {
        final modelData = _sendData();
        final resp =
            await Get.find<KkakkaController>().requestKkakka(modelData);
        if (resp == 'SUCCESS') {
          final sheet = SubBtnBottomSheet(
            boldTitle: _user.userNameOpponent,
            blueTitle: _user.userRelCdOpponentValue,
            boldTitle02: '에게',
            title: '까까 요청이 완료되었습니다.',
          );

          final result = await Get.bottomSheet<String>(sheet);
          if (result == 'OK') Get.offAllNamed('/main-home');
        } else
          Get.snackbar('실패', resp);
      } on DioError catch (e) {
        final msg = e.response!.data.toString();
        print(msg);
      }
    });
  }

  _onFocusChange() async {
    // print("Focus: " + _focus.hasFocus.toString());
    // if (_focus.hasFocus) {
    //   _focus.unfocus();
    //   final result =
    //       await Get.bottomSheet<MoneyBottomSheetResult>(MoneyBottomSheet());
    //   _requsetAmountController.text =
    //       textUtils.numberToLocalCurrency(amount: result.amount) + ' 원';
    //   print('result : ${result}');
    // }
    final result = await Get.bottomSheet<MoneyBottomSheetResult>(
        MoneyKeyboardBottomSheet(),
        isScrollControlled: true);
    _requestAmountText =
        textUtils.numberToLocalCurrency(amount: result!.amount) + ' 원';
    print('result : ${_requestAmountText}');
    setState(() {});
  }

  // _keyboardUp() {
  //   FocusScope.of(context).requestFocus(new FocusNode());
  //   showKeyboard = true;
  //   _scrollController.animateTo(MediaQuery.of(context).size.height / 3,
  //       duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
  //   setState(() {});
  // }

  // _onKeyboardInput(val) {
  //   final resp = Get.find<PaymentController>().onAmountInput(val: val);
  //   print('~~~~~  ${resp} ~~~~~');
  //   _kkakkaPrice = resp;
  //   setState(() {});
  // }

  // _onKeyboardConfirm() {
  //   showKeyboard = false;
  //   setState(() {});
  // }

  renderLabel(String label, {String? subLabel}) {
    final theme = ThemeFactory.of(context).theme;
    return Row(
      children: [
        Text(
          label,
          style: TextStyle(
              fontSize: theme.fontSize13,
              color: Colors.black,
              fontWeight: theme.heavyFontWeight),
        ),
        SizedBox(width: 4),
        Text(
          subLabel ?? '',
          style: TextStyle(fontSize: theme.fontSize13, color: kinsGrayB6),
        ),
      ],
    );
  }

  renderAnimationTopBar() {
    return SimpleHideTopBar(
      title: '까까 요청하기',
      scrollController: _scrollController,
    );
  }

  renderInputTextFild() {
    final theme = ThemeFactory.of(context).theme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 28),
        renderLabel('한줄 편지'),
        SizedBox(height: 8),
        Center(
          child: Container(
            height: 80,
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 14),
            alignment: Alignment.topLeft,
            decoration: BoxDecoration(
              border: Border.all(
                color: Color(0xFFD7D8EE),
                width: 1,
              ),
              borderRadius: BorderRadius.circular(6.0),
            ),
            child: TextField(
              controller: _singleLineController,
              maxLines: null,
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.done,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: const EdgeInsets.all(0),
                hintStyle: TextStyle(
                    color: Color(0xFFB6B6B6), fontSize: theme.fontSize15),
                hintText: '회원님의 마음을 입력해주세요.',
              ),
            ),
          ),
        ),
        SizedBox(height: 26),
      ],
    );
  }

  renderSendPerson() {
    final theme = ThemeFactory.of(context).theme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        renderLabel('요청할 분'),
        Container(
          height: 42,
          margin: EdgeInsets.only(top: 18, bottom: 21),
          child: Row(
            children: [
              CircleAvatar(
                  backgroundImage:
                      CachedNetworkImageProvider(_user.profileImgOpponent)),
              SizedBox(width: 15),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                      text: _user.userNameOpponent,
                      children: [
                        TextSpan(
                          text: ' ${_user.userRelCdOpponentValue}',
                          style: TextStyle(
                            color: Color(0xFF4042AB),
                            fontSize: theme.fontSize12,
                            fontWeight: theme.primaryFontWeight,
                          ),
                        ),
                      ],
                      style: TextStyle(
                        color: Color(0xFF363636),
                        fontSize: theme.fontSize14,
                        fontWeight: theme.primaryFontWeight,
                      ),
                    ),
                  ),
                  // SizedBox(height: 6),
                  Text(
                    _user.userPhoneOpponent,
                    style: TextStyle(
                      color: Color(0xFF535353),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  renderUnderLineContainer(String hintText, String trailing) {
    final theme = ThemeFactory.of(context).theme;
    return Container(
      height: 43,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Color(0xFF878787),
            width: 1.0,
          ),
        ),
      ),
      child: Stack(
        alignment: Alignment.centerLeft,
        children: [
          Text(
            _requestAmountText.isEmpty ? hintText : _requestAmountText,
            style: TextStyle(
              color:
                  _requestAmountText.isEmpty ? Color(0xFFD6D6D6) : Colors.black,
              fontSize: theme.fontSize15,
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Text(
              trailing,
              style: TextStyle(
                color: Color(0xFF262626),
                fontSize: theme.fontSize15,
              ),
            ),
          ),
        ],
      ),
    );
  }

  // renderCardList(int index, ThreeStringModel card) {
  //   return ListTile(
  //     dense: true,
  //     leading: Text('하나카드',
  //         style: TextStyle(
  //           color: Colors.grey,
  //         )),
  //     title: Text(card.secoundString),
  //   );
  // }

  // renderExpansionListTile({String label, String subtitle}) {
  //   final theme = ThemeFactory.of(context).theme;

  //   return Container(
  //     margin: EdgeInsets.only(top: 26),
  //     padding: EdgeInsets.symmetric(horizontal: 16),
  //     child: Column(
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: [
  //         renderLabel(label),
  //         SizedBox(height: 12),
  //         ExpansionTile(
  //           tilePadding: EdgeInsets.zero,
  //           title: null,
  //           expandedAlignment: Alignment.centerLeft,
  //           leading: Text(
  //             subtitle,
  //             style: TextStyle(
  //               color: subtitle == '선택없음' ? Colors.grey : Colors.black,
  //               fontSize: theme.fontSize15,
  //             ),
  //           ),
  //           children: [
  //             Divider(
  //               color: Color(0xFF878787),
  //               height: 1,
  //             ),
  //             ..._bankList
  //                 .asMap()
  //                 .map((index, card) => renderCardList(index, card))
  //                 .values
  //                 .toList(),
  //           ],
  //         ),
  //         Divider(
  //           height: 1,
  //           color: Color(0xFF878787),
  //         ),
  //       ],
  //     ),
  //   );
  // }

  // renderSuffixIcon() {
  //   return GestureDetector(
  //     onTap: () {
  //       _requsetAmountController.clear();
  //     },
  //     child: Container(
  //       width: 16.0,
  //       height: 16.0,
  //       decoration: BoxDecoration(
  //         shape: BoxShape.circle,
  //         color: Color(0xFFD8D8D8),
  //       ),
  //       child: Icon(
  //         Icons.close,
  //         size: 10.0,
  //         color: Colors.white,
  //       ),
  //     ),
  //   );
  // }

  // renderUnderLineTextFormField(String hintText) {
  //   final theme = ThemeFactory.of(context).theme;
  //   final baseInputDecoration = InputDecoration(
  //       hintText: hintText,
  //       hintStyle: TextStyle(
  //         color: Color(0xFFB2ACFF),
  //         fontSize: theme.fontSize15,
  //       ));
  //   // final suffixInputDecoration = baseInputDecoration.copyWith(
  //   //   suffix: renderSuffixIcon(),
  //   // );
  //   return TextFormField(
  //     controller: _requsetAmountController,
  //     focusNode: _focus,
  //     validator: (val) {
  //       if (val.isEmpty) {
  //         return '잘못된 입력';
  //       }
  //       return null;
  //     },
  //     onSaved: (val) {
  //       print('val');
  //     },
  //     decoration: baseInputDecoration,
  //   );
  // }

  renderSelectTextField({required String label, required String hintText}) {
    return InkWell(
      onTap: _onFocusChange,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(height: 28),
          renderLabel(label),
          SizedBox(height: 10),
          renderUnderLineContainer(hintText, ''),
        ],
      ),
    );
  }

  renderCategory() {
    return Column(
      children: [
        renderLabel('카테고리'),
        SizedBox(height: 8),
        BoxGrid(
          rowCount: 2,
          models: models,
          onLastSelectItem: _onGridSelectModel,
        ),
      ],
    );
  }

  renderPublishButton() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      margin: const EdgeInsets.only(bottom: 16),
      child: PrimaryButton(
        label: "요청",
        onTap: _showBottomSubBtnSheet,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Form(
        key: _formGlobalKey,
        child: Column(
          children: [
            renderAnimationTopBar(),
            SizedBox(height: 30),
            Expanded(
              child: ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                controller: _scrollController,
                children: [
                  renderSendPerson(),
                  Divider(color: Color(0xFFE8E7EC), thickness: 6),
                  renderSelectTextField(label: '요청 금액', hintText: '0'),
                  renderInputTextFild(),
                  renderCategory(),
                ],
              ),
            ),
            renderPublishButton(),
          ],
        ),
      ),
    );
  }
}
