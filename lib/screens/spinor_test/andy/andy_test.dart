import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/biometrics_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/card_payment_complete_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/new_payment_method_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/sub_btn_sheet.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/models/local/local_emergency_model.dart';
import 'package:kins_healthcare/screens/auth/register/profile/profile_detail_screen.dart';
import 'package:kins_healthcare/screens/kkakka/issuance/donate/donate_target_screen.dart';
import 'package:kins_healthcare/screens/kkakka/issuance/donate/kkakka_donate_screen.dart';
import 'package:kins_healthcare/screens/kkakka/request/kkakka_request_screen.dart';
import 'package:kins_healthcare/screens/kkakka/request/request_target.dart';
import 'package:kins_healthcare/screens/kkakka/request/select_request_type.dart';
import 'package:kins_healthcare/screens/main_home/request/detail/request_see_more.dart';
import 'package:kins_healthcare/screens/main_home/request/request_page_screen.dart';
import 'package:kins_healthcare/screens/my_kins/anniversary/register/add_anniversary_screen.dart';
import 'package:kins_healthcare/screens/payment/emergency/detail/hospital_detail.dart';
import 'package:kins_healthcare/screens/payment/emergency/emergency_reg_screen.dart';
import 'package:kins_healthcare/screens/payment/emergency/search/find_hospital_screen.dart';
import 'package:kins_healthcare/screens/payment/methods/register/account_reg_pageList.dart';
import 'package:kins_healthcare/screens/payment/methods/register/card/add_card.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_keyboard_bottom_sheet.dart';
import 'package:kins_healthcare/screens/kkakka/issuance/issuance_send_screen.dart';

// TODO andy 테스트용 삭제예정!
class AndyTest extends StatefulWidget {
  @override
  _AndyTestState createState() => _AndyTestState();
}

class _AndyTestState extends State<AndyTest> {
  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 16.0,
        ),
        child: ListView(
          children: [
            Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.lightGreen,
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Column(
                children: [
                  Text(
                    "Welcom Andy World!!",
                    style: TextStyle(color: Colors.yellow, fontSize: 25),
                  ),
                  TextButton(
                    onPressed: () {
                      Get.to(
                        ProfileDetailScreen(
                          userRelNo: 2,
                          tagUrl: 'https://d3q7af4rsfl8rt.cloudfront.net/image/202104/1617594322718_image_cropper_1617594298669.jpg',
                        ),
                      );
                    },
                    child: Text(
                      "HY_2003(신규 프사)",
                      style: TextStyle(decoration: TextDecoration.lineThrough),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      Get.toNamed('/my-kins/family-children');
                    },
                    child: Text(
                      "HY_CHILD_LIST(나의 자녀 리스트))",
                      style: TextStyle(decoration: TextDecoration.lineThrough),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      Get.toNamed('/auth/register-kid');
                    },
                    child: Text(
                      "HM_CHILD(자녀 회원가입)",
                      style: TextStyle(decoration: TextDecoration.lineThrough),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      Get.to(
                        SelectRequestType(),
                      );
                    },
                    child: Text(
                      "HS_0001(까까요청 -> 발행할 까까 선택)",
                      style: TextStyle(decoration: TextDecoration.lineThrough),
                    ),
                  ),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          RequestTarget(),
                        );
                      },
                      child: Text(
                        "HS_3001(까까 요청 -> 대상선정)",
                        style: TextStyle(decoration: TextDecoration.lineThrough),
                      )),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          IssuanceSendScreen(),
                        );
                      },
                      child: Text("HU_4002N(신규 까까 발행)")),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          DonateTargetScreen(),
                        );
                      },
                      child: Text("HU_3001(까까로 기부 -> 대상선정)")),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          KkakkaDonateScreen(),
                        );
                      },
                      child: Text("HU_3003(까까로 기부)")),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          KkakkaRequestScreen(),
                        );
                      },
                      child: Text("HS_3002N(신규 까까 요청)")),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          RequestSeeMore(),
                        );
                      },
                      child: Text("HD_0001(까까발행 -> 자세히보기")),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          AddCard(),
                        );
                      },
                      child: Text("HP_3002(계좌등록 -> 결제카드추가")),
                  TextButton(
                      onPressed: () {
                        Get.bottomSheet<MoneyBottomSheetResult>(MoneyKeyboardBottomSheet(), isScrollControlled: true);
                      },
                      child: Text("커스텀 키보드 바탐시트")),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          CardPaymentCompleteBottomSheet(),
                        );
                      },
                      child: Text("HP_3003 카드등록완료 바탐시트", style: TextStyle(decoration: TextDecoration.lineThrough))),
                  TextButton(
                      onPressed: () {
                        Get.bottomSheet(NewPaymentMethodBottomSheet());
                      },
                      child: Text("HP_3001_N(결제수단등록 스넥바)", style: TextStyle(decoration: TextDecoration.lineThrough))),
                  TextButton(
                      onPressed: () {
                        Get.bottomSheet(SubBtnBottomSheet(
                          title: '타이틀',
                          boldTitle: 'dl',
                          blueTitle: '',
                        ));
                      },
                      child: Text("스넥바", style: TextStyle(decoration: TextDecoration.lineThrough))),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          EmergencyRegScreen(),
                        );
                      },
                      child: Text("HP_5001(응급병원관리 -> 등록한 병원 목록보기", style: TextStyle(decoration: TextDecoration.lineThrough))),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          FindHospitalScreen(),
                        );
                      },
                      child: Text("HP_5002(응급병원관리(병원찾기) -> JC Pagination 응급병원찾기", style: TextStyle(decoration: TextDecoration.lineThrough))),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          HospitalDetail(),
                        );
                      },
                      child: Text("HP_5003(병원 상세정보 -> 병원 상세정보", style: TextStyle(decoration: TextDecoration.lineThrough))),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          RequestScreenPage(type: ScreenTypes.received),
                        );
                      },
                      child: Text("HH_3001", style: TextStyle(decoration: TextDecoration.lineThrough))),
                  TextButton(
                      onPressed: () {
                        Get.toNamed('/payment/emergency/share-register', arguments: EmHospitalDetailData(hsptlName: '서울삼성병원', hsptlId: 18));
                      },
                      child: Text("HP_5004(병원 상세정보 -> 나누기 설정", style: TextStyle(decoration: TextDecoration.lineThrough))),
                  TextButton(
                      onPressed: () {
                        Get.toNamed('/payment/emergency/share-update', arguments: EmHospitalDetailData(hsptlName: '서울삼성병원', hsptlId: 18));
                      },
                      child: Text("HP_5004(병원 상세정보 -> 나누기 업데이트", style: TextStyle(decoration: TextDecoration.lineThrough))),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          AccountRegPageList(),
                        );
                      },
                      child: Text("HP_3004 (계좌등록 => 은행선택), HP_3005(계좌등록 => 계좌번호입력), HP_3006(계좌등록 => ARS)")),
                  TextButton(
                      onPressed: () {
                        Get.to(
                          AddAnniversaryScreen(),
                        );
                      },
                      child: Text("HY_3003 + HY_3004(기념일 등록 및 공개가족선택")),
                  // TextButton(
                  //     onPressed: () {
                  //       Get.toNamed(
                  //         '/auth/verify/phone/phone',
                  //         arguments: PhoneScreenArguments(
                  //           phone: '01062162588',
                  //         ),
                  //       );
                  //     },
                  //     child: Text("PhoneScreen ")),
                  TextButton(
                      onPressed: () {
                        Get.toNamed('/auth/register/profile');
                      },
                      child: Text("프로필 스크린")),
                  TextButton(
                      onPressed: () {
                        Get.toNamed('/friends');
                      },
                      child: Text("신규기획")),
                  TextButton(
                      onPressed: () {
                        Get.toNamed('/coach-mark/main-home');
                      },
                      child: Text("코치마크")),

                  TextButton(
                      onPressed: () {
                        Get.bottomSheet<BiometricsBottomSheet>(BiometricsBottomSheet(), isScrollControlled: true);
                      },
                      child: Text("BiometricsBottomSheet")),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
