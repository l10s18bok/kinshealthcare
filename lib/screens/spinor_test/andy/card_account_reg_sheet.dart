import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-19
 * 작성자 : Andy
 * 화면명 : HP_3001_N
 * 주요기능 : 카드 및 계좌등록 바텀시트
 */

class CardAccountRegSheet extends StatefulWidget {
  final String firstButtonTitle;
  final String firstAssetUrl;

  final String scoundButtonTitle;
  final String scoundAssetUrl;

  final String mainBtnTitle;

  const CardAccountRegSheet({
    Key? key,
    this.firstButtonTitle = '카드 등록',
    this.firstAssetUrl = 'assets/svgs/ic/ic_card_paymentpopup.svg',
    this.scoundButtonTitle = '계좌 등록',
    this.scoundAssetUrl = 'assets/svgs/ic/ic_bank_paymentpopup.svg',
    this.mainBtnTitle = '확인',
  }) : super(key: key);

  @override
  _CardAccountRegSheetState createState() => _CardAccountRegSheetState();
}

class _CardAccountRegSheetState extends State<CardAccountRegSheet> {
  //
  renderBoxCon(String imgSrc, String name) {
    final theme = ThemeFactory.of(context).theme;
    return Container(
      height: 64,
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.fromLTRB(20, 11, 20, 11),
      decoration: BoxDecoration(
        border: Border.all(
          color: Color(0xFFD7D8EE),
          width: 1,
        ),
        borderRadius: BorderRadius.circular(6.0),
      ),
      child: Stack(
        alignment: Alignment.centerLeft,
        children: [
          SvgPicture.asset(
            imgSrc,
          ),
          Align(
            alignment: Alignment.center,
            child: Text(
              name,
              style: TextStyle(color: Color(0xFF545454), fontSize: theme.fontSize17),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 279.0,
      width: 360,
      child: Container(
        padding: EdgeInsets.fromLTRB(16, 30, 16, 16),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: const Radius.circular(26.0),
              topRight: const Radius.circular(26.0),
            )),
        child: Column(
          children: [
            Material(
              borderRadius: BorderRadius.circular(6.0),
              child: InkWell(
                onTap: () {
                  Get.back(result: 'CARD');
                  //Get.offNamed('/payment/methods/register/card');
                },
                child: renderBoxCon(widget.firstAssetUrl, widget.firstButtonTitle),
              ),
            ),
            Material(
              borderRadius: BorderRadius.circular(6.0),
              child: InkWell(
                  onTap: () {
                    Get.back(result: 'ACCOUNT');
                    //Get.offNamed('/payment/methods/register/account_reg_pageList');
                  },
                  child: renderBoxCon(widget.scoundAssetUrl, widget.scoundButtonTitle)),
            ),
            Container(
              height: 55,
              alignment: Alignment.center,
              child: PrimaryButton(
                label: widget.mainBtnTitle,
                onTap: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }
}
