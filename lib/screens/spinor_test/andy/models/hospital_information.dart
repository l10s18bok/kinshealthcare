import 'dart:convert';

/*
 * 작성일 : 2021-01-19
 * 작성자 : Andy
 * 사용화면 : HP_5001(응급병원관리 -> 응급병원등록), HP_5002(응급병원관리 => 병원찾기), HospitalInfoBox(컴포넌트), HospitalInformation(컴포넌트)
 */

class HospitalInformation {
  final String? imgUrl; // 병원이미지
  final String? name; // 병원 이름
  final String? subname; // 병원 레벨(ex. 상급종합병원)
  final String? weekday; // 평일 영업시간
  final String? weekend; // 주말 영업시간

  HospitalInformation({
    this.imgUrl,
    this.name,
    this.subname,
    this.weekday,
    this.weekend,
  });

  Map<String, dynamic> toMap() {
    return {
      'imgUrl': imgUrl,
      'name': name,
      'subname': subname,
      'weekday': weekday,
      'weekend': weekend,
    };
  }

  factory HospitalInformation.fromMap(Map<String, dynamic> map) {
    return HospitalInformation(
      imgUrl: map['imgUrl'],
      name: map['name'],
      subname: map['subname'],
      weekday: map['weekday'],
      weekend: map['weekend'],
    );
  }

  String toJson() => json.encode(toMap());

  factory HospitalInformation.fromJson(String source) =>
      HospitalInformation.fromMap(json.decode(source));
}
