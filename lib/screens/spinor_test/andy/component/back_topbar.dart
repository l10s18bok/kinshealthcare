import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/**
 * @comment 뒤로 Back 하기 위한 TopBar
 * @author  이명재
 * @version 1.0
 * @since   2021-01-25
 */

// ignore: must_be_immutable
class BackTopbar extends StatelessWidget {
  BackTopbar({
    this.title,
    this.height,
  });

  String? title;
  double? height;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 16),
      alignment: Alignment.centerLeft,
      width: double.infinity,
      height: height,
      child: ListView(
        shrinkWrap: true,
        children: [
          renderBackBtn(),
          renderTitle(context, title),
        ],
      ),
    );
  }

  renderTitle(BuildContext context, String? title) {
    if (title == null) return Container();

    return Text(
      title,
      style: TextStyle(
        color: kinsBlack,
        fontWeight: FontWeight.w700,
        fontSize: ThemeFactory.of(context).theme.fontSize22,
      ),
    );
  }

  renderBackBtn() {
    return InkWell(
      child: Container(
        alignment: Alignment.centerLeft,
        height: 44,
        width: 40,
        child: SvgPicture.asset(
          'assets/svgs/ic/ic_arrowback_common.svg',
          width: 24,
          height: 24,
          color: kinsGrayA3,
        ),
      ),
      onTap: () => Get.back(),
    );
  }
}
