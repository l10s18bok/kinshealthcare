import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kins_healthcare/services/emergency/model/post_search_list_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-20
 * 작성자 : Andy
 * 사용화면 : HP_5001(응급병원등록), HP_5002(응급병원관리(병원찾기))
 */

class HospitalInfoBox extends StatelessWidget {
  final PostSearchListResponse hospitalInformation;
  final VoidCallback? onTabBox;
  final VoidCallback? onTabWrite;
  final VoidCallback? onTabDelete;

  const HospitalInfoBox(
      {Key? key,
      required this.hospitalInformation,
      this.onTabBox,
      this.onTabWrite,
      this.onTabDelete})
      : super(key: key);

  renderImageBox() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(6),
      child: CachedNetworkImage(
        imageUrl: hospitalInformation.photo1!,
        fit: BoxFit.fitHeight,
        alignment: Alignment.center,
        width: 98,
        height: 98,
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
    );
  }

  renderOpeningHospital(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(top: 6, bottom: 7),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              hospitalInformation.hsptlName!,
              style: TextStyle(
                  fontSize: theme.fontSize17,
                  color: Color(0xFF232323),
                  fontWeight: theme.heavyFontWeight),
            ),
            SizedBox(height: 5),
            Text(
              hospitalInformation.hsptlType!,
              style: TextStyle(
                fontSize: theme.fontSize14,
                color: Color(0xFF727272),
              ),
            ),
            SizedBox(height: 4),
            Text(
              '평일 : ${hospitalInformation.wkdayS} ~ ${hospitalInformation.wkdayE}',
              style: TextStyle(
                  fontSize: theme.fontSize10,
                  color: Color(0xFF424242),
                  fontWeight: theme.heavyFontWeight),
            ),
            SizedBox(height: 2),
            Text(
              '주말 : ${hospitalInformation.wkdayS} ~ ${hospitalInformation.wkdayE}',
              style: TextStyle(
                  fontSize: theme.fontSize10,
                  color: Color(0xFF424242),
                  fontWeight: theme.heavyFontWeight),
            ),
          ],
        ),
      ),
    );
  }

  renderOnTabFor(BuildContext context) {
    return InkWell(
      onTap: onTabBox,
      child: Row(
        children: [
          renderImageBox(),
          SizedBox(width: 12),
          renderOpeningHospital(context),
        ],
      ),
    );
  }

  renderIconBox() {
    if (onTabDelete == null || onTabWrite == null) {
      return Container();
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        InkWell(
          onTap: onTabWrite!,
          child: SvgPicture.asset(
            'assets/svgs/ic/ic_modify_hospital.svg',
          ),
        ),
        InkWell(
          onTap: onTabDelete!,
          child: SvgPicture.asset(
            'assets/svgs/ic/ic_delete_hospital.svg',
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 10),
      decoration: BoxDecoration(
        border: Border.all(
          color: Color(0xFFD7D8EE),
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(6),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            flex: 4,
            child: renderOnTabFor(context),
          ),
          Expanded(
            flex: 1,
            child: renderIconBox(),
          ),
        ],
      ),
    );
  }
}
