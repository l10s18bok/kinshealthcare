import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/friend_button.dart';
import 'package:kins_healthcare/components/card/friend_with_button_card.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/controllers/relation_controller.dart';
import 'package:kins_healthcare/layouts/layouts.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/relation/relation_require_opponent_model.dart';

class TestBody extends GeneralPaginationBody {
  final String test;

  TestBody({
    this.test = 'test',
  });
}

class DemoScreen extends StatefulWidget {
  @override
  _DemoScreenState createState() => _DemoScreenState();
}

class _DemoScreenState extends State<DemoScreen> {
  renderRegisterFriendCard({
    required RelationReqOpponentResponse relation,
  }) {
    bool isOnContacts = false;

    return FriendCardWithButtons(
      meName: '나',
      meRelationship: relation.userRelCdMeValue,
      oppName: relation.userRelCdOpponentValue!,
      oppRelationship: relation.userRelCdOpponentValue!,
      isOnContacts: isOnContacts,
      phoneNumber: relation.userPhoneMe!,
      avatarImageUrl: null,
      buttons: [
        FriendButton(
          variant: FriendButtonVariant.lightBlue,
          onTap: () {
            // TODO 미완
            Get.snackbar(
              '연동 미완',
              'API가 완료되었는데 이 메세지가 보이면 작업 요청을 해주세요!',
            );
          },
          text: '재요청',
        ),
        FriendButton(
          variant: FriendButtonVariant.blue,
          onTap: () {},
          text: '수락',
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return TitleLayout(
      title: 'ListView Test',
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: GetBuilder<RelationController>(
          builder: (c) {
            return PaginationListView<RelationReqOpponentResponse>(
              request: c.listRelationReqOpponents,
              controller: c,
              itemCount: c.relationReqOpponents.length,
              itemBuilder: (BuildContext context, int index) {
                return renderRegisterFriendCard(
                  relation: c.relationReqOpponents[index],
                );
              },
              separatorBuilder: (context, index) {
                return Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Divider(),
                );
              },
            );
          },
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    Get.find<RelationController>().listRelationReqOpponents(
      reset: true,
    );
  }
}
