// import 'dart:ui';

// import 'package:flutter/material.dart';
// import 'package:get/instance_manager.dart';
// import 'package:kins_healthcare/components/listview/new_pagination_list_view.dart';
// import 'package:kins_healthcare/controllers/post_controller.dart';
// import 'package:kins_healthcare/services/post/model/new_post_list_model.dart';

// /*
//  작성일 : 2021-05-10
//  작성자 : (baron)
//  화면명 : (화면 코드 입력)
//  경로 :
//  클래스 : PostListView
//  설명 : (설명 )
// */

// class PostListView extends StatefulWidget {
//   @override
//   _PostListViewState createState() => _PostListViewState();
// }

// class _PostListViewState extends State<PostListView> {
//   PostController? postController;
//   List<Map<String, dynamic>> result = [];
//   ScrollController? scrollController;
//   bool? isLast;
//   // 더 보기

//   @override
//   void initState() {
//     postController = Get.find<PostController>();
//     scrollController = ScrollController();
//     scrollController!.addListener(() {
//       var isbottom = scrollController!.position.pixels ==
//           scrollController!.position.maxScrollExtent;
//       Future.delayed(Duration(seconds: 2));
//     });
//     initResult();
//     super.initState();
//   }

//   initResult() async {
//     await postController!
//         .postListRequest(
//       reset: true,
//     )
//         .then((_) {
//       var temp = postController!.postequestList;
//       if (temp is List) {
//         temp.forEach((item) {
//           var temp = item as TheNewPostListModel;
//           setState(() {
//             result.add(temp.message!.length > 100
//                 ? {'isContentsMore': true, 'result': temp}
//                 : {'isContentsMore': false, 'result': temp});
//           });
//         });
//       }
//     });
//   }

//   Widget showContentsMore(int index) {
//     var data = result[index]['result'] as TheNewPostListModel;
//     return Text(
//       data.message!,
//     );
//   }

//   Widget showContentsTruncate(int index) {
//     var data = result[index]['result'] as TheNewPostListModel;
//     return Text(
//       data.message!,
//       maxLines: 1,
//     );
//   }

//   Widget showMoreButton(int index) {
//     return TextButton(
//         onPressed: () {
//           setState(() {
//             result[index]['isContentsMore'] == true;
//           });
//         },
//         child: Text('더 보기'));
//   }

//   @override
//   Widget build(BuildContext context) {
//     return result.isNotEmpty
//         ? NewPaginationListView<TheNewPostListModel>(
//             controller: postController!,
//             cacheKey: 'default',
//             scrollController: scrollController,
//             itemCount: result.length,
//             itemBuilder: (context, index) {
//               return Column(
//                 children: [
//                   GestureDetector(
//                     onTap: () => {},
//                     child: Container(
//                       decoration: BoxDecoration(),
//                       child: Column(
//                         children: [
//                           Image.network('https://picsum.photos/200/300'),
//                           Center(
//                             child: Text('${index}'),
//                           )
//                         ],
//                       ),
//                     ),
//                   ),
//                   Row(
//                     children: [
//                       result[index]['isContentsMore']
//                           ? showMoreButton(index)
//                           : Text('not more'),
//                     ],
//                   )
//                 ],
//               );
//             },
//             lastWidget: Container(
//                 child: Center(
//               child: Text('last'),
//             )),
//             request: postController!.postListRequest,
//           )
//         : Center(child: CircularProgressIndicator());
//   }
// }
