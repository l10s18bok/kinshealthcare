import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/screens/auth/reset/email/email_screen.dart';
import 'package:kins_healthcare/screens/auth/reset/password/password_screen.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HL_IP_0002 & HL_IP_0001
 * 주요기능 : 아이디/비밀번호 찾기 탭
 */

/// /auth/reset/forgot \
/// 아이디/비밀번호 찾기 탭
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A6665)
///
/// - 화면명 : HL_IP_0002 & HL_IP_0001 \
/// - 작성자 : JC \
/// - 작성일 : 2021-01-11
///
/// {@category Screen}
class ForgotScreen extends StatefulWidget {
  @override
  ForgotScreenState createState() => ForgotScreenState();
}

/// /auth/reset/forgot \
/// 아이디/비밀번호 찾기 탭 종류
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A6665)
///
/// - 화면명 : HL_IP_0002 & HL_IP_0001 \
/// - 작성자 : JC \
/// - 작성일 : 2021-01-11
///
/// {@category ScreenMisc}
enum ScreenType {
  /// 이메일 찾기
  email,

  /// 비밀번호 찾기
  password,
}

/// 아이디/비밀번호 찾기 탭 종류
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A6665)
///
/// - 화면명 : HL_IP_0002 & HL_IP_0001 \
/// - 작성자 : JC \
/// - 작성일 : 2021-01-11
///
/// {@category ScreenMisc}
class ForgotScreenState extends State<ForgotScreen> {
  late ScreenType screenType;

  @override
  void initState() {
    super.initState();

    // email || password || null
    final type = Get.parameters['type'];

    if (type == 'email') {
      screenType = ScreenType.email;
    } else {
      screenType = ScreenType.password;
    }
  }

  renderTopText() {
    final theme = ThemeFactory.of(context).theme;

    final selectedStyle = TextStyle(
      color: Color(0xFF262626),
      fontSize: theme.fontSize22,
      fontWeight: theme.heavyFontWeight,
    );

    final unselectedStyle = selectedStyle.copyWith(
      color: Color(0xFFC2C3C8),
    );

    return Row(
      children: [
        InkWell(
          onTap: () {
            setState(() {
              screenType = ScreenType.email;
            });
          },
          child: Text(
            '아이디 찾기',
            style: screenType == ScreenType.email
                ? selectedStyle
                : unselectedStyle,
          ),
        ),
        Text(
          ' / ',
          style: unselectedStyle,
        ),
        InkWell(
          onTap: () {
            setState(() {
              screenType = ScreenType.password;
            });
          },
          child: Text(
            '비밀번호 찾기',
            style: screenType == ScreenType.email
                ? unselectedStyle
                : selectedStyle,
          ),
        ),
      ],
    );
  }

  renderBody() {
    if (screenType == ScreenType.email) {
      return EmailScreen();
    } else {
      return PasswordScreen();
    }
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(
        customTitle: renderTopText(),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            // renderTopText(),
            Container(height: 50.0),
            Expanded(
              child: renderBody(),
            ),
          ],
        ),
      ),
    );

    // return TitleLayout(
    //   body: Padding(
    //     padding: EdgeInsets.symmetric(horizontal: 16.0),
    //     child: Column(
    //       children: [
    //         Container(height: 18.0),
    //         renderTopText(),
    //         Container(height: 50.0),
    //         Expanded(
    //           child: renderBody(),
    //         ),
    //       ],
    //     ),
    //   ),
    // );
  }
}
