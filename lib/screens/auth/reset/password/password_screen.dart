import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/auth_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/text_field/email_text_field.dart';
import 'package:kins_healthcare/components/text_field/phone_number_text_field.dart';
import 'package:kins_healthcare/controllers/login_controller.dart';
import 'package:kins_healthcare/models/form/find_password_form_model.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HL_IP_0002
 * 주요기능 : 비밀번호 찾기
 */

/// /auth/reset/password \
/// 비밀번호 찾기
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A6636)
///
/// - 화면명 : HL_IP_0001 \
/// - 작성자 : JC \
/// - 작성일 : 2021-01-11
///
/// {@category Screen}
class PasswordScreen extends StatefulWidget {
  @override
  PasswordScreenState createState() => PasswordScreenState();
}

/// /auth/reset/password \
/// 비밀번호 찾기
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A6636)
///
/// - 화면명 : HL_IP_0001 \
/// - 작성자 : JC \
/// - 작성일 : 2021-01-11
///
/// {@category Screen}
class PasswordScreenState extends State<PasswordScreen> {
  final formKey = GlobalKey<FormState>();

  final formModel = FindPasswordFormModel();

  renderEmail() {
    return EmailTextField(
      onSaved: this.formModel.setEmail,
      hasCreateEmail: false,
    );
  }

  renderPhoneNumber() {
    return PhoneNumberTextField(
      onCountryCodeSaved: this.formModel.setNation,
      onPhoneSaved: this.formModel.setPhone,
    );
  }

  renderButton() {
    return PrimaryButton(
      label: '확인',
      onTap: () async {
        if (this.formKey.currentState!.validate()) {
          this.formKey.currentState!.save();

          try {
            await Get.find<LoginController>().findPassword(
              body: formModel.toBody(),
            );

            Get.bottomSheet(
              AuthBottomSheet(
                title: '요청하신 email로\n초기화된 비밀번호를 보내드렸습니다 :)',
                subTitle: '확인 후 비밀번호를 재설정해주세요.',
              ),
            );
          } on DioError catch (e) {
            SnackbarUtils().parseDioErrorMessage(
              error: e,
              title: '비밀번호 찾기 에러',
            );
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        children: [
          Expanded(
            child: ListView(
              physics: ClampingScrollPhysics(),
              children: [
                renderEmail(),
                Container(height: 28.0),
                renderPhoneNumber(),
              ],
            ),
          ),
          renderButton(),
          Container(height: 8.0),
        ],
      ),
    );
  }
}
