import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/auth_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/text_field/phone_number_text_field.dart';
import 'package:kins_healthcare/components/text_field/text_field.dart';
import 'package:kins_healthcare/controllers/login_controller.dart';
import 'package:kins_healthcare/models/form/find_id_form_model.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HL_IP_0001
 * 주요기능 : ID 찾기
 */

/// /auth/reset/email \
/// ID 찾기
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A6636)
///
/// - 화면명 : HL_IP_0001 \
/// - 작성자 : JC \
/// - 작성일 : 2021-01-11
///
/// {@category Screen}
class EmailScreen extends StatefulWidget {
  @override
  EmailScreenState createState() => EmailScreenState();
}

/// /auth/reset/email \
/// ID 찾기
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A6636)
///
/// - 화면명 : HL_IP_0001 \
/// - 작성자 : JC \
/// - 작성일 : 2021-01-11
///
/// {@category Screen}
class EmailScreenState extends State<EmailScreen> {
  final formKey = GlobalKey<FormState>();

  FindIdFormModel formModel = FindIdFormModel();

  renderPhoneNumber() {
    return PhoneNumberTextField(
      onPhoneSaved: this.formModel.setPhone,
      onCountryCodeSaved: this.formModel.setNation,
    );
  }

  renderName() {
    return NameTextField(
      onSavedLastName: this.formModel.setLastname,
      onSavedFirstName: this.formModel.setFirstname,
    );
  }

  renderButton() {
    return PrimaryButton(
      label: '확인',
      onTap: () async {
        if (this.formKey.currentState!.validate()) {
          this.formKey.currentState!.save();

          try {
            await Get.find<LoginController>().findId(body: formModel.toBody());

            Get.bottomSheet(
              AuthBottomSheet(
                title: '요청하신 전화번호로\nID를 보내드렸습니다 :)',
                subTitle: '확인후 아이디를 재설정해주세요!',
              ),
            );
          } on DioError catch (e) {
            SnackbarUtils().parseDioErrorMessage(
              error: e,
              title: '아이디 찾기 에러',
            );
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        children: [
          Expanded(
            child: ListView(
              physics: ClampingScrollPhysics(),
              children: [
                renderName(),
                Container(height: 28.0),
                renderPhoneNumber(),
              ],
            ),
          ),
          renderButton(),
          Container(height: 8.0),
        ],
      ),
    );
  }
}
