import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/base_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/drop_down/country_drop_down_form_field.dart';
import 'package:kins_healthcare/components/drop_down/register_question_dropdown_form_field.dart';
import 'package:kins_healthcare/components/text_field/email_text_field.dart';
import 'package:kins_healthcare/components/text_field/name_text_field.dart';
import 'package:kins_healthcare/components/text_field/password_text_field.dart';
import 'package:kins_healthcare/components/text_field/text_field.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/models/form/email_create_form_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';

/// [EmailScreen] 에서 결과값 리턴해주는 용도
///
/// - 화면명 : HM_0001 \
/// - 작성자 : JC \
/// - 작성일 : 2021-01-11
///
/// {@category ScreenResult}
class EmailScreenResults {
  /// 가입한 이메일
  final String email;

  EmailScreenResults({
    required this.email,
  });
}

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HM_0001
 * 주요기능 : 메일주소 만들기
 */

/// /auth/register/email \
/// 메일주소 만들기
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A5777)
///
/// - 화면명 : HM_0001 \
/// - 작성자 : JC \
/// - 작성일 : 2021-01-11
///
/// {@category Screen}
class EmailScreen extends StatefulWidget {
  @override
  EmailScreenState createState() => EmailScreenState();
}

/// /auth/register/email \
/// 메일주소 만들기
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A5777)
///
/// - 화면명 : HM_0001 \
/// - 작성자 : JC \
/// - 작성일 : 2021-01-11
///
/// {@category Screen}
class EmailScreenState extends State<EmailScreen> {
  final formModel = EmailCreateFormModel();

  final formKey = GlobalKey<FormState>();

  renderName() {
    return NameTextField(
      onSavedFirstName: this.formModel.setFirstname,
      onSavedLastName: this.formModel.setLastname,
    );
  }

  renderEmail() {
    return EmailTextField(
      suffixDomain: true,
      onSaved: this.formModel.setId,
    );
  }

  renderPassword() {
    return PasswordTextField(
      onSaved: this.formModel.setPassword,
    );
  }

  renderCountryDropDownFormField() {
    return Column(
      children: [
        CountryDropDownFormField(
          label: '국가/지역',
          onSaved: this.formModel.setNation,
        ),
      ],
    );
  }

  renderPasswordQuestion() {
    return RegisterQuestionDropDownFormField(
      onSaved: this.formModel.setQuestion,
    );
  }

  renderPasswordQuestionAnswer() {
    return UnderlineTextFormField(
      validator: (val) {
        return null;
      },
      onSaved: this.formModel.setAnswer,
      hintText: '답변',
      labelText: '비밀번호 질문 답변',
    );
  }

  renderBottomSheet() {
    final theme = ThemeFactory.of(context).theme;

    return BaseBottomSheet(
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                '이메일 계정을 만들었어요!',
                style: TextStyle(
                  fontWeight: theme.heavyFontWeight,
                  fontSize: theme.fontSize17,
                ),
              ),
            ],
          ),
          Container(height: 10.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                this.formModel.id!,
                style: TextStyle(
                  fontSize: theme.fontSize14,
                  color: theme.primaryColor,
                ),
              ),
            ],
          ),
          PrimaryButton(
            label: '완료',
            onTap: () {},
          ),
        ],
      ),
    );
  }

  renderButton() {
    return Padding(
      padding: EdgeInsets.only(bottom: 16.0),
      child: PrimaryButton(
        label: '다음',
        onTap: () async {
          if (this.formKey.currentState!.validate()) {
            this.formKey.currentState!.save();

            try {
              await Get.find<UserController>().emailCreate(
                body: this.formModel.toBody(),
              );

              Get.back(
                result: EmailScreenResults(
                  email: this.formModel.id! + '@spinormedia.com',
                ),
              );
            } on DioError catch (e) {
              SnackbarUtils().parseDioErrorMessage(
                error: e,
                title: '회원가입 실패',
              );
            }
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollLayout(
      topBar: BackTopBar(
        title: '메일 만들기',
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Form(
          key: this.formKey,
          child: Column(
            children: [
              Container(height: 40.0),
              renderName(),
              Container(height: 28.0),
              renderEmail(),
              Container(height: 28.0),
              renderPassword(),
              Container(height: 28.0),
              renderCountryDropDownFormField(),
              Container(height: 28.0),
              renderPasswordQuestion(),
              Container(height: 28.0),
              renderPasswordQuestionAnswer(),
              Container(height: 28.0),
              renderButton(),
            ],
          ),
        ),
      ),
    );

    // return TitleLayout(
    //   title: '메일 만들기',
    //   resizeToAvoidBottomInset: false,
    //   body: Padding(
    //     padding: EdgeInsets.symmetric(horizontal: 16.0),
    //     child: Form(
    //       key: this.formKey,
    //       child: Column(
    //         children: [
    //           Expanded(
    //             child: ListView(
    //               physics: ClampingScrollPhysics(),
    //               children: [
    //                 renderName(),
    //                 Container(height: 28.0),
    //                 renderEmail(),
    //                 Container(height: 28.0),
    //                 renderPassword(),
    //                 Container(height: 28.0),
    //                 renderCountryDropDownFormField(),
    //                 Container(height: 28.0),
    //                 renderPasswordQuestion(),
    //                 Container(height: 28.0),
    //                 renderPasswordQuestionAnswer(),
    //                 Container(height: 28.0),
    //               ],
    //             ),
    //           ),
    //           renderButton(),
    //         ],
    //       ),
    //     ),
    //   ),
    // );
  }
}
