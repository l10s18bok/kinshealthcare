import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/avatar/profile_image_avatar.dart';
import 'package:kins_healthcare/components/bottom_sheet/auth_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/controllers/upload_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/title_layout.dart';
import 'package:kins_healthcare/screens/auth/register/register_screen.dart';
import 'package:kins_healthcare/screens/auth/verify/pin/pin_screen.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HR_0004
 * 주요기능 : 프로필 사진 등록
 */

/// /auth/register/profile \
/// 프로필 사진 등록
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A7736)
///
/// - 화면명 : HR_0004
/// - 작성자 : JC
/// - 작성일 : 2021-01-11
///
/// {@category Screen}
class ProfileScreen extends StatefulWidget {
  @override
  ProfileScreenState createState() => ProfileScreenState();
}

/// /auth/register/profile \
/// 프로필 사진 등록
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A7736)
///
/// - 화면명 : HR_0004
/// - 작성자 : JC
/// - 작성일 : 2021-01-11
///
/// {@category Screen}
class ProfileScreenState extends State<ProfileScreen> {
  /// 업로드 파일
  File? image;

  /// 업로드 파일 패스
  String? filePath;

  bool loading = false;

  renderNextButton() {
    return GetBuilder<UploadController>(builder: (c) {
      return Expanded(
        child: Padding(
          padding: EdgeInsets.only(bottom: 16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              PrimaryButton(
                label: '다음',
                onTap: () async {
                  try {
                    setState(() {
                      loading = true;
                    });

                    final backResult = await Get.toNamed(
                      '/auth/verify/pin',
                      arguments: PinScreenArgument(
                        hasSkip: false,
                        isRegister: true,
                        isBack: true,
                      ),
                    );

                    if (backResult != 'OK') return;

                    if (filePath != null) {
                      await Get.find<UserController>()
                          .registerProfileImg(path: filePath!);
                    }

                    if (c.isLoading || Get.find<UserController>().loading) {
                      Get.snackbar('업로드중', '이미지가 업로드중입니다.');
                      return;
                    }

                    await Get.bottomSheet(
                      AuthBottomSheet(
                        title: '축하드려요 🥳\n회원가입이 완료되었습니다.',
                      ),
                    );

                    final step = StepArguments(regStep: RegStep.all);
                    Get.back(result: step);
                  } on DioError catch (e) {
                    SnackbarUtils().parseDioErrorMessage(
                      error: e,
                      title: '이미지 업로드 실패',
                    );

                    setState(() {
                      loading = false;
                    });
                  }
                },
              ),
            ],
          ),
        ),
      );
    });
  }

  renderText() {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      children: [
        Text(
          '킨즈헬스케어에 오신것을 환영합니다!',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: theme.heavyFontWeight,
            fontSize: theme.fontSize15,
            color: Color(0xFF565656),
          ),
        ),
        Container(height: 12),
        Text(
          '가족들이 나를 잘 알아볼수 있도록\n프로필 사진을 등록해보세요.',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: theme.primaryFontWeight,
            fontSize: theme.fontSize12,
            color: Color(0xFF565656),
          ),
        ),
      ],
    );
  }

  renderImagePicker() {
    return ProfileImageAvatar(
      onChange: (File file) {
        setState(() {
          this.image = file;
        });
      },
      onUploadFinish: (path) async {
        setState(() {
          filePath = path;
        });
      },
    );
  }

  renderUpLoadingProgress() {
    final theme = ThemeFactory.of(context).theme;
    return Center(
      child: Container(
        padding: const EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: Colors.black38,
          borderRadius: BorderRadius.circular(6),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            CircularProgressIndicator(
              backgroundColor: Colors.yellow,
            ),
            SizedBox(height: 35),
            Text(
              '잠시만 기다려주세요',
              style: TextStyle(
                color: Colors.white,
                fontSize: theme.fontSize16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return TitleLayout(
      title: '프로필 사진 등록',
      body: Padding(
        padding: EdgeInsets.only(
          left: 16.0,
          right: 16.0,
        ),
        child: Column(
          children: [
            Container(height: 20),
            renderImagePicker(),
            Container(height: 60),
            renderText(),
            renderNextButton(),
          ],
        ),
      ),
    );
  }
}
