import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/layouts.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/user/model/user_profile_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 * 작성일 : 2021-04-07
 * 작성자 : Andy
 * 화면명 : HY_2003
 * 클래스 : ProfileDetailScreen
 * 설명 : 프로필
 */

/// /auth/register/profile/detail \
/// 프로필 스크린
///
/// [Figma 링크]()
///
/// - 화면명 : HY_2003 \
/// - 작성자 : Andy \
/// - 작성일 : 2021-04-07
///
/// {@category Screen}
class ProfileDetailScreen extends StatefulWidget {
  final int userRelNo;
  final String tagUrl;
  final String? heroTag;

  const ProfileDetailScreen({
    Key? key,
    required this.userRelNo,
    required this.tagUrl,
    this.heroTag,
  }) : super(key: key);
  @override
  ProfileDetailScreenState createState() => ProfileDetailScreenState();
}

/// /auth/register/profile/detail \
/// 프로필 스크린
///
/// [Figma 링크]()
///
/// - 화면명 : HY_2003 \
/// - 작성자 : Andy \
/// - 작성일 : 2021-04-07
///
/// {@category Screen}
class ProfileDetailScreenState extends State<ProfileDetailScreen> {
  /// 유저 프로필
  late UserProfileResponse _userProfile;

  @override
  void initState() {
    super.initState();
    _userProfile = UserProfileResponse();
    _getUserProfile();
  }

  _getUserProfile() async {
    try {
      _userProfile = await Get.find<UserController>().getUserProfile(body: UserProfileBody(userRelNo: widget.userRelNo));
      setState(() {});
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  renderBackBtn() {
    return InkWell(
      child: Align(
        alignment: Alignment.centerLeft,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          // height: 44,
          // width: 40,
          child: SvgPicture.asset(
            'assets/svgs/ic/ic_arrowback_common.svg',
            width: 24,
            height: 24,
            color: kinsWhite,
          ),
        ),
      ),
      onTap: () => Get.back(),
    );
  }

  renderTitle() {
    return Center(
      child: Text(
        _userProfile.userName ?? '',
        style: TextStyle(
          color: kinsWhite,
          fontWeight: FontWeight.w700,
          fontSize: ThemeFactory.of(context).theme.fontSize22,
        ),
      ),
    );
  }

  renderTopbar() {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      //alignment: Alignment.center,
      child: Stack(
        alignment: Alignment.center,
        children: [
          renderBackBtn(),
          renderTitle(),
        ],
      ),
    );
  }

  renderRightAnnualZone() {
    String level = _userProfile.userLevel.toString();
    if (_userProfile.userLevel.toString().length == 1) {
      if (_userProfile.userLevel! < 1) {
        level = '01';
      } else if (_userProfile.userLevel! > 12) {
        level = '12';
      } else {
        level = '0' + _userProfile.userLevel.toString();
      }
    }
    return Align(
      alignment: Alignment.topRight,
      child: Padding(
        padding: const EdgeInsets.only(right: 16),
        child: Container(
          width: 48,
          height: 70,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Image.asset('assets/png/profile_vector.png'),
              SvgPicture.asset(
                'assets/svgs/img/tree_level_$level.svg',
                height: 28,
                width: 28,
              ),
            ],
          ),
        ),
      ),
    );
  }

  renderCoverImage() {
    return Container(
      color: Color.fromRGBO(0, 0, 0, 0.80),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: renderHeroImage(),
    );
  }

  Widget renderHeroImage() {
    final heroTag = widget.heroTag;
    if (heroTag == null) return renderImage();

    return Hero(tag: heroTag, child: renderImage());
  }

  Widget renderImage() {
    final indicator = LinearProgressIndicator(backgroundColor: Colors.grey);

    return CachedNetworkImage(
      imageUrl: widget.tagUrl,
      placeholder: (context, url) => indicator,
      fit: BoxFit.fitWidth,
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }

  renderImageButton({required String imgPath, required String btnName}) {
    return Column(
      children: [
        SizedBox(height: 14),
        Image.asset(
          imgPath,
          color: Colors.white,
          width: 64,
          height: 64,
        ),
        SizedBox(height: 10),
        Text(
          btnName,
          style: TextStyle(
            color: kinsWhite,
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize15,
          ),
        ),
        SizedBox(height: 16),
      ],
    );
  }

  renderVerticalDivider() {
    return Container(
      width: 1,
      height: 70,
      color: Colors.white,
    );
  }

  renderRowImageButton() {
    return Container(
      height: 126,
      color: Color.fromRGBO(0, 0, 0, 0.32),
      child: Row(
        children: [
          Expanded(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  print('allow');
                },
                child: renderImageButton(imgPath: 'assets/svgs/img/profile_allow.png', btnName: '소개 허락하기'),
              ),
            ),
          ),
          renderVerticalDivider(),
          Expanded(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  print('reject');
                },
                child: renderImageButton(imgPath: 'assets/svgs/img/profile_reject.png', btnName: '소개 거절하기'),
              ),
            ),
          ),
        ],
      ),
    );
  }

  renderBottomBox() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Column(
        children: [
          // renderRowImageButton(),
          InkWell(
            onTap: () {
              Get.toNamed('/mykins/family/list', arguments: UserProfileBody(userRelNo: widget.userRelNo));
            },
            child: Container(
              height: 44,
              color: Color.fromRGBO(0, 0, 0, 0.5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '함께 아는 가족',
                    style: TextStyle(
                      color: kinsWhite,
                      fontSize: ThemeFactory.of(context).theme.fontSize15,
                    ),
                  ),
                  SizedBox(width: 10),
                  Text(
                    '${_userProfile.count ?? 0} 명',
                    style: TextStyle(
                      color: Color(0xFFF29061),
                      fontSize: ThemeFactory.of(context).theme.fontSize15,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Stack(
        children: [
          renderCoverImage(),
          renderRightAnnualZone(),
          Column(
            children: [
              SizedBox(height: 25),
              renderTopbar(),
              Spacer(),
              renderBottomBox(),
            ],
          ),
        ],
      ),
    );
  }
}
