import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-02-16
 작성자 : Victor
 화면명 : HR_0006
 클래스 : RegisterCompleteBottomSheet
 경로 :
 설명 : 회원가입 완료를 표시하는 하단 팝업창
*/

// TODO 이거 왜 여기있죠? 적합한 위치로 옮기거나 네이밍을 변경해주세요
class RegisterCompleteBottomSheet extends StatefulWidget {
  @override
  RegisterCompleteBottomSheetState createState() =>
      RegisterCompleteBottomSheetState();
}

class RegisterCompleteBottomSheetState
    extends State<RegisterCompleteBottomSheet> {
  final padding = 16.0;

  //시안 화면 크기 : 360*750
  final sw = 360;
  final sh = 750;

  var safeWidth;
  var safeHeight;
  var widthRatio;
  var heightRatio;

  Widget decoLeft() {
    //left deco (lrtb) : 7.99 226.45 15.78 135.91
    final ldw = (sw - 7.99 - 226.45) * widthRatio;
    final ldh = (sh - 15.78 - 135.91 - 528) * heightRatio;

    return Positioned(
      top: 0,
      left: 0,
      width: ldw,
      height: ldh,
      child: SvgPicture.asset(
        'lib/screens/spinor_test/victor/sample_image/deco_left.svg',
      ),
    );
  }

  Widget decoRight() {
    //right deco (lrtb) : 278.29 7.05 11.04 143.73
    final rdw = (sw - 278.29 - 7.05) * widthRatio;
    final rdh = (sh - 11.04 - 143.73 - 528) * heightRatio;

    return Positioned(
      top: 0,
      right: 0,
      width: rdw,
      height: rdh,
      child: SvgPicture.asset(
        'lib/screens/spinor_test/victor/sample_image/deco_right.svg',
      ),
    );
  }

  Widget centerText() {
    //text : (top)55, 192 * 48
    final textTop = 55 * heightRatio - padding;
    final tw = 192 * widthRatio as double;
    final th = 48 * heightRatio as double;

    final theme = ThemeFactory.of(context).theme;

    final titleStyle = TextStyle(
      color: Colors.black,
      fontWeight: theme.heavyFontWeight,
    );

    return Positioned(
      top: textTop,
      width: tw,
      height: th,
      child: FittedBox(
        fit: BoxFit.fitWidth,
        child: Text(
          '환영해요!\n회원가입이 완료되었어요 :)',
          textAlign: TextAlign.center,
          style: titleStyle,
        ),
      ),
    );
  }

  Widget confirmButton() {
    //button : 328 * 55
    final buttonHeight = heightRatio * 55;

    return Positioned(
      left: 0,
      right: 0,
      bottom: 0,
      height: buttonHeight,
      child: PrimaryButton(
        label: '확인',
        onTap: () {
          Get.back();
          // TODO : 액션 추가
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    safeWidth = MediaQuery.of(context).size.width -
        MediaQuery.of(context).padding.left -
        MediaQuery.of(context).padding.right;
    safeHeight = MediaQuery.of(context).size.height -
        MediaQuery.of(context).padding.top -
        MediaQuery.of(context).padding.bottom;

    widthRatio = safeWidth / sw;
    heightRatio = safeHeight / sh;

    //원이미지 크기 : 360*222
    final popupHeight = heightRatio * 222;

    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(26.0),
          topLeft: Radius.circular(26.0),
        ),
      ),
      child: SafeArea(
        bottom: true,
        child: Container(
          height: popupHeight,
          child: Padding(
            padding: EdgeInsets.all(padding),
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: [
                decoLeft(),
                decoRight(),
                centerText(),
                confirmButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
