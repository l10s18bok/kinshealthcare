import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/text_field/phone_number_text_field.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/screens/auth/verify/phone/phone_screen.dart';

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HR_0002-3
 * 변경내용 : 2021.04.26 Andy => 이메일 인증후 이화면 진입(X) 바로 폰번호인증 화면(verify => PhoneScreen)으로 점프
 * 주요기능 : 회원가입 핸드폰 번호 입력
 */

/// /auth/register/phone \
/// 회원가입 핸드폰 번호 입력
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A6815)
///
/// - 화면명 : HR_0002-3 \
/// - 작성자 : JC \
/// - 작성일 : 2021-01-11
///
/// {@category Screen}
class PhoneScreen extends StatefulWidget {
  @override
  PhoneScreenState createState() => PhoneScreenState();
}

/// /auth/register/phone \
/// 회원가입 핸드폰 번호 입력
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A6815)
///
/// - 화면명 : HR_0002-3 \
/// - 작성자 : JC \
/// - 작성일 : 2021-01-11
///
/// {@category Screen}
class PhoneScreenState extends State<PhoneScreen> {
  final formKey = GlobalKey<FormState>();

  /// 핸드폰 번호
  String? phone;

  /// 나라 코드
  String? countryCode;

  renderPhoneNumber() {
    return Expanded(
      child: PhoneNumberTextField(
        onPhoneSaved: (String? value) {
          setState(() {
            this.phone = value;
          });
        },
        onCountryCodeSaved: (value) {
          setState(() {
            this.countryCode = value;
          });
        },
      ),
    );
  }

  renderNextButton() {
    return PrimaryButton(
      label: '다음',
      onTap: () async {
        if (this.formKey.currentState!.validate()) {
          this.formKey.currentState!.save();

          Get.toNamed(
            '/auth/verify/phone/signUp', // 폰번호 인증
            arguments: PhoneScreenArguments(
              phone: this.phone!,
            ),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(
        title: '회원가입',
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Form(
          key: this.formKey,
          child: Column(
            children: [
              Container(height: 40.0),
              renderPhoneNumber(),
              renderNextButton(),
              Container(height: 8.0),
            ],
          ),
        ),
      ),
    );

    // return TitleLayout(
    //   title: '회원가입',
    //   body: Padding(
    //     padding: EdgeInsets.symmetric(horizontal: 16.0),
    //     child: Form(
    //       key: this.formKey,
    //       child: Column(
    //         children: [
    //           renderPhoneNumber(),
    //           renderNextButton(),
    //           Container(height: 8.0),
    //         ],
    //       ),
    //     ),
    //   ),
    // );
  }
}
