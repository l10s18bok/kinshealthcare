import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/text_field/account_type_radio_form_field.dart';
import 'package:kins_healthcare/components/text_field/dob_text_form_field.dart';
import 'package:kins_healthcare/components/text_field/email_text_field.dart';
import 'package:kins_healthcare/components/text_field/password_text_field.dart';
import 'package:kins_healthcare/components/text_field/phone_number_text_field.dart';
import 'package:kins_healthcare/components/text_field/sex_radio_form_field.dart';
import 'package:kins_healthcare/components/text_field/text_field.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/models/form/register_form_model.dart';
import 'package:kins_healthcare/screens/auth/verify/email/email_screen.dart';
import 'package:kins_healthcare/screens/my_kins/family/family_screen.dart';
import 'package:kins_healthcare/utils/validator_utils.dart';

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HR_0001
 * 주요기능 : 회원가입
 * 2021.04.09 자녀회원가입 공통 사용(Andy 추가)
   title - adult || kid
   default - adult
 */

class StepArguments {
  final RegStep regStep;

  StepArguments({required this.regStep});
}

class RegisterScreen extends StatefulWidget {
  final String title;

  const RegisterScreen({
    Key? key,
    required this.title,
  }) : super(key: key);
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

enum SignUpType {
  adult,
  kid,
}

enum RegStep {
  all,
  email,
  phone,
  profile,
}

class _RegisterScreenState extends State<RegisterScreen> {
  late SignUpType signUpType;

  final formKey = GlobalKey<FormState>();

  //male, female
  String sex = 'male';

  // true - 의료인 false - 낫 의료인
  bool isMedical = false;

  final RegisterFormModel formModel = RegisterFormModel();

  @override
  void initState() {
    super.initState();

    signUpType = SignUpType.adult;
    if (widget.title == 'kid') {
      signUpType = SignUpType.kid;
    }
  }

  renderName() {
    return NameTextField(
      onSavedLastName: this.formModel.setLastname,
      onSavedFirstName: this.formModel.setFirstname,
    );
  }

  renderDoB() {
    return DobTextFormField(
      // 생년월일
      onSaved: this.formModel.setBirth,
      validator: Validators.dobValidator,
    );
  }

  renderSex() {
    return SexRadioFormField(
      onSaved: this.formModel.setGender,
      validator: Validators.sexValidator,
    );
  }

  renderPhoneNumber() {
    return PhoneNumberTextField(
      onCountryCodeSaved: this.formModel.setNation,
      onPhoneSaved: this.formModel.setPhone,
    );
  }

  renderEmail() {
    return EmailTextField(
      onSaved: this.formModel.setId,
      hasCreateEmail: false, // '이메일 만들기' 버튼 생성 =  true
    );
  }

  renderNextButton() {
    return Expanded(
      child: PrimaryButton(
        label: '다음',
        onTap: () async {
          if (this.formKey.currentState!.validate()) {
            this.formKey.currentState!.save();
            int? seq;

            ///임시발급 아이디
            try {
              /// 일반회원 등록
              // if (formModel.mediYn == null) {
              //   formModel.setMediYn('N');   /// 자녀회원등록일때 의료종사자 = 'N'
              // }

              if (signUpType == SignUpType.adult) {
                final c = Get.find<UserController>();
                final response = await c.register(
                  body: this.formModel.toBody(),
                );
                seq = response.data;
              } else {
                /// 자녀회원 등록
                final c = Get.find<MykinsController>();
                final response = await c.registerKids(
                  body: this.formModel.toChildBody(),
                );
                seq = response.data;
              }

              final step = await Get.toNamed(
                '/auth/verify/email',
                arguments: EmailScreenArguments(
                  seq: seq ?? 0,
                  password: this.formModel.password!,
                  email: this.formModel.id!,
                  phone: this.formModel.phone!,
                  signUpType: signUpType,
                ),
              );
              if (step != null) {
                if (step.regStep == RegStep.all && signUpType == SignUpType.adult) {
                  Get.offAllNamed(
                    '/my-kins/family',
                    arguments: FamilyScreenArguments(hasHomeButton: true),
                  );
                } else if (step.regStep == RegStep.all && signUpType == SignUpType.kid) Get.back();
              }
            } on DioError catch (e) {
              Get.snackbar(
                '회원가입 실패',
                e.response!.data['resultMsg'],
              );
            }
          }
        },
      ),
    );
  }

  renderMedical() {
    return AccountTypeRadioFormField(
      onSaved: this.formModel.setMediYn,
    );
  }

  renderPassword() {
    return PasswordTextField(
      onSaved: this.formModel.setPassword,
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollLayout(
      topBar: BackTopBar(
        //2021.04.09 Andy 추가
        title: signUpType == SignUpType.adult ? '회원가입' : '자녀 회원가입',
      ),
      body: Padding(
        padding: EdgeInsets.only(
          left: 16.0,
          right: 16.0,
          top: 40.0,
          bottom: 16.0,
        ),
        child: Form(
          key: this.formKey,
          child: Column(
            children: [
              renderName(),
              Container(height: 28),
              renderDoB(),
              Container(height: 28),
              renderSex(),
              Container(height: 28),
              renderPhoneNumber(),
              Container(height: 28),
              renderEmail(),
              Container(height: 28),
              renderPassword(),
              Container(height: 28),
              //2021.04.09 Andy 추가
              signUpType == SignUpType.adult ? renderMedical() : Container(),
              Container(height: 28),
              renderNextButton(),
              // Expanded(
              //   child: ListView(
              //     physics: ClampingScrollPhysics(),
              //     children: [
              //       renderName(),
              //       Container(height: 28),
              //       renderDoB(),
              //       Container(height: 28),
              //       renderSex(),
              //       Container(height: 28),
              //       renderPhoneNumber(),
              //       Container(height: 28),
              //       renderEmail(),
              //       Container(height: 28),
              //       renderPassword(),
              //       Container(height: 28),
              //       renderMedical(),
              //       Container(height: 28),
              //     ],
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );

    // return TitleLayout(
    //   title: '회원가입',
    //   resizeToAvoidBottomInset: false,
    //   body: Padding(
    //     padding: EdgeInsets.only(
    //       left: 16.0,
    //       right: 16.0,
    //     ),
    //     child: Form(
    //       key: this.formKey,
    //       child: Column(
    //         children: [
    //           Expanded(
    //             child: ListView(
    //               physics: ClampingScrollPhysics(),
    //               children: [
    //                 renderName(),
    //                 Container(height: 28),
    //                 renderDoB(),
    //                 Container(height: 28),
    //                 renderSex(),
    //                 Container(height: 28),
    //                 renderPhoneNumber(),
    //                 Container(height: 28),
    //                 renderEmail(),
    //                 Container(height: 28),
    //                 renderPassword(),
    //                 Container(height: 28),
    //                 renderMedical(),
    //                 Container(height: 28),
    //               ],
    //             ),
    //           ),
    //           renderNextButton(),
    //         ],
    //       ),
    //     ),
    //   ),
    // );
  }
}
