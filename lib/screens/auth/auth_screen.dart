import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/text_field/text_field.dart';
import 'package:kins_healthcare/controllers/auth_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/models/form/login_form_model.dart';
import 'package:kins_healthcare/screens/splash/test_splash_screen.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HL_0001
 * 주요기능 : 로그인
 */
class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final formKey = GlobalKey<FormState>();

  final formModel = LoginFormModel();

  bool isFormLoading = false;

  renderLogo() {
    return GestureDetector(
      onDoubleTap: () {
        //TODO 프로덕션에서 지우세요!
        final isProd = bool.fromEnvironment('dart.vm.product');

        if (!isProd) {
          Get.to(TestSplashScreen());
        }
      },
      child: Center(
        child: SvgPicture.asset(
          'assets/svgs/img/logo_splash.svg',
        ),
      ),
    );
  }

  renderAuthFields() {
    return Column(
      children: [
        EmailTextField(
          isBlack: false,
          onSaved: formModel.setEmail,
        ),
        Container(height: 28),
        PasswordTextField(
          isBlack: false,
          onSaved: formModel.setPassword,
        ),
      ],
    );
  }

  renderLoginButton() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      children: [
        Expanded(
          child: Material(
            color: Colors.white,
            borderRadius: BorderRadius.circular(6.0),
            child: InkWell(
              onTap: this.isFormLoading
                  ? null
                  : () async {
                      if (this.formKey.currentState!.validate()) {
                        setState(() {
                          this.isFormLoading = true;
                        });

                        this.formKey.currentState!.save();

                        try {
                          await Get.find<AuthController>().loginUser(
                            email: formModel.email,
                            password: formModel.password,
                          );

                          await Get.find<UserController>().deviceRegist();

                          setState(() {
                            this.isFormLoading = false;
                          });
                          if (Get.arguments == true) {
                            Get.toNamed('/coach-mark/main-home');
                          } else {
                            Get.toNamed('/main-home');
                          }
                        } on DioError catch (e) {
                          if (e.response != null) {
                            Get.snackbar(
                              '로그인 실패',
                              e.response!.data['resultMsg'],
                              backgroundColor: Colors.white,
                            );
                          }
                          setState(() {
                            this.isFormLoading = false;
                          });
                        }
                      }
                    },
              child: Container(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 18.0),
                  child: Center(
                    child: this.isFormLoading
                        ? SizedBox(
                            width: 22.0,
                            height: 22.0,
                            child: CircularProgressIndicator(
                              valueColor:
                                  AlwaysStoppedAnimation(theme.primaryColor),
                            ),
                          )
                        : Text(
                            '로그인',
                            style: TextStyle(
                              color: theme.primaryColor,
                            ),
                          ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void hideKeyboard(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      FocusManager.instance.primaryFocus!.unfocus();
    }
  }

  void hideKeyboardForAndroid() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  renderRegisterButton(String title, GestureTapCallback onTap) {
    final theme = ThemeFactory.of(context).theme;

    return GestureDetector(
      onTap: onTap,
      child: Container(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
          child: Text(
            title,
            style: TextStyle(
              decoration: TextDecoration.underline,
              color: Colors.white,
              fontSize: theme.fontSize14,
            ),
          ),
        ),
      ),
    );
  }

  renderBottom() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        renderLoginButton(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            renderRegisterButton(
                '아이디 찾기', () => Get.toNamed('/auth/reset/email')),
            renderRegisterButton(
                '비밀번호 찾기', () => Get.toNamed('/auth/reset/password')),
            renderRegisterButton('회원가입', () => Get.toNamed('/auth/register')),
          ],
        ),
        renderCopyrights(),
        Container(height: 16.0),
      ],
    );
  }

  renderCopyrights() {
    final theme = ThemeFactory.of(context).theme;

    final style = TextStyle(
      fontSize: theme.fontSize10,
      color: Colors.white,
    );

    return Column(
      children: [
        Text(
          'All rights reserved hello world Spinormedia',
          style: style,
        ),
        Text(
          '2020 spinor@gmail.com',
          style: style,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    final size = MediaQuery.of(context).size;

    print(size.height);

    return DefaultLayout(
      backgroundColor: theme.primaryColor,
      // resizeToAvoidBottomInset: false,
      body: GestureDetector(
        onTap: () {
          if (Platform.isIOS) hideKeyboard(context);
          if (Platform.isAndroid) hideKeyboardForAndroid();
        },
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                Expanded(child: LayoutBuilder(
                  builder: (_, constraints) {
                    return SingleChildScrollView(
                      child: ConstrainedBox(
                        constraints: constraints.copyWith(
                          minHeight: constraints.maxHeight,
                          maxHeight: double.infinity,
                        ),
                        child: IntrinsicHeight(
                          child: Column(
                            children: [
                              Expanded(
                                flex: 4,
                                child: ConstrainedBox(
                                  constraints: BoxConstraints(
                                    minHeight: 32.0,
                                  ),
                                ),
                              ),
                              renderLogo(),
                              Container(height: size.height * 0.1718),
                              renderAuthFields(),
                              Container(height: 40.0),
                              Spacer(),
                              renderBottom(),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
