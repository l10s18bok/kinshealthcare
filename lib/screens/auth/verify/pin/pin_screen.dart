import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/pin_entry/custom_pin_entry.dart';
import 'package:kins_healthcare/controllers/cert_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/layouts.dart';
import 'package:kins_healthcare/screens/my_kins/reset/pin/reset_pin_screen.dart';
import 'package:kins_healthcare/services/cert/model/simple_password_certify_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class PinScreenArgument {
  /// 건너뛰기 버튼을 렌더링 할지 여부
  final bool hasSkip;

  /// Pin 을 새로 등록하는 경우일때
  /// 설명 글귀가 바뀜
  final bool isRegister;

  /// 다음 라우트 (/register)
  final String nextRoute;

  /// 호출화면으로 되돌아가기  2021/03/26 Andy 추가
  final bool isBack;

  PinScreenArgument({
    this.hasSkip = true,
    this.isRegister = true,
    this.isBack = false, // 2021/03/26 Andy 추가
    this.nextRoute = '/auth/verify/email',
  });
}

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HR_0005
 * 주요기능 : 간편 비밀번호
 */

class PinScreen extends StatefulWidget {
  @override
  _PinScreenState createState() => _PinScreenState();
}

class _PinScreenState extends State<PinScreen> {
  String password = '';
  String pin = '';
  String checkPin = ''; // 2021.05.03 Andy 추가

  bool hasSkip = true;
  bool isRegister = true;
  bool isBack = false; // 2021/03/26 Andy 추가
  InputState? curInputState; // 2021.05.03 Andy 추가
  TextEditingController textEditingController = TextEditingController(); // 2021.05.03 Andy 추가
  final FocusNode focus = FocusNode(); // 2021.05.07 Andy 추가
  late String nextRoute;

  @override
  void initState() {
    super.initState();

    hasSkip = true;
    isRegister = true;

    final args = Get.arguments;

    if (args != null && args is PinScreenArgument) {
      hasSkip = args.hasSkip;
      isRegister = args.isRegister;
      isBack = args.isBack;
      nextRoute = args.nextRoute;
    } else {
      throw Exception('required arguments not passed');
    }
  }

  renderTitle() {
    final theme = ThemeFactory.of(context).theme;

    if (isRegister) {
      return Column(
        children: [
          Text(
            '간편한 이용을 위해',
            style: TextStyle(
              fontSize: theme.fontSize13,
              color: Color(0xFF393939),
            ),
          ),
          Container(
            height: 10.0,
          ),
          Text(
            '간편 비밀번호를 등록해 보시겠어요?',
            style: TextStyle(
              fontSize: theme.fontSize15,
              color: Color(0xFF393939),
              fontWeight: theme.heavyFontWeight,
            ),
          ),
        ],
      );
    } else {
      return Text(
        '간편 비밀번호를 입력해주세요',
        style: TextStyle(
          fontSize: theme.fontSize15,
          color: Color(0xFF393939),
          fontWeight: theme.heavyFontWeight,
        ),
      );
    }
  }

  renderCharacter({
    required bool isActive,
  }) {
    final color = Color(0xFFCACAE7);

    final activeIcon = Icon(
      Icons.check_circle,
      color: color,
    );
    final inactiveIcon = Icon(
      Icons.circle,
      color: color,
    );

    return Expanded(
      child: Column(
        children: [
          isActive ? activeIcon : inactiveIcon,
          Container(
            height: 12.0,
          ),
          Container(
            color: color,
            height: 1.0,
            width: 44.0,
          ),
        ],
      ),
    );
  }

  toNextRoute() async {
    try {
      await Get.find<CertController>().simplepwCertify(
        body: SimplePwCertifyBody(
          simplePw: this.pin,
        ),
      );

      isBack ? Get.back(result: 'OK') : Get.offNamed(this.nextRoute);
    } on DioError catch (e) {
      final msg = e.response!.data['resultMsg'];
      Get.snackbar('간편 비밀번호 에러.', msg);
      WidgetsBinding.instance!.addPostFrameCallback((_) {
        textEditingController.clear();
        pin = '';
        FocusScope.of(context).requestFocus(focus);
      });
      return;
    }
  }

  regPin() async {
    if (checkPin == pin) {
      try {
        await Get.find<UserController>().createPinCode(pin: pin);
        Get.back(result: 'OK');
      } on DioError catch (e) {
        Get.snackbar(
          '간편 비밀번호 저장 실패',
          e.response!.data['resultMsg'],
        );
      }
    } else {
      Get.snackbar('간편 비밀번호가 다릅니다.', '다시 입력해주세요.');
    }
  }

  renderPinCodeField() {
    String normalTxt = '';
    String boldTxt = '';
    if (isRegister) {
      if (curInputState == null) {
        normalTxt = '간편한 이용을 위해';
        boldTxt = '간편 비밀번호를 등록 해보시겠어요?';
      } else if (curInputState == InputState.inputChangePin) {
        normalTxt = '';
        boldTxt = '간편 비밀번호를 한 번 더 입력하세요';
      }
    }
    return Expanded(
      child: CustomPinEntry(
        textEditingController: textEditingController,
        focus: focus,
        normalText: normalTxt,
        boldText: boldTxt,
        onChanged: (val) {
          pin = val;
          if (this.pin.length == 6) {
            //2021.04.20 Andy 추가 ==> 간편비번 확인
            if (!isRegister)
              toNextRoute();
            else {
              if (curInputState == null) {
                checkPin = pin;
                curInputState = InputState.inputChangePin;
                WidgetsBinding.instance!.addPostFrameCallback((_) {
                  textEditingController.clear();
                  pin = '';
                  FocusScope.of(context).requestFocus(focus);
                });
                setState(() {});
                return;
              }
              regPin();
            }
          }
        },
      ),
    );
  }

  renderNextButton() {
    if (hasSkip) {
      return PrimaryButton(
        label: '다음',
        onTap: () async {
          /// TODO Production 에서 지우기
          toNextRoute();
          //return;

          if (this.pin.length == 0) {
            Get.snackbar(
              '간편 비밀번호를 입력해주세요',
              '간편 비밀번호 6자리를 입력해주세요',
            );
            return;
          }

          try {
            if (this.isRegister) {
              await Get.find<UserController>().createPinCode(pin: pin);
            } else {
              await Get.find<UserController>();
            }

            Get.toNamed(this.nextRoute);
          } on DioError catch (e) {
            Get.snackbar(
              '간편 비밀번호 저장 실패',
              e.response!.data['resultMsg'],
            );
          }
        },
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return TitleLayout(
      title: '간편 비밀번호',
      // actionText: '건너뛰기',
      // onActionTap: () {
      //   Get.toNamed('/auth/verify/email');
      // },
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            renderPinCodeField(),
            renderNextButton(),
            Container(height: 8.0),
          ],
        ),
      ),
    );
  }
}
