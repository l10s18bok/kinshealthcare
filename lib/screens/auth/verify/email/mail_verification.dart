import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/text_field/titled_input_text.dart';
import 'package:kins_healthcare/controllers/comm_controller.dart';
import 'package:kins_healthcare/controllers/widget/text_timer_controller.dart';
import 'package:kins_healthcare/layouts/layouts.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/ok_bottom_sheet.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/validator_utils.dart';

/*
 작성일 : 2021-02-17
 작성자 : Victor
 화면명 : HL_IP_0002_EM
 클래스 : MailVerification
 경로 : /auth/verify/email
 설명 : 이메일 인증용 화면
*/

class EmailVerification extends StatefulWidget {
  @override
  _EmailVerification createState() => _EmailVerification();
}

class _EmailVerification extends State<EmailVerification> {
  late TextTimerController controller;
  late DateTime startTime;

  final controllerTag = 'MailVerificationState';
  final emailInputTextKey = 'EV_emailInput';
  final authInputTextKey = 'EV_authInput';
  final successCode = 'SUCCESS';
  late CommController commController;

  final TextEditingController textEditingController1 = TextEditingController();
  final TextEditingController textEditingController2 = TextEditingController();

  @override
  void initState() {
    super.initState();
    commController = Get.find<CommController>();
  }

  @override
  Widget build(BuildContext context) {
    initString();

    return TitleLayout(
      title: '휴대폰 인증',
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            emailInput(),
            authInput(),
          ],
        ),
      ),
      bottomNavigationBar: nextButton(),
    );
  }

  @override
  void dispose() {
    controller.stopTimer();

    super.dispose();
  }

  Widget nextButton() {
    return SafeArea(
      child: Container(
        width: double.infinity,
        height: 55,
        padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
        alignment: Alignment.center,
        child: PrimaryButton(
          label: '인증하기',
          onTap: () async {
            if (textEditingController2.text.length != 6) {
              final sheet = OkBottomSheet(title: '입력 오류', content: '인증번호를 제대로 입력해주세요.');
              Get.bottomSheet(sheet);
              return;
            }

            if (remainTime <= 0) {
              final sheet = OkBottomSheet(title: '시간 초과', content: '인증번호를 다시 받아주세요.');
              Get.bottomSheet(sheet);
              return;
            }

            try {
              final model = await commController.postOkEmail(
                  textEditingController1.text, textEditingController2.text);

              if (model.resultCode == successCode) {
                controller.stopTimer();
                controller.setString(authInputTextKey, '');

                //TODO : 다음 화면 이동
                //이하 임시코드
                final sheet = OkBottomSheet(title: '확인', content: '인증 완료');
                Get.bottomSheet(sheet);
              }
            } on DioError catch (e) {
              final String message = e.response!.data['resultMsg'];
              final sheet = OkBottomSheet(title: '서버통신 에러', content: message);
              Get.bottomSheet(sheet);
            }
          },
        ),
      ),
    );
  }

  int get remainTime => startTime.difference(DateTime.now()).inSeconds < 0
      ? 0
      : startTime.difference(DateTime.now()).inSeconds;

  String remainTimeString() {
    var format = NumberFormat('00');

    var sec = remainTime;

    return '남은시간 ${sec ~/ 60}:${format.format(sec % 60)}';
  }

  Widget emailInput() {
    var theme = ThemeFactory.of(context).theme;

    return Obx(() => TitledInputText(
          controller: textEditingController1,
          inputType: TextInputType.emailAddress,
          titleText: '이메일 (ID로 사용예정)',
          actionText: controller.getString(emailInputTextKey),
          actionTextColor: theme.primaryBlueColor,
          actionTappedFunc: (text) async {
            bool emailValid = Validators.isEmail(text);

            if (emailValid) {
              try {
                final model = await commController.postVerifyEmail(text);

                if (model.resultCode == successCode) {
                  controller.setString(emailInputTextKey, '인증번호 재전송');
                  startTime = DateTime.now().add(Duration(seconds: 180));

                  controller.startTimer((timer) {
                    var c = Get.find<TextTimerController>(tag: controllerTag);
                    c.setString(authInputTextKey, remainTimeString());
                  });
                }
              } on DioError catch (e) {
                final message = e.response!.data['resultMsg'];
                final sheet = OkBottomSheet(title: '서버통신 에러', content: message);
                Get.bottomSheet(sheet);
              }
            } else {
              final sheet = OkBottomSheet(title: '입력 오류', content: '이메일이 맞는지 확인해주세요.');
              Get.bottomSheet(sheet);
            }
          },
        ));
  }

  Widget authInput() {
    var theme = ThemeFactory.of(context).theme;

    return Obx(() => TitledInputText(
          controller: textEditingController2,
          inputType: TextInputType.number,
          obscureText: true,
          maxLength: 6,
          titleText: '인증번호 확인',
          hintText: '6자리 입력',
          actionText: controller.getString(authInputTextKey),
          actionTextColor: theme.primaryRedColor,
          actionTappedFunc: (_) {},
        ));
  }

  void initString() {
    if (Get.isRegistered<TextTimerController>(tag: controllerTag)) {
      controller = Get.find<TextTimerController>(tag: controllerTag);
    } else {
      controller = Get.put(TextTimerController(), tag: controllerTag)!;
    }

    controller.setString(emailInputTextKey, '인증번호 전송');
    controller.setString(authInputTextKey, '');
  }
}
