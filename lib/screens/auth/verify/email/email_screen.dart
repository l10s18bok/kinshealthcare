import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/pin_entry/auth_pin_entry.dart';
import 'package:kins_healthcare/components/text_field/verification_code_text_field.dart';
import 'package:kins_healthcare/components/text_field/verification_target_text_field.dart';
import 'package:kins_healthcare/controllers/comm_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/title_layout.dart';
import 'package:kins_healthcare/models/form/email_verify_form_model.dart';
import 'package:kins_healthcare/screens/auth/register/register_screen.dart';
import 'package:kins_healthcare/screens/auth/verify/phone/phone_screen.dart';
import 'package:kins_healthcare/services/comm/model/cert_request_email_model.dart';

/// [EmailScreen] 에 전달할 정보
///
/// - 작성자 : JC
/// - 작성일 : 2021-05-02
///
/// {@category ScreenArgument}
class EmailScreenArguments {
  /// 이메일 (현재 스크린에서 검증)
  /// id = 임시아이디
  final int seq;
  final String password;
  final String email;

  /// 전화번호 (다음 스크린으로 넘김)
  final String phone;
  final SignUpType signUpType;

  EmailScreenArguments({
    required this.seq,
    required this.password,
    required this.email,
    required this.phone,
    required this.signUpType,
  });
}

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HR_0002
 * 주요기능 : 이메일 인증하기
 */

/// /auth/verify/email \
/// 이메일 인증하기
///
/// [피그마](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A6787)
///
/// - 화면명 : HR_0002
/// - 작성자 : JC
/// - 작성일 : 2021-05-02
///
/// {@category ScreenArgument}
class EmailScreen extends StatefulWidget {
  @override
  EmailScreenState createState() => EmailScreenState();
}

/// /auth/verify/email \
/// 이메일 인증하기
///
/// [피그마](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=0%3A6787)
///
/// - 화면명 : HR_0002
/// - 작성자 : JC
/// - 작성일 : 2021-05-02
///
/// {@category ScreenArgument}
class EmailScreenState extends State<EmailScreen> {
  final formKey = GlobalKey<FormState>();

  final formModel = EmailVerifyFormModel();

  late int seq;
  late String password;
  late String targetEmail;
  late String targetPhone;

  late int countDown;

  late DateTime countDownEnd;

  late EmailScreenArguments args;

  late SignUpType signUpType;

  Timer? timer;

  @override
  void initState() {
    super.initState();

    args = Get.arguments;

    if (args is EmailScreenArguments) {
      seq = args.seq;
      targetEmail = args.email;
      password = args.password;
      targetPhone = args.phone;
      signUpType = args.signUpType;
    } else {
      throw Exception('아규먼트가 제공되지 않았습니다.');
    }

    resendVerificationCode();
    resetCount();
  }

  renderPinCodeField() {
    return Expanded(
      child: AuthPinEntry(
        normalText: '인증번호가 발송되었습니다.',
        boldText: '인증번호 6자리를 입력해주세요.',
      ),
    );
  }

  renderNextButton() {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          PrimaryButton(
            label: '인증하기',
            onTap: () async {
              if (this.formKey.currentState!.validate()) {
                this.formKey.currentState!.save();
                this.formModel.setSeq(seq);
                if (this.countDown == 0) {
                  Get.snackbar(
                    '인증번호 만료',
                    '인증번호가 만료되었습니다. 인증번호를 재전송 해주세요!',
                  );
                  return;
                }

                try {
                  await Get.find<UserController>().regOkEmail(
                    body: this.formModel.toRegBody(),
                  );
                  // if (signUpType == SignUpType.adult) {
                  //   await Get.find<UserController>().regOkEmail(
                  //     body: this.formModel.toRegBody(),
                  //   );
                  // } else {
                  //   await Get.find<UserController>().regChildOkEmail(
                  //     body: this.formModel.toRegBody(),
                  //   );
                  // }
                  timer!.cancel();

                  //Get.toNamed('/auth/register/phone');  //이전 라우팅
                  //2021.04.26 Andy 수정 신규 라우팅
                  final step = await Get.toNamed(
                    '/auth/verify/phone/signUp', // 폰번호 인증
                    arguments: PhoneScreenArguments(
                      seq: seq,
                      email: targetEmail,
                      password: password,
                      phone: targetPhone,
                      signUpType: args.signUpType,
                    ),
                  );

                  if (step != null) Get.back(result: step);
                } on DioError catch (e) {
                  Get.snackbar(
                    '인증실패',
                    e.response!.data['resultMsg'],
                  );
                }
              }
            },
          ),
        ],
      ),
    );
  }

  resetCount() {
    setState(() {
      this.countDown = 180;
      this.countDownEnd = DateTime.now().add(
        Duration(
          seconds: this.countDown,
        ),
      );
    });
  }

  runCountDown() {
    if (timer != null && timer!.isActive) {
      timer!.cancel();
    }

    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      final count = this.countDownEnd.difference(DateTime.now()).inSeconds;
      setState(() {
        this.countDown = count > 0 ? count : 0;
      });
    });
  }

  resendVerificationCode() async {
    try {
      await Get.find<CommController>().certRequestEmail(
        body: CertRequestEmailBody(
          email: this.targetEmail,
        ),
      );

      resetCount();
      runCountDown();

      Get.snackbar(
        '인증번호 전송',
        '${this.targetEmail}로 인증번호가 전송되었습니다.',
      );
    } on DioError catch (e) {
      Get.snackbar(
        '인증번호 전송 실패',
        e.response!.data['resultMsg'],
      );
    }
  }

  renderEmailField() {
    return VerificationTargetTextField(
      onSaved: formModel.setEmail,
      initialValue: this.targetEmail,
      label: '이메일 주소',
      onResendTap: resendVerificationCode,
    );
  }

  renderVerificationCode() {
    return VerificationCodeTextField(
      onSaved: formModel.setVerificationCode,
      countDown: this.countDown,
    );
  }

  @override
  void dispose() {
    timer!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TitleLayout(
      title: '이메일 인증',
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              renderEmailField(),
              Container(height: 28.0),
              renderVerificationCode(),
              renderNextButton(),
              Container(height: 8.0),
            ],
          ),
        ),
      ),
    );
  }
}
