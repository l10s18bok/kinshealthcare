import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/auth_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/pin_entry/auth_pin_entry.dart';
import 'package:kins_healthcare/components/text_field/underline_text_form_field.dart';
import 'package:kins_healthcare/components/text_field/verification_code_text_field.dart';
import 'package:kins_healthcare/controllers/auth_controller.dart';
import 'package:kins_healthcare/controllers/cert_controller.dart';
import 'package:kins_healthcare/controllers/comm_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/layouts.dart';
import 'package:kins_healthcare/models/form/phone_verify_form_model.dart';
import 'package:kins_healthcare/screens/auth/register/register_screen.dart';
import 'package:kins_healthcare/services/comm/model/cert_request_sms_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/validator_utils.dart';

class PhoneScreenArguments {
  final String? email;
  final String phone;
  final int? seq;
  final String? password;
  final SignUpType? signUpType;

  PhoneScreenArguments({
    required this.phone,
    this.email,
    this.seq,
    this.password,
    this.signUpType,
  });
}

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HR_0003
 * 주요기능 : 휴대폰 인증하기
 */

// 2021.04.21 Andy 추가
enum CertificationType {
  signUp,

  /// 회원가입시 휴대폰 인증
  phone,

  /// 전화번호 인증시
}

class PhoneScreen extends StatefulWidget {
  @override
  _PhoneScreenState createState() => _PhoneScreenState();
}

class _PhoneScreenState extends State<PhoneScreen> {
  late CertificationType certificationType;
  final formKey = GlobalKey<FormState>();
  final formModel = PhoneVerifyFormModel();

  late int seq;
  late String email;
  String? password;
  late String phone;
  late int countDown;
  late DateTime countDownEnd;
  late PhoneScreenArguments args;
  Timer? timer;

  @override
  void initState() {
    super.initState();

    final cType = Get.parameters['type'];

    /// 기본 : 회원가입시 휴대폰 인증
    certificationType = CertificationType.signUp;

    /// 회원가입후 전화번호 인증만 필요한 경우
    if (cType == 'phone') {
      certificationType = CertificationType.phone;
    }

    args = Get.arguments;

    if (args is PhoneScreenArguments) {
      seq = args.seq ?? 0;
      email = args.email ?? '';
      password = args.password ?? '';
      phone = args.phone;
    } else {
      throw Exception('아규먼트가 제공되지 않았습니다.');
    }

    print('==========> seq : ${seq.toString()}');
    resetCount();
    resendVerificationCode();
  }

  resetCount() {
    setState(() {
      this.countDown = 180;
      this.countDownEnd = DateTime.now().add(
        Duration(
          seconds: this.countDown,
        ),
      );
    });
  }

  runCountDown() {
    if (timer != null && timer!.isActive) {
      timer!.cancel();
    }

    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      final count = this.countDownEnd.difference(DateTime.now()).inSeconds;
      setState(() {
        this.countDown = count > 0 ? count : 0;
      });
    });
  }

  renderPinCodeField() {
    return Expanded(
      child: AuthPinEntry(
        normalText: '인증번호가 발송되었습니다.',
        boldText: '인증번호 6자리를 입력해주세요.',
      ),
    );
  }

  renderVerificationCodeField() {
    return VerificationCodeTextField(
      onSaved: this.formModel.setCertText,
      countDown: this.countDown,
    );
  }

  resendVerificationCode() async {
    try {
      await Get.find<CommController>().certRequestSms(
        body: CertRequestSmsBody(
          phone: this.phone,
        ),
      );

      resetCount();
      runCountDown();

      Get.snackbar('인증번호 전송', '${this.phone}으로 인증번호를 전송했습니다.');
    } on DioError catch (e) {
      Get.snackbar(
        '인증번호 전송 실패',
        e.response!.data['resultMsg'],
      );
    }
  }

  // renderPhoneField() {
  //   return VerificationTargetTextField(
  //     onSaved: this.formModel.setPhone,
  //     initialValue: this.phone,
  //     label: '핸드폰 번호',
  //     onResendTap: resendVerificationCode,
  //   );
  // }
  // 2021.05.04 Andy 수정 : 입력필드 동작을 막고 + "인증번호 재전송" 버튼을 동작시키기 위해 아래와 같이 수정
  renderPhoneField() {
    final theme = ThemeFactory.of(context).theme;
    return Stack(
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width,
          child: AbsorbPointer(
            child: UnderlineTextFormField(
              labelText: '핸드폰 번호',
              validator: Validators.phoneValidator,
              onSaved: this.formModel.setPhone,
              readOnly: true,
              keyboardType: TextInputType.number,
              initialValue: this.phone,
            ),
          ),
        ),
        Positioned(
          top: 35,
          right: 0,
          child: InkWell(
            onTap: resendVerificationCode,
            child: Text(
              '인증번호 재전송',
              style: TextStyle(
                fontSize: theme.fontSize13,
                fontWeight: theme.heavyFontWeight,
                color: theme.primaryColor,
              ),
            ),
          ),
        )
      ],
    );
  }

  renderNextButton() {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          PrimaryButton(
            label: '인증하기',
            onTap: () async {
              if (this.formKey.currentState!.validate()) {
                this.formKey.currentState!.save();
                this.formModel.setSeq(seq);

                if (this.countDown == 0) {
                  Get.snackbar(
                    '인증번호 만료',
                    '인증번호가 만료되었습니다. 인증번호를 재전송 해주세요!',
                  );
                  return;
                }
                timer?.cancel();
                try {
                  if (certificationType == CertificationType.signUp) {
                    /// 회원가입
                    if (args.signUpType == SignUpType.adult) {
                      await Get.find<UserController>().regOkSmS(
                        body: this.formModel.toRegBody(),
                      );

                      final ac = Get.find<AuthController>();

                      await ac.loginUser(
                        email: this.email,
                        password: this.password!,
                      );
                      final step = await Get.toNamed('/auth/register/profile');
                      if (step != null) Get.back(result: step);
                    } else {
                      /// 자녀회원 등록은 여기(전화번호 인증)까지만..

                      await Get.find<CertController>().regChildOkSmS(
                        body: this.formModel.toRegBody(),
                      );

                      await Get.bottomSheet(
                        AuthBottomSheet(
                          title: '축하드려요 🥳\n자녀 회원가입이 완료되었습니다.',
                        ),
                      );
                      Get.back(result: StepArguments(regStep: RegStep.all));
                    }
                  } else {
                    /// 전화번호 인증만..
                    final resp = await Get.find<CommController>().certOkSmS(
                      body: this.formModel.toBody(),
                    );
                    Get.back(result: resp.resultCode);
                  }
                } on DioError catch (e) {
                  Get.snackbar(
                    '인증 실패',
                    e.response!.data['resultMsg'],
                  );
                }
              }
            },
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TitleLayout(
      title: '휴대폰 인증',
      onActionTap: () {},
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Form(
          key: this.formKey,
          child: Column(
            children: [
              renderPhoneField(),
              Container(height: 28.0),
              renderVerificationCodeField(),
              renderNextButton(),
              Container(height: 8.0),
            ],
          ),
        ),
      ),
    );
  }
}
