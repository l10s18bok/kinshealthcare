import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/tab/circle_tap.dart';
import 'package:kins_healthcare/controllers/custom_controller.dart';
import 'package:kins_healthcare/layouts/IntrinsicHeightLayout.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/custom/model/custom_detail_model.dart';
import 'package:kins_healthcare/services/custom/model/custom_list_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-04-01
 작성자 : Mark,
 화면명 : HN_2002
 경로 : /advertise/custom/detail
 클래스 : CustomDetailScreen,
 설명 : 맞춤 광고 정보의 List를 표시하기 위한 Page
*/

/// /advertise/custom/detail \
/// 맞춤 광고 정보의 List를 표시하기 위한 Page
///
/// [Figma 링크]()
///
/// - 화면명 : HN_2002
/// - 작성자 : Mark
/// - 작성일 : 2021-04-01
///
/// {@category Screen}
class CustomDetailScreen extends StatefulWidget {
  @override
  CustomDetailScreenState createState() => CustomDetailScreenState();
}

/// /advertise/custom/detail \
/// 맞춤 광고 정보의 List를 표시하기 위한 Page
///
/// [Figma 링크]()
///
/// - 화면명 : HN_2002 \
/// - 작성자 : Mark \
/// - 작성일 : 2021-04-01
///
/// {@category Screen}
class CustomDetailScreenState extends State<CustomDetailScreen> {
  int _currentPage = 0;
  CustomDetailModel? _detailModel;
  List<String> image = [];

  @override
  void initState() {
    super.initState();
    setCustomList();
  }

  setCustomList() async {
    CustomListModel model = Get.arguments;

    try {
      _detailModel = await Get.find<CustomController>().detail(model.itemNo!);

      final mainImage = _detailModel?.itemMainImage;
      if (mainImage != null) image.add(mainImage);
      final subImages = _detailModel?.itemSubImages;
      if (subImages != null) image.addAll(subImages);

      _setStateEndAnimation();
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeightLayout(
      body: Column(
        children: [
          renderTopList(),
          renderTitle(),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Container(height: 1.0, color: Color(0xFFE3E3E3)),
          ),
          Container(height: 24.0),
          Expanded(child: renderContent()),
          Container(height: 20.0),
          renderBottomButton(),
          Container(height: 16.0),
        ],
      ),
    );
  }

  /// hello
  Widget renderTopList() {
    return Stack(
      children: [
        renderCarousel(image),
        Positioned(
          top: 16,
          left: 16,
          child: renderBackBtn(),
        ),
        Positioned(
          bottom: 13,
          left: 0,
          right: 0,
          child: CircleTap(
            length: image.length,
            currentPage: _currentPage,
          ),
        ),
      ],
    );
  }

  Widget renderBackBtn() {
    return InkWell(
      child: Container(
        alignment: Alignment.centerLeft,
        height: 44,
        width: 40,
        child: SvgPicture.asset(
          'assets/svgs/ic/ic_arrowback_common.svg',
          width: 24,
          height: 24,
          color: kinsWhite,
        ),
      ),
      onTap: () => Get.back(),
    );
  }

  Widget renderCarousel(List<String> images) {
    final ratio = 360.0 / 340.0;
    if (images.length == 0) {
      return AspectRatio(aspectRatio: ratio, child: renderNonCard());
    }

    return AspectRatio(
      aspectRatio: ratio,
      child: PageView(
        children: images.map((path) => renderImage(path)).toList(),
        onPageChanged: (index) => setState(() => _currentPage = index),
      ),
    );
  }

  Widget renderImage(String? path) {
    if (path == null || !path.startsWith('http')) return renderNonCard();

    return CachedNetworkImage(
      imageUrl: path,
      fit: BoxFit.cover,
      width: double.infinity,
      errorWidget: (context, url, error) => renderNonCard(),
    );
  }

  Widget renderNonCard() {
    return Padding(
      padding: EdgeInsets.all(10),
      child: NonKkakkaCard(title: 'No Image', height: double.infinity),
    );
  }

  Widget renderTitle() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            _detailModel?.itemTitle ?? '',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: theme.fontSize18,
            ),
          ),
          Container(height: 8.0),
          Text(
            _detailModel?.itemPrice ?? '',
            style: TextStyle(
              fontSize: theme.fontSize16,
            ),
          ),
        ],
      ),
    );
  }

  Widget renderContent() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.topLeft,
      child: Text(
        _detailModel?.itemContent ?? '',
        style: TextStyle(
          fontSize: theme.fontSize12,
          color: Color(0xFF7E7E7E),
        ),
      ),
    );
  }

  Widget renderBottomButton() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      height: 55,
      child: PrimaryButton(
        padding: EdgeInsets.zero,
        label: '제품 확인하기',
        onTap: () {},
      ),
    );
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }
}
