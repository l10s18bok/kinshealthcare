import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/card/price_card.dart';
import 'package:kins_healthcare/components/listview/price_list_view.dart';
import 'package:kins_healthcare/components/tab/scroll_tab.dart';
import 'package:kins_healthcare/controllers/custom_controller.dart';
import 'package:kins_healthcare/layouts/main_tab_body_layout.dart';
import 'package:kins_healthcare/services/custom/model/custom_list_model.dart';
import 'package:kins_healthcare/services/custom/model/like_register_model.dart';
import 'package:kins_healthcare/utils/resource.dart';

import '../../spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';

/*
 작성일 : 2021-03-31
 작성자 : Mark,
 화면명 : HN_2001
 경로 : /advertise/custom/list
 클래스 : AdvertisementListScreen,
 설명 : 맞춤 광고 정보의 List를 표시하기 위한 Page
*/

/// /advertise/custom \
/// 맞춤 광고 정보의 List를 표시하기 위한 Page
///
/// [Figma 링크]()
///
/// - 화면명 : HN_2002 \
/// - 작성자 : Mark \
/// - 작성일 : 2021-04-01
///
/// {@category Screen}
class AdListScreen extends StatefulWidget {
  @override
  AdListScreenState createState() => AdListScreenState();
}

/// /advertise/custom \
/// 맞춤 광고 정보의 List를 표시하기 위한 Page
///
/// [Figma 링크]()
///
/// - 화면명 : HN_2002 \
/// - 작성자 : Mark \
/// - 작성일 : 2021-04-01
///
/// {@category Screen}
class AdListScreenState extends State<AdListScreen> {
  late CustomController customController;
  FixedExtentScrollController _scrollController = FixedExtentScrollController();
  int nowIndex = 0, page = 0;
  List<CustomListModel>? customList;

  final List<String> list = [
    'https://test-image-sm.s3.ap-northeast-2.amazonaws.com/sister2.jpg',
    'assets/png/ad_image_02.png',
    'assets/test/carousel_image.jpg',
    'assets/png/ad_image_01.png',
    'assets/test/cho.jpg',
    'assets/png/ad_image_02.png',
    'assets/test/carousel_image.jpg',
    'assets/png/ad_image_01.png',
    'assets/test/cho.jpg',
  ];

  final List<MidListModel> midList = [
    MidListModel(path: 'assets/png/ad_menu_01.png', title: '정관장 신상\n기획전'),
    MidListModel(path: 'assets/png/ad_menu_02.png', title: '면파스타\n업뎃'),
    MidListModel(path: 'assets/png/ad_menu_03.png', title: '정관장 신상\n기획전'),
    MidListModel(path: 'assets/png/ad_menu_01.png', title: '정관장 신상\n기획전'),
    MidListModel(path: 'assets/png/ad_menu_02.png', title: '면파스타\n업뎃'),
    MidListModel(path: 'assets/png/ad_menu_03.png', title: '정관장 신상\n기획전'),
  ];

  @override
  void initState() {
    super.initState();
    customController = Get.find<CustomController>();
    _setCustomList(page);
  }

  _setCustomList(int page) async {
    if (page == -1) return;

    try {
      final resultList = await customController.getList(page: page);
      if (resultList == null) return page = -1;

      customList = resultList;
      _setStateEndAnimation();
    } on DioError catch (e) {
      this.page = -1;
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MainTabBodyLayout(
      title: '맞춤광고',
      action: renderAction(),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: renderMainScrollView(),
      ),
      topMargin: 0.0,
    );
  }

  Widget renderAction() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        GestureDetector(
          onTap: () {},
          child: Padding(
            padding: EdgeInsets.only(right: 12.0),
            child:
                SvgPicture.asset('assets/svgs/ic/ic_search_customizedpage.svg'),
          ),
        ),
        GestureDetector(
          onTap: () {},
          child: Padding(
            padding: EdgeInsets.only(right: 16.0),
            child: SvgPicture.asset('assets/svgs/ic/ic_filter.svg'),
          ),
        ),
      ],
    );
  }

  Widget renderMainScrollView() {
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Column(
            children: [
              renderTop(),
              SizedBox(height: 30),
              renderMid(),
              SizedBox(height: 24),
            ],
          ),
        ),
        SliverPadding(
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
          sliver: renderBottomGrid(),
        ),
      ],
    );
  }

  Widget renderTop() {
    return Container(
      color: Color(0xFFF3F4F6),
      height: 344,
      child: Stack(
        children: [
          PriceListView(
            scrollController: _scrollController,
            list: list,
            nowIndex: nowIndex,
            endScrollCallBack: (index) => setState(() => nowIndex = index),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 12,
            child: renderScrollBar(),
          ),
        ],
      ),
    );
  }

  Widget renderScrollBar() {
    return Align(
      alignment: Alignment.center,
      child: SizedBox(
        width: 250,
        child: ScrollTap(
          maxCount: list.length,
          scrollController: _scrollController,
          backgroundColor: kinsBlack14,
          barColor: kinsBlack0D,
        ),
      ),
    );
  }

  Widget renderMid() {
    return Container(
      height: 110,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: midList.length,
        itemBuilder: (_, index) {
          final leftPadding = index == 0 ? 16.0 : 0.0;
          return Padding(
            padding: EdgeInsets.only(left: leftPadding, right: 20),
            child: renderMidItem(midList[index]),
          );
        },
      ),
    );
  }

  Widget renderMidItem(MidListModel model) {
    return Column(
      children: [
        Image.asset(model.path, width: 69, height: 69),
        Text(
          model.title,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 11),
        ),
      ],
    );
  }

  Widget renderBottomGrid() {
    final list = this.customList;
    if (list == null) return renderLoading();

    return SliverGrid(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 14,
        mainAxisSpacing: 24,
        childAspectRatio: 157 / 250,
      ),
      delegate: SliverChildBuilderDelegate(
        (context, index) {
          if (index == list.length - 1)
            _setCustomList(page == -1 ? page : page++);
          if (index >= list.length) return Container();

          return PriceCard(
            model: list[index],
            onFavouriteTap: () => _onFavouriteTap(index),
          );
        },
        childCount: list.length,
      ),
    );
  }

  Widget renderLoading() {
    return SliverToBoxAdapter(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        alignment: Alignment.center,
        height: 50,
        child: CircularProgressIndicator(),
      ),
    );
  }

  Future<void> _refresh() async {
    page = 0;
    await _setCustomList(page);
  }

  _onFavouriteTap(int index) async {
    final list = customList;
    if (list == null) return;

    final model = list[index];
    try {
      if (model.likeNo == null) {
        LikeRegisterModel result =
            await customController.likeRegister(model.itemNo!);
        model.likeNo = result.likeNo;
      } else {
        await customController.likeDelete(model.itemNo!, model.likeNo!);
        model.likeNo = null;
      }

      final sheet = ErrorBottomSheet(title: '완료', content: '성공');
      await Get.bottomSheet(sheet);
      _setStateEndAnimation();
    } on DioError catch (e) {
      this.page = -1;
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }
}

class MidListModel {
  String path;
  String title;

  MidListModel({
    required this.path,
    required this.title,
  });
}
