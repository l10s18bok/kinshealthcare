import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/components/card/advert_article_card.dart';
import 'package:kins_healthcare/components/card/advert_card.dart';
import 'package:kins_healthcare/components/text_field/rounded_textfield.dart';
import 'package:kins_healthcare/layouts/main_tab_body_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/// /advertise/list \
/// ???
///
/// [Figma 링크]()
///
/// - 화면명 :  \
/// - 작성자 :  \
/// - 작성일 :
///
/// {@category Screen}
class ListScreen extends StatefulWidget {
  @override
  ListScreenState createState() => ListScreenState();
}

/// /advertise/list \
/// ???
///
/// [Figma 링크]()
///
/// - 화면명 :  \
/// - 작성자 :  \
/// - 작성일 :
///
/// {@category Screen}
class ListScreenState extends State<ListScreen> {
  renderAction() {
    return GestureDetector(
      onTap: () {},
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(right: 16.0),
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_heart.svg',
            ),
          ),
        ],
      ),
    );
  }

  renderIconButton({
    required String iconName,
    required String label,
    required String subLabel,
    @required GestureTapCallback? onTap,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return GestureDetector(
      onTap: onTap,
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: 56.0,
              width: 56.0,
              decoration: BoxDecoration(
                color: Color(0xFFFFF4EF),
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Padding(
                padding: EdgeInsets.all(16.0),
                child: SvgPicture.asset(
                  'assets/svgs/ic/ic_advert_$iconName.svg',
                ),
              ),
            ),
            Container(width: 20.0),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Text(
                        label,
                        style: TextStyle(
                          fontSize: theme.fontSize15,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        subLabel,
                        style: TextStyle(
                          fontSize: theme.fontSize12,
                          color: Color(0xFF929292),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  renderButtonDivider() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Container(
        height: 1.0,
        color: Color(0xFFF1F1F1),
      ),
    );
  }

  renderIconButtons() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          renderIconButton(
            iconName: 'hospital',
            label: '병원',
            subLabel: '우리 동네 병원을 알아보자',
            onTap: () {},
          ),
          renderButtonDivider(),
          renderIconButton(
            iconName: 'bandage',
            label: '건강 기능식품',
            subLabel: '건강 기능식품을 꼭 먹어야하는 이유!',
            onTap: () {},
          ),
          renderButtonDivider(),
          renderIconButton(
            iconName: 'stethoscope',
            label: '일반의약품',
            subLabel: '우리 동네 병원을 알아보자',
            onTap: () {},
          ),
          renderButtonDivider(),
          renderIconButton(
            iconName: 'medicine',
            label: '건강의료용품',
            subLabel: '건강 기능식품을 꼭 먹어야하는 이유!',
            onTap: () {},
          ),
        ],
      ),
    );
  }

  renderAdverts() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: AdvertCard(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MainTabBodyLayout(
      title: '맞춤정보',
      action: renderAction(),
      body: ListView(
        children: [
          RoundedTextField(),
          Container(height: 20.0),
          renderIconButtons(),
          Container(height: 36.0),
          AdvertArticleCard(),
          Container(height: 30.0),
          renderAdverts(),
          Container(height: 16.0),
        ],
      ),
    );
  }
}
