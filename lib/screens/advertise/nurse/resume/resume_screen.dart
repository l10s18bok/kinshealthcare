import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/advert_filter_bottom_sheet.dart';
import 'package:kins_healthcare/components/card/job_post_card.dart';
import 'package:kins_healthcare/components/tab/check_tab.dart';
import 'package:kins_healthcare/components/tab/page_tab.dart';
import 'package:kins_healthcare/components/text_field/rounded_textfield.dart';
import 'package:kins_healthcare/layouts/main_tab_body_layout.dart';

/// /advertise/nurse/resume \
/// ???
///
/// [Figma 링크]()
///
/// - 화면명 :  \
/// - 작성자 :  \
/// - 작성일 :
///
/// {@category Screen}
class ResumeScreen extends StatefulWidget {
  @override
  ResumeScreenState createState() => ResumeScreenState();
}

/// /advertise/nurse/resume \
/// ???
///
/// [Figma 링크]()
///
/// - 화면명 :  \
/// - 작성자 :  \
/// - 작성일 :
///
/// {@category Screen}
class ResumeScreenState extends State<ResumeScreen> {
  PageController _pageController = PageController(initialPage: 0);

  renderAction() {
    return Container(
      padding: EdgeInsets.only(right: 16.0),
      child: Container(
        alignment: Alignment.centerRight,
        width: 25,
        height: 25,
        child: InkWell(
          onTap: () => Get.toNamed('/advertise/nurse/favourite'),
          child: SvgPicture.asset('assets/svgs/ic/ic_heart.svg'),
        ),
      ),
    );
  }

  renderTabs() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: PageTap(
        pageController: _pageController,
        titleList: ['전체', '종합병원', '요양병원', '한방병원'],
      ),
    );
  }

  renderCheckTabs() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CheckTab(tabNames: ['최신순', '오래된 순']),
          GestureDetector(
            onTap: () {
              Get.bottomSheet(
                AdvertFilterBottomSheet(),
                isScrollControlled: true,
              );
            },
            child: SvgPicture.asset('assets/svgs/ic/ic_filter.svg'),
          ),
        ],
      ),
    );
  }

  renderBanner() {
    return Container(
      height: 150.0,
      color: Colors.grey[200],
    );
  }

  renderMainPageView() {
    return PageView(
      controller: _pageController,
      children: <Widget>[
        renderAdverts(),
        renderAdverts(),
        renderAdverts(),
        renderAdverts(),
      ],
    );
  }

  renderAdverts() {
    return ListView.builder(
      itemBuilder: (content, index) {
        if (index == 0) return renderFirstItem();
        final modeIndex = index - 1;

        return JobPostCard(
          onTap: () => Get.toNamed('/advertise/nurse/detail'),
          onShareTap: () => Get.toNamed('/advertise/nurse/public'),
          onFavouriteTap: () {
            print('modeIndex : $modeIndex');
          },
        );
      },
      itemCount: 10,
    );
  }

  renderFirstItem() {
    return Column(
      children: [
        renderCheckTabs(),
        renderBanner(),
        Container(height: 16.0),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: MainTabBodyLayout(
        title: '맞춤공고',
        action: renderAction(),
        body: Column(
          children: [
            SizedBox(
              height: 43,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: RoundedTextField(),
              ),
            ),
            Container(height: 30.0),
            renderTabs(),
            Expanded(
              child: renderMainPageView(),
            ),
          ],
        ),
      ),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
    );
  }

  void dispose() {
    super.dispose();
    _pageController.dispose();
  }
}
