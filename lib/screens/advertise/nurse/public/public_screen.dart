import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/payment_user_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/// /advertise/nurse/public \
/// ???
///
/// [Figma 링크]()
///
/// - 화면명 :  \
/// - 작성자 :  \
/// - 작성일 :
///
/// {@category Screen}
class PublicScreen extends StatefulWidget {
  @override
  PublicScreenState createState() => PublicScreenState();
}

/// /advertise/nurse/public \
/// ???
///
/// [Figma 링크]()
///
/// - 화면명 :  \
/// - 작성자 :  \
/// - 작성일 :
///
/// {@category Screen}
class PublicScreenState extends State<PublicScreen> {
  bool isChecked = false;
  late List<PaymentUserModel> userList;

  @override
  void initState() {
    super.initState();
    userList = _getUserList();
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(title: '공유할 가족 선택'),
      body: Padding(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(child: renderListView()),
            renderSubmitBtn(),
          ],
        ),
      ),
    );
  }

  Widget renderListView() {
    return ListView.builder(
      itemCount: userList.length,
      itemBuilder: (_, index) {
        return renderListItem(userList[index]);
      },
    );
  }

  Widget renderListItem(PaymentUserModel model) {
    return InkWell(
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: kinsGrayEA,
              width: 1.0,
            ),
          ),
        ),
        height: 84,
        child: Row(
          children: [
            renderItemImage(model.isOnContacts),
            SizedBox(width: 16),
            Expanded(child: renderItemContent(model)),
            SizedBox(width: 16),
            renderCheckBtn(model),
          ],
        ),
      ),
      onTap: () {
        model.isChecked = !model.isChecked;
        setState(() {});
      },
    );
  }

  Widget renderItemImage(bool isOnContacts) {
    return SizedBox(
      width: 56,
      height: 56,
      child: Stack(
        children: [
          Positioned.fill(child: renderNonImage(56)),
          Positioned(child: renderPhoneIcon(isOnContacts)),
        ],
      ),
    );
  }

  Widget renderNonImage(double imageHeight) {
    return SvgPicture.asset(
      'assets/svgs/ic/ic_profile_signin.svg',
      width: imageHeight,
      height: imageHeight,
    );
  }

  Widget renderPhoneIcon(bool isOnContacts) {
    if (isOnContacts) {
      return SvgPicture.asset(
        'assets/svgs/ic/ic_call_contactpage_on.svg',
        width: 19,
        height: 19,
      );
    } else {
      return SvgPicture.asset(
        'assets/svgs/ic/ic_call_contactpage_off.svg',
        width: 19,
        height: 19,
      );
    }
  }

  Widget renderItemContent(PaymentUserModel model) {
    final name = model.name;
    final nickName = model.nickName;
    final phoneNumber = model.phoneNum;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '$name ($nickName)',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
          ),
        ),
        SizedBox(height: 10),
        Text(
          phoneNumber,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
          ),
        ),
      ],
    );
  }

  renderCheckBtn(PaymentUserModel model) {
    final backgroundColor = model.isChecked ? kinsBlue40 : kinsBlueCF;

    return Container(
      height: 26,
      child: Row(
        children: [
          AnimatedContainer(
            alignment: Alignment.center,
            duration: Duration(milliseconds: 100),
            width: 26,
            height: 26,
            decoration: BoxDecoration(
              color: backgroundColor,
              borderRadius: BorderRadius.all(Radius.circular(4)),
              border: Border.all(color: kinsGrayC2),
            ),
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_checkbox_unselect.svg',
              width: 12,
              height: 10,
            ),
          ),
        ],
      ),
    );
  }

  Widget renderSubmitBtn() {
    var isCheck = false;

    for (final item in userList) {
      if (item.isChecked) {
        isCheck = true;
        break;
      }
    }

    return Container(
      margin: EdgeInsets.only(bottom: 16),
      height: 55,
      child: PrimaryButton(
        label: '확인',
        bgColor: isCheck ? null : kinsBlueB8,
        onTap: () {
          if (isCheck == false) return;
          Get.back();
        },
      ),
    );
  }

  List<PaymentUserModel> _getUserList() {
    return [
      PaymentUserModel(
        name: '아빠',
        phoneNum: '010.1234.5678',
        nickName: '아바마마',
        isOnContacts: true,
        userRelNo: 0,
      ),
      PaymentUserModel(
        name: '누나',
        phoneNum: '010.1234.5678',
        nickName: '이쁘니',
        isOnContacts: false,
        userRelNo: 0,
      ),
      PaymentUserModel(
        name: '딸',
        phoneNum: '010.1234.5678',
        nickName: '서니',
        isOnContacts: true,
        userRelNo: 0,
      ),
    ];
  }
}
