import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/advertisement_list_button.dart';
import 'package:kins_healthcare/components/card/advert_article_card.dart';
import 'package:kins_healthcare/components/card/advert_card.dart';
import 'package:kins_healthcare/layouts/main_tab_body_layout.dart';

/// /advertise/nurse \
/// ???
///
/// [Figma 링크]()
///
/// - 화면명 :
/// - 작성자 :
/// - 작성일 :
///
/// {@category Screen}
class NurseScreen extends StatefulWidget {
  @override
  NurseScreenState createState() => NurseScreenState();
}

/// /advertise/nurse \
/// ???
///
/// [Figma 링크]()
///
/// - 화면명 :
/// - 작성자 :
/// - 작성일 :
///
/// {@category Screen}
class NurseScreenState extends State<NurseScreen> {
  Widget renderDivider() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Container(
        height: 1.0,
        color: Color(0xFFF1F1F1),
      ),
    );
  }

  Widget renderButtons() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          AdvertisementListButton(
            onTap: () {
              Get.toNamed('/advertise/nurse/resume');
            },
            icon: 'hospital',
            label: '맞춤공고',
            subLabel: '새로 올라온 공고',
          ),
          renderDivider(),
          AdvertisementListButton(
            onTap: () {
              Get.toNamed('/advertise/custom/list');
            },
            icon: 'bandage',
            label: '전문 의료기기',
            subLabel: '전문 의료기기의 모든것',
          ),
          renderDivider(),
          AdvertisementListButton(
            onTap: () {},
            icon: 'medicine',
            label: '전문의 약품',
            subLabel: '우리 동네 병원을 알아보자',
          ),
        ],
      ),
    );
  }

  Widget renderAdvert() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(16.0),
      child: AdvertCard(),
    );
  }

  Widget _renderIntrinsicHeight(Widget body, double minHeight) {
    return SingleChildScrollView(
      child: IntrinsicHeight(
        child: Container(
          constraints: BoxConstraints(
            minHeight: minHeight,
          ),
          child: body,
        ),
      ),
    );
  }

  Widget renderMain() {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final maxHeight = constraints.maxHeight;
        return _renderIntrinsicHeight(
          Column(
            children: [
              renderButtons(),
              Container(height: 36.0),
              AdvertArticleCard(),
              Container(height: 30.0),
              Spacer(),
              renderAdvert(),
            ],
          ),
          maxHeight,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MainTabBodyLayout(
      title: '나이팅게일',
      body: renderMain(),
    );
  }
}
