import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/card/job_post_card.dart';
import 'package:kins_healthcare/components/tab/page_tab.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/layouts/layouts.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/// /advertise/nurse/favourite \
/// ???
///
/// [Figma 링크]()
///
/// - 화면명 :  \
/// - 작성자 :  \
/// - 작성일 :
///
/// {@category Screen}
class FavouriteScreen extends StatefulWidget {
  @override
  FavouriteScreenState createState() => FavouriteScreenState();
}

/// /advertise/nurse/favourite \
/// ???
///
/// [Figma 링크]()
///
/// - 화면명 :  \
/// - 작성자 :  \
/// - 작성일 :
///
/// {@category Screen}
class FavouriteScreenState extends State<FavouriteScreen> {
  PageController _pageController = PageController(initialPage: 0);

  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  renderTopBar() {
    return BackTopBar(title: '관심공고');
  }

  renderAnimationTab() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: PageTap(
        pageController: _pageController,
        titleList: ['찜', '최근 본 공고'],
      ),
    );
  }

  renderRecentPostButton() {
    final theme = ThemeFactory.of(context).theme;

    return GestureDetector(
      onTap: () {},
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    '[서울삼성병원] 혈관외과 SA간호사 채용',
                    style: TextStyle(
                      fontSize: theme.fontSize14,
                      color: Color(0xFF232323),
                    ),
                  ),
                  Container(height: 14.0),
                  Text(
                    '2020.11.21 | 서울∙한국',
                    style: TextStyle(
                      fontSize: theme.fontSize12,
                      color: Color(0xFF898989),
                    ),
                  ),
                ],
              ),
            ),
            Icon(
              Icons.chevron_right,
            ),
          ],
        ),
      ),
    );
  }

  renderMainPageView() {
    return PageView(
      controller: _pageController,
      children: <Widget>[
        renderFavouriteList(),
        renderRecentlyList(),
      ],
    );
  }

  renderFavouriteList() {
    return ListView.separated(
      separatorBuilder: (_, index) {
        return Container(height: 12.0);
      },
      itemBuilder: (_, index) {
        return JobPostCard(
          onTap: () {},
          onShareTap: () {},
          onFavouriteTap: () {},
        );
      },
      itemCount: 100,
    );
  }

  renderRecentlyList() {
    return ListView.separated(
      separatorBuilder: (_, index) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: Container(height: 1.0, color: Color(0xFFF1F1F1)),
        );
      },
      itemBuilder: (_, index) {
        return renderRecentPostButton();
      },
      itemCount: 100,
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Column(
        children: [
          renderTopBar(),
          SizedBox(height: 30),
          renderAnimationTab(),
          Container(height: 16.0),
          Expanded(child: renderMainPageView()),
        ],
      ),
    );
  }
}
