import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/card/advert_card.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/// /advertise/nurse/detail \
/// ???
///
/// [Figma 링크]()
///
/// - 화면명 :  \
/// - 작성자 :  \
/// - 작성일 :
///
/// {@category Screen}
class DetailScreen extends StatefulWidget {
  @override
  DetailScreenState createState() => DetailScreenState();
}

/// /advertise/nurse/detail \
/// ???
///
/// [Figma 링크]()
///
/// - 화면명 :  \
/// - 작성자 :  \
/// - 작성일 :
///
/// {@category Screen}
class DetailScreenState extends State<DetailScreen> {
  renderCarousel() {
    return AspectRatio(
      aspectRatio: 360 / 250,
      child: PageView(
        children: [
          Image.asset(
            'assets/test/carousel_image.jpg',
            fit: BoxFit.cover,
          ),
          Image.asset(
            'assets/test/carousel_image.jpg',
            fit: BoxFit.cover,
          ),
          Image.asset(
            'assets/test/carousel_image.jpg',
            fit: BoxFit.cover,
          ),
          Image.asset(
            'assets/test/carousel_image.jpg',
            fit: BoxFit.cover,
          ),
          Image.asset(
            'assets/test/carousel_image.jpg',
            fit: BoxFit.cover,
          ),
        ],
      ),
    );
  }

  renderTitle() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 24.0),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                '[서울삼성병원] 혈관외과 SA간호사 채용',
                style: TextStyle(fontSize: theme.fontSize14),
              ),
            ],
          ),
          Container(height: 8.0),
          Row(
            children: [
              Text(
                '2020.11.21 | 서울 한국',
                style: TextStyle(
                  fontSize: theme.fontSize12,
                  color: Color(0xFF898989),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  renderStatus() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Color(0xFFECECF3),
              border: Border.all(
                color: Color(0xFFB4B5D0),
                width: 1.0,
              ),
              borderRadius: BorderRadius.circular(
                5.0,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 4.0),
              child: Text(
                '채용중',
                style: TextStyle(
                  fontSize: theme.fontSize13,
                  color: theme.primaryColor,
                ),
              ),
            ),
          ),
          Container(width: 21.0),
          Text(
            '2020년 12월 31일까지 모집',
            style: TextStyle(
              fontSize: theme.fontSize13,
              color: theme.primaryColor,
            ),
          ),
        ],
      ),
    );
  }

  renderContent() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.centerLeft,
      child: Text(
        '''[서울삼성병원] 혈관외과 SA간호사 채용합니다

모집부분 및 요건
부분: 혈관외과 간호사
주요업무: 혈관외과 수술지원, 수술대상 환자 응대
(신입도 지원가능

근무조건 및 근무환경
연봉범위: 2600~2800만원
부분: 혈관외과 간호사
주요업무: 혈관외과 수술지원, 수술대상 환자 응대
(신입도 지원가능''',
        style: TextStyle(
          fontSize: theme.fontSize12,
          color: Color(0xFF7E7E7E),
        ),
      ),
    );
  }

  renderAdvert() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: AdvertCard(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollLayout(
      topBar: BackTopBar(title: '공고 상세정보'),
      body: Column(
        children: [
          Container(height: 20.0),
          renderCarousel(),
          renderTitle(),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Container(height: 1.0, color: Color(0xFFE3E3E3)),
          ),
          Container(height: 24.0),
          renderStatus(),
          Container(height: 24.0),
          renderContent(),
          Container(height: 16.0),
          renderAdvert(),
          Container(height: 16.0),
        ],
      ),
    );
  }
}
