import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/card/timeline_bubble_card.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/components/tab/check_tab.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/controllers/timeline_controller.dart';
import 'package:kins_healthcare/layouts/main_tab_body_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/timeline/model/timeline_list_model.dart';
import 'package:kins_healthcare/services/timeline/model/timeline_list_pagination_body.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 * 작성일 : 2021-02-01
 * 작성자 : JC
 * 화면명 : HT_0001
 * 경로 :
 * 클래스 : TimelineScreen
 * 설명 : 타임라인 스크린
 */
class TimelineScreen extends StatefulWidget {
  @override
  _TimelineScreenState createState() => _TimelineScreenState();
}

class _TimelineScreenState extends State<TimelineScreen> {
  late List<String> tabs, typeList;
  List<TimelineListModel> timelineList = [];
  late TimelineController timelineController;
  late MykinsController mykinsController;
  TimelinePaginationBody pageBody = TimelinePaginationBody(timelineType: 'ALL');

  @override
  void initState() {
    mykinsController = Get.find<MykinsController>();
    var test = mykinsController.getFamilyList();
    print(test);
    print(test);
    print(test);
    print(test);
    print(test);
    print(test);
    super.initState();
    tabs = ['전체', '까까', '결제', '관계', '시스템'];
    typeList = ['ALL', 'KKAKKA', 'SETLE', 'RELATION', 'SYSTEM'];
    _setTimelineController();
    _refresh();
  }

  _setTimelineController() {
    timelineController = Get.find<TimelineController>();
    timelineController.addListener(_setStateEndAnimation);
  }

  @override
  void dispose() {
    super.dispose();
    timelineController.removeListener(_setStateEndAnimation);
  }

  @override
  Widget build(BuildContext context) {
    return MainTabBodyLayout(
      title: '타임라인',
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            renderTabs(),
            Expanded(
              child: RefreshIndicator(
                onRefresh: _refresh,
                //child 에게 length를 전달하는 이유?
                child: renderPageListView(
                  timelineController.timelineList.length,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget renderTabs() {
    return CheckTab(
      tabNames: this.tabs,
      onChange: (index) {
        pageBody.timelineType = typeList[index];
        _refresh();
      },
    );
  }

  Widget renderPageListView(int length) {
    //length가 0 인지 확인.
    final isNon = length == 0;

    return PaginationListView<TimelineListModel>(
      //0이면 cont = 1
      itemCount: length,
      itemBuilder: (_, index) => renderListItem(index),
      controller: timelineController,
      request: timelineController.getList,
      lastWidget: Container(),
      loadingWidget: renderLoading(),
    );
  }

  Widget renderListItem(int index) {
    final preIndex = index - 1;
    if (preIndex < 0) return renderDateItem(index);

    final TimelineListModel preModel =
        timelineController.timelineList[preIndex];
    final TimelineListModel model = timelineController.timelineList[index];

    if (preModel.isSameDate(model.timelineDate)) return renderCard(index);
    return renderDateItem(index);
  }

  Widget renderDateItem(int index) {
    final TimelineListModel model = timelineController.timelineList[index];
    final dateString = model.dateString;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 14),
        Text(
          dateString,
          style: TextStyle(
            color: kinsBlack72,
            fontSize: ThemeFactory.of(context).theme.fontSize12,
            fontWeight: FontWeight.w700,
          ),
        ),
        renderCard(index),
      ],
    );
  }

  Widget renderCard(int index) {
    final TimelineListModel model = timelineController.timelineList[index];

    return Padding(
      padding: EdgeInsets.only(top: 14),
      child: InkWell(
        child: TimelineBubbleCard(model: model),
        onTap: () => _itemOnTap(model),
      ),
    );
  }

  Widget renderNonCard() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10, left: 16, right: 16, top: 20.0),
      child: NonKkakkaCard(title: '표시할 타임라인이 없습니다.'),
    );
  }

  Widget renderLoading() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      height: 50,
      child: CircularProgressIndicator(),
    );
  }

  Future<void> _refresh() async {
    await timelineController.getList(body: pageBody, reset: true);
  }

  _itemOnTap(TimelineListModel model) async {
    try {
      await timelineController.check(model.timelineId);

      model.readYn = 'Y';
      _setStateEndAnimation();
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }
}
