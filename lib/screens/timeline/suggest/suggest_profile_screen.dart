import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/btn_bottom_sheet.dart';
import 'package:kins_healthcare/controllers/introduce_controller.dart';
import 'package:kins_healthcare/screens/auth/register/profile/profile_detail_screen.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 * 작성일 : 2021-04-23
 * 작성자 : Mark
 * 화면명 : HY_PROF_A
 * 경로 : /timeline/suggest_profile
 * 클래스 : SuggestProfileScreen
 * 설명 : 소개 수락하기 화면
 */

class SuggestProfileScreen extends StatefulWidget {
  final int introduceId;
  final int userRelNo;
  final String tagUrl;

  const SuggestProfileScreen({
    Key? key,
    required this.userRelNo,
    required this.tagUrl,
    required this.introduceId,
  }) : super(key: key);

  @override
  _SuggestProfileScreenState createState() => _SuggestProfileScreenState();
}

class _SuggestProfileScreenState extends State<SuggestProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: ProfileDetailScreen(
              userRelNo: widget.userRelNo,
              tagUrl: widget.tagUrl,
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 44,
            height: 136,
            child: renderBack(),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            height: 180,
            child: renderBtn(),
          ),
        ],
      ),
    );
  }

  Widget renderBack() {
    return Opacity(
      opacity: 0.4796338762555803,
      child: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(4)),
          color: kinsBlack30,
        ),
      ),
    );
  }

  Widget renderBtn() {
    return InkWell(
      child: renderImage(),
      onTap: _onBtnTap,
    );
  }

  Widget renderImage() {
    return Column(
      children: [
        SizedBox(height: 20),
        SizedBox(
          height: 64,
          width: 64,
          child: SvgPicture.asset(
            'assets/svgs/ic/ic_approve_profile.svg',
            fit: BoxFit.cover,
          ),
        ),
        SizedBox(height: 9),
        Text(
          '소개 허락하기',
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize15,
            fontWeight: FontWeight.w700,
            color: kinsWhite,
          ),
        ),
      ],
    );
  }

  _onBtnTap() async {
    try {
      final model = await Get.find<IntroduceController>().approve(widget.introduceId);

      final sheet = BtnBottomSheet(
        title: model.resultMsg ?? '소개 허락하기 완료',
        isOneBtn: true,
      );
      await Get.bottomSheet(sheet);
      Get.back();
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }
}
