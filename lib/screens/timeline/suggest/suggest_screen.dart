import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/btn_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/text_field/out_line_text_field.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/introduce_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/introduce/model/introduce_relation_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 * 작성일 : 2021-04-13
 * 작성자 : Mark
 * 화면명 : HI_0003
 * 경로 : /timeline/suggest
 * 클래스 : SuggestScreen
 * 설명 : 소개해주기 스크린
 */

class SuggestScreen extends StatefulWidget {
  @override
  _SuggestScreenState createState() => _SuggestScreenState();
}

class _SuggestScreenState extends State<SuggestScreen> {
  final tec = TextEditingController();
  late IntroduceRelationModel model;

  @override
  void initState() {
    super.initState();
    model = Get.arguments;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: CustomScrollLayout(
        topBar: BackTopBar(title: '소개해주기'),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: renderTop(),
            ),
            Container(height: 5, color: kinsGrayE8E),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: renderBottom(),
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
    );
  }

  Widget renderTop() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 30),
        Text(
          '소개받을 사람',
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize12,
            fontWeight: FontWeight.w700,
          ),
        ),
        SizedBox(height: 16),
        renderUserRow(),
        SizedBox(height: 26),
      ],
    );
  }

  Widget renderUserRow() {
    return Row(
      children: [
        CircleNetworkImage(path: model.userImgTarget, imageSize: 46),
        SizedBox(width: 8.0),
        Expanded(child: renderUserContent()),
      ],
    );
  }

  Widget renderUserContent() {
    final targetName = model.userNameTarget ?? '';
    final targetRelation = model.userRelCdMeTarget ?? '';

    final toName = model.userNameTo ?? '';
    final toRelation = model.userRelCdMeTo ?? '';

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        renderTitle(
          toName,
          ' ($toRelation)',
          '님 에게',
        ),
        SizedBox(height: 5),
        renderTitle(
          targetName,
          ' ($targetRelation)',
          '님 을 소개합니다 :)',
        ),
      ],
    );
  }

  Widget renderTitle(String bold, String blue, String normal) {
    return RichText(
      text: TextSpan(
        text: bold,
        style: TextStyle(
          color: kinsBlack30,
          fontWeight: FontWeight.w700,
          fontSize: ThemeFactory.of(context).theme.fontSize14,
        ),
        children: <TextSpan>[
          TextSpan(
            text: blue,
            style: TextStyle(color: kinsBlue40),
          ),
          TextSpan(
            text: normal,
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: ThemeFactory.of(context).theme.fontSize14,
            ),
          ),
        ],
      ),
    );
  }

  Widget renderBottom() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 20),
        Text(
          '소개메세지',
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize13,
            fontWeight: FontWeight.w700,
          ),
        ),
        SizedBox(height: 6),
        renderTextField('상대방에게 보낼 한마디를 남겨보세요!', tec),
        Spacer(),
        SizedBox(height: 16),
        renderSubmitBtn(),
      ],
    );
  }

  Widget renderTextField(String hint, TextEditingController tec) {
    return OutLineTfCS(
      controller: tec,
      hint: hint,
      height: 240,
      outLineColor: kinsBlueD7,
      textColor: kinsBlack,
      inputType: TextInputType.multiline,
      maxLine: 10,
      maxLength: 400,
      hintColor: kinsGrayB6,
      fontSize: ThemeFactory.of(context).theme.fontSize15,
      fontWeight: FontWeight.w400,
      counterText: '',
      radius: 6.0,
    );
  }

  Widget renderSubmitBtn() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      height: 60,
      child: PrimaryButton(
        padding: EdgeInsets.zero,
        label: '다음',
        onTap: _btnOnTap,
      ),
    );
  }

  _btnOnTap() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    final message = tec.text;
    final id = model.introduceReqId!;

    try {
      final data = await Get.find<IntroduceController>().message(id, message);
      final sheet = BtnBottomSheet(
        title: data.resultMsg ?? '소개 해주기',
        isOneBtn: true,
      );
      await Get.bottomSheet(sheet);
      Get.back();
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }
}
