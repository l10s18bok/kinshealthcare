/*
 작성일 : 
 작성자 : 
 화면명 : HP_2002_C
 경로 : 
 업데이트 : baron 
 업데이트 날짜 : 2021-05-03
 클래스 : CalendarScreenResult,
 설명 : 
 */
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/calendar/calendar.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/layouts/title_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class CalendarScreenResult {
  final List<DateTime> range;

  CalendarScreenResult({
    required this.range,
  });
}

class CalendarScreen extends StatefulWidget {
  @override
  _CalendarScreenState createState() => _CalendarScreenState();
}

class _CalendarScreenState extends State<CalendarScreen> {
  late DateTime? rangeStart;
  late DateTime? rangeEnd;
  late DateTime focusedDay;
  int rangeIndex = 0;
  String selectedStartDate = '날짜를 선택해주세요';
  String selectedEndDate = '날짜를 선택해주세요';
  @override
  void initState() {
    super.initState();
    focusedDay = DateTime.now();
    var args = Get.arguments;
    if (args is List<DateTime> && args.length == 2) {
      rangeStart = args[0];
      rangeEnd = args[1];
      selectedStartDate =
          '${rangeStart!.year}년 ${rangeStart!.month}월 ${rangeStart!.day}일';
      selectedEndDate =
          '${rangeEnd!.year}년 ${rangeEnd!.month}월 ${rangeEnd!.day}일';
    }
  }

  onRangeSelected(start, end, focused) {
    setState(() {
      rangeStart = start;
      rangeEnd = end;
      focusedDay = focused;
    });
    if (start != null) {
      setState(() {
        selectedStartDate = '${start!.year}년 ${start!.month}월 ${start!.day}일';
        selectedEndDate = '날짜를 선택해주세요.';
      });
    }
    if (end != null) {
      setState(() {
        selectedEndDate = '${end!.year}년 ${end!.month}월 ${end!.day}일';
      });
    }
  }

// TODO  MainCalendar  State CallBack을 여기서 호출 해서 가져옴.
  List<Widget> renderRange() {
    Map<String, dynamic> data = {
      'child_one_Text_List': ['날짜를 선택해주세요.', '시작일'],
      'child_tow_Text_List': ['날짜를 선택해주세요.', '종료일']
    };
    List<Widget> buildWidget = [];
    final theme = ThemeFactory.of(context).theme;
    Color bgColor = theme.primaryColor;
    Color txtColor = Colors.white;
    Color borderColor = theme.primaryColor;

    data.forEach((key, value) => buildWidget.add(Container(
          width: (MediaQuery.of(context).size.width / 2) - 20,
          decoration: BoxDecoration(
            color: key == 'child_one_Text_List' ? bgColor : Colors.white,
            border: Border.all(
              color: key == 'child_one_Text_List' ? borderColor : Colors.grey,
              width: 1.0,
            ),
            borderRadius: BorderRadius.circular(
              6.0,
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: 8.0,
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      'assets/svgs/ic/ic_calender_select_kkakkapage.svg',
                      color: key == 'child_one_Text_List'
                          ? txtColor
                          : Colors.black,
                    ),
                    Text(
                      key == 'child_one_Text_List'
                          ? selectedStartDate
                          : selectedEndDate,
                      style: TextStyle(
                        fontSize: theme.fontSize15,
                        color: key == 'child_one_Text_List'
                            ? txtColor
                            : Colors.black,
                      ),
                    ),
                  ],
                ),
                Text(
                  selectedStartDate != '' && key == 'child_one_Text_List'
                      ? '부터'
                      : selectedEndDate != '' && key == 'child_tow_Text_List'
                          ? '까지'
                          : value[1],
                  style: TextStyle(
                    fontSize: theme.fontSize13,
                    color:
                        key == 'child_one_Text_List' ? txtColor : Colors.black,
                  ),
                ),
              ],
            ),
          ),
        )));
    return buildWidget;
  }

  renderClendar() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: MainCalendar(
          focusedDay: focusedDay,
          // TODO 외부에서 callback 호출.
          onRangeSelected: onRangeSelected,
          rangeEndDay: rangeEnd,
          rangeStartDay: rangeStart,
        ),
      ),
    );
  }

  renderSubmitBtn() {
    return Container(
      margin: EdgeInsets.all(16),
      height: 60,
      child: PrimaryButton(
        label: '완료',
        onTap: () {
          setState(() {
            //reset text;  >> baron
            selectedStartDate = '날짜를 선택해주세요';
            selectedEndDate = '날짜를 선택해주세요';
          });
          if (rangeStart == null) {
            Get.snackbar('날짜를 선택해주세요', '시작일을 선택해주세요');
            return;
          }

          if (rangeEnd == null) {
            Get.snackbar('날짜를 선택해주세요', '종료일을 선택해주세요');
            return;
          }

          Get.back(
            result: CalendarScreenResult(
              range: [
                rangeStart!,
                rangeEnd!,
              ],
            ),
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return TitleLayout(
      title: '기간 설정',
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: renderRange(),
            ),
          ),
          renderClendar(),
          renderSubmitBtn(),
        ],
      ),
    );
  }
}
