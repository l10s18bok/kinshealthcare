import 'package:flutter/cupertino.dart';
import 'package:kins_healthcare/components/modal/home_option_modal.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-04-21
 * 작성자 : Daniel
 * 화면명 : Coach Mark Main Tab
 * 주요기능 : 코치마크 메인 탭 스크린
 */

class CoachMarkMainTabs extends StatefulWidget {
  final int initialTabIndex;

  CoachMarkMainTabs({
    int initialTabIndex = 2,
  }) : this.initialTabIndex = initialTabIndex;

  @override
  _CoachMarkMainTabsState createState() => _CoachMarkMainTabsState();
}

class _CoachMarkMainTabsState extends State<CoachMarkMainTabs>
    with TickerProviderStateMixin {
  late TabController tabController;
  late int tabIndex;

  late GlobalKey addKey;

  late TutorialCoachMark tutorialCoachMark;
  List<TargetFocus> targets = <TargetFocus>[];

  GlobalKey key = GlobalKey();
  GlobalKey keyButton1 = GlobalKey();
  GlobalKey keyButton2 = GlobalKey();
  GlobalKey keyButton3 = GlobalKey();
  GlobalKey keyButton4 = GlobalKey();

  @override
  initState() {
    addKey = GlobalKey();
    super.initState();
    tabIndex = widget.initialTabIndex;
    tabController = TabController(
      vsync: this,
      length: 5,
      initialIndex: widget.initialTabIndex,
    );
    tabController.addListener(() {
      setState(() {
        tabIndex = tabController.index;
      });
    });
    initTargets();
    showTutorial();
  }

  void initTargets() {
    targets.add(
      coachMarkTarget1(),
    );
    targets.add(
      coachMarkTarget2(),
    );
    targets.add(
      coachMarkTarget3(),
    );
    targets.add(
      coachMarkTarget4(),
    );
  }

  TargetFocus coachMarkTarget1() {
    return TargetFocus(
      identify: "Target 1",
      keyTarget: keyButton1,
      contents: [
        TargetContent(
          align: ContentAlign.top,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    orangeTextSpan('소식, 받은 까까, 보낸 까까'),
                    whiteTextSpan('에 대해\n확인해보세요 :)'),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
      shape: ShapeLightFocus.RRect,
      radius: 5,
    );
  }

  TargetFocus coachMarkTarget2() {
    return TargetFocus(
      identify: "Target 2",
      keyTarget: keyButton2,
      contents: [
        TargetContent(
          align: ContentAlign.top,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    orangeTextSpan('여기'),
                    whiteTextSpan('에서 내용을 확인할 수 있습니다.'),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
      shape: ShapeLightFocus.RRect,
      radius: 5,
    );
  }

  TargetFocus coachMarkTarget3() {
    return TargetFocus(
      identify: "Target 3",
      keyTarget: keyButton3,
      contents: [
        TargetContent(
          align: ContentAlign.left,
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                RichText(
                  text: TextSpan(
                    children: <TextSpan>[
                      orangeTextSpan('\'+\' 버튼'),
                      whiteTextSpan('을 통해 가족에게'),
                    ],
                  ),
                ),
                RichText(
                  text: TextSpan(
                    children: <TextSpan>[
                      whiteTextSpan('용돈을'),
                      orangeTextSpan(' 요청'),
                      whiteTextSpan('하거나 '),
                      orangeTextSpan('발행'),
                      whiteTextSpan('해보세요.'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  TargetFocus coachMarkTarget4() {
    return TargetFocus(
      identify: "Target 4",
      keyTarget: keyButton4,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                RichText(
                  text: TextSpan(
                    children: <TextSpan>[
                      whiteTextSpan('여기를 눌러'),
                      orangeTextSpan(' QR 코드로 결제'),
                      whiteTextSpan(' 할 수 있습니다.'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void showTutorial() {
    tutorialCoachMark = TutorialCoachMark(
      context,
      targets: targets,
      colorShadow: Colors.grey.shade900,
      textSkip: "SKIP",
      paddingFocus: 10,
      opacityShadow: 0.8,
      onFinish: () {
        print("finish");
        Get.offAllNamed('/main-home');
      },
      onClickTarget: (target) {
        print('onClickTarget: $target');
      },
      onSkip: () {
        print("skip");
        Get.offAllNamed('/main-home');
      },
      onClickOverlay: (target) {
        print('onClickOverlay: $target');
      },
    )..show();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Expanded(
              flex: 10,
              child: renderTopInfo(),
            ),
            Expanded(
              flex: 12,
              child: renderMidContents(context),
            ),
            renderBottomNavigationBar(),
          ],
        ),
      ),
    );
  }

  TextSpan whiteTextSpan(_text) {
    return TextSpan(
      text: _text,
      style: TextStyle(
          fontWeight: FontWeight.bold, color: Colors.white, fontSize: 16),
    );
  }

  TextSpan orangeTextSpan(_text) {
    return TextSpan(
      text: _text,
      style: TextStyle(
          fontWeight: FontWeight.bold, color: Color(0xFFF29061), fontSize: 16),
    );
  }

  ListView renderMidContents(BuildContext context) {
    return ListView(
      children: [
        renderPageTap(),
        renderUnderLine(context),
        SizedBox(height: 13),
        renderAllBtn(context),
        SizedBox(height: 7),
        renderListTopBar(context),
        SizedBox(height: 10),
        renderKkakkaCardTitle(),
        SizedBox(height: 12),
        renderContentCard(context),
        // SizedBox(height: 20),
        // Container(height: 1, color: kinsGrayE8E),
      ],
    );
  }

  Column renderTopInfo() {
    return Column(
      children: [
        renderAppBar(),
        SizedBox(
          height: 40,
        ),
        renderWelcome(),
        SizedBox(
          height: 30,
        ),
        renderNotification(),
      ],
    );
  }

  renderTab({
    required String iconName,
    required String name,
    required bool isCurTab,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return Tab(
      icon: SvgPicture.asset(
        'assets/svgs/ic/${iconName}.svg',
      ),
      iconMargin: EdgeInsets.zero,
      child: Text(
        name,
        style: TextStyle(
          fontSize: theme.fontSize9,
          color: isCurTab ? theme.primaryColor : theme.greyPrimaryTextColor,
        ),
      ),
    );
  }

  renderBottomNavigationBar() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      height: 70,
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: theme.primaryGreyColor,
            width: 1.0,
          ),
        ),
      ),
      child: BottomAppBar(
        child: Row(
          children: [
            Expanded(
              child: renderTab(
                iconName: 'ic_payment_gnb_unselect',
                name: '결제',
                isCurTab: false,
              ),
            ),
            Expanded(
              child: renderTab(
                iconName: 'ic_timeline_gnb_unselect',
                name: '타임라인',
                isCurTab: false,
              ),
            ),
            Expanded(
              child: renderTab(
                iconName: 'ic_home_gnb_select',
                name: '홈',
                isCurTab: true,
              ),
            ),
            Expanded(
              child: renderTab(
                iconName: 'ic_information_gnb_unselect',
                name: '맞춤정보',
                isCurTab: false,
              ),
            ),
            Expanded(
              child: renderTab(
                iconName: 'ic_mykins_gnb_unselect',
                name: '마이킨즈',
                isCurTab: false,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container renderContentCard(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;
    return Container(
      height: 200,
      width: MediaQuery.of(context).size.width,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Card(
          key: keyButton2,
          child: Stack(
            children: [
              Positioned(
                child: Align(
                  alignment: Alignment.center,
                  child: SvgPicture.asset(
                    'assets/svgs/img/payment_my_payment.svg',
                    width: 200,
                  ),
                ),
              ),
              Positioned(
                child: Align(
                  alignment: Alignment.topRight,
                  child: Card(
                    margin: EdgeInsets.all(10),
                    color: Colors.amber,
                    child: Container(
                        width: 55,
                        height: 25,
                        child: Center(
                            child: Text(
                          '용돈',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: theme.fontSize14,
                          ),
                        ))),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Padding renderKkakkaCardTitle() {
    final theme = ThemeFactory.of(context).theme;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          SizedBox(height: 15),
          Row(
            children: [
              Stack(
                children: [
                  Container(
                    width: 40,
                    height: 40,
                    decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.blueGrey,
                    ),
                  ),
                  Positioned.fill(
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        width: 25,
                        height: 25,
                        decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                            fit: BoxFit.fill,
                            image: AssetImage('assets/png/popcorn.png'),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '스피너미디어',
                      style: TextStyle(
                        fontSize: theme.fontSize14,
                        fontWeight: theme.heavyFontWeight,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      '28일 남음',
                      style: TextStyle(
                        fontSize: theme.fontSize12,
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Row(
                children: [
                  Text(
                    '9,320',
                    style: TextStyle(
                      fontSize: theme.fontSize22,
                      fontWeight: theme.heavyFontWeight,
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    '원',
                    style: TextStyle(
                      fontSize: theme.fontSize20,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Container renderListTopBar(BuildContext context) {
    return Container(
      height: 30,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          RichText(
            text: TextSpan(
              style: TextStyle(
                fontSize: ThemeFactory.of(context).theme.fontSize12,
                color: kinsBlack,
              ),
              children: <TextSpan>[
                TextSpan(text: '총'),
                TextSpan(
                  text: ' 6',
                  style: TextStyle(
                    fontSize: ThemeFactory.of(context).theme.fontSize14,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                TextSpan(text: '개'),
              ],
            ),
          ),
          Spacer(),
          Text(
            "231,300원",
            style: TextStyle(
              color: kinsBlue40,
              fontWeight: FontWeight.w700,
              fontSize: ThemeFactory.of(context).theme.fontSize20,
            ),
          ),
        ],
      ),
    );
  }

  Widget renderAllBtn(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            '전체보기',
            style: TextStyle(
              fontSize: ThemeFactory.of(context).theme.fontSize12,
              fontWeight: FontWeight.w700,
              color: kinsBlack4F,
            ),
          ),
          SizedBox(width: 3),
          Icon(
            Icons.arrow_forward_ios,
            color: kinsBlack4F,
            size: 12,
          ),
          SizedBox(width: 16),
        ],
      ),
    );
  }

  Row renderUnderLine(BuildContext context) {
    return Row(
      children: [
        Container(width: context.width / 3, height: 1, color: kinsGrayE8E),
        Container(width: context.width / 3, height: 2, color: kinsBlue40),
        Container(width: context.width / 3, height: 1, color: kinsGrayE8E),
      ],
    );
  }

  Widget renderPageTap() {
    return Container(
      height: 50,
      child: Row(key: keyButton1, children: [
        Expanded(
          flex: 1,
          child: Text(
            '소식',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              color: kinsGrayA3,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Expanded(
          flex: 1,
          child: Text(
            '받은 까까',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              color: kinsBlack,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Expanded(
          flex: 1,
          child: Text(
            '보낸 까까',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              color: kinsGrayA3,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ]),
    );
  }

  Widget renderAlertTitle(int length) {
    return RichText(
      text: TextSpan(
        style: TextStyle(
          color: kinsWhite,
          fontSize: ThemeFactory.of(context).theme.fontSize14,
        ),
        children: <TextSpan>[
          TextSpan(text: '🔔 까까 요청 '),
          TextSpan(
            text: ' $length',
            style: TextStyle(
              color: Color(0xFFF29061),
              fontWeight: FontWeight.w700,
              fontSize: ThemeFactory.of(context).theme.fontSize14,
            ),
          ),
          TextSpan(text: '개가 회원님을 기다리고 있어요.'),
        ],
      ),
    );
  }

  Widget renderNotification() {
    return Container(
      height: 48,
      padding: EdgeInsets.symmetric(horizontal: 14),
      margin: EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: kinsBlue40,
      ),
      child: Row(
        children: [
          Expanded(child: renderAlertTitle(7)),
          Icon(
            Icons.arrow_forward_ios,
            color: kinsWhite,
            size: 15,
          ),
        ],
      ),
    );
  }

  Container renderWelcome() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '반가워요,',
            style: TextStyle(
              fontSize: theme.fontSize20,
            ),
          ),
          SizedBox(height: 2),
          Row(
            children: [
              Text(
                '스피너미디어 님 🤗',
                style: TextStyle(
                  fontSize: theme.fontSize20,
                  fontWeight: theme.heavyFontWeight,
                ),
              ),
              Spacer(),
              renderOptionButtons(),
            ],
          ),
        ],
      ),
    );
  }

  Widget renderOptionButtons() {
    return GestureDetector(
      onTap: () {
        final box = addKey.currentContext!.findRenderObject() as RenderBox;
        final Offset position = box.localToGlobal(Offset.zero);

        double y = position.dy;
        double x = position.dx;
        Navigator.of(context)
            .push(HomeOptionModal(x: x, y: y, haveAccount: true));
      },
      child: SvgPicture.asset(
        'assets/svgs/ic/ic_plus_main.svg',
        key: addKey,
      ),
      key: keyButton3,
    );
  }

  Container renderAppBar() {
    final theme = ThemeFactory.of(context).theme;
    return Container(
      padding: EdgeInsets.only(top: 16, left: 16, right: 16),
      child: Row(
        children: [
          SvgPicture.asset(
            'assets/svgs/img/logo_splash.svg',
            color: theme.primaryColor,
            width: 110.0,
          ),
          Spacer(),
          SvgPicture.asset('assets/svgs/ic/ic_qr_main.svg', key: keyButton4),
        ],
      ),
    );
  }
}
