import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/animation/animated_circle_bar_indicator.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
/**
 * 작성일 : 2021-04-23
 * 작성자 : Daniel
 * 화면명 : GuideScreen
 * 목적 : 앱 소개 화면
 */

class GuideScreen extends StatefulWidget {
  @override
  _GuideScreenState createState() => _GuideScreenState();
}

class _GuideScreenState extends State<GuideScreen> {
  final _items = [
    pageContent(
        mainText: '까까 가이드 페이지1 - 사진 첨부될 예정\n(현재는 디자인 미비)', imagePath: ''),
    pageContent(
        mainText: '까까 가이드 페이지2 - 사진 첨부될 예정\n(현재는 디자인 미비)', imagePath: ''),
  ];
  final _currentPageNotifier = ValueNotifier<int>(0);
  final _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: buildSkipButton(),
            ),
            Expanded(
              flex: 8,
              child: buildPageView(),
            ),
            Expanded(
              flex: 1,
              child: renderIndicator(),
            ),
            // padding: EdgeInsets.all(16),
            renderNextButton(),
          ],
        ),
      ),
    );
  }

  Padding renderNextButton() {
    return Padding(
      padding: EdgeInsets.all(16),
      child: PrimaryButton(
        label: '다음',
        onTap: () {
          if (_currentPageNotifier.value == _items.length - 1) {
            Get.offAndToNamed('/coach-mark/main-home');
          }
          _currentPageNotifier.value++;
          final index = _currentPageNotifier.value;
          _pageController.animateToPage(
            index,
            duration: Duration(milliseconds: 250),
            curve: Curves.linear,
          );
        },
      ),
    );
  }

  GestureDetector buildSkipButton() {
    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Align(
          alignment: Alignment.centerRight,
          child: Text(
            'SKIP',
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
      onTap: () {
        Get.offAndToNamed('/coach-mark/main-home');
      },
    );
  }

  Column textField(
    String mainText,
  ) {
    final theme = ThemeFactory.of(context).theme;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          mainText,
          style: TextStyle(color: Colors.white, fontSize: theme.fontSize24),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  buildPageView() {
    return Container(
      color: Colors.black87,
      child: PageView.builder(
          itemCount: _items.length,
          controller: _pageController,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                Expanded(
                  flex: 1,
                  child: textField(
                    _items[_currentPageNotifier.value].mainText,
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Center(
                    child: FlutterLogo(
                      // colors: _items[index],
                      size: 100.0,
                    ),
                  ),
                ),
              ],
            );
          },
          onPageChanged: (int index) {
            setState(() {
              if (_currentPageNotifier.value == _items.length) {
                Get.offAndToNamed('/coach-mark/main-home');
              }
              _currentPageNotifier.value = index;
            });
          }),
    );
  }

  renderIndicator() {
    final theme = ThemeFactory.of(context).theme;
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: CirclePageIndicator(
          size: 8,
          selectedSize: 10,
          selectedBarColor: theme.primaryBlueColor,
          dotColor: Color(0xFFC5C5EA),
          itemCount: _items.length,
          currentPageNotifier: _currentPageNotifier,
          barLength: 8,
        ),
      ),
    );
  }
}

class pageContent {
  final String mainText;
  // final String subText;
  final String imagePath;

  pageContent({required this.mainText, required this.imagePath});
}
