import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/payment_confirm_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/payment_overlay_bottom_sheet.dart';
import 'package:kins_healthcare/components/card/pending_payment_overlay_card.dart';
import 'package:kins_healthcare/controllers/force_controller.dart';
import 'package:kins_healthcare/layouts/layouts.dart';
import 'package:kins_healthcare/screens/advertise/nurse/nurse_screen.dart';
import 'package:kins_healthcare/screens/my_kins/my_kins_screen.dart';
import 'package:kins_healthcare/screens/payment/payment_screen.dart';
import 'package:kins_healthcare/screens/timeline/timeline_screen.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

import 'main_home/new_home_screen.dart';

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : N/A
 * 주요기능 : 메인 탭 스크린
 */
class MainTabs extends StatefulWidget {
  final int initialTabIndex;

  MainTabs({
    int initialTabIndex = 2,
  }) : this.initialTabIndex = initialTabIndex;

  @override
  _MainTabsState createState() => _MainTabsState();
}

class _MainTabsState extends State<MainTabs> with TickerProviderStateMixin {
  late TabController tabController;
  late int tabIndex;
  DateTime? lastPressed;

  // TODO 프로덕션에서 삭제
  bool showOverlay = false;

  @override
  initState() {
    super.initState();

    tabIndex = widget.initialTabIndex;

    tabController = TabController(
      vsync: this,
      length: 5,
      initialIndex: widget.initialTabIndex,
    );

    final c = Get.find<ForceController>();

    tabController.addListener(() {
      setState(() {
        tabIndex = tabController.index;
      });

      c.forceEventUnder(
        reset: true,
      );
    });

    c.forceEventUnder(
      reset: true,
    );
  }

  List<Widget> renderTabBarViewChildren() {
    return [
      PaymentScreen(),
      TimelineScreen(),
      NewHomeScreen(),
      NurseScreen(),
      MyKinsScreen(),
    ];
  }

  renderTabBarView() {
    return Stack(
      children: [
        TabBarView(
          controller: tabController,
          children: renderTabBarViewChildren(),
        ),
        GetBuilder<ForceController>(builder: (c) {
          return Positioned(
            bottom: 0.0,
            child: Container(
              color: Colors.red,
              height: 0.0, //60.0,
              width: MediaQuery.of(context).size.width,
              child: PageView.builder(
                itemCount: c.events.length,
                controller: PageController(viewportFraction: 0.9),
                itemBuilder: (_, index) {
                  return Padding(
                    padding: EdgeInsets.only(
                      bottom: 16.0,
                      right: index == c.events.length - 1 ? 0 : 8.0,
                    ),
                    child: GestureDetector(
                      onTap: () async {
                        final event =
                            await Get.find<ForceController>().forceEventDetail(
                          eventParam: c.events[index].eventParam.toString(),
                        );

                        if (event.isKkakka) {
                          await Get.bottomSheet(
                            PaymentConfirmBottomSheet(
                              event: event,
                            ),
                            isScrollControlled: true,
                          );
                        } else {
                          await Get.bottomSheet(
                            PaymentOverlayBottomSheet(
                              event: event,
                            ),
                            isScrollControlled: true,
                            isDismissible: true,
                          );
                        }
                      },
                      child: PendingPaymentOverlayCard(
                        event: c.events[index],
                      ),
                    ),
                  );
                },
              ),
            ),
          );
        }),
      ],
    );
  }

  // 현재 제플린에서 fill 아이콘이 export 가 안됨.
  // 탭 이름 + '_fill' 로 아이콘 이름을 저장하면
  // 자동으로 렌더
  renderTab({
    required String iconName,
    required String name,
    required int index,
  }) {
    final theme = ThemeFactory.of(context).theme;

    final isCurTab = index == tabIndex;

    if (isCurTab) {
      iconName += '_select';
    } else {
      iconName += '_unselect';
    }

    return Tab(
      icon: SvgPicture.asset(
        'assets/svgs/ic/${iconName}.svg',
      ),
      iconMargin: EdgeInsets.zero,
      child: Text(
        name,
        style: TextStyle(
          fontSize: theme.fontSize9,
          color: isCurTab ? theme.primaryColor : theme.greyPrimaryTextColor,
        ),
      ),
    );
  }

  renderTabs() {
    return <Widget>[
      renderTab(
        iconName: 'ic_payment_gnb',
        name: '결제',
        index: 0,
      ),
      renderTab(
        iconName: 'ic_timeline_gnb',
        name: '타임라인',
        index: 1,
      ),
      renderTab(
        iconName: 'ic_home_gnb',
        name: '홈',
        index: 2,
      ),
      renderTab(
        iconName: 'ic_information_gnb',
        name: '맞춤정보',
        index: 3,
      ),
      renderTab(
        iconName: 'ic_mykins_gnb',
        name: '마이킨즈',
        index: 4,
      ),
    ];
  }

  renderBottomNavigationBar() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: theme.primaryGreyColor,
            width: 1.0,
          ),
        ),
      ),
      child: BottomAppBar(
        child: TabBar(
          controller: tabController,
          tabs: renderTabs(),
          indicator: BoxDecoration(),
        ),
      ),
    );
  }

  doubleTapCloseApp() async {
    final now = DateTime.now();
    final maxDuration = Duration(seconds: 2);
    final isWarning =
        lastPressed == null || now.difference(lastPressed!) > maxDuration;

    if (isWarning) {
      lastPressed = DateTime.now();

      final snackBar = SnackBar(
        content: Text('뒤로가기 두번 탭하면 종료'),
        duration: maxDuration,
      );

      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(snackBar);

      return false;
    } else {
      if (Platform.isAndroid) {
        SystemNavigator.pop();
      } else {
        exit(0);
      }
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => doubleTapCloseApp(),
      child: DefaultLayout(
        body: renderTabBarView(),
        bottomNavigationBar: renderBottomNavigationBar(),
      ),
    );
  }
}
