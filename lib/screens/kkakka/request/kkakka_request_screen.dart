import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/image_picker_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_keyboard_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/sub_btn_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/grid/box_grid.dart';
import 'package:kins_healthcare/components/top_bar/simple_hide_top_bar.dart';
import 'package:kins_healthcare/controllers/kkakka_controller.dart';
import 'package:kins_healthcare/controllers/upload_controller.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/models/local/local_kkakka_model.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/box_grid_model.dart';
import 'package:kins_healthcare/services/kkakka/model/kkakka_request_models.dart';
import 'package:kins_healthcare/services/relation/model/post_relation_search_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

/*
 * 작성일 : 2021-02-17
 * 작성자 : Andy
 * 화면명 : HS_3002(까까 요청)
 * 클래스 : KkakkaRequestScreen
 * 경로  : /kkakka/request/target/edit
 * 설명 : 까까 요청의 내용작성
 */
enum SelectTab { NONE, SEND, LIMITED, CALENDAR }

class KkakkaRequestScreen extends StatefulWidget {
  @override
  _KkakkaRequestScreenState createState() => _KkakkaRequestScreenState();
}

class _KkakkaRequestScreenState extends State<KkakkaRequestScreen> {
  File? image;
  TextEditingController _singleLineController = TextEditingController(); //한줄 편지 내용 저장
  TextEditingController _sendEditController = TextEditingController(); //보낼금액 저장
  ScrollController _scrollController = ScrollController();
  final TextUtils textUtils = TextUtils();

  final models = [
    BoxGridModel(
      title: '기념일',
      imagePath: 'assets/svgs/ic/ic_anniversary_publish_unselect.svg',
      selectedImagePath: 'assets/svgs/ic/ic_anniversary_publish_select.svg',
      ratio: 105 / 90,
    ),
    BoxGridModel(
      title: '용돈',
      imagePath: 'assets/svgs/ic/ic_money_publish_unselect.svg',
      selectedImagePath: 'assets/svgs/ic/ic_money_publish_select.svg',
      ratio: 105 / 90,
    ),
    BoxGridModel(
      title: '의료비',
      imagePath: 'assets/svgs/ic/ic_medical_publish_unselect.svg',
      selectedImagePath: 'assets/svgs/ic/ic_medical_publish_select.svg',
      ratio: 105 / 90,
    ),
  ];
  bool doubleClick = false; // 다음버튼 중복 탭 방지
  bool _sendAmountClick = false; //보낼금액 클릭 상태 저장
  String? _sendErrText = null; // 보낼금액 에러 알림 ..
  late SearchRelResponse _user; // 보낼 유저 정보 저장
  late LocalKkakkaModel localKkakkaModel; // 보낼 유저정보 + 카테고리(이전페이지에서 받은 정보)

  @override
  void initState() {
    super.initState();
    localKkakkaModel = Get.arguments;
    if (localKkakkaModel.userRelation != null) {
      _user = localKkakkaModel.userRelation!;
    } else {
      _user = SearchRelResponse(
        profileImgOpponent: '',
        userNameOpponent: 'Andy',
        userRelCdOpponentValue: '',
        userPhoneOpponent: '01012345678',
        userRelCdMeValue: '',
        userNoRel: 0,
        userStatusOpponent: '',
      );
    }
    models[1].isSelect = true; // 2021.04.26 기본 카테고리 용돈으로 ..
    localKkakkaModel.type = localKkakkaModel.getTitleToKkakkaType('용돈');
  }

  @override
  void dispose() {
    _singleLineController.dispose();
    _sendEditController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  _onGridSelectModel(BoxGridModel model) {
    setState(() {
      localKkakkaModel.type = localKkakkaModel.getTitleToKkakkaType(model.title);
    });
  }

  _sendData({String? upLoadImgPath}) {
    final kkakkaType = localKkakkaModel.kkakkaType;

    if (_sendEditController.text.trim() == '0원') {
      Get.snackbar('알림', '요청금액을 입력하세요');
      return;
    }
    String delWon = _sendEditController.text.replaceAll('원', '');
    final priceL = delWon.replaceAll(',', '');
    final priceKkakka = int.tryParse(priceL);

    final sendData = PostKkakkaRequestRequestBody(
      kkakkaImage: upLoadImgPath,
      kkakkaMessage: _singleLineController.text,
      kkakkaPrice: priceKkakka,
      kkakkaType: kkakkaType,
      endDate: null,
      startDate: null,
      userNoRel: _user.userNoRel,
    );
    print('전송데이터 : ${sendData.toJson()}');
    return sendData;
  }

  _showBottomSubBtnSheet() async {
    if (_sendEditController.text.isEmpty || _sendEditController.text == '0 원') {
      _sendErrText = '보낼금액을 입력하세요';
      setState(() {});
      return;
    }

    if (doubleClick) return;
    doubleClick = true;

    Future.microtask(() async {
      try {
        String? _upLoadImgPath;
        if (image != null) {
          _upLoadImgPath = await Get.find<UploadController>().uploadImage(image: image!);
        }
        print('=========> _upLodePath : ${_upLoadImgPath}');

        final modelData = _sendData(upLoadImgPath: _upLoadImgPath);

        final resp = await Get.find<KkakkaController>().requestKkakka(modelData);
        if (resp == 'SUCCESS') {
          final sheet = SubBtnBottomSheet(
            boldTitle: _user.userNameOpponent,
            blueTitle: _user.userRelCdOpponentValue,
            boldTitle02: '에게',
            title: '까까 요청이 완료되었습니다.',
          );

          await Get.bottomSheet<String>(sheet);
          Get.offAllNamed('/main-home');
        } else
          Get.snackbar('실패', resp);
      } on DioError catch (e) {
        doubleClick = false;
        final msg = e.response!.data.toString();
        final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
        Get.bottomSheet(sheet);
        return;
      }
    });
  }

  _onFocusChange() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final result = await Get.bottomSheet<MoneyBottomSheetResult>(MoneyKeyboardBottomSheet(), isScrollControlled: true);

    return result;
  }

  _onTabSendAmount() async {
    _sendAmountClick = true;
    final result = await _onFocusChange();
    if (result == null) return;
    _sendEditController.text = textUtils.numberToLocalCurrency(amount: result.amount) + ' 원';

    if (_sendEditController.text != '0 원') _sendErrText = null;

    print('sendAmountText : ${_sendEditController.text}');
    setState(() {});
  }

  renderLabel(String label, {String? subLabel}) {
    final theme = ThemeFactory.of(context).theme;
    return Row(
      children: [
        Text(
          label,
          style: TextStyle(fontSize: theme.fontSize13, color: Colors.black, fontWeight: theme.heavyFontWeight),
        ),
        SizedBox(width: 4),
        Text(
          subLabel ?? '',
          style: TextStyle(fontSize: theme.fontSize13, color: kinsGrayB6),
        ),
      ],
    );
  }

  renderAnimationTopBar() {
    return SimpleHideTopBar(
      title: '까까 요청하기',
      scrollController: _scrollController,
    );
  }

  _getImage() async {
    final ImagePickerBottomSheetResult? resp = await Get.bottomSheet(
      ImagePickerBottomSheet(
        processImage: true,
      ),
    );

    if (resp == null) return;

    setState(() {
      image = resp.file;
    });
  }

  renderCameraBox() {
    final theme = ThemeFactory.of(context).theme;
    return InkWell(
      onTap: () {
        _getImage();
      },
      child: Container(
          height: 72,
          width: 72,
          decoration: BoxDecoration(
            border: Border.all(
              color: Color(0xFFD7D8EE),
              width: 1,
            ),
            borderRadius: BorderRadius.circular(6),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                'assets/svgs/ic/ic_camera.svg',
              ),
              Text(
                '0/1',
                style: TextStyle(fontSize: theme.fontSize12, color: Color(0xFF9596A7), fontWeight: theme.heavyFontWeight),
              ),
            ],
          )),
    );
  }

  renderImageBox() {
    return InkWell(
      onTap: () {
        setState(() {
          image = null;
        });
      },
      child: Container(
        height: 80,
        width: 80,
        child: Stack(
          alignment: Alignment.centerLeft,
          children: [
            Container(
              width: 72,
              height: 72,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color(0xFFD7D8EE),
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(6),
              ),
              child: Image.file(image!, fit: BoxFit.cover),
            ),
            Positioned(
              right: 0,
              top: 0,
              child: SvgPicture.asset(
                'assets/svgs/ic/ic_delete.svg',
                width: 20,
                height: 20,
              ),
            ),
          ],
        ),
      ),
    );
  }

  renderTopPhotoSelect() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        renderLabel('카드 사진'),
        SizedBox(height: 8),
        image == null ? renderCameraBox() : renderImageBox(),
      ],
    );
  }

  renderInputTextFild() {
    final theme = ThemeFactory.of(context).theme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        renderLabel('한 줄 편지', subLabel: '선택'),
        SizedBox(height: 8),
        TextField(
          controller: _singleLineController,
          maxLines: 2,
          keyboardType: TextInputType.multiline,
          textInputAction: TextInputAction.done,
          decoration: InputDecoration(
            border: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFFD7D8EE), width: 1), borderRadius: BorderRadius.circular(6.0)),
            //contentPadding: const EdgeInsets.all(0),
            hintStyle: TextStyle(color: Color(0xFFB6B6B6), fontSize: theme.fontSize15),
            hintText: '회원님의 마음을 입력해주세요.',
          ),
        ),
        SizedBox(height: 26),
      ],
    );
  }

  renderSendPerson({required String title, double? topMargin, double? bottomMargin}) {
    final theme = ThemeFactory.of(context).theme;
    return Column(
      children: [
        renderLabel(title),
        ListTile(
          contentPadding: const EdgeInsets.all(0),
          leading: CircleNetworkImage(
            imageSize: 40,
            path: _user.profileImgOpponent,
            userNo: _user.userNoRel,
            heroTag: _user.profileImgOpponent,
          ),
          //CircleAvatar(backgroundImage: CachedNetworkImageProvider(_user.profileImgOpponent!)),
          title: RichText(
            text: TextSpan(
              text: _user.userNameOpponent,
              children: [
                TextSpan(
                  text: ' ${_user.userRelCdOpponentValue}',
                  style: TextStyle(
                    color: Color(0xFF4042AB),
                    fontSize: theme.fontSize12,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
              style: TextStyle(
                color: Color(0xFF363636),
                fontSize: theme.fontSize14,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          subtitle: Text(_user.userPhoneOpponent,
              style: TextStyle(
                color: Color(0xFF535353),
              )),
        ),
      ],
    );
  }

  renderUnderLineContainer({TextEditingController? controller, required String hintText, bool hintBlack = false, String? errorText}) {
    final theme = ThemeFactory.of(context).theme;
    return AbsorbPointer(
      child: TextField(
        controller: controller,
        style: TextStyle(
          color: hintBlack ? Colors.black : Color(0xFFD6D6D6),
          fontSize: theme.fontSize15,
        ),
        decoration: InputDecoration(
          hintText: hintText,
          errorText: errorText,
          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
        ),
      ),
    );
  }

  renderSelectTextField(
      {required String label,
      SelectTab? selectTab,
      TextEditingController? controller,
      required String hintText,
      String? errorText,
      required Function onTab}) {
    bool black = false;
    if (selectTab == SelectTab.SEND && _sendAmountClick) {
      black = true;
    }

    return InkWell(
      onTap: () => onTab(),
      child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        renderLabel(label),
        SizedBox(height: 10),
        renderUnderLineContainer(controller: controller, hintText: hintText, errorText: errorText, hintBlack: black),
      ]),
    );
  }

  renderCategory() {
    return Column(
      children: [
        SizedBox(height: 20),
        renderLabel('카테고리'),
        SizedBox(height: 8),
        BoxGrid(
          rowCount: 3,
          models: models,
          onLastSelectItem: _onGridSelectModel,
        ),
        SizedBox(height: 20),
      ],
    );
  }

  renderNextButton() {
    return PrimaryButton(
      label: "다음",
      onTap: _showBottomSubBtnSheet,
    );
  }

  renderTop() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          SizedBox(height: 30),
          renderTopPhotoSelect(),
          SizedBox(height: 20),
          renderInputTextFild(),
          renderSendPerson(title: '받는 분', topMargin: 18, bottomMargin: 21),
        ],
      ),
    );
  }

  renderDivider() {
    return Container(
      height: 6.0,
      color: Color(0xFFEFEFF4),
      child: Align(
        alignment: Alignment.topCenter,
        child: Container(
          height: 1,
          color: Color(0xFFE8E7EC),
        ),
      ),
    );
  }

  renderBottom() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          renderCategory(),
          renderSelectTextField(
            label: '요청 금액',
            controller: _sendEditController,
            selectTab: SelectTab.SEND,
            hintText: '0원',
            errorText: _sendErrText,
            onTab: _onTabSendAmount,
          ),
          SizedBox(height: 40),
          renderNextButton(),
          SizedBox(height: 16),
        ],
      ),
    );
  }

  renderUpLoadingProgress() {
    final theme = ThemeFactory.of(context).theme;
    return Center(
      child: Container(
        padding: const EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: Colors.black38,
          borderRadius: BorderRadius.circular(6),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            CircularProgressIndicator(
              backgroundColor: Colors.yellow,
            ),
            SizedBox(height: 35),
            Text(
              '잠시만 기다려주세요',
              style: TextStyle(color: Colors.white, fontSize: theme.fontSize16, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<UploadController>(builder: (c) {
      return DefaultLayout(
        body: Stack(
          children: [
            GestureDetector(
              onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
              child: Column(
                children: [
                  renderAnimationTopBar(),
                  Expanded(
                    child: ListView(
                      controller: _scrollController,
                      children: [
                        renderTop(),
                        renderDivider(),
                        renderBottom(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            c.isLoading ? renderUpLoadingProgress() : Container(),
          ],
        ),
      );
    });
  }
}
