import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/components/text/kkakka_type_translate.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/kkakka/model/kkakka_request_list_models.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/controllers/kkakka_controller.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';

class KkakkaRequestListScreen extends StatefulWidget {
  @override
  _KkakkaRequestListScreenState createState() =>
      _KkakkaRequestListScreenState();
}

class _KkakkaRequestListScreenState extends State<KkakkaRequestListScreen> {
  late KkakkaController _controller;
  int? kkakkaCount;
  ScrollController? _scrollController;
  @override
  void initState() {
    _controller = Get.find<KkakkaController>();
    _scrollController = ScrollController();
    requestKkakkaCount();
    initResult();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController!.dispose();
    super.dispose();
  }

  initResult() {
    GeneralPaginationBody body = GeneralPaginationBody().copyWith(sortBy: 'id');
    _controller.requestList(reset: true, body: body).then((value) {
      setState(() {});
    });
  }

  Widget renderNonCard() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10, left: 16, right: 16, top: 20.0),
      child: NonKkakkaCard(title: '받은 \'까까\'가 없습니다.'),
    );
  }

  requestKkakkaCount() async {
    await _controller.getReqKkakka().then((value) {
      setState(() {
        kkakkaCount = value;
      });
    });
  }

  renderLoading() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      height: 50,
      child: CircularProgressIndicator(),
    );
  }

  // nextPage() async {
  //   if (_controller.hasMoreKkakkaRequestPage != null &&
  //       _controller.hasMoreKkakkaRequestPage) {
  //     await _controller.requestKkakkaRequestList(reset: false).then((value) {
  //       setState(() {});
  //     });
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            GestureDetector(
                onTap: () => Get.back(),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: SvgPicture.asset(
                    'assets/svgs/ic/ic_arrowback_common.svg',
                    width: 24,
                    height: 24,
                    color: kinsGrayA3,
                  ),
                )),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 30, 0, 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '요청온 까까',
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.w700,
                        fontFamily: 'Noto Sans KR'),
                  ),
                  Row(
                    children: [
                      Text(
                        '${kkakkaCount} 개',
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            color: Color.fromRGBO(64, 66, 171, 1)),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Expanded(
                child: NotificationListener<ScrollNotification>(
                    onNotification: (scrollNotification) {
                      if (scrollNotification is ScrollStartNotification) {
                      } else if (scrollNotification
                          is ScrollUpdateNotification) {
                      } else if (scrollNotification is ScrollEndNotification) {
                        if (scrollNotification.metrics.maxScrollExtent ==
                            scrollNotification.metrics.pixels) {
                          // nextPage();
                        }
                      }
                      return false;
                    },
                    child: PaginationListView<ListRequestModel>(
                      controller: _controller,
                      itemCount: _controller.cache_listRequest.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () async {
                            // await Get.toNamed('/main-home/request/detail',
                            //     arguments: _controller
                            //         .kkakkaRequestList[index].kkakkaReqId!);
                          },
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                            child: Container(
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      CachedNetworkImage(
                                        imageUrl:
                                            '${_controller.cache_listRequest[index].profileImgOpponent}',
                                        imageBuilder: (context, image) =>
                                            CircleAvatar(
                                          backgroundImage: image,
                                          radius: 25,
                                          backgroundColor: Colors.indigo,
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 12),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Text(
                                              '${_controller.cache_listRequest[index].userNameOpponent}',
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.w700,
                                                  fontFamily: 'Noto Sans KR'),
                                            ),
                                            Text(
                                              '${KkakkaTypeTranslate(model: _controller.cache_listRequest[index]).daysTillreqDate.toString()} 일 전',
                                              style: TextStyle(
                                                  fontSize: 11,
                                                  fontWeight: FontWeight.normal,
                                                  fontFamily: 'Noto Sans KR'),
                                            )
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                          child: Padding(
                                        padding:
                                            const EdgeInsets.only(right: 20),
                                        child: Align(
                                          alignment: Alignment.centerRight,
                                          child: Text(
                                            _controller.cache_listRequest[index]
                                                        .kkakkaPrice !=
                                                    null
                                                ? '${KkakkaTypeTranslate(model: _controller.cache_listRequest[index]).price(_controller.cache_listRequest[index].kkakkaPrice)} 원'
                                                : '기부금액 미설정',
                                            style: TextStyle(
                                                fontSize: 24,
                                                fontWeight: FontWeight.w600,
                                                fontFamily: 'Noto Sans KR'),
                                          ),
                                        ),
                                      ))
                                    ],
                                  ),
                                  SizedBox(
                                    height: 24,
                                  ),
                                  CachedNetworkImage(
                                    imageUrl:
                                        '${_controller.cache_listRequest[index].kkakkaImage}',
                                    imageBuilder: ((context, image) =>
                                        Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              .25,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                image: image,
                                                fit: BoxFit.cover,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Stack(
                                            children: [
                                              Positioned(
                                                  top: 10,
                                                  right: 10,
                                                  child: Container(
                                                      height: 26,
                                                      width: 61,
                                                      decoration: BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(4),
                                                          color: KkakkaTypeTranslate(
                                                                  model: _controller
                                                                          .cache_listRequest[
                                                                      index])
                                                              .primaryColor),
                                                      child: Center(
                                                        child: Text(
                                                          '${_controller.cache_listRequest[index].kkakkaType}',
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white),
                                                        ),
                                                      )))
                                            ],
                                          ),
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                      emptyWidget: renderNonCard(),
                      lastWidget: Container(),
                      request: _controller.requestList,
                    ))),
          ],
        ),
      ),
    );
  }
}
