import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/controllers/relation_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/models/local/local_kkakka_model.dart';
import 'package:kins_healthcare/services/relation/model/post_relation_search_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

import '../../../components/top_bar/back_top_bar.dart';

/*
 작성일 : 2021-01-26
 작성자 : Mark,
 화면명 : HU_4001,
 경로 : /kkakka/issuance/target
 클래스 : IssuanceTargetScreen,
 설명 : 까까 발행할 가족을 선택하는 Screen
*/

class IssuanceTargetScreen extends StatefulWidget {
  @override
  IssuanceTargetState createState() => IssuanceTargetState();
}

class IssuanceTargetState extends State<IssuanceTargetScreen> {
  int curRequestId = 0;
  bool checkFlag = false;
  SearchRelResponse? targetModel;
  late RelationController relationController;

  @override
  void initState() {
    super.initState();
    _setRelationController();
  }

  _setRelationController() {
    relationController = Get.find<RelationController>();
    relationController.addListener(_setStateEndAnimation);
    relationController.searchRelSend(reset: true);
  }

  @override
  void dispose() {
    super.dispose();
    relationController.removeListener(_setStateEndAnimation);
  }

  @override
  Widget build(BuildContext context) {
    final length = relationController.relationsSend.length;

    return NestedScrollLayout(
      topBar: BackTopBar(title: '까까 보내기'),
      body: Padding(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: renderPageListView(length),
            ),
            renderSubmitBtn(),
          ],
        ),
      ),
    );
  }

  Widget renderPageListView(int length) {
    final isNon = length == 0;

    return PaginationListView<SearchRelResponse>(
      itemCount: isNon ? 1 : length,
      itemBuilder: (_, index) {
        if (index == 0) return renderFirstListItem(index, isNon);
        return renderListItem(index);
      },
      controller: relationController,
      request: relationController.searchRelSend,
      lastWidget: Container(),
      loadingWidget: renderLoading(),
    );
  }

  Widget renderFirstListItem(int index, bool isNon) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 30),
        renderTitle(),
        SizedBox(height: 30),
        isNon ? renderNonCard() : renderListItem(index),
      ],
    );
  }

  Widget renderNonCard() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10, left: 16, right: 16, top: 0),
      child: NonKkakkaCard(title: '까까 발행할 가족이 없습니다.'),
    );
  }

  Widget renderTitle() {
    return RichText(
      text: TextSpan(
        text: '어떤 분께\n보내시겠어요? ',
        style: TextStyle(
          color: kinsBlack30,
          fontWeight: FontWeight.w700,
          fontSize: ThemeFactory.of(context).theme.fontSize17,
        ),
        children: <TextSpan>[
          TextSpan(
            text: '(한분만 선택가능)',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: ThemeFactory.of(context).theme.fontSize15,
            ),
          ),
        ],
      ),
    );
  }

  Widget renderListItem(int index) {
    final model = relationController.relationsSend[index];

    return InkWell(
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: kinsGrayEA, width: 1.0),
          ),
        ),
        height: 84,
        child: Row(
          children: [
            CircleNetworkImage(
              imageSize: 56,
              path: model.profileImgOpponent!,
              userNo: model.userNoRel,
              heroTag: model.profileImgOpponent! + model.userNoRel.toString(),
            ),
            SizedBox(width: 16),
            Expanded(child: renderItemContent(model)),
            SizedBox(width: 16),
            renderCheckBtn(model),
          ],
        ),
      ),
      onTap: () {
        setState(() {
          checkFlag = true;
          curRequestId = model.userNoRel!;
          targetModel = model;
        });
      },
    );
  }

  Widget renderItemContent(SearchRelResponse model) {
    final name = model.userNameOpponent;
    final nickName = model.userRelCdOpponentValue;
    final phoneNumber = model.userPhoneOpponent;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '$name ($nickName)',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
          ),
        ),
        SizedBox(height: 10),
        Text(
          phoneNumber,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
          ),
        ),
      ],
    );
  }

  renderCheckBtn(SearchRelResponse model) {
    final backgroundColor =
        curRequestId == model.userNoRel ? kinsBlue40 : kinsBlueCF;

    return Container(
      height: 26,
      child: Row(
        children: [
          AnimatedContainer(
            alignment: Alignment.center,
            duration: Duration(milliseconds: 100),
            width: 26,
            height: 26,
            decoration: BoxDecoration(
              color: backgroundColor,
              borderRadius: BorderRadius.all(Radius.circular(4)),
              border: Border.all(color: kinsGrayC2),
            ),
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_checkbox_unselect.svg',
              width: 12,
              height: 10,
            ),
          ),
        ],
      ),
    );
  }

  Widget renderLoading() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      height: 50,
      child: CircularProgressIndicator(),
    );
  }

  Widget renderSubmitBtn() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      height: 60,
      child: PrimaryButton(
        label: '다음',
        onTap: checkFlag ? _nextBtnOnTap : null,
      ),
    );
  }

  _nextBtnOnTap() async {
    final LocalKkakkaModel localKkakkaModel = LocalKkakkaModel();
    localKkakkaModel.userRelation = targetModel;
    Get.toNamed(
      '/kkakka/issuance/target/detail',
      arguments: localKkakkaModel,
    );
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }
}
