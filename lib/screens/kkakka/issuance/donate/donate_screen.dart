import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/card/donate_request_card.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/donate_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/phil/model/donate_model.dart';
import 'package:kins_healthcare/services/donate/post_donate_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-02-08
 작성자 : phil
 화면명 : HU_3001
 경로 : /kkakka/issuance/donate
 클래스 : RelationService
 설명 : 기부 까까 화면
*/

class DonateScreen extends StatefulWidget {
  @override
  _DonateScreenState createState() => _DonateScreenState();
}

class _DonateScreenState extends State<DonateScreen> {
  int curRequestId = 0;
  List<DonateModel> mDonateModel = [];
  List<PostDonateResponse> listDonation = [];
  PostDonateResponse? targetModel;
  bool checkFlag = false;
  String iconPath = 'assets/svgs/ic/ic_tree_level_01.svg';
  String treeLevel = '3단계';
  int targetId = 0;

  @override
  void initState() {
    super.initState();

    Future.microtask(() async {
      final resp = await Get.find<DonateController>().listDonations();
      listDonation = resp.map((e) => e).toList();

      setState(() {
        this.listDonation = listDonation;
      });
    });
  }

  renderSubTitle() {
    final theme = ThemeFactory.of(context).theme;

    return Text("어떤 기관에\n기부를 하시겠어요?",
        style: TextStyle(
            color: Color(0xff303030),
            fontWeight: FontWeight.w700,
            fontFamily: "AppleSDGothicNeo",
            fontStyle: FontStyle.normal,
            fontSize: theme.fontSize17));
  }

  //바텀 버튼
  renderButton() {
    return checkFlag
        ? PrimaryButton(
            label: '다음',
            onTap: () {
              Get.toNamed('/kkakka/issuance/donate/detail', arguments: targetId);
            },
          )
        : PrimaryButton(label: '다음', onTap: null);
  }

  Widget renderListItem(PostDonateResponse model) {
    return InkWell(
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: DonateRequestCard(
                  id: model.donationId!,
                  title: model.userName!,
                  subTitle: model.donationTitle,
                  iconPath: this.iconPath,
                  isPrimaryColor: curRequestId == model.donationId,
                  percent: (model.sumPrice! / model.totalPrice!) * 100,
                  treeLevel: this.treeLevel,
                  onTap: () {
                    setState(() {
                      targetId = model.donationId!;
                      curRequestId = model.donationId!;
                      checkFlag = true;
                    });
                  },
                ),
              ),
            ],
          ),
          Container(
            height: 10,
          )
        ],
      ),
      onTap: () {
        setState(() {
          // curRequestId = model.userNoRel;
          targetModel = model;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(title: '까까로 기부'),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 16.0,
        ),
        child: Column(
          // mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              child: ListView(
                physics: ClampingScrollPhysics(),
                children: [
                  Container(height: 30.0),
                  renderSubTitle(),
                  Container(height: 30.0),
                  ...listDonation.map((e) => renderListItem(e)).toList(),
                ],
              ),
            ),
            renderButton(),
            Container(height: 8.0),
          ],
        ),
      ),
    );
  }

// @override
// Widget build(BuildContext context) {
//   return TitleLayout(
//     title: '까까로 기부',
//     body: Padding(
//       padding: EdgeInsets.symmetric(
//         horizontal: 16.0,
//       ),
//       child: Column(
//         // mainAxisSize: MainAxisSize.max,
//         children: [
//           Expanded(
//             child: ListView(
//               physics: ClampingScrollPhysics(),
//               children: [
//                 renderSubTitle(),
//                 Container(height: 30.0),
//                 ...listDonation.map((e) => renderListItem(e)).toList(),
//                 ...listDonation.map((e) => renderListItem(e)).toList(),
//               ],
//             ),
//           ),
//           renderButton(),
//           Container(height: 8.0),
//         ],
//       ),
//     ),
//   );
// }
}
