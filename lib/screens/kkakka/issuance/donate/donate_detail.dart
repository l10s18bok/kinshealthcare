import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/donate_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/phil/components/text/sub_title.dart';
import 'package:kins_healthcare/services/donate/post_donate_detail_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-02-08
 작성자 : phil
 화면명 : HU_3001_2
 경로 : /kkakka/issuance/donate/detail
 클래스 : DonateDetail
 설명 : 기부 까까 작성 화면
*/

class DonateDetail extends StatefulWidget {
  @override
  _DonateDetailState createState() => _DonateDetailState();
}

class _DonateDetailState extends State<DonateDetail> {
  late ScrollController topListViewController;
  late GlobalKey topListViewKey;
  PostDonateDetailResponse? donation;

  // List<PostDonateDetailResponse> donation =  [];

  double tLVScrollPercent = 0;
  double scrollWidth = 50.0;
  double scrollXPos = 0;
  List text = ['까까로 기부', '기부액'];
  double sumPrice = 10;
  double totalPrice = 20;
  int sumPay = 10;
  int totalPay = 20;
  List photoData = [];

  // List data = Get.arguments;

  @override
  void initState() {
    super.initState();

    Future.microtask(() async {
      final donation =
          await Get.find<DonateController>().detailDonate(Get.arguments);
      // donation.userName = resp.userName;

      setState(() {
        this.donation = donation;
      });
    });

    topListViewKey = GlobalKey();
    topListViewController = ScrollController();

    topListViewController.addListener(() {
      final box =
          topListViewKey.currentContext!.findRenderObject() as RenderBox;
      final tWidth = box.size.width;

      double perc = topListViewController.offset /
          topListViewController.position.maxScrollExtent;

      if (perc < 0) {
        perc = 0;
      }

      if (perc > 1) {
        perc = 1;
      }

      setState(() {
        tLVScrollPercent = perc;
        scrollXPos = (tWidth - scrollWidth) * perc;
      });
    });
  }

  renderDonateDetail(PostDonateDetailResponse? donation) {
    final rootPath = 'assets/svgs/ic/';
    final theme = ThemeFactory.of(context).theme;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(donation?.userName ?? "",
                style: TextStyle(
                  color: Color(0xff4042ab),
                  fontWeight: FontWeight.w700,
                  fontFamily: "AppleSDGothicNeo",
                  fontStyle: FontStyle.normal,
                  fontSize: theme.fontSize17,
                )),
            Container(height: 4),
            Text(donation?.donationContents ?? "",
                style: TextStyle(
                  color: Color(0xff3e3e3e),
                  fontWeight: FontWeight.w400,
                  fontFamily: "AppleSDGothicNeo",
                  fontStyle: FontStyle.normal,
                  fontSize: theme.fontSize11,
                )),
          ],
        ),
        Column(
          children: [
            SvgPicture.asset(
              rootPath + 'ic_tree_level_01.svg',
              height: 37,
              width: 37,
            ),
            Container(height: 4),
            Text("3단계",
                style: TextStyle(
                  color: Color(0xff3e3e3e),
                  fontWeight: FontWeight.w700,
                  fontFamily: "AppleSDGothicNeo",
                  fontStyle: FontStyle.normal,
                  fontSize: theme.fontSize10,
                ),
                textAlign: TextAlign.center)
          ],
        )
      ],
    );
  }

  renderDonatePrice() {
    final theme = ThemeFactory.of(context).theme;

    sumPay = donation?.sumPrice?.toInt() ?? 1;
    totalPay = donation?.totalPrice?.toInt() ?? 1;

    return Column(
      children: [
        Row(
          children: [
            Text("모은 금액",
                style: TextStyle(
                    color: Color(0xff303030),
                    fontWeight: FontWeight.w700,
                    fontFamily: "AppleSDGothicNeo",
                    fontStyle: FontStyle.normal,
                    fontSize: theme.fontSize12)),
            Container(width: 8),
            Text(NumberFormat('#,###').format(sumPay).toString() + "원",
                style: TextStyle(
                    color: Color(0xff303030),
                    fontWeight: FontWeight.w400,
                    fontFamily: "AppleSDGothicNeo",
                    fontStyle: FontStyle.normal,
                    fontSize: theme.fontSize12)),
          ],
        ),
        Container(height: 6),
        Row(
          children: [
            Text("목표 금액",
                style: TextStyle(
                    color: Color(0xff303030),
                    fontWeight: FontWeight.w700,
                    fontFamily: "AppleSDGothicNeo",
                    fontStyle: FontStyle.normal,
                    fontSize: theme.fontSize12)),
            Container(width: 8),
            Text(NumberFormat('#,###').format(totalPay).toString() + " 원",
                style: TextStyle(
                    color: Color(0xff4042ab),
                    fontWeight: FontWeight.w700,
                    fontFamily: "AppleSDGothicNeo",
                    fontStyle: FontStyle.normal,
                    fontSize: theme.fontSize12))
          ],
        )
      ],
    );
  }

  renderProgress() {
    sumPrice = donation?.sumPrice ?? 1;
    totalPrice = donation?.totalPrice ?? 1;
    return Stack(
      children: [
        Container(
            height: 7,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(3.5)),
                color: const Color(0xffe4e4ed))),
        Container(
            width: (sumPrice / totalPrice) * 360,
            height: 7,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(3.5)),
                color: const Color(0xff4042ab))),
      ],
    );
  }

  renderDescription() {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("모금 소개",
            style: TextStyle(
                color: Color(0xff2e2e2e),
                fontWeight: FontWeight.w700,
                fontFamily: "AppleSDGothicNeo",
                fontStyle: FontStyle.normal,
                fontSize: theme.fontSize12)),
        Container(height: 10),
        Stack(
          children: [
            Container(
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(7)),
                    border: Border.all(
                        color: const Color(0xffccd1d5), width: 1.4))),
            Container(
              padding: EdgeInsets.only(left: 15, right: 15, top: 15),
              child: Text("어린이의 정학 및 결식 아동을 위해 사용되는 기부금 입니다. 많은 도움 부탁드려요",
                  style: TextStyle(
                    color: Color(0xff262626),
                    fontWeight: FontWeight.w400,
                    fontFamily: "AppleSDGothicNeo",
                    fontStyle: FontStyle.normal,
                    fontSize: theme.fontSize15,
                  )),
            )
          ],
        ),
        Container(height: 34),
      ],
    );
  }

  renderImageText() {
    final theme = ThemeFactory.of(context).theme;
    return Text("관련 이미지",
        style: TextStyle(
            color: Color(0xff2e2e2e),
            fontWeight: FontWeight.w700,
            fontFamily: "AppleSDGothicNeo",
            fontStyle: FontStyle.normal,
            fontSize: theme.fontSize12));
  }

  renderImageContents() {
    return IntrinsicHeight(
      child: Row(
        children: [renderImages()],
      ),
    );
  }

  renderImages() {
    //
    // print(donation.photoData[0].toString());
    // print(photoData.toString());
    // photoData = donation.photoData.map((e) => e).toList();
    // print(photoData.toString());
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            height: 106,
            child: ListView(
              controller: topListViewController,
              scrollDirection: Axis.horizontal,
              children: List.filled(
                5,
                Padding(
                  padding: EdgeInsets.only(right: 8.0),
                  // child: CachedNetworkImage(
                  //   imageUrl: widget.kkakka.kkakkaImage,
                  //   fit: BoxFit.cover,
                  // ),
                  child: SvgPicture.asset(
                    'assets/svgs/ic/ic_tree_level_01.svg',
                    height: 106,
                    width: 200,
                  ),
                ),
              ),
            ),
          ),
          Container(
            height: 15.0,
          ),
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: renderIndicator(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  renderIndicator() {
    return Stack(
      key: topListViewKey,
      children: [
        Container(
          height: 4.0,
          color: Colors.grey[300],
        ),
        AnimatedPositioned(
          left: scrollXPos,
          child: Container(
            height: 4.0,
            width: scrollWidth,
            color: const Color(0xfff29061),
          ),
          duration: Duration(
            milliseconds: 0,
          ),
        ),
      ],
    );
  }

  //바텀 버튼
  renderButton() {
    return PrimaryButton(
      label: '다음',
      onTap: () {
        Get.toNamed(
          'kkakka/issuance/target/detail',
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // if (donation == null) return renderIndicator();

    final theme = ThemeFactory.of(context).theme;

    return NestedScrollLayout(
      topBar: BackTopBar(title: '까까로 기부'),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(height: 30),
                SubTitle(title: '기부 내용을\n확인해주세요.'),
                Container(height: 20),
                renderDonateDetail(donation),
              ],
            ),
          ),
          Container(height: 18),
          Container(height: 6.0, color: theme.primaryGreyColor),
          Container(height: 18),
          Expanded(
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 16),
              physics: ClampingScrollPhysics(),
              children: [
                renderDonatePrice(),
                Container(height: 16),
                renderProgress(),
                Container(height: 34),
                renderDescription(),
                renderImageText(),
                renderImageContents(),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: renderButton(),
          )
        ],
      ),
    );
  }

// @override
// Widget build(BuildContext context) {
//   // if (donation == null) return renderIndicator();
//
//   final theme = ThemeFactory.of(context).theme;
//
//   return TitleLayout(
//     title: '까까로 기부',
//     body: Column(
//       children: [
//         Container(
//           padding: EdgeInsets.symmetric(horizontal: 16),
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               SubTitle(title: '기부 내용을\n확인해주세요.'),
//               Container(height: 20),
//               renderDonateDetail(donation),
//             ],
//           ),
//         ),
//         Container(height: 18),
//         Container(height: 6.0, color: theme.primaryGreyColor),
//         Container(height: 18),
//         Expanded(
//           child: ListView(
//             padding: EdgeInsets.symmetric(horizontal: 16),
//             physics: ClampingScrollPhysics(),
//             children: [
//               renderDonatePrice(),
//               Container(height: 16),
//               renderProgress(),
//               Container(height: 34),
//               renderDescription(),
//               renderImageText(),
//               renderImageContents(),
//
//             ],
//           ),
//         ),
//
//         Container(
//           padding: EdgeInsets.symmetric(horizontal: 16),
//           child: renderButton(),
//         )
//       ],
//     ),
//   );
// }
}

// % 설정 , 금액 0,000 설정 , 에러잡
