import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/components/top_bar/simple_hide_top_bar.dart';
import 'package:kins_healthcare/controllers/donate_controller.dart';
import 'package:kins_healthcare/layouts/layouts.dart';
import 'package:kins_healthcare/models/local/local_kkakka_model.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/donate/post_donate_models.dart';
import 'package:kins_healthcare/services/relation/model/post_relation_search_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

/*
 작성일 : 2021-04-01
 작성자 : Andy
 화면명 : HU_3001
 경로 : /kkakka/issuance/donate/target
 클래스 : DonateTargetScreen
 설명 : 까까기부 대상선정 화면
*/

class DonateTargetScreen extends StatefulWidget {
  @override
  _DonateTargetScreenState createState() => _DonateTargetScreenState();
}

class _DonateTargetScreenState extends State<DonateTargetScreen> {
  final PAGE_SIZE = 10;

  ScrollController scrollController = ScrollController();

  SearchRelResponse? targetModel;

  @override
  void initState() {
    super.initState();
    _setInitService();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  // Future<void> _refresh() async {
  //   await _setInitService();
  // }

  _setInitService() {
    try {
      Get.find<DonateController>().list(body: PostDonateBody(), reset: true);
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  renderAnimationTopBar() {
    return SimpleHideTopBar(
      title: '까까로 기부하기',
      scrollController: scrollController,
    );
  }

  renderNextButton() {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
      child: PrimaryButton(
        label: "다음",
        onTap: () {},
      ),
    );
  }

  renderLoading() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      height: 50,
      child: CircularProgressIndicator(),
    );
  }

  renderPageListView() {
    return GetBuilder<DonateController>(builder: (c) {
      return Expanded(
        child: Container(
          margin: const EdgeInsets.only(top: 30),
          padding: const EdgeInsets.only(bottom: 16, left: 16, right: 16),
          child: PaginationListView<NewDonateResponse>(
            controller: c,
            request: c.list,
            lastWidget: Container(),
            loadingWidget: renderLoading(),
            itemCount: c.regDonateList.length,
            scrollController: scrollController,
            itemBuilder: (_, index) {
              return InkWell(
                onTap: () {
                  final donateModel = SearchRelResponse(
                    userNoRel: c.regDonateList[index].donationId,
                    userNameOpponent: c.regDonateList[index].userName ?? '',
                    profileImgOpponent: c.regDonateList[index].logoImage ?? '',
                    userRelCdOpponentValue: '',
                    userPhoneOpponent: '',
                    userStatusOpponent: '',
                    userRelCdMeValue: '',
                  );
                  final LocalKkakkaModel localKkakkaModel = LocalKkakkaModel();
                  localKkakkaModel.userRelation = donateModel;
                  Get.toNamed(
                    '/kkakka/issuance/donate/edit',
                    arguments: localKkakkaModel,
                  );
                },
                child: DonateCard(
                  donateModel: c.regDonateList[index],
                ),
              );
            },
            separatorBuilder: (context, index) {
              return Divider(height: 12.0, color: Colors.transparent);
            },
          ),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Column(
        children: [
          renderAnimationTopBar(),
          renderPageListView()
          //renderNextButton(),
        ],
      ),
    );
  }
}

class DonateCard extends StatelessWidget {
  final NewDonateResponse donateModel;
  final defaultLogo =
      'https://drive.google.com/uc?export=download&id=1-fIA67Fd3DaX5pTGcJOxfsd5f40V711f';
  const DonateCard({
    Key? key,
    required this.donateModel,
  }) : super(key: key);

  renderNonImage() {
    return SvgPicture.asset(
      'assets/svgs/ic/ic_profile_signin.svg',
      fit: BoxFit.cover,
    );
  }

  renderImgUrl({required String path}) {
    return CachedNetworkImage(
      imageUrl: donateModel.logoImage ?? defaultLogo,
      errorWidget: (_, __, ___) => Container(),
    );
  }

  renderTitle(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;
    return ListTile(
      contentPadding: const EdgeInsets.all(0),
      dense: true,
      horizontalTitleGap: 0.0,
      leading: SizedBox(
        width: 30,
        height: 30,
        child: donateModel.logoImage != null
            ? renderImgUrl(path: donateModel.logoImage!)
            : renderNonImage(),
      ),
      title: Text(
        donateModel.userName ?? '',
        style: TextStyle(
          color: Color(0xFF262626),
          fontSize: theme.fontSize17,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  renderTextDetail(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;
    return Container(
      margin: const EdgeInsets.only(top: 21, bottom: 14),
      width: double.infinity,
      child: Text(
        donateModel.donationTitle ?? '',
        style: TextStyle(
          color: Color(0xFF878787),
          fontSize: theme.fontSize13,
        ),
      ),
    );
  }

  renderRowImage() {
    return Row(
      children: [
        Expanded(
          child: SizedBox(
            height: 90,
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6)),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(6),
                child: CachedNetworkImage(
                  //imageUrl: 'https://drive.google.com/uc?export=download&id=1GtRr8atXN63Bx4wLACZhFmxDdGk9FcB7',
                  imageUrl: donateModel.photo?[0] ?? '',
                  fit: BoxFit.cover,
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ),
          ),
        ),
        SizedBox(width: 8),
        Expanded(
          child: SizedBox(
            height: 90,
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6)),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(3.5),
                child: CachedNetworkImage(
                  //imageUrl: 'https://drive.google.com/uc?export=download&id=1GtRr8atXN63Bx4wLACZhFmxDdGk9FcB7',
                  imageUrl: donateModel.photo?[1] ?? '',
                  fit: BoxFit.cover,
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  renderTargetAmount(BuildContext context, {required double slidePercent}) {
    final theme = ThemeFactory.of(context).theme;

    final double slideValue = slidePercent / 100;

    final TextUtils textUtils = TextUtils();
    return Stack(
      alignment: AlignmentDirectional.bottomCenter,
      children: [
        Column(
          crossAxisAlignment: slidePercent > 50
              ? CrossAxisAlignment.stretch
              : CrossAxisAlignment.end,
          children: [
            SizedBox(height: 18),
            Text(
              '목표 금액',
              style: TextStyle(
                color: Color(0xFF303030),
                fontSize: theme.fontSize12,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 4),
            Text(
              '${textUtils.numberToLocalCurrency(amount: donateModel.totalPrice?.toDouble() ?? 0)}원',
              style: TextStyle(
                color: Color(0xFF262626),
                fontSize: theme.fontSize14,
              ),
            ),
            Column(
              children: [
                SizedBox(height: 11),
                ClipRRect(
                  borderRadius: BorderRadius.circular(3.5),
                  child: LinearProgressIndicator(
                    backgroundColor: Color(0xFF979797),
                    minHeight: 7.0,
                    value: slideValue,
                    valueColor:
                        AlwaysStoppedAnimation<Color>(Color(0xFFF29061)),
                  ),
                ),
              ],
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Align(
            alignment: Alignment.lerp(
                Alignment.topLeft, Alignment.topRight, slideValue)!,
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Image.asset(
                  'assets/png/speech_bubble.png',
                  // height: 40,
                  // width: 46,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5.0),
                  child: RichText(
                    text: TextSpan(
                      text: slidePercent.toString().substring(0, 2),
                      style: TextStyle(
                          fontSize: theme.fontSize15,
                          fontWeight: theme.heavyFontWeight,
                          color: Colors.white),
                      children: [
                        TextSpan(
                          text: '%',
                          style: TextStyle(
                              fontSize: theme.fontSize12, color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6.0),
        border: Border.all(color: Color(0xFFD7D8EE), width: 1.0),
      ),
      child: Padding(
        padding: EdgeInsets.all(14.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            renderTitle(context),
            renderTextDetail(context),
            renderRowImage(),
            renderTargetAmount(context,
                slidePercent: donateModel.percentage?.toDouble() ?? 0),
          ],
        ),
      ),
    );
  }
}
