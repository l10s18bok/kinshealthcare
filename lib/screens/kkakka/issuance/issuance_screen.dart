import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/controllers/local_controller.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/components/grid/box_grid.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/models/local/local_kkakka_model.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import '../../spinor_test/mark/model/box_grid_model.dart';

/*
 작성일 : 2021-01-26
 작성자 : Mark,
 화면명 : HU_0001,
 경로 : /kkakka/issuance
 클래스 : IssuanceScreen,
 설명 : 까까 발행 Screen
*/

// ignore: must_be_immutable
class IssuanceScreen extends StatefulWidget {
  IssuanceScreen({
    this.title = '까까 발행',
    this.subTitle = '발행할 까까의\n유형을 선택하세요.',
    
  });

  String title;
  String subTitle;

  @override
  IssuanceState createState() => IssuanceState();
}

class IssuanceState extends State<IssuanceScreen> {
  String kkakkaTitle = '';
  bool checkFlag = false;
  String test = "안녕하세요.";
  String test2 = "변경됬습니다.";
  late LocalController localController;

  @override
  void initState() {
    super.initState();

    localController = Get.find<LocalController>();
    localController.updateUser(test);
    localController.updateUser(test2);
    print("뽑아 쓰기 " + localController.localModel.nickName!);
  }

  final models = [
    BoxGridModel(
      title: '기념일',
      imagePath: 'assets/svgs/ic/ic_anniversary_publish_unselect.svg',
      selectedImagePath: 'assets/svgs/ic/ic_anniversary_publish_select.svg',
      ratio: 105 / 90,
    ),
    BoxGridModel(
      title: '기부',
      imagePath: 'assets/svgs/ic/ic_donate_publish_unselect.svg',
      selectedImagePath: 'assets/svgs/ic/ic_donate_publish_select.svg',
      ratio: 105 / 90,
    ),
    BoxGridModel(
      title: '의료비',
      imagePath: 'assets/svgs/ic/ic_medical_publish_unselect.svg',
      selectedImagePath: 'assets/svgs/ic/ic_medical_publish_select.svg',
      ratio: 105 / 90,
    ),
    BoxGridModel(
      title: '용돈',
      imagePath: 'assets/svgs/ic/ic_money_publish_unselect.svg',
      selectedImagePath: 'assets/svgs/ic/ic_money_publish_select.svg',
      ratio: 105 / 90,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: renderIntrinsicHeight(
        child: Padding(
          padding: EdgeInsets.only(left: 16, right: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BackTopBar(title: widget.title),
              SizedBox(height: 30),
              renderTitle(widget.subTitle),
              SizedBox(height: 20),
              BoxGrid(
                rowCount: 3,
                models: models,
                onLastSelectItem: _onGridSelectModel,
              ),
              SizedBox(height: 10),
              Expanded(
                child: Container(
                  alignment: Alignment.bottomCenter,
                  child: renderSubmitBtn(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget renderIntrinsicHeight({required Widget child}) {
    final queryData = MediaQuery.of(context);
    final minHeight = queryData.size.height - queryData.padding.top;
    final bottom = MediaQuery.of(context).padding.bottom;

    return SingleChildScrollView(
      child: IntrinsicHeight(
        child: Container(
          constraints: BoxConstraints(minHeight: minHeight - bottom),
          child: child,
        ),
      ),
    );
  }

  renderTitle(String title) {
    return Text(
      title,
      style: TextStyle(
        color: kinsBlack30,
        fontWeight: FontWeight.w700,
        fontSize: ThemeFactory.of(context).theme.fontSize17,
      ),
    );
  }

  _onGridSelectModel(BoxGridModel model) {
    setState(() {
      checkFlag = true;
    });
    kkakkaTitle = model.title;
  }

  Widget renderSubmitBtn() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      height: 60,
      child: checkFlag
          ? PrimaryButton(
              label: '다음',
              onTap: _btnOnTap,
            )
          : PrimaryButton(
              label: '다음',
              onTap: null,
            ),
    );
  }

  _btnOnTap() {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (kkakkaTitle == '기부') {
      Get.toNamed('/kkakka/issuance/donate', arguments: kkakkaTitle);
    } else {
      LocalKkakkaModel localKkakkaModel = LocalKkakkaModel();
      localKkakkaModel.type = _getType(kkakkaTitle);
      Get.toNamed('/kkakka/issuance/target', arguments: localKkakkaModel);
    }
  }

  KkakkaType _getType(String title) {
    switch (title) {
      case '기부':
        return KkakkaType.DONATE;
      case '기념일':
        return KkakkaType.ANNIVERSARY;
      case '용돈':
        return KkakkaType.PIN;
      case '의료비':
        return KkakkaType.MEDICAL;
    }

    return KkakkaType.PIN;
  }
}
