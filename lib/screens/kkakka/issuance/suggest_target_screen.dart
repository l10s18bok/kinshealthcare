import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/simple_information_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/controllers/suggest_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/payment_user_model.dart';
import 'package:kins_healthcare/services/mykins/model/family_list_model.dart';
import 'package:kins_healthcare/services/suggest/model/suggest_user_body.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/**
 * 작성일 : 2021-04-08
 * 작성자 : Daniel
 * 화면명 : HU_4001[제안하기]
 * 클래스 : SuggestTarget
 * 경로  : /kkakka/issuance/suggest/target
 * 설명 : 까까 발행 후 다른 가족들에게 까까 제안하기
 */

class SuggestTarget extends StatefulWidget {
  @override
  SuggestTargetState createState() => SuggestTargetState();
}

class SuggestTargetState extends State<SuggestTarget> {
  List<FamilyListModel> userList = [];

  @override
  void initState() {
    super.initState();
    _getUserList();
  }

  _getUserList() async {
    var list = Get.arguments;
    try {
      userList = await Get.find<MykinsController>().getFamilyList();

      if (list is List<PaymentUserModel> == false) return _setStateEndAnimation();
      for (final model in list) {
        for (final targetModel in userList) {
          if (model.userRelNo == targetModel.relUserNo) {
            userList.remove(targetModel);
            break;
          }
        }
      }

      _setStateEndAnimation();
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(title: '가족 선택'),
      body: Padding(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(child: renderListView()),
            renderSubmitBtn(),
          ],
        ),
      ),
    );
  }

  Widget renderListView() {
    return ListView.builder(
      itemCount: userList.length == 0 ? 1 : userList.length,
      itemBuilder: (_, index) {
        if (index == 0) return renderFirstListItem(index);
        return renderListItem(index);
      },
    );
  }

  Widget renderFirstListItem(int index) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        renderTitle(),
        userList.length != 0 ? renderListItem(index) : renderNonCard(),
      ],
    );
  }

  Widget renderNonCard() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10, left: 16, right: 16, top: 0),
      child: NonKkakkaCard(title: '제안 할 가족이 없습니다.\n제안 할 가족을 저장해보세요.'),
    );
  }

  Widget renderTitle() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      child: Text(
        '어떤 분에게\n제안하시겠어요?',
        style: TextStyle(
          color: kinsBlack30,
          fontWeight: FontWeight.w700,
          fontSize: ThemeFactory.of(context).theme.fontSize17,
        ),
      ),
    );
  }

  Widget renderListItem(int index) {
    final model = userList[index];
    final path = model.relUserImage;

    return InkWell(
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: kinsGrayEA, width: 1.0),
          ),
        ),
        height: 84,
        child: Row(
          children: [
            CircleNetworkImage(path: path, imageSize: 56.0),
            SizedBox(width: 16),
            Expanded(child: renderItemContent(model)),
            SizedBox(width: 16),
            renderCheckBtn(model),
          ],
        ),
      ),
      onTap: () {
        model.isChecked = !(model.isChecked ?? false);
        setState(() {});
      },
    );
  }

  Widget renderItemContent(FamilyListModel model) {
    final name = model.relName;
    final nickName = model.relUserName;
    final phoneNumber = model.relUserPhone;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '$name ($nickName)',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
          ),
        ),
        SizedBox(height: 10),
        Text(
          phoneNumber ?? '',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
          ),
        ),
      ],
    );
  }

  renderCheckBtn(FamilyListModel model) {
    final backgroundColor = (model.isChecked ?? false) ? kinsBlue40 : kinsBlueCF;

    return Container(
      height: 26,
      child: Row(
        children: [
          AnimatedContainer(
            alignment: Alignment.center,
            duration: Duration(milliseconds: 100),
            width: 26,
            height: 26,
            decoration: BoxDecoration(
              color: backgroundColor,
              borderRadius: BorderRadius.all(Radius.circular(4)),
              border: Border.all(color: kinsGrayC2),
            ),
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_checkbox_unselect.svg',
              width: 12,
              height: 10,
            ),
          ),
        ],
      ),
    );
  }

  Widget renderSubmitBtn() {
    List<UserNo> users = [];

    final List<FamilyListModel> checkList = [];
    for (final item in userList) {
      if (item.isChecked == true) {
        checkList.add(item);
        final userNo = UserNo(userNoSuggest: item.relUserNo);
        users.add(userNo);
      }
    }
    var isCheck = checkList.length != 0;

    return Container(
      margin: EdgeInsets.only(bottom: 16),
      height: 55,
      child: PrimaryButton(
        label: '확인',
        bgColor: isCheck ? null : kinsBlueB8,
        padding: EdgeInsets.all(0.0),
        onTap: () async {
          if (isCheck == false) return;
          
          try {
            await Get.find<SuggestController>().suggestUser(Get.arguments, users);

            final sheet = SimpleInformationBottomSheet(
              message: '가족들에게\n까까를 제안했습니다 :)',
            );

            await Get.bottomSheet<String>(sheet);
            
            Get.offAllNamed('/main-home');
            } on DioError catch (e) {
              e.response!.data.toString();
              final sheet = ErrorBottomSheet(title: '제안하기에 실패하였습니다', content: '이전 작업(까까발행)은 정상적으로 처리 되었습니다.',);
              Get.bottomSheet(sheet);
            }
        },
      ),
    );
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }
}
