import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/bottom_sheet/image_picker_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_keyboard_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/simple_information_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/sub_btn_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/sub_two_btn_sheet.dart';
import 'package:kins_healthcare/components/button/action_chip_button.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/card/bank_account_and_credit_card.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/dialog/kkakka_send_dialog.dart';
import 'package:kins_healthcare/components/grid/box_grid.dart';
import 'package:kins_healthcare/components/top_bar/simple_hide_top_bar.dart';
import 'package:kins_healthcare/controllers/kkakka_controller.dart';
import 'package:kins_healthcare/controllers/upload_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/models/local/local_kkakka_model.dart';
import 'package:kins_healthcare/models/local/user_pay_card_model.dart';
import 'package:kins_healthcare/screens/auth/verify/pin/pin_screen.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/box_grid_model.dart';
import 'package:kins_healthcare/screens/temp/calendar_screen.dart';
import 'package:kins_healthcare/services/kkakka/model/kkakka_request_models.dart';
import 'package:kins_healthcare/services/relation/model/post_relation_search_models.dart';
import 'package:kins_healthcare/services/user/payment/model/payment_card_info.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

/*
 * 작성일 : 2021-01-20
 * 작성자 : Andy
 * 화면명 : HU_4002(까까 발행)
 * 클래스 : IssuanceSendScreen
 * 경로  : /kkakka/issuance/target/detail
 * 설명 : 까까 발행의 내용작성
 */

enum SelectTab { NONE, SEND, LIMITED, CALENDAR }
enum CardSelect { first, second }

class IssuanceSendScreen extends StatefulWidget {
  final String title;
  final String recipient;
  const IssuanceSendScreen({Key? key, this.title = '까까 보내기', this.recipient = '받는 분'}) : super(key: key);

  @override
  _IssuanceSendScreenState createState() => _IssuanceSendScreenState();
}

class _IssuanceSendScreenState extends State<IssuanceSendScreen> {
  File? image;
  TextEditingController _singleLineController = TextEditingController(); //한줄 편지 내용 저장
  TextEditingController _sendEditController = TextEditingController(); //보낼금액 저장
  TextEditingController _kkakkaEditController = TextEditingController(); //1회 사용한도 저장
  ScrollController _scrollController = ScrollController();
  final TextUtils textUtils = TextUtils();

  var _startDate; //시작 날짜 저장
  var _endDate; //끝 날짜 저장

  PaymentCardInfoResponse? _firstSelAmountCard; //첫번째 결제수단 ==> 선택계좌카드
  PaymentCardInfoResponse? _secoundSelAmountCard; // 두번째 결제수단 ==> 선택계좌카드
  List<UserPayCardModel> _firstAmountList = []; // 첫번째 결제수단 계좌카드 리스트
  List<UserPayCardModel> _secoundAmountList = []; // 두번째 결제수단 계좌카드 리스트

  final models = [
    BoxGridModel(
      title: '기념일',
      imagePath: 'assets/svgs/ic/ic_anniversary_publish_unselect.svg',
      selectedImagePath: 'assets/svgs/ic/ic_anniversary_publish_select.svg',
      ratio: 105 / 90,
    ),
    BoxGridModel(
      title: '용돈',
      imagePath: 'assets/svgs/ic/ic_money_publish_unselect.svg',
      selectedImagePath: 'assets/svgs/ic/ic_money_publish_select.svg',
      ratio: 105 / 90,
    ),
    BoxGridModel(
      title: '의료비',
      imagePath: 'assets/svgs/ic/ic_medical_publish_unselect.svg',
      selectedImagePath: 'assets/svgs/ic/ic_medical_publish_select.svg',
      ratio: 105 / 90,
    ),
  ]; // 카테고리 리스트

  //String? _upLoadImgPath;
  String? _kkakkaLimitErrText = null; // 한도금액 에러 알림 ..
  String? _sendErrText = null; // 보낼금액 에러 알림 ..
  bool _kkakkaLimitClick = false; // 1회 한도금액 클릭 상태 저장
  bool _sendAmountClick = false; //보낼금액 클릭 상태 저장
  bool _calenderClick = false; // 달력 클릭 상태 저장

  bool _detailButtonOn = false; //세부항목 보기 클릭(true : 펼치기, false : 닫기)

  bool doubleClick = false; // 다음버튼 중복 탭 방지

  late SearchRelResponse _targetUser; // 보낼 유저 정보 저장
  late LocalKkakkaModel localKkakkaModel; // 보낼 유저정보 + 카테고리(이전페이지에서 받은 정보)

  @override
  void initState() {
    super.initState();
    localKkakkaModel = Get.arguments;

    if (localKkakkaModel.requestModel != null && localKkakkaModel.userRelation != null) {
      //타임라인에서 보낸 데이타 인가?
      _checkTimeLine();
    } else {
      if (localKkakkaModel.userRelation != null) {
        _targetUser = localKkakkaModel.userRelation!;
      }
      models[1].isSelect = true; // 2021.04.26 기본 카테고리 용돈으로 ..
      localKkakkaModel.type = localKkakkaModel.getTitleToKkakkaType('용돈');
      _todayDate();
    }
    _getCardData();
  }

  _checkTimeLine() {
    String type;
    if (localKkakkaModel.suggestDetailModel != null) {
      type = localKkakkaModel.suggestDetailModel!.kkakkaType!;

      if (localKkakkaModel.suggestDetailModel!.kkakkaLimit != null) {
        _kkakkaLimitClick = true;
        _kkakkaEditController.text = textUtils.numberToLocalCurrency(amount: localKkakkaModel.suggestDetailModel!.kkakkaLimit!.toDouble()) + ' 원';
      }
      if (localKkakkaModel.suggestDetailModel!.kkakkaMessage != null) {
        _singleLineController.text = localKkakkaModel.suggestDetailModel!.kkakkaMessage!;
      }
      // if (localKkakkaModel.suggestDetailModel!.kkakkaImage != null) {
      //   image = File('');
      // }
    } else {
      type = localKkakkaModel.getEngTitleToKkakkaType; // 선택한 카테고리 타입
    }

    _targetUser = localKkakkaModel.userRelation!;
    localKkakkaModel.type = localKkakkaModel.getTitleToKkakkaType(type);

    var selCategory = models.firstWhere((element) => element.title.contains(type));
    selCategory.isSelect = true; // 선택한 카테고리 표시
    if (localKkakkaModel.requestModel!.startDate != null) {
      _startDate = localKkakkaModel.requestModel!.startDate!;
      _endDate = localKkakkaModel.requestModel!.endDate!;
    } else {
      _todayDate();
    }
    _sendAmountClick = true;
    _sendEditController.text = textUtils.numberToLocalCurrency(amount: localKkakkaModel.requestModel!.kkakkaPrice!.toDouble()) + ' 원'; // 보낼금액
  }

  @override
  void dispose() {
    _singleLineController.dispose();
    _sendEditController.dispose();
    _kkakkaEditController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  _getCardData() async {
    try {
      final resp = await Get.find<UserController>().getUserPayCardInfo();
      if (resp.length > 0) {
        _inputPayCard(resp);
      } else {
        final sheet = SimpleInformationBottomSheet(message: '결제 수단이 없습니다!.');
        await Get.bottomSheet<String>(sheet);
        Get.back();
      }
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  _inputPayCard(var resp) {
    for (var model in resp) {
      final UserPayCardModel usrModel = UserPayCardModel();
      final UserPayCardModel usrModel2 = UserPayCardModel();
      usrModel.cardInfo = model;
      usrModel2.cardInfo = model;

      _firstAmountList.add(usrModel);
      _secoundAmountList.add(usrModel2);
    }
    if (resp.length != 1) {
      final replexModel = _secoundAmountList[1];
      _secoundAmountList.removeAt(1);
      _secoundAmountList.insert(0, replexModel);
    }
    _firstAmountList[0].isSelected = true;
    _secoundAmountList[0].isSelected = true;

    _firstSelAmountCard = _firstAmountList[0].cardInfo;
    _secoundSelAmountCard = _secoundAmountList[0].cardInfo;

    setState(() {});
  }

  _sendData({String? upLoadImgPath}) {
    final kkakkaType = localKkakkaModel.kkakkaType;

    final priceL = _sendEditController.text.replaceAll('원', '').replaceAll(',', '');
    final priceKkakka = int.tryParse(priceL);

    int? limitKkakka;

    if (_kkakkaEditController.text.isNotEmpty) {
      final limitL = _kkakkaEditController.text.trim().replaceAll('원', '').replaceAll(',', '');
      limitKkakka = int.tryParse(limitL);
    } else
      limitKkakka = priceKkakka;

    if (priceKkakka! < limitKkakka!) {
      if (_detailButtonOn == false) _detailButtonOn = true;
      _kkakkaLimitErrText = '[까까] 한도금액이 총금액보다 높을 수 없습니다';
      return;
    }

    final sendData = PostKkakkaIssuanceSendBody(
      kkakkaImage: upLoadImgPath,
      kkakkaMessage: _singleLineController.text,
      kkakkaLimit: limitKkakka,
      kkakkaPrice: priceKkakka,
      kkakkaType: kkakkaType,
      endDate: DateFormat('yyyy.MM.dd').format(_endDate).replaceAll('.', '-'),
      startDate: DateFormat('yyyy.MM.dd').format(_startDate).replaceAll('.', '-'),
      userNoRel: _targetUser.userNoRel,
      userPaymentWayId1: _firstSelAmountCard!.paymentWayId,
      userPaymentWayId2: _secoundSelAmountCard!.paymentWayId,
    );
    return sendData;
  }

  _showBottomSubBtnSheet() async {
    _kkakkaLimitErrText = null;
    FocusScope.of(context).requestFocus(FocusNode());

    if (_sendEditController.text.isEmpty || _sendEditController.text == '0 원') {
      _sendErrText = '보낼금액을 입력하세요';
      setState(() {});
      return;
    }

    if (doubleClick) return;
    doubleClick = true;

    final checkNow = DateFormat('yyyyMMdd').format(DateTime.now()).toString();
    final startDate = DateFormat('yyyy.MM.dd').format(_startDate).toString().replaceAll('.', '');

    if (int.parse(checkNow) > int.parse(startDate)) {
      Get.snackbar('알림', '금일 이전 날짜가 선택되어있습니다.');
      return;
    }

    final respon = await _infoDialog();
    if (!respon) {
      doubleClick = false;
      return;
    }
    final backResult = await Get.toNamed(
      '/auth/verify/pin',
      arguments: PinScreenArgument(
        hasSkip: false,
        isRegister: false,
        isBack: true,
      ),
    );

    if (backResult != 'OK') {
      doubleClick = false;
      return;
    }

    /**
 * 작성일 : 2021-04-07
 * 작성자 : Daniel
 * 설명 : 까까 발행 후 제안하기 부분 추가를 위해 Future.microtask() 안의 내용 수정/추가하였음
 */
    Future.microtask(() async {
      try {
        String? _upLoadImgPath;
        if (image != null) {
          _upLoadImgPath = await Get.find<UploadController>().uploadImage(image: image!);
        }

        final modelData = _sendData(upLoadImgPath: _upLoadImgPath);

        final resp = await Get.find<KkakkaController>().sendKkakkaSuggest(modelData);

        setState(() {});
        if (resp.resultCode == 'SUCCESS') {
          if (resp.data.kkakkaId == 0) {
            final sheet = SubBtnBottomSheet(
              boldTitle: _targetUser.userNameOpponent,
              blueTitle: '[자신]',
              boldTitle02: '에게',
              title: '까까를 보냈어요!',
            );
            final result = await Get.bottomSheet<String>(sheet);
            if (result == 'OK') {
              Get.offAllNamed('/main-home');
            }
          } else {
            final sheet = SubTwoBtnBottomSheet(
              boldTitle: _targetUser.userNameOpponent,
              blueTitle: '[${_targetUser.userRelCdOpponentValue}]',
              boldTitle02: '님께',
              title: '까까를 보냈어요!',
              extraTitle: '가족분들께 같이 보내기를',
              extraTitle02: '제안해보시겠어요?',
              bottomLeftBtnRowTitle: '제안하기',
              bottomRightBtnRowTitle: '건너뛰기',
            );
            final result = await Get.bottomSheet<String>(sheet);
            if (result == 'LEFT') {
              await Get.toNamed('/kkakka/issuance/suggest/target', arguments: resp.data.kkakkaId);
            }
            Get.offAllNamed('/main-home');
          }
        }
      } on DioError catch (e) {
        doubleClick = false;
        final msg = e.response!.data.toString();
        final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
        Get.bottomSheet(sheet);
        return;
      }
    });
  }

  _getImage() async {
    final ImagePickerBottomSheetResult? resp = await Get.bottomSheet(
      ImagePickerBottomSheet(
        processImage: true,
      ),
    );

    if (resp == null) return;

    setState(() {
      image = resp.file;
    });
  }

  _todayDate() {
    _startDate = new DateTime.now();
    _endDate = _startDate.add(new Duration(days: 30));
  }

  _onFocusChange() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final result = await Get.bottomSheet<MoneyBottomSheetResult>(
      MoneyKeyboardBottomSheet(),
      isScrollControlled: true,
    );
    return result;
  }

  _onTabKkakkaLimited() async {
    _kkakkaLimitClick = true;
    final result = await _onFocusChange();
    if (result == null) return;
    _kkakkaEditController.text = textUtils.numberToLocalCurrency(amount: result.amount) + ' 원';
    setState(() {});
  }

  _onTabSendAmount() async {
    _sendAmountClick = true;
    final result = await _onFocusChange();
    if (result == null) return;
    _sendEditController.text = textUtils.numberToLocalCurrency(amount: result.amount) + ' 원';

    if (_sendEditController.text != '0 원') _sendErrText = null;

    setState(() {});
  }

  _onTabCalendar() async {
    _calenderClick = true;
    final List<DateTime> date = [DateTime.now(), DateTime.now()];
    final result = await Get.toNamed('/calendar', arguments: date);
    if (result == null) return;
    final conversion = (result as CalendarScreenResult).range;
    _startDate = conversion.first;
    _endDate = conversion.last;
    setState(() {});
  }

  _onTabDetail() {
    _detailButtonOn = !_detailButtonOn;
    setState(() {});
  }

  _cardOnTap(UserPayCardModel cardModel, CardSelect cardSelect) {
    List<UserPayCardModel> cardCheckList = [];
    if (cardSelect == CardSelect.first) {
      cardCheckList.addAll(_firstAmountList);
      _firstSelAmountCard = cardModel.cardInfo;
    } else {
      cardCheckList.addAll(_secoundAmountList);
      _secoundSelAmountCard = cardModel.cardInfo;
    }
    for (var item in cardCheckList) {
      item.isSelected = false;
    }

    cardModel.isSelected = true;

    setState(() {});
  }

  _onGridSelectModel(BoxGridModel model) {
    setState(() {
      localKkakkaModel.type = localKkakkaModel.getTitleToKkakkaType(model.title);
    });

    //kkakkaTitle = model.title;
  }

  Future<bool> _infoDialog() async {
    final category = localKkakkaModel.getCategory();
    final rep = _sendEditController.text.replaceAll('원', '').replaceAll(',', '');
    final price = int.parse(rep);
    int limit = 0;
    if (_kkakkaEditController.text.length > 0) {
      final rep2 = _kkakkaEditController.text.replaceAll('원', '').replaceAll(',', '');
      limit = int.parse(rep2);
    }

    final result = await Get.dialog(
      AlertDialog(
        backgroundColor: Colors.transparent,
        contentPadding: const EdgeInsets.all(0),
        content: Builder(
          builder: (context) {
            return KkakkaSendDialog(
              imagePath: _targetUser.profileImgOpponent,
              name: _targetUser.userNameOpponent,
              nickName: _targetUser.userRelCdOpponentValue,
              phoneNumber: _targetUser.userPhoneOpponent,
              category: BoxGridModel(title: category.title, imagePath: category.imagePath),
              startDate: _startDate,
              endDate: _endDate,
              price: price, //int.parse(_sendEditController.text),
              title: '보낼 까까 확인',
              btnTitle: '보내기',
              limit: limit,
              firstAmount: _firstSelAmountCard!.paymentCorp,
              secoundAmount: _secoundSelAmountCard!.paymentCorp,
              whichKkaka: 1,
            );
          },
        ),
      ),
      barrierDismissible: false,
    );

    return result;
  }

  renderLabel(String label, {String? subLabel}) {
    final theme = ThemeFactory.of(context).theme;
    return Row(
      children: [
        Text(
          label,
          style: TextStyle(
            fontSize: theme.fontSize13,
            color: Colors.black,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
        SizedBox(width: 4),
        Text(
          subLabel ?? '',
          style: TextStyle(fontSize: theme.fontSize13, color: kinsGrayB6),
        ),
      ],
    );
  }

  renderAnimationTopBar() {
    return SimpleHideTopBar(
      title: widget.title,
      scrollController: _scrollController,
    );
  }

  renderCameraBox() {
    final theme = ThemeFactory.of(context).theme;
    return InkWell(
      onTap: _getImage,
      child: Container(
          height: 72,
          width: 72,
          decoration: BoxDecoration(
            border: Border.all(
              color: Color(0xFFD7D8EE),
              width: 1,
            ),
            borderRadius: BorderRadius.circular(6),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                'assets/svgs/ic/ic_camera.svg',
              ),
              Text(
                '0/1',
                style: TextStyle(
                  fontSize: theme.fontSize12,
                  color: Color(0xFF9596A7),
                  fontWeight: theme.heavyFontWeight,
                ),
              ),
            ],
          )),
    );
  }

  renderImageBox() {
    return InkWell(
      onTap: () {
        setState(() {
          image = null;
        });
      },
      child: Container(
        height: 80,
        width: 80,
        child: Stack(
          alignment: Alignment.centerLeft,
          children: [
            Container(
              width: 72,
              height: 72,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Color(0xFFD7D8EE),
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(6),
              ),
              child: Image.file(image!, fit: BoxFit.cover),
            ),
            Positioned(
              right: 0,
              top: 0,
              child: SvgPicture.asset(
                'assets/svgs/ic/ic_delete.svg',
                width: 20,
                height: 20,
              ),
            ),
          ],
        ),
      ),
    );
  }

  renderTopPhotoSelect() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        renderLabel('카드 사진'),
        SizedBox(height: 8),
        image == null ? renderCameraBox() : renderImageBox(),
      ],
    );
  }

  renderInputTextField() {
    final theme = ThemeFactory.of(context).theme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        renderLabel('한 줄 편지', subLabel: '선택'),
        SizedBox(height: 8),
        TextField(
          controller: _singleLineController,
          maxLines: 2,
          keyboardType: TextInputType.multiline,
          textInputAction: TextInputAction.done,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFD7D8EE), width: 1),
              borderRadius: BorderRadius.circular(6.0),
            ),
            hintStyle: TextStyle(color: Color(0xFFB6B6B6), fontSize: theme.fontSize15),
            hintText: '따뜻한 말 한 마디로 마음을 전달해보세요.',
          ),
        ),
        SizedBox(height: 26),
      ],
    );
  }

  renderSendPerson({required String title, double? topMargin, double? bottomMargin}) {
    final theme = ThemeFactory.of(context).theme;
    return Column(
      children: [
        renderLabel(title),
        ListTile(
          contentPadding: const EdgeInsets.all(0),
          leading: CircleNetworkImage(
            imageSize: 40,
            path: _targetUser.profileImgOpponent,
            userNo: _targetUser.userNoRel,
            heroTag: _targetUser.profileImgOpponent,
          ),
          title: RichText(
            text: TextSpan(
              text: _targetUser.userNameOpponent,
              children: [
                TextSpan(
                  text: ' ${_targetUser.userRelCdOpponentValue}',
                  style: TextStyle(
                    color: Color(0xFF4042AB),
                    fontSize: theme.fontSize12,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
              style: TextStyle(
                color: Color(0xFF363636),
                fontSize: theme.fontSize14,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          subtitle: Text(_targetUser.userPhoneOpponent,
              style: TextStyle(
                color: Color(0xFF535353),
              )),
        ),
      ],
    );
  }

  renderUnderLineContainer({
    TextEditingController? controller,
    String? hintText,
    bool hintBlack = false,
    String? errorText,
  }) {
    final theme = ThemeFactory.of(context).theme;
    return AbsorbPointer(
      child: TextField(
        controller: controller,
        style: TextStyle(
          color: hintBlack ? Colors.black : Color(0xFFD6D6D6),
          fontSize: theme.fontSize15,
        ),
        decoration: InputDecoration(
          hintText: hintText,
          errorText: errorText,
          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
        ),
      ),
    );
  }

  renderSelectTextField(
      {required String label,
      SelectTab? selectTab,
      TextEditingController? controller,
      required String hintText,
      String? errorText,
      required Function onTab}) {
    bool black = false;
    if (selectTab == SelectTab.SEND && _sendAmountClick) {
      black = true;
    } else if (selectTab == SelectTab.LIMITED && _kkakkaLimitClick) {
      black = true;
    } else if (selectTab == SelectTab.CALENDAR && _calenderClick) {
      black = true;
    }

    return InkWell(
      onTap: () => onTab(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          renderLabel(label),
          SizedBox(height: 10),
          renderUnderLineContainer(
            controller: controller,
            hintText: hintText,
            errorText: errorText,
            hintBlack: black,
          ),
        ],
      ),
    );
  }

  renderCategory() {
    return Column(
      children: [
        SizedBox(height: 20),
        renderLabel('카테고리'),
        SizedBox(height: 8),
        BoxGrid(
          rowCount: 3,
          models: models,
          onLastSelectItem: _onGridSelectModel,
        ),
        SizedBox(height: 20),
      ],
    );
  }

  renderValidity() {
    return Column(
      children: [
        renderSelectTextField(
            label: '유효기간',
            selectTab: SelectTab.CALENDAR,
            hintText: '${DateFormat('yyyy.MM.dd').format(_startDate).toString()} - ${DateFormat('yyyy.MM.dd').format(_endDate).toString()}',
            onTab: _onTabCalendar),
        SizedBox(height: 28),
      ],
    );
  }

  renderLimited() {
    return Column(
      children: [
        renderSelectTextField(
            label: '1회 사용한도',
            controller: _kkakkaEditController,
            selectTab: SelectTab.LIMITED,
            hintText: '0원',
            errorText: _kkakkaLimitErrText,
            onTab: _onTabKkakkaLimited),
        SizedBox(height: 28),
      ],
    );
  }

  renderFirstCard() {
    return Column(
      key: Key('1'),
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        renderLabel('첫번째 결제수단'),
        SizedBox(height: 16),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              // ..._firstAmountList.map((e) => renderCard(e, CardSelect.first)).toList(),
              ..._firstAmountList
                  .map((e) => BankAccountAndCreditCard(
                        onTap: () => _cardOnTap(e, CardSelect.first),
                        cardModel: e,
                      ))
                  .toList(),
            ],
          ),
        ),
      ],
    );
  }

  renderSecoundCard() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        SizedBox(height: 28),
        renderLabel('두번째 결제수단'),
        SizedBox(height: 16),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              ..._secoundAmountList
                  .map((e) => BankAccountAndCreditCard(
                        onTap: () => _cardOnTap(e, CardSelect.second),
                        cardModel: e,
                      ))
                  .toList(),
            ],
          ),
        ),
      ],
    );
  }

  renderDetail() {
    return Column(
      children: [
        renderValidity(),
        renderLimited(),
        renderFirstCard(),
        renderSecoundCard(),
      ],
    );
  }

  renderNextButton() {
    return PrimaryButton(
      label: "다음",
      onTap: _showBottomSubBtnSheet,
    );
  }

  renderTop() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          SizedBox(height: 30),
          renderTopPhotoSelect(),
          SizedBox(height: 20),
          renderInputTextField(),
          renderSendPerson(title: widget.recipient, topMargin: 18, bottomMargin: 21),
        ],
      ),
    );
  }

  renderBottom() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          renderCategory(),
          renderSelectTextField(
              label: '보낼 금액',
              controller: _sendEditController,
              selectTab: SelectTab.SEND,
              hintText: '0원',
              errorText: _sendErrText,
              onTab: _onTabSendAmount),
          SizedBox(height: 28),
          AnimatedSwitcher(
            duration: Duration(milliseconds: 500),
            transitionBuilder: (child, animation) => SizeTransition(
              child: child,
              sizeFactor: animation,
            ),
            child: _detailButtonOn ? renderDetail() : renderFirstCard(),
          ),
          ActionChipButton(fold: _detailButtonOn, onTap: _onTabDetail),
          SizedBox(height: 40),
          renderNextButton(),
          SizedBox(height: 16),
        ],
      ),
    );
  }

  renderDivider() {
    return Container(
      height: 6.0,
      color: Color(0xFFEFEFF4),
      child: Align(
        alignment: Alignment.topCenter,
        child: Container(
          height: 1,
          color: Color(0xFFE8E7EC),
        ),
      ),
    );
  }

  renderUpLoadingProgress() {
    final theme = ThemeFactory.of(context).theme;
    return Center(
      child: Container(
        padding: const EdgeInsets.all(10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: Colors.black38,
          borderRadius: BorderRadius.circular(6),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            CircularProgressIndicator(
              backgroundColor: Colors.yellow,
            ),
            SizedBox(height: 35),
            Text(
              '잠시만 기다려주세요',
              style: TextStyle(
                color: Colors.white,
                fontSize: theme.fontSize16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: GetBuilder<UploadController>(
        builder: (c) {
          return Stack(
            children: [
              GestureDetector(
                onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
                child: Column(
                  children: [
                    renderAnimationTopBar(),
                    Expanded(
                      child: ListView(
                        controller: _scrollController,
                        children: [
                          renderTop(),
                          renderDivider(),
                          renderBottom(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              c.isLoading ? renderUpLoadingProgress() : Container(),
            ],
          );
        },
      ),
    );
  }
}
