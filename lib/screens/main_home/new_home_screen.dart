import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/listview/home_list_view.dart';
import 'package:kins_healthcare/components/listview/home_post_list_view.dart';
import 'package:kins_healthcare/components/modal/home_option_modal.dart';
import 'package:kins_healthcare/components/tab/page_tab.dart';
import 'package:kins_healthcare/controllers/kkakka_controller.dart';
import 'package:kins_healthcare/controllers/post_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/screens/main_home/request/request_page_screen.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/kkakka/model/sum_kkakka_model.dart';
import 'package:kins_healthcare/services/post/model/post_list_model.dart';
import 'package:kins_healthcare/services/user/model/user_info_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-04-08
 작성자 : Mark,
 화면명 : HH_0001-2
 경로
 클래스 : NewHomeScreen,
 설명 : 디자인 리뉴얼 된 Home 화면
*/

/// 디자인 리뉴얼 된 Home 화면
///
/// [Figma 링크](https://www.figma.com/file/5WrUxt9xaBFHVo8hLKqA0d/%EB%94%94%EC%9E%90%EC%9D%B8-%ED%8C%8C%EC%9D%BC?node-id=3003%3A0)
///
/// 화면명 : HH_0001-2 \
/// 작성자 : Mark \
/// 작성일 : 2021-04-08 \
///
/// {@category Screen}
class NewHomeScreen extends StatefulWidget {
  @override
  NewHomeScreenState createState() => NewHomeScreenState();
}

/// {@category Screen}
class NewHomeScreenState extends State<NewHomeScreen> {
  final bool isAlertShow = true;

  double topBarHeight = 0;
  late GlobalKey addKey; // + 버튼
  PageController _pageController = PageController(initialPage: 0);
  late KkakkaController _controller;
  int? kkakkaCount;
  SumKkakkaModel? sumModel;

  // 2021.04.12 Andy 추가
  bool haveAccount = true; // 계좌 유무 true = 유, false = 무
  // 2021.05.07 baron added
  @override
  void initState() {
    super.initState();

    addKey = GlobalKey();
    Get.find<UserController>().getUserInfo();
    _controller = Get.find<KkakkaController>();
    _setKkakkaController();
    _getKkakkaSum();
    requestKkakkaCount();
    _checkAccount(); // 2021.04.12 Andy 추가
  }

  _checkAccount() async {
    try {
      final resp = await Get.find<UserController>().getUserPayCardInfo();
      if (resp == null) haveAccount = false;
    } on DioError catch (e) {
      haveAccount = false;
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  _setKkakkaController() async {
    // 여기서는 pagination 타기 위해 body추가하여  처음으로 reset = true로  호출.
    // _controller.listKkakkas();  _controller.listPromiseMe();
    //  모두 future function으로 바꿔 await 타고 기다려야함.
    //  cache된 data 사용 하게 되면 getX로 자도update하기에  listen은 필요 없을 듯 합니다.
    GeneralPaginationBody body =
        GeneralPaginationBody(sortBy: 'id', direction: false, size: 20);
    await _controller.sentList(body: body, reset: true);
    _controller.listPromiseMe(body: body, reset: true);
    // _controller.addListener(_setStateEndAnimation);
  }

  _getKkakkaSum() async {
    try {
      sumModel = await _controller.sumKkakka();
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  @override
  void dispose() {
    super.dispose();
    // _controller.removeListener(_setStateEndAnimation);
  }

  _setTopBarHeight() {
    final width = MediaQuery.of(context).size.width;
    final height = isAlertShow ? 300 : 240;
    topBarHeight = height * (width / 360);
  }

  Widget renderTopBar() {
    return Column(
      children: [
        renderAppBar(),
        Expanded(
          child: renderWelcome(),
        ),
        kkakkaCount == 0 ? Container() : renderAlert(),
        Container(height: 12.0),
        PageTap(
          pageController: _pageController,
          titleList: ['소식', '받은 까까', '보낸 까까'],
        ),
      ],
    );
  }

  Widget renderAppBar() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      padding: EdgeInsets.only(top: 16, left: 16, right: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onDoubleTap: () => Get.toNamed('/test-splash'),
            child: SvgPicture.asset(
              'assets/svgs/img/logo_splash.svg',
              color: theme.primaryColor,
              width: 110.0,
            ),
          ),
          InkWell(
            onTap: () => Get.toNamed('/payment/qr'),
            child: SvgPicture.asset('assets/svgs/ic/ic_qr_main.svg'),
          ),
        ],
      ),
    );
  }

  Widget renderWelcome() {
    final theme = ThemeFactory.of(context).theme;

    return GestureDetector(
      onTap: () {},
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '반가워요,',
              style: TextStyle(
                fontSize: theme.fontSize20,
              ),
            ),
            SizedBox(height: 2),
            renderNameRow(),
          ],
        ),
      ),
    );
  }

  Widget renderNameRow() {
    final theme = ThemeFactory.of(context).theme;

    return GetBuilder<UserController>(
      builder: (c) {
        final UserInfoResponse? ui = c.userInfo;
        final name = ui == null ? null : '${ui.lastname + ui.firstname}님 🤗';

        return Row(
          children: [
            Text(
              name ?? '',
              style: TextStyle(
                fontSize: theme.fontSize20,
                fontWeight: theme.heavyFontWeight,
              ),
            ),
            Spacer(),
            renderOptionButtons(),
          ],
        );
      },
    );
  }

  Widget renderOptionButtons() {
    return GestureDetector(
      onTap: () {
        final box = addKey.currentContext!.findRenderObject() as RenderBox;
        final Offset position = box.localToGlobal(Offset.zero);

        double y = position.dy;
        double x = position.dx;
        Navigator.of(context)
            .push(HomeOptionModal(x: x, y: y, haveAccount: haveAccount));
      },
      child: SvgPicture.asset('assets/svgs/ic/ic_plus_main.svg', key: addKey),
    );
  }

  Widget renderAlert() {
    if (isAlertShow == false) return Container();

    return GestureDetector(
      onTap: () {
        Get.toNamed('/kkakka/list-request');
      },
      child: Container(
        height: 48,
        padding: EdgeInsets.symmetric(horizontal: 14),
        margin: EdgeInsets.symmetric(horizontal: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: kinsBlue40,
        ),
        child: Row(
          children: [
            Expanded(child: renderAlertTitle(23)),
            Icon(
              Icons.arrow_forward_ios,
              color: kinsWhite,
              size: 15,
            ),
          ],
        ),
      ),
    );
  }

  /// 작성자 : Baron \
  /// 작성일 : 2021-05-07 \
  ///
  /// {@category Function}
  requestKkakkaCount() async {
    kkakkaCount = await _controller.services.kkakkaService
        .getReqKkakka({}).then((result) => result.data);
    setState(() {});
  }

  Widget renderAlertTitle(int length) {
    return RichText(
      text: TextSpan(
        style: TextStyle(
          color: kinsWhite,
          fontSize: ThemeFactory.of(context).theme.fontSize14,
        ),
        children: <TextSpan>[
          TextSpan(text: '🔔 까까 요청 '),
          TextSpan(
            text: '${kkakkaCount}',
            style: TextStyle(
              color: Color(0xFFF29061),
              fontWeight: FontWeight.w700,
              fontSize: ThemeFactory.of(context).theme.fontSize14,
            ),
          ),
          TextSpan(text: ' 개가 회원님을 기다리고 있어요.'),
        ],
      ),
    );
  }

  Widget renderMainPageView() {
    return PageView(
      controller: _pageController,
      children: <Widget>[
        HomePostListView(
          onListItemTap: _onListItemTap,
        ),
        HomeListView(
          type: ScreenTypes.received,
          kkakkaController: _controller,
          sumModel: sumModel,
          onListItemTap: _onListItemTap,
        ),
        HomeListView(
          type: ScreenTypes.give,
          kkakkaController: _controller,
          sumModel: sumModel,
          onListItemTap: _onListItemTap,
        ),
      ],
    );
  }

  Widget renderNewsFeed() {
    return InkWell(
      child: NonKkakkaCard(
        height: double.infinity,
        title: '아직 소식이 없습니다!',
        isOutline: false,
      ),
      onTap: () {
        Get.toNamed('/main-home/news-feed');
      },
    );
  }

  _onListItemTap(List<dynamic> list, int index) async {
    final int id = list[index].runtimeType == PostListModel
        ? list[index].idNo
        : list[index].kkakkaId!;
    final model = await Get.toNamed('/main-home/request/detail', arguments: id);

    if (model == null) return;

    list[index] = model;
    _setStateEndAnimation();
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }

  @override
  Widget build(BuildContext context) {
    _setTopBarHeight();

    return NestedScrollLayout(
      expandedHeight: topBarHeight,
      collapsedHeight: 50,
      floating: false,
      topBar: SizedBox(
        height: topBarHeight,
        width: double.infinity,
        child: renderTopBar(),
      ),
      body: Column(
        children: [
          Container(height: 1, color: kinsGrayE8E),
          Expanded(
            child: renderMainPageView(),
          ),
        ],
      ),
    );
  }
}
