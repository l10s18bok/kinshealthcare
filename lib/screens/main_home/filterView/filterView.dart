// import 'package:flutter/material.dart';
// import 'package:get/instance_manager.dart';
// import 'package:get/get.dart';
// /*
//  작성일 : 2021-05-28
//  작성자 : (baron)
//  화면명 : (HH_3001, HH_4001)
//  경로 :
//  클래스 : FilterView
//  설명 : (필터 조건별 보여주는 뷰 )
// */

// class FilterView extends StatefulWidget {
//   @override
//   _FilterViewState createState() => _FilterViewState();
// }

// class _FilterViewState extends State<FilterView> {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Column(
//         children: [
//           Container(),
//           Get.arguments['type'] == 'receive'
//               ? ReceiveTabView(
//                   isFilterPage: true,
//                 )
//               : SentTabView(
//                   isFilterPage: true,
//                 )
//         ],
//       ),
//     );
//   }
// }
