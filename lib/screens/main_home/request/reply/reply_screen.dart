import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/post_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/models/local/post_model.dart';
import 'package:kins_healthcare/services/post/model/post_pagination_body.dart';
import 'package:kins_healthcare/services/post/model/reply_list_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-04-09
 작성자 : Mark,
 화면명 : HH_Like,
 경로  : /main-home/request/reply
 클래스 : ReplyScreen,
 설명 : 댓글 List 를 표시하는 Screen
*/

class ReplyScreen extends StatefulWidget {
  @override
  _ReplyScreenState createState() => _ReplyScreenState();
}

class _ReplyScreenState extends State<ReplyScreen> {
  final tec = TextEditingController();
  FocusNode node = FocusNode();

  late PostController _postController;
  late PostPaginationBody body;
  late final PostLocalModel postModel;

  @override
  void initState() {
    super.initState();
    postModel = Get.arguments;

    _postController = Get.find<PostController>();
    // baron 수정.
    // setState 줄이기 위해  GetBuilder 사용
    // _postController.addListener(_setStateEndAnimation);
    Get.find<UserController>().getUserInfo();
    _refresh();
  }

  @override
  void dispose() {
    super.dispose();
    // _postController.removeListener(_setStateEndAnimation);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        clearCache();
        return Future.value(true);
      },
      child: GestureDetector(
        child: NestedScrollLayout(
          topBar: BackTopBar(
            title: '댓글',
            customOnBack: () {
              clearCache();
              Get.back();
            },
          ),
          body: renderMain(),
        ),
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      ),
    );
  }

  Widget renderMain() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: renderPageListView(),
          ),
        ),
        renderBottomView(),
      ],
    );
  }

  Widget renderPageListView() {
    return GetBuilder<PostController>(
        builder: (controller) => PaginationListView<ReplyListModel>(
              itemCount: controller.postReplyList.length,
              itemBuilder: (context, index) {
                return renderListItem(index, controller.postReplyList);
              },
              controller: controller,
              request: controller.getReplyList,
              lastWidget: Container(),
              loadingWidget: renderLoading(),
              emptyWidget: renderNonCard(),
            ));
  }

  Widget renderNonCard() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10, left: 16, right: 16, top: 20.0),
      child: NonKkakkaCard(title: '작성된 댓글이 없습니다.'),
    );
  }

  Widget renderLoading() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      height: 50,
      child: CircularProgressIndicator(),
    );
  }

  Widget renderListItem(int index, List<dynamic> data) {
    final top = index == 0 ? 30.0 : 0.0;
    final model = data[index];
    return Container(
      padding: EdgeInsets.only(top: top, bottom: 17),
      width: double.infinity,
      child: renderReply(model),
    );
  }

  Widget renderReply(ReplyListModel model) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CircleNetworkImage(
          imageSize: 36,
          userNo: model.userNo,
          path: model.userImg,
          heroTag: model.userImg,
        ),
        SizedBox(width: 16),
        Expanded(child: renderContent(model)),
      ],
    );
  }

  Widget renderContent(ReplyListModel model) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        renderItemContent(
          model.userName ?? '',
          model.userRel ?? '',
          model.daysTillEndDate,
        ),
        SizedBox(height: 2),
        Text(
          model.content ?? '',
        ),
      ],
    );
  }

  Widget renderItemContent(String name, String nickName, String date) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          nickName.isEmpty ? '$name' : '$name ($nickName)',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
          ),
        ),
        SizedBox(width: 6),
        Text(
          '$date 전',
          style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
            color: kinsGray98,
          ),
        ),
      ],
    );
  }

  Widget renderLikeRow() {
    return Container(
      margin: EdgeInsets.only(left: 45),
      height: 30,
      child: Row(
        children: [
          renderLikeImage(30),
          SizedBox(width: 6),
          renderLikeTitle(),
        ],
      ),
    );
  }

  Widget renderLikeImage(double size) {
    return SvgPicture.asset(
      'assets/svgs/ic/ic_heart_like.svg',
      width: size,
      height: size,
    );
  }

  Widget renderLikeTitle() {
    return RichText(
      text: TextSpan(
        style: TextStyle(
          color: kinsBlack,
          fontSize: ThemeFactory.of(context).theme.fontSize13,
        ),
        children: <TextSpan>[
          TextSpan(
            text: '아빠',
            style: TextStyle(fontWeight: FontWeight.w700),
          ),
          TextSpan(text: '님 외 '),
          TextSpan(
            text: '4',
            style: TextStyle(fontWeight: FontWeight.w700),
          ),
          TextSpan(text: '명이 좋아합니다.'),
        ],
      ),
    );
  }

  Widget renderBottomView() {
    return Container(
      height: 50,
      padding: EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        color: kinsWhite,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 3,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Row(
        children: [
          GetBuilder<UserController>(builder: (c) {
            return CircleNetworkImage(
              imageSize: 32,
              path: c.userInfo != null ? c.userInfo!.image : null,
            );
          }),
          SizedBox(width: 8),
          Expanded(child: renderTextInput()),
        ],
      ),
    );
  }

  Widget renderTextInput() {
    return Container(
      height: 38,
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xFFE8E8E8)),
        borderRadius: BorderRadius.circular(4),
      ),
      child: renderTextFieldRow(),
    );
  }

  Widget renderTextFieldRow() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(child: renderTextField()),
        SizedBox(width: 10),
        InkWell(
          child: Text(
            '게시',
            style: TextStyle(
              fontSize: ThemeFactory.of(context).theme.fontSize13,
              fontWeight: FontWeight.w700,
              color: kinsBlue40,
            ),
          ),
          onTap: _inputOnTap,
        ),
        SizedBox(width: 10),
      ],
    );
  }

  Widget renderTextField() {
    return TextFormField(
      controller: tec,
      focusNode: node,
      cursorColor: Colors.black,
      style: TextStyle(
        fontSize: ThemeFactory.of(context).theme.fontSize13,
      ),
      decoration: InputDecoration(
        isDense: true,
        border: InputBorder.none,
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        disabledBorder: InputBorder.none,
        contentPadding: EdgeInsets.symmetric(horizontal: 10),
        hintText: '댓글입력',
      ),
    );
  }

  _inputOnTap() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final text = tec.text;
    if (text.isEmpty) return;
    tec.text = '';

    await _postController.insertReply(
      postModel.boardNo,
      postModel.boardType,
      text,
    );
    _refresh();
  }

  Future<void> _refresh() async {
    body = PostPaginationBody(
      sortBy: 'startDate',
      boardNo: postModel.boardNo,
      boardType: postModel.boardType,
    );

    await _postController.getReplyList(
      body: body,
      reset: true,
    );
  }

  clearCache() {
    final length = _postController.postReplyList.length;
    if (length != 0) _postController.removeCacheList(type: ReplyListModel);
  }
}
