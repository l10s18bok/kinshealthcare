import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/bottom_sheet/category_filter.dart';
import 'package:kins_healthcare/components/bottom_sheet/simple_information_bottom_sheet.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/card/request_kkakka_card.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/controllers/kkakka_controller.dart';
import 'package:kins_healthcare/components/request_top.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/models/local/local_filter_model.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/kkakka/model/list_filter_pagination_body.dart';
import 'package:kins_healthcare/services/kkakka/model/kkakka_filter_response_model.dart';

import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/services/kkakka/model/list_send_opponent.dart';
import 'package:kins_healthcare/services/kkakka/model/sum_kkakka_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import '../../../components/top_bar/pre_request_top_bar.dart';

/*
 작성일 : 2021-03-10
 작성자 : Mark,
 화면명 : HH_3001, HH_4001
 경로 :
 클래스 : RequestScreenPage,
 설명 : PaginationListView Test Class
*/

enum ScreenTypes {
  received, //까까로 받은
  give, //까까로 보낸
}

class RequestScreenPage extends StatefulWidget {
  RequestScreenPage({
    Key? key,
    required this.type,
  }) : super(key: key);

  final ScreenTypes type;

  @override
  RequestScreenStatePage createState() => RequestScreenStatePage();
}

class RequestScreenStatePage extends State<RequestScreenPage> {
  KkakkaController kkakkaController = Get.find<KkakkaController>();
  LocalFilterModel filterModel = LocalFilterModel();
  ListFilterPaginationBody? body;
  SumKkakkaModel? sumModel;
  List<ListSendOpponentModel> opponents = [];
  bool isDefaultKey = true;
  bool haveAccount = true; // 계좌 유무 true = 유, false = 무
  bool isFilter = false;

  @override
  void initState() {
    setFilterType();
    setFilterKey();
    initKkasumAndOpponent();
    checkAccount(); //2021.04.12 Andy 추가 (결제가능 유무 체크)
    initFilter();
    super.initState();
    kkakkaController.addListener(_setStateEndAnimation);
  }

  //내가 요청 받은것은 그냥 날짜없이 필터가능
  //내가 요청한것은 날짜 없이 요청시 오류발생.
  initFilter() async {
    final format = DateFormat('yyyy-MM-dd');
    if (widget.type == ScreenTypes.received) {
      body = ListFilterPaginationBody(
        kkakkaFilterType: 'REQME',
      );
    } else {
      body = ListFilterPaginationBody(
          kkakkaFilterType: 'SENDOPPO',
          fromDate: format.format(
            DateTime.now().subtract(Duration(days: 30)),
          ),
          toDate: format.format(DateTime.now()));
    }
    await requestFilter(
      b: body!,
    );
    // 필터 페이지 들어갈때 기본 base body 구성.
  }

  initKkasumAndOpponent() async {
    await _getOpponentModel();
    await _getKkakkaSum();
  }

  setFilterKey() async {
    await kkakkaController.setFilterKey(_getScreenTypeKey() + 'default');
  }

  requestFilter({required ListFilterPaginationBody b}) async {
    await kkakkaController.kkakkaListFilter(
      body: b,
      reset: true,
      key: _getScreenTypeKey() + 'filter',
    );
  }

  switchfilter() async {
    await kkakkaController
        .switchFilter(widget.type == ScreenTypes.received ? true : false);
  }

  checkAccount() async {
    try {
      final resp = await Get.find<UserController>().getUserPayCardInfo();
      if (resp == null) haveAccount = false;
    } on DioError catch (e) {
      haveAccount = false;
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  setFilterType() {
    switch (widget.type) {
      case ScreenTypes.received:
        filterModel.filterType = KkakkaFilterType.SENDME;
        break;
      case ScreenTypes.give:
        filterModel.filterType = KkakkaFilterType.SENDOPPONENT;
        break;
    }
  }

  @override
  void dispose() {
    super.dispose();
    kkakkaController.removeListener(_setStateEndAnimation);
  }

// pagination 으로 구성 예정.
// 필터링버튼 클릭하여 조건 설정후 확인버튼 누르기 전에는 보낸까까 받은까까 페이지네이션 뷰 그데로 옮겨와 보여준다.
// 필터링 후 Get.back()으로 filterBody 리턴 받으면 filter페이지네이션 뷰로 보여준다.

// 기존코드는 listView에 최근7일 data 우선 보여주고 필터링 후
  Widget renderMain() {
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 500),
      transitionBuilder: (child, animation) {
        return FadeTransition(child: child, opacity: animation);
      },
      child: renderPageListView(_getList()),
    );
  }

  Widget renderTopBar() {
    return Padding(
      padding: EdgeInsets.only(left: 16, right: 16),
      child: PreRequestTopBar(
        title: _getTitle(),
        onRightBtnTap: () async {
          if (widget.type == ScreenTypes.received)
            Get.toNamed('/kkakka/request/target');
          else if (widget.type == ScreenTypes.give) {
            //2021.04.12 Andy 추가
            if (haveAccount)
              Get.offNamed('/kkakka/issuance/target');
            else {
              final sheet =
                  SimpleInformationBottomSheet(message: '결제수단을 등록해주세요.');
              final result = await Get.bottomSheet<String>(sheet);
              if (result == 'OK')
                Get.offNamed('/payment/methods/register/card');
            }
          }
        },
        // filterBtn 리턴 sheet
        onFilterBtnTap: () async {
          final getModel = await Get.bottomSheet(
            CategoryFilter(),
            isScrollControlled: true,
          );
          if (getModel != null) {
            filterModel = getModel;
            await kkakkaController.updateFilterBody(filterModel.listFilterBody);
            await kkakkaController.setFilterKey(_getScreenTypeKey() + 'filter');
            await requestFilter(b: filterModel.listFilterBody);
            setState(() {
              isFilter = true;
            });
          }
          ;
        },
      ),
    );
  }

//
  Widget renderPageListView(List<dynamic> data) {
    return PaginationListView<KkakkaFilterResponseModel>(
        itemCount: data.length,
        itemBuilder: (context, index) {
          return renderListItem(data, index);
        },
        controller: kkakkaController,
        request: kkakkaController.kkakkaListFilter,
        // nextPageCallback: () {
        //   print('filter nextPage....');
        //   print('filter nextPage....');
        // },
        emptyWidget: renderNonCard(),
        lastWidget: Container(),
        loadingWidget: renderLoading(),
        cacheKey: kkakkaController.filterkey!);
  }

  Widget renderFirstItem() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            "${_getSumPrice(sumModel)}원",
            style: TextStyle(
              color: kinsBlue40,
              fontWeight: FontWeight.w700,
              fontSize: ThemeFactory.of(context).theme.fontSize20,
            ),
          ),
        ),
        SizedBox(height: 16),
        renderPreRequestTop(),
        SizedBox(height: 10),
      ],
    );
  }

  Widget renderPreRequestTop() {
    if (ScreenTypes.received != widget.type) return Container();

    return RequestTop(
      onAddTap: () => print('onAddTap'),
      list: opponents,
    );
  }

  Widget renderNonCard() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10, left: 16, right: 16, top: 20.0),
      child: NonKkakkaCard(title: _getNonTitle()),
    );
  }

  Widget renderListItem(List<dynamic> data, int index) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [Center(child: renderCard(data, index))],
        ));
  }

  Widget renderCard(List<dynamic> data, int index) {
    return Column(
      children: [
        index == 0 ? renderFirstItem() : Container(),
        SizedBox(height: 20),
        RequestKkakkaCard(
          kkakka: data[index],
          onTap: () async {
            await Get.toNamed('/main-home/request/detail',
                arguments: data[index].kkakkaId);
          },
          index: index,
        ),
      ],
    );
  }

  String? numberWithComma(int? param) {
    if (param == null) return null;
    return new NumberFormat('###,###,###,###')
        .format(param)
        .replaceAll(' ', '');
  }

  Widget renderLoading() {
    return Container();
  }

  _getOpponentModel() async {
    if (widget.type == ScreenTypes.give) return;

    try {
      opponents = await kkakkaController.listSendOpponent();
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  _getKkakkaSum() async {
    try {
      sumModel = await kkakkaController.sumKkakka();
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  String _getTitle() {
    switch (widget.type) {
      case ScreenTypes.received:
        return '까까로 받은 금액';
      case ScreenTypes.give:
        return '까까로 보낸 금액';
      default:
        return '까까로 받은 금액';
    }
  }

  String _getNonTitle() {
    switch (widget.type) {
      case ScreenTypes.received:
        return '아직 약속 받은 \'까까\'가 없어요.\n요청해보세요!';
      case ScreenTypes.give:
        return '아직 약속 해준 \'까까\'가 없어요.\n발행해보세요!';
      default:
        return '아직 \'까까\'가 없어요.\n요청해보세요!';
    }
  }

  String _getSumPrice(SumKkakkaModel? model) {
    if (model == null) return '0';

    switch (widget.type) {
      case ScreenTypes.received:
        if (model.sumMe == null) return '0';
        return NumberFormat("#,###").format(model.sumMe);
      case ScreenTypes.give:
        if (model.sumOpp == null) return '0';
        return NumberFormat("#,###").format(model.sumOpp);
      default:
        return '0';
    }
  }

  String _getScreenTypeKey() {
    switch (widget.type) {
      case ScreenTypes.received:
        return 'received';
      case ScreenTypes.give:
        return 'give';
    }
  }

  _getList() {
    if (isFilter) {
      print('this is filter list.....');
      switch (widget.type) {
        case ScreenTypes.received:
          return kkakkaController.cache_receiveFilter;
        case ScreenTypes.give:
          return kkakkaController.cache_giveFilter;
      }
    } else {
      switch (widget.type) {
        case ScreenTypes.received:
          return kkakkaController.cache_receiveDefaultFilter;
        case ScreenTypes.give:
          return kkakkaController.cache_giveDefaultFilter;
      }
    }
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: renderTopBar(),
      body: renderMain(),
    );
  }
}
