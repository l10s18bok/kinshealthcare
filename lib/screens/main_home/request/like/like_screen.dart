import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/components/top_bar/like_top_bar.dart';
import 'package:kins_healthcare/controllers/post_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/models/local/post_model.dart';
import 'package:kins_healthcare/services/post/model/like_list_model.dart';
import 'package:kins_healthcare/services/post/model/post_pagination_body.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 작성일 : 2021-01-26
 작성자 : Mark,
 화면명 : HH_Like,
 경로  : /main-home/request/like
 클래스 : LikeScreen,
 설명 : 좋아요를 누른 가족 List 를 표시
*/

class LikeScreen extends StatefulWidget {
  @override
  _LikeScreenState createState() => _LikeScreenState();
}

class _LikeScreenState extends State<LikeScreen> {
  late PostController _postController;
  late PostPaginationBody body;
  late final PostLocalModel postModel;
  @override
  void initState() {
    super.initState();
    postModel = Get.arguments;

    _postController = Get.find<PostController>();
    _postController.addListener(_setStateEndAnimation);
    _refresh();
  }

  @override
  void dispose() {
    super.dispose();
    _postController.removeListener(_setStateEndAnimation);
  }

  @override
  Widget build(BuildContext context) {
    final total = _postController.postLikeToTal;

    return NestedScrollLayout(
      topBar: LikeTopBar(likeCount: total),
      body: Padding(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: RefreshIndicator(
                onRefresh: _refresh,
                child: renderPageListView(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget renderPageListView() {
    final length = _postController.postLikeList.length;
    return PaginationListView<LikeListModel>(
      itemCount: length == 0 ? 1 : length,
      itemBuilder: (context, index) {
        if (length == 0) return renderNonCard();
        return renderListItem(index);
      },
      controller: _postController,
      request: _postController.getLikeList,
      lastWidget: Container(),
      loadingWidget: renderLoading(),
    );
  }

  Widget renderLoading() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      height: 50,
      child: CircularProgressIndicator(),
    );
  }

  Widget renderNonCard() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10, left: 16, right: 16, top: 20.0),
      child: NonKkakkaCard(title: '좋아요 누른 가족이 없습니다.'),
    );
  }

  Widget renderListItem(int index) {
    final top = index == 0 ? 30.0 : 0.0;
    final model = _postController.postLikeList[index] as LikeListModel;

    return Container(
      padding: EdgeInsets.only(top: top),
      width: double.infinity,
      height: 84,
      child: Row(
        children: [
          CircleNetworkImage(path: model.userImg, imageSize: 56),
          SizedBox(width: 16),
          Expanded(
              child: renderItemContent(
            model.userName ?? '',
            model.userRel ?? '',
          )),
          SizedBox(width: 16),
        ],
      ),
    );
  }

  Widget renderItemContent(String name, String nickName) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '$name ($nickName)',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
          ),
        ),
        SizedBox(height: 10),
      ],
    );
  }

  Future<void> _refresh() async {
    body = PostPaginationBody(
      boardNo: postModel.boardNo,
      boardType: postModel.boardType,
    );

    await _postController.getLikeList(
      body: body,
      reset: true,
    );
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }
}
