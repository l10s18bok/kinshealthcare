import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/btn_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/image_picker_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/text_field/out_line_text_field.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/post_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/ok_bottom_sheet.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 * 작성일 : 2021-04-12
 * 작성자 : Mark
 * 업데이트날짜: 2021-05-03
 * 작성자: baron
 * 화면명 : HU_6001
 * 경로  : /main-home/news-feed
 * 주요기능 : 소식 작성하기, 소식 수정하기
 */

class NewsFeedScreen extends StatefulWidget {
  @override
  _NewsFeedScreenState createState() => _NewsFeedScreenState();
}

class _NewsFeedScreenState extends State<NewsFeedScreen> {
  final tec = TextEditingController();
  List<File?> imageList = [null];

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: CustomScrollLayout(
        topBar: BackTopBar(title: '소식 작성하기'),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: renderMain(),
        ),
      ),
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
    );
  }

  Widget renderMain() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 20),
        Text(
          '카드 사진',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize13,
          ),
        ),
        SizedBox(height: 5),
        SizedBox(height: 80, child: renderList()),
        SizedBox(height: 20),
        renderContentTitle(),
        SizedBox(height: 5),
        renderTextField('회원님의 마음을 입력해주세요.', tec),
        Spacer(),
        renderSubmitBtn(),
      ],
    );
  }

  Widget renderList() {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: imageList.length,
      itemBuilder: (_, index) {
        if (index == 0) {
          return Align(
            alignment: Alignment.center,
            child: renderCameraBox(),
          );
        }

        final file = imageList[index];
        if (file == null) return Container();

        return renderImageBox(file);
      },
    );
  }

  Widget renderCameraBox() {
    final theme = ThemeFactory.of(context).theme;
    final itemCount = imageList.length - 1;

    return InkWell(
      onTap: _getImage,
      child: Container(
        height: 72,
        width: 72,
        decoration: BoxDecoration(
          border: Border.all(color: Color(0xFFD7D8EE), width: 1),
          borderRadius: BorderRadius.circular(6),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              'assets/svgs/ic/ic_camera.svg',
              width: 40,
              height: 40,
            ),
            Text(
              '$itemCount/3',
              style: TextStyle(
                fontSize: theme.fontSize12,
                color: Color(0xFF9596A7),
                fontWeight: theme.heavyFontWeight,
              ),
            ),
          ],
        ),
      ),
    );
  }

  _getImage() async {
    if (imageList.length >= 4) {
      final sheet = BtnBottomSheet(
        boldTitle: '최대 3개',
        title: '의 이미지를 추가할 수 있습니다.',
        isOneBtn: true,
      );
      Get.bottomSheet(sheet);
      return;
    }

    final sheet = ImagePickerBottomSheet(processImage: true);
    final resp = await Get.bottomSheet(sheet);
    if (resp == null) return;

    imageList.add(resp.file);
    setState(() {});
  }

  Widget renderImageBox(File image) {
    return Container(
      margin: EdgeInsets.only(left: 20),
      height: 80,
      width: 80,
      child: Stack(
        alignment: Alignment.centerLeft,
        children: [
          renderImage(image),
          Positioned(
            right: 0,
            top: 0,
            child: renderDeleteBtn(image),
          ),
        ],
      ),
    );
  }

  Widget renderImage(File image) {
    return Container(
      width: 72,
      height: 72,
      decoration: BoxDecoration(
        border: Border.all(
          color: Color(0xFFD7D8EE),
          width: 1,
        ),
        borderRadius: BorderRadius.circular(6),
      ),
      child: Image.file(image, fit: BoxFit.cover),
    );
  }

  Widget renderDeleteBtn(File image) {
    return InkWell(
      child: SvgPicture.asset(
        'assets/svgs/ic/ic_delete.svg',
        width: 20,
        height: 20,
      ),
      onTap: () {
        imageList.remove(image);
        setState(() {});
      },
    );
  }

  Widget renderContentTitle() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '한줄편지',
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize13,
            fontWeight: FontWeight.w700,
          ),
        ),
        SizedBox(width: 6),
        Text(
          '선택',
          style: TextStyle(
            fontSize: ThemeFactory.of(context).theme.fontSize13,
            fontWeight: FontWeight.w700,
            color: kinsGrayB6,
          ),
        ),
      ],
    );
  }

  Widget renderTextField(String hint, TextEditingController tec) {
    return OutLineTfCS(
      controller: tec,
      hint: hint,
      height: 160,
      outLineColor: kinsBlueD7,
      textColor: kinsBlack,
      inputType: TextInputType.multiline,
      maxLine: 10,
      maxLength: 400,
      hintColor: kinsGrayB6,
      fontSize: ThemeFactory.of(context).theme.fontSize15,
      fontWeight: FontWeight.w400,
      counterText: '',
      radius: 6.0,
    );
  }

  Widget renderSubmitBtn() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      height: 60,
      child: PrimaryButton(
        padding: EdgeInsets.zero,
        label: '다음',
        onTap: _onNextBtnTap,
      ),
    );
  }

  _onNextBtnTap() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    try {
      print('---------');
      print(imageList.length);
      if (imageList.length == 1) {
        final sheet = OkBottomSheet(
          title: '이미지 선택 해주세요..',
          content: '',
        );
        await Get.bottomSheet(sheet);
      } else {
        final controller = Get.find<PostController>();
        final message = tec.text;
        List<File> uploadFiles = [];

        for (final item in imageList) {
          if (item == null) continue;
          uploadFiles.add(item);
        }

        final data = await controller.register(message, uploadFiles);
        final sheet = OkBottomSheet(
          title: data.resultCode == 'SUCCESS' ? '소식 작성이 완료 되였습니다.' : '',
          content: data.resultMsg ?? '성공',
        );
        await Get.find<PostController>().getList(reset: true);
        // await Get.bottomSheet(sheet);
        Get.back();
      }
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }
}
