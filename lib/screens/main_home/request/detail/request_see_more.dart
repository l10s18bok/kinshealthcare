import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/bottom_sheet/btn_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/card/kkakka_detail_content.dart';
import 'package:kins_healthcare/components/card/radius_network_image.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/kkakka_controller.dart';
import 'package:kins_healthcare/enums/model_enums.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/models/kkakka_model.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 * 작성일 : 2021-01-29
 * 작성자 : Andy
 * 화면명 : HD_0001
 * 경로  : /main-home/request/detail
 * 주요기능 : 까까 발행 => 자세히보기
 */

class RequestSeeMore extends StatefulWidget {
  @override
  _RequestSeeMoreState createState() => _RequestSeeMoreState();
}

class _RequestSeeMoreState extends State<RequestSeeMore>
    with TickerProviderStateMixin {
  KkakkaModel? model;
  bool isUse = true;

  @override
  void initState() {
    super.initState();
    _getDetail();
  }

  _getDetail() async {
    final kkakkaId = Get.arguments;
    try {
      model = await Get.find<KkakkaController>().kkakkaDetail(kkakkaId);
      isUse = model!.kkakkaStatus == KkakkaStatus.ACTIVE;

      _setStateEndAnimation();
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollLayout(
      topBar: BackTopBar(title: '자세히 보기'),
      body: renderMain(),
    );
  }

  renderMain() {
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 500),
      transitionBuilder: (child, animation) {
        return FadeTransition(child: child, opacity: animation);
      },
      child: Column(
        key: ValueKey<KkakkaModel?>(model),
        children: [
          SizedBox(height: 10),
          Expanded(child: renderMainTop()),
          SizedBox(height: 38),
          renderBottom(),
        ],
      ),
    );
  }

  renderMainTop() {
    final content = model?.kkakkaMessage ?? '메세지를 입력하지 않았습니다.';

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        renderImage(),
        SizedBox(height: 5),
        KkakkaDetailContent(content: content),
        SizedBox(height: 20),
        renderSendTitle(),
        SizedBox(height: 12),
        renderSendPerson(),
        SizedBox(height: 26),
        Divider(color: kinsBlueE8, thickness: 6),
        SizedBox(height: 20),
        ...renderIssueList(model),
      ],
    );
  }

  renderImage() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: AspectRatio(
        aspectRatio: 328 / 220,
        child: RadiusNetworkImage(
          imageHeight: 228,
          imageWidth: double.infinity,
          radius: 6,
          path: model?.kkakkaImage,
        ),
      ),
    );
  }

  renderSendTitle() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Text(
        '받은 분',
        style: TextStyle(
          fontSize: ThemeFactory.of(context).theme.fontSize14,
          fontWeight: FontWeight.w400,
          color: kinsBlack1D,
        ),
      ),
    );
  }

  renderSendPerson() {
    return Padding(
      padding: EdgeInsets.only(left: 16, right: 16),
      child: Row(
        children: [
          renderPersonImage(),
          SizedBox(width: 15),
          renderPersonContent(),
        ],
      ),
    );
  }

  renderPersonImage() {
    final path = model?.profileImgOpponent;

    return Container(
      height: 50,
      width: 50,
      child: Stack(
        alignment: Alignment.topLeft,
        children: [
          CircleNetworkImage(path: path, imageSize: 47.0),
          Positioned(right: 0, bottom: 0, child: renderBadge())
        ],
      ),
    );
  }

  renderBadge() {
    final iconName = model?.kkakkaIcName ?? "ic_donate_kkakka.svg";
    final primaryColor = model?.primaryColor ?? kinsPuple8B;

    return Container(
      width: 22,
      height: 22,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(width: 1, color: primaryColor),
        shape: BoxShape.circle,
      ),
      child: SvgPicture.asset(
        'assets/svgs/ic/$iconName',
        height: 15,
        width: 15,
        color: primaryColor,
      ),
    );
  }

  renderPersonContent() {
    final phone = model?.userPhoneOpponent ?? '';

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        renderName(),
        SizedBox(height: 7),
        Text(
          phone,
          style: TextStyle(
            color: kinsGray53,
            fontSize: ThemeFactory.of(context).theme.fontSize11,
          ),
        ),
      ],
    );
  }

  renderName() {
    final name = model?.userNameOpponent ?? '';
    final subName = model?.userRelCdOpponent ?? '';

    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          name,
          style: TextStyle(
            color: kinsBlack36,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(width: 4),
        Text(
          subName,
          style: TextStyle(
            color: kinsBlue40,
            fontSize: ThemeFactory.of(context).theme.fontSize12,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  renderIssueList(KkakkaModel? model) {
    if (model == null) {
      return [
        renderIssue('보낸 날짜', '', ''),
        renderIssue('보낸 금액', '', ''),
      ];
    }

    final format = new DateFormat('yyyy년 MM월 dd일');
    final startDate = format.format(model.startDate!);
    final endDate = format.format(model.endDate ?? DateTime.now());
    final issueDate = format.format(model.issueDate!);
    final price = NumberFormat("#,###").format(model.kkakkaPrice);
    final limit = NumberFormat("#,###").format(model.kkakkaLimit);

    return [
      renderIssue('보낸 날짜', issueDate, '(기간:$startDate ~ $endDate)'),
      renderIssue('보낸 금액', '$price원', '(1회 사용 한도:$limit원)'),
    ];
  }

  renderIssue(String leading, String title, String subTitle) {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16, bottom: 17),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            leading,
            style: TextStyle(
              color: Color(0xFF1D1D1D),
              fontSize: ThemeFactory.of(context).theme.fontSize14,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: renderIssueValue(title, subTitle),
          )
        ],
      ),
    );
  }

  renderIssueValue(String title, String subTitle) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
            color: Color(0xFF6C6C6C),
            fontSize: ThemeFactory.of(context).theme.fontSize13,
          ),
        ),
        Text(
          subTitle,
          style: TextStyle(
            color: Color(0xFF6C6C6C),
            fontSize: ThemeFactory.of(context).theme.fontSize13,
          ),
        ),
      ],
    );
  }

  renderBottom() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          renderBalance(),
          SizedBox(height: 8),
          renderValidityText(),
          SizedBox(height: 25),
          renderBottomButton(),
          SizedBox(height: 16),
        ],
      ),
    );
  }

  renderBalance() {
    final balance = model?.kkakkaBalance ?? 0.0;
    final showBalance = NumberFormat("#,###").format(balance);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          '현재잔액',
          style: TextStyle(
            color: kinsBlack1D,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          '${showBalance}원',
          style: TextStyle(
            color: isUse ? kinsOrangeF2 : kinsBlack6D,
            fontSize: ThemeFactory.of(context).theme.fontSize16,
            fontWeight: ThemeFactory.of(context).theme.heavyFontWeight,
          ),
        ),
      ],
    );
  }

  renderValidityText() {
    final daysTillEndDate = model?.daysTillEndDate;

    String noticeText;
    Widget icon;
    Color textColor;
    if (isUse) {
      noticeText = daysTillEndDate != null
          ? '유효기간이 $daysTillEndDate 남았습니다.'
          : '유효기간이 미설정.';
      icon = Container();
      textColor = kinsGray96;
    } else {
      noticeText = '해당 쿠폰은 사용중지되었습니다';
      icon = Icon(Icons.error_outline, color: kinsBlue40, size: 20);
      textColor = kinsBlue40;
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        icon,
        Text(
          noticeText,
          textAlign: TextAlign.right,
          style: TextStyle(
            color: textColor,
            fontSize: ThemeFactory.of(context).theme.fontSize12,
          ),
        ),
      ],
    );
  }

  renderBottomButton() {
    String title = '확인';
    if (model?.authYn == 'Y') title = isUse ? '정지시키기' : '활성화하기';

    return Container(
      height: 55,
      child: PrimaryButton(
        padding: EdgeInsets.zero,
        label: title,
        onTap: () async {
          if (title == '확인') return Get.back();

          await _updateStatus(model);
          await _getDetail();
        },
      ),
    );
  }

  _updateStatus(KkakkaModel? model) async {
    if (model == null) return;

    final isSetActive = model.kkakkaStatus != KkakkaStatus.ACTIVE;
    final id = model.kkakkaId;
    try {
      final model =
          await Get.find<KkakkaController>().statusUpdate(id!, isSetActive);
      await _getDetail();

      final sheet = BtnBottomSheet(
        title: model.resultMsg ?? '완료되었습니다.',
        isOneBtn: true,
      );
      await Get.bottomSheet(sheet);
      Get.back(result: this.model);
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }
}
