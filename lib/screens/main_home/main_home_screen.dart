// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:get/get.dart';
// import 'package:kins_healthcare/components/card/card.dart';
// import 'package:kins_healthcare/components/modal/coach_mark_modal.dart';
// import 'package:kins_healthcare/components/modal/home_option_modal.dart';
// import 'package:kins_healthcare/controllers/kkakka_controller.dart';
// import 'package:kins_healthcare/layouts/default_layout.dart';
// import 'package:kins_healthcare/themes/theme_factory.dart';

// /*
//  * 작성일 : 2021-01-11
//  * 작성자 : JC
//  * 화면명 : HH_0001-2
//  * 주요기능 : 홈 스크린
//  */
// class MainHomeScreen extends StatefulWidget {
//   @override
//   _MainHomeScreenState createState() => _MainHomeScreenState();
// }

// class _MainHomeScreenState extends State<MainHomeScreen> {
//   // 0 - 내가 요청하는
//   // 1 - 내가 요청받는
//   // 2 - 내가 약속받는
//   // 3 - 내가 약속해준
//   int curRequestId = 2;

//   // + 버튼
//   late GlobalKey addKey;

//   // 발행받은
//   late GlobalKey issuedKey;

//   // 발행해준
//   late GlobalKey requestedKey;

//   // 쿠폰 카드
//   late GlobalKey kkakkaKey;

//   @override
//   void initState() {
//     super.initState();

//     addKey = GlobalKey();
//     issuedKey = GlobalKey();
//     requestedKey = GlobalKey();
//     kkakkaKey = GlobalKey();

//     // TODO set correct conditions in production
//     // if (false) {
//     // Future.delayed(
//     //   Duration(milliseconds: 500),
//     //   () async {
//     //     runCoachMarks();
//     //   },
//     // );
//     // }
//     Future.microtask(() async {
//       Get.find<KkakkaController>().listPromiseMe();
//     });
//   }

//   runCoachMarks() async {
//     await Navigator.of(context).push(
//       CoachMarkModal(
//         widget: renderCoachMarkOne(),
//       ),
//     );

//     await Navigator.of(context).push(
//       CoachMarkModal(
//         widget: renderCoachMarkTwo(),
//       ),
//     );

//     await Navigator.of(context).push(
//       CoachMarkModal(
//         widget: renderCoachMarkThree(),
//       ),
//     );

//     await Navigator.of(context).push(
//       CoachMarkModal(
//         widget: renderCoachMarkFour(),
//       ),
//     );
//   }

//   Widget renderCoachMarkOne() {
//     final theme = ThemeFactory.of(context).theme;

//     final RenderBox box = addKey.currentContext!.findRenderObject() as RenderBox;
//     final Offset position = box.localToGlobal(Offset.zero);

//     double y = position.dy;

//     final whiteStyle = TextStyle(
//       fontWeight: FontWeight.bold,
//       fontSize: 14.0,
//       color: Colors.white,
//     );

//     final orangeStyle = TextStyle(
//       color: theme.primaryOrangeColor,
//     );

//     return Positioned.fill(
//       top: y,
//       child: Padding(
//         padding: EdgeInsets.symmetric(horizontal: 16.0),
//         child: Column(
//           children: [
//             IntrinsicHeight(
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.end,
//                 crossAxisAlignment: CrossAxisAlignment.stretch,
//                 children: [
//                   IntrinsicWidth(
//                     child: Padding(
//                       padding: EdgeInsets.only(
//                         right: 8.0,
//                       ),
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.stretch,
//                         children: [
//                           RichText(
//                             text: TextSpan(
//                               text: "'+'버튼",
//                               style: orangeStyle,
//                               children: [
//                                 TextSpan(
//                                   text: '을 통해 가족에게',
//                                   style: whiteStyle,
//                                 ),
//                               ],
//                             ),
//                             textAlign: TextAlign.end,
//                           ),
//                           RichText(
//                             text: TextSpan(
//                               text: "용돈을 ",
//                               style: whiteStyle,
//                               children: [
//                                 TextSpan(
//                                   text: '요청',
//                                   style: orangeStyle,
//                                 ),
//                                 TextSpan(
//                                   text: '하거나 ',
//                                   style: whiteStyle,
//                                 ),
//                                 TextSpan(text: '발행', style: orangeStyle),
//                                 TextSpan(
//                                   text: '해보세요',
//                                   style: whiteStyle,
//                                 ),
//                               ],
//                             ),
//                             textAlign: TextAlign.end,
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//                   SvgPicture.asset(
//                     'assets/svgs/ic/ic_plus_main.svg',
//                   ),
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   renderCoachMarkTwo() {
//     final RenderBox box = this.issuedKey.currentContext!.findRenderObject() as RenderBox;
//     final pos = box.localToGlobal(Offset.zero);

//     return renderCoachMarkHands(
//       // 손가락 길이의 반 빼기
//       dx: pos.dx + box.size.width / 2 - 32,
//       dy: pos.dy,
//       keyText: '발행받은 것',
//       ltr: true,
//     );
//   }

//   renderCoachMarkThree() {
//     final box = this.requestedKey.currentContext!.findRenderObject() as RenderBox;
//     final pos = box.localToGlobal(Offset.zero);

//     return renderCoachMarkHands(
//       // 손가락 길이의 반 빼기
//       dx: (MediaQuery.of(context).size.width - pos.dx - box.size.width) + (box.size.width / 2) - 32,
//       dy: pos.dy,
//       keyText: '발행해준 것',
//       ltr: false,
//     );
//   }

//   renderCoachMarkFour() {
//     final theme = ThemeFactory.of(context).theme;

//     final box = this.kkakkaKey.currentContext!.findRenderObject() as RenderBox;
//     final pos = box.localToGlobal(Offset.zero);

//     final whiteStyle = TextStyle(
//       fontSize: 14.0,
//       fontWeight: FontWeight.bold,
//       color: Colors.white,
//     );

//     final primaryStyle = whiteStyle.copyWith(
//       color: theme.primaryOrangeColor,
//     );

//     return Positioned.fill(
//       child: Padding(
//         padding: EdgeInsets.only(
//           bottom: MediaQuery.of(context).size.height - pos.dy - box.size.height,
//         ),
//         child: Container(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.end,
//             children: [
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   RichText(
//                     text: TextSpan(
//                       text: '발행받거나, 발행해준 것',
//                       style: primaryStyle,
//                       children: [
//                         TextSpan(
//                           text: '에 대해',
//                           style: whiteStyle,
//                         ),
//                       ],
//                     ),
//                   ),
//                 ],
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   Text(
//                     '확인해보세요 :)',
//                     style: whiteStyle,
//                     textAlign: TextAlign.center,
//                   ),
//                 ],
//               ),
//               Container(height: 16.0),
//               Padding(
//                 padding: EdgeInsets.symmetric(
//                   horizontal: 16.0,
//                 ),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     Expanded(
//                       child: Container(
//                         color: Colors.white,
//                         child: GetBuilder<KkakkaController>(
//                           builder: (c) => c.receivedKkakkas.length > 0
//                               ? HomeKkakkaCard(
//                                   kkakka: c.receivedKkakkas[0],
//                                   type: this.curRequestId == 2 ? HomeKkakkaCardType.received : HomeKkakkaCardType.issued,
//                                 )
//                               : Container(),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   renderCoachMarkHands({
//     required double dx,
//     required double dy,
//     required String keyText,
//     // 패딩 왼쪽부터 오른쪽으로 넣을지
//     bool? ltr,
//   }) {
//     final theme = ThemeFactory.of(context).theme;

//     ltr ??= true;

//     final primaryStyle = TextStyle(
//       fontSize: 14.0,
//       fontWeight: FontWeight.bold,
//       color: theme.primaryOrangeColor,
//     );

//     final whiteStyle = primaryStyle.copyWith(
//       color: Colors.white,
//     );

//     String icon = 'assets/svgs/ic/';
//     MainAxisAlignment alignment;

//     if (!ltr) {
//       icon += 'ic_coachmark_hand_left.svg';
//       alignment = MainAxisAlignment.end;
//     } else {
//       icon += 'ic_coachmark_hand_right.svg';
//       alignment = MainAxisAlignment.start;
//     }

//     return Positioned.fill(
//       top: dy,
//       child: Padding(
//         padding: EdgeInsets.only(
//           left: ltr ? dx : 0,
//           right: ltr ? 0 : dx,
//         ),
//         child: Container(
//           child: Column(
//             children: [
//               Row(
//                 mainAxisAlignment: alignment,
//                 children: [
//                   SvgPicture.asset(
//                     icon,
//                   ),
//                 ],
//               ),
//               Container(height: 16.0),
//               Row(
//                 mainAxisAlignment: alignment,
//                 children: [
//                   RichText(
//                     textAlign: ltr ? TextAlign.left : TextAlign.right,
//                     text: TextSpan(
//                       text: keyText,
//                       style: primaryStyle,
//                       children: [
//                         TextSpan(
//                           text: '을 확인해보세요 :)',
//                           style: whiteStyle,
//                         ),
//                       ],
//                     ),
//                   ),
//                 ],
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   renderAppBar() {
//     final theme = ThemeFactory.of(context).theme;

//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//       children: [
//         //TODO 제플린에서 엑스포트 되면 교체
//         GestureDetector(
//           onDoubleTap: () {
//             //TODO 프로덕션에서 지우세요!
//             Get.toNamed('/test-splash');
//             // final isProd = bool.fromEnvironment('dart.vm.product');
//             //
//             // if (!isProd) {
//             //   Get.to(TestSplashScreen());
//             // }
//           },
//           child: SvgPicture.asset(
//             'assets/svgs/img/logo_splash.svg',
//             color: theme.primaryColor,
//             width: 110.0,
//           ),
//         ),
//         InkWell(
//           onTap: () {
//             Get.toNamed('/payment/qr');
//           },
//           child: SvgPicture.asset(
//             'assets/svgs/ic/ic_qr_main.svg',
//           ),
//         ),
//       ],
//     );
//   }

//   renderWelcome() {
//     final theme = ThemeFactory.of(context).theme;

//     return Column(
//       children: [
//         Row(
//           children: [
//             Text(
//               '안녕하세요 😎',
//               style: TextStyle(
//                 fontSize: theme.fontSize20,
//               ),
//             ),
//           ],
//         ),
//         Padding(
//           padding: EdgeInsets.only(
//             top: 7.0,
//           ),
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               Row(
//                 children: [
//                   Padding(
//                     padding: EdgeInsets.only(
//                       right: 8.0,
//                     ),
//                     child: Text(
//                       '홍길동님',
//                       style: TextStyle(
//                         fontSize: theme.fontSize20,
//                         fontWeight: theme.heavyFontWeight,
//                       ),
//                     ),
//                   ),
//                   SvgPicture.asset(
//                     'assets/svgs/ic/ic_arrow_main.svg',
//                   ),
//                 ],
//               ),
//               renderOptionButtons(),
//             ],
//           ),
//         ),
//       ],
//     );
//   }

//   renderOptionButtons() {
//     return GestureDetector(
//       onTap: () {
//         final box = addKey.currentContext!.findRenderObject() as RenderBox;
//         final Offset position = box.localToGlobal(Offset.zero);

//         double y = position.dy;
//         double x = position.dx;

//         Navigator.of(context).push(
//           HomeOptionModal(
//             x: x,
//             y: y,
//           ),
//         );
//       },
//       child: SvgPicture.asset(
//         'assets/svgs/ic/ic_plus_main.svg',
//         key: addKey,
//       ),
//     );
//   }

//   renderRequestButtons() {
//     final rootPath = 'assets/svgs/ic/';

//     return Column(
//       children: [
//         // Row(
//         //   children: [
//         //     Expanded(
//         //       child: HomeRequestCard(
//         //         title: '내가 요청하는',
//         //         subTitle: '까까',
//         //         iconPath: rootPath + 'ic_request_01_main.svg',
//         //         isPrimaryColor: curRequestId == 0,
//         //         onPlusTap: () {},
//         //         onTap: () {
//         //           setState(() {
//         //             curRequestId = 0;
//         //           });
//         //         },
//         //       ),
//         //     ),
//         //     Container(
//         //       width: 8.0,
//         //     ),
//         //     Expanded(
//         //       child: HomeRequestCard(
//         //         title: '내가 요청받는',
//         //         subTitle: '까까',
//         //         iconPath: rootPath + 'ic_request_02_main.svg',
//         //         isPrimaryColor: curRequestId == 1,
//         //         onPlusTap: () {},
//         //         onTap: () {
//         //           setState(() {
//         //             curRequestId = 1;
//         //           });
//         //         },
//         //       ),
//         //     ),
//         //   ],
//         // ),
//         Container(
//           height: 8.0,
//         ),
//         Row(
//           children: [
//             Expanded(
//               key: this.issuedKey,
//               child: HomeRequestCard(
//                 title: '내가 약속받은',
//                 subTitle: '까까',
//                 iconPath: rootPath + 'ic_promise_01_main.svg',
//                 isPrimaryColor: curRequestId == 2,
//                 onTap: () {
//                   setState(() {
//                     curRequestId = 2;
//                   });

//                   Get.find<KkakkaController>().listPromiseMe();
//                 },
//               ),
//             ),
//             Container(
//               width: 8.0,
//             ),
//             Expanded(
//               key: this.requestedKey,
//               child: HomeRequestCard(
//                 title: '내가 약속해준',
//                 subTitle: '까까',
//                 iconPath: rootPath + 'ic_promise_02_main.svg',
//                 isPrimaryColor: curRequestId == 3,
//                 onTap: () {
//                   setState(() {
//                     curRequestId = 3;
//                   });

//                   Get.find<KkakkaController>().listKkakkas();
//                 },
//               ),
//             ),
//           ],
//         ),
//       ],
//     );
//   }

//   renderDivider() {
//     return Padding(
//       padding: EdgeInsets.only(
//         top: 19.0,
//         bottom: 14.0,
//       ),
//       child: Divider(
//         color: Color(0xFFD9DBDF),
//       ),
//     );
//   }

//   renderSecondHalfHeader() {
//     final theme = ThemeFactory.of(context).theme;

//     final Color fontColor = Color(0xFF555555);

//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//       children: [
//         Row(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
//             Text(
//               '총',
//               style: TextStyle(
//                 color: fontColor,
//                 fontSize: theme.fontSize11,
//               ),
//             ),
//             Container(
//               width: 4.0,
//             ),
//             GetBuilder<KkakkaController>(
//               builder: (c) {
//                 int kkakkaLength = 0;

//                 if (this.curRequestId == 2) {
//                   kkakkaLength = c.issuedKkakkas.length;
//                 } else {
//                   kkakkaLength = c.receivedKkakkas.length;
//                 }

//                 return Text(
//                   kkakkaLength.toString(),
//                   style: TextStyle(
//                     color: fontColor,
//                     fontSize: theme.fontSize14,
//                     fontWeight: theme.heavyFontWeight,
//                   ),
//                 );
//               },
//             ),
//             Container(
//               width: 4.0,
//             ),
//             Text(
//               '개',
//               style: TextStyle(
//                 color: fontColor,
//                 fontSize: theme.fontSize11,
//               ),
//             ),
//           ],
//         ),
//         InkWell(
//           onTap: () {
//             if (curRequestId == 2) {
//               Get.toNamed('/main-home/request-received');
//             } else if (curRequestId == 3) {
//               Get.toNamed('/main-home/request-give');
//             } else {
//               Get.toNamed('/main-home/request/i-request');
//             }
//           },
//           child: Row(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               Text(
//                 '더보기',
//                 style: TextStyle(
//                   fontSize: theme.fontSize11,
//                   color: Color(0xFF555555),
//                 ),
//               ),
//               SvgPicture.asset(
//                 'assets/svgs/ic/ic_arrow_right.svg',
//               ),
//             ],
//           ),
//         ),
//       ],
//     );
//   }

//   renderKkakkaCards() {
//     return GetBuilder<KkakkaController>(
//       builder: (c) {
//         final kkakkas = this.curRequestId == 2 ? c.issuedKkakkas : c.receivedKkakkas;

//         return ListView.separated(
//           itemCount: kkakkas.length,
//           physics: ClampingScrollPhysics(),
//           shrinkWrap: true,
//           separatorBuilder: (_, index) {
//             return Container(height: 16);
//           },
//           itemBuilder: (_, index) {
//             return HomeKkakkaCard(
//               kkakka: kkakkas[index],
//               type: this.curRequestId == 2 ? HomeKkakkaCardType.received : HomeKkakkaCardType.issued,
//             );
//           },
//         );
//       },
//     );
//     // children: [
//     //   Row(
//     //     children: [
//     //       Expanded(
//     //         key: kkakkaKey,
//     //         child: ,
//     //       ),
//     //     ],
//     //   ),
//     //   Container(
//     //     height: 10.0,
//     //   ),
//     //   Row(
//     //     children: [
//     //       Expanded(
//     //         child: HomeKkakkaCard(
//     //           heroTag: HomeKkakkaCardTypeEnum.money,
//     //           type: HomeKkakkaCardTypeEnum.money,
//     //         ),
//     //       ),
//     //     ],
//     //   ),
//     // ],
//   }

//   @override
//   Widget build(BuildContext context) {
//     return DefaultLayout(
//       body: Padding(
//         padding: EdgeInsets.symmetric(
//           horizontal: 16.0,
//         ),
//         child: SingleChildScrollView(
//           child: Column(
//             children: [
//               renderAppBar(),
//               Container(height: 59.0),
//               renderWelcome(),
//               Container(height: 51.0),
//               renderRequestButtons(),
//               renderDivider(),
//               renderSecondHalfHeader(),
//               Container(height: 16.0),
//               renderKkakkaCards(),
//               Container(height: 16.0),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
