import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/components/drop_down/outline_drop_down.dart';
import 'package:kins_healthcare/layouts/title_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HF_0001
 * 주요기능 : 회원님을 초대한 사람들
 */
class RequestScreen extends StatefulWidget {
  @override
  _RequestScreenState createState() => _RequestScreenState();
}

class _RequestScreenState extends State<RequestScreen> {
  renderAvatar({
    bool? isPrimary,
  }) {
    isPrimary ??= false;

    final theme = ThemeFactory.of(context).theme;

    return Column(
      children: [
        Text(
          '어마마마',
          style: TextStyle(
            fontSize: theme.fontSize15,
            color: isPrimary ? theme.primaryColor : Colors.grey,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
        Container(height: 6),
        Container(
          width: 95,
          height: 95,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: isPrimary ? theme.primaryColor : Color(0xFFE5E5E5),
          ),
          child: Padding(
            padding: EdgeInsets.all(2.0),
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Color(0xFFE5E5E5),
              ),
              child: SvgPicture.asset(
                'assets/svgs/ic/ic_logo_main.svg',
                fit: BoxFit.none,
                width: 35,
                height: 24,
              ),
            ),
          ),
        ),
        Container(height: 13),
        SizedBox(
          width: 120,
          child: OutlineDropDown(
            hint: '관계',
            onChanged: (val) {},
            items: [
              DropdownMenuItem(
                child: Text('엄마'),
              ),
            ],
            validator: (val) {
              return null;
            },
            onSaved: (val) {},
          ),
        ),
      ],
    );
  }

  renderAvatars() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        renderAvatar(
          isPrimary: true,
        ),
        Container(width: 70),
        renderAvatar(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return TitleLayout(
      title: '관계 재요청',
      body: Column(
        children: [
          renderAvatars(),
        ],
      ),
    );
  }
}
