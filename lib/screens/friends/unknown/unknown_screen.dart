import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/card/friend_with_button_card.dart';
import 'package:kins_healthcare/layouts/title_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HF_0001-2
 * 주요기능 : 회원님을 초대한 모르는 사람들
 */
class UnknownScreen extends StatefulWidget {
  @override
  _UnknownScreenState createState() => _UnknownScreenState();
}

class _UnknownScreenState extends State<UnknownScreen> {
  renderList() {
    return Expanded(
      child: ListView.separated(
        itemCount: 100,
        separatorBuilder: (_, idx) {
          return Divider();
        },
        itemBuilder: (_, idx) {
          return FriendCardWithButtons(
            meName: '나',
            meRelationship: '아들',
            oppName: '홍길동',
            oppRelationship: '아빠',
            phoneNumber: '010-1234-5678',
            avatarImageUrl: null,
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return TitleLayout(
      title: '회원님을 초대한 사람들',
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  '12명',
                  style: TextStyle(
                    fontSize: theme.fontSize13,
                    fontWeight: theme.heavyFontWeight,
                  ),
                ),
              ],
            ),
            renderList(),
          ],
        ),
      ),
    );
  }
}
