import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/button/friend_button.dart';
import 'package:kins_healthcare/components/card/friend_card.dart';
import 'package:kins_healthcare/components/card/friend_with_button_card.dart';
import 'package:kins_healthcare/layouts/title_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : ???
 * 주요기능 : 친구 스크린 (새로운 기획)
 */
class FriendsScreen extends StatefulWidget {
  @override
  _FriendsScreenState createState() => _FriendsScreenState();
}

class _FriendsScreenState extends State<FriendsScreen> {
  renderAction() {
    return Container(
      width: 30,
      child: PopupMenuButton(
        onSelected: (String val) {
          Get.toNamed(val);
        },
        itemBuilder: (_) {
          return [
            PopupMenuItem(
              value: '/friends/unknown',
              child: Text('연락처 외 요청'),
            ),
            PopupMenuItem(
              value: '/friends/blocked',
              child: Text('모른척한 사람들'),
            ),
          ];
        },
      ),
    );
  }

  renderTop() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                '등록 요청한 가족 2명',
                style: TextStyle(
                  fontWeight: theme.heavyFontWeight,
                  fontSize: theme.fontSize13,
                ),
              ),
            ],
          ),
          FriendCardWithButtons(
            meName: '나',
            meRelationship: '아들',
            oppName: '홍길동',
            oppRelationship: '아빠',
            phoneNumber: '010-1234-5678',
            avatarImageUrl: null,
          ),
          Divider(color: Color(0xFFEAEAEA)),
          FriendCardWithButtons(
            meName: '나',
            meRelationship: '아들',
            oppName: '홍길동',
            oppRelationship: '아빠',
            isOnContacts: true,
            phoneNumber: '010-1234-5678',
            avatarImageUrl: null,
          ),
        ],
      ),
    );
  }

  renderBottom() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 16.0,
        vertical: 16.0,
      ),
      child: Container(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '등록된 가족 3명',
                  style: TextStyle(
                    fontWeight: theme.heavyFontWeight,
                    fontSize: theme.fontSize13,
                  ),
                ),
                FriendButton(
                  variant: FriendButtonVariant.blue,
                  onTap: () {},
                  text: '가족 추가하기',
                ),
              ],
            ),
            FriendCard(),
            Divider(color: Color(0xFFEAEAEA)),
            FriendCard(
              isOnContacts: true,
            ),
          ],
        ),
      ),
    );
  }

  renderDivider() {
    return Container(
      height: 8.0,
      color: Color(0xFFEAEAEA),
    );
  }

  renderBottomButton() {
    return Container(
      height: 55,
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: PrimaryButton(
        label: '다음',
        onTap: () {
          Get.toNamed('/main-home');
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return TitleLayout(
      title: '나의 가족',
      customActionWidget: renderAction(),
      body: ListView(
        physics: ClampingScrollPhysics(),
        children: [
          renderTop(),
          renderDivider(),
          renderBottom(),
          renderBottomButton(),
        ],
      ),
    );
  }
}
