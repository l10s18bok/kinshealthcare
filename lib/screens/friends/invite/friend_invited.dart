import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/invite_button.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/relation_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/services/relation/relation_require_opponent_model.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

/*
 작성일 : 2021-02-17
 작성자 : Victor
 화면명 : HF_0002
 클래스 : FriendInvited
 경로 : /friends/invite
 설명 : 회원님을 초대한 사람들
*/

class FriendInvited extends StatefulWidget {
  @override
  _FriendInvitedState createState() => _FriendInvitedState();
}

class _FriendInvitedState extends State<FriendInvited> {
  final padding = EdgeInsets.fromLTRB(20, 0, 20, 0);
  final controllerTag = 'FriendInvitedState';

  List<Widget> items = [];

  List<RelationReqOpponentResponse> listRelation = [];

  @override
  void initState() {
    super.initState();

    Future.microtask(() async {
      final resp = await Get.find<RelationController>().listRelationReqOpponents();
      listRelation = resp.map((e) => e).toList();

      setState(() {
        this.listRelation = listRelation;

        for (var item in listRelation.map((e) => wrappedInviteButton(e))) {
          items.add(item);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO : 부모 레이아웃 변경해야 함
    return CustomScrollLayout(
      topBar: topBar(),
      body: body(),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget paddingWrap(Widget childWidget, {Key? key}) {
    return Padding(
      key: key,
      padding: padding,
      child: childWidget,
    );
  }

  Widget topBar() {
    return paddingWrap(
      BackTopBar(
        title: '받은 가족관계 요청',
      ),
    );
  }

  Widget space(double size) {
    return Container(
      width: size,
      height: size,
      color: Colors.transparent,
    );
  }

  Widget wrappedInviteButton(RelationReqOpponentResponse model) {
    var parentKey = Key('InviteButton${DateTime.now().millisecond}');

    return paddingWrap(
        InviteButton(
          invitorId: model.userNo!,
          imagePath: model.img!,
          invitorRelation: model.userRelCdOpponentValue!,
          invitorName: model.userNameMe!,
          inviteeRelation: '나',
          inviteeName: '나',
          phoneNumber: TextUtils.addHyponToPhoneNumber(model.userPhoneMe!),
          knowsInvitor: (model.userStatusMe == 'ACTIVE'),
          onDelete: () {
            deleteWidget(parentKey);
          },
        ),
        key: parentKey);
  }

  void deleteWidget(Key key) {
    var widget = items.firstWhere((Widget w) => (w.key == key));

    setState(() {
      items.remove(widget);
    });
  }

  Widget body() {
    final peopleCount = '${listRelation.length}';

    return Column(
      children: [
        space(30),
        paddingWrap(
          Row(
            children: [
              Container(
                child: Text(
                  peopleCount,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                child: Text(' 명'),
              ),
            ],
          ),
        ),
        ...items,
        space(30),
        paddingWrap(
          PrimaryButton(
            label: '다음',
            onTap: () {
              // TODO : 액션 추가
            },
          ),
        ),
      ],
    );
  }
}
