import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/payment_cancel_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/card/new_approval_status_card.dart';
import 'package:kins_healthcare/components/card/payment_status_card.dart';
import 'package:kins_healthcare/components/card/request_approval_status_card.dart';
import 'package:kins_healthcare/components/text_field/payment_form_field_dep.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/services/payment/model/payment_payment_request_status.dart';
import 'package:kins_healthcare/services/relation/model/post_relation_search_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

class WaitScreenArguments {
  final int requiredPayment;

  /// 이미 승인받은 결제
  /// key - id
  /// value - amount
  final Map amounts;

  /// 승이 요청할 결제
  /// key - SearchRelResponse
  /// value - amount
  final Map<SearchRelResponse, int> requiredAmounts;

  WaitScreenArguments({
    required this.requiredPayment,
    required this.amounts,
    required this.requiredAmounts,
  });
}

/*
 * 작성일 : 2021-01-24
 * 작성자 : JC
 * 화면명 : HP_1012
 * 주요기능 : 결제승인 대기
 */
class WaitScreen extends StatefulWidget {
  @override
  _WaitScreenState createState() => _WaitScreenState();
}

class _WaitScreenState extends State<WaitScreen> {
  // late int requiredPayment;
  // late Map amounts;
  // late Map<SearchRelResponse, int> requiredAmounts;

  int tabIdx = 0;
  bool isApproved = false;

  late Timer timer;

  @override
  initState() {
    super.initState();

    Future.microtask(() {
      checkStatus(timer);
    });

    timer = Timer.periodic(
      Duration(
        seconds: 5,
      ),
      checkStatus,
    );

    // final args = Get.arguments;
    //
    // if (args != null && args is WaitScreenArguments) {
    //   this.requiredPayment = args.requiredPayment;
    //   this.amounts = args.amounts;
    //   this.requiredAmounts = args.requiredAmounts;
    // }
  }

  forceApprove() {
    setState(() {
      this.isApproved = true;
    });
  }

  onAllPaymentsApproved() {
    /// Push route
  }

  onPaymentRejected() {
    Get.offNamed('/payment/fail');
  }

  checkStatus(Timer timer) async {
    final c = Get.find<PaymentController>();
    final resp = await c.checkPaymentStatus();

    bool allApproved = true;

    for (var item in resp) {
      if (item.isRejected) {
        timer.cancel();
        onPaymentRejected();
        return;
      }

      if (!item.isApproved) {
        allApproved = false;
      }
    }

    if (allApproved) {
      if (c.paymentStep == 0) {
        setState(() {
          this.isApproved = true;
        });
      } else {
        Get.toNamed('/payment/done');
      }

      timer.cancel();

      // onAllPaymentsApproved();
    }
  }

  @override
  dispose() {
    timer.cancel();
    super.dispose();
  }

  renderApprovalStatuses() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      color: Color(0xFFF3F4F6),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 30.0),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  '승인상태',
                  style: TextStyle(
                    fontSize: theme.fontSize13,
                    color: Color(0xFF2E2E2E),
                    fontWeight: theme.heavyFontWeight,
                  ),
                ),
              ],
            ),
            Container(height: 24.0),
            // RequestApprovalStatusCard(
            //   label: '한국 고용협회',
            //   amount: 8000,
            // ),
            // Container(height: 18.0),
            // RequestApprovalStatusCard(
            //   label: '이쁘니 하트',
            //   subLabel: '홍길동 (누나)',
            //   amount: 50000,
            // ),
            Container(height: 18.0),
            // RequestApprovalStatusCard(
            //   label: '룰루랄라 뽀로로같은 인생',
            //   subLabel: '홍길동 (형)',
            //   amount: 8000,
            // ),
            Container(height: 18.0),
            PaymentFormFieldDep(
              onSaved: (String? val) {},
              validator: (String? val) {
                return null;
              },
            ),
          ],
        ),
      ),
    );
  }

  renderPendingCard() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xFFFFF5D3),
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 14.0),
          child: Row(
            children: [
              SvgPicture.asset(
                'assets/svgs/ic/ic_bell.svg',
              ),
              Padding(
                padding: EdgeInsets.only(left: 12.0),
                child: Text(
                  '추가 결제요청에 대한 응답을 기다리고 있어요!',
                  style: TextStyle(
                    fontSize: theme.fontSize13,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  renderFooterRow({
    required String label,
    required int amount,
    bool? isPrimary,
  }) {
    isPrimary ??= false;

    final theme = ThemeFactory.of(context).theme;

    final labelStyle = TextStyle(
      color: Color(0xFF343434),
      fontSize: theme.fontSize15,
      fontWeight: theme.primaryFontWeight,
    );

    final amountStyle = TextStyle(
      color: Color(0xFF8C8C8C),
      fontSize: theme.fontSize20,
      fontWeight: theme.heavyFontWeight,
    );

    final wonStyle = amountStyle.copyWith(
      fontSize: theme.fontSize16,
      fontWeight: theme.primaryFontWeight,
    );

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          label,
          style: labelStyle,
        ),
        Row(
          children: [
            Text(
              amount.toString(),
              style: isPrimary
                  ? amountStyle.copyWith(
                      color: theme.primaryOrangeColor,
                    )
                  : amountStyle,
            ),
            Container(width: 4.0),
            Text(
              '원',
              style: isPrimary
                  ? wonStyle.copyWith(
                      color: theme.primaryOrangeColor,
                    )
                  : wonStyle,
            )
          ],
        ),
      ],
    );
  }

  // renderFooter() {
  //   return Padding(
  //     padding: EdgeInsets.symmetric(horizontal: 16.0),
  //     child: Column(
  //       children: [
  //         Container(height: 28.0),
  //         renderFooterRow(
  //           label: '총 결제한 금액',
  //           amount: 2400,
  //         ),
  //         Container(height: 15.0),
  //         renderFooterRow(
  //           label: '승인 받은 금액',
  //           amount: 2400,
  //           isPrimary: true,
  //         ),
  //         Container(height: 32.0),
  //         Row(
  //           children: [
  //             Expanded(
  //               child: PrimaryButton(
  //                 label: '취소',
  //                 onTap: () {
  //                   Get.bottomSheet(
  //                     CancelPaymentBottomSheet(),
  //                   );
  //                 },
  //                 bgColor: Color(0xFFE0E2E6),
  //                 txtColor: Color(0xFF868686),
  //               ),
  //             ),
  //             Container(width: 12.0),
  //             Expanded(
  //               child: PrimaryButton(
  //                 label: '결제',
  //                 onTap: () {
  //                   Get.toNamed('/hp-1011');
  //                 },
  //               ),
  //             ),
  //           ],
  //         ),
  //       ],
  //     ),
  //   );
  // }

  renderIcon() {
    return GestureDetector(
      onTap: () {
        /// TODO Production 에서 삭제
        final isProd = bool.fromEnvironment('dart.vm.product');

        if (!isProd) {
          setState(() {
            isApproved = true;
          });

          timer.cancel();
        }
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            'assets/svgs/ic/ic_bannerimg_02_payment.svg',
          ),
        ],
      ),
    );
  }

  renderKeyValue({
    required String key,
    required String value,
    bool isOrange = false,
    Color? color,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            key,
            style: TextStyle(
              color: Colors.grey,
              fontSize: theme.fontSize15,
            ),
          ),
          Text(
            value,
            style: TextStyle(
              fontWeight: theme.heavyFontWeight,
              fontSize: theme.fontSize20,
              color: color == null
                  ? isOrange
                      ? theme.primaryOrangeColor
                      : Colors.grey
                  : color,
            ),
          ),
        ],
      ),
    );
  }

  renderTop() {
    return GetBuilder<PaymentController>(builder: (c) {
      return Column(
        children: [
          PaymentStatusCard(
            isFail: false,
            text: '추가 결제요청에 대한 응답을 기다리고 있어요!',
          ),
          // renderIcon(),
          // Padding(
          //   padding: EdgeInsets.only(top: 16.0),
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.center,
          //     children: [
          //       Flexible(
          //         child: Padding(
          //           padding: EdgeInsets.symmetric(horizontal: 16.0),
          //           child: Text(
          //             this.isApproved
          //                 ? '결제가 준비되었어요!'
          //                 : '추가 결제요청에 대한 응답을 기다리고 있어요.',
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          Container(height: 36.0),
          if (!this.isApproved)
            renderKeyValue(
              key: '결제할 금액',
              color: Colors.black,
              value: TextUtils().numberToLocalCurrency(
                    amount: c.amount.toDouble(),
                    // amount: requiredPayment.toDouble(),
                  ) +
                  ' 원',
            ),
          if (!this.isApproved) Container(height: 16.0),
          if (!this.isApproved)
            renderKeyValue(
              key: '모자란 금액',
              value: TextUtils().numberToLocalCurrency(
                    amount: c.paymentStatuses.where((x) => !x.isApproved).fold(
                        0,
                        (previousValue, element) =>
                            previousValue + element.amount),
                    // amount: requiredPayment.toDouble(),
                  ) +
                  ' 원',
              isOrange: true,
            ),
          if (!this.isApproved) Container(height: 8.0),
          if (!this.isApproved) Divider(),
        ],
      );
    });
  }

  renderTab() {
    final theme = ThemeFactory.of(context).theme;

    final activeContainerDeco = BoxDecoration(
      border: Border(
        bottom: BorderSide(
          width: 2.0,
          color: theme.primaryColor,
        ),
      ),
    );

    final inactiveContainerDeco = BoxDecoration();

    final activeTextStyle = TextStyle(
      fontWeight: theme.heavyFontWeight,
      fontSize: theme.fontSize15,
      color: theme.primaryColor,
    );

    final inactiveTextStyle = activeTextStyle.copyWith(
      color: Colors.grey,
    );

    return Row(
      children: [
        Expanded(
          child: InkWell(
            onTap: () {
              setState(() {
                this.tabIdx = 0;
              });
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    child: Text(
                      '승인',
                      style: this.tabIdx == 0
                          ? activeTextStyle
                          : inactiveTextStyle,
                    ),
                  ),
                  decoration: this.tabIdx == 0
                      ? activeContainerDeco
                      : inactiveContainerDeco,
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: InkWell(
            onTap: () {
              setState(() {
                this.tabIdx = 1;
              });
            },
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    decoration: this.tabIdx == 1
                        ? activeContainerDeco
                        : inactiveContainerDeco,
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                        '미승인',
                        style: this.tabIdx == 1
                            ? activeTextStyle
                            : inactiveTextStyle,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  renderStatusCard(dynamic item) {
    if (item is PaymentRequestStatusResponseMe) {
      return RequestApprovalStatusCard(model: item);
    } else if (item is PaymentRequestStatusResponseRequest) {
      return RequestApprovalStatusCard(model: item);
    } else {
      throw Exception('알수없는 타입입니다.');
    }
  }

  renderBody() {
    return Expanded(
      child: GetBuilder<PaymentController>(builder: (c) {
        return Container(
          color: Color(0xFFF3F4F6),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Column(
              children: [
                Container(height: 24.0),
                ...c.paymentStatuses
                    .map(
                      (x) => Padding(
                        key: UniqueKey(),
                        padding: EdgeInsets.only(bottom: 16.0),
                        child: NewApprovalStatusCard(
                          model: x,
                        ),
                      ),
                    )
                    .toList(),
              ],
            ),
          ),
        );
      }),
    );
    // if (this.isApproved) {
    //   return Expanded(
    //     child: GetBuilder<PaymentController>(builder: (c) {
    //       return Container(
    //         color: Color(0xFFF3F4F6),
    //         child: Padding(
    //           padding: EdgeInsets.symmetric(horizontal: 16.0),
    //           child: Column(
    //             children: [
    //               Container(height: 24.0),
    //               ...c.paymentStatuses
    //                   .map(
    //                     (x) => Padding(
    //                       padding: EdgeInsets.only(bottom: 16.0),
    //                       child: renderStatusCard(x),
    //                     ),
    //                   )
    //                   .toList(),
    //               // Container(height: 24.0),
    //               // RequestApprovalStatusCard(
    //               //   label: '한국 고용협회',
    //               //   amount: 8000,
    //               // ),
    //             ],
    //           ),
    //         ),
    //       );
    //     }),
    //   );
    // }
    // if (tabIdx == 0) {
    //   return Expanded(
    //     child: GetBuilder<PaymentController>(builder: (c) {
    //       return Container(
    //         color: Color(0xFFF3F4F6),
    //         child: Padding(
    //           padding: EdgeInsets.symmetric(horizontal: 16.0),
    //           child: Column(
    //             children: [
    //               Container(height: 24.0),
    //               ...c.paymentStatuses
    //                   .where((item) => item.isApproved && item.amount > 0)
    //                   .map(
    //                     (x) => Padding(
    //                       padding: EdgeInsets.only(bottom: 16.0),
    //                       child: renderStatusCard(x),
    //                     ),
    //                   )
    //                   .toList(),
    //               // Container(height: 24.0),
    //               // RequestApprovalStatusCard(
    //               //   label: '한국 고용협회',
    //               //   amount: 8000,
    //               // ),
    //             ],
    //           ),
    //         ),
    //       );
    //     }),
    //   );
    // } else {
    //   return Expanded(
    //     child: GetBuilder<PaymentController>(builder: (c) {
    //       return Container(
    //         color: Color(0xFF3F4F6),
    //         child: Padding(
    //           padding: EdgeInsets.symmetric(horizontal: 16.0),
    //           child: Column(
    //             children: [
    //               Container(height: 24.0),
    //               ...c.paymentStatuses
    //                   .where((item) => !item.isApproved && item.amount > 0)
    //                   .map(
    //                     (x) => Padding(
    //                       padding: EdgeInsets.only(bottom: 16.0),
    //                       child: renderStatusCard(x),
    //                     ),
    //                   )
    //                   .toList(),
    //               // Container(height: 24.0),
    //               // RequestApprovalStatusCard(
    //               //   label: '한국 고용협회',
    //               //   amount: 8000,
    //               // ),
    //             ],
    //           ),
    //         ),
    //       );
    //     }),
    //   );
    //   // return Container(
    //   //   color: Colors.grey[300],
    //   //   child: Column(
    //   //     children: [],
    //   //   ),
    //   // );
    // }
  }

  renderExpandedPart() {
    return Expanded(
      child: Column(
        children: [
          renderTop(),
          // if (!this.isApproved) renderTab(),
          renderBody(),
        ],
      ),
    );
  }

  renderFooter() {
    if (this.isApproved) {
      return GetBuilder<PaymentController>(builder: (c) {
        return Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: [
              renderKeyValue(
                key: '결제된 금액',
                color: Colors.black,
                value: TextUtils().numberToLocalCurrency(
                      amount: c.amount.toDouble(),
                    ) +
                    ' 원',
              ),
              renderKeyValue(
                key: '모자란 금액',
                value: '0 원',
                isOrange: true,
              ),
              Container(height: 32.0),
              Row(
                children: [
                  Expanded(
                    child: PrimaryButton(
                      label: '취소',
                      bgColor: Color(0xFFF3F4F6),
                      txtColor: Colors.grey,
                      onTap: () async {
                        await Get.bottomSheet(
                          PaymentCancelBottomSheet(),
                        );
                      },
                    ),
                  ),
                  if (c.paymentStep == 0) Container(width: 16.0),
                  if (c.paymentStep == 0)
                    Expanded(
                      child: PrimaryButton(
                        label: '결제',
                        onTap: () async {
                          try {
                            // await Get.find<PaymentController>().requestPayment();

                            Get.toNamed('/payment/done');
                          } on DioError catch (e) {
                            SnackbarUtils().parseDioErrorMessage(
                              error: e,
                              title: '결제 에러',
                            );
                          }
                        },
                      ),
                    ),
                ],
              ),
            ],
          ),
        );
      });
    } else {
      return Padding(
        padding: EdgeInsets.all(16.0),
        child: PrimaryButton(
          label: '취소',
          bgColor: Color(0xFFF3F4F6),
          txtColor: Colors.grey,
          onTap: () async {
            await Get.bottomSheet(
              PaymentCancelBottomSheet(),
            );
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;

    return WillPopScope(
      onWillPop: () async => false,
      child: CustomScrollLayout(
        topBar: BackTopBar(
          title: '응답 대기중',
          rightButtons: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: () async {
                  await Get.bottomSheet(
                    PaymentCancelBottomSheet(),
                  );
                },
                child: Text(
                  '요청 취소',
                  style: TextStyle(
                    color: Color(
                      0xFFF979797,
                    ),
                    fontSize: theme.fontSize16,
                    fontWeight: theme.heavyFontWeight,
                  ),
                ),
              ),
            ],
          ),
          customOnBack: () async {
            Get.offNamedUntil('/payment', (route) => false);
            // await Get.bottomSheet(
            //   PaymentCancelBottomSheet(),
            // );
          },
        ),
        body: Padding(
          padding: EdgeInsets.only(top: 40.0),
          child: Column(
            children: [
              renderExpandedPart(),
              renderFooter(),
            ],
          ),
        ),
      ),
    );

    // return PaymentIconLayout(
    //   message: '승인대기중인 요청이 있습니다.',
    //   title: '결제승인 대기중',
    //   body: ListView(
    //     children: [
    //       renderApprovalStatuses(),
    //       renderFooter(),
    //     ],
    //   ),
    // );
  }
}
