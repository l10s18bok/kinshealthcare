import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/cancel_payment_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/text/payment_amount_text.dart';
import 'package:kins_healthcare/layouts/payment_icon_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-02-04
 * 작성자 : JC
 * 화면명 : HP_1013
 * 주요기능 : 결제진행
 */
class ProceedScreen extends StatefulWidget {
  @override
  _ProceedScreenState createState() => _ProceedScreenState();
}

class _ProceedScreenState extends State<ProceedScreen> {
  renderStatuses() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      decoration: BoxDecoration(
        color: Color(0xFFF3F4F6),
      ),
      child: Padding(
        padding:
            EdgeInsets.only(top: 27.0, bottom: 40.0, left: 16.0, right: 16.0),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  '승인상태',
                  style: TextStyle(
                    fontSize: theme.fontSize13,
                    fontWeight: theme.heavyFontWeight,
                  ),
                ),
              ],
            ),
            Container(height: 20.0),
            // RequestApprovalStatusCard(
            //   label: '돈잘버는 우리 형',
            //   subLabel: '홍길동 (형)',
            //   amount: 82000,
            // ),
            // Container(height: 20.0),
            // RequestApprovalStatusCard(
            //   label: '이쁘니 하트',
            //   subLabel: '홍길동 (누나)',
            //   amount: 50000,
            // ),
          ],
        ),
      ),
    );
  }

  renderFooter() {
    return Padding(
      padding:
          EdgeInsets.only(top: 45.0, bottom: 16.0, left: 16.0, right: 16.0),
      child: Column(
        children: [
          PaymentAmountText(
            label: '결제된 금액',
            amount: 2400,
          ),
          Container(height: 15.0),
          PaymentAmountText(
            label: '미결제 금액',
            amount: 2400,
            isPrimary: true,
          ),
          Container(height: 32.0),
          Row(
            children: [
              Expanded(
                child: PrimaryButton(
                  label: '취소',
                  onTap: () async {
                    cancelPayment();
                  },
                  theme: PrimaryButtonTheme.grey,
                ),
              ),
              Container(width: 12.0),
              Expanded(
                child: PrimaryButton(
                  label: '결제',
                  onTap: () {
                    Get.toNamed('/payment/done');
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  cancelPayment() async {
    final result = await Get.bottomSheet(CancelPaymentBottomSheet());
    if (result == 'OK') Get.offAllNamed('/main-home');
  }

  @override
  Widget build(BuildContext context) {
    return PaymentIconLayout(
      message: '결제가 준비 되었습니다',
      title: '결제승인 대기중',
      body: Column(
        children: [
          Expanded(
            child: renderStatuses(),
          ),
          renderFooter(),
        ],
      ),
    );
  }
}
