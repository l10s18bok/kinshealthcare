import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/btn_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/card/payment_rate_user_card.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/emergency_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/models/local/local_emergency_model.dart';
import 'package:kins_healthcare/screens/payment/share/share_screen.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/payment_user_model.dart';
import 'package:kins_healthcare/services/emergency/model/post_hosptial_share_set_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 * 작성일 : 2021-03-08
 * 작성자 : Andy
 * 화면명 : HP_5004(응급병원 나누기 설정)
 * 클래스 : EmergencyShareScreen
 * 경로  : /payment/emergency/share
 * 설명 : 까까 발행의 내용작성
 */

enum HospitalSetup { REGISTER, UPDATE }

class EmergencyShareScreen extends StatefulWidget {
  final HospitalSetup hospitalSetup;

  const EmergencyShareScreen({Key? key, this.hospitalSetup = HospitalSetup.REGISTER}) : super(key: key);

  @override
  _EmergencyShareScreenState createState() => _EmergencyShareScreenState();
}

class _EmergencyShareScreenState extends State<EmergencyShareScreen> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey<AnimatedListState>();
  late EmHospitalDetailData _emHospitalDetailData;
  int selectIndex = -1;
  List<FocusNode> focusList = [FocusNode()];
  List<PaymentUserModel> modelList = [PaymentUserModel(name: 'HEADER', phoneNum: 'HEADER', userRelNo: -1)];
  bool _isChange = false;
  @override
  void initState() {
    super.initState();
    _emHospitalDetailData = Get.arguments;
    _switching();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _switching() {
    switch (widget.hospitalSetup) {
      case HospitalSetup.REGISTER:
        modelList = [PaymentUserModel(name: 'HEADER', phoneNum: 'HEADER', userRelNo: -1)];
        break;
      case HospitalSetup.UPDATE:
        _getList();
        break;
    }
  }

  _getPaymentModelList(var hospShareList) {
    for (var item in hospShareList) {
      int index = modelList.length;
      final model = PaymentUserModel(
        name: item.userName,
        nickName: item.userRelCdMeValue,
        userRelNo: item.userRelNo,
        phoneNum: item.userPhone,
        imageUrl: item.photo,
        percentage: item.percentage.toString(),
      );
      modelList.add(model);
      focusList.add(FocusNode());
      _listKey.currentState!.insertItem(index, duration: Duration(milliseconds: 300));
    }
  }

  _getList() async {
    try {
      final resp = await Get.find<EmergencyController>().listHosptialShare(hsptlId: _emHospitalDetailData.hsptlId!);
      _getPaymentModelList(resp);
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
    setState(() {});
  }

  _listItemBtnOnTap(int index) {
    if (index == selectIndex) selectIndex = -1;
    _remove(index);

    setState(() {});
  }

  _percentOnChanged(PaymentUserModel model, String value) {
    if (value.isEmpty) return setState(() {});
    model.percentage = value;

    model.isError = false;

    if (_checkMax() < 0)
      model.isError = true;
    else {
      _isChange = true;
      modelList = modelList.map((item) {
        item.isError = false;
        return item;
      }).toList();
    }

    setState(() {});
  }

  double _checkMax() {
    double nowPercentage = 0.0;
    for (final item in modelList) {
      if (item.percentage == null) continue;
      final percentage = double.parse(item.percentage!);
      nowPercentage += percentage;
    }

    return 100.0 - nowPercentage;
  }

  void _remove(int index) async {
    final keyState = _listKey.currentState;
    if (keyState == null) return;

    final sheet = BtnBottomSheet(
      boldTitle: '[${modelList[index].name}]',
      title: '\n을 가족 목록에서 삭제할까요?',
    );
    final result = await Get.bottomSheet<String>(sheet);

    if (result != 'OK') return;

    keyState.removeItem(
      index,
      (context, animation) {
        final item = _buildRemovedItem(context, index, animation);
        modelList.removeAt(index);
        focusList.removeAt(index);

        return item;
      },
      duration: Duration(milliseconds: 500),
    );
  }

  _addOnTab(var userList) {
    if (userList == null) return;
    var future = Future(() {});
    for (var item in userList) {
      future.then((_) {
        final model = PaymentUserModel(
          name: item.relUserName,
          nickName: item.relName,
          userRelNo: item.relUserNo,
          phoneNum: item.relUserPhone,
          imageUrl: item.relUserImage,
          percentage: '0',
        );
        modelList.insert(1, model);
        focusList.insert(1, FocusNode());
        _listKey.currentState!.insertItem(0, duration: Duration(milliseconds: 300));
      });
    }
  }

  _saveOnTab() async {
    PostHosptialShareSetBody reqBody;
    List<HsptlShareUser> users = [];

    FocusScope.of(context).requestFocus(new FocusNode());

    for (var i = 0; i < modelList.length; i++) {
      if (i == 0) continue;
      final dou = double.parse(modelList[i].percentage!);
      final user = HsptlShareUser(
        percentage: dou.toInt(),
        userRelNo: modelList[i].userRelNo,
      );
      users.add(user);
    }
    reqBody = PostHosptialShareSetBody(hsptlId: _emHospitalDetailData.hsptlId, users: users);

    try {
      if (widget.hospitalSetup == HospitalSetup.REGISTER) {
        final resp = await Get.find<EmergencyController>().hosptialShareRegist(reqBody: reqBody);
        if (resp.resultCode == 'SUCCESS') {
          return resp.resultCode;
        }
      } else if (widget.hospitalSetup == HospitalSetup.UPDATE) {
        final resp = await Get.find<EmergencyController>().hosptialShareUpdate(reqBody: reqBody);
        if (resp.resultCode == 'SUCCESS') {
          return resp.resultCode;
        }
      }
    } on DioError catch (e) {
      final data = e.response!.data;

      if (data is Map) {
        final resultCode = data["resultCode"];
        final resultMsg = data["resultMsg"];

        final sheet = ErrorBottomSheet(title: resultCode, content: resultMsg);
        Get.bottomSheet(sheet);
      } else {
        final msg = data.toString();

        final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
        Get.bottomSheet(sheet);
      }
    }
    return 'Err';
  }

  _showSaveBottomSheet() async {
    if (_checkMax() < 0) {
      final sheet = ErrorBottomSheet(title: 'PROCESS_ERROR', content: '퍼센트의 합이 100이 넘습니다.');
      Get.bottomSheet(sheet);
      return;
    }

    String title = '';
    String lastTitle = '';
    switch (widget.hospitalSetup) {
      case HospitalSetup.REGISTER:
        title = '을\n응급병원으로 등록하시겠습니까?';
        lastTitle = '이\n내 응급병원으로 등록되었습니다.';
        break;
      case HospitalSetup.UPDATE:
        title = '\n수정한 내용을 적용할까요?';
        lastTitle = '\n내용이 수정 되었습니다.';
        break;
    }

    final sheet = BtnBottomSheet(
      boldTitle: '[${_emHospitalDetailData.hsptlName ?? ''}]',
      title: title,
    );
    final selectTitle = await Get.bottomSheet<String>(sheet);

    if (selectTitle != 'OK') return;

    final result = await _saveOnTab();

    if (result != 'SUCCESS') return;

    final sheet02 = BtnBottomSheet(
      boldTitle: '[${_emHospitalDetailData.hsptlName ?? ''}]',
      title: lastTitle,
      isOneBtn: true,
    );
    await Get.bottomSheet<String>(sheet02, isDismissible: false);
    Get.back(result: 'OK');
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: NestedScrollLayout(
        topBar: renderTopBar(),
        body: renderMain(),
      ),
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
    );
  }

  Widget renderTopBar() {
    return BackTopBar(title: '병원 상세정보');
  }

  Widget renderMain() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          Expanded(child: renderAnimatedListView()),
          renderSubmitBtn(),
        ],
      ),
    );
  }

  renderAnimatedListView() {
    return AnimatedList(
      key: _listKey,
      initialItemCount: modelList.length,
      itemBuilder: _listItemBuilder,
    );
  }

  renderListItem(
    int index, {
    required Animation<double> animation,
    bool isRemove = false,
  }) {
    if (index == 0) return renderFirstItem();
    return renderItem(index, animation: animation, isRemove: isRemove);
  }

  Widget renderFirstItem() {
    return Column(
      children: [
        SizedBox(height: 20),
        renderTitle(),
        SizedBox(height: 30),
        modelList.length == 1 ? renderNonCard() : Container(),
      ],
    );
  }

  Widget renderNonCard() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10, left: 16, right: 16, top: 0),
      child: NonKkakkaCard(title: '저장된 가족이 없습니다.\n나눌 가족을 저장해보세요.'),
    );
  }

  Widget renderItem(
    int index, {
    required Animation<double> animation,
    bool isRemove = false,
  }) {
    final model = modelList[index];
    final isSelect = selectIndex == index;

    return InkWell(
      child: PaymentRateUserCard(
        animation: animation,
        model: model,
        isSelect: isSelect,
        onBtnTap: () => isRemove ? null : _listItemBtnOnTap(index),
        focus: focusList[index],
        onFocus: () {
          selectIndex = index;
          setState(() {});
        },
        onChanged: (value) => _percentOnChanged(model, value),
      ),
      onTap: () {
        if (isRemove) return;
        FocusScope.of(context).requestFocus(new FocusNode());
        selectIndex = index;
        setState(() {});
      },
    );
  }

  renderTitle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          '[${_emHospitalDetailData.hsptlName}] 에 \n나누기할 가족을 선택해주세요',
          style: TextStyle(
            color: kinsBlack30,
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize17,
          ),
        ),
        InkWell(
          child: Container(
            padding: EdgeInsets.all(4),
            width: 40,
            height: 40,
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_plus_main.svg',
              width: 32,
              height: 32,
            ),
          ),
          onTap: () async {
            ShareList shareList = ShareList(modelList: modelList, mult: true);
            final result = await Get.toNamed('/payment/share-target', arguments: shareList);

            _addOnTab(result);
            setState(() {});
          },
        )
      ],
    );
  }

  Widget _listItemBuilder(BuildContext context, int index, Animation<double> animation) {
    return renderListItem(index, animation: animation);
  }

  Widget _buildRemovedItem(BuildContext context, int index, Animation<double> animation) {
    return renderListItem(index, animation: animation, isRemove: true);
  }

  Widget renderSubmitBtn() {
    if (modelList.length == 1) return Container();
    String label = '등록';
    if (widget.hospitalSetup == HospitalSetup.UPDATE) label = '수정';

    return Container(
        margin: EdgeInsets.only(bottom: 16),
        height: 55,
        child: PrimaryButton(
          label: label,
          padding: EdgeInsets.all(0.0),
          onTap: _isChange ? _showSaveBottomSheet : null,
        ));
  }
}
