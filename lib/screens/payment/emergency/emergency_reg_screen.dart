import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/btn_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/components/top_bar/simple_hide_top_bar.dart';
import 'package:kins_healthcare/controllers/emergency_controller.dart';
import 'package:kins_healthcare/layouts/layouts.dart';
import 'package:kins_healthcare/models/local/local_emergency_model.dart';
import 'package:kins_healthcare/screens/spinor_test/andy/component/hospital_info_box.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/common/general_body_model.dart';
import 'package:kins_healthcare/services/emergency/model/post_hosptial_detail_model.dart';
import 'package:kins_healthcare/services/emergency/model/post_search_list_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-21
 * 작성자 : Andy
 * 화면명 : HP_5001
 * 클래스 : EmergencyRegistration
 * 경로  : /payment/emergency
 * 설명 : 응급병원관리 -> 응급병원등록
 */

class EmergencyRegScreen extends StatefulWidget {
  EmergencyRegScreen({
    Key? key,
  }) : super(key: key);

  @override
  _EmergencyRegScreenState createState() => _EmergencyRegScreenState();
}

class _EmergencyRegScreenState extends State<EmergencyRegScreen> {
  late ScrollController _scrollController;
  late EmergencyController _emcController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    // _scrollController.addListener(_scrollListener);
    _emcController = Get.find<EmergencyController>();
    _initRefreshHospitalList();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  // _scrollListener() {
  //   print(_scrollController.position.maxScrollExtent);
  //   if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
  //     _initRefreshHospitalList(page: _page++, reset: false);
  //   }
  // }

  _initRefreshHospitalList() {
    final reqBody = GeneralPaginationBody();

    try {
      _emcController.listRegHosptial(body: reqBody, reset: true);
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  _goDetail(var hsptlInfo) {
    final trModel = LocalEmergencyModel();
    trModel.hsptlId = hsptlInfo.hsptlId;
    trModel.regList = _emcController.regHosptialList;
    Get.toNamed('/payment/emergency/detail', arguments: trModel);
  }

  _goSearch() async {
    final trModel = LocalEmergencyModel();
    if (_emcController.regHosptialList.length == 0)
      trModel.regList = [];
    else
      trModel.regList = _emcController.regHosptialList;
    await Get.toNamed('/payment/emergency/search', arguments: trModel);
    //_emcController.resetCache(type: PostSearchListResponse, key: 'EmHome');
    _initRefreshHospitalList();
  }

  _onTabWrite(var hsptlInfo) {
    Get.toNamed('/payment/emergency/share-update',
        arguments: EmHospitalDetailData(
          hsptlName: hsptlInfo.hsptlName,
          hsptlId: hsptlInfo.hsptlId,
        ));
  }

  _onTabDelete({required List<PostSearchListResponse> hsptlList, required int index}) async {
    final sheet = BtnBottomSheet(
      boldTitle: '[${hsptlList[index].hsptlName ?? ''}]',
      title: '\n을 삭제할까요?',
    );
    final selectTitle = await Get.bottomSheet<String>(sheet);

    if (selectTitle != 'OK') return;

    final reqBody = PostOnlyHsptlIdBody(hsptlId: hsptlList[index].hsptlId);
    _emcController.hosptialShareDelete(reqBody: reqBody);
  }

  renderAnimationTopBar() {
    return SimpleHideTopBar(
      title: '응급병원관리',
      scrollController: _scrollController,
    );
  }

  renderListEmpty() {
    final theme = ThemeFactory.of(context).theme;
    return Container(
      height: MediaQuery.of(context).size.height / 1.5,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SvgPicture.asset(
            'assets/svgs/img/ic_hospital_hospital.svg',
            height: 55,
            width: 55,
          ),
          SizedBox(height: 26),
          Text(
            '현재 등록된 병원이 없습니다',
            style: TextStyle(
              color: Color(0xFF444445),
              fontSize: theme.fontSize15,
              fontWeight: theme.heavyFontWeight,
            ),
          ),
        ],
      ),
    );
  }

  Widget renderLoading() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      height: 50,
      child: CircularProgressIndicator(),
    );
  }

  renderRegistButton() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      margin: const EdgeInsets.only(bottom: 16),
      child: PrimaryButton(
        label: '응급병원등록',
        onTap: _goSearch,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Column(
        children: [
          renderAnimationTopBar(),
          GetBuilder<EmergencyController>(
            builder: (c) {
              return Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(16, 30, 16, 0),
                  child: PaginationListView<PostSearchListResponse>(
                    cacheKey: 'EmHome',
                    loadingWidget: renderLoading(),
                    emptyWidget: renderListEmpty(),
                    request: c.listRegHosptial,
                    controller: c,
                    scrollController: _scrollController,
                    lastWidget: Container(),
                    itemCount: c.regHosptialList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return HospitalInfoBox(
                        hospitalInformation: c.regHosptialList[index],
                        onTabBox: () => _goDetail(c.regHosptialList[index]),
                        onTabWrite: () => _onTabWrite(c.regHosptialList[index]),
                        onTabDelete: () => _onTabDelete(hsptlList: c.regHosptialList, index: index),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Divider(height: 10, color: Colors.transparent);
                    },
                  ),
                ),
              );
            },
          ),
          KeyboardVisibilityBuilder(builder: (context, isKeyboardVisible) {
            if (isKeyboardVisible) return Container();
            return renderRegistButton();
          }),
        ],
      ),
    );
  }
}
