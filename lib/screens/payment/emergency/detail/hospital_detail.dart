import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/btn_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/slide_widget/normal_slide_image.dart';
import 'package:kins_healthcare/components/top_bar/simple_hide_top_bar.dart';
import 'package:kins_healthcare/controllers/emergency_controller.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/models/local/local_emergency_model.dart';
import 'package:kins_healthcare/services/emergency/model/post_hosptial_detail_model.dart';
import 'package:kins_healthcare/services/emergency/model/post_search_list_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:url_launcher/url_launcher.dart';

/*
 * 작성일 : 2021-01-22
 * 작성자 : Andy
 * 화면명 : HP_5003
 * 클래스 : HospitalDetail
 * 경로  : /payment/emergency/detail
 * 설명 : 병원 상세정보
 */

class HospitalDetail extends StatefulWidget {
  HospitalDetail({Key? key}) : super(key: key);

  @override
  _HospitalDetailState createState() => _HospitalDetailState();
}

class _HospitalDetailState extends State<HospitalDetail> {
  late ScrollController _scrollController;
  late PostHosptialDetailResponse _hospitalDetailData;
  late LocalEmergencyModel _localEmergencyModel;
  bool _regHsptl = false;

  List<String> _imgList = [];

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _hospitalDetailData = PostHosptialDetailResponse();
    _getDetailHospital();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  _phoneCall({required String phoneNum}) async {
    print('$phoneNum');
    if (await canLaunch(phoneNum)) {
      await launch(phoneNum);
    } else {
      throw 'Could not launch $phoneNum';
    }
    //setState(() {});
  }

  _getDetailHospital() async {
    _localEmergencyModel = Get.arguments;
    print('Get.arguments = ${_localEmergencyModel.hsptlId.toString()}');
    final postRequest = PostOnlyHsptlIdBody(hsptlId: _localEmergencyModel.hsptlId);
    _hospitalDetailData = await Get.find<EmergencyController>().detailHosptial(body: postRequest);

    if (_hospitalDetailData.photo != null) _imgList = _hospitalDetailData.photo!;

    _filterRegHsptl();
    setState(() {});
  }

  _filterRegHsptl() {
    for (var item in _localEmergencyModel.regList!) {
      if (_localEmergencyModel.hsptlId == item.hsptlId) _regHsptl = true;
    }
  }

  _onTabRegBtn() async {
    final argData = EmHospitalDetailData(hsptlId: _localEmergencyModel.hsptlId, hsptlName: _hospitalDetailData.hsptlName);
    final result = await Get.toNamed('/payment/emergency/share-register', arguments: argData);
    if (result == 'OK') {
      final addModel = PostSearchListResponse(hsptlId: argData.hsptlId, hsptlName: argData.hsptlName);
      _localEmergencyModel.regList?.add(addModel);
    }
    Get.back(result: _localEmergencyModel);
  }

  renderAnimationTopBar() {
    return SimpleHideTopBar(
      title: '병원상세정보',
      scrollController: _scrollController,
    );
  }

  renderSlideImage() {
    return Container(
      width: double.infinity,
      height: 250,
      child: NormalSlideImage(imageList: _imgList),
    );
  }

  renderHosptialAddress() {
    final theme = ThemeFactory.of(context).theme;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            _hospitalDetailData.hsptlName ?? '',
            style: TextStyle(
              color: Color(0xFF232323),
              fontSize: theme.fontSize17,
              fontWeight: theme.heavyFontWeight,
            ),
          ),
          SizedBox(height: 16),
          Text(
            _hospitalDetailData.adres1 ?? '',
            style: TextStyle(
              color: Color(0xFF5B5B5B),
              fontSize: theme.fontSize14,
            ),
          ),
          SizedBox(height: 9),
          InkWell(
            onTap: () => _phoneCall(phoneNum: 'tel:${_hospitalDetailData.phone}'),
            child: Row(
              children: [
                SvgPicture.asset(
                  'assets/svgs/ic/ic_phone_hospital.svg',
                ),
                Text(
                  _hospitalDetailData.phone ?? '',
                  style: TextStyle(
                    color: Color(0xFF1D1F75),
                    fontSize: theme.fontSize14,
                    fontWeight: theme.heavyFontWeight,
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Color(0xFFE3E3E3),
            thickness: 1,
            height: 16,
          ),
        ],
      ),
    );
  }

  renderTextDetail() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Text(
        _hospitalDetailData.intrcn ?? '',
      ),
    );
  }

  renderRegistButton() {
    return Container(
      height: 71,
      color: Color(0xFFEEEFF2),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Align(
          alignment: Alignment.topCenter,
          child: SizedBox(
            height: 55,
            child: PrimaryButton(
              label: "응급병원등록하기",
              onTap: _regHsptl ? null : _onTabRegBtn,
            ),
          ),
        ),
      ),
    );
  }

  Future<String?> showBottomSheet() async {
    final sheet = BtnBottomSheet(
      boldTitle: '[${_hospitalDetailData.hsptlName ?? ''}]',
      title: '을\n응급병원으로 등록하시겠습니까?',
    );
    final selectTitle = await Get.bottomSheet<String>(sheet);

    if (selectTitle != 'OK') return selectTitle;

    final sheet02 = BtnBottomSheet(
      boldTitle: '[${_hospitalDetailData.hsptlName ?? ''}]',
      title: '이\n내 응급병원으로 등록되었습니다.',
      isOneBtn: true,
    );
    final finalData = await Get.bottomSheet<String>(sheet02);

    return finalData;
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Column(
        children: [
          renderAnimationTopBar(),
          Expanded(
              child: ListView(
            controller: _scrollController,
            children: [
              SizedBox(height: 26),
              renderSlideImage(),
              SizedBox(height: 26),
              renderHosptialAddress(),
              renderTextDetail(),
            ],
          )),
          renderRegistButton(),
        ],
      ),
    );
  }
}
