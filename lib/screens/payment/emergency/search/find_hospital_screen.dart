import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/components/top_bar/simple_hide_top_bar.dart';
import 'package:kins_healthcare/controllers/emergency_controller.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/models/local/local_emergency_model.dart';
import 'package:kins_healthcare/screens/spinor_test/andy/component/hospital_info_box.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/emergency/model/post_search_list_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-21
 * 작성자 : Andy
 * 화면명 : HP_5002
 * 클래스 : FindHospital
 * 경로  : /payment/emergency/search
 * 설명  : 응급병원관리(응급병원찾기)
 */

class FindHospitalScreen extends StatefulWidget {
  @override
  _FindHospitalScreenState createState() => _FindHospitalScreenState();
}

class _FindHospitalScreenState extends State<FindHospitalScreen> {
  ScrollController _scrollController = ScrollController();
  TextEditingController _editingController = TextEditingController();
  // final _debouncer = Debouncer(milliseconds: 500);
  LocalEmergencyModel _trModel = LocalEmergencyModel();

  @override
  void initState() {
    super.initState();
    _trModel = Get.arguments;
    _searchList();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _editingController.dispose();
    super.dispose();
  }

  _searchList({String text = ''}) {
    final reqBody = PostSearchListBody(
      hsptlName: text,
    );
    try {
      Get.find<EmergencyController>().findHosptialList(body: reqBody, reset: true);
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  // _onTextChange(String text) {
  //   _debouncer.run(() {
  //     setState(() {
  //       _searchList(text: text);
  //     });
  //   });
  // }

  _goDetail(PostSearchListResponse? respModel) async {
    if (respModel == null) return;
    _trModel.hsptlId = respModel.hsptlId;
    _trModel = await Get.toNamed('/payment/emergency/detail', arguments: _trModel);
  }

  renderAnimationTopBar() {
    return SimpleHideTopBar(
      title: '응급병원관리(병원찾기)',
      scrollController: _scrollController,
    );
  }

  renderTextField() {
    final theme = ThemeFactory.of(context).theme;
    final hintColor = Color(0xFFB2ACFF);
    final style = TextStyle(
      fontSize: 15.0,
      color: Colors.black,
    );
    final baseInputDecoration = InputDecoration(
      hintText: "검색",
      hintStyle: TextStyle(
        color: hintColor,
        fontSize: theme.fontSize15,
      ),
      border: InputBorder.none,
      isDense: true,
      contentPadding: const EdgeInsets.symmetric(horizontal: 0, vertical: 0),
      //suffix: renderSuffixIcon(),
    );
    return TextField(
        controller: _editingController,
        textInputAction: TextInputAction.search,
        decoration: baseInputDecoration,
        style: style,
        onSubmitted: (val) {
          FocusScope.of(context).requestFocus(FocusNode());
          _searchList(text: val);
        }
        //onChanged: _onTextChange,
        );
  }

  renderSearchIcon() {
    return Positioned(
      right: -17,
      child: IconButton(
        onPressed: () {
          FocusScope.of(context).requestFocus(FocusNode());
          _searchList(text: _editingController.text);
        },
        icon: Icon(
          Icons.search,
          color: Color(0xFF999999),
        ),
      ),
    );
  }

  renderListEmpty() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      height: MediaQuery.of(context).size.height / 1.5,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SvgPicture.asset(
            'assets/svgs/img/ic_hospital_hospital.svg',
            height: 55,
            width: 55,
          ),
          SizedBox(height: 26),
          Text(
            '현재 등록된 병원이 없습니다',
            style: TextStyle(
              color: Color(0xFF444445),
              fontSize: theme.fontSize15,
              fontWeight: theme.heavyFontWeight,
            ),
          ),
        ],
      ),
    );
  }

  renderFindBox() {
    return Padding(
      padding: const EdgeInsets.only(top: 30, left: 16, right: 16),
      child: Container(
        padding: const EdgeInsets.fromLTRB(24, 10, 24, 10),
        height: 43,
        margin: const EdgeInsets.only(bottom: 22),
        decoration: BoxDecoration(
          border: Border.all(
            color: Color(0xFFE8E8E8),
          ),
          borderRadius: BorderRadius.circular(21.5),
        ),
        child: Stack(
          alignment: Alignment.centerRight,
          children: [
            renderTextField(),
            renderSearchIcon(),
          ],
        ),
      ),
    );
  }

  Widget renderLoading() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      height: 50,
      child: CircularProgressIndicator(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Column(
          children: [
            renderAnimationTopBar(),
            renderFindBox(),
            GetBuilder<EmergencyController>(
              builder: (c) {
                return Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    child: PaginationListView<PostSearchListResponse>(
                      loadingWidget: renderLoading(),
                      emptyWidget: renderListEmpty(),
                      request: c.findHosptialList,
                      controller: c,
                      scrollController: _scrollController,
                      lastWidget: Container(),
                      itemCount: c.postSearchList.length,
                      itemBuilder: (BuildContext context, int index) {
                        //if (index == 0) return renderSearchBox(c.postSearchList[0]);
                        return HospitalInfoBox(
                          hospitalInformation: c.postSearchList[index],
                          onTabBox: () => _goDetail(c.postSearchList[index]),
                        );
                      },
                      separatorBuilder: (context, index) {
                        return Divider(height: 10, color: Colors.transparent);
                      },
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class Debouncer {
  final int milliseconds;
  VoidCallback? action;
  Timer? timer;

  Debouncer({
    required this.milliseconds,
    this.action,
    this.timer,
  });

  run(VoidCallback action) {
    if (timer != null) {
      timer!.cancel();
    }

    timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}
