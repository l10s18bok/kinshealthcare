import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:rxdart/rxdart.dart';

/*
 * 작성일 : 2021-01-18
 * 작성자 : JC
 * 화면명 : HP_1001
 * 주요기능 : QR 결제하기
 */
class QrScreen extends StatefulWidget {
  @override
  _QrScreenState createState() => _QrScreenState();
}

class _QrScreenState extends State<QrScreen> {
  GlobalKey qrKey = GlobalKey();
  Barcode? qrResult;
  late QRViewController qrController;
  String countDown = '03:00';
  Timer? timer;
  late DateTime timerStartTime;
  Duration allowedDuration = Duration(seconds: 180);
  DateTime? lastScan;

  @override
  void initState() {
    super.initState();
    resetTimer();
    Get.find<PaymentController>().reset();
  }

  @override
  void dispose() {
    if (timer != null) {
      timer!.cancel();
    }
    qrController.dispose();

    super.dispose();
  }

  resetTimer() {
    timerStartTime = DateTime.now();

    timer = Timer.periodic(
      Duration(seconds: 1),
      (_) {
        final now = DateTime.now();

        final diff = now.difference(timerStartTime);

        int totalSec = allowedDuration.inSeconds - diff.inSeconds;

        if (totalSec < 0) {
          totalSec = 0;
        }

        final sec = totalSec % 60;
        final min = (totalSec / 60).floor();

        setState(() {
          countDown =
              '${min.toString().padLeft(2, '0')}:${sec.toString().padLeft(2, '0')}';
        });
      },
    );
  }

  onQrViewCreated(QRViewController controller) {
    print('created');
    qrController = controller;

    qrController.scannedDataStream
        .throttleTime(Duration(milliseconds: 1500))
        .listen((data) async {
      if (data.code.length == 0) {
        return;
      }

      final c = Get.find<PaymentController>();

      final resp = await c.onQrScan(data);

      if (resp) {
        qrController.pauseCamera();

        await Get.toNamed('/payment/qr/input-amount');

        qrController.resumeCamera();
      } else {
        if (Get.isSnackbarOpen == false) {
          Get.snackbar('QR 인식 실패', 'QR 인식에 실패했습니다.', backgroundColor: Colors.white);
        }
        qrController.resumeCamera();
      }
    });
  }

  renderQrScanner() {
    final theme = ThemeFactory.of(context).theme;

    final size = MediaQuery.of(context).size;

    var scanArea = min(size.width, size.height) * 0.6;

    return Expanded(
      flex: 2,
      child: Stack(
        children: [
          Positioned.fill(
            child: QRView(
              overlay: QrScannerOverlayShape(
                borderColor: Colors.white,
                borderRadius: 10,
                borderLength: 30,
                borderWidth: 10,
                cutOutSize: scanArea,
              ),
              formatsAllowed: [BarcodeFormat.qrcode],
              overlayMargin: EdgeInsets.all(20.0),
              key: qrKey,
              onQRViewCreated: onQrViewCreated,
            ),
          ),
          Positioned.fill(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 24.0, bottom: 12.0),
                    child: Row(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Get.back();
                          },
                          child: Icon(
                            Icons.chevron_left,
                            size: 24.0,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Container(
                        color: Colors.transparent,
                        child: Text(
                          'QR 코드로 결제',
                          style: TextStyle(
                            fontSize: theme.fontSize22,
                            fontWeight: theme.heavyFontWeight,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  renderQrCode() {
    final theme = ThemeFactory.of(context).theme;

    return Expanded(
      flex: 1,
      child: GestureDetector(
        onTap: () async {
          await Get.find<PaymentController>().forceSetBarcodeId(1);
          await Get.toNamed('/payment/qr/input-amount');
        },
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 16.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'QR코드를 사격형안에 스캔해주세요',
                      style: TextStyle(
                        fontSize: theme.fontSize15,
                        fontWeight: theme.heavyFontWeight,
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                        '남은시간 $countDown',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: theme.fontSize13,
                          color: theme.primaryOrangeColor,
                          fontWeight: theme.heavyFontWeight,
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: Center(
                          child: QrImage(
                            data: 'ID=1',
                            version: QrVersions.auto,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  renderPadding() {
    final tPadding = MediaQuery.of(context).viewPadding.top;

    if (tPadding > 0) {
      return Container(
        height: tPadding,
        color: Colors.transparent,
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      useSafeArea: false,
      backgroundColor: Colors.transparent,
      body: Column(
        children: [
          renderPadding(),
          renderQrScanner(),
          renderQrCode(),

          //TODO Production 에서 지우기
          // RaisedButton(
          //   color: Colors.red,
          //   child: Text(
          //     '다음',
          //     style: TextStyle(color: Colors.white),
          //   ),
          //   onPressed: () {
          //     Get.toNamed('/payment/qr/input-amount');
          //   },
          // ),
        ],
      ),
    );
  }
}
