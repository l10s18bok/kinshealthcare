import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/card/request_approval_status_card.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-02-04
 * 작성자 : JC
 * 화면명 : HP_1014
 * 주요기능 : 결제 완료
 */

class DoneScreen extends StatefulWidget {
  @override
  _DoneScreenState createState() => _DoneScreenState();
}

class _DoneScreenState extends State<DoneScreen> {
  @override
  initState() {
    super.initState();

    Future.microtask(() {
      Get.find<PaymentController>().checkPaymentStatus();
    });
  }

  renderReceiptKeyValue({
    required String key,
    required String value,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      children: [
        Text(
          key,
          style: TextStyle(
            fontSize: theme.fontSize14,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 16.0),
            child: Text(
              value,
              style: TextStyle(
                fontSize: theme.fontSize14,
              ),
              textAlign: TextAlign.end,
            ),
          ),
        ),
      ],
    );
  }

  renderStatuses() {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          child: GetBuilder<PaymentController>(builder: (c) {
            return Row(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      Positioned.fill(
                        child: LayoutBuilder(
                          builder: (context, constraints) {
                            return Image.asset(
                              'assets/png/payment_done_receipt.png',
                              fit: BoxFit.fill,
                              width: constraints.maxWidth,
                            );
                          },
                        ),
                      ),
                      Positioned(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                    vertical: 36.0,
                                    horizontal: 22.0,
                                  ),
                                  child: Text(
                                    '결제가 성공적으로 완료되었습니다.',
                                    style: TextStyle(
                                      fontSize: theme.fontSize15,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 6.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: SvgPicture.asset(
                                      'assets/svgs/img/payment_done_receipt_dotted_line.svg',
                                      fit: BoxFit.fitWidth,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(height: 18.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 22.0),
                              child: renderReceiptKeyValue(
                                key: '가맹점',
                                value: c.storeInfo!.comName!,
                              ),
                            ),
                            Container(height: 12.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 22.0),
                              child: renderReceiptKeyValue(
                                key: '주소',
                                value: c.storeInfo!.address1! +
                                    c.storeInfo!.address2!,
                              ),
                            ),
                            Container(height: 18.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16.0),
                              child: Divider(),
                            ),
                            Container(height: 18.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 22.0),
                              child: Row(
                                children: [
                                  Text(
                                    '결제금액',
                                    style: TextStyle(
                                      fontWeight: theme.heavyFontWeight,
                                      fontSize: theme.fontSize14,
                                    ),
                                  ),
                                  Expanded(
                                    child: Text(
                                      c.amountDisplay + '원',
                                      textAlign: TextAlign.end,
                                      style: TextStyle(
                                        fontSize: theme.fontSize20,
                                        color: theme.primaryOrangeColor,
                                        fontWeight: theme.heavyFontWeight,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(height: 45.0),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            );

            return Container(
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 27.0, horizontal: 16.0),
                child: Column(
                  children: [
                    Divider(),
                    Container(height: 30.0),
                    Row(
                      children: [
                        Text(
                          '승인상태',
                          style: TextStyle(
                            fontSize: theme.fontSize13,
                            color: Color(0xFF2E2E2E),
                            fontWeight: theme.heavyFontWeight,
                          ),
                        ),
                      ],
                    ),
                    Container(height: 20.0),
                    ...c.paymentStatuses
                        .asMap()
                        .entries
                        .map(
                          (x) => Padding(
                            padding: EdgeInsets.only(
                                bottom: x.key == c.paymentStatuses.length - 1
                                    ? 0
                                    : 16.0),
                            child: RequestApprovalStatusCard(
                              model: x.value,
                              type: StatusTypes.approved,
                            ),
                          ),
                        )
                        .toList(),
                    // ListView.separated(
                    //     itemBuilder: (_, index) {
                    //       return RequestApprovalStatusCard(
                    //         model: c.paymentStatuses[index],
                    //         type: StatusTypes.approved,
                    //       );
                    //     },
                    //     separatorBuilder: (_, index) {
                    //       return Container(height: 16.0);
                    //     },
                    //     itemCount: c.paymentStatuses.length),
                    Container(height: 30.0),
                    Divider(),
                  ],
                ),
              ),
            );
          }),
        ),
      ],
    );
  }

  renderFooter() {
    return GetBuilder<PaymentController>(builder: (c) {
      return Padding(
        padding: EdgeInsets.only(top: 80.0, left: 16.0, right: 16.0),
        child: Column(
          children: [
            // PaymentAmountText(
            //   amount: c.amount!.toDouble(),
            //   label: '총 결제 금액',
            //   isPrimary: true,
            // ),
            // Container(height: 32.0),
            PrimaryButton(
              label: '확인',
              onTap: () {
                onDoneClick();
              },
            ),
          ],
        ),
      );
    });
  }

  renderIcon() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 48.0,
          height: 48.0,
          decoration: BoxDecoration(
            color: theme.primaryColor,
            shape: BoxShape.circle,
          ),
          child: Padding(
            padding: EdgeInsets.all(14.0),
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_check.svg',
            ),
          ),
        ),
        // SvgPicture.asset(
        //   'assets/svgs/ic/ic_payment_done.svg',
        // ),
      ],
    );
  }

  renderTop() {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      children: [
        renderIcon(),
        Padding(
          padding: EdgeInsets.only(top: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  child: Text(
                    '결제완료',
                    style: TextStyle(
                      fontSize: theme.fontSize18,
                      fontWeight: theme.heavyFontWeight,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        // Container(height: 36.0),
        // renderKeyValue(
        //   key: '결제할 금액',
        //   value: TextUtils().numberToLocalCurrency(
        //     amount: c.amount!.toDouble(),
        //     // amount: requiredPayment.toDouble(),
        //   ) +
        //       ' 원',
        // ),
        // Container(height: 16.0),
        // renderKeyValue(
        //   key: '모자란 금액',
        //   value: TextUtils().numberToLocalCurrency(
        //     amount: c.paymentStatuses.where((x) => !x.isApproved).fold(
        //         0,
        //             (previousValue, element) =>
        //         previousValue + element.amount),
        //     // amount: requiredPayment.toDouble(),
        //   ) +
        //       ' 원',
        //   isOrange: true,
        // ),
        // Container(height: 8.0),
        // Divider(),
      ],
    );
    // return GetBuilder<PaymentController>(builder: (c) {
    //   return ;
    // });
  }

  onDoneClick() {
    Get.offNamedUntil('/payment', (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    final bgColor = Color(0xFFF3F4F6);

    return CustomScrollLayout(
      bgColor: bgColor,
      topBar: BackTopBar(
        title: '',
        bgColor: bgColor,
        customOnBack: () async {
          onDoneClick();
        },
      ),
      body: Container(
        color: bgColor,
        child: Padding(
          padding: EdgeInsets.only(top: 40.0, bottom: 16.0),
          child: Column(
            children: [
              renderTop(),
              SizedBox(height: 24.0),
              Expanded(
                child: renderStatuses(),
              ),
              renderFooter(),
            ],
          ),
        ),
      ),
    );
  }
}
