import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/card/new_approval_status_card.dart';
import 'package:kins_healthcare/components/card/payment_status_card.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/services/payment/model/payment_payment_request_status.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-02-04
 * 작성자 : JC
 * 화면명 : HP_1014
 * 주요기능 : 결제 완료
 */

class FailScreen extends StatefulWidget {
  @override
  _FailScreenState createState() => _FailScreenState();
}

class _FailScreenState extends State<FailScreen> {
  renderStatuses() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      color: Color(0xFFF3F4F6),
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: GetBuilder<PaymentController>(builder: (c) {
          return ListView.separated(
            itemBuilder: (_, index) {
              return NewApprovalStatusCard(model: c.paymentStatuses[index]);
            },
            separatorBuilder: (_, index) {
              return Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0),
                child: Divider(
                  color: Color(0xFFDDDDDD),
                ),
              );
            },
            itemCount: c.paymentStatuses.length,
          );

          // return Container(
          //   decoration: BoxDecoration(
          //     color: Colors.white,
          //   ),
          //   child: Padding(
          //     padding: EdgeInsets.symmetric(vertical: 27.0, horizontal: 16.0),
          //     child: Column(
          //       children: [
          //         Divider(),
          //         Container(height: 30.0),
          //         Row(
          //           children: [
          //             Text(
          //               '승인상태',
          //               style: TextStyle(
          //                 fontSize: theme.fontSize13,
          //                 color: Color(0xFF2E2E2E),
          //                 fontWeight: theme.heavyFontWeight,
          //               ),
          //             ),
          //           ],
          //         ),
          //         Container(height: 20.0),
          //         ...c.paymentStatuses
          //             .asMap()
          //             .entries
          //             .map(
          //               (x) => Padding(
          //                 padding: EdgeInsets.only(
          //                     bottom:
          //                         x.key == c.paymentStatuses.length - 1 ? 0 : 16.0),
          //                 child: RequestApprovalStatusCard(
          //                   model: x.value,
          //                   type: x.value.isApproved
          //                       ? StatusTypes.approved
          //                       : StatusTypes.rejected,
          //                 ),
          //               ),
          //             )
          //             .toList(),
          //         // ListView.separated(
          //         //     itemBuilder: (_, index) {
          //         //       return RequestApprovalStatusCard(
          //         //         model: c.paymentStatuses[index],
          //         //         type: StatusTypes.approved,
          //         //       );
          //         //     },
          //         //     separatorBuilder: (_, index) {
          //         //       return Container(height: 16.0);
          //         //     },
          //         //     itemCount: c.paymentStatuses.length),
          //         Container(height: 30.0),
          //         Divider(),
          //       ],
          //     ),
          //   ),
          // );
        }),
      ),
    );
  }

  renderFooter() {
    return GetBuilder<PaymentController>(builder: (c) {
      // final approvedAmount = c.paymentStatuses
      //     .where((e) => e.isApproved)
      //     .fold(0, (num total, element) => total + element.amount);
      return Container(
        color: Color(0xFFF3F4F6),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            children: [
              // PaymentAmountText(
              //   amount: approvedAmount.toDouble(),
              //   label: '결제된 금액',
              // ),
              // PaymentAmountText(
              //   amount: (c.amount! - approvedAmount).toDouble(),
              //   label: '미결제 금액',
              //   isPrimary: true,
              // ),
              // Container(height: 32.0),
              PrimaryButton(
                label: '확인',
                onTap: () {
                  onDoneClick();
                },
              ),
            ],
          ),
        ),
      );
    });
  }

  renderIcon() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset(
          'assets/svgs/ic/ic_payment_done.svg',
        ),
      ],
    );
  }

  renderTop() {
    return Column(
      children: [
        renderIcon(),
        Padding(
          padding: EdgeInsets.only(top: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  child: Text(
                    '결제가 실패되었습니다.\n한도가 초과되어 결제에 실패했습니다.',
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ),
        // Container(height: 36.0),
        // renderKeyValue(
        //   key: '결제할 금액',
        //   value: TextUtils().numberToLocalCurrency(
        //     amount: c.amount!.toDouble(),
        //     // amount: requiredPayment.toDouble(),
        //   ) +
        //       ' 원',
        // ),
        // Container(height: 16.0),
        // renderKeyValue(
        //   key: '모자란 금액',
        //   value: TextUtils().numberToLocalCurrency(
        //     amount: c.paymentStatuses.where((x) => !x.isApproved).fold(
        //         0,
        //             (previousValue, element) =>
        //         previousValue + element.amount),
        //     // amount: requiredPayment.toDouble(),
        //   ) +
        //       ' 원',
        //   isOrange: true,
        // ),
        // Container(height: 8.0),
        // Divider(),
      ],
    );
    // return GetBuilder<PaymentController>(builder: (c) {
    //   return ;
    // });
  }

  onDoneClick() {
    Get.offNamedUntil('/payment', (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(
        title: '결제실패',
        customOnBack: () async {
          onDoneClick();
        },
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 40.0, bottom: 16.0),
        child: GetBuilder<PaymentController>(builder: (c) {
          final filtered = c.paymentStatuses.where((element) {
            if (element is PaymentRequestStatusResponseMe) {
              return element.isRejected;
            } else if (element is PaymentRequestStatusResponseRequest) {
              return element.isRejected;
            } else {
              return false;
            }
          });

          return Column(
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 24.0),
                child: PaymentStatusCard(
                    isFail: true,
                    text: c.setleStatus == '쓰실분 취소'
                        ? '결제를 취소하였습니다.'
                        : '거절된 결제요청이 ${filtered.length}건이 있어,\n결제가 진행되지 않았습니다'),
              ),
              Expanded(
                child: renderStatuses(),
              ),
              // renderTop(),
              // Expanded(
              //   child: renderStatuses(),
              // ),
              renderFooter(),
            ],
          );
        }),
      ),
    );
    // return PaymentIconLayout(
    //   message: '결제가 완료되었어요',
    //   title: '결제완료',
    //   body: Column(
    //     children: [
    //       Expanded(
    //         child: renderStatuses(),
    //       ),
    //       renderFooter(),
    //     ],
    //   ),
    // );
  }
}
