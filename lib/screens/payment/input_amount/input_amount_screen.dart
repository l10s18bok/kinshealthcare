import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_keyboard_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/card/pay_info_card.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

/*
 * 작성일 : 2021-01-18
 * 작성자 : JC
 * 화면명 : HP_1002
 * 주요기능 : 결제 입력 스크린
 */
class InputAmountScreen extends StatefulWidget {
  @override
  _InputAmountScreenState createState() => _InputAmountScreenState();
}

class _InputAmountScreenState extends State<InputAmountScreen>
    with TickerProviderStateMixin {
  late TextEditingController moneyController;
  late AnimationController animationController;
  late FocusNode focusNode;

  bool showKeyboard = true;

  @override
  void dispose() {
    moneyController.dispose();
    animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    Get.find<PaymentController>().onAmountInput(val: '0');

    moneyController = TextEditingController();
    focusNode = FocusNode();
    animationController = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );

    focusNode.addListener(() {
      setState(() {
        showKeyboard = focusNode.hasFocus;
      });
    });
  }

  renderText() {
    final theme = ThemeFactory.of(context).theme;

    final style = TextStyle(
      fontSize: theme.fontSize17,
      fontWeight: theme.heavyFontWeight,
      color: Color(0xFF303030),
    );

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                '결제하실',
                style: style,
              ),
            ],
          ),
          Row(
            children: [
              Text(
                '금액을 입력해주세요',
                style: style,
              ),
            ],
          ),
        ],
      ),
    );
  }

  renderMoneyTextField() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 23.0),
      child: GestureDetector(
        onTap: () async {
          final MoneyBottomSheetResult? resp = await Get.bottomSheet(
            MoneyKeyboardBottomSheet(),
            isScrollControlled: true,
          );

          if (resp != null) {
            Get.find<PaymentController>().setAmount(resp.amount.toInt());
          }
        },
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Color(0xFF878787),
                width: 1.0,
              ),
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GetBuilder<PaymentController>(builder: (c) {
                  return Text(
                    TextUtils()
                        .numberToLocalCurrency(amount: c.amount.toDouble()),
                    style: TextStyle(
                      color: Color(0xFFB6B6B6),
                    ),
                  );
                }),
                Text(
                  '원',
                ),
              ],
            ),
          ),
        ),
      ),
    );
    // return Padding(
    //   padding: EdgeInsets.symmetric(horizontal: 16.0),
    //   child: TextFormField(
    //     focusNode: focusNode,
    //     controller: moneyController,
    //     keyboardType: TextInputType.number,
    //     readOnly: true,
    //     autofocus: true,
    //     decoration: InputDecoration(
    //       suffixText: '원',
    //     ),
    //   ),
    // );
  }

  // renderKeyboard() {
  //   return Column(
  //     key: ValueKey<int>(1),
  //     mainAxisAlignment: MainAxisAlignment.end,
  //     children: [
  //       MoneyInputKeyboard(
  //         onKeyboardInput: (val) {
  //           final resp = Get.find<PaymentController>().onAmountInput(val: val);
  //           moneyController.text = resp;
  //         },
  //         onConfirm: () {
  //           if (focusNode.hasFocus) {
  //             focusNode.unfocus();
  //           }
  //         },
  //       ),
  //     ],
  //   );
  // }

  renderPayInfo() {
    return Column(
      key: ValueKey<int>(0),
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(height: 1.0, color: Color(0xFFE8E7EC)),
        GetBuilder<PaymentController>(builder: (c) {
          return PayInfoCard(
            total: TextUtils().numberToLocalCurrency(
              amount: c.amount.toDouble(),
            ),
            address: c.storeInfo!.address1!,
            price: c.amountDisplay + ' 원',
            companyName: c.storeInfo!.comName!,
          );
        }),
        Padding(
          padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 22.0),
          child: GetBuilder<PaymentController>(builder: (c) {
            return PrimaryButton(
              label: '다음',
              onTap: c.amount < 100
                  ? null
                  : () async {
                      await Get.find<PaymentController>().createPayment();

                      Get.toNamed('/payment/confirm');
                    },
            );
          }),
        ),
      ],
    );
  }

  renderFooter() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(bottom: 16.0),
        child: AnimatedSwitcher(
          duration: Duration(milliseconds: 50),
          child: renderPayInfo(),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(
        title: '결제금액 입력',
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 40.0),
        child: Column(
          children: [
            renderText(),
            renderMoneyTextField(),
            renderFooter(),
          ],
        ),
      ),
    );

    // return TitleLayout(
    //   title: '결제금액 입력',
    //   body: Column(
    //     children: [
    //       renderText(),
    //       renderMoneyTextField(),
    //       renderFooter(),
    //     ],
    //   ),
    // );
  }
}
