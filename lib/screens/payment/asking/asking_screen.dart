import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_bottom_sheet.dart';
import 'package:kins_healthcare/components/bottom_sheet/money_keyboard_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/services/payment/model/payment_request_steps_models.dart';
import 'package:kins_healthcare/services/relation/model/post_relation_search_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';
import 'package:kins_healthcare/utils/text_utils.dart';

class AskingScreenArguments {
  final int requiredPayment;

  /// key - id
  /// value - amount
  final Map<int, int> amounts;

  AskingScreenArguments({
    required this.requiredPayment,
    required this.amounts,
  });
}

/*
 * 작성일 : 2021-01-27
 * 작성자 : JC
 * 화면명 : HP_1011
 * 주요기능 : 추가 결제요청
 */
class AskingScreen extends StatefulWidget {
  @override
  _AskingScreenState createState() => _AskingScreenState();
}

class _AskingScreenState extends State<AskingScreen> {
  List<SearchRelResponse> family = [];

  // int requiredPayment = 0;

  /// 요청할 금액
  /// key - userNoRel
  /// value - amount
  Map<int, int> askingPayments = {};

  bool isLoading = false;

  @override
  initState() {
    super.initState();

    // final args = Get.arguments;
    //
    // if (args != null && args is AskingScreenArguments) {
    //   requiredPayment = args.requiredPayment;
    //   amounts = args.amounts;
    // }
  }

  renderFamilyCard({
    required SearchRelResponse family,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.only(bottom: 16.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: Color(0xFFE2E3F4),
            width: 1.0,
          ),
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        CircleAvatar(
                          backgroundImage: NetworkImage(
                            family.profileImgOpponent,
                          ),
                        ),
                        Container(width: 12.0),
                        Text(
                          '${family.userNameOpponent} (${family.userRelCdOpponentValue})',
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        this.askingPayments.removeWhere(
                              (key, value) => key == family.userNoRel,
                            );
                        this.family.removeWhere(
                              (element) =>
                                  element.userNoRel == family.userNoRel,
                            );
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey[300]!,
                          width: 1.0,
                        ),
                        borderRadius: BorderRadius.circular(4.0),
                      ),
                      child: Center(
                        child: Icon(
                          Icons.close,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(height: 16.0),
              GestureDetector(
                onTap: () async {
                  final parsedVal = await Get.bottomSheet(
                    MoneyKeyboardBottomSheet(),
                    isScrollControlled: true,
                  );

                  if (parsedVal != null &&
                      parsedVal is MoneyBottomSheetResult) {
                    setState(() {
                      this.askingPayments = {
                        ...askingPayments,
                        family.userNoRel: parsedVal.amount.toInt(),
                      };
                    });
                  }
                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4.0),
                    border: Border.all(
                      color: Colors.grey.shade400,
                      width: 1.0,
                    ),
                  ),
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            TextUtils().numberToLocalCurrency(
                              amount:
                                  this.askingPayments[family.userNoRel] == null
                                      ? 0
                                      : this
                                          .askingPayments[family.userNoRel]!
                                          .toDouble(),
                            ),
                            style: TextStyle(
                              color:
                                  this.askingPayments[family.userNoRel] == null
                                      ? Colors.grey
                                      : Colors.black,
                            ),
                          ),
                        ),
                        Text(
                          '원',
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              // Padding(
              //   padding: EdgeInsets.only(top: 16.0),
              //   child: TextFormField(
              //     keyboardType: TextInputType.number,
              //     onChanged: (val) {
              //       int parsedVal = val.length == 0 ? 0 : int.parse(val);
              //
              //       setState(() {
              //         this.askingPayments = {
              //           ...askingPayments,
              //           family.userNoRel: parsedVal,
              //         };
              //       });
              //     },
              //     decoration: InputDecoration(
              //       hintText: '0',
              //       hintStyle: TextStyle(
              //         color: Colors.grey,
              //       ),
              //       border: OutlineInputBorder(),
              //       suffixIcon: Column(
              //         mainAxisAlignment: MainAxisAlignment.center,
              //         children: [
              //           Text(
              //             '원',
              //             style: TextStyle(
              //               fontWeight: theme.heavyFontWeight,
              //               color: Colors.grey,
              //             ),
              //           ),
              //         ],
              //       ),
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  renderFamilies() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              ...List<Widget>.from(
                this.family.map(
                      (e) => renderFamilyCard(
                        family: e,
                      ),
                    ),
              ),
              renderAddFamilyButton(),
            ],
          ),
        ),
      ),
    );
  }

  renderAddFamilyButton() {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      children: [
        Expanded(
          child: TextButton(
            style: TextButton.styleFrom(
              backgroundColor: Color(0xFFE2E3F4),
            ),
            onPressed: () async {
              final resp = await Get.toNamed(
                '/payment/asking/family',
              );

              if (resp != null) {
                setState(() {
                  this.family.add(resp);
                });
              }
            },
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 4.0),
              child: Text(
                '+ 요청할 가족 추가',
                style: TextStyle(
                  color: theme.primaryColor,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  formatPrice(int price) {
    final formatter = NumberFormat('#,###');
    return formatter.format(price);
  }

  renderDeficitKeyValue({
    required String key,
    required String value,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          key,
          style: TextStyle(
            color: Colors.grey,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
        Text(
          value,
        ),
      ],
    );
  }

  renderFooter() {
    final theme = ThemeFactory.of(context).theme;

    final orangeStyle = TextStyle(
      fontSize: theme.fontSize20,
      color: theme.primaryOrangeColor,
      fontWeight: theme.heavyFontWeight,
    );

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '모자란 금액',
                style: TextStyle(
                  fontSize: theme.fontSize15,
                  color: Color(0xFF343435),
                  fontWeight: theme.mediumFontWeight,
                ),
              ),
              Row(
                children: [
                  GetBuilder<PaymentController>(builder: (c) {
                    return Text(
                      formatPrice(c.requestUserAmount),
                      style: orangeStyle,
                    );
                  }),
                  Container(width: 8.0),
                  Text(
                    '원',
                    style: orangeStyle.copyWith(
                      fontWeight: theme.primaryFontWeight,
                    ),
                  ),
                ],
              ),
            ],
          ),
          Container(height: 16.0),
          renderDeficitKeyValue(
            key: '요청할 금액',
            value: this.askingPayments.values.length == 0
                ? '0원'
                : formatPrice(
                      this.askingPayments.values.reduce(
                            (value, element) => value + element,
                          ),
                    ) +
                    '원',
          ),
          Container(height: 16.0),
          GetBuilder<PaymentController>(builder: (c) {
            return PrimaryButton(
              label: '다음',
              isLoading: this.isLoading,
              onTap: isLoading
                  ? null
                  : () async {
                      if (this.askingPayments.isEmpty) {
                        Get.snackbar('요청할 금액을 입력해주세요.', '요청할 금액을 입력해주세요.');
                        return;
                      }

                      final inputAmount = this
                          .askingPayments
                          .values
                          .reduce((value, element) => value + element);

                      if (c.requestUserAmount != inputAmount) {
                        Get.snackbar('요청할 금액 에러', '모자란 금액만큼 요청을 해주세요.');
                        return;
                      }

                      await c.updateRequestPayments(
                        toUsers: this
                            .askingPayments
                            .keys
                            .map(
                              (x) => PaymentStepsToUser(
                                amount: this.askingPayments[x]!,
                                toUserNo: x,
                              ),
                            )
                            .toList(),
                      );

                      try {
                        setState(() {
                          isLoading = true;
                        });

                        await c.makePaymentStepN();
                      } on DioError catch (e) {
                        setState(() {
                          isLoading = false;
                        });

                        SnackbarUtils().parseDioErrorMessage(
                          error: e,
                          title: '결제 에러',
                        );
                        return;
                      }

                      setState(() {
                        isLoading = false;
                      });

                      Get.toNamed(
                        '/payment/wait',
                        // arguments: WaitScreenArguments(
                        //   requiredPayment: this.requiredPayment,
                        //   requiredAmounts: this.askingPayments.map(
                        //         (key, value) => MapEntry(
                        //           this.family.firstWhere(
                        //               (element) => element.userNoRel == key),
                        //           value,
                        //         ),
                        //       ),
                        //   amounts: this.amounts,
                        // ),
                      );
                    },
            );
          }),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      resizeToAvoidBottomInset: false,
      topBar: BackTopBar(
        title: '모자란 금액 요청',
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 40.0, bottom: 16.0),
        child: Column(
          children: [
            renderFamilies(),
            renderFooter(),
          ],
        ),
      ),
    );
  }
}
