import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/card/request_bubble_card.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/components/radio_button/check_radio_button.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/services/payment/model/payment_payment_request_list_models.dart';

enum SortOptions {
  createdAtAsc,
  createdAtDes,
}

/*
 * 작성일 : 2021-01-22
 * 작성자 : JC
 * 화면명 : HP_2001
 * 주요기능 : 결제 리스트
 */
class RequestsScreen extends StatefulWidget {
  @override
  _RequestsScreenState createState() => _RequestsScreenState();
}

class _RequestsScreenState extends State<RequestsScreen> {
  late SortOptions sortOption;

  @override
  void initState() {
    super.initState();

    sortOption = SortOptions.createdAtDes;

    Future.microtask(() {
      Get.find<PaymentController>().getPaymentRequestList(
        reset: true,
      );
    });
  }

  renderTabs() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        children: [
          CheckRadioButton(
            label: '최근 순',
            value: SortOptions.createdAtDes,
            onTap: (val) {
              setState(() {
                sortOption = SortOptions.createdAtDes;
              });
            },
            groupValue: sortOption,
          ),
          Container(width: 14.0),
          CheckRadioButton(
            label: '오래된 순',
            value: SortOptions.createdAtAsc,
            onTap: (val) {
              setState(() {
                sortOption = SortOptions.createdAtAsc;
              });
            },
            groupValue: sortOption,
          ),
        ],
      ),
    );
  }

  renderItem(PaymentRequestListResponse item) {
    return Padding(
      padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 20.0),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: RequestBubbleCard(
                  onRejectTap: () async {
                    await Get.find<PaymentController>().rejectPaymentRequest(
                      setlePaymentId: item.setlePaymentId,
                    );
                  },
                  onAcceptTap: () async {
                    final c = Get.find<PaymentController>();

                    c.setNStepPrerequisites(
                      amount: item.amount,
                      step: item.requestCount,
                      address1: item.address1,
                      address2: item.address2,
                      comName: item.comName,
                      storeId: item.storeId,
                      setlePaymentId: item.setlePaymentId,
                    );

                    Get.toNamed('/payment/confirm');
                  },
                  profileImgFrom: item.profileImgFrom,
                  amount: item.amount,
                  userNameUse: item.userNameUse,
                  paymentDate: DateTime.parse(item.createDateTime),
                  profileImgUse: item.profileImgUse,
                  address: item.address1 + item.address2,
                  store: item.comName,
                  userNameFrom: item.userNameFrom,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  renderBody() {
    return GetBuilder<PaymentController>(builder: (c) {
      return PaginationListView(
        itemCount: c.paymentRequestList.length,
        itemBuilder: (_, index) {
          return renderItem(c.paymentRequestList[index]);
        },
        request: c.getPaymentRequestList,
        controller: c,
        loadingWidget: Container(),
        lastWidget: Container(),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(
        title: '받은 결제요청',
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 40.0),
        child: Column(
          children: [
            renderTabs(),
            Expanded(
              child: renderBody(),
            ),
          ],
        ),
      ),
    );

    // return TitleLayout(
    //   title: '받은 결제요청',
    //   body: Column(
    //     children: [
    //       renderTabs(),
    //       Expanded(
    //         child: ListView(
    //           children: [
    //             renderBody(),
    //           ],
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }
}
