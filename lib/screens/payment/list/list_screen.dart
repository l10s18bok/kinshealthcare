import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/check_button.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/card/payment_list_card.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/services/payment/model/payment_list_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_list_pagination_body.dart';

/*
 * 작성일 : 2021-01-21
 * 작성자 : JC
 * 화면명 : HP_0002
 * 주요기능 : 결제 목록
 */
class ListScreen extends StatefulWidget {
  @override
  _ListScreenState createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  late PaymentController _paymentController;
  bool isFinish = false;
  PaymentListPaginationBody pageBody =
      PaymentListPaginationBody(target: 'USER');
  bool isConnect = true, isNewest = false;
  int page = -1;

  @override
  void initState() {
    super.initState();
    _setPaymentController();
    _refresh();
  }

  _setPaymentController() {
    _paymentController = Get.find<PaymentController>();
    _paymentController.addListener(_setStateEndAnimation);
  }

  @override
  void dispose() {
    super.dispose();
    _paymentController.removeListener(_setStateEndAnimation);
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: renderTopBar(),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          child: renderPageListView(
            _paymentController.paymentList.length,
          ),
        ),
      ),
    );
  }

  Widget renderTopBar() {
    return BackTopBar(title: '결제목록');
  }

  Widget renderPageListView(int length) {
    return PaginationListView<PaymentListModel>(
      itemCount: length == 0 ? 1 : length,
      itemBuilder: (_, index) {
        if (index == 0) return renderFirstItem();
        return renderListItem(index);
      },
      controller: _paymentController,
      request: _paymentController.getPaymentList,
      lastWidget: Container(),
      loadingWidget: renderLoading(),
    );
  }

  Widget renderNonCard() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10, left: 16, right: 16, top: 0),
      child: NonKkakkaCard(title: '결제 목록이 없습니다.'),
    );
  }

  Widget renderFirstItem() {
    final list = _paymentController.paymentList;
    final model = list.length > 0 ? list[0] : null;

    return Column(
      children: [
        SizedBox(height: 31),
        renderFilter(),
        Container(height: 16.0),
        model != null
            ? PaymentListCard(
                model: model,
                endCallBack: () => _paymentCancelRequestCallBack(0),
              )
            : renderNonCard(),
        Container(height: 10.0),
      ],
    );
  }

  Widget renderListItem(int index) {
    final model = _paymentController.paymentList[index];

    return Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: PaymentListCard(
        model: model,
        endCallBack: () => _paymentCancelRequestCallBack(index),
      ),
    );
  }

  Widget renderFilter() {
    return Row(
      children: [
        CheckButton(
          label: '최근 순',
          padding: EdgeInsets.zero,
          onTap: () => _refresh(isNewest: true),
          isActive: isNewest,
        ),
        Container(width: 12.0),
        CheckButton(
          label: '오래된 순',
          padding: EdgeInsets.zero,
          onTap: () => _refresh(isNewest: false),
          isActive: !isNewest,
        ),
      ],
    );
  }

  Widget renderLoading() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      height: 50,
      child: CircularProgressIndicator(),
    );
  }

  _paymentCancelRequestCallBack(int index) {
    final model = _paymentController.paymentList[index];
    _paymentController.paymentList[index] =
        model.copyWith(setleStatus: "CANCEL_REQUEST");

    setState(() {});
  }

  Future<void> _refresh({bool isNewest = true}) async {
    this.isNewest = isNewest;
    pageBody = pageBody.copyWith(direction: !isNewest);
    await _paymentController.getPaymentList(body: pageBody, reset: true);
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }
}
