import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/btn_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/screens/payment/methods/register/account_reg_pageList/enter_account_number.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/box_grid_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

import 'account_reg_PageList/ars_certification.dart';
import 'account_reg_PageList/bank_selection.dart';

/*
 * 작성일 : 2021-01-25
 * 작성자 : Andy
 * 화면명 : HP_3004, HP_3005, HP_3006
 * 클래스 : AccountRegPagelist
 * 설명 : HP_3004 (계좌등록 => 은행선택), HP_3005(계좌등록 => 계좌번호입력), HP_3006(계좌등록 => ARS인증))
 */

class AccountRegPageList extends StatefulWidget {
  @override
  _AccountRegPageListState createState() => _AccountRegPageListState();
}

class _AccountRegPageListState extends State<AccountRegPageList> {
  int _currentPage = 0;
  FocusNode _arsFocusNode = FocusNode();
  PageController _pageController = PageController();
  final _formGlobalKey = GlobalKey<FormState>();

  String _bankName = ''; // 은행
  String _bankCode = ''; // 은행 코드
  String _userCon = ''; // 예금주
  String _birthCon = ''; // 생년월일/사업자번호
  String _accNumCon = ''; // 계좌번호
  String _bankNicCon = ''; // 은행별명
  bool _arsIng = false;
  bool _nextBtnActive = false;
  final List<String> _titleList = ['계좌등록', '계좌등록', 'ARS 인증'];

  String orderNo = '';
  String sendDt = '';
  String accountTradeIdEnc = '';

  bool doubleClick = false; // 다음버튼 중복 탭 방지

  late List<BoxGridModel> bankList = [];
  List<BoxGridModel> brokerageList = [];

  @override
  void initState() {
    super.initState();
    Future.microtask(() async {
      final bList = await Get.find<PaymentController>().getBankCode();
      for (var bank in bList) {
        if (bank.desc.contains('은행')) {
          final model = BoxGridModel(
            title: bank.desc,
            bankCode: bank.code,
            imagePath: returnBankLogo(bankCode: bank.code) ??
                'assets/png/card_ic_sh.png',
            backgroundColor: kinsGrayF2,
            borderColor: kinsGrayF2,
            selectBorderColor: kinsBlue40,
            selectColor: kinsWhite,
          );

          bankList.add(model);
        }
        if (bank.desc.contains('증권')) {
          final model = BoxGridModel(
            title: bank.desc,
            bankCode: bank.code,
            imagePath: returnBankLogo(bankCode: bank.code) ??
                'assets/png/card_ic_sh.png',
            backgroundColor: kinsGrayF2,
            borderColor: kinsGrayF2,
            selectBorderColor: kinsBlue40,
            selectColor: kinsWhite,
          );
          brokerageList.add(model);
        }
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    _arsFocusNode.dispose();
    _pageController.dispose();
    super.dispose();
  }

  _pageChangeCheck() {
    if (_currentPage == 1 && _bankName.length == 0) {
      Get.snackbar('안내', '은행을 선택해주세요');
      _pageController.jumpToPage(0);
    }
    if (!_arsIng && _currentPage == 2) {
      _goARS();
    }
    if (_currentPage == 1 && _arsIng) {
      _arsIng = false;
      doubleClick = false;
    }
  }

  _onChanged(int index) {
    FocusScope.of(context).requestFocus(new FocusNode());
    setState(() {
      _currentPage = index;
    });
    _pageChangeCheck();
  }

  _onTapNext() async {
    if (_arsIng && _currentPage == 2) {
      _confirmationNumber();
    } else {
      _pageController.nextPage(
        duration: Duration(milliseconds: 800),
        curve: Curves.easeInOutQuint,
      );
    }
  }

  _textFildCheck() {
    final currentState = this._formGlobalKey.currentState;
    if (currentState == null) return;

    if (currentState.validate()) {
      currentState.save();
      return false;
    }
    return true;
  }

  /// ARS 인증전
  _goARS() async {
    if (_textFildCheck()) {
      _pageController.jumpToPage(1);
      return;
    }
    _arsIng = true;
    FocusScope.of(context).requestFocus(_arsFocusNode);

    if (doubleClick) return;
    doubleClick = true;

    try {
      final resp = await Get.find<PaymentController>().authRequest(
        bankC: _bankCode,
        accountE: _accNumCon,
        paymentA: _bankNicCon,
        privateNo: _birthCon,
        usrNameE: _userCon,
      );
      if (resp == null) {
        Get.snackbar('에러', '잘못된 결제 정보입니다!');
        doubleClick = false;
        return;
      }
      orderNo = resp.orderNo!;
      sendDt = resp.sendDt!;
      doubleClick = false;
    } on DioError catch (e) {
      _arsIng = false;
      doubleClick = false;
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
      _pageController.jumpToPage(1);
    }
  }

  /// ARS 인증후
  _confirmationNumber() async {
    if (doubleClick) return;
    doubleClick = true;
    try {
      final resp = await Get.find<PaymentController>().authConfirm(
        bankC: _bankCode,
        orderN: orderNo,
        sendD: sendDt,
      );
      if (resp == null) {
        doubleClick = false;
        return;
      }
      _registerAccount(
          orderNo: resp.orderNo!, accountTradeIdEnc: resp.accountTradeIdEnc!);
    } on DioError catch (e) {
      _arsIng = false;
      doubleClick = false;
      final msg = e.response!.data['resultMsg'];
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  /// ㄱㅖ좌 최종 등록
  _registerAccount(
      {required String orderNo, required String accountTradeIdEnc}) async {
    try {
      await Get.find<PaymentController>().registerAccount(
        accountE: _accNumCon,
        bankC: _bankCode,
        accountTradeIdE: accountTradeIdEnc,
        orderN: orderNo,
        paymentA: _bankNicCon, //결제별칭
      );

      _showBottomSheet();
    } on DioError catch (e) {
      _arsIng = false;
      doubleClick = false;
      final msg = e.response!.data['resultMsg'];
      final sheet = ErrorBottomSheet(title: '에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  _showBottomSheet() async {
    final sheet = BtnBottomSheet(
      title: '계좌등록이 완료되었습니다.',
      isOneBtn: true,
    );
    await Get.bottomSheet<String>(sheet);
    Get.back();
  }

  _arsOnChange(String value) {
    print('ARS : $value');
  }

  renderTitleIndicator(String label) {
    final theme = ThemeFactory.of(context).theme;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          label,
          style: TextStyle(
            color: Colors.black,
            fontSize: theme.fontSize22,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List<Widget>.generate(_titleList.length, (int index) {
            return AnimatedContainer(
              duration: Duration(milliseconds: 300),
              height: index == _currentPage ? 20 : 10,
              width: index == _currentPage ? 20 : 10,
              margin: EdgeInsets.symmetric(horizontal: 3),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: index == _currentPage
                    ? Color(0xFF4042AB)
                    : Color(0xFFD4D8DC),
              ),
              child: Center(
                child: index == _currentPage
                    ? Text(
                        '${(index + 1).toString()}',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xFFF2F2F4),
                          fontSize: theme.fontSize12,
                          fontWeight: theme.heavyFontWeight,
                        ),
                      )
                    : Container(),
              ),
            );
          }),
        ),
      ],
    );
  }

  renderPageBuilder() {
    return Expanded(
      child: PageView.builder(
        scrollDirection: Axis.horizontal,
        onPageChanged: _onChanged,
        controller: _pageController,
        itemCount: _titleList.length,
        physics: _nextBtnActive ? null : NeverScrollableScrollPhysics(),
        itemBuilder: (context, int index) {
          switch (index) {
            case 0:
              if (bankList.isEmpty || brokerageList.isEmpty) return Container();
              return BankSelection(
                  bankList: bankList,
                  brokerageList: brokerageList,
                  onChanged: (model) {
                    _bankName = model.title;
                    _bankCode = model.bankCode!;
                    print('bank title : $_bankName    bank code : $_bankCode');
                    setState(() {
                      _nextBtnActive = true;
                    });
                  });
            case 1:
              return EnterAccountNumber(
                bankName: _bankName,
                userCon: (val){_userCon = val!;},
                birthCon: (val){_birthCon = val!;},
                accNumCon: (val){_accNumCon = val!;},
                bankNicCon: (val){_bankNicCon = val!;},
              );
            case 2:
              return ArsCertification(
                focusNode: _arsFocusNode,
                onChanged: _arsOnChange,
              );
            default:
              return Container();
          }
        },
      ),
    );
  }

  renderRegistButton() {
    return PrimaryButton(
      label: '다음', //!_arsIng && _currentPage == 2 ? 'ARS 인증전화 요청' : '다음',
      onTap: _nextBtnActive ? _onTapNext : null,
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
        child: Form(
          key: _formGlobalKey,
          child: Column(
            children: [
              BackTopBar(
                title: null,
                useDefaultPadding: false,
              ),
              renderTitleIndicator(_titleList[_currentPage]),
              SizedBox(height: 40),
              renderPageBuilder(),
              renderRegistButton(),
            ],
          ),
        ),
      ),
    );
  }
}
