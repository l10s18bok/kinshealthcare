import 'package:flutter/material.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-25
 * 작성자 : Andy
 * 화면명 : HP_3006
 * 경로  : /payment/methods/register/ars
 * 주요기능 : ARS 인증
 */

class ArsCertification extends StatelessWidget {
  final FocusNode focusNode;
  final ValueChanged<String> onChanged;

  const ArsCertification({
    required this.focusNode,
    required this.onChanged,
  });

  renderTextInputFild() {
    return SizedBox(
      height: 142,
      width: 142,
      child: CircleAvatar(
        backgroundColor: Color(0xFFF0F1F9),
        child: Container(),
        // TextFormField(
        //   onChanged: onChanged,
        //   focusNode: this.focusNode,
        //   //autofocus: true,
        //   maxLength: 4,
        //   textAlign: TextAlign.center,
        //   keyboardType: TextInputType.number,
        //   style: TextStyle(
        //     color: Color(0xFF4042AB),
        //     fontSize: 32.0,
        //   ),
        //   decoration: InputDecoration(
        //     counterText: '',
        //     border: InputBorder.none,
        //     contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        //   ),
        // ),
      ),

    );
  }

  renderInfoText(BuildContext context) {
    final theme = ThemeFactory.of(context).theme;
    return Align(
      alignment: Alignment.center,
      child: Column(
        children: [
          Text(
            //'하단 ARS인증전화 요청을 누르시고,',
            'ARS 인증전화 요청을 하였습니다.',
            style: TextStyle(
              color: Color(0xFF6D6D6D),
              fontSize: theme.fontSize15,
              fontWeight: theme.primaryFontWeight,
            ),
          ),
          SizedBox(height: 5),
          Text(
            //'수신되는 전화에 위 숫자를 입력해주세요.',
            '안내에따라 통화가 끝나면 다음 버튼을 눌러주세요.',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Color(0xFF6D6D6D),
              fontSize: theme.fontSize15,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(focusNode);
      },
      child: ListView(
        shrinkWrap: true,
        children: [
          renderTextInputFild(),
          SizedBox(height: 26),
          renderInfoText(context),
        ],
      ),
    );
  }
}
