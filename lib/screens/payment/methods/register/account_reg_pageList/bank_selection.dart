import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/grid/box_grid.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/box_grid_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-25
 * 작성자 : Andy
 * 화면명 : HP_3004
 * 경로  : 
 * 주요기능 : 계좌등록(은행선택)
 */

class BankSelection extends StatefulWidget {
  final ValueChanged<BoxGridModel>? onChanged;
  final ScrollController? scrollController;
  final bool? onlyBank;
  final List<BoxGridModel> bankList;
  final List<BoxGridModel> brokerageList; //증권사
  
  const BankSelection({
    Key? key,
    this.onChanged,
    this.onlyBank = false,
    this.scrollController,
    required this.bankList,
    required this.brokerageList,
  }) : super(key: key);

  @override
  _BankSelectionState createState() => _BankSelectionState();
}

class _BankSelectionState extends State<BankSelection> {
  // final bankList = [
  //   BoxGridModel(
  //     title: '농협',
  //     bankCode: 'B011',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=011',
  //     backgroundColor: kinsGrayF2,
  //     borderColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '국민은행',
  //     bankCode: 'B004',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=004',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '우리은행',
  //     bankCode: 'B020',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=020',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '신한은행',
  //     bankCode: 'B088',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=088',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '하나은행',
  //     bankCode: 'B081',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=005',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '기업은행',
  //     bankCode: 'B003',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=003',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '부산은행',
  //     bankCode: 'B032',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=032',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '우체국',
  //     bankCode: 'B071',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=071',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '대구은행',
  //     bankCode: 'B031',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=031',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '새마을금고',
  //     bankCode: 'B045',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=045',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '광주은행',
  //     bankCode: 'B034',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=034',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '경남은행',
  //     bankCode: 'B039',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=039',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '신협',
  //     bankCode: 'B048',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=048',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '전북은행"',
  //     bankCode: 'B037',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=037',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '수협은행',
  //     bankCode: 'B007',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=007',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '제주은행',
  //     bankCode: 'B035',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=035',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: '산업은행',
  //     bankCode: 'B002',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=002',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   ),
  //   BoxGridModel(
  //     title: 'K뱅크',
  //     bankCode: 'B089',
  //     imagePath: 'http:\/\/1.233.179.197:9091\/cmm\/img\/getImage.do?imgType=1&prm1=0000000181&prm2=089',
  //     borderColor: kinsGrayF2,
  //     backgroundColor: kinsGrayF2,
  //     selectBorderColor: kinsBlue40,
  //     selectColor: kinsWhite,
  //   )
  // ];

  List<Widget> renderBank() {
    final theme = ThemeFactory.of(context).theme;
    return [
      Text(
        '은행',
        style: TextStyle(
          color: Color(0xFF4042AB),
          fontSize: theme.fontSize14,
          fontWeight: FontWeight.bold,
        ),
      ),
      SizedBox(height: 14),
      BoxGrid(
        models: widget.bankList,
        onLastSelectItem: widget.onChanged,
        isBank: true,
        radius: 9.0,
      ),
    ];
  }

  List<Widget> renderSecuritiesCompany() {
    final theme = ThemeFactory.of(context).theme;
    if (widget.onlyBank! || widget.brokerageList.isEmpty) return [];
    return [
      Text(
        '증권사',
        style: TextStyle(
          color: Color(0xFF4042AB),
          fontSize: theme.fontSize14,
          fontWeight: FontWeight.bold,
        ),
      ),
      SizedBox(height: 14),
      BoxGrid(
        models: widget.brokerageList,
        onLastSelectItem: widget.onChanged,
        isBank: true,
        radius: 9.0,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      controller: widget.scrollController,
      padding: widget.onlyBank! ? const EdgeInsets.only(top: 37, left: 16, right: 16) : null,
      children: [
        ...renderBank(),
        SizedBox(height: 32),
        ...renderSecuritiesCompany(),
      ],
    );
  }
}
