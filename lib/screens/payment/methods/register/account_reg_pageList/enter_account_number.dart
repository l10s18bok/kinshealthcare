import 'package:flutter/material.dart';
import 'package:kins_healthcare/components/text_field/underline_text_form_field.dart';

/*
 * 작성일 : 2021-02-02
 * 작성자 : Andy
 * 화면명 : HP_3005
 * 경로  : /payment/methods/register/account
 * 주요기능 : 계좌번호 입력
 */


class EnterAccountNumber extends StatefulWidget {
  final String bankName;
  final FormFieldSetter<String> birthCon;
  final FormFieldSetter<String> accNumCon;
  final FormFieldSetter<String> bankNicCon;
  final FormFieldSetter<String> userCon;

  const EnterAccountNumber({
    required this.bankName,
    required this.birthCon,
    required this.accNumCon,
    required this.bankNicCon,
    required this.userCon,
  });

  @override
  _EnterAccountNumberState createState() => _EnterAccountNumberState();
}

class _EnterAccountNumberState extends State<EnterAccountNumber> {

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: [
        AbsorbPointer(
          child: UnderlineTextFormField(
            labelText: '은행명',
            initialValue: widget.bankName,
            suffixText: '',
            onSaved: (val){},
            validator: (val){},
          ),
        ),
        SizedBox(height: 41),
        UnderlineTextFormField(
          labelText: '예금주',
          validator: (val) {
            if (val!.length == 0) {
              return '잘못된 입력';
            }
            return null;
          },
          onSaved: widget.userCon,
          hintText: '예금주 입력',
        ),
        SizedBox(height: 41),
        UnderlineTextFormField(
          labelText: '생년월일/사업자번호',
          keyboardType: TextInputType.number,
          maxLength: 10,
          validator: (val) {
            if (val!.length == 0) {
              return '잘못된 입력';
            }
            return null;
          },
          onSaved: widget.birthCon,
          hintText: '생년월일 8자리(YYYYMMDD)/사업자번호 입력',
        ),
        SizedBox(height: 41),
        UnderlineTextFormField(
          labelText: '계좌번호',
          keyboardType: TextInputType.number,
          maxLength: 15,
          validator: (val) {
            if (val!.length == 0) {
              return '잘못된 입력';
            }
            return null;
          },
          onSaved: widget.accNumCon,
          hintText: '계좌번호 입력',
        ),
        SizedBox(height: 41),
        UnderlineTextFormField(
          labelText: '통장별명',
          validator: (val) {
            if (val!.length == 0) {
              return '잘못된 입력';
            }
            return null;
          },
          onSaved: widget.bankNicCon,
          hintText: '통장별명 입력',
        ),
      ],
    );
  }
}
