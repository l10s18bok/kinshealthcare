import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/top_bar/simple_hide_top_bar.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/screens/payment/methods/register/account_reg_pageList/bank_selection.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/box_grid_model.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 * 작성일 : 2021-05-07
 * 작성자 : Andy
 * 화면명 : HP_3004_2
 * 경로  : /payment/methods/register/bank
 * 주요기능 : 계좌등록(은행선택)
 */

class BankSelectScreen extends StatefulWidget {
  @override
  _BankSelectScreenState createState() => _BankSelectScreenState();
}

class ChoiceBank {
  final String bank;

  ChoiceBank({required this.bank});
}

class _BankSelectScreenState extends State<BankSelectScreen> {
  ScrollController scrollController = ScrollController();
  String bankName = ''; // 은행
  bool nextBtnActive = false;

  late List<BoxGridModel> bankList = [];
  @override
  void initState() { 
    super.initState();
    Future.microtask(() async {
      final bList = await Get.find<PaymentController>().getBankCode();
      for (var bank in bList) {
        if (bank.desc.contains('은행')) {
          final model = BoxGridModel(
            title: bank.desc,
            bankCode: bank.code,
            imagePath: returnBankLogo(bankCode : bank.code) ?? 'assets/png/card_ic_sh.png',
            backgroundColor: kinsGrayF2,
            borderColor: kinsGrayF2,
            selectBorderColor: kinsBlue40,
            selectColor: kinsWhite,
          );
          
          bankList.add(model);
        }
        setState(() {
          
        });

      }
    });
  }

  renderAnimationTopBar() {
    return SimpleHideTopBar(
      title: '은행 선택',
      scrollController: scrollController,
    );
  }

  putButton() async {
    final result = await Get.toNamed(
      '/payment/methods/register/card',
      arguments: ChoiceBank(bank: bankName),
    );
    if (result != null) Get.back();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  renderNextButton() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      // margin: const EdgeInsets.only(bottom: 16),
      child: PrimaryButton(
        label: "다음",
        onTap: nextBtnActive ? putButton : null,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Column(
        children: [
          renderAnimationTopBar(),
          //SizedBox(height: 37),
          Expanded(
            child: bankList.isEmpty ? Container() : BankSelection(
              bankList: bankList,
              brokerageList: [],
              onChanged: (model) {
                bankName = model.title;
                setState(() {
                  nextBtnActive = true;
                });
              },
              onlyBank: true,
            ),
          ),
          //SizedBox(height: 50),
          renderNextButton()
        ],
      ),
    );
  }
}
