import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/simple_information_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/text_field/underline_text_form_field.dart';
import 'package:kins_healthcare/components/top_bar/simple_hide_top_bar.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/layouts/layouts.dart';
import 'package:kins_healthcare/screens/payment/methods/register/card/bank_select_screen.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-02-03
 * 작성자 : Andy
 * 화면명 : HP_3002
 * 클래스 : AddCard
 * 경로  : /payment/methods/register/card
 * 설명 : 계좌등록 => 결제카드추가
 */

class AddCard extends StatefulWidget {
  @override
  _AddCardState createState() => _AddCardState();
}

class _AddCardState extends State<AddCard> {
  final _formGlobalKey = GlobalKey<FormState>();
  late ScrollController _scrollController;
  final FocusNode _firstCardNumFocus = FocusNode();
  final FocusNode _secondCardNumFocus = FocusNode();
  final FocusNode _thirdCardNumFocus = FocusNode();
  final FocusNode _lastCardNumFocus = FocusNode();
  final FocusNode _cardDateFocus = FocusNode();
  final FocusNode _cardPasswordFocus = FocusNode();

  ///명의자 이름
  String? ownerName;

  ///생년월일이나 사업자번호
  String? privateNumber;

  ///카드 별명
  String? cardNickname;

  ///카드 번호
  List<String> cardNum = ['', '', '', ''];

  ///카드유효기간 (MM/YY)
  String? cardDate;

  ///비밀번호 (앞자리 2개)
  String? password;

  ///선택한 은행
  String? bank;

  /// 다음버튼 중복 탭 방지
  bool doubleClick = false;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    final args = Get.arguments;

    if (args is ChoiceBank) {
      bank = args.bank;
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  renderAnimationTopBar() {
    return SimpleHideTopBar(
      title: '결제카드 추가',
      scrollController: _scrollController,
    );
  }

  renderFieldLabel(String label, {String? subLabel}) {
    final theme = ThemeFactory.of(context).theme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              label,
              style: TextStyle(
                color: Colors.black,
                fontSize: theme.fontSize13,
                fontWeight: theme.heavyFontWeight,
              ),
            ),
            SizedBox(width: 8),
            Text(
              subLabel ?? '',
              style: TextStyle(
                color: Color(0xFF4042AB),
                fontSize: theme.fontSize11,
              ),
            ),
          ],
        ),
        SizedBox(height: 5),
      ],
    );
  }

  renderRowDivider() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 7),
      child: Text('-'),
    );
  }

  renderCardNum() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: UnderlineTextFormField(
            labelText: '카드번호',
            onSaved: (val) {
              cardNum[0] = val!;
            },
            validator: (val) {
              if (val!.length != 4) {
                return '잘못된 입력';
              }
              return null;
            },
            maxLength: 4,
            hintText: '0000',
            focusNode: _firstCardNumFocus,
            keyboardType: TextInputType.number,
            suffixText: '',
          ), 
        ),
        renderRowDivider(),
        Expanded(
          child: UnderlineTextFormField(
            labelText: '',
            onSaved: (val) {
              cardNum[1] = val!;
            },
            validator: (val) {
              if (val!.length != 4) {
                return '잘못된 입력';
              }
              return null;
            },
            maxLength: 4,
            hintText: '0000',
            focusNode: _secondCardNumFocus,
            keyboardType: TextInputType.number,
            suffixText: '',
          ),
        ),
        renderRowDivider(),
        Expanded(
          child: UnderlineTextFormField(
            labelText: '',
            onSaved: (val) {
              cardNum[2] = val!;
            },
            validator: (val) {
              if (val!.length != 4) {
                return '잘못된 입력';
              }
              return null;
            },
            maxLength: 4,
            hintText: '0000',
            focusNode:  _thirdCardNumFocus,
            keyboardType: TextInputType.number,
            suffixText: '',
          ),
        ),
        renderRowDivider(),
        Expanded(
          child: UnderlineTextFormField(
            labelText: '',
            onSaved: (val) {
              cardNum[3] = val!;
            },
            validator: (val) {
              if (val!.length != 4) {
                return '잘못된 입력';
              }
              return null;
            },
            maxLength: 4,
            hintText: '0000',
            focusNode:  _lastCardNumFocus,
            keyboardType: TextInputType.number,
            suffixText: '',
          ),
        ),
      ],
    );
  }

  renderCardDate() {
    return Row(
      children: [
        Expanded(
          child: UnderlineTextFormField(
            labelText: '카드 유효기간',
            onSaved: (val) {
              cardDate = val;
            },
            validator: (val) {
              if (val!.length != 4) {
                return '잘못된 입력';
              }
              return null;
            },
            maxLength: 4,
            hintText: 'MM/YY',
            focusNode:  _cardDateFocus,
            keyboardType: TextInputType.number,
                ),
        ),
        SizedBox(width: 20),
        Expanded(
          child: UnderlineTextFormField(
            labelText: '카드 비밀번호',
            onSaved: (val) {
              cardDate = val;
            },
            validator: (val) {
              if (val!.length < 1 || val.length > 2) {
                return '잘못된 입력';
              }
              return null;
            },
            maxLength: 2,
            hintText: '앞 2자리',
            focusNode:  _cardPasswordFocus,
            keyboardType: TextInputType.number,
          ),
        ),
      ],
    );
  }


  renderFormFieldList() {
    return ListView(
      controller: _scrollController,
      padding: const EdgeInsets.fromLTRB(16, 0, 16, 50),
      children: [
        SizedBox(height: 37),
        UnderlineTextFormField(
          labelText: '카드 명의자 이름',
          validator: (val) {
            if (val!.length < 1) {
              return '필수 입력사항입니다.';
            }
            return null;
          },
          onSaved: (val) {
            ownerName = val;
          },
          hintText: '이름 입력',
        ),
        SizedBox(height: 28),
        UnderlineTextFormField(
          labelText: '생년월일 또는 사업자번호',
          validator: (val) {
            if (val == null) return null;
            if (val.length < 1) {
              return '필수 입력사항입니다.';
            } else if (!(val.length == 6 || val.length == 10)) {
              return '생년월일 6자리 혹은 사업자번호 10자리를 입력하세요.';
            }
            return null;
          },
          onSaved: (val) {
            privateNumber = val;
          },
          hintText: '생년월일 또는 사업자번호 입력',
          keyboardType: TextInputType.number,
        ),
        SizedBox(height: 28),
        renderFieldLabel('카드 별명', subLabel: '*사용하시는 카드의 별명을 작성해주세요.'),
        UnderlineTextFormField(
          validator: (val) {
            if (val == null) return null;
            if (val.length < 1) {
              return '필수 입력사항입니다.';
            }
            return null;
          },
          onSaved: (val) {
            cardNickname = val;
          },
          hintText: '카드별명 입력',
        ),
        SizedBox(height: 28),
        renderCardNum(),
        SizedBox(height: 28),
        renderCardDate(),
      ],
    );
  }

  renderNextButton() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      margin: const EdgeInsets.only(bottom: 16),
      child: PrimaryButton(
        label: "다음",
        onTap: () async {
          final currentState = this._formGlobalKey.currentState;
          if (currentState == null) return;

          if (currentState.validate()) {
            currentState.save();

            if (doubleClick) return;
            doubleClick = true;

            try {
              await Get.find<PaymentController>().registerCard(
                ownerName: ownerName!,
                privateNumber: privateNumber!,
                companyName: bank!,
                cardNickname: cardNickname!,
                cardNum: '${cardNum[0]}${cardNum[1]}${cardNum[2]}${cardNum[3]}',
                cardDateYear: cardDate!.substring(2, 4),
                cardDateMonth: cardDate!.substring(0, 2),
                password: password!,
              );

              final sheet = SimpleInformationBottomSheet(message: '카드를 등록했어요 :)');
              await Get.bottomSheet<String>(sheet);

              Get.back(result: 'OK');
            } on DioError catch (e) {
              doubleClick = false;
              final msg = e.response!.data.toString();
              final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
              Get.bottomSheet(sheet);
              return;
            }
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Form(
        key: _formGlobalKey,
        child: Column(
          children: [
            renderAnimationTopBar(),
            Expanded(child: renderFormFieldList()),
            renderNextButton(),
          ],
        ),
      ),
    );
  }
}
