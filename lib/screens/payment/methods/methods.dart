import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/new_payment_method_bottom_sheet.dart';
import 'package:kins_healthcare/components/tab/text_tab.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/services/user/payment/model/payment_card_info.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-24
 * 작성자 : JC
 * 화면명 : HP_3001
 * 주요기능 : 결제수단 관리
 */
class MethodsScreen extends StatefulWidget {
  @override
  _MethodsScreenState createState() => _MethodsScreenState();
}

class _MethodsScreenState extends State<MethodsScreen> {
  bool isCardSelected = true;

  @override
  void initState() {
    super.initState();

    Future.microtask(() async {
      //패이지 들어올때 항상 http통신. >>baronCheck<<
      await Get.find<UserController>().getUserPayCardInfo(
        reset: true,
      );
    });
  }

  renderMessage() {
    final theme = ThemeFactory.of(context).theme;

    final style = TextStyle(
      fontWeight: theme.heavyFontWeight,
      fontSize: theme.fontSize17,
      color: Color(0xFF303030),
    );

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                '결제수단을',
                style: style,
              ),
            ],
          ),
          Row(
            children: [
              Text(
                '추가/편집할 수 있습니다.',
                style: style,
              ),
            ],
          ),
        ],
      ),
    );
  }

  renderCard({
    String? imageUrl,
  }) {
    final theme = ThemeFactory.of(context).theme;

    if (imageUrl == null) {
      return Container(
        color: theme.primaryColor,
        width: 65.0,
        height: 40.0,
      );
    }
    return CachedNetworkImage(
      imageUrl: imageUrl,
    );
  }

  renderCardRowText({
    required String label,
    required String subLabel,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(left: 12.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  label,
                  style: TextStyle(
                    fontSize: theme.fontSize12,
                    fontWeight: theme.heavyFontWeight,
                    color: Color(0xFF545454),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  subLabel,
                  style: TextStyle(
                    fontSize: theme.fontSize11,
                    fontWeight: theme.primaryFontWeight,
                    color: Color(0xFF545454),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  renderCardRowIcon() {
    return Row(
      children: [
        Icon(
          Icons.chevron_right,
          color: Colors.grey,
        ),
      ],
    );
  }

  renderCardRow({
    required String label,
    required String subLabel,
    String? imageUrl,
  }) {
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          renderCard(
            imageUrl: imageUrl,
          ),
          renderCardRowText(
            label: label,
            subLabel: subLabel,
          ),
          renderCardRowIcon(),
        ],
      ),
    );
  }

  renderLabel({
    required String title,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(vertical: 24.0),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 16.0,
            ),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 24.0),
                  child: Row(
                    children: [
                      Text(
                        title,
                        style: TextStyle(
                          fontSize: theme.fontSize11,
                          color: Color(0xFF929292),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  renderTabs() {
    return TextTab(
      labels: ['카드', '계좌'],
      onChange: (int index) {
        setState(() {
          this.isCardSelected = (index == 0);
        });
      },
    );
  }

  renderItems() {
    return Expanded(
      child: GetBuilder<UserController>(
        builder: (c) {
          final theme = ThemeFactory.of(context).theme;

          List<PaymentCardInfoResponse> items = [];
          String label = '등록된 카드';

          if (this.isCardSelected) {
            items = c.paymentCardInfos
                .where((element) => element.paymentTypeDesc == '신용카드')
                .toList();
          } else {
            items = c.paymentCardInfos
                .where((element) => element.paymentTypeDesc == '가상계좌')
                .toList();

            label = '등록된 계좌';
          }

          return Padding(
            padding: EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      label,
                      style: TextStyle(
                        fontSize: theme.fontSize11,
                        fontWeight: theme.heavyFontWeight,
                        color: Color(0xFF929292),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text(
                        items.length.toString(),
                        style: TextStyle(
                          fontSize: theme.fontSize11,
                          fontWeight: theme.heavyFontWeight,
                          color: theme.primaryColor,
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 16.0),
                    child: ListView.separated(
                      itemBuilder: (_, index) {
                        final item = items[index];

                        return renderCardRow(
                          subLabel: item.paymentAlias,
                          imageUrl: null,
                          label: item.paymentCorp,
                        );
                      },
                      separatorBuilder: (_, index) {
                        return Container(height: 16.0);
                      },
                      itemCount: items.length,
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(
        title: '결제수단 관리',
        rightButtons: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () async{
                // Get.bottomSheet(
                //   NewPaymentMethodBottomSheet(),
                // );
                //2021.05.31 Andy 수정
                final result = await Get.bottomSheet<String>(
                  NewPaymentMethodBottomSheet(),
                );
                if(result == null) return;
                if(result == 'CARD') {
                  await Get.toNamed('/payment/methods/register/bank');
                } else {
                  await Get.toNamed('/payment/methods/register/account_reg_pageList');
                }
                await Get.find<UserController>().getUserPayCardInfo(
                  reset: true,
                );
                setState(() {});
              },
              child: SvgPicture.asset(
                'assets/svgs/ic/ic_plus_main.svg',
                width: 30.0,
                height: 30.0,
              ),
            ),
          ],
        ),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 40.0),
        child: Column(
          children: [
            renderMessage(),
            Container(height: 65.0),
            renderTabs(),
            renderItems(),
          ],
        ),
      ),
    );
    // return TitleLayout(
    //   title: '결제수단 관리',
    //   customActionWidget: GestureDetector(
    //     onTap: () {
    //       Get.bottomSheet(
    //         NewPaymentMethodBottomSheet(),
    //       );
    //     },
    //     child: SvgPicture.asset(
    //       'assets/svgs/ic/ic_plus_main.svg',
    //     ),
    //   ),
    //   body: Column(
    //     children: [
    //       renderMessage(),
    //       Container(height: 65.0),
    //       renderTabs(),
    //       Expanded(
    //         child: renderItems(),
    //       ),
    //     ],
    //   ),
    // );
  }
}
