import 'package:dio/dio.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/card/bank_account_and_credit_card.dart';
import 'package:kins_healthcare/components/card/kkakka_payment_card.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/payment_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/models/local/user_pay_card_model.dart';
import 'package:kins_healthcare/screens/auth/verify/pin/pin_screen.dart';
import 'package:kins_healthcare/services/payment/model/payment_kkakka_list_models.dart';
import 'package:kins_healthcare/services/payment/model/payment_request_steps_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';

/*
 * 작성일 : 2021-01-22
 * 작성자 : JC
 * 화면명 : HP_1003
 * 주요기능 : 결제금액 입력 확인
 */
class ConfirmScreen extends StatefulWidget {
  @override
  _ConfirmScreenState createState() => _ConfirmScreenState();
}

class _ConfirmScreenState extends State<ConfirmScreen> {
  bool isTypeOne = false;
  List<FocusNode> focusNodes = [];
  bool showKeyboard = false;

  /// key : id value : amount
  Map<int, int> amounts = {};
  int totalInput = 0;

  bool paymentsExpanded = false;
  bool payRemainingAmount = false;

  bool isLoading = false;

  /// 가상계좌 || 신용카드 || 가족요청
  String openedPayment = '가상계좌';

  int? selectedPaymentId;

  @override
  void initState() {
    super.initState();

    Future.microtask(() async {
      final pController = Get.find<PaymentController>();

      pController.resetPaymentBodies();

      final List<PostPaymentKkakkaListResponse> resp = await pController.listKkakkas();

      final userC = Get.find<UserController>();
      await userC.getUserPayCardInfo(
        reset: true,
      );

      selectedPaymentId = userC.paymentCardInfos
          .where(
            (element) => element.paymentTypeDesc == openedPayment,
          )
          .first
          .paymentWayId;

      setState(() {
        this.amounts = Map.fromIterable(resp, key: (el) => el.kkakkaId, value: (el) => 0);
        // this.amounts = Map..generate(resp.length, (index) => {
        //   resp[index]: 0,
        // });
      });
    });
  }

  renderCard() {
    final theme = ThemeFactory.of(context).theme;

    return GetBuilder<PaymentController>(builder: (c) {
      return Padding(
        padding: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 24.0),
        child: Row(
          children: [
            Expanded(
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        c.storeInfo!.comName!,
                        style: TextStyle(
                          fontSize: theme.fontSize15,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        c.amountDisplay,
                        style: TextStyle(
                          fontSize: theme.fontSize20,
                          color: theme.primaryBlueColor,
                          fontWeight: theme.heavyFontWeight,
                        ),
                      ),
                      Text(
                        '원',
                        style: TextStyle(
                          color: theme.primaryBlueColor,
                          fontSize: theme.fontSize16,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            // renderChip(),
          ],
        ),
      );

      // return PayInfoCard(
      //   total: c.amountDisplay,
      //   squashAmount: false,
      //   address: c.storeInfo!.address1!,
      //   price: c.amountDisplay,
      //   companyName: c.storeInfo!.comName!,
      // );
    });
  }

  renderChip() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      decoration: BoxDecoration(
        color: YELLOW_COLOR,
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 11.0, vertical: 3.0),
        child: Text(
          '교육비',
          style: TextStyle(
            color: Colors.white,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
      ),
    );
  }

  renderAmountInputsTitle() {
    final theme = ThemeFactory.of(context).theme;

    final color = Color(0xFFF3F4F6);

    return Container(
      color: color,
      child: Padding(
        padding: EdgeInsets.only(
          left: 16.0,
          right: 16.0,
          top: 30.0,
          bottom: 22.0,
        ),
        child: Row(
          children: [
            Text(
              '사용가능한 까까',
              style: TextStyle(
                fontSize: theme.fontSize13,
                color: Color(0xFF2E2E2E),
                fontWeight: theme.heavyFontWeight,
              ),
            ),
          ],
        ),
      ),
    );
  }

  renderShowMoreButton() {
    if (this.amounts.keys.length < 3) {
      return Container();
    } else {
      return Padding(
        padding: EdgeInsets.only(bottom: 16.0),
        child: TextButton(
          onPressed: () {
            setState(() {
              this.paymentsExpanded = !this.paymentsExpanded;
            });
          },
          style: TextButton.styleFrom(
            shape: StadiumBorder(
              side: BorderSide(color: Colors.grey, width: 1.0),
            ),
            backgroundColor: Colors.white,
          ),
          child: Padding(
            padding: EdgeInsets.only(left: 16.0, right: 6.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  this.paymentsExpanded ? '숨기기' : '까까 전체보기',
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 3),
                  child: Icon(
                    Icons.chevron_right,
                    // this.paymentsExpanded
                    //     ? Icons.keyboard_arrow_up
                    //     : Icons.keyboard_arrow_down,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }

  renderAmountInputs() {
    List keys = this.amounts.keys.toList();

    if (!this.paymentsExpanded && keys.length > 2) {
      keys = keys.getRange(0, 2).toList();
    }

    return AnimatedContainer(
      color: Color(0xFFF3F4F6),
      duration: Duration(milliseconds: 200),
      child: Column(
        children: [
          renderAmountInputsTitle(),
          ...keys
              .map(
                (x) => GetBuilder<PaymentController>(
                  builder: (c) {
                    int curCouponAmount = this.amounts[x] == null ? 0 : this.amounts[x]!;

                    int maxAmount = c.amount - totalInput + curCouponAmount;

                    return Padding(
                      padding: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
                      child: Column(
                        children: [
                          KkakkaPaymentCard(
                            kkakka: c.kkakkas.firstWhere(
                              (element) => element.kkakkaId == x,
                            ),
                            onAmountChange: (double amount) {
                              setState(() {
                                this.amounts[x] = amount.toInt();
                                totalInput = this.amounts.values.reduce((value, element) => value + element);
                              });
                            },
                            maxAmount: maxAmount,
                          ),
                        ],
                      ),
                    );
                    // return Container(
                    //   color: color,
                    //   child: Padding(
                    //     padding: EdgeInsets.only(
                    //       left: 16.0,
                    //       right: 16.0,
                    //       bottom: 20.0,
                    //     ),
                    //     child: PaymentFormField(
                    //       onChanged: (String? value) {
                    //         setState(() {
                    //           this.amounts[x] =
                    //               value == null || value.length == 0
                    //                   ? 0
                    //                   : int.parse(value);
                    //           totalInput = this
                    //               .amounts
                    //               .values
                    //               .reduce((value, element) => value + element);
                    //         });
                    //       },
                    //       kkakka: c.kkakkas
                    //           .firstWhere((element) => element.kkakkaId == x),
                    //       name: c.kkakkas
                    //           .firstWhere((element) => element.kkakkaId == x)
                    //           .userNameOpponent!,
                    //       validator: (String? value) {
                    //         final kkakka = c.kkakkas
                    //             .firstWhere((element) => element.kkakkaId == x);
                    //
                    //         final intVal = int.parse(value!);
                    //
                    //         if (intVal > kkakka.kkakkaLimit!) {
                    //           return '1회 금액을 넘어설 수 없습니다.';
                    //         }
                    //
                    //         if (intVal > kkakka.kkakkaPrice!) {
                    //           return '총 금액을 넘어설 수 없습니다.';
                    //         }
                    //
                    //         return null;
                    //       },
                    //       totalAmount: c.kkakkas
                    //           .firstWhere((element) => element.kkakkaId == x)
                    //           .kkakkaPrice!,
                    //       avatarImage: c.kkakkas
                    //           .firstWhere((element) => element.kkakkaId == x)
                    //           .profileImgOpponent!,
                    //     ),
                    //   ),
                    // );
                  },
                ),
              )
              .toList(),
          renderShowMoreButton(),
        ],
      ),
    );
  }

  renderTitle({
    required String key,
    required String value,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          key,
          style: TextStyle(
            fontSize: theme.fontSize15,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
        Text(
          value,
          style: TextStyle(
            fontSize: theme.fontSize15,
            fontWeight: theme.heavyFontWeight,
            color: theme.primaryOrangeColor,
          ),
        ),
      ],
    );
  }

  renderSubKeyValuePair({
    required String key,
    required String value,
  }) {
    final theme = ThemeFactory.of(context).theme;

    final keyStyle = TextStyle(
      fontSize: theme.fontSize14,
      fontWeight: theme.heavyFontWeight,
      color: Color(0xFF6D6D6D),
    );

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          key,
          style: keyStyle,
        ),
        Text(
          value,
          style: keyStyle.copyWith(
            color: Color(0xFF262626),
            fontWeight: theme.primaryFontWeight,
          ),
        )
      ],
    );
  }

  renderTotal() {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 16.0,
        vertical: 20.0,
      ),
      child: Column(
        children: [
          renderTitle(
            key: '남은 결제 금액',
            value: '30,000원',
          ),
          Container(height: 20.0),
          Container(height: 1.0, color: Color(0xFFE8E7EC)),
          Container(height: 21.0),
          renderSubKeyValuePair(
            key: '상품금액',
            value: '90,000원',
          ),
          Container(height: 15.0),
          renderSubKeyValuePair(
            key: '까까 1',
            value: '-30,000원',
          ),
          Container(height: 15.0),
          renderSubKeyValuePair(
            key: '까까 2',
            value: '-30,000원',
          ),
        ],
      ),
    );
  }

  // renderPaymentMethod() {
  //   final theme = ThemeFactory.of(context).theme;
  //
  //   return Padding(
  //     padding: EdgeInsets.symmetric(
  //       horizontal: 16.0,
  //       vertical: 21.0,
  //     ),
  //     child: Column(
  //       children: [
  //         renderTitle(
  //           key: '결제 수단',
  //           value: '신용/체크카드',
  //         ),
  //         Row(
  //           mainAxisAlignment: MainAxisAlignment.end,
  //           children: [
  //             Text(
  //               '*결제수단은 카드 하나로 동일합니다.',
  //               style: TextStyle(
  //                 fontSize: theme.fontSize14,
  //                 fontWeight: theme.primaryFontWeight,
  //                 color: Color(0xFF6D6D6D),
  //               ),
  //             ),
  //           ],
  //         ),
  //         Container(height: 24.0),
  //         DefaultDropDownFormField(
  //           label: '카드사 선택',
  //           onChanged: (val) {},
  //           items: [
  //             DropdownMenuItem(
  //               child: Text('신한카드'),
  //             ),
  //           ],
  //           validator: (val) {
  //             return null;
  //           },
  //           onSaved: (val) {},
  //         ),
  //         Container(height: 28.0),
  //         UnderlineTextFormField(
  //           labelText: '기업계좌',
  //           hintText: '000000-000000',
  //           validator: (val) {
  //             return null;
  //           },
  //           onSaved: (val) {},
  //         ),
  //         Container(height: 40.0),
  //         PrimaryButton(
  //           label: '결제요청',
  //           onTap: () {
  //             setState(() {
  //               this.isTypeOne = false;
  //             });
  //           },
  //         ),
  //       ],
  //     ),
  //   );
  // }

  renderRadio({
    required bool isActive,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      height: 18.0,
      width: 18.0,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey,
          width: isActive ? 0 : 1.0,
        ),
        shape: BoxShape.circle,
        color: isActive ? theme.primaryColor : Colors.white,
      ),
      child: Center(
        child: Container(
          height: 8.0,
          width: 8.0,
          decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle,
          ),
        ),
      ),
    );
  }

  renderAccordion({
    required String title,
    required bool isOpened,
    required GestureTapCallback onTap,
    Widget? body,
  }) {
    return Column(
      children: [
        InkWell(
          onTap: onTap,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 16.0),
            child: Row(
              children: [
                renderRadio(isActive: isOpened),
                Container(width: 8.0),
                Text(
                  title,
                ),
              ],
            ),
          ),
        ),
        Divider(),
        if (body != null && isOpened) body,
      ],
    );
  }

  renderDeficitKeyValue({
    required String key,
    required String value,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          key,
          style: TextStyle(
            color: Colors.grey,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
        Text(
          value,
        ),
      ],
    );
  }

  renderAddAccount({bool isCard = false}) {
    final theme = ThemeFactory.of(context).theme;

    String title = '계좌추가';
    String content = '계좌는 최대 3개까지 등록할 수 있어요';

    if (isCard) {
      title = '카드추가';
      content = '카드는 최대 3개까지 등록할 수 있어요';
    }

    return GestureDetector(
      onTap: () async {
        if (isCard) {
          Get.toNamed('/payment/methods/register/bank');
        } else {
          Get.toNamed('/payment/methods/register/account_reg_pageList');
        }

        await Get.find<UserController>().getUserPayCardInfo(
          reset: true,
        );
      },
      child: DottedBorder(
        radius: Radius.circular(6.0),
        color: Color(0xFFE1E2E8),
        dashPattern: [6],
        strokeWidth: 2.0,
        child: Container(
          constraints: BoxConstraints(
            minWidth: 220.0,
          ),
          decoration: BoxDecoration(
            color: Color(0xFFF7F8F9),
            borderRadius: BorderRadius.circular(
              6.0,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 36.0,
                width: 36.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xFFCBCBCB),
                ),
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 7.0),
                child: Text(
                  title,
                  style: TextStyle(
                    fontSize: theme.fontSize12,
                    fontWeight: theme.heavyFontWeight,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 4.0),
                child: Text(
                  content,
                  style: TextStyle(
                    fontSize: theme.fontSize9,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// 가상계좌 || 신용카드 || 가족요청
  renderPaymentMethods({
    required bool isOpened,
    String type = '가상계좌',
  }) {
    return GetBuilder<UserController>(
      builder: (c) {
        final cards = c.paymentCardInfos
            .where(
              (element) => element.paymentTypeDesc == type,
            )
            .toList();

        String title = '내 계좌로 결제';

        if (type == '신용카드') {
          title = '내 카드로 결제';
        } else if (type == '가족요청') {
          title = '가족에게 요청하기';
        }

        return renderAccordion(
          onTap: () {
            setState(() {
              this.openedPayment = type;
              if (type == '가족요청') {
                this.selectedPaymentId = null;
              } else {
                this.selectedPaymentId = cards[0].paymentWayId;
              }
            });
          },
          title: title,
          isOpened: isOpened,
          body: type == '가족요청'
              ? null
              : SizedBox(
                  height: 150.0,
                  child: PaginationListView(
                    itemCount: cards.length > 2 ? cards.length : cards.length + 1,
                    scrollDirection: Axis.horizontal,
                    loadingWidget: Container(),
                    lastWidget: Container(),
                    itemBuilder: (_, index) {
                      // 마지막 인덱스일경우
                      if (index == cards.length && cards.length < 3) {
                        return renderAddAccount(isCard: type == '신용카드');
                      }

                      return Padding(
                        padding: EdgeInsets.only(right: 16.0),
                        child: BankAccountAndCreditCard(
                          onTap: () {
                            setState(() {
                              this.selectedPaymentId = cards[index].paymentWayId;
                            });
                          },
                          cardModel: UserPayCardModel(
                            cardInfo: cards[index],
                            isSelected: selectedPaymentId == cards[index].paymentWayId,
                          ),
                        ),
                      );
                    },
                    request: c.getUserPayCardInfo,
                    controller: c,
                  ),
                ),
        );
      },
    );
  }

  formatPrice(int price) {
    final formatter = NumberFormat('#,###');
    return formatter.format(price);
  }

  renderDeficitPaymentMethods() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '모자란 금액 결제하기',
                style: TextStyle(
                  fontWeight: theme.heavyFontWeight,
                ),
              ),
              // CupertinoSwitch(
              //   value: payRemainingAmount,
              //   activeColor: theme.primaryColor,
              //   onChanged: (val) {
              //     setState(() {
              //       this.payRemainingAmount = val;
              //     });
              //   },
              // ),
            ],
          ),
          Container(height: 24.0),
          renderPaymentMethods(
            type: '가상계좌',
            isOpened: this.openedPayment == '가상계좌',
          ),
          renderPaymentMethods(
            type: '신용카드',
            isOpened: this.openedPayment == '신용카드',
          ),
          GetBuilder<PaymentController>(
            builder: (c) {
              if (c.paymentStep != 2) {
                return renderPaymentMethods(
                  type: '가족요청',
                  isOpened: this.openedPayment == '가족요청',
                );
              } else {
                return Container();
              }
            },
          ),
        ],
      ),
    );
  }

  renderDeficit() {
    final theme = ThemeFactory.of(context).theme;

    final orangeStyle = TextStyle(
      fontSize: theme.fontSize20,
      color: theme.primaryOrangeColor,
      fontWeight: theme.heavyFontWeight,
    );

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: GetBuilder<PaymentController>(
        builder: (c) {
          if (c.amount == this.totalInput) {
            return Container();
          }

          String label = '모자란 금액';

          if (c.amount < this.totalInput) {
            label = '초과한 금액';
          }

          return GetBuilder<PaymentController>(builder: (c) {
            return Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      label,
                      style: TextStyle(
                        fontSize: theme.fontSize15,
                        color: Color(0xFF343435),
                        fontWeight: theme.mediumFontWeight,
                      ),
                    ),
                    Row(
                      children: [
                        GetBuilder<PaymentController>(builder: (c) {
                          return Text(
                            formatPrice(c.amount - totalInput),
                            style: orangeStyle,
                          );
                        }),
                        Container(width: 8.0),
                        Text(
                          '원',
                          style: orangeStyle.copyWith(
                            fontWeight: theme.primaryFontWeight,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                  child: Divider(),
                ),
                renderDeficitKeyValue(
                  key: '전체 금액',
                  value: c.amountDisplay + '원',
                ),
                Container(
                  height: 16.0,
                ),
                renderDeficitKeyValue(
                  key: '사용한 까까',
                  value: formatPrice(this.totalInput) + '원',
                ),
                Container(height: 16.0),
                // Row(
                //   children: [
                //     Text(
                //       '*모자란 금액이 생기면 추가로 결제요청을 할 수 있어요.',
                //       style: TextStyle(
                //         fontSize: theme.fontSize11,
                //         fontWeight: theme.heavyFontWeight,
                //         color: Color(0xFFA1A2D6),
                //       ),
                //     ),
                //   ],
                // ),
                // Container(height: 14.0),
              ],
            );
          });
        },
      ),
    );
  }

  onPayTap() async {
    final c = Get.find<PaymentController>();
    // if (checkTotalInput(c) == false) return;

    try {
      /// 가족요청이 아닌데 모자란 금액 결제 방법이 선택 안됐을때
      if (this.openedPayment != '가족요청' && this.totalInput != c.amount && this.selectedPaymentId == null) {
        Get.snackbar(
          '결제 실패',
          '모자란 금액을 결제할 카드 또는 계좌를 선택해주세요!',
        );
        return;
      }

      await c.updateSelfPayments(
        requestAmount: this.openedPayment == '가족요청' ? c.amount - this.totalInput : 0,
        // amount: this.openedPayment == '가족요청' ? 0 : c.amount! - this.totalInput,
        kkakkas: this
            .amounts
            .keys
            .where((key) => this.amounts[key] != null && this.amounts[key]! > 0)
            .map(
              (x) => PaymentStepsKkakka(
                approveAmount: this.amounts[x]!,
                kkakkaId: x,
              ),
            )
            .toList(),
        localCurrencies: [],
        userPayments: this.selectedPaymentId == null || c.amount == this.totalInput
            ? []
            : [
                PaymentStepsUserPaymentWay(
                  approveAmount: c.amount - this.totalInput,
                  userPaymentWayId: this.selectedPaymentId!,
                ),
              ],
      );

      if (this.openedPayment == '가족요청' && this.totalInput != c.amount) {
        Get.toNamed(
          '/payment/asking',
        );
      } else {
        final resp = await Get.toNamed(
          '/auth/verify/pin',
          arguments: PinScreenArgument(
            hasSkip: false,
            isRegister: false,
            // nextRoute: '/payment/done',
            isBack: true,
          ),
        );

        if (resp == 'OK') {
          setState(() {
            this.isLoading = true;
          });

          await c.makePaymentStepN();

          if (c.paymentStep == 0) {
            await c.requestPayment();
          }

          setState(() {
            this.isLoading = false;
          });

          Get.offAndToNamed('/payment/done');
        }
      }
    } on DioError catch (e) {
      setState(() {
        this.isLoading = false;
      });
      SnackbarUtils().parseDioErrorMessage(
        error: e,
        title: '결제 에러',
      );
    }

    // try {
    //   await c.requestPayment(
    //     kkakkas: this
    //         .amounts
    //         .keys
    //         .where((element) => this.amounts[element]! > 0)
    //         .map(
    //           (key) => PostPaymentRequestBodyKkakka(
    //               amount: this.amounts[key]!, kkakkaId: key),
    //         )
    //         .toList(),
    //   );
    //
    //   Get.toNamed(
    //     '/auth/verify/pin',
    //     arguments: PinScreenArgument(
    //       hasSkip: false,
    //       isRegister: false,
    //       nextRoute: '/payment/done',
    //     ),
    //   );
    // } on DioError catch (e) {
    //   Get.snackbar(
    //     '결제실패',
    //     e.response!.data['resultMsg'],
    //   );
    // }
  }

  renderDeficitAndButton() {
    return Expanded(
      child: GetBuilder<PaymentController>(
        builder: (c) {
          final bool isBtnActivated = true;
          // final bool isBtnActivated = !this.isLoading &&
          //     (this.payRemainingAmount ||
          //         (!this.payRemainingAmount && this.openedPayment == '가족요청') ||
          //         (this.totalInput == c.amount));
          return Column(
            children: [
              if (c.amount != null && this.totalInput < c.amount) Container(height: 24.0),
              renderDeficit(),
              Container(
                height: 5.0,
                color: Color(0xFFF3F4F6),
              ),
              if (this.totalInput < c.amount) renderDeficitPaymentMethods(),
              Expanded(
                child: Container(
                  color: Color(0xFFF3F4F6),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        color: Colors.white,
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                          child: PrimaryButton(
                            label: '결제요청',
                            onTap: isBtnActivated ? onPayTap : null,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  bool checkTotalInput(PaymentController controller) {
    if (controller.amount == totalInput) return true;

    if (controller.amount > totalInput) {
      Get.toNamed('/payment/asking');
    }

    if (controller.amount < totalInput) {
      Get.snackbar('결제실패', '금액이 초과하였습니다.');
    }

    return false;
  }

  List<Widget> renderTypeTwoFooters() {
    return [
      renderDeficitAndButton(),
    ];
  }

  // List<Widget> renderTypeOneFooters() {
  //   return [
  //     renderTotal(),
  //     Container(
  //       height: 7.0,
  //       color: Color(0xFFE8E7EC),
  //     ),
  //     renderPaymentMethod(),
  //   ];
  // }

  renderFooter() {
    return Padding(
      padding: EdgeInsets.only(bottom: 16.0),
      child: renderDeficitAndButton(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollLayout(
      topBar: BackTopBar(
        title: '까까로 결제',
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 37.0, bottom: 16.0),
        child: Column(
          children: [
            renderCard(),
            Expanded(
              child: Column(
                children: [
                  renderAmountInputs(),
                  renderDeficitAndButton(),
                  // renderFooter(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
