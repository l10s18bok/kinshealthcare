import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import 'package:kins_healthcare/components/bottom_sheet/btn_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/mykins/model/family_list_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

import '../../../components/card/payment_rate_user_card.dart';
import '../../spinor_test/mark/model/payment_user_model.dart';

/*
 작성일 : 2021-01-25
 작성자 : Mark,
 화면명 : HP_4001,
 경로 : /payment/share
 클래스 : ShareScreen,
 설명 : 까까 나누기 설정을 하기 위한 Screen
*/

class ShareList {
  List<PaymentUserModel>? modelList;
  bool? mult; 

  ShareList({
    this.modelList,
    this.mult,
  });
}

class ShareScreen extends StatefulWidget {
  @override
  ShareRateState createState() => ShareRateState();
}

class ShareRateState extends State<ShareScreen> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey<AnimatedListState>();
  int selectIndex = -1;
  late MykinsController controller;
  List<FocusNode> focusList = [FocusNode()];
  List<PaymentUserModel> modelList = [
    PaymentUserModel(name: 'HEADER', phoneNum: 'HEADER', userRelNo: -1)
  ];

  @override
  void initState() {
    super.initState();
    controller = Get.find<MykinsController>();
    _getDivision();
  }

  _getDivision() async {
    try {
      final list = await controller.getDivision();
      for (final item in list) {
        _insert(item.paymentUserModel);
      }
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: NestedScrollLayout(
        topBar: renderTopBar(),
        body: renderMain(),
      ),
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
    );
  }

  Widget renderTopBar() {
    return BackTopBar(title: '나누기');
  }

  Widget renderMain() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          Expanded(child: renderAnimatedListView()),
          renderSubmitBtn(),
        ],
      ),
    );
  }

  renderAnimatedListView() {
    return AnimatedList(
      key: _listKey,
      initialItemCount: modelList.length,
      itemBuilder: _listItemBuilder,
    );
  }

  renderListItem(
    int index, {
    required Animation<double> animation,
    bool isRemove = false,
  }) {
    if (index == 0) return renderFirstItem();
    return renderItem(index, animation: animation, isRemove: isRemove);
  }

  Widget renderFirstItem() {
    return Column(
      children: [
        SizedBox(height: 20),
        renderTitle(),
        SizedBox(height: 30),
        modelList.length == 1 ? renderNonCard() : Container(),
      ],
    );
  }

  Widget renderNonCard() {
    return Padding(
      padding: EdgeInsets.only(bottom: 10, left: 16, right: 16, top: 0),
      child: NonKkakkaCard(title: '저장된 가족이 없습니다.\n까까를 나눌 가족을 저장해보세요.'),
    );
  }

  Widget renderItem(
    int index, {
    required Animation<double> animation,
    bool isRemove = false,
  }) {
    final model = modelList[index];
    final isSelect = selectIndex == index;

    return InkWell(
      child: PaymentRateUserCard(
        animation: animation,
        model: model,
        isSelect: isSelect,
        onBtnTap: () => isRemove ? null : _listItemBtnOnTap(index),
        focus: focusList[index],
        onFocus: () {
          selectIndex = index;
          setState(() {});
        },
        onChanged: (value) => _percentOnChanged(model, value),
      ),
      onTap: () {
        if (isRemove) return;
        FocusScope.of(context).requestFocus(new FocusNode());
        selectIndex = index;
        setState(() {});
      },
    );
  }

  _percentOnChanged(PaymentUserModel model, String value) {
    model.percentage = value;
    model.isError = false;

    if (value.isEmpty) return setState(() {});

    if (_checkMax() < 0)
      model.isError = true;
    else {
      modelList = modelList.map((item) {
        item.isError = false;
        return item;
      }).toList();
    }

    setState(() {});
  }

  double _checkMax() {
    double nowPercentage = 0.0;
    for (final item in modelList) {
      if (item.percentage == null) continue;
      final percentage = double.parse(item.percentage!);
      nowPercentage += percentage;
    }

    return 100.0 - nowPercentage;
  }

  renderTitle() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          '까까를\n나눌 가족을 선택해주세요',
          style: TextStyle(
            color: kinsBlack30,
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize17,
          ),
        ),
        InkWell(
          child: Container(
            padding: EdgeInsets.all(4),
            width: 40,
            height: 40,
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_plus_main.svg',
              width: 32,
              height: 32,
            ),
          ),
          onTap: _plusBtnOnTap,
        )
      ],
    );
  }

  _plusBtnOnTap() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    ShareList shareList = ShareList(modelList: modelList, mult: true);
    final list = await Get.toNamed(
      '/payment/share-target',
      arguments: shareList,
    );

    if (list is List<FamilyListModel>) {
      for (final item in list) {
        _insert(item.paymentUserModel);
      }
    }
  }

  _listItemBtnOnTap(int index) {
    if (index == selectIndex) selectIndex = -1;
    _remove(index);

    setState(() {});
  }

  Widget _listItemBuilder(BuildContext context, int index, Animation<double> animation) {
    return renderListItem(index, animation: animation);
  }

  void _insert(PaymentUserModel model) {
    int index = modelList.length;
    modelList.add(model);
    focusList.add(FocusNode());

    _listKey.currentState!.insertItem(index, duration: Duration(milliseconds: 300));
  }

  void _remove(int index) {
    final keyState = _listKey.currentState;
    if (keyState == null) return;

    keyState.removeItem(
      index,
      (context, animation) {
        final removeItem = _buildRemovedItem(context, index, animation);
        modelList.removeAt(index);
        focusList.removeAt(index);

        return removeItem;
      },
      duration: Duration(milliseconds: 500),
    );
  }

  Widget _buildRemovedItem(
    BuildContext context,
    int index,
    Animation<double> animation,
  ) {
    return renderListItem(index, animation: animation, isRemove: true);
  }

  Widget renderSubmitBtn() {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      height: 55,
      child: PrimaryButton(
        label: '저장',
        padding: EdgeInsets.all(0.0),
        onTap: _updateDivision,
      ),
    );
  }

  _updateDivision() async {
    final list = modelList.map((item) => item.divisionBody).toList();
    list.removeWhere((item) => item.relUserNo == -1);

    try {
      FocusScope.of(context).requestFocus(new FocusNode());

      final model = await controller.updateDivision(list);
      final sheet = BtnBottomSheet(
        title: model.resultMsg ?? '완료되었습니다.',
        isOneBtn: true,
      );

      await Get.bottomSheet(sheet);
      Get.back();
    } on DioError catch (e) {
      final data = e.response!.data;

      if (data is Map) {
        final resultCode = data["resultCode"];
        final resultMsg = data["resultMsg"];

        final sheet = ErrorBottomSheet(title: resultCode, content: resultMsg);
        Get.bottomSheet(sheet);
      } else {
        final msg = data.toString();

        final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
        Get.bottomSheet(sheet);
      }
    }
  }
}
