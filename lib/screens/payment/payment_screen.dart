import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-11
 * 작성자 : JC
 * 화면명 : HP_0001
 * 주요기능 : 결제 스크린
 */
class PaymentScreen extends StatefulWidget {
  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  renderTitle() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.only(top: 16.0),
      child: Text(
        '결제',
        style: TextStyle(
          fontSize: theme.fontSize16,
          fontWeight: theme.heavyFontWeight,
        ),
      ),
    );
  }

  renderTopCard({
    required String title,
    required GestureTapCallback? onTap,
    required String svgName,
    Color? fontColor,
    Color? bgColor,
    bool? isTextLeft,
  }) {
    fontColor ??= Color(0xFF434343);
    bgColor ??= Color(0xFFd4d5ec);
    isTextLeft ??= true;

    final theme = ThemeFactory.of(context).theme;

    return Material(
      color: bgColor,
      borderRadius: BorderRadius.circular(
        6.0,
      ),
      child: InkWell(
        onTap: onTap,
        child: Row(
          children: [
            Expanded(
              child: Stack(
                children: [
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 64.0, vertical: 36.0),
                    child: Row(
                      mainAxisAlignment: isTextLeft
                          ? MainAxisAlignment.start
                          : MainAxisAlignment.end,
                      children: [
                        Text(
                          title,
                          style: TextStyle(
                            fontSize: theme.fontSize16,
                            color: fontColor,
                            fontWeight: theme.heavyFontWeight,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned.fill(
                    left: isTextLeft ? null : 25.0,
                    right: isTextLeft ? 25.0 : null,
                    child: SvgPicture.asset(
                      'assets/svgs/img/$svgName',
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  renderTopCards() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          renderTopCard(
            title: '내가 결제하기',
            onTap: () {
              Get.toNamed('/payment/qr');
            },
            svgName: 'payment_my_payment.svg',
          ),
          Container(height: 10.0),
          renderTopCard(
            title: '요청받은 결제승인',
            onTap: () {
              Get.toNamed('/payment/requests');
            },
            bgColor: Color(0xFF5A5B8A),
            fontColor: Color(0xFFFFFFFF),
            isTextLeft: false,
            svgName: 'payment_received_payment.svg',
          ),
        ],
      ),
    );
  }

  renderIconButton({
    required String icName,
    required String label,
    required GestureTapCallback onTap,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return Material(
      borderRadius: BorderRadius.circular(6.0),
      color: Colors.white,
      child: InkWell(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6.0),
            border: Border.all(
              width: 1.0,
              color: Color(0xFFd7d8ee),
            ),
          ),
          child: AspectRatio(
            aspectRatio: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      'assets/svgs/ic/$icName',
                    ),
                  ],
                ),
                Container(height: 16.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      label,
                      style: TextStyle(
                        fontSize: theme.fontSize12,
                        color: Color(0xFF1d1f75),
                        fontWeight: theme.heavyFontWeight,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  renderBottomButtons() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: renderIconButton(
                  icName: 'ic_card_payment.svg',
                  label: '결제수단 관리',
                  onTap: () {
                    Get.toNamed('/payment/methods');
                  },
                ),
              ),
              Container(width: 7.0),
              Expanded(
                child: renderIconButton(
                  icName: 'ic_share_payment.svg',
                  label: '나누기 설정',
                  onTap: () => Get.toNamed('/payment/share'),
                ),
              ),
              Container(width: 7.0),
              Expanded(
                child: renderIconButton(
                  icName: 'ic_pay_payment.svg',
                  label: '송금하기',
                  onTap: () {
                    Get.snackbar('준비 중..', '송금하기 기능준비중입니다.');
                  },
                ),
              ),
            ],
          ),
          Container(height: 7.0),
          Row(
            children: [
              Expanded(
                child: renderIconButton(
                  icName: 'ic_hospital_payment.svg',
                  label: '응급병원관리',
                  onTap: () => Get.toNamed('/payment/emergency'),
                ),
              ),
              Container(width: 7.0),
              Expanded(
                child: renderIconButton(
                  icName: 'ic_list_payment.svg',
                  label: '결제목록',
                  onTap: () {
                    Get.toNamed('/payment/list');
                  },
                ),
              ),
              Container(width: 7.0),
              Spacer(),
            ],
          ),
        ],
      ),
    );
  }

  renderAdvert() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(
          left: 16.0,
          right: 16.0,
          bottom: 16.0,
          top: 16.0,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              children: [
                Expanded(
                  child: Image.asset(
                    'assets/svgs/img/img_banner.jpg',
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: LayoutBuilder(builder: (context, constraints) {
        return SingleChildScrollView(
          child: ConstrainedBox(
            constraints: constraints.copyWith(
              minHeight: constraints.maxHeight,
              maxHeight: double.infinity,
            ),
            child: IntrinsicHeight(
              child: Column(
                children: [
                  renderTitle(),
                  Container(height: 15.0),
                  renderTopCards(),
                  Container(height: 16.0),
                  Container(height: 6.0, color: Color(0xFFEFEFF4)),
                  Container(height: 16.0),
                  renderBottomButtons(),
                  renderAdvert(),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
