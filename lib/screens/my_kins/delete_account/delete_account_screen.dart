import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-28
 * 작성자 : JC
 * 화면명 : HY_1003
 * 주요기능 : 계정 삭제
 */
class DeleteAccountScreen extends StatefulWidget {
  @override
  _DeleteAccountScreenState createState() => _DeleteAccountScreenState();
}

class _DeleteAccountScreenState extends State<DeleteAccountScreen> {
  bool agree = false;

  renderTopLogo() {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      children: [
        SvgPicture.asset(
          'assets/svgs/ic/ic_logo_main.svg',
          color: theme.primaryColor,
        ),
        Container(height: 19.0),
        Text(
          '정말 탈퇴하실건가요?',
          style: TextStyle(
            fontSize: theme.fontSize14,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
        Container(height: 5.0),
        Text(
          '서비스 탈퇴전 확인해주세요.',
          style: TextStyle(
            fontSize: theme.fontSize15,
            color: Color(0xFF454545),
          ),
        ),
        Container(height: 61.0),
      ],
    );
  }

  renderCheckbox() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      width: 26.0,
      height: 26.0,
      decoration: BoxDecoration(
        color: agree ? theme.primaryColor : Color(0xFFCFCFE2),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Padding(
        padding: EdgeInsets.all(6.0),
        child: SvgPicture.asset(
          'assets/svgs/ic/ic_checkbox_unselect.svg',
        ),
      ),
    );
  }

  renderAgree() {
    final theme = ThemeFactory.of(context).theme;

    return Expanded(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            InkWell(
              onTap: () {
                setState(() {
                  this.agree = !this.agree;
                });
              },
              child: Row(
                children: [
                  renderCheckbox(),
                  Container(width: 16.0),
                  Text(
                    '위 모든 내용에 동의 합니다.',
                    style: TextStyle(
                      fontSize: theme.fontSize15,
                      color: Color(0xFF636363),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  renderMessage() {
    final theme = ThemeFactory.of(context).theme;

    final thickStyle = TextStyle(
      fontSize: theme.fontSize13,
      color: Color(0xFF454545),
      fontWeight: theme.heavyFontWeight,
    );

    final normStyle = thickStyle.copyWith(
      fontWeight: theme.primaryFontWeight,
    );

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Color(0xFFEFF0F5),
              borderRadius: BorderRadius.circular(6.0),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 24.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Flexible(
                        child: Text(
                          '∙아이디 재사용 및 복구 불가',
                          style: thickStyle,
                        ),
                      ),
                    ],
                  ),
                  Container(height: 12.0),
                  Row(
                    children: [
                      Flexible(
                        child: Text(
                          '''탈퇴를 진행하시면 사용하고 계신 아이디의 복구 및 재사용이 불가하오니 신중하게 선택해주세요.''',
                          style: normStyle,
                        ),
                      ),
                    ],
                  ),
                  Container(height: 26.0),
                  Row(
                    children: [
                      Flexible(
                        child: Text(
                          '∙회원정보/ 결제 서비스 정보/ 이용기록 삭제',
                          style: thickStyle,
                        ),
                      ),
                    ],
                  ),
                  Container(height: 12.0),
                  Row(
                    children: [
                      Flexible(
                        child: Text(
                          '''회원정보 및 카드&계좌 등의 결제정보, 서비스 이용기록은 모두 삭제되며, 삭제된 데이터는 복구되지 않습니다''',
                          style: normStyle,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  renderButton() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          PrimaryButton(
            onTap: () async {
              if (!this.agree) {
                Get.snackbar('동의 필수', '동의를 눌러주세요!');
                return;
              }

              await Get.find<UserController>().dropUser();

              Get.offAllNamed('/auth');
            },
            label: '탈퇴하기',
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollLayout(
      topBar: BackTopBar(
        title: '회원탈퇴',
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 40.0),
        child: Column(
          children: [
            // Container(height: 40.0),
            renderTopLogo(),
            // Container(height: 61.0),
            renderMessage(),
            renderAgree(),
            renderButton(),
          ],
        ),
      ),
    );

    // return TitleLayout(
    //   title: '회원탈퇴',
    //   body: Column(
    //     children: [
    //       renderTopLogo(),
    //       Container(height: 61.0),
    //       renderMessage(),
    //       renderAgree(),
    //       renderButton(),
    //     ],
    //   ),
    // );
  }
}
