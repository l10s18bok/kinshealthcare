import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/text_field/text_field.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/cert_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/services/cert/model/user_certify_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';

/*
 * 작성일 : 2021-01-28
 * 작성자 : JC
 * 화면명 : HY_1001
 * 주요기능 : 본인 확인
 */
class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final formKey = GlobalKey<FormState>();
  String pass = '';

  renderTextField() {
    return PasswordTextField(
      onSaved: (String? value) {
        setState(() {
          pass = value ?? '';
        });
      },
    );
  }

  renderForgottenPassword() {
    final theme = ThemeFactory.of(context).theme;

    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {},
              child: Text(
                '혹시 비밀번호를 잊으셨나요?',
                style: TextStyle(
                  fontSize: theme.fontSize12,
                  color: Color(0xFF9A9A9A),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  renderFooter() {
    return Padding(
      padding: EdgeInsets.only(bottom: 16.0),
      child: PrimaryButton(
        onTap: () async {
          try {
            if (this.formKey.currentState!.validate()) {
              this.formKey.currentState!.save();

              await Get.find<CertController>().userCertify(
                body: UserCertifyBody(
                  pw: this.pass,
                ),
              );

              Get.offAndToNamed('/my-kins/profile');
            }
          } on DioError catch (e) {
            SnackbarUtils().parseDioErrorMessage(
              error: e,
              title: '비밀번호 에러',
            );
          }
        },
        label: '다음',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(
        title: '본인확인',
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Form(
          key: this.formKey,
          child: Column(
            children: [
              Container(height: 40.0),
              renderTextField(),
              Container(height: 36.0),
              Expanded(
                child: renderForgottenPassword(),
              ),
              renderFooter(),
            ],
          ),
        ),
      ),
    );
    // return TitleLayout(
    //   title: '본인확인',
    //   body: Padding(
    //     padding: EdgeInsets.symmetric(horizontal: 16.0),
    //     child: Form(
    //       key: this.formKey,
    //       child: Column(
    //         children: [
    //           renderTextField(),
    //           Container(height: 36.0),
    //           Expanded(
    //             child: renderForgottenPassword(),
    //           ),
    //           renderFooter(),
    //         ],
    //       ),
    //     ),
    //   ),
    // );
  }
}
