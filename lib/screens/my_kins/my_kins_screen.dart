import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/simple_information_bottom_sheet.dart';
import 'package:kins_healthcare/controllers/auth_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/services/user/model/user_info_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-01-28
 * 작성자 : JC
 * 화면명 : HY_0001
 * 주요기능 : 마이킨즈
 */
class MyKinsScreen extends StatefulWidget {
  @override
  _MyKinsScreenState createState() => _MyKinsScreenState();
}

class _MyKinsScreenState extends State<MyKinsScreen> {
  renderTitle() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        children: [
          Text(
            '마이킨즈',
            style: TextStyle(
              fontSize: theme.fontSize22,
              fontWeight: theme.heavyFontWeight,
            ),
          ),
        ],
      ),
    );
  }

  renderButton({
    required String svgName,
    required String label,
    required GestureTapCallback onTap,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 12.0),
        child: Row(
          children: [
            SvgPicture.asset(
              'assets/svgs/ic/$svgName.svg',
            ),
            Padding(
              padding: EdgeInsets.only(left: 24.0),
              child: Text(
                label,
                style: TextStyle(
                  fontSize: theme.fontSize15,
                  color: Color(0xFF636363),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  renderButtons() {
    return Column(
      children: [
        renderButton(
          onTap: () {
            Get.toNamed('/my-kins/auth');
          },
          svgName: 'ic_profile_setting',
          label: '내 정보 관리',
        ),
        renderButton(
          onTap: () {
            Get.toNamed('/my-kins/family');
          },
          svgName: 'ic_family_setting',
          label: '나의 가족',
        ),
        renderButton(
          onTap: () {
            Get.toNamed('/my-kins/anniversary');
          },
          svgName: 'ic_anniversary_setting',
          label: '기념일 관리',
        ),
        renderButton(
          onTap: () {
            Get.toNamed('/my-kins/age');
          },
          svgName: 'ic_tree_setting',
          label: '킨즈 나이테',
        ),
        renderDivider(),
        renderButton(
          onTap: () => Get.toNamed('/my-kins/certification-method'),
          svgName: 'ic_message_setting',
          label: '인증수단 관리',
        ),
        renderButton(
          onTap: () => Get.toNamed('/my-kins/notification-setting'),
          svgName: 'ic_alarm_setting',
          label: '알림 설정',
        ),
        renderButton(
          onTap: () => Get.toNamed('/my-kins/notice'),
          svgName: 'ic_notice_setting',
          label: '공지사항 / 이용안내',
        ),
        renderButton(
          onTap: () => Get.toNamed('/my-kins/faq'),
          svgName: 'ic_faq_setting',
          label: '1:1 문의 / FAQ',
        ),
        renderButton(
          onTap: () {},
          svgName: 'ic_advertise_setting',
          label: '광고문의',
        ),
      ],
    );
  }

  renderDivider() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Container(
        height: 1.0,
        color: Color(0xFFE4E4E4),
      ),
    );
  }

  renderFooter() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'ver 1.0.0',
            style: TextStyle(
              fontSize: theme.fontSize12,
              color: Color(0xFF6E6E6E),
              fontWeight: theme.heavyFontWeight,
            ),
          ),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () async {
              await Get.find<AuthController>().logoutUser();

              Get.offAllNamed('/auth');
            },
            child: Text(
              '로그아웃',
              style: TextStyle(
                fontSize: theme.fontSize15,
                color: Color(0xFF8A8A8A),
              ),
            ),
          ),
        ],
      ),
    );
  }

  renderProfileCard() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: GetBuilder<UserController>(builder: (c) {
        final UserInfoResponse? ui = c.userInfo;

        if (ui == null) return Container();

        return GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            Get.toNamed('/my-kins/auth');
          },
          child: Row(
            children: [
              CircleAvatar(
                backgroundImage: NetworkImage(ui.image),
                radius: 25.0,
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: IntrinsicWidth(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          ui.lastname + ui.firstname,
                          style: TextStyle(
                            fontSize: theme.fontSize14,
                          ),
                        ),
                        Container(height: 3.0),
                        Text(
                          ui.phone,
                          style: TextStyle(
                            fontSize: theme.fontSize11,
                            color: Color(0xFF6D6D6D),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SvgPicture.asset(
                'assets/svgs/ic/ic_arrow_right.svg',
              ),
            ],
          ),
        );
      }),
    );
  }

  renderGreyDivider() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 30.0),
      child: Container(
        height: 5.0,
        color: Color(0xFFE8E7EC),
      ),
    );
  }

  renderMiddleButton({
    required String ic,
    required String label,
    GestureTapCallback? onTap,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Column(
        children: [
          SvgPicture.asset(
            ic,
          ),
          Container(height: 8.0),
          Text(
            label,
            style: TextStyle(
              fontSize: theme.fontSize12,
            ),
          ),
        ],
      ),
    );
  }

  renderMiddleButtons() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          renderMiddleButton(
            ic: 'assets/svgs/ic/ic_family_setting.svg',
            label: '나의 가족',
            onTap: () {
              Get.toNamed('/my-kins/family');
            },
          ),
          renderMiddleButton(
            ic: 'assets/svgs/ic/ic_anniversary_setting.svg',
            label: '기념일 관리',
            onTap: () {
              Get.toNamed('/my-kins/anniversary');
            },
          ),
          renderMiddleButton(
            ic: 'assets/svgs/ic/ic_tree_setting.svg',
            label: '킨즈 나이테',
            onTap: () {
              Get.toNamed('/my-kins/age');
            },
          ),
        ],
      ),
    );
  }

  renderMiddleDivider() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: Divider(
        color: Color(0xFFE4E4E4),
      ),
    );
  }

  renderBottomButton({
    required String ic,
    required String label,
    GestureTapCallback? onTap,
  }) {
    final theme = ThemeFactory.of(context).theme;

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 16.0),
        child: Row(
          children: [
            SvgPicture.asset(
              ic,
              width: 28.0,
              height: 28.0,
            ),
            Container(width: 24.0),
            Text(
              label,
              style: TextStyle(
                fontSize: theme.fontSize14,
                color: Color(0xFF444444),
              ),
            ),
          ],
        ),
      ),
    );
  }

  renderBottomButtons() {
    return Column(
      children: [
        renderBottomButton(
          ic: 'assets/svgs/ic/ic_message_setting.svg',
          label: '인증수단 관리',
          onTap: () {
            Get.toNamed('/my-kins/certification-method');
          },
        ),
        renderBottomButton(
          ic: 'assets/svgs/ic/ic_alarm_setting.svg',
          label: '알림 설정',
          onTap: () {
            Get.toNamed('/my-kins/notification-setting');
          },
        ),
        renderBottomButton(
          ic: 'assets/svgs/ic/ic_notice_setting.svg',
          label: '공지사항 / 이용안내',
          onTap: () {
            Get.toNamed('/my-kins/notice');
          },
        ),
        renderBottomButton(
          ic: 'assets/svgs/ic/ic_faq_setting.svg',
          label: '1:1 문의 / FAQ',
          onTap: () {
            Get.toNamed('/my-kins/faq');
          },
        ),
        renderBottomButton(
          ic: 'assets/svgs/ic/ic_advertise_setting.svg',
          label: '광고문의',
          onTap: () {
            final sheet = SimpleInformationBottomSheet(message: '준비중 입니다.');
            Get.bottomSheet<String>(sheet);
          },
        ),
        renderBottomButton(
          ic: 'assets/svgs/ic/ic_child_setting.svg',
          label: '자녀회원 관리',
          onTap: () {
            Get.toNamed('/my-kins/family-children');
          },
        ),
        renderBottomButton(
          ic: 'assets/svgs/ic/ic_mykins_drop_user.svg',
          label: '회원 탈퇴',
          onTap: () {
            Get.toNamed('/my-kins/delete-account');
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    // 2021.05.13 Andy 레이아웃 약간 조정(로그아웃을 리스트뷰에 포함)
    return ListView(
      children: [
        Container(height: 30.0),
        renderTitle(),
        Container(height: 21.0),
        renderProfileCard(),
        renderGreyDivider(),
        renderMiddleButtons(),
        renderMiddleDivider(),
        renderBottomButtons(),
        renderFooter(),
      ],
    );
  }

  @override
  void initState() {
    super.initState();

    Get.find<UserController>().getUserInfo();
  }
}
