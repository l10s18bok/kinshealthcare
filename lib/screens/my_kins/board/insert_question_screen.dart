import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/text_field/out_line_text_field_scrollbar.dart';
import 'package:kins_healthcare/components/text_field/underline_text_form_field.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/ok_bottom_sheet.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

import '../../../components/top_bar/back_top_bar.dart';

/*
 작성일 : 2021-01-22
 작성자 : Mark,
 화면명 : HY_6002,
 경로 : /my-kins/faq/insert_question
 클래스 : InsertQuestionScreen,
 설명 : 1:1 문의를 입력하기 위한 Screen
*/
/**
 수정일 : 2021-04-15
 수정자 : Daniel,
 화면명 : HY_6002,
 설명 : 기획 변경으로 인한 내용 및 화면 수정
*/

class InsertQuestionScreen extends StatefulWidget {
  @override
  InsertQuestionState createState() => InsertQuestionState();
}

class InsertQuestionState extends State<InsertQuestionScreen> {
  int selectIndex = 0;
  final nameTec = TextEditingController(), titleTec = TextEditingController();
  final contentTec = TextEditingController(),
      passwordTec = TextEditingController();

  @override
  void dispose() {
    nameTec.dispose();
    titleTec.dispose();
    contentTec.dispose();
    passwordTec.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: CustomScrollLayout(
        topBar: renderTopBar(),
        body: renderMain(),
      ),
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
    );
  }

  Widget renderTopBar() {
    return BackTopBar(title: '1:1 문의');
  }

  Widget renderMain() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          SizedBox(height: 30),
          Expanded(
            child: renderInputContent(),
          ),
        ],
      ),
    );
  }

  renderInputContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...renderTextField('제목', '제목을 적어주세요. (최대 50자)', titleTec),
        SizedBox(height: 10),
        ...renderTextFieldContent('문의내용', '내용을 적어주세요. (최대 400자)', contentTec),
        Expanded(
          child: Container(
            alignment: Alignment.bottomCenter,
            child: renderSubmitBtn(),
          ),
        ),
      ],
    );
  }

  renderTextField(
    String title,
    String hint,
    TextEditingController tec, {
    bool obscure = false,
  }) {
    return [
      renderTitleText(title),
      Container(
        width: double.infinity,
        alignment: Alignment.center,
        child: UnderlineTextFormField(
          controller: tec,
          hintText: hint,
          validator: (val) {
            return null;
          },
          onSaved: (val) {},
          maxLengthCheck: true,
        ),
      ),
    ];
  }

  renderTextFieldContent(String title, String hint, TextEditingController tec) {
    return [
      renderTitleText(title),
      SizedBox(height: 12),
      OutLineTextFieldScrollbar(
        controller: tec,
        hint: hint,
        height: 444,
        outLineColor: kinsBlack87,
        textColor: kinsBlack,
        inputType: TextInputType.multiline,
        maxLine: 20,
        maxLength: 400,
        hintColor: kinsGrayB6,
        fontSize: ThemeFactory.of(context).theme.fontSize15,
        fontWeight: FontWeight.w400,
        counterText: '',
        radius: 6.0,
      ),
    ];
  }

  renderTitleText(String title) {
    return Text(
      title,
      style: TextStyle(
        color: kinsBlack,
        fontWeight: FontWeight.w700,
        fontSize: ThemeFactory.of(context).theme.fontSize13,
      ),
    );
  }

  Widget renderSubmitBtn() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      height: 60,
      child: PrimaryButton(
        label: '다음',
        onTap: _submit,
      ),
    );
  }

  void _submit() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (_dataCheck() == false) return;

    try {
      final data = await Get.find<MykinsController>().postInsertInquiry(
        titleTec.text,
        nameTec.text,
        contentTec.text,
        passwordTec.text,
        'Y',
      );

      await Get.find<MykinsController>().postInquiryList();

      String title;
      String msg;
      if (data.resultCode == 'SUCCESS') {
        title = '완료';
        msg = '문의 입력이 완료되었습니다.';
      } else {
        title = '실패';
        msg = '문의하기에 실패하였습니다. resultCode:FAIL';
      }

      final sheet = OkBottomSheet(title: title, content: msg);
      await Get.bottomSheet(sheet);
      Get.back();
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  bool _dataCheck() {
    final title = titleTec.text;
    final content = contentTec.text;

    String? message;

    if (title.isEmpty) {
      message = '제목을 입력해 주세요.';
    } else if (content.isEmpty) {
      message = '내용을 입력해 주세요.';
    }

    if (message == null) return true;

    Get.snackbar('정보 미입력', message, backgroundColor: Colors.white);
    return false;
  }
}
