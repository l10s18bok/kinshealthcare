import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/card/board_content.dart';
import 'package:kins_healthcare/controllers/comm_controller.dart';
import 'package:kins_healthcare/enums/screen_enums.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/local_board_model.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import '../../../components/list_tile/expanded_tile.dart';

/*
 작성일 : 2021-01-22
 작성자 : Mark,
 화면명 : HY_5001/HY_5002/HY_6001/HY_7001,
 경로 : ,
 클래스 : Board,
 설명 : 게시판이 2개 붙어 있는 형태의 List에 해당 하는 부분
 공지사항, 1:1 문의, FAQ, 이용정보 4개의 화면에서 공유하기 위해서 만들어진 Widget
*/

class Board extends StatefulWidget {
  final BoardType type;
  Board({required this.type});

  @override
  _BoardState createState() => _BoardState();
}

class _BoardState extends State<Board> {
  late CommController commController;
  List<LocalBoardModel> modelList = [];

  @override
  void initState() {
    super.initState();
    commController = Get.find<CommController>();
    _setData();
  }

  _setData() async {
    switch (widget.type) {
      case BoardType.NOTICE:
        _setBoardModel('notice');
        break;
      case BoardType.FAQ:
        _setBoardModel('faq');
        break;
      case BoardType.INFO:
        _setBoardModel('information');
        break;
      default:
    }
  }

  _setBoardModel(String id) async {
    try {
      final values = await commController.postBoardList(id);
      modelList = values.map((item) => item.localBoardModel).toList();

      _setStateEndAnimation();
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (modelList.length == 0) {
      return renderProgress();
    }

    return ListView.builder(
      itemCount: modelList.length,
      itemBuilder: (BuildContext context, int index) {
        final model = modelList[index];
        if (index == 0) return renderFirstListItem(model);
        return renderListItem(model);
      },
    );
  }

  Widget renderProgress() {
    return Column(
      children: [
        SizedBox(height: 140),
        CircularProgressIndicator(),
      ],
    );
  }

  Widget renderFirstListItem(LocalBoardModel model) {
    return Column(
      children: [
        SizedBox(height: 27),
        renderListItem(model),
      ],
    );
  }

  Widget renderListItem(LocalBoardModel model) {
    if (widget.type == BoardType.NOTICE) {
      return InkWell(
        child: renderNoticeItem(model),
        onTap: () {
          Get.toNamed('/my-kins/notice-detail/${model.ids}');
        },
      );
    }

    return ExpansionTileCS(
      title: renderTitleRow(model),
      iconColor: kinsGrayB9,
      children: [
        renderTitleContent(model),
      ],
    );
  }

  Widget renderTitleRow(LocalBoardModel model) {
    final title =
        model.answer == null ? model.title : '${model.title}\n${model.content}';

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 16),
          Text(
            title,
            style: TextStyle(
              color: kinsBlack44,
              fontWeight: FontWeight.w500,
              fontSize: ThemeFactory.of(context).theme.fontSize14,
            ),
          ),
          SizedBox(height: 8),
          Text(
            model.regDate,
            style: TextStyle(
              color: kinsGrayF7,
              fontWeight: FontWeight.w500,
              fontSize: ThemeFactory.of(context).theme.fontSize10,
            ),
          ),
          SizedBox(height: 16),
        ],
      ),
    );
  }

  Widget renderTitleContent(LocalBoardModel model) {
    return BoardContent(type: widget.type, model: model);
  }

  Widget renderNoticeItem(LocalBoardModel model) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        renderTitleRow(model),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Container(
            height: 1,
            width: double.infinity,
            color: kinsGrayDD,
          ),
        ),
      ],
    );
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }
}
