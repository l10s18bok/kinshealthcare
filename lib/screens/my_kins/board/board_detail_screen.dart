import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/controllers/comm_controller.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/ok_bottom_sheet.dart';
import 'package:kins_healthcare/services/comm/model/board_one_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-02-16
 작성자 : Mark,
 화면명 : (화면명),
 경로 : /my-kins/notice-detail/:id
 클래스 : BoardDetailScreen,
 설명 : 공지사항의 내용을 보여주기 위한 Screen
*/

/// {@category Screen}
/// {@subCategory test subcategory}
class BoardDetailScreen extends StatefulWidget {
  @override
  BoardDetailState createState() => BoardDetailState();
}

class BoardDetailState extends State<BoardDetailScreen> {
  BoardOneModel? contentModel;
  int id = 0;

  @override
  void initState() {
    super.initState();
    final idString = Get.parameters['id'];
    if (idString != null) id = int.parse(idString);

    _getNoticeOne();
  }

  _getNoticeOne() async {
    try {
      final result = await Get.find<CommController>().postBoardOne('noticeOne', id);
      contentModel = result.data.values;

      _setStateEndAnimation();
    } on DioError catch (e) {
      final msg = e.response!.data;
      final sheet = OkBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Column(
        children: [
          renderTopBar(),
          Expanded(child: renderMain()),
        ],
      ),
    );
  }

  Widget renderMain() {
    final model = contentModel;
    if (model == null) return renderIndicator();

    return SingleChildScrollView(
      child: Column(
        children: [
          renderTitle(model),
          SizedBox(height: 36),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            alignment: Alignment.topLeft,
            width: double.infinity,
            child: Text(
              model.content ?? '',
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: ThemeFactory.of(context).theme.fontSize14,
                color: kinsBlack87,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget renderTopBar() {
    return Container(
      height: 63,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: kinsGrayEE,
            width: 1,
          ),
        ),
      ),
      child: Stack(
        children: [
          Positioned(left: 16, top: 0, bottom: 0, child: renderBackBtn()),
          Positioned.fill(child: renderTopTitle('공지 상세보기')),
        ],
      ),
    );
  }

  renderTopTitle(String? title) {
    if (title == null) return Container();

    return Align(
      alignment: Alignment.center,
      child: Text(
        title,
        style: TextStyle(
          color: kinsBlack,
          fontWeight: FontWeight.w700,
          fontSize: ThemeFactory.of(context).theme.fontSize15,
        ),
      ),
    );
  }

  renderBackBtn() {
    return InkWell(
      child: Container(
        alignment: Alignment.centerLeft,
        height: 44,
        width: 40,
        child: SvgPicture.asset(
          'assets/svgs/ic/ic_arrowback_common.svg',
          width: 24,
          height: 24,
          color: kinsGrayA3,
        ),
      ),
      onTap: () => Get.back(),
    );
  }

  renderTitle(BoardOneModel model) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        renderText(model),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Container(
            height: 1,
            width: double.infinity,
            color: kinsGrayDD,
          ),
        ),
      ],
    );
  }

  renderText(BoardOneModel model) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 16),
          Text(
            model.title ?? '',
            style: TextStyle(
              color: kinsBlack44,
              fontWeight: FontWeight.w500,
              fontSize: ThemeFactory.of(context).theme.fontSize14,
            ),
          ),
          SizedBox(height: 8),
          Text(
            model.timeString,
            style: TextStyle(
              color: kinsGrayF7,
              fontWeight: FontWeight.w500,
              fontSize: ThemeFactory.of(context).theme.fontSize10,
            ),
          ),
          SizedBox(height: 16),
        ],
      ),
    );
  }

  Widget renderIndicator() {
    return Center(
      child: SizedBox(
        width: 50,
        height: 50,
        child: CircularProgressIndicator(),
      ),
    );
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }
}
