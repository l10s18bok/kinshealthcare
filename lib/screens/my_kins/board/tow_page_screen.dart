import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/enums/screen_enums.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/screens/my_kins/board/board_question.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

import 'board.dart';
import '../../../utils/resource.dart';

/*
 작성일 : 2021-01-22
 작성자 : Mark,
 화면명 : HY_5001/HY_5002/HY_6001/HY_7001,
 경로 : /my-kins/notice, /my-kins/faq
 클래스 : TowPageScreen,
 설명 : 게시판이 2개 붙어 있는 형태의 Screen, Type 로 어떤 화면인지 구분
*/

// ignore: must_be_immutable
class TowPageScreen extends StatefulWidget {
  static final towPageTypeNotice = 'NOTICE';
  static final towPageTypeInfo = 'INFO';

  final String type;
  get firstType => type == towPageTypeNotice ? BoardType.NOTICE : BoardType.INQUIRY;
  get secondType => type == towPageTypeNotice ? BoardType.INFO : BoardType.FAQ;

  get firstName => type == towPageTypeNotice ? '공지사항' : '1:1 문의';
  get secondName => type == towPageTypeNotice ? '이용안내' : 'FAQ';

  TowPageScreen({
    required this.type,
  });

  @override
  TowPageState createState() => TowPageState();
}

class TowPageState extends State<TowPageScreen> {
  PageController _pageController = PageController(initialPage: 0);
  int pageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: renderTopBar(),
      body: renderMain(),
    );
  }

  renderTopBar() {
    return Container(
      padding: EdgeInsets.only(top: 16, left: 16, right: 16),
      alignment: Alignment.centerLeft,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            child: Container(
              alignment: Alignment.centerLeft,
              height: 44,
              width: 40,
              child: SvgPicture.asset(
                'assets/svgs/ic/ic_arrowback_common.svg',
                width: 24,
                height: 24,
                color: kinsGrayA3,
              ),
            ),
            onTap: () => Get.back(),
          ),
          renderPageControl(),
        ],
      ),
    );
  }

  renderPageControl() {
    final btnOnTap = _getBtnOnTap();

    return Row(
      children: [
        InkWell(
          child: renderControlText(widget.firstName, index: 0),
          onTap: () => setState(() => moveToPage(0)),
        ),
        SizedBox(width: 10),
        renderControlText('/'),
        SizedBox(width: 10),
        InkWell(
          child: renderControlText(widget.secondName, index: 1),
          onTap: () {
            setState(() => moveToPage(1));
          },
        ),
        renderControlBtn(onTap: btnOnTap),
      ],
    );
  }

  renderControlText(String title, {int? index}) {
    return InkWell(
      child: Text(
        title,
        style: TextStyle(
          color: index != pageIndex ? kinsGrayC2 : kinsBlack,
          fontWeight: FontWeight.w700,
          fontSize: ThemeFactory.of(context).theme.fontSize22,
        ),
      ),
      onTap: () {
        setState(() => moveToPage(index!));
      },
    );
  }

  renderControlBtn({VoidCallback? onTap}) {
    if (onTap == null) return Container();

    return Expanded(
      child: Container(
        alignment: Alignment.centerRight,
        child: InkWell(
          child: Container(
            child: Text(
              '1:1 문의 하기',
              style: TextStyle(
                color: kinsBlue40,
                fontWeight: FontWeight.w700,
                fontSize: ThemeFactory.of(context).theme.fontSize15,
              ),
            ),
          ),
          onTap: onTap,
        ),
      ),
    );
  }

  renderMain() {
    return Column(
      children: [
        Expanded(
          child: renderMainPageView(),
        ),
      ],
    );
  }

  renderMainPageView() {
    return PageView(
      controller: _pageController,
      children: <Widget>[
        renderFirstBoardWidget(),
        Board(type: widget.secondType),
      ],
      onPageChanged: (index) {
        setState(() {
          FocusScope.of(context).requestFocus(new FocusNode());
          pageIndex = index;
        });
      },
    );
  }

  renderFirstBoardWidget() {
    if (BoardType.INQUIRY == widget.firstType) return BoardQuestion();
    return Board(type: widget.firstType);
  }

  moveToPage(int index) {
    pageIndex = index;

    _pageController.animateToPage(
      pageIndex,
      duration: Duration(milliseconds: 700),
      curve: Curves.easeOutCirc,
    );
  }

  _getBtnOnTap() {
    if (pageIndex == 0 && widget.type == TowPageScreen.towPageTypeInfo) {
      return () {
        Get.toNamed('/my-kins/faq/insert_question');
      };
    }

    return null;
  }
}
