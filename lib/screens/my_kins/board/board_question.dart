import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/card/board_content.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/enums/screen_enums.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/local_board_model.dart';
import 'package:kins_healthcare/utils/resource.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import '../../../components/list_tile/expanded_tile.dart';

/*
 작성일 : 2021-03-18
 작성자 : Mark,
 화면명 : HY_6001
 경로 : ,
 클래스 : Board,
 설명 : 게시판이 2개 붙어 있는 형태의 List에 해당 하는 부분 중 1:1 문의를 별도로 표시하기 위한 Widget
*/

class BoardQuestion extends StatefulWidget {
  BoardQuestion();

  @override
  _BoardQuestionState createState() => _BoardQuestionState();
}

class _BoardQuestionState extends State<BoardQuestion> {
  late MykinsController mykinsController;
  List<LocalBoardModel>? modelList;

  @override
  void initState() {
    super.initState();
    mykinsController = Get.find<MykinsController>();
    mykinsController.addListener(_setQuestionModelList);
    _initModelList();
  }

  _initModelList() {
    final inquiryList = mykinsController.inquiryList;
    if (inquiryList == null) {
      mykinsController.postInquiryList();
      return;
    }
    modelList = inquiryList.map((item) => item.localBoardModel).toList();
  }

  _setQuestionModelList() {
    final inquiryList = mykinsController.inquiryList!;
    modelList = inquiryList.map((item) => item.localBoardModel).toList();

    _setStateEndAnimation();
  }

  @override
  void dispose() {
    super.dispose();
    mykinsController.removeListener(_setQuestionModelList);
  }

  @override
  Widget build(BuildContext context) {
    final list = modelList;
    if (list == null) return renderProgress();
    if (list.length == 0) return renderNonQuestion();

    return ListView.builder(
      itemCount: list.length,
      itemBuilder: (BuildContext context, int index) {
        final model = list[index];
        if (index == 0) return renderFirstListItem(model);
        return renderListItem(model);
      },
    );
  }

  Widget renderProgress() {
    return Column(
      children: [
        SizedBox(height: 140),
        CircularProgressIndicator(),
      ],
    );
  }

  Widget renderNonQuestion() {
    return Column(
      children: [
        SizedBox(height: 140),
        SvgPicture.asset(
          'assets/svgs/ic/ic_question_empty.svg',
          width: 72,
          height: 72,
        ),
        SizedBox(height: 24),
        Text(
          "문의하신 내용이 없습니다 :D",
          style: TextStyle(
            color: kinsBlack5B,
            fontWeight: FontWeight.w400,
            fontSize: ThemeFactory.of(context).theme.fontSize16,
          ),
        ),
      ],
    );
  }

  Widget renderFirstListItem(LocalBoardModel model) {
    return Column(
      children: [
        SizedBox(height: 27),
        renderListItem(model),
      ],
    );
  }

  Widget renderListItem(LocalBoardModel model) {
    return ExpansionTileCS(
      title: renderTitleRow(model),
      iconColor: kinsGrayB9,
      children: [
        renderTitleContent(model),
      ],
    );
  }

  Widget renderTitleRow(LocalBoardModel model) {
    return Container(
      padding: EdgeInsets.only(left: 16),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 16),
          Text(
            model.title,
            style: TextStyle(
              color: kinsBlack44,
              fontWeight: FontWeight.w500,
              fontSize: ThemeFactory.of(context).theme.fontSize14,
            ),
          ),
          SizedBox(height: 8),
          renderDateRow(model),
          SizedBox(height: 16),
        ],
      ),
    );
  }

  Widget renderDateRow(LocalBoardModel model) {
    final isAnswer = model.answer != null;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          model.regDate,
          style: TextStyle(
            color: kinsGrayF7,
            fontWeight: FontWeight.w500,
            fontSize: ThemeFactory.of(context).theme.fontSize10,
          ),
        ),
        Text(
          isAnswer ? '답변완료' : '미응답',
          style: TextStyle(
            color: isAnswer ? kinsOrangeF2 : kinsBlack7F,
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize12,
          ),
        ),
      ],
    );
  }

  Widget renderTitleContent(LocalBoardModel model) {
    return BoardContent(type: BoardType.INQUIRY, model: model);
  }

  Widget renderNoticeItem(LocalBoardModel model) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        renderTitleRow(model),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Container(
            height: 1,
            width: double.infinity,
            color: kinsGrayDD,
          ),
        ),
      ],
    );
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }
}
