import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/text_field/phone_number_text_field.dart';
import 'package:kins_healthcare/components/text_field/text_field.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/login_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/services/login/model/find_password_model.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';

/*
 * 작성일 : 2021-02-09
 * 작성자 : JC
 * 화면명 : HY_8004
 * 주요기능 : 비밀번호 찾기
 */
class PasswordScreen extends StatefulWidget {
  @override
  _PasswordScreenState createState() => _PasswordScreenState();
}

class _PasswordScreenState extends State<PasswordScreen> {
  final formKey = GlobalKey<FormState>();

  String? email;
  String? countryCode;
  String? phone;

  renderTextFields() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              // NameTextField(
              //   onSavedLastName: (val) {},
              //   onSavedFirstName: (val) {},
              // ),
              // Container(height: 16.0),
              // DobTextFormField(
              //   onSaved: (val) {},
              //   validator: (val) {
              //     return null;
              //   },
              // ),
              EmailTextField(
                onSaved: (val) {
                  setState(() {
                    this.email = val;
                  });
                },
                hasCreateEmail: false,
                customLabel: '이메일',
              ),
              Container(height: 16.0),
              PhoneNumberTextField(
                onCountryCodeSaved: (value) {
                  setState(() {
                    this.countryCode = value;
                  });
                },
                onPhoneSaved: (String? value) {
                  setState(() {
                    this.phone = value;
                  });
                },
              ),
              // Container(height: 16.0),
              // UnderlineTextFormField(
              //   labelText: '인증번호',
              //   hintText: '4자리',
              //   validator: (val) {
              //     return null;
              //   },
              //   onSaved: (val) {},
              // ),
            ],
          ),
        ),
      ),
    );
  }

  renderButton() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: PrimaryButton(
        label: '다음',
        onTap: () async {
          if (this.formKey.currentState!.validate()) {
            this.formKey.currentState!.save();

            try {
              await Get.find<LoginController>().findPassword(
                body: FindPasswordBody(
                  email: this.email,
                  nation: this.countryCode,
                  phone: this.phone,
                ),
              );
            } on DioError catch (e) {
              SnackbarUtils().parseDioErrorMessage(
                error: e,
                title: '비밀번호 찾기 에러',
              );
            }
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      resizeToAvoidBottomInset: false,
      topBar: BackTopBar(
        title: '비밀번호 찾기',
      ),
      body: Column(
        children: [
          Container(height: 40.0),
          renderTextFields(),
          renderButton(),
        ],
      ),
    );

    // return TitleLayout(
    //   title: '비밀번호 찾기',
    //   body: Column(
    //     children: [
    //       renderTextFields(),
    //       renderButton(),
    //     ],
    //   ),
    // );
  }
}
