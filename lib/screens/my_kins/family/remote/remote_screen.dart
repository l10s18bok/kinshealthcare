import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/card/family_register_card.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/relation_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/screens/my_kins/family/register/relation/relation_screen.dart';
import 'package:kins_healthcare/services/relation/model/search_user_by_phone_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
/*
 * 작성일 : 2021-03-17
 * 작성자 : JC
 * 화면명 : HY_2002-2
 * 경로 : /mykins/family/remote
 * 클래스 : RemoteScreen
 * 설명 : 서버에 등록된 가족 선택
 */

class RemoteScreenArgs {
  final String phone;

  RemoteScreenArgs({
    required this.phone,
  });
}

class RemoteScreen extends StatefulWidget {
  @override
  _RemoteScreenState createState() => _RemoteScreenState();
}

class _RemoteScreenState extends State<RemoteScreen> {
  bool loading = false;

  @override
  void initState() {
    super.initState();

    final args = Get.arguments;

    Future.microtask(() {
      if (args is RemoteScreenArgs) {
        try {
          loading = true;

          Get.find<RelationController>().searchUserByPhone(
            body: SearchUserByPhoneBody(
              phone: args.phone,
            ),
          );

          setState(() {
            loading = false;
          });
        } catch (e) {
          setState(() {
            loading = false;
          });
        }
      }
    });
  }

  renderResults() {
    return Padding(
      padding: EdgeInsets.only(top: 26.0, left: 16.0, right: 16.0),
      child: GetBuilder<RelationController>(builder: (c) {
        final items = c.userDataByPhone;

        return ListView.separated(
          separatorBuilder: (_, index) {
            return Padding(
              padding: EdgeInsets.symmetric(vertical: 18.0),
              child: Divider(
                color: Color(0xFFEAEAEA),
              ),
            );
          },
          itemBuilder: (_, index) {
            final theme = ThemeFactory.of(context).theme;

            final item = items[index];

            return FamilyRegisterCard(
              name: item.userName,
              phone: item.phone,
              avatarImgUrl: item.photo,
              avatarBgColor:
                  theme.pastelColors[index % theme.pastelColors.length],
              onTap: () {
                Get.toNamed(
                  '/my-kins/family/register/relation/?id=${item.userNo}',
                  arguments: RelationScreenArguments(
                    isNew: true,
                    user: item,
                  ),
                );
              },
            );
          },
          itemCount: items.length,
        );
      }),
    );
  }

  renderBody() {
    return GetBuilder<RelationController>(
      builder: (c) {
        final data = c.userDataByPhone;

        if (loading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          if (data.length == 0) {
            return Center(
              child: Text('검색 결과가 없습니다.'),
            );
          } else {
            return renderResults();
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(
        title: '가족 선택',
      ),
      body: renderBody(),
    );
  }
}
