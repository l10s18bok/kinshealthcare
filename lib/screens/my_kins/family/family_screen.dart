import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/btn_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/button/friend_button.dart';
import 'package:kins_healthcare/components/card/friend_with_button_card.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/controllers/relation_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/services/mykins/model/post_only_child_id_body.dart';
import 'package:kins_healthcare/services/mykins/model/search_my_children_models.dart';
import 'package:kins_healthcare/services/relation/model/post_relation_search_models.dart';
import 'package:kins_healthcare/services/relation/relation_require_opponent_model.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';

class FamilyScreenArguments {
  final bool hasHomeButton;

  FamilyScreenArguments({
    required this.hasHomeButton,
  });
}

/*
 * 작성일 : 2021-02-20
 * 작성자 : JC,
 * 화면명 : HY_2001(나의 가족), HY_CHILD_LIST(나의 자녀)
 * 경로1 : /my-kins/family(나의 가족)
 * 경로2 : /my-kins/family-children(나의 자녀)  2021/03/31 Andy 추가
 * 클래스 : FamilyScreen
 * 설명 : (HY_2001 과 HY_CHILD_LIST 공용으로 사용 ==> title 로 구분)
 */
class FamilyScreen extends StatefulWidget {
  final String title;

  const FamilyScreen({Key? key, required this.title}) : super(key: key);

  @override
  _FamilyScreenState createState() => _FamilyScreenState();
}

class _FamilyScreenState extends State<FamilyScreen> {
  late int tabIndex = 0;
  late final bool hasHomeButton;

  @override
  void initState() {
    super.initState();

    final args = Get.arguments;

    if (args != null && args is FamilyScreenArguments) {
      hasHomeButton = args.hasHomeButton;
    } else {
      hasHomeButton = false;
    }

    if (widget.title == 'family') {
      final c = Get.find<RelationController>();

      c.searchRel(
        reset: true,
      );
      c.listRelationReqOpponents(
        reset: true,
      );
    } else {
      tabIndex = -1;
      _getMyChildList();
    }
  }

  _getMyChildList() async {
    final m = Get.find<MykinsController>();

    await m.getChildList(
      reset: true,
    );
    setState(() {});
  }

  renderTab({
    required String label,
    required int index,
    String? subLabel,
  }) {
    bool isActive = false;

    if (tabIndex == index) {
      isActive = true;
    }

    final theme = ThemeFactory.of(context).theme;

    return InkWell(
      onTap: () {
        setState(() {
          tabIndex = index;
        });
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: isActive ? theme.primaryColor : Color(0xFFE9E9E9),
              width: isActive ? 2.0 : 1.0,
            ),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.all(15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                label,
                style: TextStyle(
                  fontSize: theme.fontSize13,
                  fontWeight: theme.heavyFontWeight,
                ),
              ),
              if (subLabel != null) Container(width: 5.0),
              if (subLabel != null)
                Text(
                  subLabel,
                  style: TextStyle(
                    fontSize: theme.fontSize13,
                    fontWeight: theme.heavyFontWeight,
                    color: theme.primaryOrangeColor,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  renderTabs() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: GetBuilder<RelationController>(builder: (c) {
        return Row(
          children: [
            Expanded(
              child: renderTab(
                index: 0,
                label: '등록요청',
                subLabel: c.relationReqOpponents.length.toString(),
              ),
            ),
            Expanded(
              child: renderTab(
                label: '등록된 가족',
                index: 1,
              ),
            ),
          ],
        );
      }),
    );
  }

  renderButton({
    required String label,
    required GestureTapCallback onTap,
    bool isFilled = false,
  }) {
    final theme = ThemeFactory.of(context).theme;

    Color bgColor = Color(0xFFE9E9F4);
    Color borderColor = bgColor;
    Border border = Border.all(
      color: borderColor,
      width: 2.0,
    );
    TextStyle txtStyle = TextStyle(
      fontSize: theme.fontSize13,
      color: Color(0xFF696A8B),
    );

    if (!isFilled) {
      bgColor = Colors.white;
      borderColor = Color(0xFFD7D8EE);
      border = Border.all(
        color: borderColor,
        width: 2.0,
      );
      txtStyle = TextStyle(
        fontSize: theme.fontSize13,
        color: Color(0xFF696A8B),
      );
    }

    return Container(
      decoration: BoxDecoration(
        border: border,
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Material(
        borderRadius: BorderRadius.circular(4.0),
        color: bgColor,
        child: InkWell(
          onTap: onTap,
          child: Center(
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Text(
                label,
                style: txtStyle,
              ),
            ),
          ),
        ),
      ),
    );
  }

  renderRegisterFriendCard({
    required RelationReqOpponentResponse relation,
    required int index,
  }) {
    bool isOnContacts = false;

    return FriendCardWithButtons(
      meName: '나',
      meRelationship: relation.userRelCdMeValue,
      oppName: relation.userRelCdOpponentValue!,
      oppRelationship: relation.userRelCdOpponentValue!,
      isOnContacts: isOnContacts,
      phoneNumber: relation.userPhoneMe!,
      avatarImageUrl: relation.img,
      userNoRel: relation.userNo,
      buttons: [
        FriendButton(
          variant: FriendButtonVariant.lightBlue,
          onTap: () {},
          text: '재요청',
        ),
        FriendButton(
          variant: FriendButtonVariant.blue,
          onTap: () async {
            try {
              await Get.find<RelationController>().relationApprove(
                relation.userNo!,
              );

              Get.snackbar('관계 수락 완료', '관계가 수락되었습니다.');
            } on DioError catch (e) {
              SnackbarUtils().parseDioErrorMessage(
                error: e,
                title: '관계 수락 실패',
              );
            }
          },
          text: '수락',
        ),
      ],
    );
  }

  renderRegisteredFriendCard({
    required SearchRelResponse relation,
    required int index,
  }) {
    return FriendCardWithButtons(
      phoneNumber: relation.userPhoneOpponent,
      oppRelationship: relation.userNameOpponent,
      oppName: relation.userRelCdMeValue,
      userNoRel: relation.userNoRel,
      options: Expanded(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () async {
                // 2021.04.30 Andy 추가
                await Get.toNamed(
                  '/my-kins/family/register/relation/?id=${relation.userNoRel}',
                );
                Get.find<RelationController>().searchRel(
                  reset: true,
                );
              },
              child: SvgPicture.asset(
                'assets/svgs/ic/ic_modify_hospital.svg',
              ),
            ),
            Container(width: 12.0),
            GestureDetector(
              onTap: () async {
                // 2021.04.30 Andy 추가
                final sheet = BtnBottomSheet(
                  boldTitle: '[${relation.userNameOpponent}]',
                  title: '\n을 가족 목록에서 삭제할까요?',
                );
                final result = await Get.bottomSheet<String>(sheet);

                if (result != 'OK') return;

                await Get.find<RelationController>().drop(
                  userNoOpponent: relation.userNoRel,
                );
              },
              child: SvgPicture.asset(
                'assets/svgs/ic/ic_delete_hospital.svg',
              ),
            ),
          ],
        ),
      ),
      avatarImageUrl: relation.profileImgOpponent,
    );
  }

  ///나의 자녀
  renderRegisteredChildCard({
    required List<SearchMyChildrensResponse> childList,
    required int index,
  }) {
    return FriendCardWithButtons(
      phoneNumber: childList[index].childUserPhone,
      oppRelationship: childList[index].childUserName,
      avatarImageUrl: childList[index].childUserProfile,
      userNoRel: childList[index].childUserNO,
      oppName: childList[index].userRelChild,
      options: Expanded(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () async {
                await Get.find<MykinsController>().childInfo(
                  body: PostOnlyChildIdBody(
                  childUserNO: childList[index].childUserNO,
                ));
                await Get.toNamed('/my-kins/profile/kid/?id=${childList[index].childUserNO}');
                _getMyChildList();
              },
              child: SvgPicture.asset(
                'assets/svgs/ic/ic_modify_hospital.svg',
              ),
            ),
            Container(width: 12.0),
            GestureDetector(
              onTap: () async {
                final sheet = BtnBottomSheet(
                  boldTitle: '[${childList[index].childUserName}]',
                  title: '\n을 자녀 목록에서 삭제할까요?',
                );
                final result = await Get.bottomSheet<String>(sheet);

                if (result != 'OK') return;
                final m = Get.find<MykinsController>();

                await m.deleteChild(body: PostOnlyChildIdBody(childUserNO: childList[index].childUserNO));
                // _getMyChildList();
                // setState(() {});
              },
              child: SvgPicture.asset(
                'assets/svgs/ic/ic_delete_hospital.svg',
              ),
            ),
          ],
        ),
      ),
    );
  }

  renderBody() {
    if (this.tabIndex == 0) {
      return Expanded(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: GetBuilder<RelationController>(
            builder: (c) {
              if (c.relationReqOpponents.length == 0) {
                // TODO 로딩상태 넣어주기
                return Container();
              }

              return PaginationListView<RelationReqOpponentResponse>(
                itemCount: c.relationReqOpponents.length,
                loadingWidget: renderLoading(),
                itemBuilder: (context, index) => renderRegisterFriendCard(
                  index: index,
                  relation: c.relationReqOpponents[index],
                ),
                separatorBuilder: (context, index) => Column(
                  children: [
                    Container(height: 17.0),
                    Container(height: 1.0, color: Color(0xFFEAEAEA)),
                    Container(height: 17.0),
                  ],
                ),
                lastWidget: Container(),
                request: c.listRelationReqOpponents,
                controller: c,
              );
            },
          ),
        ),
      );
    } else if (tabIndex == -1) {
      // 나의 자녀
      return Expanded(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: GetBuilder<MykinsController>(builder: (c) {
            return PaginationListView<SearchMyChildrensResponse>(
              itemCount: c.childList.length,
              loadingWidget: renderLoading(),
              itemBuilder: (context, index) => renderRegisteredChildCard(
                index: index,
                childList: c.childList,
              ),
              separatorBuilder: (context, index) => Column(
                children: [
                  Container(height: 17.0),
                  Container(height: 1.0, color: Color(0xFFEAEAEA)),
                  Container(height: 17.0),
                ],
              ),
              lastWidget: Container(),
              request: c.getChildList,
              controller: c,
            );
          }),
        ),
      );
    } else {
      return Expanded(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: GetBuilder<RelationController>(builder: (c) {
            return PaginationListView<SearchRelResponse>(
              itemCount: c.relations.length,
              loadingWidget: renderLoading(),
              itemBuilder: (context, index) => renderRegisteredFriendCard(
                index: index,
                relation: c.relations[index],
              ),
              separatorBuilder: (context, index) => Column(
                children: [
                  Container(height: 17.0),
                  Container(height: 1.0, color: Color(0xFFEAEAEA)),
                  Container(height: 17.0),
                ],
              ),
              lastWidget: Container(),
              request: c.searchRel,
              controller: c,
            );
            // return ListView.separated(
            //   itemBuilder: (_, index) {
            //     return renderRegisteredFriendCard(
            //       index: index,
            //       relation: c.relations[index],
            //     );
            //   },
            //   separatorBuilder: (_, index) {
            //     return Column(
            //       children: [
            //         Container(height: 17.0),
            //         Container(height: 1.0, color: Color(0xFFEAEAEA)),
            //         Container(height: 17.0),
            //       ],
            //     );
            //   },
            //   itemCount: c.relations.length,
            // );
          }),
        ),
      );
    }
  }

  Widget renderLoading() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      height: 50,
      child: CircularProgressIndicator(),
    );
  }

  renderHomeButton() {
    return Padding(
      padding: EdgeInsets.only(
        left: 16.0,
        right: 16.0,
        bottom: 16.0,
      ),
      child: PrimaryButton(
        label: '홈으로',
        onTap: () {
          //Get.offAllNamed('/main-home');
          Get.offAllNamed('/coach-mark/main-home');
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(
        title: widget.title == 'family' ? '나의 가족' : '나의 자녀',
        customOnBack: () {
          if (this.hasHomeButton) {
            Get.offAllNamed('/main-home');
          } else {
            Get.back();
          }
        },
        rightButtons: this.tabIndex == 0
            ? null
            : Container(
                alignment: Alignment.centerRight,
                child: InkWell(
                  onTap: () async {
                    if (widget.title == 'family') {
                      Get.toNamed(
                        '/my-kins/family/register',
                      );
                    } else {
                      await Get.toNamed('/auth/register-kid');
                      _getMyChildList();
                    }
                  },
                  child: SvgPicture.asset(
                    'assets/svgs/ic/ic_plus_main.svg',
                  ),
                ),
              ),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 43.0),
        child: Column(
          children: [
            // tapbar or  other?
            widget.title == 'family' ? renderTabs() : Container(),
            renderBody(),
            if (this.hasHomeButton) renderHomeButton(),
          ],
        ),
      ),
    );
  }
}
