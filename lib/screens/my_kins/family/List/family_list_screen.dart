import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/card/non_kkakka_card.dart';
import 'package:kins_healthcare/components/listview/pagination_list_view.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/relation_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/relation/model/know_together_model.dart';
import 'package:kins_healthcare/services/user/model/user_profile_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

/*
 작성일 : 2021-05-06
 작성자 : Andy,
 화면명 : (화면명),
 경로 : /mykins/family/list
 클래스 : FamilyListScreen,
 설명 : 나와 함께 하는 가족목록(프로필 화면에서 링크)
*/
class FamilyListScreen extends StatefulWidget {
  @override
  _FamilyListScreenState createState() => _FamilyListScreenState();
}

class _FamilyListScreenState extends State<FamilyListScreen> {
  late UserProfileBody _user;
  @override
  void initState() {
    super.initState();
    final arg = Get.arguments;

    if (arg is UserProfileBody) {
      _user = arg;
    }

    _knowTogether();
  }

  _knowTogether() async {
    final reqBody = KnowTogetherListBody(userRelNo: _user.userRelNo);
    try {
      Get.find<RelationController>().knowTogether(
        body: reqBody,
        reset: true,
      );

    } on DioError catch (e) {
      final msg = e.response!.data['resultMsg'];
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(title: '가족 목록'),
      body: Padding(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20,
            ),
            Expanded(child: renderListView()),
          ],
        ),
      ),
    );
  }

  Widget renderLoading() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      alignment: Alignment.center,
      height: 50,
      child: CircularProgressIndicator(),
    );
  }

  Widget renderListView() {
      return GetBuilder<RelationController>(
        builder: (c) {
          return PaginationListView<KnowTogetherListResponse>(
            request: c.knowTogether,
            loadingWidget: renderLoading(),
            emptyWidget: renderNonCard(),
            lastWidget: Container(),
            controller: c,
            itemCount: c.knowTogetherList.length,
            itemBuilder: (BuildContext context, int index) {
              return renderListItem(index);
            },
            separatorBuilder: (context, index) {
              return Divider(height: 10, color: Colors.transparent);
            },
          );
        },
      );
  }


  Widget renderNonCard() {
    return NonKkakkaCard(title: '가족이 없습니다.\n나의 가족에서 가족을 추가해 주세요.');
  }


  Widget renderListItem(int index) {
    final model = Get.find<RelationController>().knowTogetherList[index];
    final path = model.userImage;
    final userNo = model.userNo;

    return InkWell(
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: kinsGrayEA, width: 1.0),
          ),
        ),
        height: 84,
        child: Row(
          children: [
            CircleNetworkImage(
              path: path ?? '',
              imageSize: 56.0,
              userNo: userNo,
              heroTag: path,
            ),
            SizedBox(width: 16),
            Expanded(child: renderItemContent(model)),
          ],
        ),
      ),
      onTap: () {},
    );
  }

  Widget renderItemContent(KnowTogetherListResponse model) {
    final name = model.userName;
    final phoneNumber = model.phone;

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          name ?? '',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
          ),
        ),
        SizedBox(height: 10),
        Text(
          phoneNumber ?? '',
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: ThemeFactory.of(context).theme.fontSize14,
          ),
        ),
      ],
    );
  }

}
