import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/card/family_register_card.dart';
import 'package:kins_healthcare/components/text_field/rounded_textfield.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/relation_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/screens/my_kins/family/remote/remote_screen.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:permission_handler/permission_handler.dart';

/*
 * 작성일 : 2021-02-04
 * 작성자 : JC
 * 화면명 : HY_2002
 * 주요기능 : 가족 선택 및 등록
 */
class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  String query = '';
  bool isPermissionGranted = false;

  @override
  initState() {
    super.initState();

    getPermission();

    Future.microtask(() async {
      Get.find<RelationController>().searchRelReq(
        reset: true,
      );
    });
  }

  getPermission() async {
    final resp = await [
      Permission.contacts,
    ].request();

    if (resp[Permission.contacts] == PermissionStatus.granted) {
      setState(() {
        this.isPermissionGranted = true;
      });
    }
  }

  renderSectionTitle({required String title}) {
    final theme = ThemeFactory.of(context).theme;

    return Row(
      children: [
        Text(
          title,
          style: TextStyle(
            fontSize: theme.fontSize12,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
      ],
    );
  }

  renderRegisterRequests() {
    return GetBuilder<RelationController>(
      builder: (c) {
        return Padding(
          padding: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 20.0),
          child: Column(
            children: [
              renderSectionTitle(
                title: '등록 요청중인 가족 ${c.searchRelReqResults.length}명',
              ),
              Container(height: 20.0),
              ...(c.searchRelReqResults as List)
                  .asMap()
                  .entries
                  .map(
                    (x) => Padding(
                      padding: EdgeInsets.only(
                        bottom: x.key == c.searchRelReqResults.length - 1 ? 0 : 20.0,
                      ),
                      child: FamilyRegisterCard(
                        variant: FamilyRegisterCardVariant.smallAvatar,
                        buttonType: FamilyRegisterCardButtonType.cancelRequest,
                        name: x.value.userNameOpponent,
                        phone: x.value.userPhoneOpponent,
                        avatarImgUrl: x.value.img,
                        onTap: () async {
                          await Get.find<RelationController>().reject(x.value.userNoRel);
                        },
                      ),
                    ),
                  )
                  .toList(),
            ],
          ),
        );
      },
    );
  }

  renderContacts() {
    if (!this.isPermissionGranted) {
      return Expanded(
        child: Center(
          child: Text('연락처 권한을 허가해주세요!'),
        ),
      );
    }

    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 20.0),
        child: FutureBuilder(
          future: query.length == 0
              ? ContactsService.getContacts(
                  withThumbnails: false,
                )
              : ContactsService.getContacts(
                  query: query,
                  withThumbnails: false,
                ),
          builder: (context, snapshot) {
            int itemCount = 1;

            if (snapshot.hasData) {
              final list = (snapshot.data! as Iterable).toList();
              itemCount = list.length + 1;
            }

            if (snapshot.connectionState == ConnectionState.waiting) {
              itemCount = 2;
            }

            return Column(
              children: [
                Expanded(
                  child: ListView.separated(
                    itemCount: itemCount,
                    separatorBuilder: (_, index) {
                      return Container(height: 20.0);
                    },
                    itemBuilder: (_, index) {
                      final theme = ThemeFactory.of(context).theme;

                      if (index == 0) {
                        return Column(
                          children: [
                            Container(height: 28.0),
                            renderRegisterRequests(),
                            Container(height: 5.0, color: Color(0xFFEBE7EC)),
                            Container(height: 20.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16.0),
                              child: renderSectionTitle(
                                title: '주소록',
                              ),
                            ),
                            Container(height: 20.0),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16.0),
                              child: RoundedTextField(
                                onChanged: (val) {
                                  setState(() {
                                    query = val!;
                                  });
                                },
                              ),
                            ),
                            Container(height: 20.0),
                          ],
                        );
                      }

                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }

                      if (snapshot.hasData) {
                        // TODO 전화번호가 없는 연락처도 있는데 어찌 처리할지?
                        final Contact item = (snapshot.data as Iterable<Contact>).elementAt(index - 1);

                        return Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16.0),
                          child: FamilyRegisterCard(
                            name: item.displayName!,
                            avatarBgColor: theme.pastelColors[(index - 1) % theme.pastelColors.length],
                            phone: item.phones!.length > 0 ? item.phones!.first.value! : '',
                            onTap: () {
                              if (item.phones!.length == 0) {
                                Get.snackbar(
                                  '전화번호 인식 불가',
                                  '등록되어있는 전화번호가 없습니다.',
                                );
                                return;
                              }

                              Get.toNamed(
                                '/my-kins/family/remote',
                                arguments: RemoteScreenArgs(
                                  phone: item.phones!.first.value!.replaceAll('-', ''),
                                ),
                              );
                            },
                          ),
                        );
                      } else if (snapshot.connectionState == ConnectionState.waiting) {
                        return CircularProgressIndicator();
                      } else {
                        return Container();
                      }
                    },
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(
        title: '연락처로 가족등록',
      ),
      body: Column(
        children: [
          renderContacts(),
        ],
      ),
    );
    // return TitleLayout(
    //   title: '연락처로 가족등록',
    //   body: Column(
    //     children: [
    //       renderRegisterRequests(),
    //       Container(height: 5.0, color: Color(0xFFEBE7EC)),
    //       renderContacts(),
    //     ],
    //   ),
    // );
  }
}
