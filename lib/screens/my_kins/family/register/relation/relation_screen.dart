import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/simple_information_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/button/family_relation_button.dart';
import 'package:kins_healthcare/components/card/family_register_card.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/relation_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/screens/my_kins/family/register/relation/user/user_screen.dart';
import 'package:kins_healthcare/services/relation/model/search_user_by_phone_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';
import 'package:kins_healthcare/utils/widget_utils.dart';

class RelationScreenArguments {
  /// 새로운 관계맺기인지
  /// true - 새로운관계 false - 관계 수정
  final bool isNew;

  /// 유저 정보
  final SearchUserByPhoneResponse? user;

  RelationScreenArguments({
    this.isNew = false,
    this.user,
  });
}

/*
 * 작성일 : 2021-03-17
 * 작성자 : JC
 * 화면명 : HY_2002_P
 * 경로 : /my-kins/family/register/relation
 * 클래스 : RelationScreen
 * 설명 : 가족 관계 선택 스크린
 */
class RelationScreen extends StatefulWidget {
  @override
  _RelationScreenState createState() => _RelationScreenState();
}

class _RelationScreenState extends State<RelationScreen> {
  late final bool isNew;
  String? selectedId;
  int? userNoRel;
  SearchUserByPhoneResponse? user;

  @override
  void initState() {
    super.initState();

    final userId = Get.parameters['id'];

    if (userId != null) {
      userNoRel = int.parse(userId);
      print('userNoRel: ============> $userNoRel');
    }

    Future.microtask(() {
      Get.find<UserController>().getUserInfo();
      Get.find<RelationController>().search();
    });

    final args = Get.arguments;

    if (args is RelationScreenArguments) {
      this.isNew = args.isNew;
      this.user = args.user;
    } else {
      this.isNew = false;
    }
  }

  renderDivider() {
    return Padding(
      padding: EdgeInsets.only(top: 24.0, bottom: 14.0),
      child: Container(
        height: 5.0,
        color: Color(0xFFEFEFF4),
      ),
    );
  }

  renderRelationButtons() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: GetBuilder<RelationController>(
        builder: (c) {
          return WidgetUtils().renderListWidgetsInMultipleRows(
            rowMax: 3,
            widgetSpacing: 10.0,
            widgets: List<Widget>.from(
              c.searchResponses
                  .map(
                    (x) => FamilyRelationButton(
                      text: x.nm,
                      isActive: selectedId == x.code,
                      onTap: () {
                        setState(() {
                          selectedId = x.code;
                        });
                      },
                    ),
                  )
                  .toList(),
            ),
          );
        },
      ),
    );
  }

  renderTopFamilyCard() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: GetBuilder<UserController>(
        builder: (c) {
          if (c.userInfo == null) {
            return Container();
          }

          final user = c.userInfo!;

          if (this.user != null) {
            return FamilyRegisterCard(
              buttonType: FamilyRegisterCardButtonType.none,
              avatarImgUrl: this.user!.photo,
              name: this.user!.userName,
              phone: this.user!.phone,
            );
          } else {
            return FamilyRegisterCard(
              buttonType: FamilyRegisterCardButtonType.none,
              avatarImgUrl: user.image,
              name: user.lastname + user.firstname,
              phone: user.phone,
            );
          }
        },
      ),
    );
  }

  renderFamilyTreeButton() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.only(top: 30.0, bottom: 26.0),
      child: TextButton(
        onPressed: () {
          Get.snackbar('미구현', '미구현');
        },
        style: TextButton.styleFrom(
          backgroundColor: Color(0xFFE9E9F4),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 28.0, vertical: 4.0),
          child: Text(
            '가족관계도 보러가기',
            style: TextStyle(
              fontSize: theme.fontSize15,
              fontWeight: theme.heavyFontWeight,
              color: theme.primaryColor,
            ),
          ),
        ),
      ),
    );
  }

  _retMyFamilyScreen() async {
    try {
      if (isNew) {
        await Get.find<RelationController>().request(
          userNoRel: this.userNoRel!,
          userRelCdOpponent: this.selectedId!,
        );

        Get.offNamedUntil('/my-kins', (route) => false);
      } else {
        if (userNoRel == null) {}
        await Get.find<RelationController>().modify(
          userNoRel: this.userNoRel!,
          userRelCdOpponent: this.selectedId!,
        );

        final sheet = SimpleInformationBottomSheet(message: '가족관계가 수정되었습니다.');
        await Get.bottomSheet<String>(sheet);
        Get.back();
      }
    } on DioError catch (e) {
      SnackbarUtils().parseDioErrorMessage(
        error: e,
        title: '관계맺기 에러',
      );
    }
  }

  renderBottomButton() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            PrimaryButton(
              onTap: () async {
                if (this.selectedId == null) {
                  Get.snackbar('가족 등록 요청 에러', '선택된 관계가 없습니다.');
                  return;
                }

                if (userNoRel != null)
                  _retMyFamilyScreen();
                else {
                  Get.toNamed(
                    '/my-kins/family/register/relation/user',
                    arguments: UserScreenArguments(
                      codeOpp: selectedId!,
                    ),
                  );
                }
              },
              label: '요청',
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollLayout(
      topBar: BackTopBar(
        title: this.isNew ? '가족등록' : '가족관계 수정',
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 40.0),
        child: Column(
          children: [
            renderTopFamilyCard(),
            renderDivider(),
            renderRelationButtons(),
            renderFamilyTreeButton(),
            renderBottomButton(),
          ],
        ),
      ),
    );
  }
}
