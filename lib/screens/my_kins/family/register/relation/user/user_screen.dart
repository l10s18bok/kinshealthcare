import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/relation_request_done_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/button/family_relation_button.dart';
import 'package:kins_healthcare/components/card/family_register_card.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/relation_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';
import 'package:kins_healthcare/utils/widget_utils.dart';

class UserScreenArguments {
  final String codeOpp;

  UserScreenArguments({
    required this.codeOpp,
  });
}

/*
 * 작성일 : 2021-03-29
 * 작성자 : JC
 * 화면명 : 5013_어머니 선택
 * 경로 : /my-kins/family/register/relation/user
 * 클래스 : UserScreen
 * 설명 : 가족 등록할 유저 선택하는 스크린
 */
class UserScreen extends StatefulWidget {
  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  int? selectedUserNo;
  String? codeOpp;

  @override
  void initState() {
    super.initState();

    final UserScreenArguments args = Get.arguments;

    codeOpp = args.codeOpp;

    Future.microtask(() async {
      await Get.find<UserController>().getUserInfo();
    });
  }

  renderTopFamilyCard() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: GetBuilder<UserController>(
        builder: (c) {
          if (c.userInfo == null) {
            return Container();
          }

          final user = c.userInfo!;

          return FamilyRegisterCard(
            buttonType: FamilyRegisterCardButtonType.none,
            avatarImgUrl: user.image,
            name: user.lastname + user.firstname,
            phone: user.phone,
          );
        },
      ),
    );
  }

  renderDivider() {
    return Padding(
      padding: EdgeInsets.only(top: 24.0, bottom: 14.0),
      child: Container(
        height: 5.0,
        color: Color(0xFFEFEFF4),
      ),
    );
  }

  renderText() {
    return Padding(
      padding: EdgeInsets.only(bottom: 30.0, right: 16.0, left: 16.0),
      child: GetBuilder<UserController>(
        builder: (c) {
          if (c.userInfo == null) {
            return Container();
          }

          final user = c.userInfo!;

          return Text(
            user.lastname + user.firstname + '님과 가족관계에 있는 분은 누구인가요?',
          );
        },
      ),
    );
  }

  renderUserButtons() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: GetBuilder<RelationController>(
        builder: (c) {
          if (c.userDataByPhone.length < 3) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: c.userDataByPhone
                  .map(
                    (x) => FamilyRelationButton(
                      isActive: selectedUserNo == x.userNo,
                      text: x.userName,
                      onTap: () {
                        setState(() {
                          selectedUserNo = x.userNo;
                        });
                      },
                    ),
                  )
                  .toList(),
            );
          } else {
            return WidgetUtils().renderListWidgetsInMultipleRows(
              rowMax: 3,
              widgetSpacing: 10.0,
              widgets: c.userDataByPhone
                  .map(
                    (x) => FamilyRelationButton(
                      isActive: x.userNo == this.selectedUserNo,
                      text: x.userName,
                      onTap: () {
                        setState(() {
                          selectedUserNo = x.userNo;
                        });
                      },
                    ),
                  )
                  .toList(),
            );
          }
        },
      ),
    );
  }

  renderNextButton() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            PrimaryButton(
              label: '다음',
              onTap: () async {
                if (this.selectedUserNo == null) {
                  Get.snackbar('이름을 선택해주세요.', '이름을 선택해주세요.');
                  return;
                }

                try {
                  await Get.find<RelationController>().request(
                    userNoRel: this.selectedUserNo!,
                    userRelCdOpponent: this.codeOpp!,
                  );

                  await Get.bottomSheet(RelationRequestBottomSheet());
                } on DioError catch (e) {
                  SnackbarUtils().parseDioErrorMessage(
                    error: e,
                    title: '관계맺기 에러',
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollLayout(
      topBar: BackTopBar(
        title: '가족등록',
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 40.0),
        child: Column(
          children: [
            renderTopFamilyCard(),
            renderDivider(),
            renderText(),
            renderUserButtons(),
            renderNextButton(),
          ],
        ),
      ),
    );
  }
}
