import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/enums/screen_enums.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/ok_bottom_sheet.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/layout/custom_keyboard_layout.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/keyboard_model_notifier.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

import '../../../components/bottom_sheet/time_bottom_sheet.dart';
import '../../../components/bottom_sheet/wheel_bottom_sheet.dart';
import '../../../components/switch/kins_switch.dart';

/*
 작성일 : 2021-01-21
 작성자 : Mark,
 화면명 : HY_4001,
 경로 : /my-kins/notification-setting
 클래스 : NotificationSettingScreen,
 설명 : 알림 설정 Screen
*/

class NotificationSettingScreen extends StatefulWidget {
  @override
  NotificationSettingState createState() => NotificationSettingState();
}

class NotificationSettingState extends State<NotificationSettingScreen> {
  Map<NotificationType, bool?> checkMap = {};
  late MykinsController mykinsController;

  final customTec = TextEditingController();
  final scrollController = ScrollController();

  final keyboardNotifier = KeyboardModelNotifier(false);
  final sumKey = GlobalKey();

  String sumTitle = '0', reissueTitle = '1 일', medicalTitle = '1 일';
  String noAlarmTimeStart = 'AM 90:00', noAlarmTimeEnd = 'PM 6:00';
  double backdropHeight = 380;

  @override
  void initState() {
    super.initState();
    mykinsController = Get.find<MykinsController>();
    _setNotification();
  }

  _setNotification() async {
    try {
      final model = await mykinsController.postNotification();

      checkMap[NotificationType.KKAKKA] = model.kkakkaYn == 'Y';
      checkMap[NotificationType.PAYMENT] = model.payYn == 'Y';
      checkMap[NotificationType.ANNIVERSARY] = model.anniversaryYn == 'Y';
      checkMap[NotificationType.KKAKKA_SUM] = model.balanceYn == 'Y';
      sumTitle = model.balanceAmount ?? '';
      checkMap[NotificationType.RE_INSSUANCE] = model.selfKkakkaYn == 'Y';
      reissueTitle = model.selfKkakkaDay ?? '';
      checkMap[NotificationType.MEDICAL] = model.mediKkakkaYn == 'Y';
      medicalTitle = model.mediKkakkaDay ?? '';
      checkMap[NotificationType.EVENT] = model.eventYn == 'Y';
      checkMap[NotificationType.DISTURB] = model.disturbYn == 'Y';
      noAlarmTimeStart = model.noDisturbFrom ?? '';
      noAlarmTimeEnd = model.noDisturbTo ?? '';
      checkMap[NotificationType.VIBRATOR] = model.vibratYn == 'Y';

      _setStateEndAnimation();
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  @override
  void dispose() {
    super.dispose();
    customTec.dispose();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardLayout(
      body: renderMainLayout(),
      keyboardVisibleNotifier: keyboardNotifier,
      scrollController: scrollController,
      onKeyboardInput: _onKeyboardInput,
      customTec: customTec,
    );
  }

  _onKeyboardInput(val) {
    if (keyboardNotifier.key == sumKey) sumTitle = val;
    setState(() {});
  }

  renderMainLayout() {
    return CustomScrollLayout(
      scrollController: scrollController,
      topBar: renderTopBar(),
      body: renderMain(),
    );
  }

  Widget renderTopBar() {
    return BackTopBar(title: '알림 설정');
  }

  Widget renderMain() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          SizedBox(height: 20),
          ...renderSwitchList(NotificationType.VIBRATOR),
          ...renderSwitchList(NotificationType.KKAKKA),
          ...renderSwitchList(NotificationType.PAYMENT),
          ...renderSwitchList(NotificationType.ANNIVERSARY),
          renderSwitchBoxContainer(
            type: NotificationType.KKAKKA_SUM,
            boxContent: sumTitle,
            content: '만원 이하 일경우 알림합니다.',
            onTap: () {
              keyboardNotifier.setIsVisible(true, sumKey);
              customTec.text = sumTitle;
            },
            dataKey: sumKey,
          ),
          renderSwitchBoxContainer(
            type: NotificationType.RE_INSSUANCE,
            boxContent: reissueTitle,
            content: '매월 이날에 재발행을 알림합니다.',
            onTap: () => _listBottomOnTap(NotificationType.RE_INSSUANCE),
          ),
          renderSwitchBoxContainer(
            type: NotificationType.MEDICAL,
            boxContent: medicalTitle,
            content: '매월 이날에 재발행을 알림합니다.',
            onTap: () => _listBottomOnTap(NotificationType.MEDICAL),
          ),
          ...renderSwitchList(NotificationType.EVENT),
          renderTimeContainer(),
          Expanded(
            child: Container(
              alignment: Alignment.bottomCenter,
              child: renderSubmitBtn(),
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> renderSwitchList(NotificationType type) {
    return [
      Container(
        height: 67,
        width: double.infinity,
        alignment: Alignment.center,
        child: renderSwitchRow(type),
      ),
      renderBottomLine(),
    ];
  }

  Widget renderSwitchTextField({
    required NotificationType type,
    required String boxContent,
    required String content,
    GlobalKey? dataKey,
  }) {
    return Container(
      key: dataKey,
      width: double.infinity,
      child: Column(
        children: [
          SizedBox(height: 24),
          renderSwitchRow(type),
          SizedBox(height: 17),
          renderBoxRow(boxContent, content, true),
          SizedBox(height: 17),
          renderBottomLine(),
        ],
      ),
    );
  }

  Widget renderSwitchBoxContainer({
    required NotificationType type,
    required String boxContent,
    required String content,
    GestureTapCallback? onTap,
    GlobalKey? dataKey,
  }) {
    return Container(
      key: dataKey,
      width: double.infinity,
      child: Column(
        children: [
          SizedBox(height: 24),
          renderSwitchRow(type),
          SizedBox(height: 17),
          InkWell(
            child: renderBoxRow(boxContent, content, false),
            onTap: onTap,
          ),
          SizedBox(height: 17),
          renderBottomLine(),
        ],
      ),
    );
  }

  Widget renderSwitchRow(NotificationType type) {
    if (checkMap[type] == null) checkMap[type] = false;

    return Row(
      children: [
        renderRowTitleText(_getTitle(type)),
        Expanded(
          child: Container(
            alignment: Alignment.centerRight,
            child: KinsSwitch(
              checked: checkMap[type]!,
              onCheckChange: (isChecked) {
                setState(() => checkMap[type] = isChecked);
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget renderBoxRow(String boxContent, String content, bool isTextFiled) {
    return Container(
      height: 39,
      width: double.infinity,
      child: Row(
        children: [
          renderBoxInText(boxContent),
          SizedBox(width: 10),
          Text(
            content,
            style: TextStyle(
              color: kinsGray96,
              fontWeight: FontWeight.w400,
              fontSize: ThemeFactory.of(context).theme.fontSize13,
            ),
          ),
        ],
      ),
    );
  }

  Widget renderBoxInText(String title) {
    return Container(
      height: 39,
      padding: EdgeInsets.only(left: 16, right: 16),
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(6)),
        border: Border.all(color: kinsGrayE8, width: 1),
        color: kinsGrayF3,
      ),
      child: Text(
        title,
        style: TextStyle(
          color: kinsBlack23,
          fontWeight: FontWeight.w400,
          fontSize: ThemeFactory.of(context).theme.fontSize15,
        ),
      ),
    );
  }

  Widget renderBoxRightWidget(String? boxRightContent) {
    if (boxRightContent == null) Container();

    return Container(
      alignment: Alignment.centerRight,
      child: renderBoxInText(boxRightContent!),
    );
  }

  Widget renderTimeContainer() {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 24),
          renderSwitchRow(NotificationType.DISTURB),
          SizedBox(height: 18),
          renderTimeRow(),
          SizedBox(height: 17),
        ],
      ),
    );
  }

  Widget renderTimeRow() {
    if (checkMap[NotificationType.DISTURB] == false) {
      return Container();
    }

    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: renderTimeColumn(
            title: '시작',
            timeString: noAlarmTimeStart,
            onTap: () => _timeOnTap('방해금지 시작 시간'),
          ),
        ),
        SizedBox(width: 12),
        Expanded(
          child: renderTimeColumn(
            title: '종료',
            timeString: noAlarmTimeEnd,
            onTap: () => _timeOnTap('방해금지 종료 시간'),
          ),
        ),
      ],
    );
  }

  Widget renderTimeColumn({
    required String title,
    required String timeString,
    Color? borderColor,
    GestureTapCallback? onTap,
  }) {
    return InkWell(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
              color: kinsBlack5A,
              fontWeight: FontWeight.w700,
              fontSize: ThemeFactory.of(context).theme.fontSize12,
            ),
          ),
          SizedBox(height: 4),
          renderTimeBox(timeString),
        ],
      ),
      onTap: onTap,
    );
  }

  Widget renderTimeBox(String timeString) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      width: double.infinity,
      height: 39,
      decoration: BoxDecoration(
        border: Border.all(color: kinsGrayE8, width: 1),
        borderRadius: BorderRadius.all(Radius.circular(6.0)),
        color: kinsGrayF3,
      ),
      child: Row(
        children: [
          Text(
            timeString,
            style: TextStyle(
              fontSize: ThemeFactory.of(context).theme.fontSize14,
              color: kinsBlue40,
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: Icon(
                Icons.arrow_drop_down,
                size: ThemeFactory.of(context).theme.fontSize11,
                color: kinsBlue40,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget renderRowTitleText(String title) {
    return Text(
      title,
      style: TextStyle(
        color: kinsGray63,
        fontWeight: FontWeight.w700,
        fontSize: ThemeFactory.of(context).theme.fontSize15,
      ),
    );
  }

  Widget renderBottomLine() {
    return Container(height: 1, color: kinsGrayE4);
  }

  void _listBottomOnTap(NotificationType type) async {
    List<String> items = [];
    for (int i = 1; i <= 31; ++i) {
      items.add('$i 일');
    }

    final data = await showBottomSheet('까까 재발행 알림', items);
    if (data == null) return;

    if (type == NotificationType.RE_INSSUANCE) {
      reissueTitle = data.trim();
    } else if (type == NotificationType.MEDICAL) {
      medicalTitle = data.trim();
    }

    setState(() {});
  }

  void _timeOnTap(String title) async {
    final sheet = TimeBottomSheet(title: title);
    final selectTitle = await Get.bottomSheet<String>(sheet);
    if (selectTitle == null) return;

    if (title == '방해금지 시작 시간') {
      noAlarmTimeStart = selectTitle.trim();
    } else {
      noAlarmTimeEnd = selectTitle.trim();
    }
    setState(() {});
  }

  Future<String?> showBottomSheet(String title, List<String> items) async {
    final sheet = WheelBottomSheet(
      title: title,
      items: items,
    );
    final selectTitle = await Get.bottomSheet<String>(sheet);

    return selectTitle;
  }

  Widget renderSubmitBtn() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      height: 60,
      child: PrimaryButton(
        label: '저장',
        onTap: () async {
          FocusScope.of(context).requestFocus(new FocusNode());

          try {
            final data = await mykinsController.updateNotification(
              checkMap[NotificationType.KKAKKA] == true ? 'Y' : 'N',
              checkMap[NotificationType.PAYMENT] == true ? 'Y' : 'N',
              checkMap[NotificationType.ANNIVERSARY] == true ? 'Y' : 'N',
              checkMap[NotificationType.KKAKKA_SUM] == true ? 'Y' : 'N',
              checkMap[NotificationType.KKAKKA_SUM] == true ? sumTitle : '',
              checkMap[NotificationType.RE_INSSUANCE] == true ? 'Y' : 'N',
              checkMap[NotificationType.RE_INSSUANCE] == true ? reissueTitle : '',
              checkMap[NotificationType.MEDICAL] == true ? 'Y' : 'N',
              checkMap[NotificationType.MEDICAL] == true ? medicalTitle : '',
              checkMap[NotificationType.EVENT] == true ? 'Y' : 'N',
              checkMap[NotificationType.DISTURB] == true ? 'Y' : 'N',
              checkMap[NotificationType.DISTURB] == true ? noAlarmTimeStart : '',
              checkMap[NotificationType.DISTURB] == true ? noAlarmTimeEnd : '',
              checkMap[NotificationType.VIBRATOR] == true ? 'Y' : 'N',
            );

            String title;
            String msg;
            if (data.resultCode == 'SUCCESS') {
              title = '완료';
              msg = '알림 설정 저장이 완료되었습니다.';
            } else {
              title = '실패';
              msg = '알림 설정 저징이 실패되었습니다. resultCode:FAIL';
            }

            final sheet = OkBottomSheet(title: title, content: msg);
            await Get.bottomSheet(sheet);
            Get.back();
          } on DioError catch (e) {
            final msg = e.response!.data.toString();
            final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
            Get.bottomSheet(sheet);
          }
        },
      ),
    );
  }

  String _getTitle(NotificationType type) {
    switch (type) {
      case NotificationType.KKAKKA:
        return '까까';
      case NotificationType.PAYMENT:
        return '결제';
      case NotificationType.ANNIVERSARY:
        return '기념일';
      case NotificationType.KKAKKA_SUM:
        return '까까 총액';
      case NotificationType.RE_INSSUANCE:
        return '셀프 까까 재발행 알림';
      case NotificationType.MEDICAL:
        return '의료비 까까 재발행 알림';
      case NotificationType.EVENT:
        return '이벤트/혜택';
      case NotificationType.DISTURB:
        return '방해금지 시간';
      case NotificationType.VIBRATOR:
        return '진동';
      default:
        return '제목';
    }
  }

  void _setStateEndAnimation() {
    var route = ModalRoute.of(context);
    if (route == null) return setState(() {});
    final animation = route.animation;
    if (animation == null) return setState(() {});

    if (animation.isCompleted) return setState(() {});

    void handler(status) {
      if (status == AnimationStatus.completed) {
        setState(() {});
        animation.removeStatusListener(handler);
      }
    }

    animation.addStatusListener(handler);
  }
}
