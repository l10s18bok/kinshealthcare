import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/screens/my_kins/anniversary/register/add_anniversary_screen.dart';
import 'package:kins_healthcare/services/mykins/model/get_anniversary_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:table_calendar/table_calendar.dart';

/*
 * 작성일 : 2021-02-08
 * 작성자 : JC
 * 화면명 : HY_3001
 * 주요기능 : 나의 기념일 달력
 */
class AnniversaryScreen extends StatefulWidget {
  @override
  _AnniversaryScreenState createState() => _AnniversaryScreenState();
}

class _AnniversaryScreenState extends State<AnniversaryScreen> {
  late DateTime selectedDay;
  late DateTime focusedDay;

  @override
  initState() {
    super.initState();

    focusedDay = DateTime.now();
    selectedDay = DateTime.now();

    Get.find<MykinsController>().getAnniversary();
  }

  renderCalendarHeader() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 32.0),
      child: Row(
        children: [
          Text(
            '${focusedDay.year}-${focusedDay.month}',
            style: TextStyle(
              fontSize: theme.fontSize20,
            ),
          ),
          Container(width: 27.0),
          GestureDetector(
            onTap: () {
              setState(() {
                this.focusedDay = DateTime(
                    this.focusedDay.year, this.focusedDay.month - 1, 1);
              });
            },
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_kkakkapage_left.svg',
              width: 24.0,
              height: 24.0,
              color: Color(0xFFC5C5C5),
            ),
          ),
          Container(width: 16.0),
          GestureDetector(
            onTap: () {
              setState(() {
                this.focusedDay = DateTime(
                    this.focusedDay.year, this.focusedDay.month + 1, 1);
              });
            },
            child: SvgPicture.asset(
              'assets/svgs/ic/ic_kkakkapage_right.svg',
              width: 24.0,
              height: 24.0,
              color: Color(0xFFC5C5C5),
            ),
          ),
        ],
      ),
    );
  }

  renderAddEvent() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      child: Material(
        color: Colors.white,
        borderRadius: BorderRadius.circular(23.5),
        child: InkWell(
          onTap: () async {
            await Get.toNamed('/my-kins/anniversary/register/public',
                arguments: AddAnniversaryArguments(selectedDay: selectedDay));

            Get.find<MykinsController>().getAnniversary();
          },
          borderRadius: BorderRadius.circular(23.5),
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Color(0xFFD7DBEE),
                width: 1.0,
              ),
              borderRadius: BorderRadius.circular(
                23.5,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 28.0, vertical: 12.0),
              child: Text(
                '기념일 추가',
                style: TextStyle(
                  color: Color(0xFF4042AB),
                  fontSize: theme.fontSize13,
                  fontWeight: theme.heavyFontWeight,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  List loadEvents(DateTime day, List<GetAnniversaryResponse> anniversaries) {
    return anniversaries
        .where(
          (element) =>
              element.dates ==
              '${day.year}-${day.month.toString().padLeft(2, '0')}-${day.day.toString().padLeft(2, '0')}',
        )
        .map((x) => x.anniName)
        .toList();
  }

  renderCalendar() {
    final theme = ThemeFactory.of(context).theme;

    TextStyle dayStyle = TextStyle(
      fontSize: theme.fontSize14,
      color: Color(0xFF888888),
    );

    return GetBuilder<MykinsController>(
      builder: (c) {
        return TableCalendar(
          focusedDay: focusedDay,
          firstDay: DateTime.utc(1900, 1, 1),
          lastDay: DateTime.utc(2400, 12, 31),
          selectedDayPredicate: (day) {
            return isSameDay(selectedDay, day);
          },
          eventLoader: (DateTime day) => loadEvents(day, c.anniversaries),
          headerVisible: false,
          daysOfWeekStyle: DaysOfWeekStyle(
            dowTextFormatter: (DateTime day, _) {
              final List dowTxt = [
                'M',
                'T',
                'W',
                'T',
                'F',
                'S',
                'S',
              ];
              return dowTxt[day.weekday - 1];
            },
            weekendStyle: TextStyle(
              fontSize: theme.fontSize12,
              color: Color(0xFFFF4A4A),
              fontWeight: theme.heavyFontWeight,
            ),
            weekdayStyle: TextStyle(
              fontSize: theme.fontSize12,
              color: Color(0xFF3D3D3D),
              fontWeight: theme.heavyFontWeight,
            ),
          ),
          calendarStyle: CalendarStyle(
            selectedDecoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                color: theme.primaryOrangeColor,
                width: 1.0,
              ),
            ),
            outsideDaysVisible: false,
            selectedTextStyle: dayStyle,
            defaultTextStyle: dayStyle,
            todayDecoration: BoxDecoration(
              shape: BoxShape.circle,
              color: theme.primaryOrangeColor,
              border: Border.all(
                color: theme.primaryOrangeColor,
                width: 1.0,
              ),
            ),
          ),
          onPageChanged: (DateTime day) {
            setState(() {
              focusedDay = day;
            });
          },
          onDaySelected: (selected, focused) {
            setState(() {
              selectedDay = selected;
            });
          },
        );
      },
    );
  }

  getDDay() {}

  renderCard({
    required GetAnniversaryResponse anniversary,
  }) {
    final theme = ThemeFactory.of(context).theme;

    final greyBorder = BorderSide(
      color: Color(0xFFEEEFF3),
      width: 2.0,
    );

    return ClipRRect(
      borderRadius: BorderRadius.circular(
        4.0,
      ),
      child: InkWell(
        onTap: () {},
        borderRadius: BorderRadius.circular(4.0),
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              left: BorderSide(
                color: Color(0xFFFF9B95),
                width: 4.0,
              ),
            ),
          ),
          child: Container(
            decoration: BoxDecoration(
              border: Border(
                right: greyBorder,
                top: greyBorder,
                bottom: greyBorder,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          SvgPicture.asset(
                            'assets/svgs/ic/ic_anniversary_kkakka.svg',
                          ),
                          Container(width: 4.0),
                          Text(
                            '기념일',
                            style: TextStyle(
                              fontSize: theme.fontSize12,
                              fontWeight: theme.heavyFontWeight,
                            ),
                          ),
                        ],
                      ),
                      Text(
                        'D${anniversary.dDay}',
                        style: TextStyle(
                          fontSize: theme.fontSize22,
                          color: theme.primaryColor,
                        ),
                      ),
                    ],
                  ),
                  Container(height: 10.0),
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 11.0,
                        backgroundImage: NetworkImage(
                          anniversary.relUserImage!,
                        ),
                      ),
                      Container(width: 10.0),
                      Text(
                        '${anniversary.relUserName}(${anniversary.relName})',
                        style: TextStyle(
                          fontSize: theme.fontSize10,
                          color: Color(0xFF7D7D7D),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 16.0),
                    child: Container(
                      height: 1.0,
                      color: Color(0xFFE8E7EC),
                    ),
                  ),
                  Row(
                    children: [
                      Text(
                        anniversary.anniName!,
                        style: TextStyle(
                          fontSize: theme.fontSize14,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  renderCards() {
    return Expanded(
      child: GetBuilder<MykinsController>(builder: (c) {
        print(c.anniversaries.map((e) => e.toJson()));
        final anniversaries = c.anniversaries
            .where(
              (element) => element.isSelectedDay(
                selectedDay,
              ),
            )
            .toList();

        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          child: ListView.separated(
            separatorBuilder: (_, index) {
              return Container(
                height: 16.0,
              );
            },
            itemBuilder: (_, index) {
              return renderCard(anniversary: anniversaries[index]);
            },
            itemCount: anniversaries.length,
          ),
        );
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(
        title: '나의 기념일 달력',
      ),
      body: Column(
        children: [
          Container(height: 40.0),
          renderCalendarHeader(),
          renderCalendar(),
          renderAddEvent(),
          Container(height: 5.0, color: Color(0xFFE8E7EC)),
          Container(height: 20.0),
          renderCards(),
        ],
      ),
    );

    // return TitleLayout(
    //   title: '나의 기념일 달력',
    //   body: Column(
    //     children: [
    //       renderCalendarHeader(),
    //       renderCalendar(),
    //       renderAddEvent(),
    //       Container(height: 5.0, color: Color(0xFFE8E7EC)),
    //       Container(height: 20.0),
    //       renderCards(),
    //     ],
    //   ),
    // );
  }
}
