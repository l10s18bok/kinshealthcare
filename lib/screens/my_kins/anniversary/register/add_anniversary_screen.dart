import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:kins_healthcare/components/bottom_sheet/simple_information_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/primary_button.dart';
import 'package:kins_healthcare/components/calendar/add_anniversary_calendar.dart';
import 'package:kins_healthcare/components/card/circle_network_image.dart';
import 'package:kins_healthcare/components/grid/box_grid.dart';
import 'package:kins_healthcare/components/top_bar/simple_hide_top_bar.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/screens/payment/share/share_screen.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/box_grid_model.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/model/payment_user_model.dart';
import 'package:kins_healthcare/services/mykins/model/family_list_model.dart';
import 'package:kins_healthcare/services/mykins/model/getUserInfo_response.dart';
import 'package:kins_healthcare/services/mykins/model/insert_anniversary_body.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

/*
 * 작성일 : 2021-02-09
 * 작성자 : Andy
 * 화면명 : HY_3003 + HY_3004
 * 클래스 : AddAnniversary
 * 경로  : /my-kins/anniversary/register/public
 * 설명 : 기념일 등록 및 공개가족선택
 */

class AddAnniversaryArguments {
  final DateTime selectedDay;

  AddAnniversaryArguments({
    required this.selectedDay,
  });
}

class AddAnniversaryScreen extends StatefulWidget {
  @override
  _AddAnniversaryScreenState createState() => _AddAnniversaryScreenState();
}

class _AddAnniversaryScreenState extends State<AddAnniversaryScreen> {
  ScrollController _scrollController = ScrollController(); //;
  TextEditingController _nameTextController = TextEditingController();
  TextEditingController _notiTextController = TextEditingController();

  bool _categoryCheckFlag = false; //카테고리 선택 유무
  bool _lunarBtn = false; // 음력버튼
  bool _liftBtn = true; // 양력버튼
  bool doubleClick = false; // 다음버튼 중복 탭 방지

  String _kkakkaType = '';

  String? _errorText;

  late DateTime _selectedDate;

  List<FamilyListModel> _familyList = [];
  List<GetUserInfoResponse> _reciveUser = [];

//
  final _listAnniversary = [
    BoxGridModel(
      title: '생일',
      imagePath: 'assets/svgs/ic/ic_birthday_anniversary.svg',
      selectedImagePath: 'assets/svgs/ic/ic_birthday_anniversary.svg',
      ratio: 105 / 90,
    ),
    BoxGridModel(
      title: '결혼기념일',
      imagePath: 'assets/svgs/ic/ic_wedding_anniversary.svg',
      selectedImagePath: 'assets/svgs/ic/ic_wedding_anniversary.svg',
      ratio: 105 / 90,
    ),
    BoxGridModel(
      title: '기타',
      imagePath: 'assets/svgs/ic/ic_etc_anniversary.svg',
      selectedImagePath: 'assets/svgs/ic/ic_etc_anniversary.svg',
      ratio: 105 / 90,
    ),
  ];

  @override
  void initState() {
    super.initState();

    final AddAnniversaryArguments? args = Get.arguments;

    if (args == null) {
      _selectedDate = DateTime.now();
    } else {
      _selectedDate = args.selectedDay;
    }

    _getRecivePerson();
    //_getUserList();
  }

  @override
  void dispose() {
    _scrollController.dispose(); //
    _nameTextController.dispose();
    _notiTextController.dispose();
    super.dispose();
  }

  _getRecivePerson() async {
    try {
      final resp = await Get.find<MykinsController>().getUserInfo();

      //if (resp == null) return;

      setState(() {
        _reciveUser.add(resp);
      });
    } on DioError catch (e) {
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  // _getUserList() async {
  //   try {
  //     final userList = await Get.find<MykinsController>().getFamilyList();

  //     for (final model in _familyList) {
  //       for (final targetModel in userList) {
  //         if (model.relUserNo == targetModel.relUserNo) {
  //           userList.remove(targetModel);
  //           break;
  //         }
  //       }
  //     }
  //     _familyList.clear();

  //     setState(() {
  //       _familyList.addAll(userList);
  //     });
  //   } on DioError catch (e) {
  //     final msg = e.response!.data.toString();
  //     final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
  //     Get.bottomSheet(sheet);
  //   }
  // }

  _sendData() {
    final shareList = _familyList.map((e) => InsertAnniShareUser(shareUserNo: e.relUserNo!)).toList();
    final sendData = InsertAnniversaryBody(
      anniName: _nameTextController.text,
      anniType: InsertAnniversaryBody.getTitleToKkakkaType(_kkakkaType),
      calType: _liftBtn ? 0 : 1,
      day: DateFormat('yyyy-MM-dd').format(_selectedDate),
      //_selectedDate.toString().replaceAll('.', '-'),
      pushYn: _notiTextController.text.isEmpty ? 'N' : 'Y',
      pushDay: _notiTextController.text.isEmpty ? '0' : _notiTextController.text,
      userRelNo: _reciveUser[0].userNo,
      shareList: shareList, //// 확인
    );
    print('==================> ${sendData.toJson()}');
    shareList.map((e) => print('shareListMember relUserNo : ${e.shareUserNo.toString()}')).toList();

    return sendData;
  }

  _showBottomSubBtnSheet() async {
    if (!_categoryCheckFlag) {
      Get.snackbar('알림', '카테고리를 선택해주세요.');
      return;
    }

    if (_nameTextController.text.isEmpty) {
      setState(() {
        _errorText = '기념일 이름을 입력하세요.';
      });
      return;
    }

    if (doubleClick) return;
    doubleClick = true;

    _errorText = null;

    final data = _sendData();

    try {
      final resp = await Get.find<MykinsController>().insertAnniversary(data);
      if (resp.resultCode == 'SUCCESS') {
        final sheet = SimpleInformationBottomSheet(message: '새 기념일이 등록되었습니다.');
        await Get.bottomSheet<String>(sheet);
        Get.back();
      }
    } on DioError catch (e) {
      doubleClick = false;
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
    }
  }

  _onGridSelectModel(BoxGridModel model) {
    print('${model.title}');
    _kkakkaType = model.title;
    _categoryCheckFlag = true;
  }

  _changeKeyboardType(keyType) {
    if (keyType == "Number") {
      return TextInputType.number;
    }
    return TextInputType.text;
  }

  _showDialogCalendar() {
    Get.dialog(
      AlertDialog(
        backgroundColor: Colors.white,
        contentPadding: const EdgeInsets.all(0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
        content: Builder(
          builder: (context) {
            return AddAnniversaryCalendar(
              onChanged: (DateTime selDate) {
                setState(() {
                  _selectedDate = selDate;
                });
              },
            );
          },
        ),
      ),
      barrierDismissible: true,
    );
  }

  // _showDataPicker() {
  //   Future<DateTime?> selectedDate = showDatePicker(
  //     context: context,
  //     //locale: const Locale('ko', 'KO'),
  //     initialDate: DateTime.now(),
  //     // 초깃값
  //     firstDate: DateTime(2018),
  //     // 시작일
  //     lastDate: DateTime(2030),
  //     // 마지막일
  //     builder: (BuildContext? context, Widget? child) {
  //       return Theme(
  //         data: ThemeData.light().copyWith(
  //           colorScheme: ColorScheme.fromSwatch(
  //             primarySwatch: Colors.purple,
  //             primaryColorDark: Colors.purple,
  //             accentColor: Colors.purple,
  //           ),
  //           dialogBackgroundColor: Colors.white, // 다크테마(ThemeData.dark(),)
  //         ),
  //         child: child!,
  //       );
  //     },
  //   );

  //   selectedDate.then((dateTime) {
  //     String formattedDate = DateFormat('yyyy.MM.dd').format(dateTime!);
  //     setState(() {
  //       _selectedDate = formattedDate;
  //     });
  //   });
  // }

  _shareFamily({required List list}) async {
    //Get.toNamed('/kkakka/request/target');
    var payUserModel;
    ShareList shareList;

    if (list is List<GetUserInfoResponse>) {
      payUserModel = _reciveUser.map((e) => PaymentUserModel(name: e.userName, phoneNum: '', userRelNo: e.userNo)).toList();
      shareList = ShareList(modelList: payUserModel, mult: false);
    } else {
      payUserModel = _familyList.map((e) => PaymentUserModel(name: e.relUserName!, phoneNum: e.relUserPhone!, userRelNo: e.relUserNo!)).toList();
      shareList = ShareList(modelList: payUserModel, mult: true);
    }

    final result = await Get.toNamed('/payment/share-target', arguments: shareList);
    if (result == null) return;
    if (list is List<GetUserInfoResponse>) {
      GetUserInfoResponse getUserInfoResponse = GetUserInfoResponse(
        userNo: result[0].relUserNo,
        userImage: result[0].relUserImage,
        userName: result[0].relUserName,
        relName: result[0].relName,
      );
      _reciveUser.clear();
      _reciveUser.add(getUserInfoResponse);
    } else {
      _familyList.insertAll(0, result);
    }
    setState(() {});
  }

  _delete({required List list, required int index}) {
    setState(() {
      list.removeAt(index);
    });
  }

  renderFieldLabel(String label, {String? sub}) {
    final theme = ThemeFactory.of(context).theme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              label,
              style: TextStyle(
                color: Colors.black,
                fontSize: theme.fontSize13,
                fontWeight: theme.heavyFontWeight,
              ),
            ),
            SizedBox(width: 7),
            Text(
              sub ?? '',
              style: TextStyle(
                color: Color(0xFF4042AB),
                fontSize: theme.fontSize11,
              ),
            ),
          ],
        ),
        SizedBox(height: 10),
      ],
    );
  }

  renderTextFieldElement({
    required String hintText,
    String? errorText,
    TextEditingController? controller,
    String? inputType,
    String? trail,
  }) {
    final theme = ThemeFactory.of(context).theme;
    return Stack(
      children: [
        TextField(
          controller: controller,
          keyboardType: this._changeKeyboardType(inputType),
          textInputAction: TextInputAction.done,
          decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
            hintText: hintText,
            errorText: errorText,
            hintStyle: TextStyle(fontSize: theme.fontSize15, color: Color(0XFFB6B6B6)),
          ),
          onSubmitted: (term) {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          onChanged: (val) {
            setState(() {
              _errorText = null;
            });
          },
        ),
        Positioned(
          right: 0,
          bottom: 13,
          child: Text(
            trail ?? '',
            style: TextStyle(color: Color(0xFF262626), fontSize: theme.fontSize15),
          ),
        )
      ],
    );
  }

  renderBoxButton(String btnName, bool sel) {
    final theme = ThemeFactory.of(context).theme;
    return InkWell(
      onTap: () {
        setState(() {
          _lunarBtn = !_lunarBtn;
          _liftBtn = !_liftBtn;
        });
      },
      child: Container(
        width: 56,
        height: 39,
        decoration: BoxDecoration(
          color: sel ? Color(0XFF4042AB) : Colors.transparent,
          border: Border.all(
            color: Color(0xFFD7D8EE),
            width: 1,
          ),
          borderRadius: BorderRadius.circular(4),
        ),
        child: Align(
          alignment: Alignment.center,
          child: Text(
            btnName,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: theme.fontSize14,
              color: sel ? Colors.white : Color(0XFF696A8B),
              fontWeight: theme.heavyFontWeight,
            ),
          ),
        ),
      ),
    );
  }

  renderCalenderBox() {
    final theme = ThemeFactory.of(context).theme;

    return InkWell(
      onTap: _showDialogCalendar, //_showDataPicker,
      child: Container(
        height: 39,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Color(0xFF878787),
              width: 1.0,
            ),
          ),
        ),
        child: Row(
          children: [
            SvgPicture.asset(
              'assets/svgs/ic/ic_calender_select_kkakkapage.svg',
              color: Color(0xFF525358),
            ),
            SizedBox(width: 11),
            Text(
              DateFormat('yyyy.MM.dd').format(_selectedDate),
              style: TextStyle(
                color: Color(0xFFB6B6B6),
                fontSize: theme.fontSize15,
              ),
            )
          ],
        ),
      ),
    );
  }

  renderBtnCalender() {
    return Row(
      children: [
        renderBoxButton('양력', _liftBtn),
        SizedBox(width: 8),
        renderBoxButton('음력', _lunarBtn),
        SizedBox(width: 17),
        Expanded(child: renderCalenderBox())
      ],
    );
  }

  renderFamilyBox({required List list, required int index}) {
    final theme = ThemeFactory.of(context).theme;
    late String bgImage;
    late String name;
    late int userNo;
    final bool isReciveUser = list is List<GetUserInfoResponse>;
    if (!isReciveUser) {
      bgImage = _familyList[index].relUserImage!;
      name = _familyList[index].relUserName!;
      userNo = _familyList[index].relUserNo!;
    } else {
      bgImage = _reciveUser[index].userImage;
      name = _reciveUser[index].userName;
      userNo = _reciveUser[index].userNo;
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CircleNetworkImage(
          imageSize: 42,
          path: bgImage,
          userNo: userNo,
          heroTag: bgImage + userNo.toString(),
        ),
        SizedBox(width: 10),
        Expanded(
          child: Text(
            name,
            style: TextStyle(
              color: Color(0xFF252525),
              fontSize: theme.fontSize14,
            ),
          ),
        ),
        isReciveUser
            ? Container()
            : InkWell(
                onTap: () => _delete(list: list, index: index),
                child: Container(
                  width: 28,
                  height: 28,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xFFD7D8EE),
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: Icon(
                    Icons.clear,
                    size: 24,
                  ),
                ),
              )
      ],
    );
  }

  renderAddLabel({required List list, required String titleLabel, required String subLabel}) {
    final theme = ThemeFactory.of(context).theme;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          titleLabel,
          style: TextStyle(
            color: Colors.black,
            fontSize: theme.fontSize13,
            fontWeight: FontWeight.bold,
          ),
        ),
        InkWell(
          onTap: () => _shareFamily(list: list),
          child: Text(
            subLabel,
            style: TextStyle(
              color: Color(0XFFF29061),
              fontSize: theme.fontSize13,
              fontWeight: theme.heavyFontWeight,
            ),
          ),
        )
      ],
    );
  }

  renderListFamily() {
    return ListView.separated(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: _familyList.length,
      separatorBuilder: (_, idx) {
        return Divider(
          height: 10,
          color: Colors.transparent,
        );
      },
      itemBuilder: (_, idx) {
        return renderFamilyBox(list: _familyList, index: idx);
      },
    );
  }

  renderBottomBtn() {
    return Container(
      margin: const EdgeInsets.only(bottom: 16),
      child: PrimaryButton(
        label: '등록',
        onTap: () {
          _showBottomSubBtnSheet();
          FocusScope.of(context).requestFocus(new FocusNode());
        },
      ),
    );
  }

  renderAnimationTopBar() {
    return SimpleHideTopBar(
      title: '새 기념일 추가하기',
      scrollController: _scrollController,
    );
  }

  renderDivider() {
    return Container(
      height: 6.0,
      margin: const EdgeInsets.only(bottom: 24),
      color: Color(0xFFEFEFF4),
      child: Align(
        alignment: Alignment.topCenter,
        child: Container(
          height: 1,
          color: Color(0xFFE8E7EC),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Column(
          children: [
            renderAnimationTopBar(),
            Expanded(
              child: ListView(
                controller: _scrollController,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(16, 27, 16, 16),
                    child: Column(
                      children: [
                        BoxGrid(
                          models: _listAnniversary,
                          onLastSelectItem: _onGridSelectModel,
                        ),
                        SizedBox(height: 35),
                        renderFieldLabel('기념일 이름'),
                        renderTextFieldElement(hintText: '뽀뽀 태어난지 100일', errorText: _errorText, controller: _nameTextController, inputType: 'String'),
                        SizedBox(height: 28),
                        renderFieldLabel(
                          '사용기간',
                        ),
                        renderBtnCalender(),
                        SizedBox(height: 28),
                        renderFieldLabel('기념일 알림 설정', sub: '*해당일 전에 기념일 알림을 드립니다'),
                        renderTextFieldElement(hintText: '0', errorText: null, controller: _notiTextController, inputType: 'Number', trail: '일 전'),
                        SizedBox(height: 36),
                      ],
                    ),
                  ),
                  renderDivider(),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      children: [
                        renderAddLabel(list: _reciveUser, titleLabel: '받는 사람', subLabel: '수정하기'),
                        SizedBox(height: 10),
                        _reciveUser.length != 0 ? renderFamilyBox(list: _reciveUser, index: 0) : Container(),
                        SizedBox(height: 28),
                        renderAddLabel(list: _familyList, titleLabel: '공유할 사람', subLabel: '+ 추가하기'),
                        SizedBox(height: 10),
                        renderListFamily(),
                        SizedBox(height: 50),
                        renderBottomBtn(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
