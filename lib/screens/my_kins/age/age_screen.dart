import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/widget_utils.dart';

class ExpMap {
  final String label;
  final int points;

  ExpMap({
    required this.label,
    required this.points,
  });
}

class AgeScreen extends StatefulWidget {
  @override
  _AgeScreenState createState() => _AgeScreenState();
}

class _AgeScreenState extends State<AgeScreen> {
  List<ExpMap> exps = [
    ExpMap(
      label: '한 살',
      points: 50,
    ),
    ExpMap(
      label: '두 살',
      points: 100,
    ),
    ExpMap(
      label: '세 살',
      points: 150,
    ),
    ExpMap(
      label: '네 살',
      points: 200,
    ),
    ExpMap(
      label: '다섯 살',
      points: 250,
    ),
    ExpMap(
      label: '여섯 살',
      points: 300,
    ),
    ExpMap(
      label: '일곱 살',
      points: 350,
    ),
    ExpMap(
      label: '여덟 살',
      points: 400,
    ),
    ExpMap(
      label: '아홉 살',
      points: 450,
    ),
    ExpMap(
      label: '열 살',
      points: 500,
    ),
    ExpMap(
      label: '열한 살',
      points: 550,
    ),
    ExpMap(
      label: '열두 살',
      points: 600,
    ),
  ];

  @override
  initState() {
    super.initState();

    Get.find<MykinsController>().getExp();
  }

  renderSubtitle() {
    final theme = ThemeFactory.of(context).theme;

    final userC = Get.find<UserController>();

    TextStyle baseStyle = TextStyle(
      fontSize: theme.fontSize15,
      color: Color(0xFF636363),
    );

    TextStyle primaryStyle = baseStyle.copyWith(
      fontSize: theme.fontSize17,
      color: theme.primaryColor,
      fontWeight: theme.heavyFontWeight,
    );

    return GetBuilder<MykinsController>(builder: (c) {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Row(
          children: [
            Text(
              '${userC.userInfo!.lastname}${userC.userInfo!.firstname}님 마음 속 나무는 ',
              style: baseStyle,
            ),
            Text(
              c.exp?.userLevel ?? '',
              style: primaryStyle,
            ),
            Text(
              '이예요 :)',
              style: baseStyle,
            ),
          ],
        ),
      );
    });
  }

  renderTreeAge() {
    final theme = ThemeFactory.of(context).theme;

    return GetBuilder<MykinsController>(builder: (c) {
      double expValue = 0.0;

      final exp = c.exp;
      if (exp != null) {
        final idx = this.exps.indexWhere(
              (element) => element.label == exp.userLevel,
            );

        double prevVal = idx == 0 ? 0.0 : this.exps[idx - 1].points.toDouble();

        expValue = (exp.userExp! - prevVal) / this.exps[idx].points;
      }

      int treeLevel = this.exps.indexWhere((element) => element.label == c.exp?.userLevel) + 1;

      return Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      '계속 자라고 있어요!',
                      style: TextStyle(
                        fontSize: theme.fontSize12,
                        fontWeight: theme.heavyFontWeight,
                      ),
                    ),
                    Container(height: 6.0),
                    LinearProgressIndicator(
                      backgroundColor: Color(0xFFE4E4ED),
                      minHeight: 7.0,
                      value: expValue,
                      valueColor: AlwaysStoppedAnimation<Color>(theme.primaryColor),
                    ),
                  ],
                ),
              ),
              SvgPicture.asset(
                'assets/svgs/img/tree_level_${treeLevel.toString().padLeft(2, '0')}.svg',
              ),
            ],
          ),
        ),
      );
    });
  }

  renderBody() {
    final utils = WidgetUtils();
    final theme = ThemeFactory.of(context).theme;

    return utils.renderListWidgetsInMultipleRows(
      widgets: this
          .exps
          .asMap()
          .entries
          .map(
            (item) => Column(
              children: [
                SvgPicture.asset(
                  'assets/svgs/img/tree_level_${(item.key + 1).toString().padLeft(2, '0')}.svg',
                ),
                Container(
                  height: 16.0,
                ),
                Text(
                  item.value.label,
                  style: TextStyle(
                    fontSize: theme.fontSize9,
                    fontWeight: theme.heavyFontWeight,
                  ),
                ),
                Container(height: 2.0),
                Text(
                  '(${item.value.points})',
                  style: TextStyle(
                    fontSize: theme.fontSize8,
                    color: Color(0xFF9B9B9B),
                  ),
                ),
              ],
            ),
          )
          .toList(),
      rowMax: 4,
      lineHeight: 16.0,
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollLayout(
      topBar: BackTopBar(
        title: '킨즈나이테',
      ),
      body: Column(
        children: [
          Container(height: 40.0),
          renderSubtitle(),
          Container(height: 16.0),
          renderTreeAge(),
          Container(height: 24.0),
          Container(height: 5.0, color: Color(0xFFE8E7EC)),
          Container(height: 24.0),
          renderBody(),
        ],
      ),
    );

    // return TitleLayout(
    //   title: '킨즈나이테',
    //   body: Column(
    //     children: [
    //       renderSubtitle(),
    //       Container(height: 16.0),
    //       renderTreeAge(),
    //       Container(height: 24.0),
    //       Container(height: 5.0, color: Color(0xFFE8E7EC)),
    //       Container(height: 24.0),
    //       renderBody(),
    //     ],
    //   ),
    // );
  }
}
