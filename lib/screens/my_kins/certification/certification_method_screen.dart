import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/resource.dart';

import '../../../components/switch/kins_switch.dart';
import '../../../components/top_bar/back_top_bar.dart';

/*
 작성일 : 2021-01-22
 작성자 : Mark,
 화면명 : HY_8001,
 경로 : /my-kins/certification-method
 클래스 : CertificationMethodScreen,
 설명 : 인증수단관리 Screen
*/

class CertificationMethodScreen extends StatefulWidget {
  @override
  CertificationMethodState createState() => CertificationMethodState();
}

class CertificationMethodState extends State<CertificationMethodScreen> {
  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(
        title: '인증수단 관리',
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            SizedBox(height: 20),
            renderBtnListItem(
              '간편 비밀번호 재설정',
              onTap: () {
                Get.toNamed('/my-kins/reset/pin', arguments: true);
              },
            ),
            // renderBtnListItem(
            //   '비밀번호 찾기',
            //   onTap: () {
            //     Get.toNamed('/my-kins/search/password');
            //   },
            // ),
            renderBtnListItem(
              '비밀번호 재설정',
              onTap: () {
                Get.toNamed('/my-kins/reset/password');
              },
            ),
            // renderBtnListItem(
            //   '인증비밀번호 변경',
            //   onTap: () {},
            // ),
            // renderBtnListItem(
            //   '바이오메트릭 인증 시트 열기',
            //   onTap: () {
            //     Get.bottomSheet(
            //       BiometricsBottomSheet(),
            //     );
            //   },
            // ),
            // renderBtnListItem(
            //   '바이오메트릭 인증 등록 성공 시트 열기',
            //   onTap: () {
            //     Get.bottomSheet(
            //       BiometricsSuccessBottomSheet(),
            //     );
            //   },
            // ),
            ...renderSwitchList(
              '지문 인증',
              onCheckChange: (isChecked) {
                print(isChecked ? '지문 인증 Check' : '지문 인증 unCheck');
              },
            ),
          ],
        ),
      ),
    );

    // return DefaultLayout(
    //   body: Padding(
    //     padding: EdgeInsets.only(top: 16, left: 16, right: 16),
    //     child: Column(
    //       children: [
    //         BackTopBar(
    //           title: '인증수단 관리',
    //           useDefaultPadding: false,
    //         ),
    //         SizedBox(height: 20),
    //         renderBtnListItem(
    //           '간편 비밀번호 재설정',
    //           onTap: () {
    //             Get.toNamed('/my-kins/reset/pin');
    //           },
    //         ),
    //         renderBtnListItem(
    //           '비밀번호 찾기',
    //           onTap: () {
    //             Get.toNamed('/my-kins/search/password');
    //           },
    //         ),
    //         renderBtnListItem(
    //           '비밀번호 재설정',
    //           onTap: () {
    //             Get.toNamed('/my-kins/reset/password');
    //           },
    //         ),
    //         renderBtnListItem(
    //           '인증비밀번호 변경',
    //           onTap: () {},
    //         ),
    //         renderBtnListItem(
    //           '바이오메트릭 인증 시트 열기',
    //           onTap: () {
    //             Get.bottomSheet(
    //               BiometricsBottomSheet(),
    //             );
    //           },
    //         ),
    //         renderBtnListItem(
    //           '바이오메트릭 인증 등록 성공 시트 열기',
    //           onTap: () {
    //             Get.bottomSheet(
    //               BiometricsSuccessBottomSheet(),
    //             );
    //           },
    //         ),
    //         ...renderSwitchList(
    //           '지문 인증',
    //           onCheckChange: (isChecked) {
    //             print(isChecked ? '지문 인증 Check' : '지문 인증 unCheck');
    //           },
    //         ),
    //       ],
    //     ),
    //   ),
    // );
  }

  renderBtnListItem(String title, {VoidCallback? onTap}) {
    return InkWell(
      child: Column(
        children: [
          Container(
            height: 67,
            width: double.infinity,
            alignment: Alignment.center,
            child: Row(
              children: [
                renderRowTitleText(title),
                Expanded(child: Container()),
              ],
            ),
          ),
          renderBottomLine(),
        ],
      ),
      onTap: onTap,
    );
  }

  renderSwitchList(String title, {OnCheckChange? onCheckChange}) {
    return [
      Container(
        height: 67,
        width: double.infinity,
        alignment: Alignment.center,
        child: Row(
          children: [
            renderRowTitleText(title),
            Expanded(child: renderSwitch(onCheckChange)),
          ],
        ),
      ),
      renderBottomLine(),
    ];
  }

  renderSwitch(OnCheckChange? onCheckChange) {
    if (onCheckChange == null) return Container();
    return Container(
      alignment: Alignment.centerRight,
      child: KinsSwitch(onCheckChange: onCheckChange),
    );
  }

  renderRowTitleText(String title) {
    return Text(
      title,
      style: TextStyle(
        color: kinsGray63,
        fontWeight: FontWeight.w400,
        fontSize: ThemeFactory.of(context).theme.fontSize15,
      ),
    );
  }

  renderBottomLine() {
    return Container(
      height: 1,
      color: kinsGrayE4,
    );
  }
}
