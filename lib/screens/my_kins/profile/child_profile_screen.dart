import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/avatar/profile_image_avatar.dart';
import 'package:kins_healthcare/components/bottom_sheet/simple_information_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/text_field/dob_text_form_field.dart';
import 'package:kins_healthcare/components/text_field/kid_name_text.field.dart';
import 'package:kins_healthcare/components/text_field/text_field.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/screens/auth/verify/phone/phone_screen.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/mykins/model/child_profile_update_body.dart';
import 'package:kins_healthcare/services/mykins/model/post_only_child_id_body.dart';
import 'package:kins_healthcare/services/user/model/user_info_models.dart';

/*
 * 작성일 : 2021-04-14
 * 작성자 : Andy
 * 화면명 : 
 * 주요기능 : 자식 프로필 수정
 */
class ChildProfileScreen extends StatefulWidget {
  @override
  _ChildProfileScreenState createState() => _ChildProfileScreenState();
}

class _ChildProfileScreenState extends State<ChildProfileScreen> {
  File? profileImg;
  String? filePath;

  UserInfoResponse? _childInfo;

  String _firstName = '';
  String _lastName = '';
  String? _birth = '';
  String? _phone = '';

  /// 전체필드에 수정사항이 있을경우 true
  bool _isChange = false;

  bool doubleClick = false; // 다음버튼 중복 탭 방지

  /// 전화번호 변경 및 인증이 완료된 경우 true
  bool _isPhoneChange = false;

  late final _childUserNo;

  @override
  void initState() {
    super.initState();
    _childUserNo = Get.parameters['id'];

    if (_childUserNo == null) Get.back();

    Future.microtask(() async {
      try {
        _childInfo = await Get.find<MykinsController>().childInfo(
            body: PostOnlyChildIdBody(
                childUserNO: int.parse(
          _childUserNo!,
        )));
        if (_childInfo != null) {
          _firstName = _childInfo!.firstname;
          _lastName = _childInfo!.lastname;
          _birth = _childInfo!.birth;
        }

        setState(() {});
      } on DioError catch (e) {
        final msg = e.response!.data.toString();
        final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
        await Get.bottomSheet(sheet);
        Get.back();
      }
    });
  }

  _sendData() async {
    if (doubleClick) return;
    doubleClick = true;
    final sendData = ChildProfileUpdateBody(
      childUserNO: _childUserNo,
      lastname: _lastName,
      firstname: _firstName,
      birth: _birth,
      image: filePath ?? _childInfo!.image,
      phone: _isPhoneChange ? _phone : _childInfo!.phone,
    );
    try {
      final result = await Get.find<MykinsController>().modifyChildInfo(body: sendData);
      if (result.resultCode == 'SUCCESS') {
        final sheet = SimpleInformationBottomSheet(message: '자녀 회원정보가 수정되었습니다.');
        await Get.bottomSheet<String>(sheet);
        Get.back();
      }
    } on DioError catch (e) {
      doubleClick = false;
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
      return;
    }
  }

  _phoneChange() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final result = await Get.toNamed(
      '/auth/verify/phone/phone',
      arguments: PhoneScreenArguments(
        phone: _phone!,
      ),
    );
    if (result == 'OK') {
      Get.snackbar('알림', '인증되었습니다.');
      _isChange = true;
      _isPhoneChange = true;
    } else
      _isPhoneChange = false;

    setState(() {});
  }

  renderAvatar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ProfileImageAvatar(
          initialUrl: _childInfo!.image,
          onChange: (File image) {
            setState(() {
              profileImg = image;
            });
          },
          onUploadFinish: (String path) {
            setState(() {
              this.filePath = path;
              setState(() {
                _isChange = true;
              });
            });
          },
        ),
      ],
    );
  }

  renderVerificationCode() {
    return Row(
      children: [
        Expanded(
          child: UnderlineTextFormField(
            hintText: '인증번호를 입력하세요',
            validator: (val) {
              return null;
            },
            onSaved: (val) {},
          ),
        ),
        Container(width: 12.0),
        PrimaryButton(
          label: '인증확인',
          onTap: () {},
        ),
      ],
    );
  }

  renderFields() {
    // 텍스트필드 Form Field 형태로 한번 다 갈아 엎어야함.
    // onSaved 했을때 상태관리는 하되 validator 는 widget 에서 관리
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          AbsorbPointer(
              child: UnderlineTextFormField(
            validator: (val) {
              return null;
            },
            onSaved: (val) {},
            labelText: '이메일(ID)',
            initialValue: _childInfo!.email,
            readOnly: true,
          )
              // EmailTextField(
              //   onSaved: (val){},
              //   hasCreateEmail: false,
              //   customLabel: '이메일(ID)',
              //   // readOnlyValue: _childInfo!.email,
              // ),
              ),
          Container(height: 22.0),
          KidNameTextField(
            onSavedChildNam: (val) {},
            onSavedFamilyName: (val) {},
            readOnlyValueChildName: _childInfo!.firstname,
            readOnlyValueFamilyName: _childInfo!.lastname,
            onChangeLastName: (val) {
              _lastName = val;
              setState(() {
                _isChange = true;
              });
            },
            onChangeFirstName: (val) {
              _firstName = val;

              setState(() {
                _isChange = true;
              });
            },
          ),
          Container(height: 22.0),
          AbsorbPointer(
            child: DobTextFormField(
              onSaved: (val) => null,
              onChange: (val) {
                _birth = val;
                setState(() {
                  _isChange = true;
                });
              },
              validator: (val) {
                return null;
              },
              readOnlyValue: _childInfo!.birth,
            ),
          ),
          Container(height: 22.0),
          UnderlineTextFormField(
            initialValue: _childInfo!.phone,
            keyboardType: TextInputType.number,
            maxLength: 11,
            labelText: '전화번호',
            validator: (val) {
              return null;
            },
            onSaved: (val) {},
            onChange: (value) {
              if (value.length >= 10 && value.length <= 11) {
                _phone = value;
              } else
                _phone = '';
              setState(() {});
            },
            suffixIcon: TextButton(
              onPressed: _phone?.length != 0 ? _phoneChange : null,
              child: Text("전화번호 인증하기"),
            ),
          ),
        ],
      ),
    );
  }

  renderFooter() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            PrimaryButton(
              label: '저장',
              onTap: _isChange ? _sendData : null,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollLayout(
      topBar: BackTopBar(
        title: '자녀 회원정보',
      ),
      body: _childInfo != null
          ? Column(
              children: [
                Container(height: 40.0),
                renderAvatar(),
                Container(height: 40.0),
                renderFields(),
                renderFooter(),
              ],
            )
          : Center(child: CircularProgressIndicator()),
    );
  }
}
