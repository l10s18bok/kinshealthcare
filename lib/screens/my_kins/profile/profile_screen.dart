import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:kins_healthcare/components/avatar/profile_image_avatar.dart';
import 'package:kins_healthcare/components/bottom_sheet/simple_information_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/text_field/dob_text_form_field.dart';
import 'package:kins_healthcare/components/text_field/name_text_field.dart';
import 'package:kins_healthcare/components/text_field/text_field.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/mykins_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/screens/auth/register/register_screen.dart';
import 'package:kins_healthcare/screens/auth/verify/phone/phone_screen.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/error_bottom_sheet.dart';
import 'package:kins_healthcare/services/mykins/model/child_profile_update_body.dart';
import 'package:kins_healthcare/services/user/model/user_info_models.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';
import 'package:kins_healthcare/utils/validator_utils.dart';

/*
 * 작성일 : 2021-01-28
 * 작성자 : JC / Andy
 * 화면명 : HY_1002
 * 주요기능 : 사용자 프로필 / 자녀 프로필 수정 
 */

class ProfileScreen extends StatefulWidget {
  final String title;

  const ProfileScreen({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  late SignUpType signUpType;

  final formKey = GlobalKey<FormState>(); //2021.04.19 Andy 추가

  File? profileImg;
  String? filePath;

  late final String _childUserNo;

  String? _firstname; // 2021.04.19 Andy 추가
  String? _lastname; // 2021.04.19 Andy 추가
  //String? _birth; // 2021.04.19 Andy 추가
  String? _phoneNumber = ''; // 2021.04.19 Andy 추가

  bool doubleClick = false; // 2021.04.19 Andy 추가 ..프로필 갱신중일때 입력받지 않음.. 프로필 수정중 : ture

  bool _isPhoneChange = false; // 전화번호 변경 인증 완료 확인 true = 인증완료

  @override
  void initState() {
    super.initState();

    final getP = Get.parameters['id'];

    signUpType = SignUpType.adult;

    if (widget.title == 'kid') {
      signUpType = SignUpType.kid;
      _childUserNo = getP as String;

    } else {
      Future.microtask(() {
        Get.find<UserController>().getUserInfo();
      });
    }
  }


  _sendData() async {
    // 2021.04.19 Andy 추가
    if (doubleClick) return;
    doubleClick = true;
    final signAdult = signUpType == SignUpType.adult;

    final c = Get.find<UserController>();  //사용자 프로필정보
    final ui = c.userInfo;
    
    final ch = Get.find<MykinsController>(); //자녀 프로필정보
    final cui = ch.myChildInfo;

    final firstNu = signAdult ? ui?.firstname : cui?.firstname;
    final lastNu = signAdult ? ui?.lastname : cui?.lastname;
    final phonNu = signAdult ? ui?.phone : cui?.phone;
    final birthD = signAdult ? ui?.birth : cui?.birth;
    final imagePath = signAdult ? ui?.image : cui?.image;

    final firstN = _firstname ?? firstNu;
    final lastN = _lastname ?? lastNu;
    final phonN = _isPhoneChange ? _phoneNumber : phonNu;
    final image = filePath ?? imagePath;

    final sendData = signAdult ? UserInfoResponse(
      firstname: firstN!,
      lastname: lastN!,
      birth: birthD,
      email: ui?.email,
      phone: phonN!,
      image: image!,
    ) : ChildProfileUpdateBody(
      childUserNO: _childUserNo,
      lastname: lastN,
      firstname: firstN,
      birth: birthD,
      image: image,
      phone: phonN,
    );

    try {
      if (signAdult) {
        await c.userModify(body: sendData as UserInfoResponse);
        Get.find<UserController>().getUserInfo();
      } else {
        await Get.find<MykinsController>().modifyChildInfo(body: sendData as ChildProfileUpdateBody);
      }
      final message = signAdult ? '회원정보가 수정되었습니다.' : '자녀 회원정보가 수정되었습니다.';
      final sheet = SimpleInformationBottomSheet(message: message);
      await Get.bottomSheet<String>(sheet);
      //if (signAdult) c.setUserInfo(sendData as UserInfoResponse);
      Get.back();
    } on DioError catch (e) {
      doubleClick = false;
      final msg = e.response!.data.toString();
      final sheet = ErrorBottomSheet(title: '서버통신 에러', content: msg);
      Get.bottomSheet(sheet);
      return;
    }
  }

  _phoneChange() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final result = await Get.toNamed(
      '/auth/verify/phone/phone',
      arguments: PhoneScreenArguments(
        phone: _phoneNumber!,
      ),
    );
    if (result == 'SUCCESS') {
      _isPhoneChange = true;
      print('===> result = ${result}');
      Get.snackbar('알림', '인증되었습니다.');
    } else
      _isPhoneChange = false;

    setState(() {});
  }

  renderProfileImg({required imgPath}) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ProfileImageAvatar(
            initialUrl: imgPath ?? '',
            onChange: (File image) {
              setState(() {
                profileImg = image;
              });
            },
            onUploadFinish: (String path) {
              setState(() {
                this.filePath = path;
              });
            },
          ),
        ],
      );
  }

  renderAvatar() {
      if (signUpType == SignUpType.kid) {
        return GetBuilder<MykinsController>(builder: (c) {

          return renderProfileImg(imgPath: c.myChildInfo?.image);
        });
      }

    return GetBuilder<UserController>(builder: (c) {

      return renderProfileImg(imgPath: c.userInfo?.image);
    });
    }

  renderVerificationCode() {
    return Row(
      children: [
        Expanded(
          child: UnderlineTextFormField(
            hintText: '인증번호를 입력하세요',
            validator: (val) {
              return null;
            },
            onSaved: (val) {},
          ),
        ),
        Container(width: 12.0),
        PrimaryButton(
          label: '인증확인',
          onTap: () {},
        ),
      ],
    );
  }

  renderLabel(String label, {String? subLabel}) {
    final theme = ThemeFactory.of(context).theme;
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        Text(
          label,
          style: TextStyle(
            fontSize: theme.fontSize13,
            color: Colors.black,
            fontWeight: theme.heavyFontWeight,
          ),
        ),
      ],
    );
  }

  renderEmailFields({required String email}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [renderLabel('이메일')],
        ),
        SizedBox(height: 10),
        Text(
          email,
          style: TextStyle(
            fontSize: 15.0,
            color: Colors.black,
          ),
        ),
        SizedBox(height: 5),
        Divider(
          color: Colors.black45,
          thickness: 1,
        ),
      ],
    );
  }

  renderCenterFields({final email, final firstN, final lastN, final birthD, final phonN}) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            renderEmailFields(email: email),
            Container(height: 22.0),
            NameTextField(
              onSavedFirstName: (val) {
                _firstname = val;
              },
              onSavedLastName: (val) {
                _lastname = val;
              },
              readOnlyValueFirstName: firstN ?? '',
              readOnlyValueLastName: lastN ?? '',
            ),
            Container(height: 22.0),
            AbsorbPointer(
              child: DobTextFormField(
                onSaved: (val) {},
                validator: (val) => Validators.dobValidator(val),
                readOnlyValue: birthD,
              ),
            ),
            Container(height: 22.0),
            UnderlineTextFormField(
              initialValue: phonN ?? '',
              keyboardType: TextInputType.number,
              labelText: '전화번호',
              maxLength: 11,
              validator: (val) {
                return null;
              },
              onSaved: (val) {},
              onChange: (value) {
                if (value.length >= 10 && value.length <= 11) {
                  _phoneNumber = value;
                } else
                  _phoneNumber = '';

                setState(() {});
              },
              suffixIcon: TextButton(
                onPressed: _phoneNumber?.length != 0 ? _phoneChange : null,
                child: Text('전화번호 인증하기'),
              ),
            ),

          ],
        ),
      );
  }

  renderFields() {
    // 텍스트필드 Form Field 형태로 한번 다 갈아 엎어야함.
    // onSaved 했을때 상태관리는 하되 validator 는 widget 에서 관리
    final signKid = signUpType == SignUpType.kid;

    if (signKid) {
      return GetBuilder<MykinsController>(builder: (c) {
        final ui = c.myChildInfo;
        return  renderCenterFields(email: ui?.email, firstN: ui?.firstname, lastN: ui?.lastname, birthD: ui?.birth, phonN: ui?.phone);
      });
      
    }

    return GetBuilder<UserController>(builder: (c) {
      final ui = c.userInfo;
      return renderCenterFields(email: ui?.email, firstN: ui?.firstname, lastN: ui?.lastname, birthD: ui?.birth, phonN: ui?.phone);
    });
  }

  renderFooter() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            // InkWell(
            //   onTap: () {
            //     Get.toNamed('/my-kins/delete-account');
            //   },
            //   child: Padding(
            //     padding: EdgeInsets.all(16.0),
            //     child: Text('회원탈퇴'),
            //   ),
            // ),
            PrimaryButton(
              label: '저장',
              onTap: doubleClick
                  ? null
                  : () async {
                      // if (this.filePath == null) {
                      //   Get.snackbar(
                      //     '이미지 업로드 실패',
                      //     '업로드된 이미지가 존재하지 않습니다.',
                      //   );
                      //   return;
                      // }

                      // await Get.find<UserController>().registerProfileImg(
                      //   path: this.filePath!,
                      // );
                      if (this.formKey.currentState!.validate()) {
                        //2021.04.19 Andy 수정
                        this.formKey.currentState!.save();
                        _sendData();
                      }
                    },
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollLayout(
      expandedHeight: 50,
      topBar: BackTopBar(
        title: signUpType == SignUpType.adult ? '회원정보' : '자녀 회원정보',
      ),
      body: Form(
        key: this.formKey,
        child: Column(
          children: [
            Container(height: 40.0),
            renderAvatar(),
            Container(height: 40.0),
            renderFields(),
            renderFooter(),
          ],
        ),
      ),
    );

    // return TitleLayout(
    //   title: '회원정보',
    //   body: Column(
    //     children: [
    //       Expanded(
    //         child: ListView(
    //           children: [
    //             renderAvatar(),
    //             Container(height: 40.0),
    //             renderFields(),
    //           ],
    //         ),
    //       ),
    //       renderFooter(),
    //     ],
    //   ),
    // );
  }
}
