import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/bottom_sheet/auth_bottom_sheet.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/text_field/password_text_field.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/custom_scroll_layout.dart';
import 'package:kins_healthcare/services/user/model/password_modify_models.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';

/*
 * 작성일 : 2021-02-09
 * 작성자 : JC
 * 화면명 : HY_8005
 * 주요기능 : 비밀번호 재설정
 */
class PasswordScreen extends StatefulWidget {
  @override
  _PasswordScreenState createState() => _PasswordScreenState();
}

class _PasswordScreenState extends State<PasswordScreen> {
  final formKey = GlobalKey<FormState>();

  String curPassword = '';
  String newPassword = '';
  String newPasswordConfirm = '';

  renderFields() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              PasswordTextField(
                customLabel: '현재 비밀번호',
                onSaved: (String? value) {
                  setState(() {
                    curPassword = value ?? '';
                  });
                },
              ),
              Container(height: 16.0),
              // ignore: missing_required_param
              PasswordTextField(
                customLabel: '새 비밀번호',
                onSaved: (val) {
                  setState(() {
                    newPassword = val ?? '';
                  });
                },
              ),
              Container(height: 16.0),
              // ignore: missing_required_param
              PasswordTextField(
                customLabel: '새 비밀번호 확인',
                onSaved: (val) {
                  setState(() {
                    newPasswordConfirm = val ?? '';
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  renderButton() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: PrimaryButton(
        label: '다음',
        onTap: () async {
          if (this.formKey.currentState?.validate() == true) {
            this.formKey.currentState!.save();

            try {
              await Get.find<UserController>().updatePassword(
                body: PasswordModifyBody(
                  pw: curPassword,
                  pwNew: newPassword,
                  pwNewCheck: newPasswordConfirm,
                ),
              );

              await Get.bottomSheet(
                AuthBottomSheet(
                  title: '비밀번호 변경 완료',
                ),
              );
              Get.offNamedUntil('/my-kins', (route) => false);
              // Get.snackbar('비밀번호 변경 완료', '비밀번호가 변경되었습니다.');
            } on DioError catch (e) {
              SnackbarUtils().parseDioErrorMessage(
                error: e,
                title: '비밀번호 변경 실패',
              );
            }
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollLayout(
      topBar: BackTopBar(
        title: '비밀번호 재설정',
      ),
      body: Column(
        children: [
          Container(height: 40.0),
          renderFields(),
          renderButton(),
        ],
      ),
    );

    // return TitleLayout(
    //   title: '비밀번호 재설정',
    //   body: Column(
    //     children: [
    //       renderFields(),
    //       renderButton(),
    //     ],
    //   ),
    // );
  }
}
