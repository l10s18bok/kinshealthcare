import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/components/button/button.dart';
import 'package:kins_healthcare/components/pin_entry/custom_pin_entry.dart';
import 'package:kins_healthcare/controllers/cert_controller.dart';
import 'package:kins_healthcare/controllers/user_controller.dart';
import 'package:kins_healthcare/layouts/layouts.dart';
import 'package:kins_healthcare/screens/spinor_test/mark/components/bottom_sheet/ok_bottom_sheet.dart';
import 'package:kins_healthcare/services/cert/model/simple_password_certify_model.dart';
import 'package:kins_healthcare/utils/snackbar_utils.dart';

/**
 * 작성일 : 2021-04-15
 * 작성자 : Daniel
 * 화면명 : HY_8003
 * 경로: '/my-kins/reset/pin'
 *
 * 주요기능 : 간편 비밀번호 재설정
 */

enum InputState { inputCurPin, inputChangePin, checkChangePin }

class ResetPinScreen extends StatefulWidget {
  @override
  _ResetPinScreenState createState() => _ResetPinScreenState();
}

class _ResetPinScreenState extends State<ResetPinScreen> {
  final FocusNode focus = FocusNode(); // 2021.05.07 Andy 추가
  String password = '';
  String pin = '';
  bool checker = false;
  bool errorChecker = false;
  String checkPin = '';

  String resp = '';

  InputState curInputState = InputState.inputCurPin;

  TextEditingController textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _checkSimplePasswordIsSet();
  }

  _checkSimplePasswordIsSet() async {
    checker = await Get.find<UserController>().verifySimplePassword();
  }

  renderPinCodeField() {
    String text = '현재 간편 비밀번호를 입력하세요';
    switch (curInputState) {
      case InputState.inputCurPin:
        text = '현재 간편 비밀번호를 입력하세요';
        break;
      case InputState.inputChangePin:
        text = '변경하실 간편 비밀번호를 입력하세요';
        break;
      case InputState.checkChangePin:
        text = '변경하실 간편 비밀번호를 한 번 더 입력하세요';
        break;
    }
    return renderTitle(text);
  }

  Expanded renderTitle(String _bolText) {
    return Expanded(
      child: CustomPinEntry(
        textEditingController: textEditingController,
        focus: focus,
        boldText: _bolText,
        boldTextSize: 15,
        onChanged: (val) {
          setState(() {
            pin = val;
            if (pin.length == 6) {
              if (curInputState == InputState.inputCurPin) {
                certifyPin();
              } else if (curInputState == InputState.inputChangePin) {
                checkPin = pin;
                curInputState = InputState.checkChangePin;

                WidgetsBinding.instance!.addPostFrameCallback((_) {
                  textEditingController.clear();
                  pin = '';
                  FocusScope.of(context).requestFocus(focus);
                });
              } else {
                changePin();
              }
            }
          });
        },
      ),
    );
  }

  certifyPin() async {
    if (checker == false) {
      errorChecker = true;
      Get.snackbar(
        '간편 비밀번호가 등록되지 않은 유저입니다.',
        '1:1 문의를 이용하여 문의 해주세요.',
      );
      return;
    } else {
      errorChecker = false;
      try {
        await Get.find<CertController>().simplepwCertify(body: SimplePwCertifyBody(simplePw: pin));
        curInputState = InputState.inputChangePin;
        FocusScope.of(context).requestFocus(focus);
      } on DioError catch (e) {
        SnackbarUtils().parseDioErrorMessage(
          error: e,
          title: '잘못된 간편 비밀번호 입니다.',
        );
      }
    }

    textEditingController.clear();
    pin = '';
  }

  // checkPin() async {}

  changePin() async {
    String title = '';
    String msg = '';
    if (checkPin == pin) {
      try {
        errorChecker = false;
        await Get.find<UserController>().updateSimplePassword(password: pin);
        title = '간편 비민번호 변경이\n완료되었습니다 :)';
        Get.back();
      } on DioError catch (e) {
        title = '간편 비밀번호 변경에\n실패하였습니다 :(';
        Get.snackbar(
          '간편 비밀번호 저장 실패',
          e.response!.data['resultMsg'],
        );
      }
      final sheet = OkBottomSheet(title: title, content: msg);
      Get.bottomSheet(sheet);
    } else {
      Get.snackbar('간편 비밀번호가 다릅니다.', '다시 입력해주세요.');
    }
  }

  renderNextButton() {
    return PrimaryButton(
      label: '다음',
      onTap: () async {
        if (this.pin.length != 6) {
          errorChecker = true;
          Get.snackbar(
            '간편 비밀번호를 입력해주세요',
            '간편 비밀번호 6자리를 입력해주세요',
          );
          return;
        } else if (curInputState == InputState.inputCurPin) {
          certifyPin();
          textEditingController.clear();
          pin = '';
        } else if (curInputState == InputState.inputChangePin) {
          changePin();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return TitleLayout(
      title: '간편 비밀번호 재설정',
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            renderPinCodeField(),
            // renderNextButton(),
            Container(height: 8.0),
          ],
        ),
      ),
    );
  }
}
