import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/layouts/layouts.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class RequestDetailLayout extends StatefulWidget {
  final Widget body;
  final Widget titleWidget;
  final Widget action;

  RequestDetailLayout({
    required this.body,
    required this.titleWidget,
    required this.action,
  });

  @override
  _RequestDetailLayoutState createState() => _RequestDetailLayoutState();
}

class _RequestDetailLayoutState extends State<RequestDetailLayout> {
  renderAppbar() {
    final theme = ThemeFactory.of(context).theme;

    return Container(
      height: 64.0,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: theme.primaryGreyColor,
            width: 1,
          ),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 16.0,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                Get.back();
              },
              child: SvgPicture.asset(
                'assets/svgs/ic/ic_arrow_left.svg',
              ),
            ),
            Row(
              children: [
                widget.titleWidget,
              ],
            ),
            widget.action,
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      body: Column(
        children: [
          renderAppbar(),
          Expanded(
            child: widget.body,
          ),
        ],
      ),
    );
  }
}
