import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/layouts/layouts.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class TitleLayout extends StatefulWidget {
  final Widget body;
  final String? title;
  final String? actionText;
  final GestureTapCallback? onActionTap;
  final bool? resizeToAvoidBottomInset;
  final Widget? customActionWidget;
  final GestureTapCallback? onTap;
  final PointerDownEventListener? onPointerDown;
  final Widget? bottomNavigationBar;

  TitleLayout({
    required this.body,
    this.title,
    this.actionText,
    this.onActionTap,
    this.resizeToAvoidBottomInset,
    this.customActionWidget,
    this.onTap,
    this.onPointerDown,
    this.bottomNavigationBar,
  });

  @override
  _TitleLayoutState createState() => _TitleLayoutState();
}

class _TitleLayoutState extends State<TitleLayout> {
  renderTitle() {
    final theme = ThemeFactory.of(context).theme;

    if (widget.title != null) {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  widget.title!,
                  style: TextStyle(
                    fontSize: theme.fontSize22,
                    fontWeight: theme.heavyFontWeight,
                  ),
                ),
                if (widget.actionText != null)
                  GestureDetector(
                    onTap: widget.onActionTap,
                    child: Text(
                      widget.actionText!,
                      style: TextStyle(
                        color: theme.primarySkyBlueColor,
                        fontSize: theme.fontSize14,
                      ),
                    ),
                  ),
                if (widget.customActionWidget != null) widget.customActionWidget!,
              ],
            ),
            Container(
              height: 40.0,
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  renderAppBar() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            InkWell(
              onTap: () {
                Get.back();
              },
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 16.0,
                  vertical: 6.0,
                ),
                child: SvgPicture.asset(
                  'assets/svgs/ic/ic_arrow_left.svg',
                ),
              ),
            ),
          ],
        ),
        Container(height: 18.0),
        renderTitle(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultLayout(
      onTap: widget.onTap,
      onPointerDown: widget.onPointerDown,
      resizeToAvoidBottomInset: widget.resizeToAvoidBottomInset,
      body: Column(
        children: [
          renderAppBar(),
          Expanded(
            child: widget.body,
          ),
        ],
      ),
      bottomNavigationBar: widget.bottomNavigationBar,
    );
  }
}
