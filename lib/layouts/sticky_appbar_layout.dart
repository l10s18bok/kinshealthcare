import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:kins_healthcare/layouts/default_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class StickyAppbarLayout extends StatefulWidget {
  final Widget child;
  final GestureTapCallback onButtonTap;
  final bool renderListView;

  StickyAppbarLayout({
    required this.child,
    required this.onButtonTap,
    this.renderListView = false,
  });

  @override
  _StickyAppbarLayoutState createState() => _StickyAppbarLayoutState();
}

class _StickyAppbarLayoutState extends State<StickyAppbarLayout> {
  int length = 20;
  late ScrollController controller;
  double minHeight = 200;
  double maxHeight = 200;
  GlobalKey sliverAppbarKey = GlobalKey();
  double offset = 150;

  @override
  initState() {
    super.initState();

    controller = ScrollController();

    controller.addListener(() {
      setState(() {});
    });
  }

  renderAppbar() {
    return SliverAppBar(
      backgroundColor: Colors.white,
      automaticallyImplyLeading: false,
      flexibleSpace: renderFlexibleSpace(),
      floating: true,
      collapsedHeight: minHeight,
      expandedHeight: maxHeight,
      elevation: 0,
    );
  }

  renderFlexibleSpace() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            InkWell(
              onTap: () {
                Get.back();
              },
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 16.0,
                  vertical: 6.0,
                ),
                child: SvgPicture.asset(
                  'assets/svgs/ic/ic_arrow_left.svg',
                ),
              ),
            ),
          ],
        ),
        Container(height: 18.0),
        renderTitle(),
      ],
    );
  }

  renderTitle() {
    final theme = ThemeFactory.of(context).theme;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '타이틀',
                style: TextStyle(
                  fontSize: theme.fontSize22,
                  fontWeight: theme.heavyFontWeight,
                ),
              ),
              ElevatedButton(
                onPressed: widget.onButtonTap,
                child: Text('체체체체체인지'),
              ),
            ],
          ),
          Container(
            height: 40.0,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final padding = MediaQuery.of(context).padding;
    final height = size.height - this.maxHeight - padding.top - padding.bottom;

    return DefaultLayout(
      body: NestedScrollView(
        floatHeaderSlivers: true,
        controller: controller,
        headerSliverBuilder: (context, innerBoxIsScrolled) {
          return [
            renderAppbar(),
          ];
        },
        body: Builder(
          builder: (context) {
            return CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: SingleChildScrollView(
                    child: IntrinsicHeight(
                      child: Container(
                        constraints: BoxConstraints(
                          minHeight: height,
                        ),
                        child: widget.child,
                      ),
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
