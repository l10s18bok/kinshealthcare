import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kins_healthcare/components/top_bar/back_top_bar.dart';
import 'package:kins_healthcare/layouts/nested_scroll_layout.dart';
import 'package:kins_healthcare/themes/theme_factory.dart';

class PaymentIconLayout extends StatefulWidget {
  final Widget body;
  final String title;
  final String message;
  final SvgPicture icon;

  /// 아이콘과 메세지 바로 밑에 추가할 위젯
  final Widget? customBottomWidget;

  PaymentIconLayout({
    required this.body,
    required this.title,
    required this.message,
    this.customBottomWidget,
    SvgPicture? icon,
  }) : this.icon = icon ??
            SvgPicture.asset('assets/svgs/ic/ic_bannerimg_02_payment.svg');

  @override
  _PaymentIconLayoutState createState() => _PaymentIconLayoutState();
}

class _PaymentIconLayoutState extends State<PaymentIconLayout> {
  renderTopIcon() {
    return Container(
      alignment: Alignment.center,
      child: widget.icon,
    );
  }

  renderMessage() {
    final theme = ThemeFactory.of(context).theme;

    return Text(
      widget.message,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Color(0xFF4C4C4C),
        fontWeight: theme.heavyFontWeight,
        fontSize: theme.fontSize14,
      ),
    );
  }

  renderHeader() {
    return Column(
      children: [
        renderTopIcon(),
        Container(height: 16.0),
        renderMessage(),
        if (widget.customBottomWidget != null) widget.customBottomWidget!,
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollLayout(
      topBar: BackTopBar(
        title: widget.title,
      ),
      body: Column(
        children: [
          renderHeader(),
          Container(height: 24.0),
          Expanded(
            child: widget.body,
          ),
        ],
      ),
    );
    // return TitleLayout(
    //   title: widget.title,
    //   body: Column(
    //     children: [
    //       renderHeader(),
    //       Container(height: 24.0),
    //       Expanded(
    //         child: widget.body,
    //       ),
    //     ],
    //   ),
    // );
  }
}
